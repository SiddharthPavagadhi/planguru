﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Xml;

namespace Recurly
{
    public class RecurlyAddOn
    {
        //public RecurlySubscription Subscription { get; private set; }
        public RecurlyAccount Account { get;  set; }       
        public string add_on_code { get; set; }
        public string name { get; set; }
        private bool display_quantity_on_hosted_page { get; set; }
        public int? quantity { get; set; }
        public int unit_amount_in_cents { get; set; }
        public List<RecurlyAddOn> Addons;
        public RecurlyAddOn Addon;
        /// <summary>
        /// Account Code or unique ID for the account in Recurly
        /// </summary>
        /// 
        public string AccountCode { get; set; }

        //private const string UrlPrefix = "/accounts/";
        private const string UrlPostfix = "/add_ons";

        public RecurlyAddOn()
        {

        }

        public RecurlyAddOn(RecurlyAccount AccountCode) : this()
        {
            this.Account = AccountCode;
            //this.quantity = 1;
        }

        //public static RecurlyAddOn Get(string AccountCode)
        //{
        //    return Get(new RecurlyAccount(AccountCode));
        //}

        //public static RecurlyAddOn Get(RecurlyAccount AccountCode)
        //{
        //    RecurlyAddOn addOn = new RecurlyAddOn(AccountCode);

        //    HttpStatusCode statusCode = RecurlyClient.PerformRequest(RecurlyClient.HttpRequestMethod.Get,
        //        AddOnUrl(AccountCode.AccountCode),
        //        new RecurlyClient.ReadXmlDelegate(addOn.ReadXml));

        //    if (statusCode == HttpStatusCode.NotFound)
        //        return null;

        //    return addOn;
        //}

        internal RecurlyAddOn(XmlTextReader xmlReader)
        {
            ReadXml(xmlReader);
        }

        /// <summary>
        /// Update an account's billing info in Recurly
        /// </summary>
        public void Create()
        {
            Update();
        }

        /// <summary>
        /// Update an AddOn in Recurly
        /// </summary>
        public void Update()
        {
             HttpStatusCode statusCode = RecurlyClient.PerformRequest(RecurlyClient.HttpRequestMethod.Post,
                AddOnUrl(this.Account.AccountCode),
                new RecurlyClient.WriteXmlDelegate(this.WriteXml),
                new RecurlyClient.ReadXmlDelegate(this.ReadXml));
        }

         internal static string AddOnUrl(string accountCode)
         {
             //return RecurlyAddOn.UrlPrefix + System.Web.HttpUtility.UrlEncode(accountCode) + UrlPostfix;
             return System.Web.HttpUtility.UrlEncode(accountCode) + UrlPostfix;
         }

         internal void ReadXml(XmlTextReader reader)
         {
             Addons = new List<RecurlyAddOn>();
             while (reader.Read())
             {
                 // End of billing_info element, get out of here
                 if (reader.Name == "add_ons" && reader.NodeType != XmlNodeType.EndElement) { }                    
                     
                 if (reader.Name == "add_on" && reader.NodeType != XmlNodeType.EndElement)
                        Addon = new RecurlyAddOn();

                 if (reader.Name == "add_on" && reader.NodeType == XmlNodeType.EndElement)
                        Addons.Add(Addon); 

                 if (reader.NodeType == XmlNodeType.Element)
                 {
                     switch (reader.Name)
                     {
                         case "add_on_code":
                             this.Addon.add_on_code = reader.ReadElementContentAsString();
                             break;

                         case "name":
                             this.Addon.name = reader.ReadElementContentAsString();
                             break;

                         case "display_quantity_on_hosted_page":
                             this.Addon.display_quantity_on_hosted_page = Convert.ToBoolean(reader.ReadElementContentAsString());
                             break;

                         case "quantity":
                         case "default_quantity":
                             this.Addon.quantity = Convert.ToInt16(reader.ReadElementContentAsString());
                             break;

                         case "unit_amount_in_cents":
                         case "default_unit_amount_in_cents":
                             this.Addon.unit_amount_in_cents = Convert.ToInt16(reader.ReadElementContentAsString());
                             break;
                     }
                 }
             }
         }

         internal void WriteXml(XmlTextWriter xmlWriter)
         {
             xmlWriter.WriteStartElement("add_on"); // Start: add_on

             xmlWriter.WriteElementString("add_on_code", this.add_on_code);
             xmlWriter.WriteElementString("name", this.name);
             //xmlWriter.WriteElementString("display_quantity_on_hosted_page", this.display_quantity_on_hosted_page.ToString());
             xmlWriter.WriteElementString("quantity", this.quantity.ToString());
             xmlWriter.WriteElementString("unit_amount_in_cents", this.unit_amount_in_cents.ToString());

             xmlWriter.WriteEndElement(); // End: AddOn
         }
    }
}
