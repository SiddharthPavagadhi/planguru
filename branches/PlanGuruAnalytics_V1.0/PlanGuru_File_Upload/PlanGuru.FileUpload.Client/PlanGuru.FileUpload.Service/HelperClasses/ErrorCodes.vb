﻿Imports System.Resources
Public Class ErrorCodes

    Friend Const SQLExceptionCode As Integer = 201
    Friend Const NullReferenceCode As Integer = 301
    Friend Const IndexOutOfRangeCode As Integer = 401
    Friend Const CastExceptionCode As Integer = 501
    Friend Const GenericCode As Integer = 601
    Friend Const GenericInternalCheckFailure As Integer = 701

    ''' <summary>
    ''' his method returns the respective error message from the key.
    ''' </summary>
    ''' <param name="key">Error Code</param>
    ''' <returns>Error Message As String</returns>
    ''' <remarks></remarks>
    Public Shared Function ReadResourceValue(ByVal key As String) As String
        Dim resourceValue = String.Empty
        Dim objresourceManager As Global.System.Resources.ResourceManager = New Global.System.Resources.ResourceManager("PlanGuru.FileUpload.Service.Resources", GetType(ResourceSet).Assembly)
        If Not (String.IsNullOrEmpty(objresourceManager.GetString(key))) Then
            resourceValue = objresourceManager.GetString(key)
        End If
        Return resourceValue
    End Function

End Class
