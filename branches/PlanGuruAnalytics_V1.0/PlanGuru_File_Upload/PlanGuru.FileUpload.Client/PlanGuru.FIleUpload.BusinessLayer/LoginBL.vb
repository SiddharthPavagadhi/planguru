﻿Imports PlanGuru.FIleUpload.BusinessLayer.PlanGuruService

''' <summary>
''' This is Login Business Logic class which will call WCF Service methods to GET or POST data related to Login Functionality
''' </summary>
''' <remarks></remarks>
Public Class LoginBL

    Dim client As PlanGuruFileUploadServiceClient = New PlanGuruFileUploadServiceClient()
    Private _userName As String
    Private _userrole As Integer
    Dim objLoginBL As LoginBL
 

    ''' <summary>
    ''' Thie method will call WCF login method to check whether User is valid or not and 
    ''' If user valid then store the User information globally.
    ''' </summary>
    ''' <param name="uname">UserName as string</param>
    ''' <param name="pass">Password as string</param>
    ''' <returns>LoginContract Object</returns>
    ''' <remarks></remarks>
    Public Function Login(ByVal uname As String, ByVal pass As String) As LoginContract
        Dim objlogin As LoginContract = New LoginContract()

        Common.LogInfoWriter("Login in LoginBL execution start")
        Try
            Dim objBaseContract As BaseContract = New BaseContract With {.UserName = uname, .Password = pass}
            objlogin = client.Login(objBaseContract)
            GlobalSettings.UserName = objlogin.UserName
            GlobalSettings.UserRole = objlogin.UserRole
            GlobalSettings.UserId = objlogin.UserID
            GlobalSettings.Password = pass
            objlogin.ErrorMessage = Common.SUCCESS
            Common.LogInfoWriter("Login in LoginBL execution end")
        Catch ex As Exception
            objlogin.ErrorMessage = ex.Message
            Common.LogErrorWriter("Login in LoginBL execution end with error message:" & ex.Message & ex.StackTrace)
        End Try
        Return objlogin
    End Function

End Class
