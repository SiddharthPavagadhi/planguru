﻿Imports PlanGuru.FIleUpload.BusinessLayer.PlanGuruService

''' <summary>
''' This is Company Business Logic class which will call WCF Service methods to GET or POST data related to Company Functionality
''' </summary>
''' <remarks></remarks>
Public Class CompanyBL

    Dim client As PlanGuruFileUploadServiceClient = New PlanGuruFileUploadServiceClient()
 
    ''' <summary>
    ''' This method will call WCF GetCompanies to get the list of companies based on Logged in User.
    ''' </summary>
    ''' <returns>List of CompanyContract as object</returns>
    ''' <remarks></remarks>
    Public Function GetCompanies() As List(Of CompanyContract)
        Common.LogInfoWriter("GetCompanies in CompanyBL execution start")
        Dim companies As List(Of CompanyContract) = New List(Of CompanyContract)
        Try
            Dim objCompanyRequestContract As CompanyRequestContract = New CompanyRequestContract()
            objCompanyRequestContract.UserId = GlobalSettings.UserId
            objCompanyRequestContract.UserName = GlobalSettings.UserName
            objCompanyRequestContract.Password = GlobalSettings.Password
            companies = client.GetCompaniesByUserId(objCompanyRequestContract).ToList()
            Common.LogInfoWriter("GetCompanies in CompanyBL execution End")
        Catch ex As Exception
            Common.LogErrorWriter("GetCompanies in CompanyBL execution End with Error Message: " & ex.Message & ex.StackTrace)
        End Try
        Return companies.ToList()
    End Function

End Class
