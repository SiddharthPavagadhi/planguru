﻿Imports PlanGuru.FIleUpload.BusinessLayer.PlanGuruService

''' <summary>
''' This Class contains Shared Property as Global Data which can accessible anywhere in app.
''' </summary>
''' <remarks></remarks>
Public Class GlobalSettings

    Shared Property FilePath() As String

    Shared Property LogPath() As String 'SES Added for logging to PG data folder

    Shared Property UserId() As Integer
       
    Shared Property UserName() As String
     
    Shared Property Password() As String

    Shared Property UserRole() As String
     
    Shared Property FileData() As DataSet
    
    Shared Property ErrorData() As DataSet
     
    Shared Property UploadResponse() As FileUploadResponseContract
     
    Shared Property RequestErrorAcctType() As DataTable
     
    Shared Property RequestErrorAccount() As DataTable
     
    Shared Property RequestErrorBalance() As DataTable
    
End Class
