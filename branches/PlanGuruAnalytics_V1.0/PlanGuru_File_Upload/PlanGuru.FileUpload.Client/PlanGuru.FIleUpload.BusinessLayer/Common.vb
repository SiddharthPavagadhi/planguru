﻿Imports System.IO
Imports System.Data
Imports System.Data.OleDb

'This Common class have Logging Methods to write Information or Error logs in respective files.
Public Class Common

    Public Const DATEFORMAT As String = "dd-MM-yyyy_HH_mm"
    Public Const LoggerDateFormat As String = "dd-MM-yyyy"
    Public Const EXT_CSV As String = ".csv"
    Public Const EXT_XLS As String = ".xls"
    Public Const EXT_XLSX As String = ".xlsx"
    Public Const UPLOADEDFILES As String = "\UploadedFiles"
    Public Const INPUTFOLDER As String = "Input"
    Public Const ERRORFOLDER As String = "Error"
    Public Const ARCHIEVEFOLDER As String = "Archive"
    Public Const MSG_INFORMATION As String = "Information Message"
    Public Const MSG_ERROR As String = "Error Message"
    Public Const SUCCESS As String = "Success"
    Public Const Logfile_DateFormat As String = "[{0:MM/dd/yyyy HH:mm:ss.fff }] {1}"
    Shared _basePath As String

    Public Shared ReadOnly Property BaseDirectoryPath() As String
        Get
            '_basePath = Environment.CurrentDirectory.ToString() & UPLOADEDFILES
            _basePath = GlobalSettings.LogPath & UPLOADEDFILES
            Return _basePath
        End Get
    End Property
    Public Shared Property FilePath() As String
     
    ''' <summary>
    ''' 'This Method will Write Error logs into ErrorLog file.
    ''' </summary>
    ''' <param name="message">Message to Write in Logfile</param>
    ''' <remarks></remarks>
    Public Shared Sub LogErrorWriter(ByVal message As String)

        Dim FILE_NAME As String = GlobalSettings.LogPath & "Logs\Errors\ErrorLog" & Date.Now.Date.ToString(LoggerDateFormat) & ".log"

        If Not System.IO.Directory.Exists(GlobalSettings.LogPath & "Logs\Errors") Then
            System.IO.Directory.CreateDirectory(GlobalSettings.LogPath & "Logs\Errors")
        End If

        Dim sw As System.IO.StreamWriter = System.IO.File.AppendText(FILE_NAME)

        Try
            Dim logLine As String = System.String.Format(Logfile_DateFormat, DateTime.Now, message)
            sw.WriteLine(logLine)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        Finally
            sw.Close()
        End Try
    End Sub

    ''' <summary>
    ''' This Method will Write Info logs into InfoLog file.
    ''' </summary>
    ''' <param name="message">Message to write in Logfile</param>
    ''' <remarks></remarks>
    Public Shared Sub LogInfoWriter(ByVal message As String)

        Dim FILE_NAME As String = GlobalSettings.LogPath & "Logs\Info\InfoLog" & Date.Now.Date.ToString(LoggerDateFormat) & ".log"

        If Not System.IO.Directory.Exists(GlobalSettings.LogPath & "Logs\Info") Then
            System.IO.Directory.CreateDirectory(GlobalSettings.LogPath & "Logs\Info")
        End If

        Dim sw As System.IO.StreamWriter = System.IO.File.AppendText(FILE_NAME)

        Try
            Dim logLine As String = System.String.Format(Logfile_DateFormat, DateTime.Now, message)
            sw.WriteLine(logLine)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        Finally
            sw.Close()
        End Try
    End Sub


    Public Shared Sub TableBalanceColumnList(ByRef balanceColumnList As ArrayList)

        balanceColumnList.Add("BalanceId")

        For intHCtr = 1 To 5
            For intCtr = 1 To 12
                If (intCtr > 9) Then
                    balanceColumnList.Add(String.Concat("H", intHCtr.ToString(), intCtr.ToString()))
                Else
                    balanceColumnList.Add(String.Concat("H", intHCtr.ToString(), "0", intCtr.ToString()))
                End If
            Next
        Next

        For intCtr = 1 To 12
            If (intCtr > 9) Then
                balanceColumnList.Add(String.Concat("B1", intCtr.ToString()))
            Else
                balanceColumnList.Add(String.Concat("B10", intCtr.ToString()))
            End If
        Next

        For intCtr = 1 To 12
            If (intCtr > 9) Then
                balanceColumnList.Add(String.Concat("A1", intCtr.ToString()))
            Else
                balanceColumnList.Add(String.Concat("A10", intCtr.ToString()))
            End If
        Next

        balanceColumnList.Add("AccountID")

    End Sub

    Public Shared Function NumberWithSuffix(ByVal number As Integer) As String
        Dim numberSuffix As String

        If Right(number, 2) = "11" Or Right(number, 2) = "12" Or Right(number, 2) = "13" Then
            numberSuffix = String.Concat(number, "th")
        Else

            Select Case Right(number, 1)
                Case 1
                    numberSuffix = String.Concat(number, "st")
                Case 2
                    numberSuffix = String.Concat(number, "nd")
                Case 3
                    numberSuffix = String.Concat(number, "rd")
                Case Else
                    numberSuffix = String.Concat(number, "th")
            End Select
        End If

        Return numberSuffix
    End Function
End Class
