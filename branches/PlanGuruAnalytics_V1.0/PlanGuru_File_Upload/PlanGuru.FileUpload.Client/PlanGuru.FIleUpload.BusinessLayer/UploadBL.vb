﻿Imports PlanGuru.FIleUpload.BusinessLayer.PlanGuruService

''' <summary>
''' This is Upload Business Logic class which will call WCF Service methods to GET or POST data related to Upload Functionality.
''' </summary>
''' <remarks></remarks>
Public Class UploadBL

    Dim client As PlanGuruFileUploadServiceClient = New PlanGuruFileUploadServiceClient()
 
    ''' <summary>
    ''' This method will call WCF UploadFile Method with 
    ''' </summary>
    ''' <param name="dsExcelFileData"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function UploadFile(ByVal dsExcelFileData As DataSet, ByVal analysisId As String, ByVal companyid As String) As String
        Dim response As FileUploadResponseContract = New FileUploadResponseContract()
        Common.LogInfoWriter("UploadFile in UploadBL execution start")
        Try
            Dim request = ConvertDataSetToRequestContract(dsExcelFileData)

            request.AccountFailCount = GlobalSettings.RequestErrorAccount.Rows.Count
            request.AcctTypeFailCount = GlobalSettings.RequestErrorAcctType.Rows.Count
            request.BalanceFailCount = GlobalSettings.RequestErrorBalance.Rows.Count
            request.UserId = GlobalSettings.UserId
            request.AnalysisId = analysisId
            request.CompanyId = companyid
            request.FileName = System.IO.Path.GetFileName(GlobalSettings.FilePath)
            request.UserName = GlobalSettings.UserName
            request.Password = GlobalSettings.Password
            If Not request Is Nothing Then
                response = client.UploadFile(request)
            End If
            GlobalSettings.UploadResponse = response
            Common.LogInfoWriter("UploadFile in UploadBL execution end")
        Catch ex As Exception
            Common.LogErrorWriter("UploadFile in UploadBL execution end with Error Message: " & ex.Message & ex.StackTrace)
        End Try
       
        Return response.ResponseMessage
    End Function

    ''' <summary>
    ''' This Method will convert DataSet into RequestContract object.
    ''' </summary>
    ''' <param name="dsExcelFileData">Excel File as DataSet</param>
    ''' <returns>RequestContract Object</returns>
    ''' <remarks></remarks>
    Private Function ConvertDataSetToRequestContract(ByVal dsExcelFileData As DataSet) As FileUploadRequestContract

        Dim objRequestContract As FileUploadRequestContract = New FileUploadRequestContract()
        Try
            Common.LogInfoWriter("ConvertDataSetToRequestContract in UploadBL execution start")
            objRequestContract.AcctTypeData = ConvertAcctTypeDataToAcctTypeEntity(dsExcelFileData).ToArray()
            objRequestContract.AccountData = ConvertAccountTAccountEntity(dsExcelFileData).ToArray()
            objRequestContract.BalanceData = ConvertBalanceToBalanceEntity(dsExcelFileData).ToArray()
            Common.LogInfoWriter("ConvertDataSetToRequestContract in UploadBL execution end")
        Catch ex As Exception
            Common.LogErrorWriter("ConvertDataSetToRequestContract in UploadBL execution end with Error Message: " & ex.Message & ex.StackTrace)
        End Try
        Return objRequestContract

    End Function

    ''' <summary>
    ''' This Method will Convert AcctType data into AcctTypeEntity Object.
    ''' </summary>
    ''' <param name="dsExcelFileData">Excel File as DataSet</param>
    ''' <returns>AcctTypeEntity Object</returns>
    ''' <remarks></remarks>
    Private Function ConvertAcctTypeDataToAcctTypeEntity(ByVal dsExcelFileData As DataSet) As List(Of AcctTypeEntity)
        Dim objListAcctType As List(Of AcctTypeEntity) = New List(Of AcctTypeEntity)
        Try
            Common.LogInfoWriter("ConvertAcctTypeDataToAcctTypeEntity in UploadBL execution start")
            GlobalSettings.RequestErrorAcctType = CreateAcctTypeDataTable()
            Dim objAcctType As AcctTypeEntity
            If Not (dsExcelFileData.Tables("AcctType") Is Nothing) Then
                For Each AcctTypeRow In dsExcelFileData.Tables("AcctType").Rows
                    Try
                        objAcctType = New AcctTypeEntity()
                        objAcctType.AcctTypeId = AcctTypeRow.Item("AcctTypeId")
                        objAcctType.TypeDesc = AcctTypeRow.Item("TypeDesc").ToString()
                        objAcctType.ClassDesc = AcctTypeRow.Item("ClassDesc").ToString()
                        objAcctType.SubclassDesc = AcctTypeRow.Item("SubclassDesc").ToString()
                        objListAcctType.Add(objAcctType)
                    Catch ex As Exception
                        Dim newRow As DataRow = GlobalSettings.RequestErrorAcctType.NewRow()
                        newRow("AcctTypeId") = AcctTypeRow.Item("AcctTypeId").ToString()
                        newRow("TypeDesc") = AcctTypeRow.Item("TypeDesc").ToString()
                        newRow("ClassDesc") = AcctTypeRow.Item("ClassDesc").ToString()
                        newRow("SubclassDesc") = AcctTypeRow.Item("SubclassDesc").ToString()
                        GlobalSettings.RequestErrorAcctType.Rows.Add(newRow)
                    End Try

                Next
            End If
            Common.LogInfoWriter("ConvertAcctTypeDataToAcctTypeEntity in UploadBL execution end")
        Catch ex As Exception
            Common.LogErrorWriter("ConvertAcctTypeDataToAcctTypeEntity in UploadBL execution end with Error Message: " & ex.Message & ex.StackTrace)
        End Try
        Return objListAcctType
    End Function

    ''' <summary>
    ''' This Method will Convert Account data into AcctTypeEntity Object.
    ''' </summary>
    ''' <param name="dsExcelFileData">Excel File as DataSet</param>
    ''' <returns>AccountEntity Object</returns>
    ''' <remarks></remarks>
    Private Function ConvertAccountTAccountEntity(ByVal dsExcelFileData As DataSet) As List(Of AccountEntity)
        Dim objAccount As AccountEntity
        Dim objListAccount As List(Of AccountEntity) = New List(Of AccountEntity)

        Try
            Common.LogInfoWriter("ConvertAccountTAccountEntity in UploadBL execution start")
            GlobalSettings.RequestErrorAccount = CreateAccountDataTable()
            If Not (dsExcelFileData.Tables("Account") Is Nothing) Then
                For Each AccountRow In dsExcelFileData.Tables("Account").Rows
                    Try
                        objAccount = New AccountEntity()
                        objAccount.AccountId = AccountRow.Item("AccountId")
                        objAccount.AcctDescriptor = AccountRow.Item("AcctDescriptor").ToString()
                        objAccount.SortSequence = AccountRow.Item("SortSequence").ToString()
                        objAccount.Description = AccountRow.Item("Description").ToString()
                        objAccount.SubGrouping = AccountRow.Item("Subgrouping").ToString()                      
                        objAccount.AcctTypeId = AccountRow.Item("AcctTypeId").ToString()
                        objAccount.NumberFormat = IIf(String.IsNullOrEmpty(AccountRow.Item("NumberFormat").ToString()), 0, AccountRow.Item("NumberFormat").ToString())
                        objAccount.TotalType = IIf(String.IsNullOrEmpty(AccountRow.Item("TotalType").ToString()), 0, AccountRow.Item("TotalType").ToString())

                        objListAccount.Add(objAccount)
                    Catch ex As Exception
                        Dim newRow As DataRow = GlobalSettings.RequestErrorAccount.NewRow()
                        newRow("AccountId") = AccountRow.Item("AccountId").ToString()
                        newRow("AcctDescriptor") = AccountRow.Item("AcctDescriptor").ToString()
                        newRow("SortSequence") = AccountRow.Item("SortSequence").ToString()
                        newRow("Description") = AccountRow.Item("Description").ToString()
                        newRow("Subgrouping") = AccountRow.Item("Subgrouping").ToString()
                        newRow("AcctTypeId") = AccountRow.Item("AcctTypeId").ToString()
                        newRow("NumberFormat") = IIf(String.IsNullOrEmpty(AccountRow.Item("NumberFormat").ToString()), 0, AccountRow.Item("NumberFormat").ToString())
                        newRow("TotalType") = IIf(String.IsNullOrEmpty(AccountRow.Item("TotalType").ToString()), 0, AccountRow.Item("TotalType").ToString())
                        GlobalSettings.RequestErrorAccount.Rows.Add(newRow)
                    End Try

                Next
            End If
            Common.LogInfoWriter("ConvertAccountTAccountEntity in UploadBL execution end")
        Catch ex As Exception
            Common.LogErrorWriter("ConvertAccountTAccountEntity in UploadBL execution end with Error Message: " & ex.Message & ex.StackTrace)
        End Try
        Return objListAccount
    End Function

    ''' <summary>
    ''' This Method will Convert Balance data into BalnaceContract Object.
    ''' </summary>
    ''' <param name="dsExcelFileData">Excel File as DataSet</param>
    ''' <returns>BalnaceContract Object</returns>
    ''' <remarks></remarks>
    ''' 
    Private Function ConvertBalanceToBalanceEntity(ByVal dsExcelFileData As DataSet) As List(Of BalanceEntity)
        Dim objBalance As BalanceEntity
        Dim objListBalance As List(Of BalanceEntity) = New List(Of BalanceEntity)
        Try
            Common.LogInfoWriter("ConvertBalanceToBalanceEntity in UploadBL execution start")
            GlobalSettings.RequestErrorBalance = CreateBalanceDataTable()
            If Not (dsExcelFileData.Tables("Balance") Is Nothing) Then
                For Each BalanceRow In dsExcelFileData.Tables("Balance").Rows
                    Try
                        objBalance = New BalanceEntity()
                        objBalance.BalanceId = BalanceRow.Item("BalanceId")

                        objBalance.H101 = IIf(String.IsNullOrEmpty(BalanceRow.Item("H101").ToString()), 0, BalanceRow.Item("H101").ToString())
                        objBalance.H102 = IIf(String.IsNullOrEmpty(BalanceRow.Item("H102").ToString()), 0, BalanceRow.Item("H102").ToString())
                        objBalance.H103 = IIf(String.IsNullOrEmpty(BalanceRow.Item("H103").ToString()), 0, BalanceRow.Item("H103").ToString())
                        objBalance.H104 = IIf(String.IsNullOrEmpty(BalanceRow.Item("H104").ToString()), 0, BalanceRow.Item("H104").ToString())
                        objBalance.H105 = IIf(String.IsNullOrEmpty(BalanceRow.Item("H105").ToString()), 0, BalanceRow.Item("H105").ToString())
                        objBalance.H106 = IIf(String.IsNullOrEmpty(BalanceRow.Item("H106").ToString()), 0, BalanceRow.Item("H106").ToString())
                        objBalance.H107 = IIf(String.IsNullOrEmpty(BalanceRow.Item("H107").ToString()), 0, BalanceRow.Item("H107").ToString())
                        objBalance.H108 = IIf(String.IsNullOrEmpty(BalanceRow.Item("H108").ToString()), 0, BalanceRow.Item("H108").ToString())
                        objBalance.H109 = IIf(String.IsNullOrEmpty(BalanceRow.Item("H109").ToString()), 0, BalanceRow.Item("H109").ToString())
                        objBalance.H110 = IIf(String.IsNullOrEmpty(BalanceRow.Item("H110").ToString()), 0, BalanceRow.Item("H110").ToString())
                        objBalance.H111 = IIf(String.IsNullOrEmpty(BalanceRow.Item("H111").ToString()), 0, BalanceRow.Item("H111").ToString())
                        objBalance.H112 = IIf(String.IsNullOrEmpty(BalanceRow.Item("H112").ToString()), 0, BalanceRow.Item("H112").ToString())
                        objBalance.H201 = IIf(String.IsNullOrEmpty(BalanceRow.Item("H201").ToString()), 0, BalanceRow.Item("H201").ToString())
                        objBalance.H202 = IIf(String.IsNullOrEmpty(BalanceRow.Item("H202").ToString()), 0, BalanceRow.Item("H202").ToString())
                        objBalance.H203 = IIf(String.IsNullOrEmpty(BalanceRow.Item("H203").ToString()), 0, BalanceRow.Item("H203").ToString())
                        objBalance.H204 = IIf(String.IsNullOrEmpty(BalanceRow.Item("H204").ToString()), 0, BalanceRow.Item("H204").ToString())
                        objBalance.H205 = IIf(String.IsNullOrEmpty(BalanceRow.Item("H205").ToString()), 0, BalanceRow.Item("H205").ToString())
                        objBalance.H206 = IIf(String.IsNullOrEmpty(BalanceRow.Item("H206").ToString()), 0, BalanceRow.Item("H206").ToString())
                        objBalance.H207 = IIf(String.IsNullOrEmpty(BalanceRow.Item("H207").ToString()), 0, BalanceRow.Item("H207").ToString())
                        objBalance.H208 = IIf(String.IsNullOrEmpty(BalanceRow.Item("H208").ToString()), 0, BalanceRow.Item("H208").ToString())
                        objBalance.H209 = IIf(String.IsNullOrEmpty(BalanceRow.Item("H209").ToString()), 0, BalanceRow.Item("H209").ToString())
                        objBalance.H210 = IIf(String.IsNullOrEmpty(BalanceRow.Item("H210").ToString()), 0, BalanceRow.Item("H210").ToString())
                        objBalance.H211 = IIf(String.IsNullOrEmpty(BalanceRow.Item("H211").ToString()), 0, BalanceRow.Item("H211").ToString())
                        objBalance.H212 = IIf(String.IsNullOrEmpty(BalanceRow.Item("H212").ToString()), 0, BalanceRow.Item("H212").ToString())
                        objBalance.H301 = IIf(String.IsNullOrEmpty(BalanceRow.Item("H301").ToString()), 0, BalanceRow.Item("H301").ToString())
                        objBalance.H302 = IIf(String.IsNullOrEmpty(BalanceRow.Item("H302").ToString()), 0, BalanceRow.Item("H302").ToString())
                        objBalance.H303 = IIf(String.IsNullOrEmpty(BalanceRow.Item("H303").ToString()), 0, BalanceRow.Item("H303").ToString())
                        objBalance.H304 = IIf(String.IsNullOrEmpty(BalanceRow.Item("H304").ToString()), 0, BalanceRow.Item("H304").ToString())
                        objBalance.H305 = IIf(String.IsNullOrEmpty(BalanceRow.Item("H305").ToString()), 0, BalanceRow.Item("H305").ToString())
                        objBalance.H306 = IIf(String.IsNullOrEmpty(BalanceRow.Item("H306").ToString()), 0, BalanceRow.Item("H306").ToString())
                        objBalance.H307 = IIf(String.IsNullOrEmpty(BalanceRow.Item("H307").ToString()), 0, BalanceRow.Item("H307").ToString())
                        objBalance.H308 = IIf(String.IsNullOrEmpty(BalanceRow.Item("H308").ToString()), 0, BalanceRow.Item("H308").ToString())
                        objBalance.H309 = IIf(String.IsNullOrEmpty(BalanceRow.Item("H309").ToString()), 0, BalanceRow.Item("H309").ToString())
                        objBalance.H310 = IIf(String.IsNullOrEmpty(BalanceRow.Item("H310").ToString()), 0, BalanceRow.Item("H310").ToString())
                        objBalance.H311 = IIf(String.IsNullOrEmpty(BalanceRow.Item("H311").ToString()), 0, BalanceRow.Item("H311").ToString())
                        objBalance.H312 = IIf(String.IsNullOrEmpty(BalanceRow.Item("H312").ToString()), 0, BalanceRow.Item("H312").ToString())
                        objBalance.H401 = IIf(String.IsNullOrEmpty(BalanceRow.Item("H401").ToString()), 0, BalanceRow.Item("H401").ToString())
                        objBalance.H402 = IIf(String.IsNullOrEmpty(BalanceRow.Item("H402").ToString()), 0, BalanceRow.Item("H402").ToString())
                        objBalance.H403 = IIf(String.IsNullOrEmpty(BalanceRow.Item("H403").ToString()), 0, BalanceRow.Item("H403").ToString())
                        objBalance.H404 = IIf(String.IsNullOrEmpty(BalanceRow.Item("H404").ToString()), 0, BalanceRow.Item("H404").ToString())
                        objBalance.H405 = IIf(String.IsNullOrEmpty(BalanceRow.Item("H405").ToString()), 0, BalanceRow.Item("H405").ToString())
                        objBalance.H406 = IIf(String.IsNullOrEmpty(BalanceRow.Item("H406").ToString()), 0, BalanceRow.Item("H406").ToString())
                        objBalance.H407 = IIf(String.IsNullOrEmpty(BalanceRow.Item("H407").ToString()), 0, BalanceRow.Item("H407").ToString())
                        objBalance.H408 = IIf(String.IsNullOrEmpty(BalanceRow.Item("H408").ToString()), 0, BalanceRow.Item("H408").ToString())
                        objBalance.H409 = IIf(String.IsNullOrEmpty(BalanceRow.Item("H409").ToString()), 0, BalanceRow.Item("H409").ToString())
                        objBalance.H410 = IIf(String.IsNullOrEmpty(BalanceRow.Item("H410").ToString()), 0, BalanceRow.Item("H410").ToString())
                        objBalance.H411 = IIf(String.IsNullOrEmpty(BalanceRow.Item("H411").ToString()), 0, BalanceRow.Item("H411").ToString())
                        objBalance.H412 = IIf(String.IsNullOrEmpty(BalanceRow.Item("H412").ToString()), 0, BalanceRow.Item("H412").ToString())
                        objBalance.H501 = IIf(String.IsNullOrEmpty(BalanceRow.Item("H501").ToString()), 0, BalanceRow.Item("H501").ToString())
                        objBalance.H502 = IIf(String.IsNullOrEmpty(BalanceRow.Item("H502").ToString()), 0, BalanceRow.Item("H502").ToString())
                        objBalance.H503 = IIf(String.IsNullOrEmpty(BalanceRow.Item("H503").ToString()), 0, BalanceRow.Item("H503").ToString())
                        objBalance.H504 = IIf(String.IsNullOrEmpty(BalanceRow.Item("H504").ToString()), 0, BalanceRow.Item("H504").ToString())
                        objBalance.H505 = IIf(String.IsNullOrEmpty(BalanceRow.Item("H505").ToString()), 0, BalanceRow.Item("H505").ToString())
                        objBalance.H506 = IIf(String.IsNullOrEmpty(BalanceRow.Item("H506").ToString()), 0, BalanceRow.Item("H506").ToString())
                        objBalance.H507 = IIf(String.IsNullOrEmpty(BalanceRow.Item("H507").ToString()), 0, BalanceRow.Item("H507").ToString())
                        objBalance.H508 = IIf(String.IsNullOrEmpty(BalanceRow.Item("H508").ToString()), 0, BalanceRow.Item("H508").ToString())
                        objBalance.H509 = IIf(String.IsNullOrEmpty(BalanceRow.Item("H509").ToString()), 0, BalanceRow.Item("H509").ToString())
                        objBalance.H510 = IIf(String.IsNullOrEmpty(BalanceRow.Item("H510").ToString()), 0, BalanceRow.Item("H510").ToString())
                        objBalance.H511 = IIf(String.IsNullOrEmpty(BalanceRow.Item("H511").ToString()), 0, BalanceRow.Item("H511").ToString())
                        objBalance.H512 = IIf(String.IsNullOrEmpty(BalanceRow.Item("H512").ToString()), 0, BalanceRow.Item("H512").ToString())
                        objBalance.B101 = IIf(String.IsNullOrEmpty(BalanceRow.Item("B101").ToString()), 0, BalanceRow.Item("B101").ToString())
                        objBalance.B102 = IIf(String.IsNullOrEmpty(BalanceRow.Item("B102").ToString()), 0, BalanceRow.Item("B102").ToString())
                        objBalance.B103 = IIf(String.IsNullOrEmpty(BalanceRow.Item("B103").ToString()), 0, BalanceRow.Item("B103").ToString())
                        objBalance.B104 = IIf(String.IsNullOrEmpty(BalanceRow.Item("B104").ToString()), 0, BalanceRow.Item("B104").ToString())
                        objBalance.B105 = IIf(String.IsNullOrEmpty(BalanceRow.Item("B105").ToString()), 0, BalanceRow.Item("B105").ToString())
                        objBalance.B106 = IIf(String.IsNullOrEmpty(BalanceRow.Item("B106").ToString()), 0, BalanceRow.Item("B106").ToString())
                        objBalance.B107 = IIf(String.IsNullOrEmpty(BalanceRow.Item("B107").ToString()), 0, BalanceRow.Item("B107").ToString())
                        objBalance.B108 = IIf(String.IsNullOrEmpty(BalanceRow.Item("B108").ToString()), 0, BalanceRow.Item("B108").ToString())
                        objBalance.B109 = IIf(String.IsNullOrEmpty(BalanceRow.Item("B109").ToString()), 0, BalanceRow.Item("B109").ToString())
                        objBalance.B110 = IIf(String.IsNullOrEmpty(BalanceRow.Item("B110").ToString()), 0, BalanceRow.Item("B110").ToString())
                        objBalance.B111 = IIf(String.IsNullOrEmpty(BalanceRow.Item("B111").ToString()), 0, BalanceRow.Item("B111").ToString())
                        objBalance.B112 = IIf(String.IsNullOrEmpty(BalanceRow.Item("B112").ToString()), 0, BalanceRow.Item("B112").ToString())
                        objBalance.A101 = IIf(String.IsNullOrEmpty(BalanceRow.Item("A101").ToString()), 0, BalanceRow.Item("A101").ToString())
                        objBalance.A102 = IIf(String.IsNullOrEmpty(BalanceRow.Item("A102").ToString()), 0, BalanceRow.Item("A102").ToString())
                        objBalance.A103 = IIf(String.IsNullOrEmpty(BalanceRow.Item("A103").ToString()), 0, BalanceRow.Item("A103").ToString())
                        objBalance.A104 = IIf(String.IsNullOrEmpty(BalanceRow.Item("A104").ToString()), 0, BalanceRow.Item("A104").ToString())
                        objBalance.A105 = IIf(String.IsNullOrEmpty(BalanceRow.Item("A105").ToString()), 0, BalanceRow.Item("A105").ToString())
                        objBalance.A106 = IIf(String.IsNullOrEmpty(BalanceRow.Item("A106").ToString()), 0, BalanceRow.Item("A106").ToString())
                        objBalance.A107 = IIf(String.IsNullOrEmpty(BalanceRow.Item("A107").ToString()), 0, BalanceRow.Item("A107").ToString())
                        objBalance.A108 = IIf(String.IsNullOrEmpty(BalanceRow.Item("A108").ToString()), 0, BalanceRow.Item("A108").ToString())
                        objBalance.A109 = IIf(String.IsNullOrEmpty(BalanceRow.Item("A109").ToString()), 0, BalanceRow.Item("A109").ToString())
                        objBalance.A110 = IIf(String.IsNullOrEmpty(BalanceRow.Item("A110").ToString()), 0, BalanceRow.Item("A110").ToString())
                        objBalance.A111 = IIf(String.IsNullOrEmpty(BalanceRow.Item("A111").ToString()), 0, BalanceRow.Item("A111").ToString())
                        objBalance.A112 = IIf(String.IsNullOrEmpty(BalanceRow.Item("A112").ToString()), 0, BalanceRow.Item("A112").ToString())
                        objBalance.AccountId = BalanceRow.Item("AccountId")

                        objListBalance.Add(objBalance)
                    Catch ex As Exception
                        Dim newRow As DataRow = GlobalSettings.RequestErrorBalance.NewRow()
                        newRow("AccountId") = BalanceRow.Item("AccountId").ToString()
                        newRow("BalanceId") = BalanceRow.Item("BalanceId").ToString()
                        newRow("H101") = IIf(String.IsNullOrEmpty(BalanceRow.Item("H101").ToString()), 0, BalanceRow.Item("H101").ToString())
                        newRow("H102") = IIf(String.IsNullOrEmpty(BalanceRow.Item("H102").ToString()), 0, BalanceRow.Item("H102").ToString())
                        newRow("H103") = IIf(String.IsNullOrEmpty(BalanceRow.Item("H103").ToString()), 0, BalanceRow.Item("H103").ToString())
                        newRow("H104") = IIf(String.IsNullOrEmpty(BalanceRow.Item("H104").ToString()), 0, BalanceRow.Item("H104").ToString())
                        newRow("H105") = IIf(String.IsNullOrEmpty(BalanceRow.Item("H105").ToString()), 0, BalanceRow.Item("H105").ToString())
                        newRow("H106") = IIf(String.IsNullOrEmpty(BalanceRow.Item("H106").ToString()), 0, BalanceRow.Item("H106").ToString())
                        newRow("H107") = IIf(String.IsNullOrEmpty(BalanceRow.Item("H107").ToString()), 0, BalanceRow.Item("H107").ToString())
                        newRow("H108") = IIf(String.IsNullOrEmpty(BalanceRow.Item("H108").ToString()), 0, BalanceRow.Item("H108").ToString())
                        newRow("H109") = IIf(String.IsNullOrEmpty(BalanceRow.Item("H109").ToString()), 0, BalanceRow.Item("H109").ToString())
                        newRow("H110") = IIf(String.IsNullOrEmpty(BalanceRow.Item("H110").ToString()), 0, BalanceRow.Item("H110").ToString())
                        newRow("H111") = IIf(String.IsNullOrEmpty(BalanceRow.Item("H111").ToString()), 0, BalanceRow.Item("H111").ToString())
                        newRow("H112") = IIf(String.IsNullOrEmpty(BalanceRow.Item("H112").ToString()), 0, BalanceRow.Item("H112").ToString())
                        newRow("H201") = IIf(String.IsNullOrEmpty(BalanceRow.Item("H201").ToString()), 0, BalanceRow.Item("H201").ToString())
                        newRow("H202") = IIf(String.IsNullOrEmpty(BalanceRow.Item("H202").ToString()), 0, BalanceRow.Item("H202").ToString())
                        newRow("H203") = IIf(String.IsNullOrEmpty(BalanceRow.Item("H203").ToString()), 0, BalanceRow.Item("H203").ToString())
                        newRow("H204") = IIf(String.IsNullOrEmpty(BalanceRow.Item("H204").ToString()), 0, BalanceRow.Item("H204").ToString())
                        newRow("H205") = IIf(String.IsNullOrEmpty(BalanceRow.Item("H205").ToString()), 0, BalanceRow.Item("H205").ToString())
                        newRow("H206") = IIf(String.IsNullOrEmpty(BalanceRow.Item("H206").ToString()), 0, BalanceRow.Item("H206").ToString())
                        newRow("H207") = IIf(String.IsNullOrEmpty(BalanceRow.Item("H207").ToString()), 0, BalanceRow.Item("H207").ToString())
                        newRow("H208") = IIf(String.IsNullOrEmpty(BalanceRow.Item("H208").ToString()), 0, BalanceRow.Item("H208").ToString())
                        newRow("H209") = IIf(String.IsNullOrEmpty(BalanceRow.Item("H209").ToString()), 0, BalanceRow.Item("H209").ToString())
                        newRow("H210") = IIf(String.IsNullOrEmpty(BalanceRow.Item("H210").ToString()), 0, BalanceRow.Item("H210").ToString())
                        newRow("H211") = IIf(String.IsNullOrEmpty(BalanceRow.Item("H211").ToString()), 0, BalanceRow.Item("H211").ToString())
                        newRow("H212") = IIf(String.IsNullOrEmpty(BalanceRow.Item("H212").ToString()), 0, BalanceRow.Item("H212").ToString())
                        newRow("H301") = IIf(String.IsNullOrEmpty(BalanceRow.Item("H301").ToString()), 0, BalanceRow.Item("H301").ToString())
                        newRow("H302") = IIf(String.IsNullOrEmpty(BalanceRow.Item("H302").ToString()), 0, BalanceRow.Item("H302").ToString())
                        newRow("H303") = IIf(String.IsNullOrEmpty(BalanceRow.Item("H303").ToString()), 0, BalanceRow.Item("H303").ToString())
                        newRow("H304") = IIf(String.IsNullOrEmpty(BalanceRow.Item("H304").ToString()), 0, BalanceRow.Item("H304").ToString())
                        newRow("H305") = IIf(String.IsNullOrEmpty(BalanceRow.Item("H305").ToString()), 0, BalanceRow.Item("H305").ToString())
                        newRow("H306") = IIf(String.IsNullOrEmpty(BalanceRow.Item("H306").ToString()), 0, BalanceRow.Item("H306").ToString())
                        newRow("H307") = IIf(String.IsNullOrEmpty(BalanceRow.Item("H307").ToString()), 0, BalanceRow.Item("H307").ToString())
                        newRow("H308") = IIf(String.IsNullOrEmpty(BalanceRow.Item("H308").ToString()), 0, BalanceRow.Item("H308").ToString())
                        newRow("H309") = IIf(String.IsNullOrEmpty(BalanceRow.Item("H309").ToString()), 0, BalanceRow.Item("H309").ToString())
                        newRow("H310") = IIf(String.IsNullOrEmpty(BalanceRow.Item("H310").ToString()), 0, BalanceRow.Item("H310").ToString())
                        newRow("H311") = IIf(String.IsNullOrEmpty(BalanceRow.Item("H311").ToString()), 0, BalanceRow.Item("H311").ToString())
                        newRow("H312") = IIf(String.IsNullOrEmpty(BalanceRow.Item("H312").ToString()), 0, BalanceRow.Item("H312").ToString())
                        newRow("H401") = IIf(String.IsNullOrEmpty(BalanceRow.Item("H401").ToString()), 0, BalanceRow.Item("H401").ToString())
                        newRow("H402") = IIf(String.IsNullOrEmpty(BalanceRow.Item("H402").ToString()), 0, BalanceRow.Item("H402").ToString())
                        newRow("H403") = IIf(String.IsNullOrEmpty(BalanceRow.Item("H403").ToString()), 0, BalanceRow.Item("H403").ToString())
                        newRow("H404") = IIf(String.IsNullOrEmpty(BalanceRow.Item("H404").ToString()), 0, BalanceRow.Item("H404").ToString())
                        newRow("H405") = IIf(String.IsNullOrEmpty(BalanceRow.Item("H405").ToString()), 0, BalanceRow.Item("H405").ToString())
                        newRow("H406") = IIf(String.IsNullOrEmpty(BalanceRow.Item("H406").ToString()), 0, BalanceRow.Item("H406").ToString())
                        newRow("H407") = IIf(String.IsNullOrEmpty(BalanceRow.Item("H407").ToString()), 0, BalanceRow.Item("H407").ToString())
                        newRow("H408") = IIf(String.IsNullOrEmpty(BalanceRow.Item("H408").ToString()), 0, BalanceRow.Item("H408").ToString())
                        newRow("H409") = IIf(String.IsNullOrEmpty(BalanceRow.Item("H409").ToString()), 0, BalanceRow.Item("H409").ToString())
                        newRow("H410") = IIf(String.IsNullOrEmpty(BalanceRow.Item("H410").ToString()), 0, BalanceRow.Item("H410").ToString())
                        newRow("H411") = IIf(String.IsNullOrEmpty(BalanceRow.Item("H411").ToString()), 0, BalanceRow.Item("H411").ToString())
                        newRow("H412") = IIf(String.IsNullOrEmpty(BalanceRow.Item("H412").ToString()), 0, BalanceRow.Item("H412").ToString())
                        newRow("H501") = IIf(String.IsNullOrEmpty(BalanceRow.Item("H501").ToString()), 0, BalanceRow.Item("H501").ToString())
                        newRow("H502") = IIf(String.IsNullOrEmpty(BalanceRow.Item("H502").ToString()), 0, BalanceRow.Item("H502").ToString())
                        newRow("H503") = IIf(String.IsNullOrEmpty(BalanceRow.Item("H503").ToString()), 0, BalanceRow.Item("H503").ToString())
                        newRow("H504") = IIf(String.IsNullOrEmpty(BalanceRow.Item("H504").ToString()), 0, BalanceRow.Item("H504").ToString())
                        newRow("H505") = IIf(String.IsNullOrEmpty(BalanceRow.Item("H505").ToString()), 0, BalanceRow.Item("H505").ToString())
                        newRow("H506") = IIf(String.IsNullOrEmpty(BalanceRow.Item("H506").ToString()), 0, BalanceRow.Item("H506").ToString())
                        newRow("H507") = IIf(String.IsNullOrEmpty(BalanceRow.Item("H507").ToString()), 0, BalanceRow.Item("H507").ToString())
                        newRow("H508") = IIf(String.IsNullOrEmpty(BalanceRow.Item("H508").ToString()), 0, BalanceRow.Item("H508").ToString())
                        newRow("H509") = IIf(String.IsNullOrEmpty(BalanceRow.Item("H509").ToString()), 0, BalanceRow.Item("H509").ToString())
                        newRow("H510") = IIf(String.IsNullOrEmpty(BalanceRow.Item("H510").ToString()), 0, BalanceRow.Item("H510").ToString())
                        newRow("H511") = IIf(String.IsNullOrEmpty(BalanceRow.Item("H511").ToString()), 0, BalanceRow.Item("H511").ToString())
                        newRow("H512") = IIf(String.IsNullOrEmpty(BalanceRow.Item("H512").ToString()), 0, BalanceRow.Item("H512").ToString())
                        newRow("B101") = IIf(String.IsNullOrEmpty(BalanceRow.Item("B101").ToString()), 0, BalanceRow.Item("B101").ToString())
                        newRow("B102") = IIf(String.IsNullOrEmpty(BalanceRow.Item("B102").ToString()), 0, BalanceRow.Item("B102").ToString())
                        newRow("B103") = IIf(String.IsNullOrEmpty(BalanceRow.Item("B103").ToString()), 0, BalanceRow.Item("B103").ToString())
                        newRow("B104") = IIf(String.IsNullOrEmpty(BalanceRow.Item("B104").ToString()), 0, BalanceRow.Item("B104").ToString())
                        newRow("B105") = IIf(String.IsNullOrEmpty(BalanceRow.Item("B105").ToString()), 0, BalanceRow.Item("B105").ToString())
                        newRow("B106") = IIf(String.IsNullOrEmpty(BalanceRow.Item("B106").ToString()), 0, BalanceRow.Item("B106").ToString())
                        newRow("B107") = IIf(String.IsNullOrEmpty(BalanceRow.Item("B107").ToString()), 0, BalanceRow.Item("B107").ToString())
                        newRow("B108") = IIf(String.IsNullOrEmpty(BalanceRow.Item("B108").ToString()), 0, BalanceRow.Item("B108").ToString())
                        newRow("B109") = IIf(String.IsNullOrEmpty(BalanceRow.Item("B109").ToString()), 0, BalanceRow.Item("B109").ToString())
                        newRow("B110") = IIf(String.IsNullOrEmpty(BalanceRow.Item("B110").ToString()), 0, BalanceRow.Item("B110").ToString())
                        newRow("B111") = IIf(String.IsNullOrEmpty(BalanceRow.Item("B111").ToString()), 0, BalanceRow.Item("B111").ToString())
                        newRow("B112") = IIf(String.IsNullOrEmpty(BalanceRow.Item("B112").ToString()), 0, BalanceRow.Item("B112").ToString())
                        newRow("A101") = IIf(String.IsNullOrEmpty(BalanceRow.Item("A101").ToString()), 0, BalanceRow.Item("A101").ToString())
                        newRow("A102") = IIf(String.IsNullOrEmpty(BalanceRow.Item("A102").ToString()), 0, BalanceRow.Item("A102").ToString())
                        newRow("A103") = IIf(String.IsNullOrEmpty(BalanceRow.Item("A103").ToString()), 0, BalanceRow.Item("A103").ToString())
                        newRow("A104") = IIf(String.IsNullOrEmpty(BalanceRow.Item("A104").ToString()), 0, BalanceRow.Item("A104").ToString())
                        newRow("A105") = IIf(String.IsNullOrEmpty(BalanceRow.Item("A105").ToString()), 0, BalanceRow.Item("A105").ToString())
                        newRow("A106") = IIf(String.IsNullOrEmpty(BalanceRow.Item("A106").ToString()), 0, BalanceRow.Item("A106").ToString())
                        newRow("A107") = IIf(String.IsNullOrEmpty(BalanceRow.Item("A107").ToString()), 0, BalanceRow.Item("A107").ToString())
                        newRow("A108") = IIf(String.IsNullOrEmpty(BalanceRow.Item("A108").ToString()), 0, BalanceRow.Item("A108").ToString())
                        newRow("A109") = IIf(String.IsNullOrEmpty(BalanceRow.Item("A109").ToString()), 0, BalanceRow.Item("A109").ToString())
                        newRow("A110") = IIf(String.IsNullOrEmpty(BalanceRow.Item("A110").ToString()), 0, BalanceRow.Item("A110").ToString())
                        newRow("A111") = IIf(String.IsNullOrEmpty(BalanceRow.Item("A111").ToString()), 0, BalanceRow.Item("A111").ToString())
                        newRow("A112") = IIf(String.IsNullOrEmpty(BalanceRow.Item("A112").ToString()), 0, BalanceRow.Item("A112").ToString())

                        GlobalSettings.RequestErrorBalance.Rows.Add(newRow)
                    End Try
                Next
            End If
            Common.LogInfoWriter("ConvertBalanceToBalanceEntity in UploadBL execution end")
        Catch ex As Exception
            Common.LogErrorWriter("ConvertBalanceToBalanceEntity in UploadBL execution end with Error Message: " & ex.Message & ex.StackTrace)
        End Try
       
        Return objListBalance
    End Function

    ''' <summary>
    ''' This method will Convert Response Object into DataSet
    ''' </summary>
    ''' <param name="objResponseContract">Response Contract as Object</param>
    ''' <returns>DataSet of Response Object</returns>
    ''' <remarks></remarks>
    Public Function ConvertResponseToDataSet(ByVal objResponseContract As FileUploadResponseContract) As DataSet

        Dim returnDataSet As DataSet = New DataSet()
        Try
            Common.LogInfoWriter("ConvertResponseToDataSet in UploadBL execution start")
            returnDataSet.Tables.Add(ConvertAcctTypeEntityToDataTable(objResponseContract))
            returnDataSet.Tables.Add(ConvertAccountEntityToDataTable(objResponseContract))
            returnDataSet.Tables.Add(ConvertBalanceEntityToDataTable(objResponseContract))
            Common.LogInfoWriter("ConvertResponseToDataSet in UploadBL execution end")
        Catch ex As Exception
            Common.LogErrorWriter("ConvertResponseToDataSet in UploadBL execution end with Error Message: " & ex.Message & ex.StackTrace)
        End Try
        Return returnDataSet
    End Function

    ''' <summary>
    ''' This Method will convert AcctType Contract into AcctType DataTable
    ''' </summary>
    ''' <param name="objResponseContract">Response Contract as Object</param>
    ''' <returns>DataTable of Account Data</returns>
    ''' <remarks></remarks>
    Private Function ConvertAcctTypeEntityToDataTable(ByVal objResponseContract As FileUploadResponseContract) As DataTable
        Dim dtAcctType As DataTable = CreateAcctTypeDataTable()
        Try
            Common.LogInfoWriter("ConvertAcctTypeEntityToDataTable in UploadBL execution start")
            For Each objAcctType In objResponseContract.ErrorMessageAcctType
                Dim newRow As DataRow = dtAcctType.NewRow()
                newRow("AcctTypeId") = objAcctType.AcctTypeId.ToString()
                newRow("TypeDesc") = objAcctType.TypeDesc.ToString()
                newRow("ClassDesc") = objAcctType.ClassDesc.ToString()
                newRow("SubclassDesc") = objAcctType.SubclassDesc.ToString()
                dtAcctType.Rows.Add(newRow)
            Next
            Common.LogInfoWriter("ConvertAcctTypeEntityToDataTable in UploadBL execution end")
        Catch ex As Exception
            Common.LogErrorWriter("ConvertAcctTypeEntityToDataTable in UploadBL execution end with Error Message: " & ex.Message & ex.StackTrace)
        End Try
        
        Return dtAcctType
    End Function

    ''' <summary>
    ''' This Method will convert Account Contract into Account DataTable
    ''' </summary>
    ''' <param name="objResponseContract">Response Contract as Object</param>
    ''' <returns>DataTable of Account Data</returns>
    ''' <remarks></remarks>
    Private Function ConvertAccountEntityToDataTable(ByVal objResponseContract As FileUploadResponseContract) As DataTable
        Dim dtAccount As DataTable = CreateAccountDataTable()
        Try
            Common.LogInfoWriter("ConvertAccountEntityToDataTable in UploadBL execution start")
            For Each objAccount In objResponseContract.ErrorMessageAccount
                Dim newRow As DataRow = dtAccount.NewRow()

                newRow("AccountId") = objAccount.AccountId.ToString()
                newRow("AcctDescriptor") = objAccount.AcctDescriptor.ToString()
                newRow("SortSequence") = objAccount.SortSequence.ToString()
                newRow("Description") = objAccount.Description.ToString()
                newRow("Subgrouping") = objAccount.SubGrouping.ToString()
                newRow("AcctTypeId") = objAccount.AcctTypeId.ToString()
                newRow("NumberFormat") = objAccount.NumberFormat.ToString()
                newRow("TotalType") = objAccount.TotalType.ToString()
                dtAccount.Rows.Add(newRow)
            Next
            Common.LogInfoWriter("ConvertAccountEntityToDataTable in UploadBL execution end")
        Catch ex As Exception
            Common.LogErrorWriter("ConvertAccountEntityToDataTable in UploadBL execution end with Error Message: " & ex.Message & ex.StackTrace)
        End Try
        Return dtAccount
    End Function

    ''' <summary>
    ''' This Method will convert Balance Conctract into Balance DataTable
    ''' </summary>
    ''' <param name="objResponseContract">Response Contract as Object</param>
    ''' <returns>DataTable of Balance Data</returns>
    ''' <remarks></remarks>
    Private Function ConvertBalanceEntityToDataTable(ByVal objResponseContract As FileUploadResponseContract) As DataTable

        Dim dtBalance As DataTable = CreateBalanceDataTable()
        Try
            Common.LogInfoWriter("ConvertBalanceEntityToDataTable in UploadBL execution start")
            For Each objBalance In objResponseContract.ErrorMessageBalance
                Dim newRow As DataRow = dtBalance.NewRow()

                newRow("AccountId") = objBalance.AccountId.ToString()
                newRow("BalanceId") = objBalance.BalanceId.ToString()                        
                newRow("H101") = objBalance.H101.ToString()
                newRow("H102") = objBalance.H102.ToString()
                newRow("H103") = objBalance.H103.ToString()
                newRow("H104") = objBalance.H104.ToString()
                newRow("H105") = objBalance.H105.ToString()
                newRow("H106") = objBalance.H106.ToString()
                newRow("H107") = objBalance.H107.ToString()
                newRow("H108") = objBalance.H108.ToString()
                newRow("H109") = objBalance.H109.ToString()
                newRow("H110") = objBalance.H110.ToString()
                newRow("H111") = objBalance.H111.ToString()
                newRow("H112") = objBalance.H112.ToString()
                newRow("H201") = objBalance.H201.ToString()
                newRow("H202") = objBalance.H202.ToString()
                newRow("H203") = objBalance.H203.ToString()
                newRow("H204") = objBalance.H204.ToString()
                newRow("H205") = objBalance.H205.ToString()
                newRow("H206") = objBalance.H206.ToString()
                newRow("H207") = objBalance.H207.ToString()
                newRow("H208") = objBalance.H208.ToString()
                newRow("H209") = objBalance.H209.ToString()
                newRow("H210") = objBalance.H210.ToString()
                newRow("H211") = objBalance.H211.ToString()
                newRow("H212") = objBalance.H212.ToString()
                newRow("H301") = objBalance.H301.ToString()
                newRow("H302") = objBalance.H302.ToString()
                newRow("H303") = objBalance.H303.ToString()
                newRow("H304") = objBalance.H304.ToString()
                newRow("H305") = objBalance.H305.ToString()
                newRow("H306") = objBalance.H306.ToString()
                newRow("H307") = objBalance.H307.ToString()
                newRow("H308") = objBalance.H308.ToString()
                newRow("H309") = objBalance.H309.ToString()
                newRow("H310") = objBalance.H310.ToString()
                newRow("H311") = objBalance.H311.ToString()
                newRow("H312") = objBalance.H312.ToString()
                newRow("H401") = objBalance.H401.ToString()
                newRow("H402") = objBalance.H402.ToString()
                newRow("H403") = objBalance.H403.ToString()
                newRow("H404") = objBalance.H404.ToString()
                newRow("H405") = objBalance.H405.ToString()
                newRow("H406") = objBalance.H406.ToString()
                newRow("H407") = objBalance.H407.ToString()
                newRow("H408") = objBalance.H408.ToString()
                newRow("H409") = objBalance.H409.ToString()
                newRow("H410") = objBalance.H410.ToString()
                newRow("H411") = objBalance.H411.ToString()
                newRow("H412") = objBalance.H412.ToString()
                newRow("H501") = objBalance.H501.ToString()
                newRow("H502") = objBalance.H502.ToString()
                newRow("H503") = objBalance.H503.ToString()
                newRow("H504") = objBalance.H504.ToString()
                newRow("H505") = objBalance.H505.ToString()
                newRow("H506") = objBalance.H506.ToString()
                newRow("H507") = objBalance.H507.ToString()
                newRow("H508") = objBalance.H508.ToString()
                newRow("H509") = objBalance.H509.ToString()
                newRow("H510") = objBalance.H510.ToString()
                newRow("H511") = objBalance.H511.ToString()
                newRow("H512") = objBalance.H512.ToString()
                newRow("B101") = objBalance.B101.ToString()
                newRow("B102") = objBalance.B102.ToString()
                newRow("B103") = objBalance.B103.ToString()
                newRow("B104") = objBalance.B104.ToString()
                newRow("B105") = objBalance.B105.ToString()
                newRow("B106") = objBalance.B106.ToString()
                newRow("B107") = objBalance.B107.ToString()
                newRow("B108") = objBalance.B108.ToString()
                newRow("B109") = objBalance.B109.ToString()
                newRow("B110") = objBalance.B110.ToString()
                newRow("B111") = objBalance.B111.ToString()
                newRow("B112") = objBalance.B112.ToString()
                newRow("A101") = objBalance.A101.ToString()
                newRow("A102") = objBalance.A102.ToString()
                newRow("A103") = objBalance.A103.ToString()
                newRow("A104") = objBalance.A104.ToString()
                newRow("A105") = objBalance.A105.ToString()
                newRow("A106") = objBalance.A106.ToString()
                newRow("A107") = objBalance.A107.ToString()
                newRow("A108") = objBalance.A108.ToString()
                newRow("A109") = objBalance.A109.ToString()
                newRow("A110") = objBalance.A110.ToString()
                newRow("A111") = objBalance.A111.ToString()
                newRow("A112") = objBalance.A112.ToString()                
                dtBalance.Rows.Add(newRow)
            Next
            Common.LogInfoWriter("ConvertBalanceEntityToDataTable in UploadBL execution end")
        Catch ex As Exception
            Common.LogErrorWriter("ConvertBalanceEntityToDataTable in UploadBL execution end with Error Message: " & ex.Message & ex.StackTrace)
        End Try
        
        Return dtBalance
    End Function

    ''' <summary>
    ''' To create DataTable for AcctType Table with Pre-defined Columns
    ''' </summary>
    ''' <returns>AcctType DataTable</returns>
    ''' <remarks></remarks>
    Private Function CreateAcctTypeDataTable() As DataTable
        Dim dtAcctType As DataTable = New DataTable()
        Try
            Common.LogInfoWriter("CreateAcctTypeDataTable in UploadBL execution start")
            dtAcctType.TableName = "AcctType"
            dtAcctType.Columns.Add("AcctTypeId")
            dtAcctType.Columns.Add("TypeDesc")
            dtAcctType.Columns.Add("ClassDesc")
            dtAcctType.Columns.Add("SubclassDesc")
            Common.LogInfoWriter("CreateAcctTypeDataTable in UploadBL execution end")
        Catch ex As Exception
            Common.LogErrorWriter("CreateAcctTypeDataTable in UploadBL execution end with Error Message: " & ex.Message & ex.StackTrace)
        End Try
        Return dtAcctType
    End Function

    ''' <summary>
    ''' To create DataTable for Account Table with Pre-defined Columns
    ''' </summary>
    ''' <returns>Account DataTable</returns>
    ''' <remarks></remarks>
    Private Function CreateAccountDataTable() As DataTable
        Dim dtAccount As DataTable = New DataTable()
        Try
            Common.LogInfoWriter("CreateAccountDataTable in UploadBL execution start")
            dtAccount.TableName = "Account"
            dtAccount.Columns.Add("AccountId")
            dtAccount.Columns.Add("AcctDescriptor")
            dtAccount.Columns.Add("SortSequence")
            dtAccount.Columns.Add("Description")
            dtAccount.Columns.Add("Subgrouping")
            dtAccount.Columns.Add("AcctTypeId")
            dtAccount.Columns.Add("NumberFormat")
            dtAccount.Columns.Add("TotalType")
            Common.LogInfoWriter("CreateAccountDataTable in UploadBL execution end")
        Catch ex As Exception
            Common.LogErrorWriter("CreateAccountDataTable in UploadBL execution end with Error Message: " & ex.Message & ex.StackTrace)
        End Try
        
        Return dtAccount
    End Function

    ''' <summary>
    ''' To create DataTable for Balance Table with Pre-defined Columns
    ''' </summary>
    ''' <returns>Balance DataTable</returns>
    ''' <remarks></remarks>
    Private Function CreateBalanceDataTable() As DataTable
        Dim dtBalance As DataTable = New DataTable()
        Try
            Common.LogInfoWriter("CreateBalanceDataTable in UploadBL execution start")
            dtBalance.TableName = "Balance"
            dtBalance.Columns.Add("BalanceId")
            dtBalance.Columns.Add("H101")
            dtBalance.Columns.Add("H102")
            dtBalance.Columns.Add("H103")
            dtBalance.Columns.Add("H104")
            dtBalance.Columns.Add("H105")
            dtBalance.Columns.Add("H106")
            dtBalance.Columns.Add("H107")
            dtBalance.Columns.Add("H108")
            dtBalance.Columns.Add("H109")
            dtBalance.Columns.Add("H110")
            dtBalance.Columns.Add("H111")
            dtBalance.Columns.Add("H112")
            dtBalance.Columns.Add("H201")
            dtBalance.Columns.Add("H202")
            dtBalance.Columns.Add("H203")
            dtBalance.Columns.Add("H204")
            dtBalance.Columns.Add("H205")
            dtBalance.Columns.Add("H206")
            dtBalance.Columns.Add("H207")
            dtBalance.Columns.Add("H208")
            dtBalance.Columns.Add("H209")
            dtBalance.Columns.Add("H210")
            dtBalance.Columns.Add("H211")
            dtBalance.Columns.Add("H212")
            dtBalance.Columns.Add("H301")
            dtBalance.Columns.Add("H302")
            dtBalance.Columns.Add("H303")
            dtBalance.Columns.Add("H304")
            dtBalance.Columns.Add("H305")
            dtBalance.Columns.Add("H306")
            dtBalance.Columns.Add("H307")
            dtBalance.Columns.Add("H308")
            dtBalance.Columns.Add("H309")
            dtBalance.Columns.Add("H310")
            dtBalance.Columns.Add("H311")
            dtBalance.Columns.Add("H312")
            dtBalance.Columns.Add("H401")
            dtBalance.Columns.Add("H402")
            dtBalance.Columns.Add("H403")
            dtBalance.Columns.Add("H404")
            dtBalance.Columns.Add("H405")
            dtBalance.Columns.Add("H406")
            dtBalance.Columns.Add("H407")
            dtBalance.Columns.Add("H408")
            dtBalance.Columns.Add("H409")
            dtBalance.Columns.Add("H410")
            dtBalance.Columns.Add("H411")
            dtBalance.Columns.Add("H412")
            dtBalance.Columns.Add("H501")
            dtBalance.Columns.Add("H502")
            dtBalance.Columns.Add("H503")
            dtBalance.Columns.Add("H504")
            dtBalance.Columns.Add("H505")
            dtBalance.Columns.Add("H506")
            dtBalance.Columns.Add("H507")
            dtBalance.Columns.Add("H508")
            dtBalance.Columns.Add("H509")
            dtBalance.Columns.Add("H510")
            dtBalance.Columns.Add("H511")
            dtBalance.Columns.Add("H512")
            dtBalance.Columns.Add("B101")
            dtBalance.Columns.Add("B102")
            dtBalance.Columns.Add("B103")
            dtBalance.Columns.Add("B104")
            dtBalance.Columns.Add("B105")
            dtBalance.Columns.Add("B106")
            dtBalance.Columns.Add("B107")
            dtBalance.Columns.Add("B108")
            dtBalance.Columns.Add("B109")
            dtBalance.Columns.Add("B110")
            dtBalance.Columns.Add("B111")
            dtBalance.Columns.Add("B112")
            dtBalance.Columns.Add("A101")
            dtBalance.Columns.Add("A102")
            dtBalance.Columns.Add("A103")
            dtBalance.Columns.Add("A104")
            dtBalance.Columns.Add("A105")
            dtBalance.Columns.Add("A106")
            dtBalance.Columns.Add("A107")
            dtBalance.Columns.Add("A108")
            dtBalance.Columns.Add("A109")
            dtBalance.Columns.Add("A110")
            dtBalance.Columns.Add("A111")
            dtBalance.Columns.Add("A112")
            dtBalance.Columns.Add("AccountId")
            Common.LogInfoWriter("CreateBalanceDataTable in UploadBL execution end")
        Catch ex As Exception
            Common.LogErrorWriter("CreateBalanceDataTable in UploadBL execution end with Error Message: " & ex.Message & ex.StackTrace)
        End Try
       
        Return dtBalance

    End Function

    ''' <summary>
    ''' Write Error Data in CSV file before uploading.
    ''' </summary>
    ''' <remarks></remarks>
    Public Function WriteRequestErrorFile() As String
        Try
            Common.LogInfoWriter("WriteRequestErrorFile in UploadBL execution start")
            Dim objExcelHelper As ExcelHelper = New ExcelHelper()
            Dim basePath As String = Common.BaseDirectoryPath & "\" & Common.ERRORFOLDER & "\"
            Dim strMessage = Common.SUCCESS
            If (GlobalSettings.RequestErrorAcctType.Rows.Count > 0) Then
                objExcelHelper.ExportDatasetToCsv(GlobalSettings.RequestErrorAcctType, basePath & "_AcctTypeRequestError" & Common.EXT_CSV)
            End If
            If (GlobalSettings.RequestErrorAccount.Rows.Count > 0) Then
                objExcelHelper.ExportDatasetToCsv(GlobalSettings.RequestErrorAccount, basePath & "_AccountRequestError" & Common.EXT_CSV)
            End If
            If (GlobalSettings.RequestErrorBalance.Rows.Count > 0) Then
                objExcelHelper.ExportDatasetToCsv(GlobalSettings.RequestErrorBalance, basePath & "_BalanceRequestError" & Common.EXT_CSV)
            End If
            Common.LogInfoWriter("WriteRequestErrorFile in UploadBL execution end")
        Catch ex As Exception
            Common.LogErrorWriter("WriteRequestErrorFile in UploadBL execution end with Error Message: " & ex.Message & ex.StackTrace)
        End Try
        Return String.Empty
    End Function

End Class
