﻿Imports System.IO
Imports System.Data
Imports System.Data.OleDb

'This Common class have Logging Methods to write Information or Error logs in respective files.
Public Class Common

    Public Const DATEFORMAT As String = "dd-MM-yyyy_HH_mm"
    Public Const LoggerDateFormat As String = "dd-MM-yyyy"
    Public Const EXT_CSV As String = ".csv"
    Public Const EXT_XLS As String = ".xls"
    Public Const EXT_XLSX As String = ".xlsx"
    Public Const UPLOADEDFILES As String = "\UploadedFiles"
    Public Const INPUTFOLDER As String = "Input"
    Public Const ERRORFOLDER As String = "Error"
    Public Const ARCHIEVEFOLDER As String = "Archive"
    Public Const MSG_INFORMATION As String = "Information Message"
    Public Const MSG_ERROR As String = "Error Message"
    Public Const SUCCESS As String = "Success"
    Public Const Logfile_DateFormat As String = "[{0:MM/dd/yyyy HH:mm:ss.fff }] {1}"
    Shared _basePath As String

    Public Shared ReadOnly Property BaseDirectoryPath() As String
        Get
            _basePath = Environment.CurrentDirectory.ToString() & UPLOADEDFILES
            Return _basePath
        End Get
    End Property
    Public Shared Property FilePath() As String
     
    ''' <summary>
    ''' 'This Method will Write Error logs into ErrorLog file.
    ''' </summary>
    ''' <param name="message">Message to Write in Logfile</param>
    ''' <remarks></remarks>
    Public Shared Sub LogErrorWriter(ByVal message As String)

        Dim FILE_NAME As String = AppDomain.CurrentDomain.BaseDirectory() & "Logs\Errors\ErrorLog" & Date.Now.Date.ToString(LoggerDateFormat) & ".log"

        If Not System.IO.Directory.Exists(AppDomain.CurrentDomain.BaseDirectory() & "Logs\Errors") Then
            System.IO.Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory() & "Logs\Errors")
        End If

        Dim sw As System.IO.StreamWriter = System.IO.File.AppendText(FILE_NAME)

        Try
            Dim logLine As String = System.String.Format(Logfile_DateFormat, DateTime.Now, message)
            sw.WriteLine(logLine)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        Finally
            sw.Close()
        End Try
    End Sub

    ''' <summary>
    ''' This Method will Write Info logs into InfoLog file.
    ''' </summary>
    ''' <param name="message">Message to write in Logfile</param>
    ''' <remarks></remarks>
    Public Shared Sub LogInfoWriter(ByVal message As String)

        Dim FILE_NAME As String = AppDomain.CurrentDomain.BaseDirectory() & "Logs\Info\InfoLog" & Date.Now.Date.ToString(LoggerDateFormat) & ".log"

        If Not System.IO.Directory.Exists(AppDomain.CurrentDomain.BaseDirectory() & "Logs\Info") Then
            System.IO.Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory() & "Logs\Info")
        End If

        Dim sw As System.IO.StreamWriter = System.IO.File.AppendText(FILE_NAME)

        Try
            Dim logLine As String = System.String.Format(Logfile_DateFormat, DateTime.Now, message)
            sw.WriteLine(logLine)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        Finally
            sw.Close()
        End Try
    End Sub

End Class
