﻿Class Application

    ' Application-level events, such as Startup, Exit, and DispatcherUnhandledException
    ' can be handled in this file.
    ' SES Added code to parse the command line argument
    Private Sub Application_Startup(ByVal sender As System.Object, ByVal e As System.Windows.StartupEventArgs)
        If (e.Args.Length > 0) Then
            Common.FilePath = e.Args.First().ToString()
            Dim iCtr As Integer = InStrRev(e.Args.First().ToString(), "\")
            Dim sLogPath As String = Mid(e.Args.First().ToString(), 1, iCtr)
            Common.LogDirectoryPath = sLogPath
            Common.BaseDirectoryPath = sLogPath & "UploadedFiles"
            Common.LogInfoWriter("Going to Upload File from the Path: " & e.Args.First().ToString())
        Else
            ' SES Added for testing
            'Common.FilePath = "C:\Users\Public\Documents\PlanGuru\Sample Co\3 Year ProjectionUpload.xls"
            'Common.LogDirectoryPath = "C:\Users\Public\Documents\PlanGuru\Sample Co\"
            'Common.BaseDirectoryPath = "C:\Users\Public\Documents\PlanGuru\Sample Co\UploadedFiles"
            'Common.LogInfoWriter("Going to Upload File from the Path: C:\Users\Public\Documents\PlanGuru\Sample Co\")
        End If
    End Sub
End Class
