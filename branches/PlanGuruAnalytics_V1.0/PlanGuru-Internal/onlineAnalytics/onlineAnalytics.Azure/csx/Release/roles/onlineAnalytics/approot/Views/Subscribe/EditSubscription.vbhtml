﻿@ModelType onlineAnalytics.Customer
@Imports onlineAnalytics
@Imports System.Collections.Generic
@Code
    ViewData("Title") = "EditSubscription"
    Layout = "~/Views/Shared/AnalyticsMaster.vbhtml"
End Code
<div class="headerTable" style="float: right">
    <table>
        <tr>
            <td align="left" width="70%">
               Edit Subscription
            </td>
        </tr>
    </table>
</div>
<br /><br />
<header>    
<link rel="stylesheet" href="@Url.Content("~/themes/default/recurly.css")" type="text/css" />
    <link rel="stylesheet" href="@Url.Content("~/themes/default/examples.css")" type="text/css" />
    <script type="text/javascript" language=javascript src="@Url.Content("~/Recurly/recurly.js")"></script>
    </header>
<script type="text/javascript" language="javascript">

    function cancelEvent() {        
            var url = '@Url.Action("Index", "Subscribe")';
            window.location = url;
    }

    $(document).ready(function () {
      
        IsSessionAlive();
        function SubscriptionResponse(response) {
            Recurly.postResult('@Url.Action("UpdateSubscription", "Subscribe")', response, null);

        }

        Recurly.config({
            subdomain: 'new-horizon-software-technologies-inc',
            currency: 'USD'
        });

        Recurly.buildBillingInfoUpdateForm({
            target: '#recurly-update-billing-info',
            // Signature must be generated server-side with a utility method provided in client libraries.
            signature: '@ViewBag.Signature'.replace(/&amp;/g, '&'),
            accountCode: '@Model.CustomerId',
            //successURL: 'confirmation.html',
            addressRequirement: 'full',
            successHandler: SubscriptionResponse,
            planCode: 'planguru-analytics',
            distinguishContactFromBillingInfo: true,
            account: {
                firstName: '@Model.CustomerFirstName',
                lastName: '@Model.CustomerLastName',
                email: '@Model.CustomerEmail',
                companyName: '@Model.CustomerCompanyName'
            },
            acceptedCards: ['mastercard',
                    'discover',
                    'american_express',
                    'visa'],
            billingInfo: {
                firstName: '@Model.FirstNameOnCard',
                lastName: '@Model.LastNameOnCard',
                phone: '@Model.ContactTelephone',
                address1: '@Model.CustomerAddress1',
                address2: '@Model.CustomerAddress2',
                city: '@Model.City',
                zip: '@Model.CustomerPostalCode',
                state: '@Model.State',
                country: '@Model.Country',
                cardNumber: '@Model.CreditCardNumber',
                CVV: 'XXX'
            }
        });      
    });
</script>
<form id="Recurly-Subscription" runat="server" style="padding-bottom: 20px;">
<div id="recurly-update-billing-info">
</div>
</form>
