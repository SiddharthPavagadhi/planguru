﻿@ModelType  PagedList.IPagedList(Of onlineAnalytics.Dashboard)
@Imports Farpoint.Web.Spread
@Imports Farpoint.Mvc.Spread
@Code
    ViewData("Title") = "Index"
    Layout = "~/Views/Shared/AnalyticsMaster.vbhtml"
    Dim index As Integer = 1
End Code
<script src="@Url.Content("~/Scripts/jquery.js")" type="text/javascript"></script>
<script src="@Url.Content("~/Scripts/jQuery.dataTables.js")" type="text/javascript"></script>
<script src="@Url.Content("~/Scripts/jquery-ui.js")" type="text/javascript"></script>
<script src="@Url.Content("~/Scripts/jQuery.dataTables.rowReordering.js")" type="text/javascript"></script>
 <script src="@Url.Content("~/Scripts/jquery.msgBox.js")" type="text/javascript"></script>

<div class="headerTable" style="float: right">
    <table>
        <tr>
            <td align="left" width="70%">
                Dashboard Item List
            </td>
            <td align="right" width="15%" class="add">
                <img src='@Url.Content("~/Content/Images/1369406210_plus.png")' class="plusIcon" />
                <a href='@Url.Action("Create", "DashboardSetup")' class="button_example">New Dashboard
                    Item</a>
            </td>
        </tr>
    </table>
</div>
<div class="center-panel">
    <div class="center-tablesection">
        <div class="CSSTableGenerator">
            <table id="DashboardItems">
                @code
                    If Model.TotalItemCount > 0 Then
                    @<thead><tr>
                        <th class="left">
                            Order
                        </th>
                        <th class="left">
                            Description
                        </th>
                        <th class="left">
                            Format
                        </th>
                        <th class="left">
                            Chart Type
                        </th>
                        <th class="left">
                            IsValid
                        </th>
                        <th style="width: 2%;">
                            <img alt="Percentage" style="vertical-align:middle;" src='@Url.Content("~/Content/Images/Percentage.png")' title="Percentage" />
                        </th>
                        <th style="vertical-align: middle; width: 2%;">
                            <img alt="Tradeline" style="vertical-align:middle;" src='@Url.Content("~/Content/Images/Tradeline.png")' title="Tradeline" />
                        </th>
                        @If (ViewBag.IsValid = True) Then
                            @<th style="width: 3%;">
                                Edit
                            </th>                                                       
                            End If
                        <th style="width: 3%; text-align: center; vertical-align: middle;">
                            Delete
                        </th>
                    </tr>
                    </thead>                  
                    @<tbody>
                        @For Each item In Model
                                Dim currentItem = item
                                Dim cssClass As String = If(index Mod 2 = 0, "", "")
                                Dim isValid As String = currentItem.IsValid.ToString()
                                isValid = IIf(isValid = "True" Or isValid = "true", "Valid", "InValid")
                                Dim percentage As String = currentItem.ShowAsPercent.ToString()
                                Dim showTradeline As String = currentItem.ShowTrendline.ToString()
                                Dim showGoal As String = currentItem.ShowGoal.ToString()
                            @<tr id="@currentItem.DashboardId" class='@cssClass'>
                                <td class="left">
                                    @Html.DisplayFor(Function(modelItem) currentItem.Order)
                                </td>
                                <td class="left">
                                    @Html.DisplayFor(Function(modelItem) currentItem.DashDescription)
                                </td>
                                <td class="left">
                                    @Html.DisplayFor(Function(modelItem) currentItem.ChartFormat.Format)
                                </td>
                                <td class="left">
                                    @Html.DisplayFor(Function(modelItem) currentItem.ChartType.Type)
                                </td>
                                <td class="left">
                                    @Html.DisplayFor(Function(modelItem) isValid)
                                </td>
                                <td class="left">
                                    @Html.DisplayFor(Function(modelItem) percentage)
                                </td>
                                <td class="left">
                                    @Html.DisplayFor(Function(modelItem) showTradeline)
                                </td>
                                @If (currentItem.IsValid = True) Then
                                    @<td>
                                        <a href='@Url.Action("Edit", "DashboardSetup", New With {.dashboardId = currentItem.DashboardId})'>
                                            <img alt="Edit Dashboard Item" src='@Url.Content("~/Content/Images/edit.png")' title="Edit Dashboard Item" />
                                        </a>
                                    </td>                                                                       
                                End If
                                <td>
                                    <a class="Delete" href="#" data='@Url.Action("Delete", "DashboardSetup", New With {.dashboardId = currentItem.DashboardId})' >
                                        <img alt="Delete dashboard item" src='@Url.Content("~/Content/Images/delete.png")' title="Delete dashboard item" /></a>
                                </td>
                            </tr> 
                                index += 1
                            Next
                    </tbody>    
                    Else
                    @<tbody><tr>
                        <td style="text-align: center;">
                            No Dashboard items found.
                        </td>
                    </tr>
                    </tbody>    
                    End If
                End Code
            </table>
        </div>      
        @If Not (DirectCast(TempData("Message"), String) Is Nothing) Then
            @<label class="success">@TempData("Message").ToString()
            </label>                         
        End If
        @If ((Not ViewBag.IsValid Is Nothing) And (Not TempData("InfoMessage") Is Nothing)) Then
            @<label class="info">@TempData("InfoMessage").ToString()
            </label>                         
        End If
        @If Not (DirectCast(TempData("ErrorMessage"), String) Is Nothing) Then
            @<label class="error">@TempData("ErrorMessage").ToString()
            </label>                         
        End If
    </div>
</div>
<script type="text/javascript" language="javascript">


    $('#DashboardItems').dataTable({"bPaginate": false}).rowReordering({ sURL: "/DashboardSetup/UpdateOrder" });
    $('#DashboardItems_length').hide();
    $('#DashboardItems_info').hide();
    $('#DashboardItems_filter').hide();
    $('#DashboardItems_paginate').hide();
    

    $(".Delete").click(function (event) {
        event.preventDefault();
        var url = $(this).attr('data');

        $.msgBox({
            title: "Confirm",
            content: "Are you sure you want to delete this dashboard item?",
            type: "confirm",
            buttons: [{ value: "Yes" }, { value: "No"}],
            success: function (result) {
                if (result == "Yes") {
                    window.location = url;
                }
            }
        });

    });

   
    
</script>
