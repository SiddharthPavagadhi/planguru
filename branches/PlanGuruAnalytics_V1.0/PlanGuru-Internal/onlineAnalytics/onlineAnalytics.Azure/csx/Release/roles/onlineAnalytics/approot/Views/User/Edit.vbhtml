﻿@ModelType onlineAnalytics.User
@Imports System.Collections.Generic
@Code
    ViewData("Title") = "Edit User"
    Layout = "~/Views/Shared/AnalyticsMaster.vbhtml"
End Code
<script src="@Url.Content("~/Scripts/jquery.validate.min.js")" type="text/javascript"></script>
<script src="@Url.Content("~/Scripts/jquery.validate.unobtrusive.min.js")" type="text/javascript"></script>
@*<div style="float: left; padding-top: 5px; padding-left: 3px;">
    <label style="font-size: 20px;">
        Edit User</label>
</div>*@
@*<div class="bredcurm-panel">
    <div class="bredcrumb-nav">
        <label>
            User</label></div>
</div>*@
<div class="headerTable" style="float: right">
    <table>
        <tr>
            <td align="left" width="70%">
                Edit User
            </td>
            <td align="right" width="12%" style="padding-left: 20px;">
                <img src='@Url.Content("~/Content/Images/1369484457_users.png")' class="usersIcon"/>
                <a href='@Url.Action("Index", "User")' class="button_example">User List</a>
            </td>
        </tr>
    </table>
</div>
<br />
<br />
<div class="center-panel">
    @Using Html.BeginForm("Edit", "User")
        @Html.AntiForgeryToken()   
        @Html.ValidationSummary(True)    
     
        @<fieldset>
            <div style="width: 100;">
                <div style="float: left; width: 33%;">
                    <ol class="round">
                        <li>
                            @Html.LabelFor(Function(m) m.UserName)<br />
                            @Html.TextBoxFor(Function(m) m.UserName)<br />
                            @Html.ValidationMessageFor(Function(m) m.UserName)
                        </li>
                        @* <li>
                        @Html.LabelFor(Function(m) m.Password)<br />
                        @Html.PasswordFor(Function(m) m.Password)<br />
                        @Html.ValidationMessageFor(Function(m) m.Password)
                    </li>*@
                        <li>
                            @Html.LabelFor(Function(m) m.UserEmail)<br />
                            @Html.TextBoxFor(Function(m) m.UserEmail)<br />
                            @Html.ValidationMessageFor(Function(m) m.UserEmail)
                        </li>
                    </ol>
                </div>
                <div style="float: left; width: 33%;">
                    <ol class="round">
                        <li>
                            @Html.LabelFor(Function(m) m.FirstName)<br />
                            @Html.TextBoxFor(Function(m) m.FirstName)<br />
                            @Html.ValidationMessageFor(Function(m) m.FirstName)
                        </li>
                        <li>
                            @Html.LabelFor(Function(m) m.LastName)<br />
                            @Html.TextBoxFor(Function(m) m.LastName)<br />
                            @Html.ValidationMessageFor(Function(m) m.LastName)
                        </li>
                    </ol>
                </div>
                <div style="float: left; width: 33%;">
                    <ol class="round">
                        <li>
                            @Html.LabelFor(Function(m) m.UserRoleId)<br />
                            @Html.DropDownList("UserRoleId", DirectCast(ViewBag.SelectedRoles, SelectList), "Select User Role", New With {.Class = "select", .Style = "width:206px;"})<br />
                            @Html.ValidationMessageFor(Function(m) m.UserRoleId)
                        </li>
                        <li>
                            @Html.LabelFor(Function(m) m.CustomerId)<br />
                            @Html.DropDownList("CustomerId", DirectCast(ViewBag.SelectedCustomer, SelectList), "Select Customer", New With {.Class = "select", .Style = "width:206px;"})<br />
                            @Html.ValidationMessageFor(Function(m) m.CustomerId)
                        </li>
                    </ol>
                </div>
                <div class="input-form">
                    <input id="Submit" type="submit" value="Update User details" class="secondbutton_example" />
                    <input id="Cancel" type="button" value="Cancel" class="secondbutton_example" data='@Url.Action("Index")' />
                </div>
                <div style="float: left; margin-left: 20px;">
                    @If Not (DirectCast(TempData("Message"), String) Is Nothing) Then
                        @<label class="success">@TempData("Message").ToString()
                        </label>                         
        End If
                    @If Not (DirectCast(TempData("ErrorMessage"), String) Is Nothing) Then
                        @<label class="error">@TempData("ErrorMessage").ToString()
                        </label>                         
        End If
                </div>
                @Html.HiddenFor(Function(m) m.UserId)
            </div>
        </fieldset>                                  
    End Using
</div>
<script type="text/javascript" language="javascript">

    $(document).ready(function () {
        IsSessionAlive();
        var msg;
        //Input Mask for landline phone number
        $("#ContactTelephone").mask("(999) 999-9999");
        //$("#FiscalMonthStart").mask("9?9");


        $("#Cancel").click(function (event) {
            IsSessionAlive();
            //event.preventDefault();
            var url = $(this).attr('data');
            window.location = url;
        });


    });
</script>
