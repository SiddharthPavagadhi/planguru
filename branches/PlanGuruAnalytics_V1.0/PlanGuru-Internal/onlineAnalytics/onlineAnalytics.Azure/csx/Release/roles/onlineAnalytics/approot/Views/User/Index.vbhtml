﻿@ModelType  PagedList.IPagedList(Of onlineAnalytics.User)
@Imports  onlineAnalytics
@Code
    ViewData("Title") = "View Users"
    Layout = "~/Views/Shared/AnalyticsMaster.vbhtml"
    Dim index As Integer = 1
    
    Dim UserInfo As New User()
    Dim ua As New UserAccess
    If Not (Session("UserAccess") Is Nothing) Then
        ua = DirectCast(Session("UserAccess"), UserAccess)
    End If
    
    If Not (Session("UserInfo") Is Nothing) Then
        UserInfo = DirectCast(Session("UserInfo"), User)
    End If
End Code
<div class="headerTable" style="float: right">
    <table>
        <tr>
            <td align="left" width="70%">
                View User List
            </td>
            <td align="right" width="12%" class="add">
                <img src='@Url.Content("~/Content/Images/1369406210_plus.png")' class="plusIcon" />
                <a href='@Url.Action("CreateUser", "User")' class="button_example">New User</a>
            </td>
        </tr>
    </table>
</div>
<div class="center-panel">
    <div class="center-tablesection">
        <div class="CSSTableGenerator">
            <table>
                @code
                    If Model.TotalItemCount > 0 Then
                    @<thead>
                        <tr>
                            <th style="width: 20%">
                                User Name
                            </th>
                            <th style="width: 15%">
                                User Full Name
                            </th>
                            <th style="width: 15%">
                                User Email
                            </th>
                            <th style="width: 15%">
                                User Role
                            </th>
                            @Code
                                    If (UserInfo.UserRoleId <> UserRoles.CAU Or UserInfo.UserRoleId <> UserRoles.CRU) Then
                                @<th style="width: 15%; text-align: center;">
                                    User Status
                                </th>
                                    End If
                            End Code
                            <th>
                                Reset Password
                            </th>
                            <th style="width: 5%">
                                Edit
                            </th>
                            <th style="width: 5%">
                                Delete
                            </th>
                        </tr>
                    </thead>               
                        For Each item In Model
                            Dim currentItem = item
                            Dim cssClass As String = If(index Mod 2 = 0, "even", "")
                            Dim userStatus As String = [Enum].GetName(GetType(StatusE), currentItem.Status)
                        
                    @<tr class='@cssClass'>
                        <td>
                            @Html.DisplayFor(Function(modelItem) currentItem.UserName)
                        </td>
                        <td>
                            @Html.DisplayFor(Function(modelItem) currentItem.LastName)
                            @Html.DisplayFor(Function(modelItem) currentItem.FirstName)
                        </td>
                        <td>
                            @Html.DisplayFor(Function(modelItem) currentItem.UserEmail)
                        </td>
                        <td>
                            @Html.DisplayFor(Function(modelItem) currentItem.UserRole.RoleName)
                        </td>
                        @Code
                                    If (UserInfo.UserRoleId <> UserRoles.CAU Or UserInfo.UserRoleId <> UserRoles.CRU) Then
                            @<td style="text-align: center;">@Html.DisplayFor(Function(modelItem) userStatus)
                            </td>
                                    End If
                        End Code
                        <td style="text-align: center;">
                            <a class="resetPassword" data='@Url.Action("ResetPassword", "User", New With {.userId = currentItem.UserId})' onclick="javascript:resetPasswordConfirmation('@currentItem.UserName');">
                                <img alt="Reset Password" src='@Url.Content("~/Content/Images/ResetPasword.png")' title="Reset Password" style="height:19px;width:19px;" /></a>
                        </td>
                        <td>
                            <a href='@Url.Action("Edit", "User", New With {.userAccountCode = currentItem.UserId})'>
                                <img alt="Edit User" src='@Url.Content("~/Content/Images/edit.png")' title="Edit User" />
                            </a>
                        </td>
                        <td>
                            <a id='@currentItem.UserId' class="Delete" href="#" data='@Url.Action("Delete", "User", New With {.userAccountCode = currentItem.UserId})' >
                                <img alt="Delete User" src='@Url.Content("~/Content/Images/delete.png")' title="Delete User" /></a>
                        </td>
                    </tr>
                            index += 1
                    
                        Next
                    
                    Else
                    @<tr>
                        <td style="text-align: center;">
                            No user records found.
                        </td>
                    </tr>
                    End If
                End Code
            </table>
        </div>

        @code
            If Model.TotalItemCount > 10 Then
                @<div class="pagination-panel">
                @If Model.HasPreviousPage Then
                    @<a href='@Url.Action("Index", "User", New With {.page = Model.PageNumber - 1})'
                    class="prevous">Previous</a> 
                Else
                    @<a class="no-prevous">Previous</a> End If
                <div class="pagination-list"> <ul>
                @For index = 1 To Model.PageCount
                    @<li><a href='@Url.Action("Index", "User", New With {.page = index})' >@index</a></li>
                Next
                </ul> </div>
                @If Model.HasNextPage Then
                    @<a href='@Url.Action("Index", "User", New With {.page = Model.PageNumber + 1})'
                    class="next">Next</a> Else
                    @<a class="no-next">Next</a> End If
            </div>
            End If
        End Code

        @If Not (DirectCast(TempData("Message"), String) Is Nothing) Then
            @<label class="success">@TempData("Message").ToString()</label>                         
        End If
        @If Not (DirectCast(TempData("ErrorMessage"), String) Is Nothing) Then
            @<label class="error">@TempData("ErrorMessage").ToString()</label>                         
        End If
    </div>
</div>
<script type="text/javascript" language="javascript">

    $(".Delete").click(function (event) {
        IsSessionAlive();
        event.preventDefault();
        var url = $(this).attr('data');

        $.msgBox({
            title: "Confirm",
            content: "Are you sure you want to delete this user?",
            type: "confirm",
            buttons: [{ value: "Yes" }, { value: "No"}],
            success: function (result) {
                if (result == "Yes") {
                    window.location = url;
                }
            }
        });

    });

    $(document).ready(function () {
        IsSessionAlive();
    });
    function resetPasswordConfirmation(UserName) {
        IsSessionAlive();
        var url = $(".resetPassword").attr('data');
        $.msgBox({
            title: "Confirm",
            content: "Are you sure you want to reset password for " + UserName + "?",
            type: "confirm",
            buttons: [{ value: "Yes" }, { value: "No"}],
            success: function (result) {
                if (result == "Yes") {
                    window.location = url;
                }
            }
        });
    }
</script>
