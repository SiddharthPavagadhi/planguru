﻿@ModelType onlineAnalytics.Customer
<div style="width: 100%">
    <div style="width: 16%; margin-bottom: 15px; margin-left: 50px;">
        <table cellpadding="5" cellspacing="5">
            <tr>
                <td>
                    @Html.LabelFor(Function(m) m.SearchCustId)
                    @Html.TextBoxFor(Function(m) m.SearchCustId)
                </td>
                <td>
                    @Html.LabelFor(Function(m) m.SearchCustName)
                    @Html.TextBoxFor(Function(m) m.SearchCustName)
                </td>
                <td>
                    @Html.LabelFor(Function(m) m.SAUName)
                    @Html.TextBoxFor(Function(m) m.SAUName)
                </td>
                <td>
                    @Html.LabelFor(Function(m) m.SearchCustTelephone)
                    @Html.TextBoxFor(Function(m) m.SearchCustTelephone)
                </td>
                <td style="padding-top: 16px;">
                    <input type="submit" value="Search" class="secondbutton_example" />
                </td>
                <td style="padding-top: 16px;">
                    <input type="reset" value="Reset" class="secondbutton_example" id="resetBtn" />
                </td>
            </tr>
        </table>
    </div>
</div>
<script type="text/javascript" language="javascript">
    $("#resetBtn").click(function () {
        $("#SearchCustId").val();
        $("#SearchCustName").val();
        $("#SAUName").val();
        $("#SearchCustTelephone").val();
    });
</script>
