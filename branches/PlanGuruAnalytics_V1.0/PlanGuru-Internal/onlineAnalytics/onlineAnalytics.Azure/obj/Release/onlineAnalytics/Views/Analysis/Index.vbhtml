﻿@ModelType  PagedList.IPagedList(Of onlineAnalytics.Analysis)
@Imports  onlineAnalytics
@Code
    ViewData("Title") = "View Analyses"
    Layout = "~/Views/Shared/AnalyticsMaster.vbhtml"
    Dim index As Integer = 1
    
    Dim ua As New UserAccess
    If Not (Session("UserAccess") Is Nothing) Then
        ua = DirectCast(Session("UserAccess"), UserAccess)
    End If
End Code

<div class="headerTable" style="float: right">
    <table>
        <tr>
            <td align="left" width="70%">
                Analysis List
            </td>
            @Code
                If (ua.AddAnalysis = True) Then
                @<td align="right" width="12%" class="add">
                    <img src='@Url.Content("~/Content/Images/1369406210_plus.png")' class="plusIcon" />
                    <a href='@Url.Action("Create", "Analysis")' class="button_example">New Analysis</a>
                </td>
                End If
            End Code
        </tr>
    </table>
</div>
<div class="center-panel">
    <div class="center-tablesection">
        <div class="CSSTableGenerator">
            <table>
            @code
                If Model.TotalItemCount > 0 Then                                    
                    @<thead><tr>
                        <th style="width: 45%">
                            Analysis(Name)
                        </th>                        
                        @Code
                            If (ua.UpdateAnalysis = True) Then
                            @<th style="width: 5%">
                                Edit
                            </th>
                            End If
                        End Code
                        @Code
                            If (ua.DeleteAnalysis = True) Then
                            @<th style="width: 5%">
                                Delete
                            </th>
                            End If
                        End Code
                    </tr></thead>
                For Each item In Model
                    Dim currentItem = item
                    Dim cssClass As String = If(index Mod 2 = 0, "even", "")
                    @<tr class='@cssClass'>
                        <td class="left">
                            @Html.DisplayFor(Function(modelItem) currentItem.AnalysisName)
                        </td>
                        @*<td>
                            @Html.DisplayFor(Function(modelItem) currentItem.Company.CompanyName)
                        </td>*@
                        @Code
                                If (ua.UpdateAnalysis = True) Then
                            @<td>
                                <a href='@Url.Action("Edit", "Analysis", New With {.id = currentItem.AnalysisId})'>
                                    <img alt="Edit Analysis" src='@Url.Content("~/Content/Images/edit.png")' title="Edit Analysis" />
                                </a>
                            </td>
                                End If
                        End Code
                        @Code
                                If (ua.DeleteAnalysis = True) Then
                            @<td>
                                <a id=@currentItem.AnalysisId class="Delete" href="#" data='@Url.Action("Delete", "Analysis", New With {.id = currentItem.AnalysisId})' >
                                    <img alt="Delete Analysis" src='@Url.Content("~/Content/Images/delete.png")' title="Delete Analysis" /></a>
                            </td>
                                End If
                        End Code
                    </tr> 
                        index += 1
                    Next
                Else
                    @<tr><td style="text-align:center;">No analysis records found.</td></tr>
                End If
                End Code
            </table>
        </div>       

        @code
            If Model.TotalItemCount > 0 Then
                 @<div class="pagination-panel">
                    @If Model.HasPreviousPage Then
                        @<a href='@Url.Action("Index", "Analysis", New With {.page = Model.PageNumber - 1}) ' class="prevous">Previous</a>
                    Else
                        @<a class="no-prevous">Previous</a>
                    End If
                    <div class="pagination-list">
                        <ul>
                            @For index = 1 To Model.PageCount
                                @<li><a href='@Url.Action("Index", "Analysis", New With {.page = index})' >@index</a></li>
                            Next
                        </ul>
                    </div>
                    @If Model.HasNextPage Then
                        @<a href='@Url.Action("Index", "Analysis", New With {.page = Model.PageNumber + 1}) ' class="next">Next</a>
                    Else
                        @<a class="no-next">Next</a>
                    End If
                </div>      
            End If
        End Code
                            
        @If Not (DirectCast(TempData("Message"), String) Is Nothing) Then
            @<label class="success">@TempData("Message").ToString()
            </label>                         
        End If
        @If Not (DirectCast(TempData("ErrorMessage"), String) Is Nothing) Then
            @<label class="error">@TempData("ErrorMessage").ToString()
            </label>                         
        End If
    </div>
</div>
<script type="text/javascript" language="javascript">

    $(".Delete").click(function (event) {
        event.preventDefault();
        var url = $(this).attr('data');

        $.msgBox({
            title: "Confirm",
            content: "Are you sure you want to delete this analysis?",
            type: "confirm",
            buttons: [{ value: "Yes" }, { value: "No"}],
            success: function (result) {
                if (result == "Yes") {
                    window.location = url;
                }
            }
        });

    }); 
</script>
