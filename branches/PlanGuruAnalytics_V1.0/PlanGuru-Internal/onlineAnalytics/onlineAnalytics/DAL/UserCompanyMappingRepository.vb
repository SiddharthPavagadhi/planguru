﻿Imports System.Collections.Generic
Imports System.Linq
Imports System.Data

Public Class UserCompanyMappingRepository

    Inherits GenericRepository(Of UserCompanyMapping)
    Implements IUserCompanyMappingRepository
    Implements IDisposable
    'Private context As DataAccess
    Private disposed As Boolean = False

    Public Sub New(context As DataAccess)
        'Me.context = context
        MyBase.New(context)
    End Sub

    Public Function GetUserCompanyMappingByCompanyId(CompanyId As Integer) As IEnumerable(Of UserCompanyMapping) Implements IUserCompanyMappingRepository.GetUserCompanyMappingByCompanyId
        Return context.UserCompanyMappings.Where(Function(uc) uc.CompanyId.Equals(CompanyId))
    End Function
    'This GetCountryCodes Function will return list of Country code with Name.
    Public Function GetUserCompanyMapping() As IEnumerable(Of UserCompanyMapping) Implements IUserCompanyMappingRepository.GetUserCompanyMapping
        Return context.UserCompanyMappings.ToList()
    End Function
    Public Function GetUserCompanyMappingById(UserCompanyMappingId As Integer) As UserCompanyMapping Implements IUserCompanyMappingRepository.GetUserCompanyMappingById
        Return context.UserCompanyMappings.Find(UserCompanyMappingId)
    End Function
    Public Sub UpdateUserCompanyMapping(UserCompanyDetails As UserCompanyMapping) Implements IUserCompanyMappingRepository.UpdateUserCompanyMapping
        context.Entry(UserCompanyDetails).State = EntityState.Modified
    End Sub

    Public Sub DeleteUserCompanyMapping(UserCompanyMappingId As Integer) Implements IUserCompanyMappingRepository.DeleteUserCompanyMapping
        Dim usercomp_map As UserCompanyMapping = context.UserCompanyMappings.Find(UserCompanyMappingId)
        context.UserCompanyMappings.Remove(usercomp_map)
    End Sub
    Public Function DeleteUserCompanyMappingByUserIdAndCompanyId(CompanyId As Integer, UserIdList As String) As Integer Implements IUserCompanyMappingRepository.DeleteUserCompanyMappingByUserIdAndCompanyId

        Dim deleteQuery As String = String.Format("Delete from UserCompanyMapping where CompanyId = {0} And UserId in ({1})", CompanyId, UserIdList)
        Return context.Database.ExecuteSqlCommand(deleteQuery)

    End Function

    Public Function DeleteUserCompanyMappingByCompany(companyId As Integer) Implements IUserCompanyMappingRepository.DeleteUserCompanyMappingByCompany
        Logger.Log.Info(("Start Query : Get the list of Users by CompanyId: " & companyId))
        Dim companies = context.UserCompanyMappings.Where(Function(a) a.CompanyId = companyId).ToList()

        If Not (companies Is Nothing) And companies.Count > 0 Then
            Logger.Log.Info(("UserCompanyMapping  count : " & companies.Count))
            Logger.Log.Info("Start deleting : UserCompanyMapping By CompanyId")
            companies.ToList().ForEach(Sub(item) context.UserCompanyMappings.Remove(item))
            Logger.Log.Info(("End deleting : UserCompanyMapping By CompanyId"))
        End If
        Return companies.Count
    End Function

    Public Function DeleteUserCompanyMappingByUserId(userId As Integer) Implements IUserCompanyMappingRepository.DeleteUserCompanyMappingByUserId
        Logger.Log.Info(("Start Query : Get the list of User Mapping by UserId: " & userId))
        Dim userMapping = (From d In context.UserCompanyMappings.Where(Function(a) a.UserId = userId)
                                       Select d)
        If Not (userMapping Is Nothing) Then
            Logger.Log.Info(("User Mapping items count : " & userMapping.Count))
            Logger.Log.Info("Start deleting : User Mapping items")
            userMapping.ToList().ForEach(Sub(item) context.UserCompanyMappings.Remove(item))
            Logger.Log.Info(("End Deleting : User Mapping items"))
        End If
        Return userMapping.Count
    End Function

    Public Function GetDistinctCompanies(deactivatedUsers As List(Of Integer)) As List(Of Integer) Implements IUserCompanyMappingRepository.GetDistinctCompanies

        Return (From ucm In context.UserCompanyMappings.Where(Function(ucm) deactivatedUsers.Contains(ucm.UserId))
                Select ucm.CompanyId).Distinct().ToList()

    End Function
    Public Sub Save() Implements IUserCompanyMappingRepository.Save
        context.SaveChanges()
    End Sub
    Protected Overridable Sub Dispose(disposing As Boolean)
        If Not Me.disposed Then
            If disposing Then
                context.Dispose()
            End If
        End If
        Me.disposed = True
    End Sub

    Public Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

End Class
