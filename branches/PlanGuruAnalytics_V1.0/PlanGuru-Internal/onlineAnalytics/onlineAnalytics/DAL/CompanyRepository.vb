﻿Imports System.Collections.Generic
Imports System.Linq
Imports System.Data

Public Class CompanyRepository
    Implements ICompanyRepository
    Implements IDisposable
    Private context As DataAccess
    Private disposed As Boolean = False

    Public Sub New(context As DataAccess)
        Me.context = context
    End Sub

    Public Function GetCompanies() As IEnumerable(Of Company) Implements ICompanyRepository.GetCompanies
        Return context.Companies.OrderBy(Function(c) c.CompanyName).ToList()
    End Function

    Public Function GetCompanyByID(companyId As Integer) As Company Implements ICompanyRepository.GetCompanyById
        Return context.Companies.Where(Function(c) c.CompanyId = companyId).FirstOrDefault()
    End Function

    Public Sub InsertCompany(company As Company) Implements ICompanyRepository.InsertCompany
        context.Companies.Add(company)
    End Sub
    Public Sub DeleteCompany(companyID As Integer) Implements ICompanyRepository.DeleteCompany
        Logger.Log.Info(("Start Execution of DeleteCompany"))
        Dim utility As New Utility
        Logger.Log.Info(("Start Query :Find the company by CompanyId: " & companyID))
        Dim company As Company = context.Companies.Find(companyID)

        Logger.Log.Info(("Start Query :To Delete the Analyses For " & companyID))
        utility.AnalysisRepository.DeleteAnalysisByCompany(companyID)
        Logger.Log.Info("Execution Ended for DeleteAnalysisByCompany")

        Logger.Log.Info(("Start Query :To Delete the UserCompany Mapping for " & companyID))
        utility.UserCompanyMappingRepository.DeleteUserCompanyMappingByCompany(companyID)
        utility.UserCompanyMappingRepository.Save()
        Logger.Log.Info("Execution Ended for DeleteUserCompanyMappingByCompany")

        Logger.Log.Info("Execution Ended for DeleteCompany")
        If Not (company Is Nothing) Then
            context.Companies.Remove(company)
            context.SaveChanges()
        End If
    End Sub

    Public Sub UpdateCompany(company As Company) Implements ICompanyRepository.UpdateCompany
        context.Entry(company).State = EntityState.Modified
    End Sub

    Public Sub Save() Implements ICompanyRepository.Save
        context.SaveChanges()
    End Sub
    Protected Overridable Sub Dispose(disposing As Boolean)
        If Not Me.disposed Then
            If disposing Then
                context.Dispose()
            End If
        End If
        Me.disposed = True
    End Sub

    Public Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

End Class
