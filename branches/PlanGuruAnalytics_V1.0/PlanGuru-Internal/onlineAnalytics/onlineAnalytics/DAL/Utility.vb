﻿Imports Recurly
Imports System.Web.Mvc
Imports System.Security.Cryptography
Imports System.Linq
Imports System.Data.Linq

Public Class Utility
    Implements IDisposable
    Private context As New DataAccess()
    Private plan As String = String.Empty
    Private _companyRepository As GenericRepository(Of Company)
    Private _companiesRepository As CompanyRepository
    Private _customerRepository As CustomerRepository
    Private _userRepository As UserRepository
    Private _analysisRepository As AnalysisRepository
    Private _countryRepository As CountryRepository
    Private _stateRepository As StateRepository
    Private _userRoleRepository As UserRoleRepository
    Private _userRolePermissionRepository As UserRolePermissionRepository
    Private _userRolePermissionMappingRepository As UserRolePermissionMappingRepository
    Private _userCompanyMappingRepository As UserCompanyMappingRepository
    Private _userAnalysisMappingRepository As UserAnalysisMappingRepository
    Private _emailInfoRepository As EmailRepository
    Private _accountRepository As AccountRepository
    Private _accountTypeRepository As AccountTypeRepository
    Private _chartFormatRepository As ChartFormatRepository
    Private _chartTypeRepository As ChartTypeRepository
    Private _dashboardRepository As DashboardRepository
    Private _balanceRespository As BalanceRepository

    Private Shared chars As String = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    Private userName As String = String.Empty

    Public ReadOnly Property RecurlyPlan() As String
        Get
            Dim plan As RecurlyPlan = RecurlyPlanList.GetPlans().First()

            If Not String.IsNullOrEmpty(plan.PlanCode) Then
                Me.plan = plan.PlanCode
            End If
            Return Me.plan
        End Get
    End Property

    'The CompanyRepository property will provide access to get Company information from Company Table.
    Public ReadOnly Property CompanyRepository() As GenericRepository(Of Company)
        Get

            If Me._companyRepository Is Nothing Then
                Me._companyRepository = New GenericRepository(Of Company)(context)
            End If
            Return _companyRepository

        End Get
    End Property

    Public ReadOnly Property CompaniesRepository() As CompanyRepository
        Get

            If Me._companiesRepository Is Nothing Then
                Me._companiesRepository = New CompanyRepository(context)
            End If
            Return _companiesRepository

        End Get
    End Property

    'The CustomerRepository property will provide access to get Customer information from Customer Table.
    Public ReadOnly Property CustomerRepository() As CustomerRepository
        Get

            If Me._customerRepository Is Nothing Then
                Me._customerRepository = New CustomerRepository(context)
            End If
            Return _customerRepository

        End Get
    End Property
    'The UserRepository property will provide access to get User information from User Table.
    Public ReadOnly Property UserRepository() As UserRepository
        Get

            If Me._userRepository Is Nothing Then
                Me._userRepository = New UserRepository(context)
            End If
            Return _userRepository
        End Get
    End Property
    'The AnalysisRepository Property will provide access to get Analysis information from Analysis Table.
    Public ReadOnly Property AnalysisRepository() As AnalysisRepository
        Get

            If Me._analysisRepository Is Nothing Then
                Me._analysisRepository = New AnalysisRepository(context)
            End If
            Return _analysisRepository
        End Get
    End Property
    'The AnalysisRepository Property will provide access to get Analysis information from Analysis Table.
    Public ReadOnly Property EmailRepository() As EmailRepository
        Get

            If Me._emailInfoRepository Is Nothing Then
                Me._emailInfoRepository = New EmailRepository(context)
            End If
            Return _emailInfoRepository
        End Get
    End Property
    'The CountryRepository Property will provide access to get Country information from CountryCode Table.
    Public ReadOnly Property CountryRepository() As CountryRepository
        Get

            If Me._countryRepository Is Nothing Then
                Me._countryRepository = New CountryRepository(context)
            End If
            Return _countryRepository
        End Get
    End Property
    'The StateRepository Property will provide access to get State information from StateCode Table.
    Public ReadOnly Property StateRepository() As StateRepository
        Get

            If Me._stateRepository Is Nothing Then
                Me._stateRepository = New StateRepository(context)
            End If
            Return _stateRepository
        End Get
    End Property
    'The UserRoleRepository Property will provide access to get User Role information from UserRole Table.
    Public ReadOnly Property UserRoleRepository() As UserRoleRepository
        Get

            If Me._userRoleRepository Is Nothing Then
                Me._userRoleRepository = New UserRoleRepository(context)
            End If
            Return _userRoleRepository
        End Get
    End Property

    Public ReadOnly Property UserRolePermissionRepository() As UserRolePermissionRepository
        Get

            If Me._userRolePermissionRepository Is Nothing Then
                Me._userRolePermissionRepository = New UserRolePermissionRepository(context)
            End If
            Return _userRolePermissionRepository
        End Get
    End Property

    Public ReadOnly Property UserRolePermissionMappingRepository() As UserRolePermissionMappingRepository
        Get

            If Me._userRolePermissionMappingRepository Is Nothing Then
                Me._userRolePermissionMappingRepository = New UserRolePermissionMappingRepository(context)
            End If
            Return _userRolePermissionMappingRepository
        End Get
    End Property

    Public ReadOnly Property UserCompanyMappingRepository() As UserCompanyMappingRepository
        Get

            If Me._userCompanyMappingRepository Is Nothing Then
                Me._userCompanyMappingRepository = New UserCompanyMappingRepository(context)
            End If
            Return _userCompanyMappingRepository
        End Get
    End Property

    Public ReadOnly Property UserAnalysisMappingRepository() As UserAnalysisMappingRepository
        Get

            If Me._userAnalysisMappingRepository Is Nothing Then
                Me._userAnalysisMappingRepository = New UserAnalysisMappingRepository(context)
            End If
            Return _userAnalysisMappingRepository
        End Get
    End Property

    Public ReadOnly Property AccountRepository() As AccountRepository
        Get

            If Me._accountRepository Is Nothing Then
                Me._accountRepository = New AccountRepository(context)
            End If
            Return _accountRepository
        End Get
    End Property

    Public ReadOnly Property AccountTypeRepository() As AccountTypeRepository
        Get

            If Me._accountTypeRepository Is Nothing Then
                Me._accountTypeRepository = New AccountTypeRepository(context)
            End If
            Return _accountTypeRepository
        End Get
    End Property

    Public ReadOnly Property ChartFormatRepository() As ChartFormatRepository
        Get

            If Me._chartFormatRepository Is Nothing Then
                Me._chartFormatRepository = New ChartFormatRepository(context)
            End If
            Return _chartFormatRepository
        End Get
    End Property

    Public ReadOnly Property ChartTypeRepository() As ChartTypeRepository
        Get

            If Me._chartTypeRepository Is Nothing Then
                Me._chartTypeRepository = New ChartTypeRepository(context)
            End If
            Return _chartTypeRepository
        End Get
    End Property
    Public ReadOnly Property DashboardRepository() As DashboardRepository
        Get

            If Me._dashboardRepository Is Nothing Then
                Me._dashboardRepository = New DashboardRepository(context)
            End If
            Return _dashboardRepository
        End Get
    End Property

    Public ReadOnly Property BalanceRepository() As BalanceRepository
        Get

            If Me._balanceRespository Is Nothing Then
                Me._balanceRespository = New BalanceRepository(context)
            End If
            Return _balanceRespository
        End Get
    End Property


    'This GetMonth function will return the list of Month as List items for dropdown with id and Text.
    Public Shared Function GetMonth(Optional SelectedMonth As Object = Nothing) As SelectList
        Dim Periods As List(Of SelectListItem) = New List(Of SelectListItem)

        Periods.Add(New SelectListItem With {.Text = MonthName(1), .Value = 1, .Selected = False})
        Periods.Add(New SelectListItem With {.Text = MonthName(2), .Value = 2, .Selected = False})
        Periods.Add(New SelectListItem With {.Text = MonthName(3), .Value = 3, .Selected = False})
        Periods.Add(New SelectListItem With {.Text = MonthName(4), .Value = 4, .Selected = False})
        Periods.Add(New SelectListItem With {.Text = MonthName(5), .Value = 5, .Selected = False})
        Periods.Add(New SelectListItem With {.Text = MonthName(6), .Value = 6, .Selected = False})
        Periods.Add(New SelectListItem With {.Text = MonthName(7), .Value = 7, .Selected = False})
        Periods.Add(New SelectListItem With {.Text = MonthName(8), .Value = 8, .Selected = False})
        Periods.Add(New SelectListItem With {.Text = MonthName(9), .Value = 9, .Selected = False})
        Periods.Add(New SelectListItem With {.Text = MonthName(10), .Value = 10, .Selected = False})
        Periods.Add(New SelectListItem With {.Text = MonthName(11), .Value = 11, .Selected = False})
        Periods.Add(New SelectListItem With {.Text = MonthName(12), .Value = 12, .Selected = False})

        Dim months As SelectList = New SelectList(Periods, "Value", "Text", SelectedMonth)

        Return months
    End Function

    Public Sub Save()
        context.SaveChanges()
    End Sub

    Public Shared Function PouplateCompanyList(Optional UserType As UserRole = Nothing, Optional User As User = Nothing, Optional CustomerId As String = Nothing, Optional SelectedCompany As Integer = Nothing) As SelectList

        Dim utility = New Utility()
        Dim companyList As IEnumerable(Of Company) = Enumerable.Empty(Of Company)()

        'Dim companyList = utility.CompanyRepository.Get(orderBy:=Function(q) q.OrderBy(Function(d) d.CompanyId))
        'Dim companyList = utility.CompaniesRepository.GetCompanies().OrderBy(Function(d) d.CompanyId)

        If (Not UserType Is Nothing) Then

            If (UserType.UserRoleId = UserRoles.SAU Or UserType.UserRoleId = UserRoles.CAU Or UserType.UserRoleId = UserRoles.CRU) Then

                companyList = (From c In utility.CompaniesRepository.GetCompanies()
                              Join ucm In utility.UserCompanyMappingRepository.GetUserCompanyMapping() On c.CompanyId Equals ucm.CompanyId
                              Join u In utility.UserRepository.GetUsers() On u.UserId Equals ucm.UserId
                              Where u.Status = StatusE.Active And u.UserId = User.UserId
                              Select New Company With {.CompanyId = c.CompanyId, .CompanyName = Common.Decrypt(c.CompanyName), .FiscalMonthStart = c.FiscalMonthStart, .FiscalMonthName = c.FiscalMonthName, .ContactFirstName = Common.Decrypt(c.ContactFirstName), .ContactLastName = Common.Decrypt(c.ContactLastName), .ContactEmail = Common.Decrypt(c.ContactEmail), .ContactTelephone = Common.Decrypt(c.ContactTelephone), .CreatedBy = c.CreatedBy, .CreatedOn = c.CreatedOn, .UpdatedBy = c.UpdatedBy, .UpdatedOn = c.UpdatedOn}).OrderBy(Function(c) c.CompanyName).ToList()

            ElseIf (UserType.UserRoleId = UserRoles.PGAAdmin Or UserType.UserRoleId = UserRoles.PGASupport) Then

                companyList = (From c In utility.CompaniesRepository.GetCompanies()
                               Select New Company With {.CompanyId = c.CompanyId, .CompanyName = Common.Decrypt(c.CompanyName), .FiscalMonthStart = c.FiscalMonthStart, .FiscalMonthName = c.FiscalMonthName, .ContactFirstName = Common.Decrypt(c.ContactFirstName), .ContactLastName = Common.Decrypt(c.ContactLastName), .ContactEmail = Common.Decrypt(c.ContactEmail), .ContactTelephone = Common.Decrypt(c.ContactTelephone), .CreatedBy = c.CreatedBy, .CreatedOn = c.CreatedOn, .UpdatedBy = c.UpdatedBy, .UpdatedOn = c.UpdatedOn}).OrderBy(Function(c) c.CompanyName).ToList()

            End If
        End If

        SelectedCompany = If(((SelectedCompany = 0) And (companyList.Count > 0)), companyList.First.CompanyId, SelectedCompany)

        Return New SelectList(companyList, "CompanyId", "CompanyName", SelectedCompany)

    End Function

    Public Shared Function PouplateAnalysisList(Optional UserType As UserRole = Nothing, Optional User As User = Nothing, Optional CustomerId As String = Nothing, Optional SelectedCompany As Integer = Nothing, Optional SelectedAnalysis As Integer = Nothing) As SelectList

        Dim utility = New Utility()
        Dim analysisList As IEnumerable(Of Analysis) = Enumerable.Empty(Of Analysis)()

        If (Not UserType Is Nothing) Then

            If ((UserType.UserRoleId = UserRoles.SAU Or UserType.UserRoleId = UserRoles.CAU Or UserType.UserRoleId = UserRoles.CRU)) Then
                analysisList = (From a In utility.AnalysisRepository.GetAnalyses()
                             Join uam In utility.UserAnalysisMappingRepository.GetUserAnalysisMapping() On a.AnalysisId Equals uam.AnalysisId
                             Join u In utility.UserRepository.GetUsers() On u.UserId Equals uam.UserId
                             Where a.CompanyId = SelectedCompany And u.Status = StatusE.Active And u.UserId = User.UserId
                             Select New Analysis With {.AnalysisId = a.AnalysisId, .AnalysisName = a.AnalysisName, .CompanyId = a.CompanyId}) _
                             .OrderBy(Function(a) a.AnalysisName).ToList()

            ElseIf (UserType.UserRoleId = UserRoles.PGAAdmin Or UserType.UserRoleId = UserRoles.PGASupport) Then

                analysisList = (From a In utility.AnalysisRepository.GetAnalyses().Where(Function(c) c.CompanyId = SelectedCompany)
                                Select New Analysis With {.AnalysisId = a.AnalysisId, .AnalysisName = a.AnalysisName, .CompanyId = a.CompanyId}) _
                               .OrderBy(Function(a) a.AnalysisName).ToList()

            End If
        End If

        SelectedAnalysis = If(((SelectedCompany = 0) And (analysisList.Count > 0)), analysisList.First.AnalysisId, SelectedAnalysis)

        Return New SelectList(analysisList, "AnalysisId", "AnalysisName", SelectedAnalysis)

    End Function

    Public Shared Function PopulateCompanyAnalysisList(Optional UserType As UserRole = Nothing, Optional User As User = Nothing, Optional CustomerId As String = Nothing, Optional SelectedCompany As Integer = Nothing, Optional SelectedAnalysis As Integer = Nothing) As Hashtable

        Dim utility = New Utility()
        Dim CompanyAnalysisList As New Hashtable()

        Dim companyList = utility.CompanyRepository.Get(orderBy:=Function(q) q.OrderBy(Function(d) d.CompanyId))

        If (Not UserType Is Nothing) Then
            If (UserType.UserRoleId = UserRoles.CAU Or UserType.UserRoleId = UserRoles.CRU) Then
                companyList = From c In companyList
                              Join ucm In utility.UserCompanyMappingRepository.GetUserCompanyMapping() On c.CompanyId Equals ucm.CompanyId
                              Join u In utility.UserRepository.GetUsers() On u.UserId Equals ucm.UserId
                              Where u.Status = StatusE.Active And u.UserId = User.UserId Select c

            End If
        End If


        If (Not CustomerId Is Nothing) Then
            companyList = companyList.Where(Function(c) c.CustomerId = CustomerId)
        End If
        SelectedCompany = If(((SelectedCompany = 0) And (companyList.Count > 0)), companyList.First.CompanyId, SelectedCompany)

        Dim analysisList = utility.AnalysisRepository.Get(orderBy:=Function(b) b.OrderBy(Function(a) a.AnalysisId))

        If (Not UserType Is Nothing) Then
            If ((SelectedCompany <> 0) Or (UserType.UserRoleId = UserRoles.CAU Or UserType.UserRoleId = UserRoles.CRU)) Then
                analysisList = analysisList.Where(Function(a) (a.CompanyId = SelectedCompany))
            End If
        End If


        CompanyAnalysisList.Add("CompanyList", New SelectList(companyList, "CompanyId", "CompanyName", SelectedCompany))
        CompanyAnalysisList.Add("AnalysisList", New SelectList(analysisList, "AnalysisId", "AnalysisName", SelectedAnalysis))

        Return CompanyAnalysisList
    End Function

    Public Shared Function SignWithParameters(ParamArray parameters As String()) As String
        Try
            Dim nonce = String.Format("{0}={1}", "nonce", Guid.NewGuid().ToString())
            Dim timestamp = String.Format("{0}={1}", "timestamp", GetUnixTimeStamp(DateTime.UtcNow))

            Dim signatureParameters As New List(Of String)()
            signatureParameters.Add(nonce)
            signatureParameters.AddRange(parameters)
            signatureParameters.Add(timestamp)

            Dim protectedString = [String].Join("&", signatureParameters)
            Logger.Log.Info(String.Format("SignWithParameters Executed"))
            Return GenerateHMAC(protectedString) & "|" & Convert.ToString(protectedString)
        Catch ex As Exception
            Logger.Log.Error(String.Format("Unable to SignWithParameters with Message- {0} " + Environment.NewLine + "Stack Trace: {1} ", ex.Message, ex.StackTrace))
            Return String.Empty
        Finally
            Logger.Log.Info(String.Format("SignWithParameters Execution Ended"))
        End Try
    End Function

    Private Shared Function GenerateHMAC(stringToHash As String) As String
        Try
            Dim privatekey = "ae389bb1e1b94e6a84fb57c940694a17"
            Dim hasher = New HMACSHA1(UTF8Encoding.UTF8.GetBytes(privatekey))
            Dim hashBytes = hasher.ComputeHash(UTF8Encoding.UTF8.GetBytes(stringToHash))
            Dim hexDigest = BitConverter.ToString(hashBytes).Replace("-", "").ToLower()
            Logger.Log.Info(String.Format("GenerateHMAC Executed"))
            Return hexDigest
        Catch ex As Exception
            Logger.Log.Error(String.Format("Unable to GenerateHMAC with Message- {0} " + Environment.NewLine + "Stack Trace: {1} ", ex.Message, ex.StackTrace))
            Return String.Empty
        Finally
            Logger.Log.Info(String.Format("GenerateHMAC Execution Ended"))
        End Try

    End Function

    Private Shared Function GetUnixTimeStamp(timestamp As DateTime) As Integer
        Try
            Dim referenceDate = New DateTime(1970, 1, 1)
            Dim ts = New TimeSpan(timestamp.Ticks - referenceDate.Ticks)
            Logger.Log.Info(String.Format("GetUnixTimeStamp Executed"))
            Return (Convert.ToInt32(ts.TotalSeconds))
        Catch ex As Exception
            Logger.Log.Error(String.Format("Unable to GetUnixTimeStamp, with Message- {0} " + Environment.NewLine + "Stack Trace: {1} ", ex.Message, ex.StackTrace))
            Return 0
        Finally
            Logger.Log.Info(String.Format("GetUnixTimeStamp Execution Ended"))
        End Try

    End Function

    Public Shared Function CheckRequiredSetupCount(Optional User As User = Nothing, Optional selectedAnalysis As Integer = Nothing) As SetupCount
        Dim utility = New Utility()
        Dim SetupCountObj As New SetupCount
        '
        If (Not User Is Nothing) Then

            SetupCountObj.selectedAnalysis = selectedAnalysis

            SetupCountObj.countOfDashboardItem = Aggregate c In utility.DashboardRepository.GetDashboardDetail.Where(Function(a) a.AnalysisId = User.AnalysisId And a.UserId = User.UserId) Into Count()


            SetupCountObj.countDataUploadedOfSelectedAnalysis = Aggregate c In utility.AccountRepository.GetAccounts.Where(Function(a) a.AnalysisId = User.AnalysisId) Into Count()

            Dim companyList = utility.CompanyRepository.Get(orderBy:=Function(q) q.OrderBy(Function(d) d.CompanyId))
            'It will get the total no of company can setup and have access to for SAU / CAU / CRU User's.
            If (User.UserRoleId = UserRoles.SAU Or User.UserRoleId = UserRoles.CAU Or User.UserRoleId = UserRoles.CRU) Then
                SetupCountObj.CompanyCount = Aggregate c In companyList
                              Join ucm In utility.UserCompanyMappingRepository.GetUserCompanyMapping() On c.CompanyId Equals ucm.CompanyId
                              Join u In utility.UserRepository.GetUsers() On u.UserId Equals ucm.UserId
                              Where u.Status = StatusE.Active And u.UserId = User.UserId
                              Into Count()
            ElseIf (User.UserRoleId = UserRoles.PGAAdmin Or User.UserRoleId = UserRoles.PGASupport) Then

                'It will get the total no of company can setup and have access to for PGAU / PGSU.
                SetupCountObj.CompanyCount = Aggregate c In companyList Into Count()

            End If

            Dim analysisList = utility.AnalysisRepository.Get(orderBy:=Function(b) b.OrderBy(Function(a) a.AnalysisId))

            If ((User.UserRoleId = UserRoles.SAU Or User.UserRoleId = UserRoles.CAU Or User.UserRoleId = UserRoles.CRU)) Then
                SetupCountObj.AanlysisCount = Aggregate a In analysisList
                             Join uam In utility.UserAnalysisMappingRepository.GetUserAnalysisMapping() On a.AnalysisId Equals uam.AnalysisId
                             Join u In utility.UserRepository.GetUsers() On u.UserId Equals uam.UserId
                             Where u.Status = StatusE.Active And u.UserId = User.UserId
                             Into Count()

            ElseIf (User.UserRoleId = UserRoles.PGAAdmin Or User.UserRoleId = UserRoles.PGASupport) Then
                'It will get the total no of analysis can setup and have access to for PGAU / PGSU.
                SetupCountObj.AanlysisCount = Aggregate c In analysisList Into Count()
            End If

        End If

        Return SetupCountObj
    End Function

    Public Shared Function CheckAccountCodeOnRecurly(newUserAccountCode As Integer) As Integer
        If (RecurlyAccount.IsExist(newUserAccountCode)) Then
            newUserAccountCode = (newUserAccountCode + 1)
            newUserAccountCode = CheckAccountCodeOnRecurly(newUserAccountCode)
        End If

        Return newUserAccountCode
    End Function

    Public Shared Function AutoGenerateUserName() As String

        Dim random As New Random()
        Dim userName As String = New String(Enumerable.Repeat(chars, 5).Select(Function(s) s(random.Next(s.Length))).ToArray() & New Random().Next(10000, 99999).ToString())

        Return userName
    End Function

#Region "IDisposable Support"
    Private disposed As Boolean ' To detect redundant calls

    ' IDisposable
    Protected Overridable Sub Dispose(disposing As Boolean)
        If Not Me.disposed Then
            If disposing Then
                context.Dispose()
            End If
        End If
        Me.disposed = True
    End Sub

    ' This code added by Visual Basic to correctly implement the disposable pattern.
    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
#End Region

End Class
