﻿@ModelType onlineAnalytics.Customer
@Imports onlineAnalytics
@Imports System.Collections.Generic
@Code
    ViewData("Title") = "PlanGuru Analytics | Edit Subscription"
    If Not Session("action") Is Nothing Then
        
        If (Session("action") = "SetDefaultAnalysis" or Session("action") = "GetAnalyses") Then
            Layout = Nothing
            Session.Remove("action")
        End If
    Else
        Layout = "~/Views/Shared/AnalyticsMaster.vbhtml"
    End If
    
    Dim ua As New UserAccess
    If Not (Session("UserAccess") Is Nothing) Then
        ua = DirectCast(Session("UserAccess"), UserAccess)
    End If
End Code
<div class="headerTable" style="float: right">
    <table>
        <tr>
            <td align="left" width="70%">
               Edit Subscription
            </td>
              @Code
                  If (ua.ViewSubscription = True) Then
                      'Below href attribute used in AnalyticsMaster.vbhtml page as Jquery function, written on $(".add").click     
                @<td align="right" width="12%" class="add button_example" href='@Url.Action("Index", "Subscribe")'>
                    <img src='@Url.Content("~/Content/Images/List.png")' class="analysisIcon" style="margin-right:5px;float:left;"/>Subscriber List
                </td>
                End If
            End Code
        </tr>
    </table>
</div>
<br /><br />
<header>    
<link rel="stylesheet" href="@Url.Content("~/themes/default/recurly.css")" type="text/css" />
    <link rel="stylesheet" href="@Url.Content("~/themes/default/examples.css")" type="text/css" />
    <script type="text/javascript" language=javascript src="@Url.Content("~/Recurly/recurly.js")"></script>
    </header>
<script type="text/javascript" language="javascript">

    function cancelEvent() {        
            var url = '@Url.Action("Index", "Subscribe")';
            window.location = url;
    }

    $(document).ready(function () {
      
        IsSessionAlive();
        function SubscriptionResponse(response) {
            Recurly.postResult('@Url.Action("UpdateSubscription", "Subscribe")', response, null);

        }

        Recurly.config({
            subdomain: 'new-horizon-software-technologies-inc',
            currency: 'USD'
        });

        Recurly.buildBillingInfoUpdateForm({
            target: '#recurly-update-billing-info',
            // Signature must be generated server-side with a utility method provided in client libraries.
            signature: '@ViewBag.Signature'.replace(/&amp;/g, '&'),
            accountCode: '@Model.CustomerId',
            //successURL: 'confirmation.html',
            addressRequirement: 'full',
            successHandler: SubscriptionResponse,
            planCode: 'planguru-analytics',
            distinguishContactFromBillingInfo: true,
            account: {
                firstName: '',
                lastName: '',
                email: '',
                companyName: ''
            },
            acceptedCards: ['mastercard',
                    'discover',
                    'american_express',
                    'visa'],
            billingInfo: {
                firstName: '',
                lastName: '',
                phone: '',
                address1: '',
                address2: '',
                city: '',
                zip: '',
                state: '',
                country: '',
                cardNumber: '',
                CVV: ''
            }
        });      
    });
</script>
<form id="Recurly-Subscription" runat="server" style="padding-bottom: 20px;">
<div id="recurly-update-billing-info">
</div>
</form>
