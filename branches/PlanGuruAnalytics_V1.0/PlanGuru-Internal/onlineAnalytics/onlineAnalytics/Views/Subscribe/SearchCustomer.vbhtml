﻿@ModelType PagedList.IPagedList(Of onlineAnalytics.Customer)
@Imports  onlineAnalytics
@Code
    ViewData("Title") = "PlanGuru Analytics | Search Customer"
    If Not Session("action") Is Nothing Then
        
        If (Session("action") = "SetDefaultAnalysis" or Session("action") = "GetAnalyses") Then
            Layout = Nothing
            Session.Remove("action")
        End If
    Else
        Layout = "~/Views/Shared/AnalyticsMaster.vbhtml"
    End If
    
    Dim index As Integer = 1
    
    Dim ua As New UserAccess
    If Not (Session("UserAccess") Is Nothing) Then
        ua = DirectCast(Session("UserAccess"), UserAccess)
    End If
End Code

<div class="headerTable" style="float: right">
    <table>
        <tr>
            <td align="left" width="70%">
                Search Customer
            </td>           
             @Code
                If (ua.ViewUser = True) Then
                    'Below href function Jquery function call written in AnalyticsMaster.vbhtml on $(".add").click     
                @<td align="right" width="12%" class="add button_example" href='@Url.Action("Index", "User")'>
                    <img src='@Url.Content("~/Content/Images/List.png")' class="usersIcon" style="margin-right:5px;float:left;"/>User List
                </td>
                End If
            End Code
        </tr>
    </table>
</div>
@Using Html.BeginForm("SearchCustomer", "Subscribe")
    @Html.AntiForgeryToken()   
    @Html.ValidationSummary(True)      
    @<fieldset style="float: left;">
        @Html.Partial("_SearchCustomerPartial", New onlineAnalytics.Customer)
        <div style="float: left; margin-left: 20px;">
            @If Not (DirectCast(TempData("Message"), String) Is Nothing) Then
                @<label class="success">@TempData("Message").ToString()
                </label>                         
    End If
            @If Not (DirectCast(TempData("ErrorMessage"), String) Is Nothing) Then
                @<label class="error">@TempData("ErrorMessage").ToString()
                </label>                         
    End If
        </div>
        <div class="CSSTableGenerator">
            <table>
                <thead>
                    <tr>
                        <th style="width: 15%">
                            Customer No
                        </th>
                        <th style="width: 20%">
                            Customer Full Name
                        </th>
                        <th style="width: 20%">
                            Telephone Number
                        </th>
                        <th style="width: 15%">
                            Company Name
                        </th>
                    </tr>
                </thead>
                @For Each item In Model
        Dim currentItem = item
        Dim cssClass As String = If(index Mod 2 = 0, "even", "")
                  
                    @<tr class='@cssClass'>
                        <td>
                            @Html.DisplayFor(Function(modelItem) currentItem.CustomerId)
                        </td>
                        <td>
                            @Html.DisplayFor(Function(modelItem) currentItem.CustomerFullName)
                        </td>
                        <td>
                            @Html.DisplayFor(Function(modelItem) currentItem.ContactTelephone)
                        </td>
                        <td>
                            @Html.DisplayFor(Function(modelItem) currentItem.CustomerCompanyName)
                        </td>
                    </tr>
        index += 1
    Next
            </table>
        </div>
    </fieldset>
   
   
End Using
@*View Result *@ @*<fieldset>
    <legend>Search Customer Result</legend>
    <br />*@ @*<table class="table-layout">
        <tr>
            <th class="left" style="width: 20%">
                Customer No
            </th>
            <th class="left" style="width: 20%">
                Customer Full Name
            </th>
            <th class="left" style="width: 20%">
                Telephone Number
            </th>
            <th class="center" style="width: 15%">
                SAU Name
            </th>         
                      
        </tr>
        @For Each item In Model
            Dim currentItem = item
            @<tr>
                <td class="left">
                    @Html.DisplayFor(Function(modelItem) currentItem.CustomerId)
                </td>
                <td class="left">
                    @Html.DisplayFor(Function(modelItem) currentItem.CustomerFullName)
                </td>
                <td class="left">
                    @Html.DisplayFor(Function(modelItem) currentItem.ContactTelephone)
                </td>
                <td class="center">
                    @Html.DisplayFor(Function(modelItem) currentItem.SAUName)
                </td>               
                             
            </tr>
        Next
    </table>    
    @If Not (DirectCast(TempData("Message"), String) Is Nothing) Then
        @<label class="success">@TempData("Message").ToString()
        </label>                         
    End If
    @If Not (DirectCast(TempData("ErrorMessage"), String) Is Nothing) Then
        @<label class="error">@TempData("ErrorMessage").ToString()
        </label>                         
    End If*@ @*</fieldset>*@