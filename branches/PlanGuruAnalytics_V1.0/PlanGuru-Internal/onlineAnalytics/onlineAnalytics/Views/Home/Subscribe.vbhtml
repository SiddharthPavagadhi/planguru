﻿@ModelType onlineAnalytics.Customer
@Imports onlineAnalytics
@Imports System.Collections.Generic
@Code
    ViewData("Title") = "PlanGuru Analytics | SignUp"
     Layout = "~/Views/Shared/_Layout.vbhtml"    
End Code

       <header>    
            <link rel="stylesheet" href="@Url.Content("~/themes/default/recurly.css")" type="text/css" />
             <link rel="stylesheet" href="@Url.Content("~/themes/default/examples.css")" type="text/css" />
          <script type="text/javascript" language=javascript src="@Url.Content("~/Recurly/recurly.js")"></script>
    </header>
<script>

    $(document).ready(function () {       

        function SubscriptionResponse(response) {

            Recurly.postResult('@Url.Action("Receipt", "Home")', response, null);
        }

        Recurly.config({
            subdomain: 'new-horizon-software-technologies-inc',
            currency: 'USD'
        });

        Recurly.buildSubscriptionForm({
            target: '#recurly-subscribe',
            // Signature must be generated server-side with a utility method provided in client libraries.
            signature: '@ViewBag.Signature'.replace(/&amp;/g, '&'),
            successHandler: SubscriptionResponse,
            //successURL: 'confirm.aspx',                
            planCode: 'planguru-analytics',
            distinguishContactFromBillingInfo: true,
            collectCompany: true,
            enableCoupons: true,
            enableAddOns: false,
            collectPhone: true,
            termsOfServiceURL: 'http://planguru.com/terms-of-service',
            acceptedCards: ['mastercard',
                    'discover',
                    'american_express',
                    'visa'],
            account: {
                accountCode: '@ViewBag.AccountCode',
                firstName: '',
                lastName: '',
                email: '',
                companyName: ''

            },
            billingInfo: {
                firstName: '',
                lastName: '',
                phone: '',
                address1: '',
                address2: '',
                city: '',
                zip: '',
                state: '',
                country: '',
                cardNumber: '',
                CVV: ''
            }
        });

    });
</script>
<div class="headerTable" style="float:right;">
    <table>
        <tr >
            <td style="border:none !important; text-align:left;" >Subscribe
            </td>            
        </tr>
    </table>
</div>
<form id="Recurly-Subscription" runat="server" style="padding-bottom: 20px;">
<div id="recurly-subscribe" style="margin: 0 0 50px;">
</div>
</form>

