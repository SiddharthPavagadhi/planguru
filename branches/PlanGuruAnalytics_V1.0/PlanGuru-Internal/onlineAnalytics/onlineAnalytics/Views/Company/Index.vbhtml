﻿@ModelType  PagedList.IPagedList(Of onlineAnalytics.Company)
@Imports  onlineAnalytics
@Code
    ViewData("Title") = "PlanGuru Analytics | View Companies"
    If Not Session("action") Is Nothing Then
        
        If (Session("action") = "SetDefaultAnalysis" or Session("action") = "GetAnalyses") Then
            Layout = Nothing
            Session.Remove("action")
        End If
    Else
        Layout = "~/Views/Shared/AnalyticsMaster.vbhtml"
    End If
    
    Dim index As Integer = 1    
    Dim ua As New UserAccess
    If Not (Session("UserAccess") Is Nothing) Then
        ua = DirectCast(Session("UserAccess"), UserAccess)
    End If
End Code
<div class="headerTable" style="float: right">
    <table>
        <tr>
            <td align="left" width="70%">
                Company List
            </td> 
            @Code
                If (ua.AddCompany = True) Then
                    'Below href attribute used in AnalyticsMaster.vbhtml page as Jquery function, written on $(".add").click     
                @<td align="right" width="12%" class="add button_example" href='@Url.Action("Create", "Company")'>
                    <img src='@Url.Content("~/Content/Images/plus.gif")' class="plusIcon" /> New Company
                </td>
                End If
            End Code
        </tr>
    </table>
</div>
<div class="center-panel">
    <div class="center-tablesection">
        <div class="CSSTableGenerator">
            <table>
                @code
                    If Model.TotalItemCount > 0 Then
                    @<thead><tr>
                        <th class="left" style="width: 18%">
                            Company Name
                        </th>
                        <th style="width: 18%">
                            Contact Name
                        </th>
                        <th style="width: 15%">
                            Contact Email
                        </th>
                        <th style="width: 15%">
                            Contact No.
                        </th>
                        <th style="width: 12%">
                            Fis. Mth Start
                        </th>
                        @Code
                                If (ua.UpdateCompany = True) Then
                            @<th style="width: 5%">
                                Edit
                            </th>
                                End If
                        End Code
                        @Code
                                If (ua.DeleteCompany = True) Then
                            @<th style="width: 5%">
                                Delete
                            </th>
                                End If
                        End Code
                        @* <th style="width: 5%">
                                Analysis
                            </th>*@
                    </tr>
                    </thead>
                        For Each item In Model
                            Dim currentItem = item
                            Dim cssClass As String = If(index Mod 2 = 0, "even", "")
                    @<tr class='@cssClass'>
                        <td>
                            @Html.DisplayFor(Function(modelItem) currentItem.CompanyName)
                        </td>
                        <td>
                            @Html.DisplayFor(Function(modelItem) currentItem.ContactLastName)
                            @Html.DisplayFor(Function(modelItem) currentItem.ContactFirstName)
                        </td>
                        <td>
                            @Html.DisplayFor(Function(modelItem) currentItem.ContactEmail)
                        </td>
                        <td>
                            @Html.DisplayFor(Function(modelItem) currentItem.ContactTelephone)
                        </td>
                        <td>
                            @Html.DisplayFor(Function(modelItem) currentItem.FiscalMonthName)
                        </td>
                        @Code
                                    If (ua.UpdateCompany = True) Then
                            @<td>
                                <a href='@Url.Action("Edit", "Company", New With {.id = currentItem.CompanyId})'>
                                    <img alt="Edit Company" src='@Url.Content("~/Content/Images/edit.png")' title="Edit Company" />
                                </a>
                            </td>
                                    End If
                        End Code
                        @Code
                                    If (ua.DeleteCompany = True) Then
                            @<td>
                                <a id='@currentItem.CompanyId' class="Delete" href="#" data='@Url.Action("Delete", "Company", New With {.companyId = currentItem.CompanyId})' >
                                    <img alt="Delete Company" src='@Url.Content("~/Content/Images/delete.png")' title="Delete Company" /></a>
                            </td>
                                    End If
                        End Code
                        @*<td>
                                        <a href='@Url.Action("Create", "Analysis", New With {.SelectedCompany = currentItem.CompanyId}) ' >
                                            <img alt="Add Analysis" src='@Url.Content("~/Content/Images/1369659978_plus_alt.png")' title="Add Analysis" /></a>
                                    </td>*@
                    </tr>                             
                            index += 1
                        Next
                    Else
                    @<tr>
                        <td style="text-align: center;">
                            No company records found.
                        </td>
                    </tr>                    
                    End If
                End Code
            </table>
        </div>
        @code
            If Model.TotalItemCount > 0 Then
            @<div class="pagination-panel">
                @If Model.HasPreviousPage Then
                    @<a href='@Url.Action("Index", "Company", New With {.page = Model.PageNumber - 1}) ' class="prevous">Previous</a>
                    Else
                    @<a class="no-prevous">Previous</a>
                    End If
                <div class="pagination-list">
                    <ul>
                        @For index = 1 To Model.PageCount
                            @<li><a href='@Url.Action("Index", "Company", New With {.page = index})' >@index</a></li>
                            Next
                    </ul>
                </div>
                @If Model.HasNextPage Then
                    @<a href='@Url.Action("Index", "Company", New With {.page = Model.PageNumber + 1}) ' class="next">Next</a>
                    Else
                    @<a class="no-next">Next</a>
                    End If
            </div>
            End If
        End Code
        
        @If Not (DirectCast(TempData("Message"), String) Is Nothing) Then
            @<label class="success">@TempData("Message").ToString()
            </label>                         
        End If
        @If Not (DirectCast(TempData("InfoMessage"), String) Is Nothing) Then
            @<label class="info">@TempData("InfoMessage").ToString()
            </label>                         
        End If
        @If Not (DirectCast(TempData("ErrorMessage"), String) Is Nothing) Then
            @<label class="error">@TempData("ErrorMessage").ToString()
            </label>                         
        End If
    </div>
</div>

<script type="text/javascript" language="javascript">
  
    $(".Delete").click(function (event) {
        IsSessionAlive();
        event.preventDefault();
        var url = $(this).attr('data');

        $.msgBox({
            title: "Confirm",
            content: "Are you sure you want to delete this company?",
            type: "confirm",
            buttons: [{ value: "Yes" }, { value: "No"}],
            success: function (result) {
                if (result == "Yes") {
                    window.location = url;
                }
            },
            afterShow: function () { $('[name=No]').focus(); }
        });

    });

    function IsSessionAlive() {
        $.ajax({
            type: "GET",
            url: '@Url.Action("IsSessionAlive", "Dashboard")',
            global: false,
            cache: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (msg) {

                if (msg == undefined || (msg == "") || msg == false) {

                    window.location = '@Url.Action("Index", "Home", New With {.session = "-1"})';
                }

            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                // var msg = JSON.parse(XMLHttpRequest.responseText);
                //theDialog.dialog('close');
            }
        });
    }
</script>
