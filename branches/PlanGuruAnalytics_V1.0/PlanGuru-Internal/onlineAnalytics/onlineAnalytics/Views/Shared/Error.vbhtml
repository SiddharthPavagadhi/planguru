﻿@ModelType System.Web.Mvc.HandleErrorInfo
@Code
    ViewData("Title") = "Error"
End Code

    <label class="warning">Sorry, an error occurred while processing your request.</label>    
