﻿@ModelType onlineAnalytics.Analysis
@Imports MvcCheckBoxList.Model
@Imports  onlineAnalytics
@Code
    ViewData("Title") = "PlanGuru Analytics | Edit Analysis"
    If Not Session("action") Is Nothing Then
        
        If (Session("action") = "SetDefaultAnalysis" or Session("action") = "GetAnalyses") Then
            Layout = Nothing
            Session.Remove("action")
        End If
    Else
        Layout = "~/Views/Shared/AnalyticsMaster.vbhtml"
    End If
    
    
    Dim ua As New UserAccess
    If Not (Session("UserAccess") Is Nothing) Then
        ua = DirectCast(Session("UserAccess"), UserAccess)
    End If
End Code
<script src="@Url.Content("~/Scripts/jquery.validate.min.js")" type="text/javascript"></script>
<script src="@Url.Content("~/Scripts/jquery.validate.unobtrusive.min.js")" type="text/javascript"></script>
@*<div class="bredcurm-panel">
    <div class="bredcrumb-nav">
        <label>
            Analysis</label></div>
</div>*@
<div class="headerTable" style="float: right">
    <table>
        <tr>
            <td align="left" width="70%">
                Edit Analysis
            </td>            
            @Code
                If (ua.ViewAnalyses = True) Then
                    'Below href function Jquery function call written in AnalyticsMaster.vbhtml on $(".add").click     
                @<td align="right" width="12%" class="add button_example" href='@Url.Action("Index", "Analysis")'>
                    <img src='@Url.Content("~/Content/Images/List.png")' class="analysisIcon" style="margin-right:5px;float:left;"/> Analysis List
                </td>
                End If
            End Code
        </tr>
    </table>
</div>
<br />
<br />
<div class="center-panel">
    @Using Html.BeginForm("Edit", "Analysis")
        @Html.AntiForgeryToken()   
        @Html.ValidationSummary(True)    
     
        @<fieldset>
            <div style="width: 100;">
                <div style="float: left; width: 33%;">
                    <ol class="round">
                        <li>
                            @Html.LabelFor(Function(m) m.AnalysisName)<br />
                            @Html.TextBoxFor(Function(m) m.AnalysisName)<br />
                            @Html.ValidationMessageFor(Function(m) m.AnalysisName)
                        </li>
                    </ol>
                </div>
                <div style="float: left; width: 36%;">
                    <ol class="round">
                        <li>
                            @Html.LabelFor(Function(m) m.CompanyId, "Company")<br />
                            @Html.EditorFor(Function(m) m.CompanyName)<br />
                            @Html.HiddenFor(Function(m) m.CompanyId)
                            @*@Html.DropDownList("CompanyId", DirectCast(ViewBag.SelectedCompany, SelectList), "Select Company", New With {.Class = "select", .Style = "width:250px;"})*@
                        </li>
                    </ol>
                </div>
                @If (ua.UserAnalysisMapping) Then
                    @<div class="associate_user">
                        @Code
            Dim htmlListInfo = New HtmlListInfo(HtmlTag.table, 5, New With {.class = "styled_list"}, TextLayout.Default, TemplateIsUsed.No)
            Dim checkBoxAtt = New With {.class = "styled_checkbox"}
                        End Code
                        <ol class="round">
                            <li>
                                @Html.LabelFor(Function(m) m.PostedUsers.UserIds) analysis
                                @Html.CheckBoxListFor(Function(u) u.PostedUsers.UserIds, Function(u) u.AvailableUsers, Function(u) u.Id, Function(u) u.Name, Function(u) u.SelectedUsers, checkBoxAtt, htmlListInfo, Nothing, Function(u) u.Tag)
                                @Html.ValidationMessageFor(Function(m) m.PostedUsers.UserIds)
                            </li>
                        </ol>
                    </div>
        End If
                <div class="input-form">
                    <input type="submit" value="Update analysis details" class="secondbutton_example" />
                    <input id="Cancel" type="button" value="Cancel" class="secondbutton_example" data='@Url.Action("Index")' />
                </div>
                <div style="float: left; margin-top: 10px;">
                    @If Not (DirectCast(TempData("Message"), String) Is Nothing) Then
                        @<label class="success">@TempData("Message").ToString()
                        </label>                         
        End If
                    @If Not (DirectCast(TempData("ErrorMessage"), String) Is Nothing) Then
                        @<label class="error">@TempData("ErrorMessage").ToString()
                        </label>                         
        End If
                </div>
                @Html.HiddenFor(Function(m) m.AnalysisId)
            </div>
        </fieldset>                                  
    End Using
</div>
<script type="text/javascript" language="javascript">

    $(document).ready(function () {

        $('[user="SAU"]').attr("class", "userlist_checkbox user SAU");
        $('[user="CAU"]').attr("class", "userlist_checkbox user CAU");
        $('[user="CRU"]').attr("class", "userlist_checkbox user CRU");

        $('[user="SAU"]').parent().prepend('<img src="@Url.Content("~/Content/Images/SAU.png")" style="height:25px;margin:0 0 0 2px;"/><br/>');
        $('[user="CAU"]').parent().prepend('<img src="@Url.Content("~/Content/Images/CAU.png")" style="height:25px;margin:0 0 0 2px;"/><br/>');
        $('[user="CRU"]').parent().prepend('<img src="@Url.Content("~/Content/Images/CRU.png")" style="height:25px;margin:0 0 0 2px;"/><br/>');

        $("#Cancel").click(function (event) {
            //event.preventDefault();
            var url = $(this).attr('data');
            window.location = url;
        });

        $("#CompanyName").attr("disabled", true);
    });  


</script>
