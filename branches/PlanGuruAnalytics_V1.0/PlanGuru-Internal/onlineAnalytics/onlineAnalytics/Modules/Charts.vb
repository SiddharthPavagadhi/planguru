﻿Imports FarPoint.Mvc.Spread
Module Charts
    Dim intDescCol As Integer = 4
    Dim intFirstNumberCol As Integer = 5

    Public Sub BuildChart(ByVal spdChart As FpSpread, ByRef spdAnalytics As FpSpread, ByRef intChartType As Integer, ByRef intFormat As Integer, ByRef intFirstNumbCol As Integer, ByVal blnShowasPerc As Boolean, ByVal blnShowTrend As Boolean, blnspdHasTotalCol As Boolean)
        Dim intSelectedRows As New ArrayList
        Dim intRowCtr As Integer
        Dim intColCtr As Integer
        Dim intStartCol As Integer
        Dim intNumbofCols As Integer
        Dim strSave As String
        Dim decChartValues(,) As Decimal
        Dim strChartDesc As String
        Dim intPeriods As Integer
        Dim intColumns As Integer
        Dim percent As FarPoint.Web.Spread.PercentCellType


        With spdChart
            .BorderColor = Drawing.Color.White
            .CommandBar.Visible = False
            .VerticalScrollBarPolicy = FarPoint.Web.Spread.ScrollBarPolicy.Never
            .HorizontalScrollBarPolicy = FarPoint.Web.Spread.ScrollBarPolicy.Never
            .Width = spdAnalytics.Width
        End With
        With spdChart.Sheets(0)
            .RowCount = 2
            .ColumnCount = 1
            .DefaultStyle.Font.Size = FontSize.XXLarge
            .Columns(0).Width = spdAnalytics.Width.Value
            .Cells(0, 0).BackColor = Drawing.Color.FromArgb(237, 237, 237)
            .Cells(0, 0).ForeColor = Drawing.Color.FromArgb(39, 111, 166)
            .Cells(0, 0).HorizontalAlign = HorizontalAlign.Center
            .Cells(0, 0).VerticalAlign = VerticalAlign.Middle
        End With
        Try
            spdChart.Sheets(0).Charts.Remove(spdChart.Sheets(0).Charts(0))
        Catch ex As Exception

        End Try
        'get selected rows
        For intRowCtr = 0 To spdAnalytics.Sheets(0).RowCount - 1
            strSave = Convert.ToString(spdAnalytics.GetEditValue(intRowCtr, 2))
            If strSave = "True" Then
                intSelectedRows.Add(intRowCtr)
            End If
        Next
        'store values to chart
        Select Case intFormat
            Case 0 To 4
                ReDim decChartValues(intSelectedRows.Count - 1, 1)
                intPeriods = 1
                intNumbofCols = 2
                intStartCol = intFirstNumbCol
            Case 5, 6
                intNumbofCols = spdAnalytics.Sheets(0).ColumnCount - intFirstNumbCol
                If blnspdHasTotalCol = True Then
                    intNumbofCols -= 1
                End If
                ReDim decChartValues(intSelectedRows.Count - 1, (spdAnalytics.Sheets(0).ColumnCount - 1) - intFirstNumbCol)
                intPeriods = (spdAnalytics.Sheets(0).ColumnCount - 1) - intFirstNumbCol
                intStartCol = intFirstNumbCol
            Case Else
                For intColCtr = intFirstNumbCol To spdAnalytics.Sheets(0).ColumnCount - 1
                    If spdAnalytics.Columns(intColCtr).Visible = True Then
                        intStartCol = intColCtr
                        Exit For
                    End If
                Next
                intNumbofCols = spdAnalytics.Sheets(0).ColumnCount - intColCtr
                ReDim decChartValues(intSelectedRows.Count - 1, intNumbofCols - 1)
                intPeriods = intNumbofCols - 1
        End Select

        For intRowCtr = 0 To intSelectedRows.Count - 1
            For intColCtr = 0 To intNumbofCols - 1
                If intColCtr = 0 Then
                    If TypeOf spdAnalytics.Sheets(0).Cells(intSelectedRows(intRowCtr), intStartCol + intColCtr).CellType Is FarPoint.Web.Spread.PercentCellType Then
                        blnShowasPerc = True
                    End If
                    
                End If
                Dim decSaveVal As Decimal = spdAnalytics.Sheets(0).GetValue(intSelectedRows(intRowCtr), intStartCol + intColCtr)
                If blnShowasPerc = True Then
                    decSaveVal = decSaveVal * 100
                End If

                decChartValues(intRowCtr, intColCtr) = decSaveVal
            Next
        Next
        If CommonProcedures.CheckBalanceSize(decChartValues, intSelectedRows.Count - 1, intPeriods) = True Then
            strChartDesc = " (in thousands)"
        Else
            If blnShowasPerc = True Then
                If blnspdHasTotalCol = True Then
                    'if true then we are on Rev & Exp
                    strChartDesc = "(as % of Total Revenue)"
                Else
                    strChartDesc = "(as % of Total Asses)"
                End If
            Else
                strChartDesc = ""
            End If

        End If
        Select Case intChartType
            Case 2, 3 'bar
                BuildBarChart(intSelectedRows, spdAnalytics, spdChart, intChartType, intNumbofCols, decChartValues, intFormat, strChartDesc)
            Case 4  'Line
                If blnShowTrend = False Then
                    BuildLineChart(intSelectedRows, spdAnalytics, spdChart, intChartType, intNumbofCols, decChartValues, intFormat, strChartDesc)
                Else
                    BuildLinewTrendChart(intSelectedRows, spdAnalytics, spdChart, intNumbofCols, decChartValues, intFormat, intStartCol, blnShowasPerc, strChartDesc)
                End If
            Case 5  'Stacked line
                BuildAreaChart(intSelectedRows, spdAnalytics, spdChart, intNumbofCols, decChartValues, intFormat, strChartDesc)
            Case 6  'Pie


        End Select

    End Sub

    Private Sub BuildBarChart(ByRef intSelectedRows As ArrayList, ByVal spdAnalytics As FpSpread, ByVal spdChart As FpSpread, ByRef intChartType As Integer, ByRef intNumbofCols As Integer, ByRef decChartValues(,) As Decimal, ByRef intFormat As Integer, ByVal strChartDesc As String)

        Dim intRow As Integer = intSelectedRows(0)
        Dim intCol As Integer = 8
        Dim intColCtr As Integer
        Dim intRowCount As Integer = 1
        Dim intColCount As Integer = 0
        Dim intSelRowCtr As Integer
        Dim cseries As New FarPoint.Web.Chart.ClusteredBarSeries
        Dim sseries As New FarPoint.Web.Chart.StackedBarSeries
        Dim plotArea As New FarPoint.Web.Chart.YPlotArea()
        '    Dim yseries As New FarPoint.Web.Chart.XYPointSeries

        Dim chart As New FarPoint.Web.Spread.Chart.SpreadChart()
        Dim model As New FarPoint.Web.Chart.ChartModel()
        Dim strCatName As String

        'set title for chart
        If intSelectedRows.Count = 1 Then
            spdChart.Sheets(0).SetValue(0, 0, spdAnalytics.Sheets(0).GetValue(intSelectedRows(0), intDescCol) & strChartDesc)
        Else
            spdChart.Sheets(0).SetValue(0, 0, strChartDesc)
            'set properties for legend
            Dim legend As New FarPoint.Web.Chart.LegendArea
            SetLegendProperties(legend, intFormat, intSelectedRows.Count)
            'Dim LegFont As System.Drawing.Font
            'Select Case intFormat
            '    Case 0 To 4
            '        legend.Location = New System.Drawing.PointF(0.75F, 0.2F)
            '    Case 5, 6
            '        legend.Location = New System.Drawing.PointF(0.8F, 0.2F)
            '    Case Else
            '        legend.Location = New System.Drawing.PointF(0.75F, 0.2F)
            'End Select
            'LegFont = New System.Drawing.Font("Arial", 8)
            ' legend.TextFont = LegFont
            model.LegendAreas.Add(legend)
            chart.Model.LegendAreas.Add(legend)
        End If
        For intSelRowCtr = 0 To intSelectedRows.Count - 1
            Dim aSeries(intSelRowCtr) As FarPoint.Web.Chart.BarSeries
            aSeries(intSelRowCtr) = New FarPoint.Web.Chart.BarSeries
            Select Case intSelRowCtr
                Case 0
                    aSeries(intSelRowCtr).BarFill = New FarPoint.Web.Chart.SolidFill(System.Drawing.Color.LightGreen)
                Case 1
                    aSeries(intSelRowCtr).BarFill = New FarPoint.Web.Chart.SolidFill(System.Drawing.Color.LightCoral)
            End Select
            With aSeries(intSelRowCtr)
                strCatName = spdAnalytics.Sheets(0).GetValue(intSelectedRows(intSelRowCtr), intDescCol)
                .SeriesName = FormatString(strCatName, intSelectedRows.Count)
                Select Case intFormat
                    Case 7, 8
                        intColCtr = 0
                        Dim intIndex As Integer = 0
                        Do While intColCtr <= ((spdAnalytics.Sheets(0).ColumnCount - 1) - intFirstNumberCol)
                            If spdAnalytics.Sheets(0).Columns(intColCtr + intFirstNumberCol).Visible = True Then
                                .Values.Add(decChartValues(intSelRowCtr, intIndex))
                                If intSelRowCtr = 0 Then
                                    Dim strXLabel As String = spdAnalytics.Sheets(0).ColumnHeader.Cells(1, intFirstNumberCol + intColCtr).Text
                                    .CategoryNames.Add(spdAnalytics.Sheets(0).ColumnHeader.Cells(1, intFirstNumberCol + intColCtr).Text)
                                End If
                                intIndex += 1
                                If intSelRowCtr = 0 Then
                                    intColCount += 1
                                End If
                            End If
                            intColCtr += 1
                        Loop
                    Case Else
                        For intColCtr = 0 To intNumbofCols - 1
                            .Values.Add(decChartValues(intSelRowCtr, intColCtr))
                            If intSelRowCtr = 0 Then
                                .CategoryNames.Add(spdAnalytics.Sheets(0).ColumnHeader.Cells(1, intFirstNumberCol + intColCtr).Text)
                            End If
                        Next
                End Select
            End With
            Select Case intChartType
                Case 2
                    cseries.Series.Add(aSeries(intSelRowCtr))
                Case 3
                    sseries.Series.Add(aSeries(intSelRowCtr))
            End Select
        Next
        'set properties of plot area
        SetPlotAreaProperties(plotArea, intFormat, intSelectedRows.Count, intColCount)

        Select Case intChartType
            Case 2
                plotArea.Series.Add(cseries)
            Case 3
                plotArea.Series.Add(sseries)
        End Select
        model.PlotAreas.Add(plotArea)
        chart.Model = model
        SetChartProperties(chart, spdAnalytics.Width.Value)
        spdChart.Sheets(0).Charts.Add(chart)


    End Sub

    Private Sub BuildLineChart(ByRef intSelectedRows As ArrayList, ByVal spdAnalytics As FpSpread, ByVal spdChart As FpSpread, ByRef intChartType As Integer, ByRef intNumbofCols As Integer, ByRef decChartValues(,) As Decimal, ByRef intFormat As Integer, ByVal strChartDesc As String)

        Dim intRow As Integer = intSelectedRows(0)
        Dim intCol As Integer = 8
        Dim intColCtr As Integer
        Dim intRowCount As Integer = 1
        Dim intColCount As Integer = 0
        Dim intSelRowCtr As Integer
        Dim plotArea As New FarPoint.Web.Chart.YPlotArea()
        Dim blnSmoothed As Boolean = True
        Dim sseries As New FarPoint.Web.Chart.StackedLineSeries
        Dim lseries As New FarPoint.Web.Chart.LineSeries
        Dim chart As New FarPoint.Web.Spread.Chart.SpreadChart()
        Dim model As New FarPoint.Web.Chart.ChartModel()
        Dim strCatName As String


        'set title for chart
        If intSelectedRows.Count = 1 Then
            spdChart.Sheets(0).SetValue(0, 0, spdAnalytics.Sheets(0).GetValue(intSelectedRows(0), intDescCol) & strChartDesc)
            If blnSmoothed = True Then
                lseries.SmoothedLine = True
            End If
            With lseries
                lseries.LineBorder = New FarPoint.Web.Chart.SolidLine(Drawing.Color.Blue, 0.5F)
                lseries.PointMarker = New FarPoint.Web.Chart.BuiltinMarker(FarPoint.Web.Chart.MarkerShape.Diamond, 3.5F)
                Select Case intFormat
                    Case 7, 8
                        intColCtr = 0
                        Dim intIndex As Integer = 0
                        Do While intColCtr <= ((spdAnalytics.Sheets(0).ColumnCount - 1) - intFirstNumberCol)
                            If intSelRowCtr = 0 Then
                                If spdAnalytics.Sheets(0).Columns(intColCtr + intFirstNumberCol).Visible = True Then
                                    .Values.Add(decChartValues(intSelRowCtr, intIndex))
                                    Dim strXLabel As String = spdAnalytics.Sheets(0).ColumnHeader.Cells(1, intFirstNumberCol + intColCtr).Text
                                    .CategoryNames.Add(spdAnalytics.Sheets(0).ColumnHeader.Cells(1, intFirstNumberCol + intColCtr).Text)
                                    intIndex += 1
                                    intColCount += 1
                                End If

                            End If
                            intColCtr += 1
                        Loop
                    Case Else
                        For intColCtr = 0 To intNumbofCols - 1
                            .Values.Add(decChartValues(intSelRowCtr, intColCtr))
                            If intSelRowCtr = 0 Then
                                If spdAnalytics.Sheets(0).Columns(intColCtr + intFirstNumberCol).Visible = True Then
                                    .CategoryNames.Add(spdAnalytics.Sheets(0).ColumnHeader.Cells(1, intFirstNumberCol + intColCtr).Text)
                                End If
                            End If
                        Next

                End Select
            End With
            plotArea.Series.Add(lseries)
        Else
            spdChart.Sheets(0).SetValue(0, 0, strChartDesc)
            Dim legend As New FarPoint.Web.Chart.LegendArea
            SetLegendProperties(legend, intFormat, intSelectedRows.Count)
            model.LegendAreas.Add(legend)
            chart.Model.LegendAreas.Add(legend)
            For intSelRowCtr = 0 To intSelectedRows.Count - 1
                Dim aSeries(intSelRowCtr) As FarPoint.Web.Chart.LineSeries
                aSeries(intSelRowCtr) = New FarPoint.Web.Chart.LineSeries
                aSeries(intSelRowCtr).SmoothedLine = True
                Select Case intSelRowCtr
                    Case 0
                        aSeries(intSelRowCtr).LineBorder = New FarPoint.Web.Chart.SolidLine(Drawing.Color.Blue, 0.5F)
                        aSeries(intSelRowCtr).PointMarker = New FarPoint.Web.Chart.BuiltinMarker(FarPoint.Web.Chart.MarkerShape.Circle, 3.5F)
                    Case 1
                        aSeries(intSelRowCtr).LineBorder = New FarPoint.Web.Chart.SolidLine(Drawing.Color.Red, 0.5F)
                        aSeries(intSelRowCtr).PointMarker = New FarPoint.Web.Chart.BuiltinMarker(FarPoint.Web.Chart.MarkerShape.Circle, 3.5F)
                    Case 2
                        aSeries(intSelRowCtr).LineBorder = New FarPoint.Web.Chart.SolidLine(Drawing.Color.Green, 0.5F)
                        aSeries(intSelRowCtr).PointMarker = New FarPoint.Web.Chart.BuiltinMarker(FarPoint.Web.Chart.MarkerShape.Circle, 3.5F)
                    Case 3
                        aSeries(intSelRowCtr).LineBorder = New FarPoint.Web.Chart.SolidLine(Drawing.Color.Gold, 0.5F)
                        aSeries(intSelRowCtr).PointMarker = New FarPoint.Web.Chart.BuiltinMarker(FarPoint.Web.Chart.MarkerShape.Circle, 3.5F)
                    Case 4
                        aSeries(intSelRowCtr).LineBorder = New FarPoint.Web.Chart.SolidLine(Drawing.Color.Lavender, 0.5F)
                        aSeries(intSelRowCtr).PointMarker = New FarPoint.Web.Chart.BuiltinMarker(FarPoint.Web.Chart.MarkerShape.Circle, 3.5F)
                    Case 5
                        aSeries(intSelRowCtr).LineBorder = New FarPoint.Web.Chart.SolidLine(Drawing.Color.LightCoral, 0.5F)
                        aSeries(intSelRowCtr).PointMarker = New FarPoint.Web.Chart.BuiltinMarker(FarPoint.Web.Chart.MarkerShape.Circle, 3.5F)
                    Case 6
                        aSeries(intSelRowCtr).LineBorder = New FarPoint.Web.Chart.SolidLine(Drawing.Color.LightSeaGreen, 0.5F)
                        aSeries(intSelRowCtr).PointMarker = New FarPoint.Web.Chart.BuiltinMarker(FarPoint.Web.Chart.MarkerShape.Circle, 3.5F)
                    Case 7
                        aSeries(intSelRowCtr).LineBorder = New FarPoint.Web.Chart.SolidLine(Drawing.Color.Magenta, 0.5F)
                        aSeries(intSelRowCtr).PointMarker = New FarPoint.Web.Chart.BuiltinMarker(FarPoint.Web.Chart.MarkerShape.Circle, 3.5F)
                    Case 8
                        aSeries(intSelRowCtr).LineBorder = New FarPoint.Web.Chart.SolidLine(Drawing.Color.LightSeaGreen, 0.5F)
                        aSeries(intSelRowCtr).PointMarker = New FarPoint.Web.Chart.BuiltinMarker(FarPoint.Web.Chart.MarkerShape.Circle, 3.5F)
                    Case 9
                        aSeries(intSelRowCtr).LineBorder = New FarPoint.Web.Chart.SolidLine(Drawing.Color.Magenta, 0.5F)
                        aSeries(intSelRowCtr).PointMarker = New FarPoint.Web.Chart.BuiltinMarker(FarPoint.Web.Chart.MarkerShape.Circle, 3.5F)

                End Select
                With aSeries(intSelRowCtr)
                    strCatName = spdAnalytics.Sheets(0).GetValue(intSelectedRows(intSelRowCtr), intDescCol)
                    .SeriesName = FormatString(strCatName, intSelectedRows.Count)
                    Select Case intFormat
                        Case 7, 8
                            intColCtr = 0
                            Dim intIndex As Integer = 0
                            Do While intColCtr <= ((spdAnalytics.Sheets(0).ColumnCount - 1) - intFirstNumberCol)
                                If spdAnalytics.Sheets(0).Columns(intColCtr + intFirstNumberCol).Visible = True Then
                                    .Values.Add(decChartValues(intSelRowCtr, intIndex))
                                    If intSelRowCtr = 0 Then
                                        Dim strXLabel As String = spdAnalytics.Sheets(0).ColumnHeader.Cells(1, intFirstNumberCol + intColCtr).Text
                                        .CategoryNames.Add(spdAnalytics.Sheets(0).ColumnHeader.Cells(1, intFirstNumberCol + intColCtr).Text)
                                    End If
                                    intIndex += 1
                                    If intSelRowCtr = 0 Then
                                        intColCount += 1
                                    End If
                                End If
                                intColCtr += 1

                            Loop
                        Case Else
                            For intColCtr = 0 To intNumbofCols - 1
                                .Values.Add(decChartValues(intSelRowCtr, intColCtr))
                                If intSelRowCtr = 0 Then
                                    .CategoryNames.Add(spdAnalytics.Sheets(0).ColumnHeader.Cells(1, intFirstNumberCol + intColCtr).Text)
                                End If
                            Next
                    End Select
                End With
                '  sseries.Series.Add(aSeries(intSelRowCtr))
                plotArea.Series.Add(aSeries(intSelRowCtr))
            Next
        End If
        'set properties of plot area

        SetPlotAreaProperties(plotArea, intFormat, intSelectedRows.Count, intColCount)
        'If intSelectedRows.Count = 1 Then
        '    plotArea.Series.Add(lseries)
        'Else
        '    '      plotArea.Series.Add(sseries)

        'End If
        model.PlotAreas.Add(plotArea)
        chart.Model = model
        SetChartProperties(chart, spdAnalytics.Width.Value)
        spdChart.Sheets(0).Charts.Add(chart)

    End Sub
    Private Sub BuildLinewTrendChart(ByRef intSelectedRows As ArrayList, ByVal spdAnalytics As FpSpread, ByVal spdChart As FpSpread, ByRef intNumbofCols As Integer, ByVal decChartValues(,) As Decimal, ByVal intFormat As Integer, ByVal intFirstNumCol As Integer, ByVal blnShowasPerc As Boolean, ByVal strChartDesc As String)

        Dim intRow As Integer = intSelectedRows(0)
        Dim intCol As Integer = 8
        Dim intColCtr As Integer
        Dim intRowCount As Integer = 1
        Dim intColCount As Integer = 0
        Dim intSelRowCtr As Integer
        Dim decIntercept As Decimal = 0
        Dim decSlope As Decimal = 0
        Dim dblCoeff As Double = 0
        Dim blnSmoothed As Boolean = True
        '' Dim cseries As New FarPoint.Web.Chart.cl
        Dim sseries As New FarPoint.Web.Chart.LineSeries
        Dim tseries As New FarPoint.Web.Chart.LineSeries
        Dim plotArea As New FarPoint.Web.Chart.YPlotArea()
        Dim chart As New FarPoint.Web.Spread.Chart.SpreadChart()
        Dim model As New FarPoint.Web.Chart.ChartModel()
        Dim intWidth As Integer = spdAnalytics.Width.Value
        'set title for chart
        ' If intSelectedRows.Count = 1 Then
        '   Dim label As New FarPoint.Web.Chart.LabelArea
        With spdChart.Sheets(0)
            If blnShowasPerc = False Then
                .SetValue(0, 0, spdAnalytics.Sheets(0).GetValue(intSelectedRows(0), intDescCol) & strChartDesc)
            Else
                .SetValue(0, 0, spdAnalytics.Sheets(0).GetValue(intSelectedRows(0), intDescCol) & " as a % of total revenue")
            End If
        End With
        Dim LabFont As System.Drawing.Font
        LabFont = New System.Drawing.Font("Arial", 10, Drawing.FontStyle.Bold)
        If blnSmoothed = True Then
            sseries.SmoothedLine = True
        End If
        tseries.PointMarker = New FarPoint.Web.Chart.NoMarker()
        sseries.PointMarker = New FarPoint.Web.Chart.BuiltinMarker(FarPoint.Web.Chart.MarkerShape.Circle, 4.0F)
        sseries.LineBorder = New FarPoint.Web.Chart.SolidLine(Drawing.Color.Blue, 0.5F)
        tseries.LineBorder = New FarPoint.Web.Chart.SolidLine(Drawing.Color.Red, 0.5F)
        'set properties for legend
        Dim legend As New FarPoint.Web.Chart.LegendArea
        SetLegendProperties(legend, intFormat, 2)
        model.LegendAreas.Add(legend)
        chart.Model.LegendAreas.Add(legend)
        With sseries
            .SeriesName = "Actual"
            Select Case intFormat
                Case 7, 8
                    intColCtr = 0
                    Dim intIndex As Integer = 0
                    Do While intColCtr <= ((spdAnalytics.Sheets(0).ColumnCount - 1) - intFirstNumberCol)
                        If intSelRowCtr = 0 Then
                            If spdAnalytics.Sheets(0).Columns(intColCtr + intFirstNumberCol).Visible = True Then
                                .Values.Add(decChartValues(intSelRowCtr, intIndex))
                                Dim strXLabel As String = spdAnalytics.Sheets(0).ColumnHeader.Cells(1, intFirstNumberCol + intColCtr).Text
                                .CategoryNames.Add(spdAnalytics.Sheets(0).ColumnHeader.Cells(1, intFirstNumberCol + intColCtr).Text)
                                intIndex += 1
                                If intSelRowCtr = 0 Then
                                    intColCount += 1
                                End If
                            End If

                        End If
                        intColCtr += 1
                    Loop
                Case Else
                    For intColCtr = 0 To intNumbofCols - 1
                        .Values.Add(decChartValues(intSelRowCtr, intColCtr))
                        If intSelRowCtr = 0 Then
                            .CategoryNames.Add(spdAnalytics.Sheets(0).ColumnHeader.Cells(1, intFirstNumCol + intColCtr).Text)
                        End If
                    Next
            End Select

        End With
        'get trend amounts
        ComputeTrend(decIntercept, decSlope, dblCoeff, decChartValues, intNumbofCols)
        With tseries
            .SeriesName = "Trend"
            For intColCtr = 0 To intNumbofCols - 1
                .Values.Add(decIntercept + (intColCtr * decSlope))
            Next
        End With

        Dim slope As New FarPoint.Web.Chart.LabelArea
        Dim strText As String
        If decSlope > 0 Then
            strText = "Trend - Increasing at rate"
        Else
            strText = "Trend - Decreasing at rate"
        End If
        If blnShowasPerc = False Then
            strText = strText & vbCrLf & "of " & Math.Round(decSlope, 0)
            Select Case intFormat
                Case 0, 2, 3, 5, 5
                    strText = strText & " per month"
                Case Else
                    strText = strText & " per year"
            End Select
        Else
            strText = strText & vbCrLf & "of " & Math.Round(decSlope * 100, 2)
            Select Case intFormat
                Case 0, 2, 3, 5, 5
                    strText = strText & "% per month"
                Case Else
                    strText = strText & "% per year"
            End Select
        End If
        slope.Text = strText
        slope.Location = New System.Drawing.PointF(0.01F, 0.6F)
        'Select Case intFormat
        '    Case 0 To 4
        '        slope.Location = New System.Drawing.PointF(0.1F, 0.6F)
        '    Case 5, 6
        '        slope.Location = New System.Drawing.PointF(0.1F, 0.6F)
        '    Case Else
        '        slope.Location = New System.Drawing.PointF(0.1F, 0.6F)
        'End Select
        LabFont = New System.Drawing.Font("Arial", 8)
        slope.TextFont = LabFont
        model.LabelAreas.Add(slope)
        SetPlotAreaProperties(plotArea, intFormat, 2, intColCount)
        plotArea.Series.Add(sseries)
        plotArea.Series.Add(tseries)
        model.PlotAreas.Add(plotArea)
        chart.Model = model
        SetChartProperties(chart, spdAnalytics.Width.Value)
        spdChart.Sheets(0).Charts.Add(chart)

    End Sub
    Private Sub BuildAreaChart(ByRef intSelectedRows As ArrayList, ByVal spdAnalytics As FpSpread, ByVal spdChart As FpSpread, ByRef intNumbofCols As Integer, ByRef decChartValues(,) As Decimal, ByRef intFormat As Integer, ByVal strChartDesc As String)

        Dim intRow As Integer = intSelectedRows(0)
        Dim intCol As Integer = 8
        Dim intColCtr As Integer
        Dim intRowCount As Integer = 1
        Dim intColCount As Integer = 0
        Dim intSelRowCtr As Integer
        Dim areaseries As New FarPoint.Web.Chart.StackedAreaSeries
        Dim plotArea As New FarPoint.Web.Chart.YPlotArea()
        Dim chart As New FarPoint.Web.Spread.Chart.SpreadChart()
        Dim model As New FarPoint.Web.Chart.ChartModel()
        Dim strCatName As String

        'set title for chart
        If intSelectedRows.Count = 1 Then
            spdChart.Sheets(0).SetValue(0, 0, spdAnalytics.Sheets(0).GetValue(intSelectedRows(0), intDescCol) & strChartDesc)
        Else
            spdChart.Sheets(0).SetValue(0, 0, strChartDesc)
            'set properties for legend
            Dim legend As New FarPoint.Web.Chart.LegendArea
            SetLegendProperties(legend, intFormat, intSelectedRows.Count)
            'Dim LegFont As System.Drawing.Font
            'Select Case intFormat
            '    Case 0 To 4
            '        legend.Location = New System.Drawing.PointF(0.75F, 0.2F)
            '    Case 5, 6
            '        legend.Location = New System.Drawing.PointF(0.8F, 0.2F)
            '    Case Else
            '        legend.Location = New System.Drawing.PointF(0.75F, 0.2F)
            'End Select

            'LegFont = New System.Drawing.Font("Arial", 8)
            'legend.TextFont = LegFont
            model.LegendAreas.Add(legend)
            chart.Model.LegendAreas.Add(legend)
        End If
        For intSelRowCtr = 0 To intSelectedRows.Count - 1
            Dim aSeries(intSelRowCtr) As FarPoint.Web.Chart.AreaSeries
            aSeries(intSelRowCtr) = New FarPoint.Web.Chart.AreaSeries
            With aSeries(intSelRowCtr)
                strCatName = spdAnalytics.Sheets(0).GetValue(intSelectedRows(intSelRowCtr), intDescCol)
                .SeriesName = FormatString(strCatName, intSelectedRows.Count)
                Select Case intFormat
                    Case 7, 8
                        intColCtr = 0
                        Dim intIndex As Integer = 0
                        Do While intColCtr <= ((spdAnalytics.Sheets(0).ColumnCount - 1) - intFirstNumberCol)
                            If spdAnalytics.Sheets(0).Columns(intColCtr + intFirstNumberCol).Visible = True Then
                                .Values.Add(decChartValues(intSelRowCtr, intIndex))
                                If intSelRowCtr = 0 Then
                                    Dim strXLabel As String = spdAnalytics.Sheets(0).ColumnHeader.Cells(1, intFirstNumberCol + intColCtr).Text
                                    .CategoryNames.Add(spdAnalytics.Sheets(0).ColumnHeader.Cells(1, intFirstNumberCol + intColCtr).Text)
                                End If
                                intIndex += 1
                                If intSelRowCtr = 0 Then
                                    intColCount += 1
                                End If
                            End If
                            intColCtr += 1
                        Loop
                    Case Else
                        For intColCtr = 0 To intNumbofCols - 1
                            .Values.Add(decChartValues(intSelRowCtr, intColCtr))
                            If intSelRowCtr = 0 Then
                                .CategoryNames.Add(spdAnalytics.Sheets(0).ColumnHeader.Cells(1, intFirstNumberCol + intColCtr).Text)
                            End If
                        Next
                End Select
            End With
            areaseries.Series.Add(aSeries(intSelRowCtr))
        Next
        SetPlotAreaProperties(plotArea, intFormat, intSelectedRows.Count, intColCount)
        'set properties of plot area
        'plotArea.Location = New System.Drawing.PointF(0.125F, 0.2F)
        'If intSelectedRows.Count = 1 Then
        '    plotArea.Size = New System.Drawing.SizeF(0.8F, 0.6F)
        'Else
        '    Select Case intFormat
        '        Case 0 To 4
        '            plotArea.Size = New System.Drawing.SizeF(0.6F, 0.6F)
        '        Case 5, 6
        '            plotArea.Location = New System.Drawing.PointF(0.05F, 0.2F)
        '            plotArea.Size = New System.Drawing.SizeF(0.75F, 0.6F)
        '        Case Else
        '            plotArea.Size = New System.Drawing.SizeF(0.6F, 0.6F)
        '    End Select

        'End If
        plotArea.Series.Add(areaseries)
        model.PlotAreas.Add(plotArea)
        chart.Model = model
        SetChartProperties(chart, spdAnalytics.Width.Value)
        spdChart.Sheets(0).Charts.Add(chart)


    End Sub

    Private Sub ComputeTrend(ByRef decIntercept As Decimal, ByRef decSlope As Decimal, ByRef dblCoeff As Double, ByVal decValues(,) As Decimal, intNumbColCount As Integer)
        Dim intDataPoints As Integer = intNumbColCount
        Dim XFactors(intNumbColCount - 1) As Decimal
        Dim YFactors(intNumbColCount - 1) As Decimal
        Dim intCtr As Integer
        Dim sx, sy, sxy, sx2, sy2, XX, YY, temp As Single

        'load values into arrays
        For intCtr = 0 To intNumbColCount - 1
            XFactors(intCtr) = intCtr
            YFactors(intCtr) = decValues(0, intCtr)
        Next
        'compute slope, intercept and coeff
        For intCtr = 0 To intDataPoints - 1
            XX = XFactors(intCtr)
            YY = YFactors(intCtr)
            sx += XX
            sy += YY
            sxy = sxy + XX * YY
            sx2 = sx2 + XX * XX
            sy2 = sy2 + YY * YY
        Next
        If (intDataPoints * sx2 - sx * sx) <> 0 Then
            decSlope = (intDataPoints * sxy - sx * sy) / (intDataPoints * sx2 - sx * sx)
        Else
            decSlope = 0
        End If
        If intDataPoints <> 0 Then
            decIntercept = (sy - decSlope * sx) / intDataPoints
        Else
            decIntercept = 0
        End If
        temp = (intDataPoints * sx2 - sx * sx) * (intDataPoints * sy2 - sy * sy)
        If temp <> 0 Then
            dblCoeff = (intDataPoints * sxy - sx * sy) / Math.Sqrt(temp)
        End If

    End Sub
    Private Sub SetChartProperties(ByRef chart As FarPoint.Web.Spread.Chart.SpreadChart, intWidth As Integer)
        chart.Width = intWidth
        chart.Height = 180
        chart.Left = 0
        chart.Top = 20
        chart.BorderColor = Drawing.Color.FromArgb(248, 248, 248)
        chart.CanMove = False
        chart.CanSelect = False
        chart.Model.Fill = New FarPoint.Web.Chart.SolidFill(Drawing.Color.FromArgb(248, 248, 248))
    End Sub
    Private Sub SetPlotAreaProperties(ByRef plotarea As FarPoint.Web.Chart.YPlotArea, ByVal intFormat As Integer, ByVal intRowCount As Integer, ByVal intColCount As Integer)
        ''set properties of plot area
        Dim plotFont As System.Drawing.Font
        plotFont = New System.Drawing.Font("Arial", 8)

        plotarea.XAxis.LabelTextFont = plotFont
        plotarea.YAxes(0).LabelTextFont = plotFont
        'If intRowCount = 1 Then
        '    plotarea.Location = New System.Drawing.PointF(0.3F, 0.2F)
        '    plotarea.Size = New System.Drawing.SizeF(0.7F, 0.6F)
        'Else

        Select Case intFormat
            Case 0 To 4
                plotarea.Location = New System.Drawing.PointF(0.3F, 0.075F)
                plotarea.Size = New System.Drawing.SizeF(0.6F, 0.75F)
            Case 5, 6
                plotarea.Location = New System.Drawing.PointF(0.225F, 0.075F)
                plotarea.Size = New System.Drawing.SizeF(0.7F, 0.75F)
            Case Else
                Select Case intColCount
                    Case 2, 3
                        plotarea.Location = New System.Drawing.PointF(0.4F, 0.075F)
                        plotarea.Size = New System.Drawing.SizeF(0.5F, 0.75F)
                    Case Else
                        plotarea.Location = New System.Drawing.PointF(0.3F, 0.075F)
                        plotarea.Size = New System.Drawing.SizeF(0.6F, 0.75F)
                End Select

        End Select
        '  End If
        plotarea.YAxes(0).LabelNumberFormat = "#,##0"
    End Sub

    Private Sub SetLegendProperties(ByRef legend As FarPoint.Web.Chart.LegendArea, ByVal intFormat As Integer, intItemCount As Integer)
        Dim LegFont As System.Drawing.Font
        LegFont = New System.Drawing.Font("Arial", 7)
        legend.TextFont = LegFont

        Select Case intItemCount
            Case 0 To 4
                legend.Location = New System.Drawing.PointF(0.005F, 0.2F)
            Case Else
                legend.Location = New System.Drawing.PointF(0.005F, 0.0F)
        End Select

    End Sub
    Private Function FormatString(strCatName As String, intItemCount As Integer) As String
        Dim intSpacePos As Integer = 0
        Dim intPrePos As Integer

        FormatString = ""
        If Len(strCatName) > 27 Then
            Do While intSpacePos < 28
                intPrePos = intSpacePos
                intSpacePos = InStr(intSpacePos + 1, strCatName, " ")
                If intSpacePos = 0 Then
                    Exit Do
                End If
            Loop
            If intItemCount > 5 Then
                FormatString = Trim(Left(strCatName, intPrePos)) & "..."
            Else
                FormatString = Trim(Left(strCatName, intPrePos)) & vbCrLf & Left(Trim(Right(strCatName, Len(strCatName) - intPrePos)), 27)
            End If
        Else
            FormatString = strCatName
        End If


    End Function

End Module
