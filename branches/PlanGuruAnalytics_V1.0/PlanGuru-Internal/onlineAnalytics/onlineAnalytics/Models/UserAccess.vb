﻿Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel
Imports System.Collections.Generic
Imports System.Data.Entity


Public Class CustAuthFilter
    Inherits AuthorizeAttribute

    Private UserType As UserRole
    Private UserInfo As User
    Private CustomerId As String = Nothing
    Private ua As UserAccess

    Public Overrides Sub OnAuthorization(filterContext As AuthorizationContext)
        'filterContext.Controller.ViewBag.AutherizationMessage = "Custom Authorization: Message from OnAuthorization method."

        Dim controller As String = filterContext.RouteData.Values.Item("controller").ToString()
        Dim action As String = filterContext.RouteData.Values.Item("action").ToString()

        If (Not filterContext.HttpContext.Session("UserType") Is Nothing) And (Not filterContext.HttpContext.Session("UserInfo") Is Nothing) And (Not filterContext.HttpContext.Session("UserAccess") Is Nothing) Then
            UserType = DirectCast(filterContext.HttpContext.Session("UserType"), UserRole)
            UserInfo = DirectCast(filterContext.HttpContext.Session("UserInfo"), User)
            ua = DirectCast(filterContext.HttpContext.Session("UserAccess"), UserAccess)
            CustomerId = If(UserType.UserRoleId = UserRoles.SAU, UserInfo.CustomerId, Nothing)
            Dim isAuthorize As Boolean = True

            If (action = "SearchCustomer" And controller = "Subscribe" And ua.SearchCustomer = False) Then
                isAuthorize = False
            ElseIf (action = "Index" And controller = "Subscribe" And ua.ViewSubscription = False) Then
                isAuthorize = False
            ElseIf (action = "Subscribe" And controller = "Subscribe" And ua.AddSubscription = False) Then
                isAuthorize = False
            ElseIf (action = "EditSubscription" And controller = "Subscribe" And ua.UpdateSubscription = False) Then
                isAuthorize = False
            ElseIf (action = "CancelSubscription" And controller = "Subscribe" And ua.CancelSubscription = False) Then
                isAuthorize = False
            ElseIf (action = "CreateUser" And controller = "User" And ua.AddUser = False) Then
                isAuthorize = False
            ElseIf (action = "Delete" And controller = "User" And ua.DeleteUser = False) Then
                isAuthorize = False
            ElseIf (action = "Index" And controller = "User" And ua.ViewUser = False) Then
                isAuthorize = False
            ElseIf (action = "Edit" And controller = "User" And ua.UpdateUser = False) Then
                isAuthorize = False
                'ElseIf (action = "EditProfile" And controller = "User" And ua.UpdateUser = False) Then
                '    isAuthorize = False
            ElseIf (action = "Create" And controller = "Company" And ua.AddCompany = False) Then
                isAuthorize = False
            ElseIf (action = "Delete" And controller = "Company" And ua.DeleteCompany = False) Then
                isAuthorize = False
            ElseIf (action = "Index" And controller = "Company" And ua.ViewCompanies = False) Then
                isAuthorize = False
            ElseIf (action = "Edit" And controller = "Company" And ua.UpdateCompany = False) Then
                isAuthorize = False
            ElseIf (action = "Create" And controller = "Analysis" And ua.AddAnalysis = False) Then
                isAuthorize = False
            ElseIf (action = "Delete" And controller = "Analysis" And ua.DeleteAnalysis = False) Then
                isAuthorize = False
            ElseIf (action = "Index" And controller = "Analysis" And ua.ViewAnalyses = False) Then
                isAuthorize = False
            ElseIf (action = "Edit" And controller = "Analysis" And ua.UpdateAnalysis = False) Then
                isAuthorize = False
            End If

            If (isAuthorize = False) Then
                Dim url = New UrlHelper(filterContext.RequestContext)
                Dim verifyEmailUrl = url.Action("Index", "Dashboard", Nothing)
                filterContext.Result = New RedirectResult(verifyEmailUrl)
            End If

        End If

    End Sub
End Class

Public Class CustomActionFilter
    Inherits ActionFilterAttribute


    Public Overrides Sub OnActionExecuting(filterContext As ActionExecutingContext)

        Dim ctx As HttpContext = HttpContext.Current

        ' check if session is supported
        If ctx.Session IsNot Nothing Then

            ' check if a new session id was generated
            If ctx.Session.IsNewSession Then

                ' If it says it is a new session, but an existing cookie exists, then it must
                ' have timed out
                Dim sessionCookie As String = ctx.Request.Headers("Cookie")
                If (sessionCookie IsNot Nothing) AndAlso (sessionCookie.IndexOf("ASP.NET_SessionId") >= 0) Then

                    filterContext.HttpContext.Session.Clear()
                    filterContext.HttpContext.Session.Abandon()
                    'Dim TempData = New System.Web.Mvc.TempDataDictionary()                    
                    'TempData("ErrorMessage") = String.Concat("Session Expired, Please login again.")

                    ctx.Response.Redirect("~/Home/Index?session=-1")
                ElseIf (sessionCookie IsNot Nothing) AndAlso (sessionCookie.IndexOf("ASP.NET_SessionId") = "-1") Then

                    ctx.Response.Redirect("~/Home/Index")

                End If
            End If
        End If

        MyBase.OnActionExecuting(filterContext)

    End Sub


End Class

Public Class UserAccess

    Public Property SubscriptionManagement() As Boolean = False
    Public Property UserManagement() As Boolean = False
    Public Property SearchCustomer() As Boolean = False
    Public Property ViewSubscription() As Boolean = False
    Public Property AddSubscription() As Boolean = False
    Public Property UpdateSubscription() As Boolean = False
    Public Property CancelSubscription() As Boolean = False
    Public Property AddUser() As Boolean = False
    Public Property DeleteUser() As Boolean = False
    Public Property ViewUser() As Boolean = False
    Public Property UpdateUser() As Boolean = False
    Public Property AddCompany() As Boolean = False
    Public Property DeleteCompany() As Boolean = False
    Public Property ViewCompanies() As Boolean = False
    Public Property UpdateCompany() As Boolean = False
    Public Property UserCompanyMapping() As Boolean = False
    Public Property AddAnalysis() As Boolean = False
    Public Property DeleteAnalysis() As Boolean = False
    Public Property ViewAnalyses() As Boolean = False
    Public Property UpdateAnalysis() As Boolean = False
    Public Property UserAnalysisMapping() As Boolean = False
    Public Property ViewAnalysisInfo() As Boolean = False
    Public Property PrintAnalysisInfo() As Boolean = False
    Public Property ModifyDashboard() As Boolean = False


End Class
