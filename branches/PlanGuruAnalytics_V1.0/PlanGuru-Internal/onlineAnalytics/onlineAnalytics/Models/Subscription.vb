﻿Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel
Imports System.Collections.Generic
Imports System.Data.Entity
Imports Recurly

Public Class Customer

    <DatabaseGenerated(DatabaseGeneratedOption.None)>
    Public Property CustomerId() As Integer

    '<Required(ErrorMessage:="Customer First Name is required.")> _
    <StringLength(50)> _
    <Display(Name:="Customer First Name")> _
    Public Property CustomerFirstName() As String

    '<Required(ErrorMessage:="Customer Last Name is required.")> _
    <StringLength(50)> _
    <Display(Name:="Customer Last Name")> _
    Public Property CustomerLastName() As String

    <StringLength(150)> _
    <Display(Name:="Customer's Company Name")> _
    Public Property CustomerCompanyName() As String

    <Display(Name:="Quantity")> _
    Public Property Quantity() As Integer

    '<Required(ErrorMessage:="Contact First Name is required.")> _
    <StringLength(50)> _
    <Display(Name:="Contact First Name")> _
    Public Property ContactFirstName() As String

    '<Required(ErrorMessage:="Contact Last Name is required.")> _
    <StringLength(50)> _
    <Display(Name:="Contact Last Name")> _
    Public Property ContactLastName() As String

    '<Required(ErrorMessage:="Customer Address1 is required.")> _
    <StringLength(100)> _
    <Display(Name:="Customer Addresss1")> _
    Public Property CustomerAddress1() As String

    <Display(Name:="Customer Address2")> _
    <StringLength(100)> _
    Public Property CustomerAddress2() As String

    '<Required(ErrorMessage:="Country is required.")> _
    <StringLength(50)> _
    <Display(Name:="Country")> _
    Public Property Country() As String

    '<Required(ErrorMessage:="State is required.")> _
    <StringLength(50)> _
    <Display(Name:="State")> _
    Public Property State() As String

    '<Required(ErrorMessage:="City is required.")> _
    <StringLength(50)> _
    <Display(Name:="City")> _
    Public Property City() As String

    '<Required(ErrorMessage:="Postal Code is required.")> _
    <StringLength(10)> _
    <Display(Name:="Postal Code")> _
    Public Property CustomerPostalCode() As String

    '<Required(ErrorMessage:="Telephone No is required.")> _
    <StringLength(15)> _
    <Display(Name:="Telephone No")> _
    <DataType(DataType.PhoneNumber)>
    Public Property ContactTelephone() As String

    '<Required(ErrorMessage:="Email is required.")> _
    '<RegularExpression("^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$", ErrorMessage:="Please enter a valid e-mail adress.")> _
    <StringLength(200)> _
    <Display(Name:="Email address")> _
    <DataType(DataType.EmailAddress)>
    Public Property CustomerEmail() As String

    '<Required(ErrorMessage:="First Name on card is required.")> _
    <Display(Name:="First Name On Card")> _
    Public Property FirstNameOnCard() As String

    '<Required(ErrorMessage:="Last Name on card is required.")> _
    <Display(Name:="Last Name On Card")> _
    Public Property LastNameOnCard() As String

    '<Required(ErrorMessage:="Credit Card Number is required.")> _
    <Display(Name:="Credit Card Number")> _
    Public Property CreditCardNumber() As String

    '<Required(ErrorMessage:="CVV is required.")> _
    <Display(Name:="CVV")> _
    Public Property CVV() As String

    Public Property ExpirationYear() As Integer

    Public Property ExpirationMonth() As Integer

    Public Property CreatedBy() As String

    Public Property CreatedOn() As DateTime = DateTime.Now()

    Public Property UpdatedBy() As String

    Public Property UpdatedOn() As DateTime = DateTime.Now()

    <NotMapped()>
    Public Property UserId() As Integer

    <NotMapped()>
    Public Property UserName() As String

    <NotMapped()>
    Public Property Status() As Integer

    <NotMapped()>
    Public Property SubscriptionStartAt() As String

    <NotMapped()>
    Public Property SubscriptionEndAt() As String

    <NotMapped()>
    Public Property SubscriptionAmount() As Double

    <Display(Name:="SAU Name")>
    <NotMapped()>
    Public Property SAUName() As String

    <Display(Name:="Customer No")>
    <NotMapped()>
    Public Property SearchCustId() As String

    <Display(Name:="Customer Name")>
    <NotMapped()>
    Public Property SearchCustName() As String

    <Display(Name:="Telephone Number")>
    <NotMapped()>
    Public Property SearchCustTelephone() As String
    'Public Overridable Property Countrycode() As CountryCode

    'Public Overridable Property Statecode() As StateCode

    Public ReadOnly Property CustomerFullName() As String
        Get
            Return String.Concat(Me.CustomerLastName, "  ", Me.CustomerFirstName)
        End Get
    End Property

    Public ReadOnly Property ContactPersonFullName() As String
        Get
            Return String.Concat(Me.ContactLastName, "  ", Me.ContactFirstName)
        End Get

    End Property
End Class

Public Class User

    <DatabaseGenerated(DatabaseGeneratedOption.None)>
    Public Property UserId As Integer

    <Required(ErrorMessage:="Username is required.")> _
    <StringLength(50)> _
    <RegularExpression("^[a-zA-Z0-9_]{5,15}$", ErrorMessage:="Username accepts min 5 and max 15 characters, it accept alphabets, numbers & underscore.")>
    <Display(Name:="Username")> _
    Public Property UserName As String

    '<Required(ErrorMessage:="Password is required.")> _
    <Display(Name:="Password")>
    Public Property Password As String

    <Required(ErrorMessage:="User First Name is required.")> _
    <StringLength(50)> _
    <Display(Name:="User First Name")> _
    Public Property FirstName As String
    <Required(ErrorMessage:="User Last Name is required.")> _
    <StringLength(50)> _
    <Display(Name:="User Last Name")> _
    Public Property LastName As String

    <Required(ErrorMessage:="User Email is required.")> _
    <StringLength(50)> _
    <RegularExpression("^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$", ErrorMessage:="Not a valid email adress.")>
    <Display(Name:="User's Email")> _
    Public Property UserEmail As String

    Public Property Status As Integer

    Public Property SecurityKey As String

    <Required(ErrorMessage:="Select User Role.")> _
    <Display(Name:="User Role")> _
    Public Property UserRoleId As Integer

    <Required(ErrorMessage:="Select Subscriber.")> _
     <Display(Name:="Subscriber Name")> _
    Public Property CustomerId As Integer

    <NotMapped()>
    Public Property AddOn As RecurlyAddOn

    Public Property AnalysisId As Integer

    <NotMapped()>
    Public Property selectedCompany As Integer

    <NotMapped()>
    Public Property fiscalMonthOfSelectedCompany As String

    Public Property CreatedBy As String

    Public Property CreatedOn As DateTime = DateTime.Now()

    Public Property UpdatedBy As String

    Public Property UpdatedOn As DateTime = DateTime.Now()

    Public Overridable Property UserRole() As UserRole
    Public Overridable Property Customer() As Customer

End Class


Public Class ResetPassword

    Public Property UserId As String
    Public Property UserRoleId As Integer

    <Required(ErrorMessage:="Username is required.")> _
    <StringLength(50)> _
     <RegularExpression("^[a-zA-Z0-9_]{5,15}$", ErrorMessage:="Username accepts min 5 and max 15 characters, it accept alphabets, numbers & underscore.")>
    <Display(Name:="Username")> _
    Public Property UserName As String

    <Required(ErrorMessage:="User First Name is required.")> _
    <StringLength(50)> _
    <Display(Name:="User First Name")> _
    Public Property FirstName As String

    <Required(ErrorMessage:="User Last Name is required.")> _
    <StringLength(50)> _
    <Display(Name:="User Last Name")> _
    Public Property LastName As String

    <Required(ErrorMessage:="User Email is required.")> _
    <StringLength(50)> _
    <RegularExpression("^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$", ErrorMessage:="Not a valid email adress.")>
    <Display(Name:="User's Email")> _
    Public Property UserEmail As String

    <Required(ErrorMessage:="Subscriber Name is required.")> _
    <Display(Name:="Subscriber Name")>
    Public Property SubscriberName As String

    Public Property ChangePassword() As Boolean

    <Display(Name:="Current Password")> _
    <DataType(DataType.Password)>
    Public Property CurrentPassword As String

    <Display(Name:="New Password")> _
    <DataType(DataType.Password)> _
    <RegularExpression("(?=^.{8,15}$)(?=.*\d)(?=.*[a-z | A-Z])(?!.*\s).*$", ErrorMessage:="Password should be minimum 8 characters, at least one number and one letter.")>
    Public Property NewPassword As String

    <Compare("NewPassword", ErrorMessage:="Confirm New Password and New Password do not match.")>
    <DataType(DataType.Password)>
    <Display(Name:="Confirm New Password")> _
    Public Property ConfirmNewPassword As String

End Class


Public Class UserVerification

    Public Property User_Id As Integer

    Public Property UserRoleId As Integer
    Public Property Status As Integer

    <Required(ErrorMessage:="New Username is required.")> _
    <StringLength(50)> _
    <RegularExpression("^[a-zA-Z0-9_]{5,15}$", ErrorMessage:="New Username accepts min 5 and max 15 characters, it accept alphabets, numbers & underscore.")>
    <Display(Name:="New Username")> _
    Public Property UserName As String

    <Required(ErrorMessage:="Temporary Password is required.")> _
    <Display(Name:="Temporary Password")> _
    <NotMapped()>
    Public Property CurrentPassword As String

    <Required(ErrorMessage:="New Password is required.")> _
    <Display(Name:="New Password")> _
    <RegularExpression("(?=^.{8,15}$)(?=.*\d)(?=.*[a-z | A-Z])(?!.*\s).*$", ErrorMessage:="Password should be minimum 8 characters, at least one number and one letter.")>
    <NotMapped()>
    Public Property NewPassword As String

    <Compare("NewPassword", ErrorMessage:="The New Password and Confirmation New Password do not match.")>
    <Required(ErrorMessage:="Confirm Password is required.")> _
    <Display(Name:="Confirm New Password")> _
    <NotMapped()>
    Public Property ConfirmNewPassword As String

End Class

Public Class Company
    Inherits User_

    Public Property CompanyId() As Integer

    <Required(ErrorMessage:="Company Name is required.")> _
    <Display(Name:="Company Name")> _
    Public Property CompanyName() As String

    '<RegularExpression("[0-9]{1,2}$", ErrorMessage:="Fiscal Month Start field accept only numeric value.")>
    '<Range(1, 12, ErrorMessage:="Fiscal Month Start must be between {1} and {2}")> _
    <Required(ErrorMessage:="Fiscal Month Start is required.")> _
    <Display(Name:="Fiscal Month Start")> _
    Public Property FiscalMonthStart() As String

    Public Property FiscalMonthName As String

    <Required(ErrorMessage:="Contact First Name is required.")> _
    <Display(Name:="Contact First Name")> _
    Public Property ContactFirstName() As String

    <Required(ErrorMessage:="Contact Last Name is required.")> _
    <Display(Name:="Contact Last Name")> _
    Public Property ContactLastName() As String

    <Required(ErrorMessage:="Contact Email is required.")> _
    <Display(Name:="Contact Email")> _
    Public Property ContactEmail() As String

    '<RegularExpression("^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage:="Please enter valid contact number.")>
    <Required(ErrorMessage:="Contact Telephone is required.")> _
    <Display(Name:="Contact Telephone")> _
    <StringLength(50, ErrorMessage:="Contact Telephone accepts maximum 15 characters.")> _
    Public Property ContactTelephone() As String

    Public Property CustomerId As Integer

    Public Property IndustryCode As Integer

    <NotMapped()>
    Public Property AvailableUsers() As IList(Of User_)

    <NotMapped()>
    Public Property SelectedUsers() As IList(Of User_)

    <NotMapped()> _
    Public Property PostedUsers() As PostedUsers

    Public Property CreatedBy() As String

    Public Property CreatedOn() As DateTime = DateTime.Now()

    Public Property UpdatedBy() As String

    Public Property UpdatedOn() As DateTime = DateTime.Now()

End Class


Public Class _CompanyEncrypted
    Inherits User_

    Public Property CompanyId() As Integer

    <Required(ErrorMessage:="Company Name is required.")> _
    <Display(Name:="Company Name")> _
    Public Property CompanyName() As String


    <Required(ErrorMessage:="Fiscal Month Start is required.")> _
    <Display(Name:="Fiscal Month Start")> _
    Public Property FiscalMonthStart() As String

    Public Property FiscalMonthName As String

    <Required(ErrorMessage:="Contact First Name is required.")> _
    <Display(Name:="Contact First Name")> _
    Public Property ContactFirstName() As String

    <Required(ErrorMessage:="Contact Last Name is required.")> _
    <Display(Name:="Contact Last Name")> _
    Public Property ContactLastName() As String

    <Required(ErrorMessage:="Contact Email is required.")> _
    <RegularExpression("^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$", ErrorMessage:="Not a valid email adress.")> _
    <Display(Name:="Contact Email")> _
    Public Property ContactEmail() As String

    '<RegularExpression("^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage:="Please enter valid contact number.")>
    <Required(ErrorMessage:="Contact Telephone is required.")> _
    <Display(Name:="Contact Telephone")> _
    <StringLength(50, ErrorMessage:="Contact Telephone accepts maximum 15 characters.")> _
    Public Property ContactTelephone() As String

    Public Property CustomerId As Integer

    Public Property IndustryCode As Integer

    Public Property AvailableUsers() As IList(Of User_)

    Public Property SelectedUsers() As IList(Of User_)

    Public Property PostedUsers() As PostedUsers

    Public Property CreatedBy() As String

    Public Property CreatedOn() As DateTime = DateTime.Now()

    Public Property UpdatedBy() As String

    Public Property UpdatedOn() As DateTime = DateTime.Now()

End Class



Public Class Companies_Analyses

    Public Property CompanyId() As Integer
    Public Property CompanyName() As String
    Public Property AnalysisId As Integer
    Public Property AnalysisName As Integer

    Public Property Company() As Company()
    Public Property Analysis() As Analysis()

End Class

<NotMapped()>
Public Class LoginModel

    <Required(ErrorMessage:="User Name is required.")> _
   <StringLength(50)> _
   <Display(Name:="User Name")> _
    Public Property UserName As String

    <Required(ErrorMessage:="Password is required.")> _
    <Display(Name:="Password")> _
    Public Property Password As String

End Class

Public Class CustomerList

    Private customer_id As Integer
    Private customer_name As String

    Public Property CustomerId() As Integer
        Get
            Return Me.customer_id
        End Get
        Set(value As Integer)
            Me.customer_id = value
        End Set
    End Property

    Public Property CustomerName() As String
        Get
            Return Me.customer_name
        End Get
        Set(value As String)
            Me.customer_name = value
        End Set
    End Property
End Class

Enum Months As Integer
    January = 1
    February = 2
    March = 3
    April = 4
    May = 5
    June = 6
    July = 7
    August = 8
    September = 9
    October = 10
    November = 11
    December = 12
End Enum

Public Class PostedUsers

    <Display(Name:="Associate User with")> _
    Public Property UserIds() As String()
End Class

Public Class User_

    Public Id As String
    Public Name As String
    Public Tag As Object
    Public IsSelected As String

    Public Sub New()

    End Sub

    Public Sub New(ByVal Id As String, ByVal Name As String, ByVal Tag As Object, IsSelected As Boolean)
        Me.Id = Id
        Me.Name = Name
        Me.Tag = Tag
        Me.IsSelected = IsSelected
    End Sub


End Class


Public Enum StatusE
    Pending = 1
    Active = 2
    Suspended = 3
    Deactivated = 4
End Enum

Public Enum EmailType
    NewSubscriptionSignup = 1 ' Subscription
    NewUserAdded = 2  ' CRU 
    NewUserSignUp = 3 ' SAU
    PasswordReset = 4 ' 
    SubcriptionCancellation = 5 ' 
    UserDeleted = 6 '
    ForgotUsername = 7
End Enum
