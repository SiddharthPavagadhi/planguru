﻿' Note: For instructions on enabling IIS6 or IIS7 classic mode, 
' visit http://go.microsoft.com/?LinkId=9394802
Imports System.Data.Entity
Imports onlineAnalytics

Public Class MvcApplication
    Inherits System.Web.HttpApplication

    Shared Sub RegisterGlobalFilters(ByVal filters As GlobalFilterCollection)
        filters.Add(New HandleErrorAttribute())       
    End Sub

    Shared Sub RegisterRoutes(ByVal routes As RouteCollection)
        routes.IgnoreRoute("{resource}.axd/{*pathInfo}")

        ' MapRoute takes the following parameters, in order:
        ' (1) Route name
        ' (2) URL with parameters
        ' (3) Parameter defaults
        routes.MapRoute( _
            "Default", _
            "{controller}/{action}/{id}", _
            New With {.controller = "Home", .action = "Index", .id = UrlParameter.Optional} _
        )

    End Sub

    Sub Application_Start()
        'Database.SetInitializer(New SampleData())
        Logger.InitLogger()
        If (System.Configuration.ConfigurationManager.AppSettings("DropDB") <> "" And System.Configuration.ConfigurationManager.AppSettings("DropDB").Equals("True".ToLower())) Then
            Database.SetInitializer(Of DataAccess)(New SampleData())
        End If
        AreaRegistration.RegisterAllAreas()
        RegisterGlobalFilters(GlobalFilters.Filters)
        RegisterRoutes(RouteTable.Routes)
    End Sub

    Sub Application_BeginRequest()
        Select Case Request.Url.Scheme
            Case "https"
                Response.AddHeader("Strict-Transport-Security", "max-age=31536000")
                Exit Select
            Case "http"
                If (System.Configuration.ConfigurationManager.AppSettings("HostedOnSSL").ToLower() = "true") Then
                    Dim path = "https://" + Request.Url.Host + Request.Url.PathAndQuery
                    Response.Status = "301 Moved Permanently"
                    Response.AddHeader("Location", path)
                    Exit Select
                End If
        End Select

    End Sub

    Sub Application_EndRequest()


    End Sub

End Class
