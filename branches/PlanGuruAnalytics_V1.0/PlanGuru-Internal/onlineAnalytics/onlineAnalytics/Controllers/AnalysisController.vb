﻿Imports System.Data.Entity
Imports PagedList
Imports onlineAnalytics


Namespace onlineAnalytics

    <CustAuthFilter()>
    Public Class AnalysisController
        Inherits System.Web.Mvc.Controller

        'Create object utility class to use data context.
        Private utility As New Utility()
        'Define parameter 
        Private analysisRepository As IAnalysisRepository
        Private userRepository As IUserRepository
        Private useranalysisMapping As IUserAnalysisMappingRepository
        Private UserType As UserRole
        Private UserInfo As User
        Private CustomerId As Integer
        Private selectedCompanyFromDropdown As Integer
        Private pageSize As Integer

        'Get configured page_size from web.config
        Public ReadOnly Property Page_Size As Integer

            Get
                If (System.Configuration.ConfigurationManager.AppSettings("PageSize") <> "") Then
                    pageSize = System.Configuration.ConfigurationManager.AppSettings("PageSize").ToString()
                Else
                    pageSize = 10
                End If
                Return Me.pageSize
            End Get
        End Property

        Public Sub New()
            Me.analysisRepository = New AnalysisRepository(New DataAccess())
            Me.userRepository = New UserRepository(New DataAccess())
        End Sub

        Public Sub New(userRepository As IUserRepository)
            Me.userRepository = userRepository
        End Sub

        Public Sub New(analysisRepository As IAnalysisRepository)
            Me.analysisRepository = analysisRepository
        End Sub

        <CustomActionFilter()>
        Function Index(page As System.Nullable(Of Integer)) As ViewResult
            Dim setupCountObj As SetupCount
            Try
                If (Not Session("UserType") Is Nothing) And (Not Session("UserInfo") Is Nothing) Then
                    UserType = DirectCast(Session("UserType"), UserRole)
                    UserInfo = DirectCast(Session("UserInfo"), User)
                    CustomerId = If(UserType.UserRoleId > UserRoles.PGASupport, UserInfo.CustomerId, Nothing)
                    selectedCompanyFromDropdown = Session("SelectedCompanyFromDropdown")
                End If

                setupCountObj = utility.CheckRequiredSetupCount(UserInfo)
                ViewBag.Setup = setupCountObj

                Dim analysis As IEnumerable(Of Analysis) = Enumerable.Empty(Of Analysis)()

                If ((UserType.UserRoleId = UserRoles.SAU Or UserType.UserRoleId = UserRoles.CAU Or UserType.UserRoleId = UserRoles.CRU)) Then
                    analysis = From a In utility.AnalysisRepository.GetAnalyses
                            Join uam In utility.UserAnalysisMappingRepository.GetUserAnalysisMapping() On a.AnalysisId Equals uam.AnalysisId
                            Join u In utility.UserRepository.GetUsers() On u.UserId Equals uam.UserId
                            Where u.Status = StatusE.Active And u.UserId = UserInfo.UserId Select a

                Else
                    analysis = From a In utility.AnalysisRepository.GetAnalyses()
                End If

                If (selectedCompanyFromDropdown > 0) Then
                    analysis = analysis.Where(Function(C) C.CompanyId = selectedCompanyFromDropdown)
                End If

                Logger.Log.Info(String.Format("Analysis Index Execution Done"))

                Dim pageNumber As Integer = (If(page, 1))
                If (analysis Is Nothing) Then
                    Return View(analysis)

                Else
                    Return View(analysis.ToPagedList(pageNumber, Page_Size))
                End If

            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Unable to Index() Analysis-", ex.Message)
                Logger.Log.Error(String.Format("\n Unable to Index() Anlaysis with Message- {0} " + Environment.NewLine + "Stack Trace: {1} ", ex.Message, ex.StackTrace))
                Return View()
            Finally
                Logger.Log.Info(String.Format("Index Analysis Execution Ended"))
            End Try
        End Function

        <CustomActionFilter()>
        Function Create(SelectedCompany As System.Nullable(Of Integer)) As ViewResult
            Dim analysisDetail As New Analysis()
            Try
                If Not (Session("SelectedCompanyFromDropdown") Is Nothing) Then
                    selectedCompanyFromDropdown = Session("SelectedCompanyFromDropdown")
                End If

                If (selectedCompanyFromDropdown = 0) Then
                    TempData("InfoMessage") = String.Concat("To create analysis, you should have atleast one company.")
                Else
                    SelectedCompany = selectedCompanyFromDropdown
                End If

                Dim companyId As Integer = SelectedCompany.GetValueOrDefault()

                If (companyId > 0 And (utility.CompanyRepository.Get(filter:=Function(c) c.CompanyId = companyId).Select(Function(s) s.CompanyName).Count > 0)) Then
                    analysisDetail.CompanyName = utility.CompanyRepository.Get(filter:=Function(c) c.CompanyId = companyId).Select(Function(s) Common.Decrypt(s.CompanyName)).FirstOrDefault().ToString()
                    analysisDetail.CompanyId = companyId
                End If

                'PopulateCompanyList(SelectedCompany)
                PopulateUserList(analysisDetail, analysisDetail.CompanyId)
                Logger.Log.Info(String.Format("Analysis Create Execution Done"))
                Return View(analysisDetail)
            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Unable to Create Analysis-", ex.Message)
                Logger.Log.Error(String.Format("\n Unable to Create() Analysis with Message- {0} " + Environment.NewLine + "Stack Trace: {1} ", ex.Message, ex.StackTrace))
                Return View()
            Finally
                Logger.Log.Info(String.Format("Create Analysis Execution Ended"))
            End Try

        End Function

        <HttpPost()>
         <CustomActionFilter()>
        Function Create(analysisDetail As Analysis) As ActionResult

            Dim useranalysis As UserAnalysisMapping
            Try
                If (Not Session("UserType") Is Nothing) And (Not Session("UserInfo") Is Nothing) Then
                    UserType = DirectCast(Session("UserType"), UserRole)
                    UserInfo = DirectCast(Session("UserInfo"), User)
                    CustomerId = UserInfo.CustomerId
                    selectedCompanyFromDropdown = Session("SelectedCompanyFromDropdown")
                End If

                If ModelState.IsValid Then

                    Dim AnalysisNameIsUnique = utility.AnalysisRepository.ValidateAnalysis(analysisDetail.AnalysisName, analysisDetail.CompanyId)

                    If (AnalysisNameIsUnique.Count > 0) Then
                        ModelState.AddModelError("AnalysisName", "Analysis name already present for selected company. Please provide distinct analysis name.")
                    Else
                        analysisDetail.CreatedBy = UserInfo.UserId
                        analysisDetail.UpdatedBy = UserInfo.UserId
                        utility.AnalysisRepository.InsertAnalysis(analysisDetail)
                        utility.AnalysisRepository.Save()
                        Logger.Log.Info(String.Format("Analysis Created successfully with id {0} ", analysisDetail.AnalysisId))

                        useranalysis = New UserAnalysisMapping()
                        If (UserInfo.UserRoleId = UserRoles.SAU) Then
                            useranalysis.AnalysisId = analysisDetail.AnalysisId
                            useranalysis.UserId = UserInfo.UserId
                            useranalysis.CreatedBy = UserInfo.UserId
                            useranalysis.UpdatedBy = UserInfo.UserId
                            utility.UserAnalysisMappingRepository.Insert(useranalysis)
                            utility.UserAnalysisMappingRepository.Save()
                        End If

                        If Not (analysisDetail.PostedUsers Is Nothing) Then
                            useranalysis = New UserAnalysisMapping()
                            For Each selecteduser As Integer In analysisDetail.PostedUsers.UserIds
                                useranalysis.AnalysisId = analysisDetail.AnalysisId
                                useranalysis.UserId = selecteduser
                                useranalysis.CreatedBy = UserInfo.UserId
                                useranalysis.UpdatedBy = UserInfo.UserId
                                utility.UserAnalysisMappingRepository.Insert(useranalysis)
                                utility.UserAnalysisMappingRepository.Save()
                            Next
                            Logger.Log.Info(String.Format("Users are mapped with analysis successfully with Analysis ID : {0} , Analysis Name {1} ", analysisDetail.AnalysisId, analysisDetail.AnalysisName))

                        End If

                        TempData("Message") = "Analysis created successfully."
                        Return RedirectToAction("Index")

                    End If
                End If

                analysisDetail.CompanyName = utility.CompanyRepository.Get(filter:=Function(c) c.CompanyId = analysisDetail.CompanyId).Select(Function(s) s.CompanyName).FirstOrDefault().ToString()
                PopulateCompanyList(analysisDetail.CompanyId)
                PopulateUserList(analysisDetail, analysisDetail.CompanyId)
                Logger.Log.Info(String.Format("Create Analysis Successfully Executed"))

            Catch dataEx As DataException
                TempData("ErrorMessage") = String.Concat("Unable to create analysis-", dataEx.Message)
                Logger.Log.Error(String.Format("Unable to Create Analysis id- {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2} ", analysisDetail.AnalysisId, dataEx.Message, dataEx.StackTrace))
            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Unable to create analysis-", ex.Message)
                Logger.Log.Error(String.Format("Unable to Create Analysis id- {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2} ", analysisDetail.AnalysisId, ex.Message, ex.StackTrace))
            Finally
                Logger.Log.Info(String.Format("Create Analysis Execution Ended"))
            End Try
            Return View(analysisDetail)
        End Function

        Private Sub PopulateCompanyList(Optional SelectedCompany As Object = Nothing)
            Logger.Log.Info(String.Format("Populate Company List  started "))
            Try

                If (Not Session("UserType") Is Nothing) And (Not Session("UserInfo") Is Nothing) Then
                    UserType = DirectCast(Session("UserType"), UserRole)
                    UserInfo = DirectCast(Session("UserInfo"), User)
                    CustomerId = If(UserType.UserRoleId = UserRoles.SAU, UserInfo.CustomerId, Nothing)

                End If

                Dim companies = utility.CompanyRepository.Get(orderBy:=Function(q) q.OrderBy(Function(d) d.CompanyName))

                If (CustomerId > 0) Then
                    companies = companies.Where(Function(c) c.CustomerId = CustomerId)
                End If

                If (Not UserType Is Nothing And Not UserInfo Is Nothing) Then
                    If (UserType.UserRoleId = UserRoles.CAU Or UserType.UserRoleId = UserRoles.CRU) Then
                        companies = From c In companies
                                      Join ucm In utility.UserCompanyMappingRepository.GetUserCompanyMapping() On c.CompanyId Equals ucm.CompanyId
                                      Join u In utility.UserRepository.GetUsers() On u.UserId Equals ucm.UserId
                                      Where u.Status = StatusE.Active And u.UserId = UserInfo.UserId Select c

                    End If
                End If

                ViewBag.SelectedCompany = New SelectList(companies, "CompanyId", "CompanyName", SelectedCompany)
                Logger.Log.Info(String.Format("Populate Company List successfully Loaded"))
            Catch dataEx As DataException
                TempData("ErrorMessage") = String.Concat("Unable to Populate Company List -", dataEx.Message)
                Logger.Log.Error(String.Format("Unable to Populate Company List with Message- {0} " + Environment.NewLine + "Stack Trace: {1} ", dataEx.Message, dataEx.StackTrace))
                Throw
            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Unable to Populate Company List-", ex.Message)
                Logger.Log.Error(String.Format("Unable to populate company list with Message- {0} " + Environment.NewLine + "Stack Trace: {1}", ex.Message, ex.StackTrace))
                Throw
            Finally
                Logger.Log.Info(String.Format("Execution Ended"))
            End Try
        End Sub

        Private Sub PopulateUserList(analysisDetail As Analysis, companyId As Integer)
            Try

                If (Not Session("UserType") Is Nothing) And (Not Session("UserInfo") Is Nothing) Then
                    UserType = DirectCast(Session("UserType"), UserRole)
                    UserInfo = DirectCast(Session("UserInfo"), User)
                    CustomerId = If(UserInfo.CustomerId > 0, UserInfo.CustomerId, Nothing)

                End If

                Dim users = utility.UserRepository.GetUsers().Where(Function(u) (u.UserRoleId <> UserRoles.PGAAdmin) And (u.UserRoleId <> UserRoles.PGASupport) _
                                                                     And u.Status = StatusE.Active)

                If ((CustomerId > 0) And (Not UserType Is Nothing) And (Not UserInfo Is Nothing) And (UserInfo.UserRoleId <> UserRoles.PGAAdmin And UserInfo.UserRoleId <> UserRoles.PGASupport)) Then

                    'users = From c In users.Where(Function(a) a.CustomerId = CustomerId And a.UserRoleId > UserInfo.UserRoleId)
                    users = From u In users
                            Join ucm In utility.UserCompanyMappingRepository.GetUserCompanyMapping() On ucm.UserId Equals u.UserId
                            Where u.CustomerId = CustomerId And u.UserRoleId > UserInfo.UserRoleId And ucm.CompanyId = companyId Select u
                Else
                    ' users = From c In users.Where(Function(a) a.UserRoleId >= UserInfo.UserRoleId)
                    users = From u In users
                   Join ucm In utility.UserCompanyMappingRepository.GetUserCompanyMapping() On ucm.UserId Equals u.UserId
                   Where u.UserRoleId >= UserInfo.UserRoleId And ucm.CompanyId = companyId Select u
                End If

                Dim userAnalysis = utility.UserAnalysisMappingRepository.GetUserAnalysisMappingByAnalysisId(analysisDetail.AnalysisId)
                Dim userList As List(Of User_) = New List(Of User_)
                'if an array of posted user ids exists and is not empty,
                'save selected ids
                For Each user As User In users.Distinct()
                    userList.Add(New User_(user.UserId, user.FirstName + " " + user.LastName, New With {.user = [Enum].GetName(GetType(UserRoles), user.UserRoleId).ToString()}, False))
                Next
                analysisDetail.AvailableUsers = userList
                If Not (userAnalysis Is Nothing) Then
                    Dim InsertedUserList As List(Of User_) = New List(Of User_)
                    Dim InsertedUsers = users.Where(Function(b) userAnalysis.Any(Function(a) a.UserId = b.UserId))
                    For Each user As User In InsertedUsers
                        InsertedUserList.Add(New User_(user.UserId, user.FirstName + " " + user.LastName, New With {.user = [Enum].GetName(GetType(UserRoles), user.UserRoleId).ToString()}, False))
                    Next
                    analysisDetail.SelectedUsers = InsertedUserList
                End If
                ' if a view model array of posted user ids exists and is not empty,
                ' save selected ids
                If Not (analysisDetail.PostedUsers Is Nothing) Then
                    Dim selectedUserList As List(Of User_) = New List(Of User_)
                    Dim selectedUsers = utility.UserRepository.GetUsers.Where(Function(u) analysisDetail.PostedUsers.UserIds.Contains(u.UserId) And u.Status = StatusE.Active).ToList()
                    For Each user As User In selectedUsers
                        selectedUserList.Add(New User_(user.UserId, user.FirstName + " " + user.LastName, New With {.user = [Enum].GetName(GetType(UserRoles), user.UserRoleId).ToString()}, False))
                    Next
                    analysisDetail.SelectedUsers = selectedUserList
                End If
                Logger.Log.Info(String.Format("Populate User List Successfully Executed"))
            Catch dataEx As DataException
                TempData("ErrorMessage") = String.Concat("Populate User List-->", dataEx.Message)
                Logger.Log.Error(String.Format("Unable to Populate User List with Analysis id- {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2} ", analysisDetail.AnalysisId, dataEx.Message, dataEx.StackTrace))
                Throw
            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Populate User List-->", ex.Message)
                Logger.Log.Error(String.Format("Unable to Populate User List with Analysis id- {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2} ", analysisDetail.AnalysisId, ex.Message, ex.StackTrace))
                Throw
            Finally
                Logger.Log.Info(String.Format("Populate UserList Execution Ended"))
            End Try
        End Sub

        <CustomActionFilter()>
        Function Edit(Id As Integer) As ViewResult
            Try
                Dim analysis = analysisRepository.GetAnalysisById(Id)

                If (Not analysis Is Nothing) Then
                    analysis.CompanyName = utility.CompanyRepository.Get(filter:=Function(c) c.CompanyId = analysis.CompanyId).Select(Function(s) Common.Decrypt(s.CompanyName)).FirstOrDefault().ToString()
                End If

                'PopulateCompanyList(analysis.CompanyId)

                PopulateUserList(analysis, analysis.CompanyId)

                Logger.Log.Info(String.Format("Analysis  updated successfully id- {0} ", Id))
                Return View(analysis)

            Catch dataEx As DataException
                TempData("ErrorMessage") = String.Concat("Unable to Edit analysis-", dataEx.Message)
                Logger.Log.Error(String.Format("Unable to Edit Analysis id- {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2} ", Id, dataEx.Message, dataEx.StackTrace))
                'Log the error (add a variable name after DataException)
                ModelState.AddModelError("", "Unable to update analysis-." + dataEx.Message)

                'Return RedirectToAction("Index")
            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Unable to Edit analysis-", ex.Message)
                Logger.Log.Error(String.Format("Unable to Edit Analysis id- {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2} ", Id, ex.Message, ex.StackTrace))
                ModelState.AddModelError("", "Unable to update analysis." + ex.Message)
            Finally
                Logger.Log.Info(String.Format("Execution Ended"))
                'Return RedirectToAction("Index")
            End Try

            Return Nothing
        End Function

        <HttpPost()>
        <CustomActionFilter()>
        Function Edit(AnalysisDetail As Analysis) As ActionResult
            Try
                If (Not Session("UserType") Is Nothing) And (Not Session("UserInfo") Is Nothing) Then
                    UserType = DirectCast(Session("UserType"), UserRole)
                    UserInfo = DirectCast(Session("UserInfo"), User)
                    CustomerId = If(UserInfo.CustomerId > 0, UserInfo.CustomerId, Nothing)

                End If

                If ModelState.IsValid Then

                    AnalysisDetail.UpdatedBy = UserInfo.UserId
                    analysisRepository.UpdateAnalysis(AnalysisDetail)
                    analysisRepository.Save()

                    'Get list of User mapped with the selected company
                    Dim selectMappedUserWithAnalysis As Dictionary(Of Integer, Integer) = utility.UserAnalysisMappingRepository.GetUserAnalysisMappingByAnalysisId(AnalysisDetail.AnalysisId).ToDictionary(Function(a) a.UserId, Function(a) a.UserAnalysisMappingId)

                    Dim useranalysis As New UserAnalysisMapping()

                    If (UserInfo.UserRoleId = UserRoles.SAU) Then
                        selectMappedUserWithAnalysis.Remove(UserInfo.UserId)
                    End If
                    If Not (AnalysisDetail.PostedUsers Is Nothing) Then
                        For Each selecteduser As String In AnalysisDetail.PostedUsers.UserIds

                            'If New
                            If selectMappedUserWithAnalysis.ContainsKey(selecteduser) = False Then

                                useranalysis.AnalysisId = AnalysisDetail.AnalysisId
                                useranalysis.UserId = Convert.ToInt32(selecteduser)
                                useranalysis.UpdatedBy = UserInfo.UserId
                                utility.UserAnalysisMappingRepository.Insert(useranalysis)
                                utility.UserAnalysisMappingRepository.Save()
                                Logger.Log.Info(String.Format("User is successfully mapped with company, Company ID : {0} , UserId {1} ", AnalysisDetail.AnalysisId, useranalysis.UserId))
                            End If

                            selectMappedUserWithAnalysis.Remove(selecteduser)
                        Next
                    End If

                    'Delete the users, which are not found within redefine selected list.
                    If (selectMappedUserWithAnalysis.Count > 0) Then

                        Logger.Log.Info(String.Format("Total {0} users found to be delete of AnalysisId {1} from UserAnalysisMapping ", selectMappedUserWithAnalysis.Count, AnalysisDetail.AnalysisId))
                        Dim getSelectMappedUser = String.Concat("'", String.Join("','", selectMappedUserWithAnalysis.Select(Function(a) a.Key.ToString())), "'")
                        Dim result = utility.UserAnalysisMappingRepository.DeleteUserAnalysisMappingByUserIdAndAnalysisId(AnalysisDetail.AnalysisId, getSelectMappedUser)

                        If (result = selectMappedUserWithAnalysis.Count) Then
                            Logger.Log.Info(String.Format("Users delete successfully, which is not found within redefine selected list with analysis, AnalysisId : {0} , Users {1} ", AnalysisDetail.AnalysisId, getSelectMappedUser))
                        Else
                            'Logger.Log.Info(String.Format("Delete users list mismatch --> FoundUsersToDelete {0} : , ActualUsersDeleted : {1} "))
                            Logger.Log.Info(String.Format("Users delete successfully, which is not found within redefine selected list with analysis, AnalysisId : {0} , Users {1} ", AnalysisDetail.AnalysisId, getSelectMappedUser))
                        End If

                    End If


                    TempData("Message") = "Analysis updated successfully."
                    Logger.Log.Info(String.Format("Analysis Updated Successfully id- {0}", AnalysisDetail.AnalysisId))
                    Return RedirectToAction("Index")

                End If
            Catch dataEx As DataException
                TempData("ErrorMessage") = String.Concat("Unable to update Analysis-", dataEx.Message)
                Logger.Log.Error(String.Format("Unable to Update Analysis id- {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2}", AnalysisDetail.AnalysisId, dataEx.Message, dataEx.StackTrace))
                'Log the error (add a variable name after DataException)
                'ModelState.AddModelError("", "Unable to update company-." + dataEx.Message)

            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Unable to update Analysis-", ex.Message)
                Logger.Log.Error(String.Format("Unable to Update Analysis id- {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2}", AnalysisDetail.AnalysisId, ex.Message, ex.StackTrace))
                'ModelState.AddModelError("", "Unable to update company." + ex.Message)
            Finally
                Logger.Log.Info(String.Format("Execution Ended"))
            End Try

            PopulateCompanyList(AnalysisDetail.CompanyId)
            PopulateUserList(AnalysisDetail, AnalysisDetail.CompanyId)
            Return View(AnalysisDetail)
        End Function

        <CustomActionFilter()>
        Function Delete(Id As Integer) As ActionResult
            Logger.Log.Info(String.Format("Analysis Delete started {0}", Id))
            Try

                If (Not Session("UserInfo") Is Nothing) Then
                    UserType = DirectCast(Session("UserType"), UserRole)
                    UserInfo = DirectCast(Session("UserInfo"), User)
                End If

                utility.AccountTypeRepository.DeleteAccountTypeByAnalysis(Id)
                utility.Save()
                Logger.Log.Info(String.Format("Account type records which has analysis Id : {0}  deleted Successfully.", Id))

                utility.AccountRepository.DeleteAccountByAnalysis(Id)
                utility.Save()
                Logger.Log.Info(String.Format("Account records which has analysis Id : {0}  deleted Successfully.", Id))

                utility.BalanceRepository.DeleteBalanceByAnalysis(Id)
                utility.Save()
                Logger.Log.Info(String.Format("Balance records which has analysis Id : {0}  deleted Successfully.", Id))

                utility.DashboardRepository.DeleteDashboardItemsByAnalysis(Id)
                utility.Save()
                Logger.Log.Info(String.Format("Dashboard records which has analysis Id : {0}  deleted Successfully.", Id))

                utility.UserAnalysisMappingRepository.DeleteUserAnalysisMappingByAnalysis(Id)
                Logger.Log.Info(String.Format("Dashboard records which has analysis Id : {0}  deleted Successfully.", Id))

                utility.AnalysisRepository.DeleteAnalysis(Id)
                utility.AnalysisRepository.Save()
                Logger.Log.Info(String.Format("Analysis Id : {0}  deleted Successfully.", Id))

                If (UserInfo.AnalysisId = Id) Then
                    Dim usr = userRepository.GetUserById(UserInfo.UserId)
                    usr.AnalysisId = 0
                    usr.UpdatedBy = UserInfo.UserId
                    userRepository.UpdateUser(usr)
                    userRepository.Save()
                    Session("UserInfo") = usr
                    Logger.Log.Info("Profile updated Successfully.")
                End If

                TempData("Message") = "Analysis deleted successfully."
                Logger.Log.Info(String.Format("Analysis  deleted successfully id- {0}", Id))
            Catch dataEx As DataException
                TempData("ErrorMessage") = String.Concat("Unable to delete analysis - ", dataEx.Message)
                Logger.Log.Error(String.Format("Unable to Delete Analysis id- {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2}", Id, dataEx.Message, dataEx.StackTrace))
                Return RedirectToAction("Index", New System.Web.Routing.RouteValueDictionary() _
                                        From {{"id", Id}, {"deleteChangesError", True}})

            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Unable to delete analysis-", ex.Message)
                Logger.Log.Error(String.Format("Unable to Delete Analysis id- {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2}", Id, ex.Message, ex.StackTrace))
                ModelState.AddModelError("Index", "Unable to delete analysis." + ex.Message)

                Return RedirectToAction("Index")
            Finally
                Logger.Log.Info(String.Format("Execution Ended"))
            End Try
            Return RedirectToAction("Index")
        End Function

        Protected Overrides Sub Dispose(disposing As Boolean)
            analysisRepository.Dispose()
            MyBase.Dispose(disposing)
        End Sub

    End Class
End Namespace
