﻿Imports System.Diagnostics.CodeAnalysis
Imports System.Security.Principal
Imports System.Web.Routing
Imports onlineAnalytics
Imports System.Collections
Imports System.Configuration
Imports System.Linq
Imports System.Data.Entity
Imports PagedList
Imports System.Web.Mvc
Imports onlineAnalytics.Common
Imports Recurly
Imports System.Data.Entity.Validation

Namespace onlineAnalytics

    <CustAuthFilter()>
    Public Class UserController
        Inherits System.Web.Mvc.Controller

        Private utility As New Utility()
        Private userRepository As IUserRepository
        Private common As New Common()
        Private UserType As UserRole
        Private UserInfo As User
        Private customerInfo As Customer
        Private CustomerId As Integer
        Private ua As UserAccess
        Private pageSize As Integer


        Public ReadOnly Property Page_Size As Integer

            Get
                If (System.Configuration.ConfigurationManager.AppSettings("PageSize") <> "") Then
                    pageSize = System.Configuration.ConfigurationManager.AppSettings("PageSize").ToString()
                Else
                    pageSize = 10
                End If
                Return Me.pageSize
            End Get
        End Property

        Public Sub New()
            Me.userRepository = New UserRepository(New DataAccess())
        End Sub

        Public Sub New(userRepository As IUserRepository)
            Me.userRepository = userRepository
        End Sub

        <CustomActionFilter()>
        Function Index(page As System.Nullable(Of Integer)) As ViewResult

            Try
                Dim users As IEnumerable(Of User)
                If (Not Session("UserType") Is Nothing) And (Not Session("UserInfo") Is Nothing) And (Not Session("UserAccess") Is Nothing) Then
                    UserType = DirectCast(Session("UserType"), UserRole)
                    UserInfo = DirectCast(Session("UserInfo"), User)
                    ua = DirectCast(Session("UserAccess"), UserAccess)
                    CustomerId = If(UserType.UserRoleId = UserRoles.SAU, UserInfo.CustomerId, Nothing)

                End If

                If (UserType.UserRoleId = UserRoles.PGAAdmin Or UserType.UserRoleId = UserRoles.PGASupport) Then
                    users = userRepository.GetUsers().Where(Function(u) (u.Status = StatusE.Active Or u.Status = StatusE.Pending) And (u.UserRoleId <> UserRoles.PGAAdmin And u.UserRoleId <> UserRoles.PGASupport And u.UserRoleId <> UserRoles.SAU))
                Else
                    users = From usr In userRepository.GetUsers().Where(Function(u) (u.Status = StatusE.Active Or u.Status = StatusE.Pending) And (u.UserRoleId <> UserRoles.SAU))
                End If


                If (CustomerId > 0) Then
                    users = users.Where(Function(c) c.CustomerId = CustomerId)
                End If

                Logger.Log.Info(String.Format("User Loaded Successfully"))

                Dim pageNumber As Integer = (If(page, 1))
                If (users Is Nothing) Then
                    Return View(users)
                Else
                    Return View(users.ToPagedList(pageNumber, Page_Size))
                End If
                Return View(users.ToPagedList(pageNumber, Page_Size))
            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Unable to Load Users-", ex.Message)
                Logger.Log.Error(String.Format("Unable to Load Users Message- {0} " + Environment.NewLine + "Stack Trace: {1} ", ex.Message, ex.StackTrace))
                Return View()
            Finally
                Logger.Log.Info(String.Format("User Index Function Execution Ended"))
            End Try
            Return View()
        End Function

        Function Login(returnUrl As String) As ActionResult
            ViewBag.ReturnUrl = returnUrl
            Return View()
        End Function

        Function Authenticate(userName As String, password As String, returnUrl As String) As JsonResult
            Dim data As New Hashtable()
            Try
                If (AuthenticateUser(userName, password)) Then

                    Logger.Log.Info(String.Format("Successfully Login"))

                    data.Add("Success", "Authenticated")
                    data.Add("Url", Url.Action("Index", "Dashboard").ToString())

                Else
                    data.Add("Error", TempData("ErrorMessage").ToString())
                End If
            Catch ex As Exception
                If (ex.Message.Equals("The remote name could not be resolved: 'api.recurly.com'")) Then
                    Logger.Log.Error(String.Format("Your internet connection is down, - {0} " + Environment.NewLine + "Stack Trace: {1} ", ex.Message, ex.StackTrace))
                    data.Add("Error", "Please check your internet connection, it's required.")
                Else
                    Logger.Log.Error(String.Format("Unable to Login with Message- {0} " + Environment.NewLine + "Stack Trace: {1} ", ex.Message, ex.StackTrace))
                    data.Add("Error", ex.Message.ToString())
                End If
            Finally
                Logger.Log.Info(String.Format("Login Function Execution Ended"))
            End Try

            Return Json(data, JsonRequestBehavior.AllowGet)
        End Function

        <HttpPost()>
        Function Login(model As LoginModel, returnUrl As String) As ActionResult
            Try
                TempData.Clear()

                If ModelState.IsValid Then
                    'Check user credentials
                    If (AuthenticateUser(model.UserName, model.Password)) Then

                        Logger.Log.Info(String.Format("Successfully Login"))

                        Return RedirectToAction("Index", "Dashboard")

                    End If

                End If
            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Unable to Login with User-", ex.Message)
                Logger.Log.Error(String.Format("Unable to Login with Message- {0} " + Environment.NewLine + "Stack Trace: {1} ", ex.Message, ex.StackTrace))

            Finally
                Logger.Log.Info(String.Format("Login Function Execution Ended"))
            End Try

            Return View()
        End Function

        Private Function AuthenticateUser(userName As String, password As String) As Boolean

            Dim flag As Boolean = False

            Dim result = utility.UserRepository.ValidateUserAccount(userName, common.Encrypt(password))

            If (Not (result Is Nothing)) Then

                If (result.Status = StatusE.Pending) Then
                    TempData("ErrorMessage") = "Your account has not been activated. <br /> Please click on the link, send to your email-id. <br /> Or Contact Administrator."
                    Return flag
                ElseIf (result.Status = StatusE.Deactivated) Then
                    TempData("ErrorMessage") = "This user account has been closed by the subscription administrator."
                    Return flag
                End If

                Dim userRole = utility.UserRoleRepository.GetByID(result.UserRoleId)
                Dim state As String = [Enum].GetName(GetType(StatusE), StatusE.Active)
                If (result.UserName.Equals("Administrator", StringComparison.OrdinalIgnoreCase) Or result.UserName.Equals("SupportAdmin", StringComparison.OrdinalIgnoreCase)) Then

                    Session("UserInfo") = result
                    Session("UserType") = userRole
                    GetUserRolePermission(userRole)
                    flag = True
                    Return flag
                End If

                Dim account As RecurlyAccount = If(userRole.UserRoleId = UserRoles.SAU, RecurlyAccount.Get(result.CustomerId), RecurlyAccount.Get(result.UserId))

                If ((account.Closed = False) And (account.State.Equals(state, StringComparison.OrdinalIgnoreCase))) Then

                    If (userRole.UserRoleId = UserRoles.SAU) Then

                        Dim customerInfo = utility.CustomerRepository.GetCustomerById(result.CustomerId)
                        If (Not customerInfo Is Nothing) Then
                            Session("CustomerInfo") = customerInfo
                        End If

                        If (result.AnalysisId <> 0) Then
                            result.selectedCompany = Convert.ToInt64(utility.AnalysisRepository().GetAnalyses().Where(Function(b) b.AnalysisId = result.AnalysisId).Select(Function(a) a.CompanyId).FirstOrDefault())

                            If (result.selectedCompany <> 0) Then
                                result.fiscalMonthOfSelectedCompany = utility.CompaniesRepository.GetCompanyByID(result.selectedCompany).FiscalMonthStart
                            Else
                                result.AnalysisId = 0
                            End If

                        End If

                    End If

                    Session("UserInfo") = result
                    Session("UserType") = userRole

                    GetUserRolePermission(userRole)

                    flag = True

                Else
                    TempData("ErrorMessage") = "This account has been " + account.State + " by the Subscriber or Administrator."
                End If
            Else
                TempData("ErrorMessage") = "The Username or Password you entered is incorrect."
            End If

            Return flag
        End Function

        Private Sub GetUserRolePermission(userRole As UserRole)

            Dim userRolePermission = From urp In utility.UserRolePermissionRepository.GetUserRolePermission()
                                     Join urpm In utility.UserRolePermissionMappingRepository.GetUserRolePermissionMapping()
                                     On urpm.UserRolePermissionId Equals urp.UserRolePermissionId Where urpm.UserRoleId = userRole.UserRoleId
                                     Order By urp.UserRolePermissionId Select urp

            Dim UserAccess As New UserAccess()

            For Each itemPermission In userRolePermission

                Select Case itemPermission.Description.ToString()
                    Case "Subscription Management"
                        UserAccess.SubscriptionManagement = True
                    Case "User Management"
                        UserAccess.UserManagement = True
                    Case "Search Customer"
                        UserAccess.SearchCustomer = True
                    Case "View Subscription"
                        UserAccess.ViewSubscription = True
                    Case "Add Subscription"
                        UserAccess.AddSubscription = True
                    Case "Update Subscription"
                        UserAccess.UpdateSubscription = True
                    Case "Cancel Subscription"
                        UserAccess.CancelSubscription = True
                    Case "Add User"
                        UserAccess.AddUser = True
                    Case "Delete User"
                        UserAccess.DeleteUser = True
                    Case "View User"
                        UserAccess.ViewUser = True
                    Case "Update User"
                        UserAccess.UpdateUser = True
                    Case "Add Company"
                        UserAccess.AddCompany = True
                    Case "Delete Company"
                        UserAccess.DeleteCompany = True
                    Case "View Companies"
                        UserAccess.ViewCompanies = True
                    Case "Update Company"
                        UserAccess.UpdateCompany = True
                    Case "User Company Mapping"
                        UserAccess.UserCompanyMapping = True
                    Case "Add Analysis"
                        UserAccess.AddAnalysis = True
                    Case "Delete Analysis"
                        UserAccess.DeleteAnalysis = True
                    Case "View Analyses"
                        UserAccess.ViewAnalyses = True
                    Case "Update Analysis"
                        UserAccess.UpdateAnalysis = True
                    Case "User Analysis Mapping"
                        UserAccess.UserAnalysisMapping = True
                    Case "View Analysis Info"
                        UserAccess.ViewAnalysisInfo = True
                    Case "Print Analysis Info"
                        UserAccess.PrintAnalysisInfo = True
                    Case "Modify Dashboard"
                        UserAccess.ModifyDashboard = True
                End Select
            Next

            Session("UserAccess") = UserAccess

        End Sub

        <AcceptVerbs(HttpVerbs.Get)> _
        Public Function SubscriptionCost(CustomerId As String) As JsonResult
            Dim data As New Hashtable()
            Logger.Log.Info(String.Format("Subscription Cost Execution Start"))
            Try
                If Not String.IsNullOrEmpty(CustomerId) Then
                    data = GetSubscriptionCost(CustomerId, data)
                End If

                Return Json(data, JsonRequestBehavior.AllowGet)

            Catch recurlyEx As RecurlyException

                data.Add("Error", recurlyEx.Message)
                Logger.Log.Error(String.Format("Error occured while Get Subscription cost of Customer Id {0} with Message - {1}" + Environment.NewLine + "Stack Trace: {2} ", CustomerId, recurlyEx.Message, recurlyEx.StackTrace))
                Return Json(data, JsonRequestBehavior.AllowGet)

            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Unable to get subscription cost - ", ex.Message)
                Logger.Log.Error(String.Format("Error occured while Get Subscription cost of Customer Id {0} with Message - {1}" + Environment.NewLine + "Stack Trace: {2} ", CustomerId, ex.Message, ex.StackTrace))
                data.Add("Error", ex.Message)
                Return Json(data, JsonRequestBehavior.AllowGet)
            Finally
                Logger.Log.Info(String.Format("Subscription Cost Execution Ended"))
            End Try

        End Function

        Private Function GetSubscriptionCost(CustomerId As String, data As Hashtable) As Hashtable

            If (Not String.IsNullOrEmpty(CustomerId)) Then

                Dim subscription = RecurlySubscription.Get(CustomerId)
                Dim Quantity As Integer
                Dim AmountInCents As Double
                Dim TotalAmountInCents As Double

                If (subscription.AddOn.Addons.Count = 0) And (Not String.IsNullOrEmpty(utility.RecurlyPlan)) Then

                    Dim plan = RecurlyPlan.Get(utility.RecurlyPlan)
                    If (plan.AddOn.Addons.Count > 0) Then
                        Quantity = plan.AddOn.Addons(0).quantity
                        AmountInCents = plan.AddOn.Addons(0).unit_amount_in_cents / 100
                        TotalAmountInCents = (Quantity * AmountInCents)

                        data.Add("AddOnAmountInCents", AmountInCents.ToString("N2"))
                        Session("NewUserAddon") = DirectCast(plan.AddOn.Addons(0), RecurlyAddOn)
                    End If
                Else

                    AmountInCents = subscription.AddOn.Addons(0).unit_amount_in_cents / 100
                    Quantity = subscription.AddOn.Addons(0).quantity + 1
                    TotalAmountInCents = (Quantity * AmountInCents)

                    data.Add("AddOnAmountInCents", AmountInCents.ToString("N2"))
                    Session("NewUserAddon") = DirectCast(subscription.AddOn.Addons(0), RecurlyAddOn)


                End If

                data.Add("Subsciption_UnitAmountInCents", TotalAmountInCents + (subscription.UnitAmountInCents / 100))

                ViewBag.Subsciption_UnitAmountInCents = TotalAmountInCents + (subscription.UnitAmountInCents / 100)
            End If

            Return data
        End Function

        ' This CreateUser Function allow user to enter required information to Create User.
        <CustomActionFilter()>
        Function CreateUser() As ActionResult
            Dim data As New Hashtable()
            Dim userDetail As New User()
            userDetail.Customer = New Customer()
            If (Not Session("UserType") Is Nothing) And (Not Session("UserInfo") Is Nothing) And (Not Session("UserAccess") Is Nothing) Then
                UserType = DirectCast(Session("UserType"), UserRole)
                UserInfo = DirectCast(Session("UserInfo"), User)
                ua = DirectCast(Session("UserAccess"), UserAccess)
                CustomerId = If(UserType.UserRoleId = UserRoles.SAU, UserInfo.CustomerId, Nothing)
            End If

            If (UserInfo.UserRoleId = UserRoles.PGAAdmin Or UserInfo.UserRoleId = UserRoles.PGASupport) Then
                PopulateCustomerList()
            ElseIf (CustomerId > 0) Then

                data = GetSubscriptionCost(CustomerId, data)
                userDetail.CustomerId = UserInfo.CustomerId
                userDetail.Customer.CustomerCompanyName = UserInfo.Customer.CustomerCompanyName
                ViewBag.AddOnAmountInCents = data.Item("AddOnAmountInCents")
                ViewBag.SubsciptionUnitAmountInCents = data.Item("Subsciption_UnitAmountInCents")
            End If

            PopulateUserRoleList()
            Return View(userDetail)

        End Function

        Private Sub SendEmail(User As User, emailId As Integer)
            Try

                Dim PlanGuruUrl As String = "http://" + HostName(Request) + If(Request.Url.IsDefaultPort, Request.ApplicationPath + "/", ":" + Request.Url.Port.ToString() + "/")
                Dim objEmailInfo = utility.EmailRepository.GetEmailInfoById(emailId)
                objEmailInfo.EmailBody = common.MergeUserEmailBody(User, objEmailInfo, PlanGuruUrl)
                common.SendUserEmail(User, objEmailInfo)
                Logger.Log.Info(String.Format("Email Sent Successfully to User: {0} for EmailId: {1}", User.UserId, emailId))
            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Unable to Send Email -", ex.Message)
                Logger.Log.Error(String.Format("Unable to Send Email, UserId - {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2} ", User.UserId, ex.Message, ex.StackTrace))
            Finally
                Logger.Log.Info(String.Format("SendEmail Execution Ended"))
            End Try
        End Sub

        <HttpPost()> _
        <CustomActionFilter()>
        Function CreateUser(User As User) As ActionResult
            Dim newUserAccountCode As Integer
            Dim customerInfo As Customer
            Dim data As New Hashtable()
            Try
                If (Not Session("UserType") Is Nothing) And (Not Session("UserInfo") Is Nothing) And (Not Session("UserAccess") Is Nothing) Then
                    UserType = DirectCast(Session("UserType"), UserRole)
                    UserInfo = DirectCast(Session("UserInfo"), User)
                    ua = DirectCast(Session("UserAccess"), UserAccess)
                    CustomerId = If(UserType.UserRoleId = UserRoles.SAU, UserInfo.CustomerId, Nothing)
                End If

                If ModelState.IsValid Then

                    Dim usernameAvailibility = utility.UserRepository().UsernameAvailibility(User.UserName)
                    Dim IsEmailAddressExists = utility.UserRepository().EmailAddressAvailibility(User.UserEmail)

                    newUserAccountCode = utility.CheckAccountCodeOnRecurly((utility.UserRepository().GetUsers().Max(Function(c) c.UserId) + 1))

                    Dim subscription = RecurlySubscription.Get(User.CustomerId)
                    If (usernameAvailibility.Count > 0) Then
                        ModelState.AddModelError("Username", "Someone already has that username. Try another.")
                    ElseIf (IsEmailAddressExists.Count > 0) Then
                        ModelState.AddModelError("UserEmail", "Someone already has that EmailAddress. Try another.")
                    ElseIf Not (subscription Is Nothing) Then
                        Dim newRecurlyAccount = New RecurlyAccount(newUserAccountCode)
                        Dim Addon = DirectCast(Session("NewUserAddon"), RecurlyAddOn)
                        newRecurlyAccount.Username = User.UserName
                        newRecurlyAccount.FirstName = User.FirstName
                        newRecurlyAccount.LastName = User.LastName
                        newRecurlyAccount.Email = User.UserEmail
                        newRecurlyAccount.Create()
                        subscription.AddOns = New List(Of RecurlyAddOn)
                        If (subscription.AddOn.Addons.Count > 0) Then
                            Addon.quantity += 1
                        End If
                        subscription.AddOns.Add(Addon)
                        subscription.ChangeSubscription(RecurlySubscription.ChangeTimeframe.Now)
                        customerInfo = utility.CustomerRepository.GetCustomerById(User.CustomerId)
                        customerInfo.Quantity = subscription.Quantity
                        utility.CustomerRepository.UpdateCustomer(customerInfo)
                        utility.CustomerRepository.Save()

                        User.UserId = newUserAccountCode
                        User.CreatedBy = UserInfo.UserId
                        User.UpdatedBy = UserInfo.UserId
                        User.Status = CType(StatusE.Pending, Integer)
                        'Generate random password while creating new account
                        'and send mail to the user to reset it.
                        Dim password As String = common.Generate(8, 2)
                        User.Password = common.Encrypt(password)
                        User.SecurityKey = Guid.NewGuid().ToString()

                        utility.UserRepository.Insert(User)
                        utility.UserRepository.Save()
                        TempData("Message") = "User created successfully."
                        Logger.Log.Info(String.Format("CreateUser Successfully Executed"))

                        SendEmail(User, EmailType.NewUserAdded)

                        Return RedirectToAction("Index")
                    End If
                End If

                If (UserInfo.UserRoleId = UserRoles.PGAAdmin Or UserInfo.UserRoleId = UserRoles.PGASupport) Then
                    PopulateCustomerList(User.CustomerId)
                ElseIf (CustomerId > 0) Then
                    data = GetSubscriptionCost(CustomerId, data)
                    User.CustomerId = UserInfo.CustomerId
                    User.Customer = utility.CustomerRepository().GetCustomerById(User.CustomerId)
                    User.Customer.CustomerCompanyName = UserInfo.Customer.CustomerCompanyName
                    ViewBag.AddOnAmountInCents = data.Item("AddOnAmountInCents")
                    ViewBag.SubsciptionUnitAmountInCents = data.Item("Subsciption_UnitAmountInCents")
                End If
                Logger.Log.Info(String.Format("CreateUser Successfully Executed"))
            Catch dataEx As DataException
                TempData("ErrorMessage") = String.Concat("Unable to create user account -", dataEx.Message)
                Logger.Log.Error(String.Format("Unable to create user account, UserId - {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2} ", newUserAccountCode, dataEx.Message, dataEx.StackTrace))
            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Unable to create user account -", ex.Message)
                Logger.Log.Error(String.Format("Unable to create user account, UserId - {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2} ", newUserAccountCode, ex.Message, ex.StackTrace))
            Finally
                Logger.Log.Info(String.Format("CreateUser Execution Ended"))
            End Try
            PopulateUserRoleList(User.UserRoleId)
            Return View(User)
        End Function

        <HttpPost()>
        <CustomActionFilter()>
        Function Edit(UserDetails As User) As ActionResult
            Logger.Log.Info(String.Format("\n User Update started {0}", UserDetails.UserId))
            Dim storedUserInfo As User
            Try
                If (Not Session("UserType") Is Nothing) And (Not Session("UserInfo") Is Nothing) And (Not Session("UserAccess") Is Nothing) Then
                    UserType = DirectCast(Session("UserType"), UserRole)
                    UserInfo = DirectCast(Session("UserInfo"), User)
                    ua = DirectCast(Session("UserAccess"), UserAccess)

                End If

                If ModelState.IsValid Then
                    Dim accountInfo = RecurlyAccount.Get(UserDetails.UserId)
                    Dim usernameAvailibility = utility.UserRepository().UsernameAvailibility(UserDetails.UserName).Where(Function(u) u.UserId <> UserDetails.UserId)
                    Dim emailAddressAvailibility = utility.UserRepository().EmailAddressAvailibility(UserDetails.UserEmail).Where(Function(u) u.UserId <> UserDetails.UserId)
                    If (usernameAvailibility.Count > 0) Then
                        ModelState.AddModelError("Username", "Someone already has that username. Try another.")
                    ElseIf (emailAddressAvailibility.Count > 0) Then
                        ModelState.AddModelError("UserEmail", "Someone already has that Email Address. Try another.")
                    Else
                        accountInfo.Username = UserDetails.UserName
                        accountInfo.FirstName = UserDetails.FirstName
                        accountInfo.LastName = UserDetails.LastName
                        accountInfo.Email = UserDetails.UserEmail
                        accountInfo.Update()

                        storedUserInfo = userRepository.GetUserById(UserDetails.UserId)

                        storedUserInfo.UserName = UserDetails.UserName
                        storedUserInfo.FirstName = UserDetails.FirstName
                        storedUserInfo.LastName = UserDetails.LastName
                        storedUserInfo.UserEmail = UserDetails.UserEmail
                        storedUserInfo.UserRole = UserDetails.UserRole
                        storedUserInfo.UserRoleId = UserDetails.UserRoleId
                        storedUserInfo.CustomerId = UserDetails.CustomerId
                        storedUserInfo.UpdatedBy = UserInfo.UserId

                        userRepository.UpdateUser(storedUserInfo)
                        userRepository.Save()

                        TempData("Message") = "User updated successfully."
                        Logger.Log.Info(String.Format("User Updated successfully with id {0}", UserDetails.UserId))
                        Return RedirectToAction("Index")
                    End If
                End If
            Catch dataEx As DataException
                TempData("ErrorMessage") = String.Concat("Unable to update User-", dataEx.Message)
                Logger.Log.Error(String.Format("\n Unable to Update User id- {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2} ", UserDetails.UserId, dataEx.Message, dataEx.StackTrace))
            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Unable to update User-", ex.Message)
                Logger.Log.Error(String.Format("\n Unable to Update User id- {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2} ", UserDetails.UserId, ex.Message, ex.StackTrace))
            Finally
                Logger.Log.Info(String.Format(" User Update Execution Ended"))
            End Try

            PopulateCustomerList(UserDetails.CustomerId)
            PopulateUserRoleList(UserDetails.UserRoleId)
            Return View(UserDetails)
        End Function

        <CustomActionFilter()>
        Public Function Edit(userAccountCode As String) As ActionResult
            Try
                Dim userInfo = userRepository.GetUserById(userAccountCode)
                PopulateCustomerList()
                PopulateUserRoleList()
                Return View(userInfo)
            Catch dataEx As DataException
                TempData("ErrorMessage") = String.Concat("Unable to view user details to Edit User Information - ", dataEx.Message)
                Logger.Log.Error(String.Format("\n Unable to view user details to Edit User Information, UserID - {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2} ", userAccountCode, dataEx.Message, dataEx.StackTrace))
            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Unable to view user details to Edit User Information - ", ex.Message)
                Logger.Log.Error(String.Format("\n Unable to Edit company id- {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2} ", userAccountCode, ex.Message, ex.StackTrace))
            Finally
                Logger.Log.Info(String.Format("Edit User Execution Ended"))
            End Try
            Return Nothing
        End Function
        <CustomActionFilter()>
        Private Sub PopulateCustomerList(Optional SelectedCustomer As Object = Nothing)
            Logger.Log.Info(String.Format("Populate Company List  started "))
            Try
                If (Not Session("UserType") Is Nothing) And (Not Session("UserInfo") Is Nothing) Then
                    UserType = DirectCast(Session("UserType"), UserRole)
                    UserInfo = DirectCast(Session("UserInfo"), User)
                    CustomerId = If(UserType.UserRoleId = UserRoles.SAU, UserInfo.CustomerId, SelectedCustomer)
                End If

                Dim customers = utility.CustomerRepository.Get(filter:=Function(q) (q.Quantity > 0), orderBy:=Function(q) q.OrderBy(Function(d) d.CustomerLastName))

                If (CustomerId > 0) Then
                    customers = customers.Where(Function(c) c.CustomerId = CustomerId)
                End If


                ViewBag.SelectedCustomer = New SelectList(customers, "CustomerId", "CustomerFullName", SelectedCustomer)
                Logger.Log.Info(String.Format("Populate Company List successfully Loaded"))
            Catch dataEx As DataException
                TempData("ErrorMessage") = String.Concat("Unable to Populate Company List -", dataEx.Message)
                Logger.Log.Error(String.Format("Unable to Populate Company List with Message- {0} " + Environment.NewLine + "Stack Trace: {1} ", dataEx.Message, dataEx.StackTrace))
                Throw
            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Unable to Populate Company List-", ex.Message)
                Logger.Log.Error(String.Format("Unable to populate company list with Message- {0} " + Environment.NewLine + "Stack Trace: {1}", ex.Message, ex.StackTrace))
                Throw
            Finally
                Logger.Log.Info(String.Format("Execution Ended"))
            End Try
        End Sub

        <CustomActionFilter()>
        Public Function Delete(userAccountCode As String) As ActionResult
            Logger.Log.Info(String.Format("User Account Deletion Started {0}", userAccountCode))
            Try
                Dim state As String = [Enum].GetName(GetType(StatusE), StatusE.Active)
                'Get Subscription AccountCode from UserInfo table from local database.
                Dim localDBUserInfo = utility.UserRepository.GetUserByID(userAccountCode)
                'Get Recurly Subscription Information using Subscription AccountCode.
                Dim recurlySubscriptionInfo = RecurlySubscription.Get(localDBUserInfo.CustomerId)

                'Get Customer Information from Local Database.
                'Dim customerInfo = utility.CustomerRepository.GetCustomerById(localDBUserInfo.CustomerId)
                If (recurlySubscriptionInfo.State.Equals(state, StringComparison.OrdinalIgnoreCase)) Then
                    'To do Add-On functionality
                    'Decrease quantity on recurly subscription
                    If (recurlySubscriptionInfo.AddOn.Addons.Count > 0) Then


                        Dim addOn As New RecurlyAddOn()
                        addOn = recurlySubscriptionInfo.AddOn.Addons(0)

                        'Check if quantity , if quantity is equal to 1 then Addon update is not required.
                        'It will automatically remove the last addon.
                        If (addOn.quantity > 1) Then
                            recurlySubscriptionInfo.AddOns = New List(Of RecurlyAddOn)
                            addOn.quantity -= 1
                            recurlySubscriptionInfo.AddOns.Add(addOn)
                        End If

                        recurlySubscriptionInfo.ChangeSubscription(RecurlySubscription.ChangeTimeframe.Now)

                    End If

                    'utility.CustomerRepository.UpdateCustomer(customerInfo)
                    'utility.Save()

                    Logger.Log.Info(String.Format("Update Quantity successfully of Customer in local database using UserId - {0}", userAccountCode))

                    'Closing account on recurly
                    RecurlyAccount.CloseAccount(userAccountCode)
                    Logger.Log.Info(String.Format("Close Account successfully on RECURLY using UserId - {0}", userAccountCode))
                    'utility.UserRepository.DeleteUser(userAccountCode)
                    localDBUserInfo.Status = StatusE.Deactivated
                    utility.UserRepository.UpdateUser(localDBUserInfo)
                    utility.UserRepository.Save()
                    If (localDBUserInfo.UserRoleId <> UserRoles.SAU) Then
                        utility.UserRepository.DeleteNonSAUUser(userAccountCode)
                        'utility.UserRepository.Save()
                    End If
                    SendEmail(localDBUserInfo, EmailType.UserDeleted)
                    Logger.Log.Info(String.Format("Deactivate Account successfully on local database using UserId - {0}", userAccountCode))
                End If
                TempData("Message") = "User account deleted successfully."
                Logger.Log.Info(String.Format("User account deleted successfully using  {0}", userAccountCode))
            Catch dataEx As DataException
                TempData("ErrorMessage") = String.Concat("Unable to delete User - ", dataEx.Message)
                Logger.Log.Error(String.Format(Environment.NewLine + " Unable to Delete User id- {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2}", userAccountCode, dataEx.Message, dataEx.StackTrace))
                Return RedirectToAction("Index", New System.Web.Routing.RouteValueDictionary() _
                                        From {{"id", userAccountCode}, {"deleteChangesError", True}})
            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Unable to delete User-", ex.Message)
                Logger.Log.Error(String.Format(" Unable to Delete User id- {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2} ", userAccountCode, ex.Message, ex.StackTrace))
            Finally
                Logger.Log.Info(String.Format("Execution Ended"))
            End Try
            Return RedirectToAction("Index")
        End Function
        <CustomActionFilter()>
        Private Sub PopulateUserRoleList(Optional SelectedUserRole As Object = Nothing)
            Logger.Log.Info(String.Format("Populate User List  started "))
            Try
                Dim user_roles = utility.UserRoleRepository.Get(filter:=Function(q) (q.UserRoleId > UserRoles.SAU), orderBy:=Function(q) q.OrderBy(Function(d) d.RoleName))
                ViewBag.SelectedRoles = New SelectList(user_roles, "UserRoleId", "RoleName", SelectedUserRole)
                Logger.Log.Info(String.Format("Populate User List successfully Loaded"))
            Catch dataEx As DataException
                TempData("ErrorMessage") = String.Concat("Unable to Populate Company List -", dataEx.Message)
                Logger.Log.Error(String.Format("Unable to Populate User List with Message- {0} " + Environment.NewLine + "Stack Trace: {1} ", dataEx.Message, dataEx.StackTrace))
                Throw
            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Unable to Populate Company List-", ex.Message)
                'Logger.Log.Error(String.Format("Unable to populate User list with Message- {0} " + Environment.NewLine + "Stack Trace: {1}", ex.Message, ex.StackTrace))
                Throw
            Finally
                ' Logger.Log.Info(String.Format("Populate User Role List Execution Ended"))
            End Try
        End Sub

        Function Verification(id As String, securitykey As String) As ActionResult
            Dim user As User
            Dim ResetPassword As New UserVerification
            Try
                ViewBag.Status = 0
                ViewBag.UserRoleId = 0

                user = userRepository.GetUserById(id)
                Dim str As String = securitykey

                If (user Is Nothing) Then
                    TempData("ErrorMessage") = String.Concat("It seems this user does not exist, Contact Administrator.")
                    Return View()
                End If

                If (user.SecurityKey = securitykey) Then
                    ResetPassword.User_Id = user.UserId
                    ResetPassword.UserRoleId = user.UserRoleId
                    ResetPassword.Status = user.Status

                    If ((user.UserRoleId > UserRoles.SAU) Or (user.UserRoleId = UserRoles.SAU And user.Status = StatusE.Active)) Then
                        ResetPassword.UserName = user.UserName
                    End If

                    ViewBag.Status = user.Status
                    ViewBag.UserRoleId = user.UserRoleId

                    Return View(ResetPassword)
                Else
                    TempData("ErrorMessage") = String.Concat("SecurityToken does not match, Contact Administrator.")
                    Return View()
                End If

            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Unable to Process Reset Password -", ex.Message)
                Logger.Log.Error(String.Format("Unable to Process Reset Password of User Id - {0} with Message - {1} " + Environment.NewLine + "Stack Trace: {1}", id, ex.Message, ex.StackTrace))
                Return View()
            Finally
                Logger.Log.Info(String.Format("Verification Function Execution Ended"))
            End Try

        End Function

        <HttpPost()> _
        Function Verification(User As UserVerification) As ActionResult

            Dim usr As User = Nothing
            Dim isAccExists As Boolean
            Dim usrOnRecurly As RecurlyAccount
            Try

                If ModelState.IsValid Then

                    Dim usernameAvailibility = utility.UserRepository().UsernameAvailibility(User.UserName).Where(Function(u) u.UserId <> User.User_Id)

                    usr = utility.UserRepository.GetUserByID(User.User_Id)

                    If (usr.UserRoleId = UserRoles.SAU) Then
                        isAccExists = RecurlyAccount.IsExist(usr.CustomerId)
                    Else
                        isAccExists = RecurlyAccount.IsExist(usr.UserId)
                    End If

                    If (isAccExists = False) Then

                        TempData("ErrorMessage") = String.Concat("This account doesn't exists!")

                    Else

                        If (usr.UserRoleId = UserRoles.SAU) Then
                            usrOnRecurly = RecurlyAccount.Get(usr.CustomerId.ToString())
                        Else
                            usrOnRecurly = RecurlyAccount.Get(usr.UserId.ToString())
                        End If

                        'usrOnRecurly = IIf(usr.UserRoleId = UserRoles.SAU, RecurlyAccount.Get(usr.CustomerId.ToString()), RecurlyAccount.Get(usr.UserId.ToString()))

                        If (usernameAvailibility.Count > 0) Then
                            ModelState.AddModelError("Username", "Someone already has that username. Try another.")
                        ElseIf (usr.Password = common.Encrypt(User.CurrentPassword)) Then

                            If (User.NewPassword.Equals(User.ConfirmNewPassword)) Then

                                usrOnRecurly.Username = User.UserName
                                usrOnRecurly.Update()

                                Logger.Log.Info(String.Format("User Role : {0} ", [Enum].GetName(GetType(UserRoles), usr.UserRoleId)))
                                Logger.Log.Info(String.Format("Update username on recurly of UserId : {0} , Username : {1} ", usr.UserId, usr.UserName))

                                usr.UserName = User.UserName
                                usr.Password = common.Encrypt(User.NewPassword)
                                usr.Status = CType(StatusE.Active, Integer)
                                utility.UserRepository.UpdateUser(usr)
                                utility.UserRepository.Save()
                                'userRepository.UpdateUser(usr)
                                'userRepository.Save()

                                TempData("Message") = "Password Reset Successfully."
                                Logger.Log.Info(String.Format("Password Reset Successfully of User : {0} , UserId : {1}", usr.UserName, usr.UserId))

                                Return RedirectToAction("Index", "Home")
                            Else
                                TempData("ErrorMessage") = String.Concat("New & Confirm Password does not match.")
                                Return View(User)
                            End If
                        Else
                            TempData("ErrorMessage") = String.Concat("Temporary password does not match.")
                            Return View(User)
                        End If

                    End If
                End If

                Return View(User)
            Catch ex As Exception
                TempData("ErrorMessage") = "Error in Password Reset."
                Logger.Log.Error(String.Format("Unable to Process Reset Password of User Id - {0} with Message - {1} " + Environment.NewLine + "Stack Trace: {1}", User.User_Id, ex.Message, ex.StackTrace))
                Return View()
            Finally
                Logger.Log.Info(String.Format("Verification Function Ended"))
            End Try
        End Function

        <HttpPost()>
         <CustomActionFilter()>
        Function EditProfile(User As ResetPassword) As ActionResult

            Logger.Log.Info(String.Format("\n User Profile update started {0}", User.UserId))
            Try

                If (ModelState.IsValid) Then

                    If (ValidateChangePassword(User) = False) Then
                        Return View(User)
                    End If

                    Dim errorMessage As String = String.Empty
                    Dim usr = userRepository.GetUserById(User.UserId)

                    Dim usernameAvailibility = utility.UserRepository().UsernameAvailibility(User.UserName).Where(Function(u) u.UserId <> User.UserId)

                    Dim emailAddressAvailibility = utility.UserRepository().EmailAddressAvailibility(User.UserEmail).Where(Function(u) u.UserId <> User.UserId)

                    If (User.ChangePassword = True) And Not (User.CurrentPassword Is Nothing And User.NewPassword Is Nothing And User.ConfirmNewPassword Is Nothing) Then

                        If (usr.Password = common.Encrypt(User.CurrentPassword)) Then
                            If (User.NewPassword.Equals(User.ConfirmNewPassword)) Then
                                usr.Password = common.Encrypt(User.NewPassword)
                            Else
                                TempData("ErrorMessage") = String.Concat("New & Confirm Password does not match.")
                                Return View(User)
                            End If
                        Else
                            TempData("ErrorMessage") = String.Concat("Current password does not match.")
                            Return View(User)
                        End If
                    End If

                    If (usernameAvailibility.Count > 0) Then
                        ModelState.AddModelError("Username", "Someone already has that username. Try another.")
                    ElseIf (emailAddressAvailibility.Count > 0) Then
                        ModelState.AddModelError("UserEmail", "Someone already has that Email Address. Try another.")
                    Else

                        If (User.UserRoleId = UserRoles.SAU) Then


                            Dim accountInfo As RecurlyAccount = RecurlyAccount.Get(usr.CustomerId)
                            Dim billingInfo As RecurlyBillingInfo = RecurlyBillingInfo.Get(usr.CustomerId)

                            accountInfo.FirstName = User.FirstName
                            accountInfo.LastName = User.LastName
                            accountInfo.Email = User.UserEmail
                            accountInfo.Username = User.UserName
                            accountInfo.Update()
                            Logger.Log.Info(String.Format("Update user's profile on recurly successfully, CustomerId : {0} , Username : {1} ", usr.CustomerId, User.UserName))

                            billingInfo.FirstName = User.FirstName
                            billingInfo.LastName = User.LastName
                            billingInfo.Update()
                            Logger.Log.Info(String.Format("Update user's billing information on recurly successfully, CustomerId : {0} , Username : {1} ", usr.CustomerId, User.UserName))

                            Dim subscriberInfo = utility.CustomerRepository.GetCustomerById(usr.CustomerId)
                            subscriberInfo.CustomerFirstName = User.FirstName
                            subscriberInfo.CustomerLastName = User.LastName
                            subscriberInfo.CustomerEmail = User.UserEmail
                            subscriberInfo.CustomerCompanyName = User.SubscriberName

                            utility.CustomerRepository.UpdateCustomer(subscriberInfo)
                            utility.CustomerRepository.Save()
                            Logger.Log.Info(String.Format("Update customer's information successfully on local database, CustomerId : {0} , Username : {1} ", usr.CustomerId, User.UserName))

                        End If

                        usr.UserName = User.UserName
                        usr.FirstName = User.FirstName
                        usr.LastName = User.LastName
                        usr.UserEmail = User.UserEmail

                        usr.UpdatedBy = User.UserId
                        userRepository.UpdateUser(usr)
                        userRepository.Save()
                        Logger.Log.Info(String.Format("Update user's information successfully on local database, UserId : {0} , Username : {1} ", User.UserId, User.UserName))

                        Session("UserInfo") = usr

                        TempData("Message") = "Profile updated Successfully."
                        Return RedirectToAction("Index")

                    End If
                End If

                Return View(User)
            Catch dataEx As DataException
                TempData("ErrorMessage") = String.Concat("Unable to update User-", dataEx.Message)
                Logger.Log.Error(String.Format("\n Unable to Update User id- {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2} ", User.UserId, dataEx.Message, dataEx.StackTrace))
            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Unable to update User-", ex.Message)
                Logger.Log.Error(String.Format("\n Unable to Update User id- {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2} ", User.UserId, ex.Message, ex.StackTrace))
            Finally
                Logger.Log.Info(String.Format("Execution Ended"))
            End Try

            Return View(User)
        End Function

        <CustomActionFilter()>
        Public Function EditProfile() As ActionResult
            Dim UserId = String.Empty
            Dim User As New ResetPassword()
            Try
                If Not (Session("UserInfo") Is Nothing) Then

                    UserInfo = DirectCast(Session("UserInfo"), User)
                    UserInfo = utility.UserRepository.GetUserByID(UserInfo.UserId)

                    UserId = User.UserId = UserInfo.UserId
                    User.UserName = UserInfo.UserName
                    User.FirstName = UserInfo.FirstName
                    User.LastName = UserInfo.LastName
                    User.CurrentPassword = UserInfo.Password
                    User.UserId = UserInfo.UserId
                    User.UserRoleId = UserInfo.UserRoleId
                    User.UserEmail = UserInfo.UserEmail

                    If (UserInfo.UserRoleId = UserRoles.SAU) Then
                        customerInfo = utility.CustomerRepository.GetCustomerById(UserInfo.CustomerId)
                        User.SubscriberName = customerInfo.CustomerCompanyName
                    Else
                        User.SubscriberName = "None"
                    End If

                Else
                    'To do : Session timeout 
                End If

                Return View(User)
            Catch dataEx As DataException
                TempData("ErrorMessage") = String.Concat("Unable to view user details to Edit User Information - ", dataEx.Message)
                Logger.Log.Error(String.Format("\n Unable to view user details to Edit User Information, UserID - {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2} ", UserId, dataEx.Message, dataEx.StackTrace))
            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Unable to view user details to Edit User Information - ", ex.Message)
                Logger.Log.Error(String.Format("\n Unable to Edit company id- {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2} ", UserId, ex.Message, ex.StackTrace))
            Finally
                Logger.Log.Info(String.Format("Edit User Execution Ended"))
            End Try
            Return Nothing
        End Function

        <CustomActionFilter()>
        Private Function ValidateChangePassword(User As ResetPassword)

            Dim Flag = True
            If (User.ChangePassword = True) And (User.CurrentPassword Is Nothing) Then
                ModelState.AddModelError("CurrentPassword", "Current Password is required.")
                Flag = False
            ElseIf (User.ChangePassword = True) And (User.NewPassword Is Nothing) Then
                ModelState.AddModelError("NewPassword", "New Password is required.")
                Flag = False
            ElseIf (User.ChangePassword = True) And (User.ConfirmNewPassword Is Nothing) Then
                ModelState.AddModelError("ConfirmNewPassword", "Confirm Password is required.")
                Flag = False
            ElseIf (User.ChangePassword = True) And (Not (User.NewPassword Is Nothing And User.ConfirmNewPassword Is Nothing)) Then

                If (Not User.NewPassword.Equals(User.ConfirmNewPassword)) Then
                    'ModelState.AddModelError("ConfirmNewPassword", "Confirm New Password and New Password do not match.")
                    Flag = False
                End If

            End If

            Return Flag
        End Function

        Function Logout()

            Session.Clear()
            Session.Abandon()
            Response.Cookies("ASP.NET_SessionId").Value = String.Empty
            Response.Cookies("ASP.NET_SessionId").Expires = DateTime.Now.AddMonths(-20)
            Return RedirectToAction("Index", "Home")
        End Function

        'Added the funtion to reset password.
        'we need to pass the userId as the parameter.
        <CustomActionFilter()>
        Public Function ResetPassword(userId As Integer, pathToRedirect As String) As ActionResult
            Logger.Log.Info(String.Format("ResetPassword Execution Started"))
            Dim userDetails As New User
            Try
                userDetails = userRepository.GetUserById(userId)
                Dim password As String = common.Generate(8, 2)
                userDetails.Password = common.Encrypt(password)
                userDetails.SecurityKey = Guid.NewGuid().ToString()
                userDetails.UpdatedBy = userId.ToString()
                userDetails.UpdatedOn = DateTime.UtcNow()
                userRepository.UpdateUser(userDetails)
                userRepository.Save()
                SendEmail(userDetails, EmailType.PasswordReset)
                TempData("Message") = "Reset Password link has been sent to your Email Id Successfully."

            Catch dbEntityEx As DbEntityValidationException

                For Each sError In dbEntityEx.EntityValidationErrors
                    Logger.Log.Error(String.Format("Entity of type ""{0}"" in state ""{1}"" has the following validation errors:", sError.Entry.Entity.[GetType].Name, sError.Entry.State))
                    For Each ve In sError.ValidationErrors
                        Logger.Log.Error(String.Format("-Property: {0}, Error: {1} ", ve.PropertyName, ve.ErrorMessage))
                    Next
                Next
            Catch ex As Exception
                TempData("ErrorMessage") = "Error occured, during reset password request - " + ex.Message.ToString()
                Logger.Log.Error(String.Format("Unable to Reset the password for {0} - " + Environment.NewLine + "{1}, Stack Trace: {2} ", userDetails.UserName, ex.Message, ex.StackTrace))
            Finally
                Logger.Log.Info(String.Format("ResetPassword Execution Ended"))
            End Try
            If (pathToRedirect = "user") Then
                Return RedirectToAction("Index")
            Else
                Return RedirectToAction("Index", "Subscribe")
            End If
        End Function
    End Class
End Namespace
