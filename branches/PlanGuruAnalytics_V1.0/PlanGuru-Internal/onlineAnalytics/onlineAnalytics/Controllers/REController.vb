﻿Imports FarPoint.Mvc.Spread
Imports System.Data.Entity
Namespace onlineAnalytics

    <CustAuthFilter()>
    Public Class REController
        Inherits System.Web.Mvc.Controller
        Dim sFormat As Integer = 1
        Dim sPeriod As Integer = Val(DateTime.Today.ToString("MM")) - 2
        Dim intFirstNumberColumn As Integer = 5
        Dim intRowTypeCol As Integer = 3
        Dim blnSTColVisible As Boolean = True
        Private intUserID As Integer
        Private intAnalysisID As Integer
        Private intFirstFiscal As Integer
        Private intNumberofPeriods As Integer
        Private accountRepository As IAccountRepository
        Private accountTypeRepository As IAccountTypeRepository
        Private balanceRepository As IBalanceRepository
        Private chartFormatRepository As IChartFormatRepository
        Private chartTypeRepository As IChartTypeRepository
        Private UserInfo As User
        Private typeDescList() As String = {"revenue", "expense"}

        Public Sub New()
            Me.chartFormatRepository = New ChartFormatRepository(New DataAccess())
            Me.chartTypeRepository = New ChartTypeRepository(New DataAccess())
            Me.accountRepository = New AccountRepository(New DataAccess())
            Me.accountTypeRepository = New AccountTypeRepository(New DataAccess())
            Me.balanceRepository = New BalanceRepository(New DataAccess())
        End Sub


        Public Sub New(chartFormatRepository As IChartFormatRepository)
            Me.chartFormatRepository = chartFormatRepository
        End Sub

        Public Sub New(chartTypeRepository As IChartTypeRepository)
            Me.chartTypeRepository = chartTypeRepository
        End Sub

        Public Sub New(accountRepository As IAccountRepository)
            Me.accountRepository = accountRepository
        End Sub

        Public Sub New(accountTypeRepository As IAccountTypeRepository)
            Me.accountTypeRepository = accountTypeRepository
        End Sub

        Public Sub New(balanceRepository As IBalanceRepository)
            Me.balanceRepository = balanceRepository
        End Sub

        'Display Account type & Account items with options.
        'List of Companies, List of Month, List of Format & Chart Type
        'Options to select (positive / negative) variance, percentage
        ' GET: /RE
        <HttpGet()> _
        <CustomActionFilter()>
        Function Index(<MvcSpread("spdAnalytics")> ByVal spdAnalytics As FpSpread, ByVal formValues As FormValues) As ActionResult
            Dim setupCountObj As SetupCount = Nothing
            Dim selectedAnalysis As Integer
            Try
                Logger.Log.Info(String.Format("REController Index (HttpGet) method execution starts"))
                If (Not Session("UserType") Is Nothing) And (Not Session("UserInfo") Is Nothing) Then
                    UserInfo = DirectCast(Session("UserInfo"), User)
                    sPeriod = Session("sPeriod")
                    sFormat = Session("sFormat")

                    If (UserInfo.AnalysisId = 0) Then
                        Return RedirectToAction("Index", "Dashboard")
                    Else
                        intAnalysisID = UserInfo.AnalysisId
                        intFirstFiscal = UserInfo.fiscalMonthOfSelectedCompany
                    End If
                End If

                If (UserInfo.AnalysisId > 0) Then
                    selectedAnalysis = UserInfo.AnalysisId
                Else
                    If (Not Session("SelectedAnalysisFromDropdown") Is Nothing) Then
                        selectedAnalysis = Session("SelectedAnalysisFromDropdown")
                    Else
                        selectedAnalysis = -1
                    End If
                End If
                Select Case sFormat

                End Select
                LoadSetupCount(setupCountObj, UserInfo, selectedAnalysis)

                Dim intCtr As Integer
                Dim periods As List(Of SelectListItem) = New List(Of SelectListItem)
                intNumberofPeriods = NumberofPeriods(sPeriod, intFirstFiscal)
                'For intCtr = 0 To intNumberofPeriods
                For intCtr = 0 To 11
                    periods.Add(New SelectListItem With {.Text = CommonProcedures.GetMonth(intCtr, intFirstFiscal), .Value = intCtr + 1, .Selected = False})
                Next
                ViewData("Periods") = New SelectList(periods, "value", "text", "May")

                Dim Formats As List(Of SelectListItem) = (From chartFormat In chartFormatRepository.GetChartFormats() Select New SelectListItem With {.Text = chartFormat.Format, .Value = chartFormat.ChartFormatId, .Selected = False}).ToList()

                ViewData("Formats") = New SelectList(Formats, "value", "text")

                Dim filterOn As List(Of SelectListItem) = New List(Of SelectListItem) From {
                    New SelectListItem With {.Text = "Current period amount", .Value = "Current", .Selected = False},
                    New SelectListItem With {.Text = "Variance amount", .Value = "Variance", .Selected = False},
                    New SelectListItem With {.Text = "Percent variance", .Value = "PercentVar", .Selected = True}
                }
                ViewData("FilterOn") = New SelectList(filterOn, "value", "text", "selected")

                Dim chartTypes As List(Of SelectListItem) = (From chartType In chartTypeRepository.GetChartTypes() Select New SelectListItem With {.Text = chartType.Type, .Value = chartType.ChartTypeId, .Selected = False}).ToList()

                ViewData("ChartType") = New SelectList(chartTypes, "value", "text", "selected")
                ViewData("blnHighVar") = False
                ViewData("blnShowPercent") = 0
                ViewData("intPeriod") = intNumberofPeriods + 1
                ViewData("intFormat") = sFormat + 1
                ViewData("intChartType") = 1
                ViewData("blnShowTrend") = False
                ViewData("blnShowBudget") = True
                ViewData("blnUseFilter") = False
                ViewData("intDept") = 1
                ViewData("Postback") = False
                Logger.Log.Info(String.Format("REController Index (HttpGet) method execution Ends"))
            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Error occured while loading Revenue & Expense Chart -", ex.Message)
                Logger.Log.Error(String.Format("REController Index (HttpGet) method execution ends with Error message: {0}, StackTrace: {1}", ex.Message, ex.StackTrace))

                LoadSetupCount(setupCountObj, UserInfo, selectedAnalysis)

            End Try

            Return View()
        End Function

        Private Sub LoadSetupCount(setupCountObj As SetupCount, UserInfo As User, selectedAnalysis As Integer)
            setupCountObj = Utility.CheckRequiredSetupCount(UserInfo, selectedAnalysis)
            ViewBag.Setup = setupCountObj
        End Sub

        'Post 
        'It will render the chart based on selected options available for accounts.
        <HttpPost()> _
        <CustomActionFilter()>
        Function Index(<MvcSpread("spdAnalytics")> ByVal spdAnalytics As FpSpread, <MvcSpread("spdChart")> ByVal spdChart As FpSpread, ByVal formValues As FormValues) As ActionResult
            ' Dim intCounter As Integer
            Dim setupCountObj As SetupCount = Nothing
            Dim selectedAnalysis As Integer
            Logger.Log.Info(String.Format("REController Index (HttpPost) method execution Starts"))
            Try
                If (Not Session("UserType") Is Nothing) And (Not Session("UserInfo") Is Nothing) Then
                    UserInfo = DirectCast(Session("UserInfo"), User)                   

                    If (UserInfo.AnalysisId = 0) Then
                        Return RedirectToAction("Index", "Dashboard")
                    Else
                        intAnalysisID = UserInfo.AnalysisId
                        intFirstFiscal = UserInfo.fiscalMonthOfSelectedCompany
                    End If
                End If

                If (UserInfo.AnalysisId > 0) Then
                    selectedAnalysis = UserInfo.AnalysisId
                Else
                    If (Not Session("SelectedAnalysisFromDropdown") Is Nothing) Then
                        selectedAnalysis = Session("SelectedAnalysisFromDropdown")
                    Else
                        selectedAnalysis = -1
                    End If
                End If

                LoadSetupCount(setupCountObj, UserInfo, selectedAnalysis)

                If (spdChart.Sheets(0).Charts.Count > 0) Then
                    spdChart.Sheets(0).Charts.Remove(spdChart.Sheets(0).Charts(0))
                End If

            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Index : Error occured while loading Revenue & Expense Chart -", ex.Message)
                Logger.Log.Error(String.Format("REController Index (HttpPost) method execution ends with Error message: {0}, StackTrace: {1}", ex.Message, ex.StackTrace))
                LoadSetupCount(setupCountObj, UserInfo, selectedAnalysis)
            End Try
            Try 
                spdAnalytics.Sheets(0).RowCount = 0
                'Select Case formValues.intFormat - 1
                '    Case 5
                '        'do nothing
                '    Case Else
                '        If formValues.intPeriod - 1 > sPeriod Then
                '            formValues.intPeriod = sPeriod + 1
                '        End If
                'End Select
                SetSpreadProperties(spdAnalytics, formValues.intFormat - 1, formValues.intPeriod - 1, formValues.blnShowasPer, formValues.blnShowBudget)
                '   Else
                If formValues.intChartType <> 1 Then
                    Charts.BuildChart(spdChart, spdAnalytics, formValues.intChartType, formValues.intFormat - 1, intFirstNumberColumn, formValues.blnShowasPer, formValues.blnShowTrend, True)
                    ViewData("blnUpdateChart") = True
                End If

                Dim periods As List(Of SelectListItem) = New List(Of SelectListItem)
                intNumberofPeriods = NumberofPeriods(sPeriod, intFirstFiscal)
                'For intCtr = 0 To intNumberofPeriods
                For intCtr = 0 To 11
                    periods.Add(New SelectListItem With {.Text = CommonProcedures.GetMonth(intCtr, intFirstFiscal), .Value = intCtr + 1, .Selected = False})
                Next
                ViewData("Periods") = New SelectList(periods, "value", "text", "May")

                Dim formats As List(Of SelectListItem) = New List(Of SelectListItem)
                For Each chartFormat As ChartFormat In chartFormatRepository.GetChartFormats()
                    formats.Add(New SelectListItem With {.Text = chartFormat.Format, .Value = chartFormat.ChartFormatId, .Selected = False})
                Next
                ViewData("Formats") = New SelectList(formats, "value", "text")

                Dim filterOn As List(Of SelectListItem) = New List(Of SelectListItem) From {
                    New SelectListItem With {.Text = "Current period amount", .Value = "Current", .Selected = False},
                    New SelectListItem With {.Text = "Variance amount", .Value = "Variance", .Selected = False},
                    New SelectListItem With {.Text = "Percent variance", .Value = "PercentVar", .Selected = True}
                }
                ViewData("FilterOn") = New SelectList(filterOn, "value", "text", "selected")

                Dim chartTypes As List(Of SelectListItem) = New List(Of SelectListItem)
                For Each chartType As ChartType In chartTypeRepository.GetChartTypes()
                    chartTypes.Add(New SelectListItem With {.Text = chartType.Type, .Value = chartType.ChartTypeId, .Selected = False})
                Next
                ViewData("ChartType") = New SelectList(chartTypes, "value", "text", "selected")
                ViewData("blnHighVar") = False
                ViewData("blnShowPercent") = 0
                ViewData("Period") = intNumberofPeriods + 1
                ViewData("intDept") = 1
                ViewData("intFormat") = formValues.intFormat
                ViewData("postback") = True

                Session("sPeriod") = formValues.intPeriod - 1
                Session("sFormat") = formValues.intFormat - 1
                Logger.Log.Info(String.Format("REController Index (HttpPost) method execution Ends"))
            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Index : Error occured while loading Revenue & Expense Chart -", ex.Message)
                Logger.Log.Error(String.Format("REController Index (HttpPost) method execution ends with Error message: {0}, StackTrace: {1}", ex.Message, ex.StackTrace))
                LoadSetupCount(setupCountObj, UserInfo, selectedAnalysis)
            End Try
            Return View()
        End Function

        <MvcSpreadEvent("Load", "spdAnalytics", DirectCast(Nothing, String()))> _
        Private Sub FpSpread_Load(ByVal sender As Object, ByVal e As EventArgs)
            Try
                Logger.Log.Info(String.Format("REController:FpSpread_Load method execution Starts"))
                Dim spread As FpSpread = DirectCast(sender, FpSpread)
                If Not spread.Page.IsPostBack Then
                    SetSpreadProperties(spread, sFormat, intNumberofPeriods, False, False)
                End If
                Logger.Log.Info(String.Format("REController:FpSpread_Load method execution Ends"))
            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("FpSpread_Load : Error occured while processing Revenue & Expense Chart -", ex.Message)
                Logger.Log.Error(String.Format("REController FpSpread_Load method execution ends with Error message: {0}, StackTrace: {1}", ex.Message, ex.StackTrace))
            End Try

        End Sub

        <MvcSpreadEvent("PreRender", "spdAnalytics", DirectCast(Nothing, String()))> _
        Private Sub FpSpread_PreRender(ByVal sender As Object, ByVal e As EventArgs)
            Try
                Logger.Log.Info(String.Format("REController:FpSpread_PreRender method execution Starts"))
                Dim spread As FpSpread = DirectCast(sender, FpSpread)
                Logger.Log.Info(String.Format("REController:FpSpread_PreRender method execution Ends"))
            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("FpSpread_PreRender : Error occured while processing Revenue & Expense Chart -", ex.Message)
                Logger.Log.Error(String.Format("REController FpSpread_PreRender method execution ends with Error message: {0}, StackTrace: {1}", ex.Message, ex.StackTrace))
            End Try
        End Sub

        Private Sub SetSpreadProperties(ByVal spdAnalytics As FpSpread, ByVal intFormat As Integer, ByVal intPeriod As Integer, ByRef blnShowasPercent As Boolean, ByVal blnShowBudget As Boolean)
            Try
                Logger.Log.Info(String.Format("REController:SetSpreadProperties method execution Starts"))
                Dim intColCount As Integer = 0
                Dim intNumberColumn As Integer = 0
                Dim intPercentColumn As Integer = 0
                Dim intCtr As Integer
                Dim intColCtr As Integer

                spdAnalytics.HorizontalScrollBarPolicy = FarPoint.Web.Spread.ScrollBarPolicy.Never
                spdAnalytics.Sheets(0).GridLineColor = Drawing.Color.LightGray
                'set column count
                Select Case intFormat
                    Case 0 To 4
                        intColCount = intFirstNumberColumn + 4
                        intNumberColumn = 3
                        intPercentColumn = 1
                    Case 5 'current year trend
                        If blnShowBudget = True Then
                            intNumberColumn = 11 + 2
                            intPercentColumn = 11 + 2
                        Else
                            intNumberColumn = intPeriod + 2
                            intPercentColumn = intPeriod + 2
                        End If
                        intColCount = intFirstNumberColumn + intNumberColumn
                    Case 6
                        intNumberColumn = 13
                        intPercentColumn = 13
                        intColCount = intFirstNumberColumn + 13
                    Case 7, 8
                        intNumberColumn = 6
                        intPercentColumn = 6
                        intColCount = intFirstNumberColumn + 6
                End Select
                CommonProcedures.SetCommonProperties(spdAnalytics, intColCount, blnShowasPercent, intFirstNumberColumn, intNumberColumn, intFormat)
                'load rows
                Select Case intFormat
                    Case 0 To 4
                        LoadSpread0to4(spdAnalytics, intFormat, intPeriod, intNumberColumn, intFirstNumberColumn, intColCount, blnShowasPercent)
                        spdAnalytics.Sheets(0).Columns(intFirstNumberColumn, intFirstNumberColumn + 3).Visible = True
                    Case 5
                        LoadSpread5(spdAnalytics, intFormat, intPeriod, intNumberColumn, intFirstNumberColumn, blnShowasPercent, intColCount, blnShowBudget)
                        spdAnalytics.Sheets(0).Columns(intFirstNumberColumn, intFirstNumberColumn + intNumberColumn - 1).Visible = True
                    Case 6
                        LoadSpread6(spdAnalytics, intFormat, intPeriod, intNumberColumn, intFirstNumberColumn, blnShowasPercent, intColCount)
                        spdAnalytics.Sheets(0).Columns(intFirstNumberColumn, intFirstNumberColumn + intNumberColumn - 1).Visible = True
                    Case 7, 8
                        LoadSpread7to8(spdAnalytics, intFormat, intPeriod, intNumberColumn, intFirstNumberColumn, blnShowasPercent, intColCount)
                        Dim blnLoop As Boolean = True
                        intCtr = 0
                        intNumberColumn = 6
                        For intColCtr = intFirstNumberColumn To intFirstNumberColumn + 5
                            spdAnalytics.Sheets(0).Columns(intColCtr).Visible = True
                        Next
                        intCtr = -1
                        Do While blnLoop = True
                            intCtr += 1
                            If spdAnalytics.Sheets(0).GetValue(intCtr, intRowTypeCol) = "Total" Then
                                For intColCtr = intFirstNumberColumn To intFirstNumberColumn + 5
                                    If spdAnalytics.Sheets(0).GetValue(intCtr, intColCtr) = 0 Then
                                        spdAnalytics.Sheets(0).Columns(intColCtr).Visible = False
                                        intColCount -= 1
                                        intNumberColumn -= 1
                                    Else
                                        blnLoop = False
                                        Exit For
                                    End If
                                Next
                            End If
                            If intCtr = spdAnalytics.Sheets(0).RowCount - 1 Then
                                blnLoop = False
                            End If
                        Loop
                End Select
                'set col widths
                CommonProcedures.SetColumnWidths(spdAnalytics, intColCount, intFirstNumberColumn, intFormat, intNumberColumn, blnSTColVisible, 4)
                If blnSTColVisible = True Then
                    spdAnalytics.Sheets(0).Columns(1).Width = 20
                Else
                    spdAnalytics.Sheets(0).Columns(1).Width = 0
                End If
                'load column headers
                CommonProcedures.SetColumnHeader(spdAnalytics, intFormat, intPeriod, intColCount, intFirstNumberColumn, intFirstFiscal, True)
                Logger.Log.Info(String.Format("REController:SetSpreadProperties method execution Ends"))
            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("SetSpreadProperties : Error occured while processing Revenue & Expense Chart -", ex.Message)
                Logger.Log.Error(String.Format("REController:SetSpreadProperties method execution ends with Error message: {0}, StackTrace: {1}", ex.Message, ex.StackTrace))
            End Try

        End Sub

        <CustomActionFilter()>
        Public Sub LoadSpread0to4(ByVal spdAnalytics As FpSpread, ByVal intFormat As Integer, ByVal intPeriod As Integer, ByVal intNumberColumn As Integer, ByVal intFirstNumColumn As Integer, ByVal intColumnCount As Integer, ByRef blnShowasPercent As Boolean)
            Try
                Logger.Log.Info(String.Format("REController:LoadSpread0to4 method execution Starts"))               
                Using utility As New Utility
                    Dim intRowCounter As Integer = -1
                    Dim intColumnCounter As Integer = 0
                    Dim strYearType(5) As String
                    Dim strPeriods(2) As String
                    Dim decAccumTotals(1) As Decimal
                    Dim decPeriodAmts(11) As Decimal
                    Dim decSaveAmts(4) As Decimal
                    Dim intSpanRows(1) As Integer
                    Dim chkbox As New FarPoint.Web.Spread.CheckBoxCellType
                    Dim blnTotalFlag As Boolean
                    Dim decIncAccumTotals(1) As Decimal
                    Dim strBrowser = Request.Browser.Browser
                    Dim strbrowerversion = Request.Browser.Version

                    chkbox.OnClientClick = CommonProcedures.CheckboxClicked(2)

                    Logger.Log.Info(String.Format("REController:LoadSpread0to4 --> selectedAccounts Query execution Starts"))

                    Dim selectedAccounts = (From a In utility.AccountRepository.GetAccountRecordsByAnalysisId(intAnalysisID)
                                            Join ac In utility.AccountTypeRepository.GetAccountTypeRecordsByAnalysisId(intAnalysisID, typeDescList)
                                            On a.AcctTypeId Equals ac.AcctTypeId Where a.AnalysisId = ac.AnalysisId
                                            Order By a.SortSequence
                                            Select a.AccountId, a.AcctTypeId, a.AnalysisId, a.AcctDescriptor, a.SortSequence, a.Description, a.Subgrouping, ac.TypeDesc, ac.ClassDesc
                                          ).ToList()

                    'Dim selectedAccounts = utility.AccountRepository.GetAccountsAndAccTypes(intAnalysisID, " 'revenue','expense' ")

                    Logger.Log.Info(String.Format("REController:LoadSpread0to4 --> selectedAccounts Query execution Ends"))

                    Logger.Log.Info(String.Format("REController:LoadSpread0to4 --> Balance Query execution Starts"))

                    Dim balanceAcc = (From b In utility.BalanceRepository.GetBalanceRecordsByAnalysisId(intAnalysisID)
                                   Join acc In selectedAccounts On b.AccountId Equals acc.AccountId
                                   Where acc.AnalysisId = b.AnalysisId
                                   Select b).ToList()

                    Logger.Log.Info(String.Format("REController:LoadSpread0to4 --> Balance Query execution Ends"))


                    Logger.Log.Info(String.Format("REController:LoadSpread0to4 --> selectedAccounts For Each execution Starts"))

                    If blnShowasPercent = True Then
                        For Each account In selectedAccounts
                            'save amounts                         
                            If account.AcctDescriptor = "Total" And account.TypeDesc = "Revenue" Then
                                Dim accountId As Integer = account.AccountId
                                For Each balance As Balance In (From b In balanceAcc.Where(Function(a) a.AccountId = accountId And a.AnalysisId = intAnalysisID) Select b)
                                    Select Case intFormat
                                        Case 0, 1
                                            GetBudgetAmts(balance, decPeriodAmts)
                                            CommonProcedures.AccumRunningTotals(intFormat, decAccumTotals, intPeriod, decPeriodAmts, 0)
                                            GetActualAmts(balance, decPeriodAmts)
                                            CommonProcedures.AccumRunningTotals(intFormat, decAccumTotals, intPeriod, decPeriodAmts, 1)
                                        Case 2
                                            GetActualAmts(balance, decPeriodAmts)
                                            If intPeriod = 0 Then
                                                CommonProcedures.AccumRunningTotals(intFormat, decAccumTotals, intPeriod, decPeriodAmts, 1)
                                                GetH5Amts(balance, decPeriodAmts)
                                                CommonProcedures.AccumRunningTotals(intFormat, decAccumTotals, intPeriod, decPeriodAmts, 0)
                                            Else
                                                CommonProcedures.AccumRunningTotals(intFormat, decAccumTotals, intPeriod - 1, decPeriodAmts, 0)
                                                CommonProcedures.AccumRunningTotals(intFormat, decAccumTotals, intPeriod, decPeriodAmts, 1)
                                            End If
                                        Case 3, 4
                                            GetH5Amts(balance, decPeriodAmts)
                                            CommonProcedures.AccumRunningTotals(intFormat, decAccumTotals, intPeriod, decPeriodAmts, 0)
                                            GetActualAmts(balance, decPeriodAmts)
                                            CommonProcedures.AccumRunningTotals(intFormat, decAccumTotals, intPeriod, decPeriodAmts, 1)
                                    End Select
                                Next
                                Exit For
                            End If
                        Next
                    End If

                    For Each account In selectedAccounts
                        'Dim accountTypeId As Integer = account.AcctTypeId
                        'Dim acctType = (From a In utility.AccountTypeRepository.GetAccountTypes.Where(Function(at) at.AcctTypeId = accountTypeId) Select a).FirstOrDefault()
                        With account
                            AddRow(spdAnalytics, intRowCounter, .Description, .AcctDescriptor, .TypeDesc, strBrowser, 4)
                            Select Case .AcctDescriptor
                                Case "Heading"
                                    FormatHeaderRow(spdAnalytics, intRowCounter, intSpanRows, blnTotalFlag, 4)
                                Case "SHeading"
                                    FormatSHeaderRow(spdAnalytics, intRowCounter, intSpanRows, blnTotalFlag, 4)
                                Case "STotal"
                                    FormatSTotalRow(spdAnalytics, intRowCounter, intFirstNumberColumn, strBrowser)
                                Case "Total"
                                    FormatTotalRow(spdAnalytics, intRowCounter, intSpanRows, blnTotalFlag, intFirstNumColumn, intNumberColumn, .ClassDesc, strBrowser, 4)
                                Case "GPTotal", "IOTotal", "IBTTotal", "NITotal"
                                    Array.Clear(intSpanRows, 0, 1)
                                    FormatGrandTotalRow(spdAnalytics, intRowCounter, blnTotalFlag, intFirstNumberColumn, intNumberColumn, .ClassDesc, strBrowser, 4)
                            End Select
                        End With
                        'save amounts                         
                        If account.AcctDescriptor <> "Heading" And account.AcctDescriptor <> "SHeading" Then
                            Array.Clear(decPeriodAmts, 0, 12)
                            Array.Clear(decSaveAmts, 0, 5)
                            Dim accountId As Integer = account.AccountId
                            spdAnalytics.Sheets(0).Cells(intRowCounter, 2).CellType = chkbox
                            For Each balance As Balance In (From b In balanceAcc.Where(Function(a) a.AccountId = accountId And a.AnalysisId = intAnalysisID) Select b)
                                Select Case intFormat
                                    Case 0
                                        GetBudgetAmts(balance, decPeriodAmts)
                                        decSaveAmts(0) = decPeriodAmts(intPeriod)
                                        GetActualAmts(balance, decPeriodAmts)
                                        decSaveAmts(1) = decPeriodAmts(intPeriod)
                                    Case 1
                                        GetBudgetAmts(balance, decPeriodAmts)
                                        For intColumnCounter = 0 To intPeriod
                                            decSaveAmts(0) += decPeriodAmts(intColumnCounter)
                                        Next
                                        GetActualAmts(balance, decPeriodAmts)
                                        For intColumnCounter = 0 To intPeriod
                                            decSaveAmts(1) += decPeriodAmts(intColumnCounter)
                                        Next
                                    Case 2
                                        GetActualAmts(balance, decPeriodAmts)
                                        If intPeriod = 0 Then
                                            decSaveAmts(1) = decPeriodAmts(intPeriod)
                                            GetH5Amts(balance, decPeriodAmts)
                                            decSaveAmts(0) = decPeriodAmts(11)
                                        Else
                                            decSaveAmts(0) = decPeriodAmts(intPeriod - 1)
                                            decSaveAmts(1) = decPeriodAmts(intPeriod)
                                        End If
                                    Case 3
                                        GetH5Amts(balance, decPeriodAmts)
                                        decSaveAmts(0) = decPeriodAmts(intPeriod)
                                        GetActualAmts(balance, decPeriodAmts)
                                        decSaveAmts(1) = decPeriodAmts(intPeriod)
                                    Case 4
                                        GetH5Amts(balance, decPeriodAmts)
                                        For intColumnCounter = 0 To intPeriod
                                            decSaveAmts(0) += decPeriodAmts(intColumnCounter)
                                        Next
                                        GetActualAmts(balance, decPeriodAmts)
                                        For intColumnCounter = 0 To intPeriod
                                            decSaveAmts(1) += decPeriodAmts(intColumnCounter)
                                        Next
                                End Select
                            Next


                            'store values
                            If blnShowasPercent = False Then
                                spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumColumn, decSaveAmts(0))
                                spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumColumn + 1, decSaveAmts(1))
                            Else
                                If decAccumTotals(0) <> 0 Then
                                    spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumColumn, decSaveAmts(0) / decAccumTotals(0))
                                    decSaveAmts(0) = decSaveAmts(0) / decAccumTotals(0)
                                Else
                                    spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumColumn, 0)
                                    decSaveAmts(0) = 0
                                End If
                                If decAccumTotals(1) <> 0 Then
                                    spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumColumn + 1, decSaveAmts(1) / decAccumTotals(1))
                                    decSaveAmts(1) = decSaveAmts(1) / decAccumTotals(1)
                                Else
                                    spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumColumn + 1, 0)
                                    decSaveAmts(1) = 0
                                End If

                            End If
                            'compute(variances)
                            Select Case account.AcctDescriptor
                                Case "GPTotal", "IOTotal", "IBTTotal", "NITotal"
                                    CommonProcedures.ComputeVariances(decSaveAmts, "Reverse", decAccumTotals, blnShowasPercent)
                                Case Else
                                    Select Case account.ClassDesc
                                        Case "Other Income(Expense)"
                                            CommonProcedures.ComputeVariances(decSaveAmts, "Reverse", decAccumTotals, blnShowasPercent)
                                        Case Else
                                            CommonProcedures.ComputeVariances(decSaveAmts, account.TypeDesc, decAccumTotals, blnShowasPercent)
                                    End Select

                            End Select

                            spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumColumn + 2, decSaveAmts(2))
                            spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumColumn + 3, decSaveAmts(3))
                        End If
                        'If account.AcctDescriptor = "Total" Then
                        '    AccumNI(spdAnalytics, account.AcctType.ClassDesc, intFormat, 2, decSaveAmts, intColumnCount, decIncAccumTotals, intRowCounter, intPeriod)
                        'End If
                    Next 
                    Logger.Log.Info(String.Format("REController:LoadSpread0to4 --> selectedAccounts For Each execution Ends"))
                    '    CommonProcedures.AddTotalRow(spdAnalytics, intFormat, intNumberColumn, intFirstNumColumn, intColumnCount, decIncAccumTotals, intRowCounter, "NI", "Net Income(Loss)", intPeriod)
                End Using
                Logger.Log.Info(String.Format("REController:LoadSpread0to4 method execution Ends"))
            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("LoadSpread0to4 : Error occured while processing Revenue & Expense Chart - ", ex.Message)
                Logger.Log.Error(String.Format("REController:LoadSpread0to4 method execution ends with Error message: {0}, StackTrace: {1}", ex.Message, ex.StackTrace))
            End Try

        End Sub

        <CustomActionFilter()>
        Public Sub LoadSpread5(ByVal spdAnalytics As FpSpread, ByVal intFormat As Integer, ByVal intCurrentMonth As Integer, ByVal intNumberColumn As Integer, ByVal intFirstNumberColumn As Integer, ByRef blnShowasPercent As Boolean, ByVal intColumnCount As Integer, ByVal blnShowBudget As Boolean)
            Try
                Logger.Log.Info(String.Format("REController:LoadSpread5 method execution Starts"))

                Using utility As New Utility()
                    Dim intRowCounter As Integer = -1
                    Dim intColumnCounter As Integer
                    Dim strYearType(5) As String
                    Dim strPeriods(2) As String
                    Dim decAccumTotals(12) As Decimal
                    Dim decPeriodAmts(11) As Decimal
                    Dim decSaveAmts(12) As Decimal
                    Dim intSpanRows(1) As Integer
                    Dim blnTotalFlag As Boolean
                    Dim intPeriods As Integer
                    Dim decIncAccumTotals(intNumberColumn) As Decimal
                    Dim chkbox As New FarPoint.Web.Spread.CheckBoxCellType
                    Dim strBrowser As String = Request.Browser.Browser

                    chkbox.OnClientClick = CommonProcedures.CheckboxClicked(2)
                    If blnShowBudget = False Then
                        intPeriods = intCurrentMonth
                    Else
                        intPeriods = 11
                    End If

                    Dim selectedAccounts = (From a In utility.AccountRepository.GetAccountRecordsByAnalysisId(intAnalysisID)
                                           Join ac In utility.AccountTypeRepository.GetAccountTypeRecordsByAnalysisId(intAnalysisID, typeDescList)
                                           On a.AcctTypeId Equals ac.AcctTypeId Where a.AnalysisId = ac.AnalysisId
                                           Order By a.SortSequence
                                           Select a.AccountId, a.AcctTypeId, a.AnalysisId, a.AcctDescriptor, a.SortSequence, a.Description, a.Subgrouping, ac.TypeDesc, ac.ClassDesc
                                         ).ToList()

                    Logger.Log.Info(String.Format("REController:LoadSpread5 --> Balance Query execution Starts"))
                    Dim balanceAcc = (From b In utility.BalanceRepository.GetBalanceRecordsByAnalysisId(intAnalysisID)
                                    Join acc In selectedAccounts On b.AccountId Equals acc.AccountId
                                    Where acc.AnalysisId = b.AnalysisId
                                    Select b).ToList()
                    Logger.Log.Info(String.Format("REController:LoadSpread5 --> Balance Query execution Starts"))

                    If blnShowasPercent = True Then
                        For Each account In selectedAccounts
                            'save amounts total revenue amounts for computing %                      
                            If account.AcctDescriptor = "Total" And account.TypeDesc = "Revenue" Then
                                Dim accountId As Integer = account.AccountId
                                For Each balance As Balance In (From b In balanceAcc.Where(Function(a) a.AccountId = accountId And a.AnalysisId = intAnalysisID) Select b)
                                    GetActualAmts(balance, decPeriodAmts)
                                    For intColumnCounter = 0 To intCurrentMonth
                                        decAccumTotals(intColumnCounter) += decPeriodAmts(intColumnCounter)
                                        decAccumTotals(intPeriods + 1) += decPeriodAmts(intColumnCounter)
                                    Next
                                    If intCurrentMonth < intPeriods Then
                                        GetBudgetAmts(balance, decPeriodAmts)
                                        For intColumnCounter = intCurrentMonth + 1 To intPeriods
                                            decAccumTotals(intColumnCounter) = decPeriodAmts(intColumnCounter)
                                            decAccumTotals(intPeriods + 1) += decPeriodAmts(intColumnCounter)
                                        Next
                                    End If
                                Next
                                Exit For
                            End If
                        Next
                    End If


                    For Each account In selectedAccounts

                        CommonProcedures.AddRow(spdAnalytics, intRowCounter, account.Description, account.AcctDescriptor, account.TypeDesc, strBrowser, 4)
                        Select Case account.AcctDescriptor
                            Case "Heading"
                                CommonProcedures.FormatHeaderRow(spdAnalytics, intRowCounter, intSpanRows, blnTotalFlag, 4)
                            Case "SHeading"
                                FormatSHeaderRow(spdAnalytics, intRowCounter, intSpanRows, blnTotalFlag, 4)
                            Case "STotal"
                                FormatSTotalRow(spdAnalytics, intRowCounter, intFirstNumberColumn, strBrowser)
                            Case "Total"
                                CommonProcedures.FormatTotalRow(spdAnalytics, intRowCounter, intSpanRows, blnTotalFlag, intFirstNumberColumn, intNumberColumn, account.ClassDesc, strBrowser, 4)
                                'Case "Detail"
                                '    spdAnalytics.Sheets(0).Cells(intRowCounter, 0).Border.BorderColorBottom = Drawing.Color.White
                            Case "GPTotal", "IOTotal", "IBTTotal", "NITotal"
                                Array.Clear(intSpanRows, 0, 1)
                                CommonProcedures.FormatGrandTotalRow(spdAnalytics, intRowCounter, blnTotalFlag, intFirstNumberColumn, intNumberColumn, account.ClassDesc, strBrowser, 4)
                        End Select
                        'save amounts                         
                        If account.AcctDescriptor <> "Heading" And account.AcctDescriptor <> "SHeading" Then
                            Array.Clear(decPeriodAmts, 0, 12)
                            Array.Clear(decSaveAmts, 0, 13)
                            Dim accountId As Integer = account.AccountId
                            spdAnalytics.Sheets(0).Cells(intRowCounter, 2).CellType = chkbox
                            For Each balance As Balance In (From b In balanceAcc.Where(Function(a) a.AccountId = accountId And a.AnalysisId = intAnalysisID) Select b)
                                GetActualAmts(balance, decPeriodAmts)
                                If blnShowasPercent = False Then
                                    For intColumnCounter = 0 To intCurrentMonth
                                        spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumberColumn + intColumnCounter, decPeriodAmts(intColumnCounter))
                                        decSaveAmts(intColumnCounter) = decPeriodAmts(intColumnCounter)
                                        decSaveAmts(intPeriods + 1) += decPeriodAmts(intColumnCounter)
                                    Next
                                Else
                                    For intColumnCounter = 0 To intCurrentMonth
                                        If decAccumTotals(intColumnCounter) <> 0 Then
                                            spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumberColumn + intColumnCounter, decPeriodAmts(intColumnCounter) / decAccumTotals(intColumnCounter))
                                            decSaveAmts(intColumnCounter) = decPeriodAmts(intColumnCounter) / decAccumTotals(intColumnCounter)
                                        Else
                                            spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumberColumn + intColumnCounter, 0)
                                            decSaveAmts(intColumnCounter) = 0
                                        End If
                                        decSaveAmts(intPeriods + 1) += decPeriodAmts(intColumnCounter)
                                    Next
                                End If
                                If blnShowBudget = True Then
                                    GetBudgetAmts(balance, decPeriodAmts)
                                    For intColumnCounter = intCurrentMonth + 1 To intPeriods
                                        If blnShowasPercent = False Then
                                            spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumberColumn + intColumnCounter, decPeriodAmts(intColumnCounter))
                                            decSaveAmts(intColumnCounter) = decPeriodAmts(intColumnCounter)
                                            decSaveAmts(intPeriods + 1) += decPeriodAmts(intColumnCounter)
                                        Else
                                            If decAccumTotals(intColumnCounter) <> 0 Then
                                                spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumberColumn + intColumnCounter, decPeriodAmts(intColumnCounter) / decAccumTotals(intColumnCounter))
                                                decSaveAmts(intColumnCounter) = decPeriodAmts(intColumnCounter) / decAccumTotals(intColumnCounter)
                                            Else
                                                spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumberColumn + intColumnCounter, 0)
                                                decSaveAmts(intColumnCounter) = 0
                                            End If
                                            decSaveAmts(intPeriods + 1) += decPeriodAmts(intColumnCounter)
                                        End If
                                        spdAnalytics.Sheets(0).Cells(intRowCounter, intFirstNumberColumn + intColumnCounter).ForeColor = Drawing.Color.FromArgb(5, 51, 97)
                                    Next
                                End If
                                'save amounts for total column
                                If blnShowasPercent = False Then
                                    spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumberColumn + intPeriods + 1, decSaveAmts(intPeriods + 1))
                                Else
                                    If decAccumTotals(intPeriods + 1) <> 0 Then
                                        spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumberColumn + intPeriods + 1, decSaveAmts(intPeriods + 1) / decAccumTotals(intPeriods + 1))
                                    Else
                                        spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumberColumn + intPeriods + 1, 0)
                                    End If
                                End If
                                If intCurrentMonth < intPeriods Then
                                    spdAnalytics.Sheets(0).Cells(intRowCounter, intFirstNumberColumn + intPeriods + 1).ForeColor = Drawing.Color.FromArgb(5, 51, 97)
                                End If
                            Next
                        End If
                    Next
                End Using
                Logger.Log.Info(String.Format("REController:LoadSpread5 method execution Ends"))
            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("LoadSpread5 : Error occured while processing Revenue & Expense Chart - ", ex.Message)
                Logger.Log.Error(String.Format("REController:LoadSpread5 method execution ends with Error message: {0}, StackTrace: {1}", ex.Message, ex.StackTrace))
            End Try
        End Sub

        <CustomActionFilter()>
        Public Sub LoadSpread6(ByVal spdAnalytics As FpSpread, ByVal intFormat As Integer, ByVal intPeriod As Integer, ByVal intNumberColumn As Integer, ByVal intFirstNumberColumn As Integer, ByRef blnShowasPercent As Boolean, ByVal intColumnCount As Integer)
            Try
                Logger.Log.Info(String.Format("REController:LoadSpread6 method execution Starts"))

                Using utility As New Utility()
                    Dim intRowCounter As Integer = -1
                    Dim intColumnCounter As Integer
                    Dim strYearType(5) As String
                    Dim strPeriods(2) As String
                    Dim decAccumTotals(12) As Decimal
                    Dim decPeriodAmts(11) As Decimal
                    Dim decSaveAmts(12) As Decimal
                    Dim intSpanRows(1) As Integer
                    Dim blnTotalFlag As Boolean
                    Dim decIncAccumTotals(intNumberColumn) As Decimal
                    Dim chkbox As New FarPoint.Web.Spread.CheckBoxCellType
                    Dim intCurrentMonth As Integer = Val(DateTime.Today.ToString("MM")) - 2
                    Dim strBrowser As String = Request.Browser.Browser

                    chkbox.OnClientClick = CommonProcedures.CheckboxClicked(2)

                    Dim selectedAccounts = (From a In utility.AccountRepository.GetAccountRecordsByAnalysisId(intAnalysisID)
                                           Join ac In utility.AccountTypeRepository.GetAccountTypeRecordsByAnalysisId(intAnalysisID, typeDescList)
                                           On a.AcctTypeId Equals ac.AcctTypeId Where a.AnalysisId = ac.AnalysisId
                                           Order By a.SortSequence
                                           Select a.AccountId, a.AcctTypeId, a.AnalysisId, a.AcctDescriptor, a.SortSequence, a.Description, a.Subgrouping, ac.TypeDesc, ac.ClassDesc
                                         ).ToList()

                    Dim balanceAcc = (From b In utility.BalanceRepository.GetBalanceRecordsByAnalysisId(intAnalysisID)
                                   Join acc In selectedAccounts On b.AccountId Equals acc.AccountId
                                   Where b.AnalysisId = acc.AnalysisId
                                   Select b).ToList()

                    If blnShowasPercent = True Then
                        For Each account In selectedAccounts
                            'save amounts total revenue amounts for computing %                      
                            If account.AcctDescriptor = "Total" And account.TypeDesc = "Revenue" Then
                                Dim accountId As Integer = account.AccountId
                                For Each balance As Balance In (From b In balanceAcc.Where(Function(a) a.AccountId = accountId And a.AnalysisId = intAnalysisID) Select b)
                                    GetH5Amts(balance, decPeriodAmts)
                                    If intPeriod < 11 Then
                                        For intColumnCounter = intPeriod + 1 To 11
                                            decAccumTotals(intColumnCounter) = decPeriodAmts(intColumnCounter)
                                            decAccumTotals(12) += decPeriodAmts(intColumnCounter)
                                        Next
                                    End If
                                    GetActualAmts(balance, decPeriodAmts)
                                    For intColumnCounter = 0 To intPeriod
                                        decAccumTotals(intColumnCounter) = decPeriodAmts(intColumnCounter)
                                        decAccumTotals(12) += decPeriodAmts(intColumnCounter)
                                    Next
                                Next
                                Exit For
                            End If
                        Next
                    End If

                    For Each account In selectedAccounts
                        CommonProcedures.AddRow(spdAnalytics, intRowCounter, account.Description, account.AcctDescriptor, account.TypeDesc, strBrowser, 4)
                        Select Case account.AcctDescriptor
                            Case "Heading"
                                CommonProcedures.FormatHeaderRow(spdAnalytics, intRowCounter, intSpanRows, blnTotalFlag, 4)
                            Case "SHeading"
                                FormatSHeaderRow(spdAnalytics, intRowCounter, intSpanRows, blnTotalFlag, 4)
                            Case "STotal"
                                FormatSTotalRow(spdAnalytics, intRowCounter, intFirstNumberColumn, strBrowser)
                            Case "Total"
                                CommonProcedures.FormatTotalRow(spdAnalytics, intRowCounter, intSpanRows, blnTotalFlag, intFirstNumberColumn, intNumberColumn, account.ClassDesc, strBrowser, 4)
                                'Case "Detail"
                                '    spdAnalytics.Sheets(0).Cells(intRowCounter, 0).Border.BorderColorBottom = Drawing.Color.White
                            Case "GPTotal", "IOTotal", "IBTTotal", "NITotal"
                                Array.Clear(intSpanRows, 0, 1)
                                CommonProcedures.FormatGrandTotalRow(spdAnalytics, intRowCounter, blnTotalFlag, intFirstNumberColumn, intNumberColumn, account.ClassDesc, strBrowser, 4)
                        End Select
                        'save amounts
                        If account.AcctDescriptor <> "Heading" And account.AcctDescriptor <> "SHeading" Then
                            Array.Clear(decPeriodAmts, 0, 12)
                            Array.Clear(decSaveAmts, 0, 13)
                            Dim accountId As Integer = account.AccountId
                            spdAnalytics.Sheets(0).Cells(intRowCounter, 2).CellType = chkbox
                            For Each balance As Balance In (From b In balanceAcc.Where(Function(a) a.AccountId = accountId And a.AnalysisId = intAnalysisID) Select b)
                                If intPeriod < 11 Then
                                    GetH5Amts(balance, decPeriodAmts)
                                    For intColumnCounter = intPeriod + 1 To 11
                                        If blnShowasPercent = False Then
                                            spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumberColumn + intColumnCounter - (intPeriod + 1), decPeriodAmts(intColumnCounter))
                                            decSaveAmts(intColumnCounter) = decPeriodAmts(intColumnCounter)
                                            decSaveAmts(12) += decPeriodAmts(intColumnCounter)
                                        Else
                                            If decAccumTotals(intColumnCounter) <> 0 Then
                                                spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumberColumn + intColumnCounter - (intPeriod + 1), decPeriodAmts(intColumnCounter) / decAccumTotals(intColumnCounter))
                                                decSaveAmts(intColumnCounter) = decPeriodAmts(intColumnCounter) / decAccumTotals(intColumnCounter)
                                            Else
                                                spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumberColumn + intColumnCounter - (intPeriod + 1), 0)
                                                decSaveAmts(intColumnCounter) = 0
                                            End If
                                            decSaveAmts(12) += decPeriodAmts(intColumnCounter)
                                        End If
                                    Next
                                End If
                                GetActualAmts(balance, decPeriodAmts)
                                For intColumnCounter = 0 To intPeriod
                                    If blnShowasPercent = False Then
                                        spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumberColumn + intColumnCounter + (11 - intPeriod), decPeriodAmts(intColumnCounter))
                                        decSaveAmts(intColumnCounter) = decPeriodAmts(intColumnCounter)
                                        decSaveAmts(12) += decPeriodAmts(intColumnCounter)
                                    Else
                                        If decAccumTotals(intColumnCounter) <> 0 Then
                                            spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumberColumn + intColumnCounter + (11 - intPeriod), decPeriodAmts(intColumnCounter) / decAccumTotals(intColumnCounter))
                                            decSaveAmts(intColumnCounter) = decPeriodAmts(intColumnCounter) / decAccumTotals(intColumnCounter)
                                        Else
                                            spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumberColumn + intColumnCounter + (11 - intPeriod), 0)
                                            decSaveAmts(intColumnCounter) = 0
                                        End If
                                        decSaveAmts(12) += decPeriodAmts(intColumnCounter)
                                    End If
                                Next
                            Next
                            If blnShowasPercent = False Then
                                spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumberColumn + 12, decSaveAmts(12))
                            Else
                                If decAccumTotals(12) <> 0 Then
                                    spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumberColumn + 12, decSaveAmts(12) / decAccumTotals(12))
                                    decSaveAmts(12) = decSaveAmts(12) / decAccumTotals(12)
                                Else
                                    spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumberColumn + 12, 0)
                                    decSaveAmts(12) = 0
                                End If

                            End If
                        End If
                    Next

                End Using
                Logger.Log.Info(String.Format("REController:LoadSpread6 method execution Ends"))
            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("LoadSpread6 : Error occured while processing Revenue & Expense Chart - ", ex.Message)
                Logger.Log.Error(String.Format("REController:LoadSpread6 method execution ends with Error message: {0}, StackTrace: {1}", ex.Message, ex.StackTrace))

            End Try


        End Sub

        <CustomActionFilter()>
        Public Sub LoadSpread7to8(ByVal spdAnalytics As FpSpread, ByVal intFormat As Integer, ByVal intPeriod As Integer, ByVal intNumberColumn As Integer, ByVal intFirstNumberColumn As Integer, ByRef blnShowasPercent As Boolean, ByVal intColumnCount As Integer)
            Try
                Logger.Log.Info(String.Format("REController:LoadSpread7to8 method execution Starts"))
                Using utility As New Utility

                    Dim intRowCounter As Integer = -1
                    Dim intColumnCounter As Integer
                    Dim intTypeCol As Integer
                    Dim strYearType(5) As String
                    Dim strPeriods(2) As String
                    Dim decPeriodAmts(11) As Decimal
                    Dim decAccumTotals(5) As Decimal
                    Dim decSaveAmts(12) As Decimal
                    Dim intSpanRows(1) As Integer
                    Dim blnTotalFlag As Boolean
                    Dim decIncAccumTotals(intNumberColumn) As Decimal
                    Dim chkbox As New FarPoint.Web.Spread.CheckBoxCellType
                    Dim strBrowser As String = Request.Browser.Browser

                    chkbox.OnClientClick = CommonProcedures.CheckboxClicked(2)

                    Dim selectedAccounts = (From a In utility.AccountRepository.GetAccountRecordsByAnalysisId(intAnalysisID)
                                           Join ac In utility.AccountTypeRepository.GetAccountTypeRecordsByAnalysisId(intAnalysisID, typeDescList)
                                           On a.AcctTypeId Equals ac.AcctTypeId Where a.AnalysisId = ac.AnalysisId
                                           Order By a.SortSequence
                                           Select a.AccountId, a.AcctTypeId, a.AnalysisId, a.AcctDescriptor, a.SortSequence, a.Description, a.Subgrouping, ac.TypeDesc, ac.ClassDesc
                                         ).ToList()

                    Dim balanceAcc = (From b In utility.BalanceRepository.GetBalanceRecordsByAnalysisId(intAnalysisID)
                                   Join acc In selectedAccounts On b.AccountId Equals acc.AccountId
                                   Where b.AnalysisId = acc.AnalysisId
                                   Select b).ToList()

                    If blnShowasPercent = True Then
                        For Each account In selectedAccounts
                            'save amounts total revenue amounts for computing %                      
                            If account.AcctDescriptor = "Total" And account.TypeDesc = "Revenue" Then
                                Dim accountId As Integer = account.AccountId
                                For Each balance As Balance In (From b In balanceAcc.Where(Function(a) a.AccountId = accountId And a.AnalysisId = intAnalysisID) Select b)
                                    GetH1Amts(balance, decPeriodAmts)
                                    CommonProcedures.AccumRunningTotals(intFormat, decAccumTotals, intPeriod, decPeriodAmts, 0)
                                    GetH2Amts(balance, decPeriodAmts)
                                    CommonProcedures.AccumRunningTotals(intFormat, decAccumTotals, intPeriod, decPeriodAmts, 1)
                                    GetH3Amts(balance, decPeriodAmts)
                                    CommonProcedures.AccumRunningTotals(intFormat, decAccumTotals, intPeriod, decPeriodAmts, 2)
                                    GetH4Amts(balance, decPeriodAmts)
                                    CommonProcedures.AccumRunningTotals(intFormat, decAccumTotals, intPeriod, decPeriodAmts, 3)
                                    GetH5Amts(balance, decPeriodAmts)
                                    CommonProcedures.AccumRunningTotals(intFormat, decAccumTotals, intPeriod, decPeriodAmts, 4)
                                    GetActualAmts(balance, decPeriodAmts)
                                    CommonProcedures.AccumRunningTotals(intFormat, decAccumTotals, intPeriod, decPeriodAmts, 5)
                                Next
                                Exit For
                            End If
                        Next
                    End If

                    For Each account In selectedAccounts

                        'Dim accountTypeId As Integer = account.AcctTypeId
                        'Dim acctType = (From a In utility.AccountTypeRepository.GetAccountTypes.Where(Function(at) at.AcctTypeId = accountTypeId) Select a).FirstOrDefault()

                        CommonProcedures.AddRow(spdAnalytics, intRowCounter, account.Description, account.AcctDescriptor, account.TypeDesc, strBrowser, 4)
                        Select Case account.AcctDescriptor
                            Case "Heading"
                                CommonProcedures.FormatHeaderRow(spdAnalytics, intRowCounter, intSpanRows, blnTotalFlag, 4)
                            Case "SHeading"
                                FormatSHeaderRow(spdAnalytics, intRowCounter, intSpanRows, blnTotalFlag, 4)
                            Case "STotal"
                                FormatSTotalRow(spdAnalytics, intRowCounter, intFirstNumberColumn, strBrowser)
                            Case "Total"
                                CommonProcedures.FormatTotalRow(spdAnalytics, intRowCounter, intSpanRows, blnTotalFlag, intFirstNumberColumn, intNumberColumn, account.ClassDesc, strBrowser, 4)
                                'Case "Detail"
                                '    spdAnalytics.Sheets(0).Cells(intRowCounter, 0).Border.BorderColorBottom = Drawing.Color.White
                            Case "GPTotal", "IOTotal", "IBTTotal", "NITotal"
                                Array.Clear(intSpanRows, 0, 1)
                                CommonProcedures.FormatGrandTotalRow(spdAnalytics, intRowCounter, blnTotalFlag, intFirstNumberColumn, intNumberColumn, account.ClassDesc, strBrowser, 4)
                        End Select
                        'save amounts 
                        If account.AcctDescriptor <> "Heading" And account.AcctDescriptor <> "SHeading" Then
                            Array.Clear(decPeriodAmts, 0, 12)
                            Array.Clear(decSaveAmts, 0, 5)
                            Dim accountId As Integer = account.AccountId
                            spdAnalytics.Sheets(0).Cells(intRowCounter, 2).CellType = chkbox
                            For Each balance As Balance In (From b In balanceAcc.Where(Function(a) a.AccountId = accountId And a.AnalysisId = intAnalysisID) Select b)
                                For intTypeCol = 0 To 5
                                    Select Case intTypeCol
                                        Case 0
                                            GetH1Amts(balance, decPeriodAmts)
                                        Case 1
                                            GetH2Amts(balance, decPeriodAmts)
                                        Case 2
                                            GetH3Amts(balance, decPeriodAmts)
                                        Case 3
                                            GetH4Amts(balance, decPeriodAmts)
                                        Case 4
                                            GetH5Amts(balance, decPeriodAmts)
                                        Case 5
                                            GetActualAmts(balance, decPeriodAmts)
                                    End Select
                                    Select Case intFormat
                                        Case 7
                                            If blnShowasPercent = False Then
                                                spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumberColumn + intTypeCol, decPeriodAmts(intPeriod))
                                                decSaveAmts(intTypeCol) = decPeriodAmts(intPeriod)
                                            Else
                                                If decAccumTotals(intTypeCol) <> 0 Then
                                                    spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumberColumn + intTypeCol, decPeriodAmts(intPeriod) / decAccumTotals(intTypeCol))
                                                    decSaveAmts(intTypeCol) = decPeriodAmts(intPeriod) / decAccumTotals(intTypeCol)
                                                Else
                                                    spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumberColumn + intTypeCol, 0)
                                                    decSaveAmts(intTypeCol) = 0
                                                End If
                                            End If
                                        Case 8
                                            decSaveAmts(intTypeCol) = 0
                                            For intColumnCounter = 0 To intPeriod
                                                decSaveAmts(intTypeCol) += decPeriodAmts(intColumnCounter)
                                            Next
                                            If blnShowasPercent = False Then
                                                spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumberColumn + intTypeCol, decSaveAmts(intTypeCol))
                                            Else
                                                If decAccumTotals(intTypeCol) <> 0 Then
                                                    spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumberColumn + intTypeCol, decSaveAmts(intTypeCol) / decAccumTotals(intTypeCol))
                                                    decSaveAmts(intTypeCol) = decSaveAmts(intTypeCol) / decAccumTotals(intTypeCol)
                                                Else
                                                    spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumberColumn + intTypeCol, 0)
                                                    decSaveAmts(intTypeCol) = 0
                                                End If
                                            End If
                                    End Select
                                Next
                            Next
                        End If
                    Next
                End Using
                Logger.Log.Info(String.Format("REController:LoadSpread7to8 method execution Ends"))
            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("LoadSpread7to8 : Error occured while processing Revenue & Expense Chart - ", ex.Message)
                Logger.Log.Error(String.Format("REController:LoadSpread7to8 method execution ends with Error message: {0}, StackTrace: {1}", ex.Message, ex.StackTrace))
            End Try


        End Sub

        'Private Sub AccumNI(ByRef spdAnalytics As FpSpread, ByVal strClassDesc As String, ByVal intFormat As Integer, ByVal intNumberColumn As Integer, ByVal decSaveAmts() As Decimal, ByVal intColumnCount As Integer, ByRef decIncAccumTotals() As Decimal, ByRef intRowCounter As Integer, ByVal intPeriod As Integer)
        '    Try
        '        Logger.Log.Info(String.Format("REController:AccumNI method execution Starts"))
        '        Select Case strClassDesc
        '            Case "Revenue"
        '                For intColCtr = 0 To intNumberColumn - 1
        '                    decIncAccumTotals(intColCtr) = decSaveAmts(intColCtr)
        '                Next
        '            Case "Cost of sales"
        '                For intColCtr = 0 To intNumberColumn - 1
        '                    decIncAccumTotals(intColCtr) -= decSaveAmts(intColCtr)
        '                Next
        '                CommonProcedures.AddTotalRow(spdAnalytics, intFormat, intNumberColumn, intFirstNumberColumn, intColumnCount, decIncAccumTotals, intRowCounter, "GP", "Gross profit", intPeriod, 4)
        '            Case Else
        '                For intColCtr = 0 To intNumberColumn - 1
        '                    decIncAccumTotals(intColCtr) -= decSaveAmts(intColCtr)
        '                Next
        '        End Select
        '        Logger.Log.Info(String.Format("REController:AccumNI method execution Ends"))
        '    Catch ex As Exception
        '        TempData("ErrorMessage") = String.Concat("AccumNI : Error occured while processing Revenue & Expense Chart - ", ex.Message)
        '        Logger.Log.Error(String.Format("REController:AccumNI method execution ends with Error message: {0}, StackTrace: {1}", ex.Message, ex.StackTrace))
        '    End Try

        'End Sub

        Protected Overrides Sub Dispose(disposing As Boolean)
            accountRepository.Dispose()
            accountTypeRepository.Dispose()
            balanceRepository.Dispose()
            chartFormatRepository.Dispose()
            chartTypeRepository.Dispose()

            MyBase.Dispose(disposing)
        End Sub
    End Class
End Namespace