﻿Imports System.Dynamic
Imports System.Data.Entity
Imports System.Web.Mvc
Imports PagedList
Imports onlineAnalytics
Imports System.Collections.Generic
Imports CheckBoxListInfo
Imports MvcCheckBoxList.Model
Imports System.Data.Entity.Validation


Namespace onlineAnalytics

    <CustAuthFilter()>
    Public Class CompanyController
        Inherits System.Web.Mvc.Controller

        Private utility As New Utility()
        Private companyRepository As ICompanyRepository
        Private userRepository As IUserRepository
        Private usercompanyMapping As IUserCompanyMappingRepository
        Private analysisRepository As IAnalysisRepository
        Private UserType As UserRole
        Private UserInfo As User
        Private CustomerId As Integer
        Private pageSize As Integer
        Private selectedCompanyFromDropdown As Integer
        Public ReadOnly Property Page_Size As Integer

            Get
                If (System.Configuration.ConfigurationManager.AppSettings("PageSize") <> "") Then
                    pageSize = System.Configuration.ConfigurationManager.AppSettings("PageSize").ToString()
                Else
                    pageSize = 10
                End If
                Return Me.pageSize
            End Get
        End Property
        Public Sub New()
            Me.companyRepository = New CompanyRepository(New DataAccess())
        End Sub

        Public Sub New(companyRepository As ICompanyRepository)
            Me.companyRepository = companyRepository
        End Sub

        <CustomActionFilter()>
        Function Index(page As System.Nullable(Of Integer)) As ViewResult
            Try

                If (Not Session("UserType") Is Nothing) And (Not Session("UserInfo") Is Nothing) Then
                    UserType = DirectCast(Session("UserType"), UserRole)
                    UserInfo = DirectCast(Session("UserInfo"), User)
                    CustomerId = If(UserType.UserRoleId > UserRoles.PGASupport, UserInfo.CustomerId, Nothing)

                End If

                Dim companies As IEnumerable(Of Company) = Enumerable.Empty(Of Company)()

                If ((UserType.UserRoleId = UserRoles.SAU Or UserType.UserRoleId = UserRoles.CAU Or UserType.UserRoleId = UserRoles.CRU)) Then

                    companies = (From c In utility.CompaniesRepository.GetCompanies()
                            Join uam In utility.UserCompanyMappingRepository.GetUserCompanyMapping() On c.CompanyId Equals uam.CompanyId
                            Join u In utility.UserRepository.GetUsers() On u.UserId Equals uam.UserId
                            Where u.Status = StatusE.Active And u.UserId = UserInfo.UserId Select New Company With {.CompanyId = c.CompanyId, .CompanyName = Common.Decrypt(c.CompanyName), .FiscalMonthStart = c.FiscalMonthStart, .FiscalMonthName = MonthName(c.FiscalMonthStart), .ContactFirstName = Common.Decrypt(c.ContactFirstName), .ContactLastName = Common.Decrypt(c.ContactLastName), .ContactEmail = Common.Decrypt(c.ContactEmail), .ContactTelephone = Common.Decrypt(c.ContactTelephone), .CreatedBy = c.CreatedBy, .CreatedOn = c.CreatedOn, .UpdatedBy = c.UpdatedBy, .UpdatedOn = c.UpdatedOn})

                ElseIf (UserType.UserRoleId = UserRoles.PGAAdmin Or UserType.UserRoleId = UserRoles.PGASupport) Then

                    companies = (From c In utility.CompaniesRepository.GetCompanies() Select New Company With {.CompanyId = c.CompanyId, .CompanyName = Common.Decrypt(c.CompanyName), .FiscalMonthStart = c.FiscalMonthStart, .FiscalMonthName = MonthName(c.FiscalMonthStart), .ContactFirstName = Common.Decrypt(c.ContactFirstName), .ContactLastName = Common.Decrypt(c.ContactLastName), .ContactEmail = Common.Decrypt(c.ContactEmail), .ContactTelephone = Common.Decrypt(c.ContactTelephone), .CreatedBy = c.CreatedBy, .CreatedOn = c.CreatedOn, .UpdatedBy = c.UpdatedBy, .UpdatedOn = c.UpdatedOn})

                End If

                'For Each item In companies
                '    'item.CustomerId = item.CustomerId
                '    'item.CompanyName = Common.Decrypt(item.CompanyName)
                '    'item.FiscalMonthStart = item.FiscalMonthStart
                '    item.FiscalMonthName = MonthName(item.FiscalMonthStart)
                '    'item.ContactFirstName = Common.Decrypt(item.ContactFirstName)
                '    'item.ContactLastName = Common.Decrypt(item.ContactLastName)
                '    'item.ContactEmail = Common.Decrypt(item.ContactEmail)
                '    'item.ContactTelephone = Common.Decrypt(item.ContactTelephone)
                '    'item.CustomerId = item.CustomerId
                '    'item.CreatedBy = item.CreatedBy
                '    'item.CreatedOn = item.CreatedOn
                '    'item.UpdatedBy = item.UpdatedBy
                '    'item.UpdatedOn = item.UpdatedOn
                'Next

                Logger.Log.Info(String.Format("Company Index Execution Done"))

                Dim pageNumber As Integer = (If(page, 1))
                If (companies Is Nothing) Then
                    Return View(companies)
                Else
                    Return View(companies.ToPagedList(pageNumber, Page_Size))
                End If
            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Unable to Index() company-", ex.Message)
                Logger.Log.Error(String.Format("\n Unable to Index() with Message- {0} " + Environment.NewLine + "Stack Trace: {1} ", ex.Message, ex.StackTrace))
                Return View()
            Finally
                Logger.Log.Info(String.Format("Company Index() Execution Ended"))
            End Try

        End Function

        <CustomActionFilter()>
        Function Create() As ViewResult

            'Dim companyDetail As New Company()
            Dim comanyEncrypted As New _CompanyEncrypted()
            Try
                PopulateMonths(0)
                PopulateUserList(comanyEncrypted)
                Logger.Log.Info(String.Format("Company Create Execution Done"))
            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Unable to Create company-", ex.Message)
                Logger.Log.Error(String.Format("\n Unable to Edit with Message- {0} " + Environment.NewLine + "Stack Trace: {1} ", ex.Message, ex.StackTrace))
                Return View()
            Finally
                Logger.Log.Info(String.Format("Company Create Execution Ended"))
            End Try
            Return View(comanyEncrypted)
        End Function

        <HttpPost()> _
       <ValidateAntiForgeryToken()> _
       <CustomActionFilter()>
        Function Create(CompanyDetail As _CompanyEncrypted) As ActionResult
            Logger.Log.Info(String.Format("Company Creation started {0}", CompanyDetail.CompanyId))
            Dim usercompany As UserCompanyMapping
            Dim customerId As Integer
            Dim companyCount As Integer
            Dim company As New Company()

            Try

                company.AvailableUsers = CompanyDetail.AvailableUsers
                company.SelectedUsers = CompanyDetail.SelectedUsers
                company.PostedUsers = CompanyDetail.PostedUsers

                If ModelState.IsValid Then
                    Dim customerInfo As Customer = Nothing
                    Dim userInfo As User = Nothing

                    If Not (Session("CustomerInfo") Is Nothing) Then
                        customerInfo = DirectCast(Session("CustomerInfo"), Customer)
                        customerId = customerInfo.CustomerId
                    End If

                    If Not (Session("UserInfo") Is Nothing) Then
                        userInfo = DirectCast(Session("UserInfo"), User)
                    End If

                    company = New Company()
                    company.CustomerId = IIf(customerId > 0, customerId, userInfo.CustomerId)
                    company.CompanyName = Common.Encrypt(CompanyDetail.CompanyName)
                    company.ContactFirstName = Common.Encrypt(CompanyDetail.ContactFirstName)
                    company.ContactLastName = Common.Encrypt(CompanyDetail.ContactLastName)
                    company.ContactEmail = Common.Encrypt(CompanyDetail.ContactEmail)
                    ' company.ContactTelephone = Common.Encrypt(CompanyDetail.ContactTelephone)
                    If (CompanyDetail.ContactTelephone.Length > 15) Then
                        company.ContactTelephone = Common.Encrypt(CompanyDetail.ContactTelephone).Substring(0, 15)
                    Else
                        company.ContactTelephone = Common.Encrypt(CompanyDetail.ContactTelephone)
                    End If
                    company.FiscalMonthStart = CompanyDetail.FiscalMonthStart
                    company.CreatedBy = userInfo.UserId
                    company.UpdatedBy = userInfo.UserId
                    companyRepository.InsertCompany(company)
                    companyRepository.Save()
                    Logger.Log.Info(String.Format("Company created successfully with Company ID : {0} , Company Name {1} ", CompanyDetail.CompanyId, CompanyDetail.CompanyName))

                    companyCount = (From c In utility.CompaniesRepository.GetCompanies
                                    Join u In utility.UserRepository.GetUsers On c.CustomerId Equals u.CustomerId
                                    Where u.UserId = userInfo.UserId Select c).Count()

                    usercompany = New UserCompanyMapping()
                    If (userInfo.UserRoleId = UserRoles.SAU) Then
                        'usercompany = New UserCompanyMapping()
                        usercompany.CompanyId = company.CompanyId
                        usercompany.UserId = userInfo.UserId
                        usercompany.CreatedBy = userInfo.UserId
                        usercompany.UpdatedBy = userInfo.UserId
                        utility.UserCompanyMappingRepository.Insert(usercompany)
                        utility.UserCompanyMappingRepository.Save()
                    End If
                    If Not (CompanyDetail.PostedUsers Is Nothing) Then
                        For Each selecteduser As String In CompanyDetail.PostedUsers.UserIds
                            usercompany.CompanyId = company.CompanyId
                            usercompany.UserId = Convert.ToInt32(selecteduser)
                            usercompany.CreatedBy = userInfo.UserId
                            usercompany.UpdatedBy = userInfo.UserId
                            utility.UserCompanyMappingRepository.Insert(usercompany)
                            utility.UserCompanyMappingRepository.Save()
                            Logger.Log.Info(String.Format("User is successfully mapped with company, Company ID : {0} , UserId {1} ", CompanyDetail.CompanyId, usercompany.UserId))
                        Next
                    End If
                    TempData("Message") = "Company created successfully."

                    If (companyCount = 1) Then
                        TempData("InfoMessage") = "To add an analysis to the new company, select the new company from the Company dropdown and then select Analyses from the Manage Account menu."
                    End If

                    Return RedirectToAction("Index")
                End If
            Catch dataEx As DataException
                TempData("ErrorMessage") = String.Concat("Unable to create company-", dataEx.Message)
                Logger.Log.Error(String.Format(Environment.NewLine + " Unable to create company id- {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2} ", CompanyDetail.CompanyId, dataEx.Message, dataEx.StackTrace))
            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Unable to create company-", ex.Message)
                Logger.Log.Error(String.Format("\n Unable to create company id- {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2} ", CompanyDetail.CompanyId, ex.Message, ex.StackTrace))
            Finally
                Logger.Log.Info(String.Format("Create Company Execution Ended"))
            End Try

            PopulateMonths(CompanyDetail.FiscalMonthStart)
            PopulateUserList(CompanyDetail)
            Return View(CompanyDetail)
        End Function

        <HttpPost()>
        <CustomActionFilter()>
        Function Edit(CompanyDetail As _CompanyEncrypted) As ActionResult
            Logger.Log.Info(String.Format("\n Company Update started {0}", CompanyDetail.CompanyId))
            Dim company As Company
            Dim customerId As Integer
            Try

                company = utility.CompanyRepository.GetByID(CompanyDetail.CompanyId)

                If ModelState.IsValid Then

                    Dim customerInfo As Customer = Nothing
                    If Not (Session("CustomerInfo") Is Nothing) Then
                        customerInfo = DirectCast(Session("CustomerInfo"), Customer)
                        customerId = customerInfo.CustomerId
                    End If

                    If Not (Session("UserInfo") Is Nothing) Then
                        UserInfo = DirectCast(Session("UserInfo"), User)
                    End If

                    company.CustomerId = IIf(customerId > 0, customerId, UserInfo.CustomerId)
                    company.CompanyName = Common.Encrypt(CompanyDetail.CompanyName)
                    company.ContactFirstName = Common.Encrypt(CompanyDetail.ContactFirstName)
                    company.ContactLastName = Common.Encrypt(CompanyDetail.ContactLastName)
                    company.ContactEmail = Common.Encrypt(CompanyDetail.ContactEmail)
                    If (CompanyDetail.ContactTelephone.Length > 15) Then
                        company.ContactTelephone = Common.Encrypt(CompanyDetail.ContactTelephone).Substring(0, 15)
                    Else
                        company.ContactTelephone = Common.Encrypt(CompanyDetail.ContactTelephone)
                    End If
                    company.FiscalMonthStart = CompanyDetail.FiscalMonthStart
                    company.UpdatedBy = UserInfo.UserId
                    utility.CompaniesRepository.UpdateCompany(company)
                    utility.CompaniesRepository.Save()
                    'Get list of User mapped with the selected company
                    Dim selectMappedUserWithComp As Dictionary(Of Integer, Integer) = utility.UserCompanyMappingRepository.GetUserCompanyMappingByCompanyId(CompanyDetail.CompanyId).ToDictionary(Function(a) a.UserId, Function(a) a.UserCompanyMappingId)
                    Dim usercompany As New UserCompanyMapping()

                    If (UserInfo.UserRoleId = UserRoles.SAU) Then
                        selectMappedUserWithComp.Remove(UserInfo.UserId)
                    End If
                    If (Not CompanyDetail.PostedUsers Is Nothing) Then
                        For Each selecteduser As String In CompanyDetail.PostedUsers.UserIds
                            'If New
                            If selectMappedUserWithComp.ContainsKey(selecteduser) = False Then
                                usercompany.CompanyId = CompanyDetail.CompanyId
                                usercompany.UserId = Convert.ToInt32(selecteduser)
                                usercompany.UpdatedBy = UserInfo.UserId
                                utility.UserCompanyMappingRepository.Insert(usercompany)
                                utility.UserCompanyMappingRepository.Save()
                                Logger.Log.Info(String.Format("User is successfully mapped with company, Company ID : {0} , UserId {1} ", CompanyDetail.CompanyId, usercompany.UserId))
                            End If
                            selectMappedUserWithComp.Remove(selecteduser)
                        Next
                    End If

                    'Delete the users, which are not found within redefine selected list.
                    If (selectMappedUserWithComp.Count > 0) Then
                        Logger.Log.Info(String.Format("Total {0} users found to be delete of Company ID {1} from UserCompanyMapping ", selectMappedUserWithComp.Count, CompanyDetail.CompanyId))
                        Dim getSelectMappedUser = String.Concat("'", String.Join("','", selectMappedUserWithComp.Select(Function(a) a.Key.ToString())), "'")
                        Dim result = utility.UserCompanyMappingRepository.DeleteUserCompanyMappingByUserIdAndCompanyId(CompanyDetail.CompanyId, getSelectMappedUser)
                        If (result = selectMappedUserWithComp.Count) Then
                            Logger.Log.Info(String.Format("Users delete successfully, which is not found within redefine selected list with company, Company ID : {0} , Users {1} ", CompanyDetail.CompanyId, getSelectMappedUser))
                        Else
                            Logger.Log.Info(String.Format("Delete users list mismatch --> FoundUsersToDelete {0} : , ActualUsersDeleted : {1} "))
                            Logger.Log.Info(String.Format("Users delete successfully, which is not found within redefine selected list with company, Company ID : {0} , Users {1} ", CompanyDetail.CompanyId, getSelectMappedUser))
                        End If
                    End If
                    TempData("Message") = "Company updated successfully."
                    Logger.Log.Info(String.Format("Company Updated successfully with id {0}", CompanyDetail.CompanyId))
                    Return RedirectToAction("Index")
                End If
            Catch dataEx As DataException
                TempData("ErrorMessage") = String.Concat("Unable to update company-", dataEx.Message)
                Logger.Log.Error(String.Format("\n Unable to Update company id- {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2} ", CompanyDetail.CompanyId, dataEx.Message, dataEx.StackTrace))
            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Unable to update company-", ex.Message)
                Logger.Log.Error(String.Format("\n Unable to Update company id- {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2} ", CompanyDetail.CompanyId, ex.Message, ex.StackTrace))
            Finally
                Logger.Log.Info(String.Format("Execution Ended"))
            End Try

            PopulateMonths(CompanyDetail.FiscalMonthStart)
            PopulateUserList(CompanyDetail)

            Return View(CompanyDetail)
        End Function

        <CustomActionFilter()>
        Public Function Edit(id As Integer) As ActionResult
            Dim _companyEncrypted As _CompanyEncrypted

            Try
                Dim company = companyRepository.GetCompanyById(id)


                _companyEncrypted = New _CompanyEncrypted
                _companyEncrypted.CompanyId = company.CompanyId
                _companyEncrypted.CompanyName = Common.Decrypt(company.CompanyName)
                _companyEncrypted.ContactFirstName = Common.Decrypt(company.ContactFirstName)
                _companyEncrypted.ContactLastName = Common.Decrypt(company.ContactLastName)
                _companyEncrypted.ContactEmail = Common.Decrypt(company.ContactEmail)
                _companyEncrypted.ContactTelephone = Common.Decrypt(company.ContactTelephone)
                _companyEncrypted.CustomerId = company.CustomerId
                _companyEncrypted.IndustryCode = company.IndustryCode
                _companyEncrypted.FiscalMonthStart = company.FiscalMonthStart

                _companyEncrypted.CreatedBy = company.CreatedBy
                _companyEncrypted.CreatedOn = company.CreatedOn
                _companyEncrypted.UpdatedBy = company.UpdatedBy
                _companyEncrypted.UpdatedOn = company.UpdatedOn

                PopulateMonths(_companyEncrypted.FiscalMonthStart)
                PopulateUserList(_companyEncrypted)
                Logger.Log.Info(String.Format("Company Edit Execution Successfully Done"))
                Return View(_companyEncrypted)
            Catch dataEx As DataException
                TempData("ErrorMessage") = String.Concat("Unable to Edit company-", dataEx.Message)
                Logger.Log.Error(String.Format("\n Unable to Edit company id- {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2} ", id, dataEx.Message, dataEx.StackTrace))
            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Unable to Edit company-", ex.Message)
                Logger.Log.Error(String.Format("\n Unable to Edit company id- {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2} ", id, ex.Message, ex.StackTrace))
            Finally
                Logger.Log.Info(String.Format("Company Edit Execution Ended"))
            End Try
            Return Nothing
        End Function

        <CustomActionFilter()>
        Public Function Delete(companyId As Integer) As ActionResult

            Logger.Log.Info(String.Format("Company Deletion Started {0}", companyId))

            Try
                If Not (Session("SelectedCompanyFromDropdown") Is Nothing) Then
                    selectedCompanyFromDropdown = Session("SelectedCompanyFromDropdown")
                    If (selectedCompanyFromDropdown = companyId) Then
                        Session.Remove("SelectedCompanyFromDropdown")
                    End If
                End If

                companyRepository.DeleteCompany(companyId)
                companyRepository.Save()
                TempData("Message") = "Company deleted successfully."
                Logger.Log.Info(String.Format("Company Deleted successfully with id {0}", companyId))
            Catch dataEx As DataException
                'Log the error
                TempData("ErrorMessage") = String.Concat("Unable to delete company - ", dataEx.Message)
                Logger.Log.Error(String.Format(Environment.NewLine + " Unable to Delete company id- {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2}", companyId, dataEx.Message, dataEx.StackTrace))
                Return RedirectToAction("Index", New System.Web.Routing.RouteValueDictionary() _
                                        From {{"id", companyId}, {"deleteChangesError", True}})
            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Unable to delete company-", ex.Message)
                Logger.Log.Error(String.Format(" Unable to Delete company id- {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2} ", companyId, ex.Message, ex.StackTrace))
            Finally
                Logger.Log.Info(String.Format("Execution Ended"))
            End Try
            Return RedirectToAction("Index")
        End Function

        Private Sub PopulateMonths(Optional SelectedMonth As Object = Nothing)
            Try
                ViewBag.Months = utility.GetMonth(SelectedMonth)
                Logger.Log.Info(String.Format("PopulateMonths Sub Successfully Executed"))
            Catch dataEx As DataException
                TempData("ErrorMessage") = String.Concat("Unable to Populate Months-", dataEx.Message)
                Logger.Log.Error(String.Format(" Unable to Populate Months with Message-{0} {1} ", dataEx.Message, dataEx.StackTrace))
                Throw
            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Unable to Populate Months-", ex.Message)
                Logger.Log.Error(String.Format(" Unable to Populate Months with Message-{0} {1} ", ex.Message, ex.StackTrace))
                Throw
            Finally
                Logger.Log.Info(String.Format("PopulateMonths Execution Ended"))
            End Try
        End Sub

        Private Sub PopulateUserList(companyDetail As _CompanyEncrypted)
            Try

                If (Not Session("UserType") Is Nothing) And (Not Session("UserInfo") Is Nothing) Then
                    UserType = DirectCast(Session("UserType"), UserRole)
                    UserInfo = DirectCast(Session("UserInfo"), User)
                    CustomerId = If(UserInfo.CustomerId > 0, UserInfo.CustomerId, Nothing)

                End If


                Dim users = utility.UserRepository.GetUsers().Where(Function(u) (u.UserRoleId <> UserRoles.PGAAdmin) And (u.UserRoleId <> UserRoles.PGASupport) _
                                                                        And u.Status = StatusE.Active)

                If (CustomerId > 0 And (Not UserType Is Nothing) And (Not UserInfo Is Nothing) And (UserInfo.UserRoleId <> UserRoles.PGAAdmin And UserInfo.UserRoleId <> UserRoles.PGASupport)) Then

                    users = From c In users.Where(Function(a) a.CustomerId = CustomerId And a.UserRoleId > UserInfo.UserRoleId)
                Else
                    users = From c In users.Where(Function(a) a.UserRoleId >= UserInfo.UserRoleId)

                End If

                Dim userList As List(Of User_) = New List(Of User_)
                'if an array of posted user ids exists and is not empty,
                'save selected ids
                For Each user As User In users.Distinct()
                    userList.Add(New User_(user.UserId, user.FirstName + " " + user.LastName, New With {.user = [Enum].GetName(GetType(UserRoles), user.UserRoleId).ToString()}, False))
                Next

                Dim userComp = utility.UserCompanyMappingRepository.GetUserCompanyMappingByCompanyId(companyDetail.CompanyId)
                companyDetail.AvailableUsers = userList
                If Not (userComp Is Nothing) Then
                    Dim InsertedUserList As List(Of User_) = New List(Of User_)
                    Dim InsertedUsers = users.Where(Function(b) userComp.Any(Function(a) a.UserId = b.UserId))
                    For Each user As User In InsertedUsers
                        'Tag the checkbox by roleid to differentiate them.
                        InsertedUserList.Add(New User_(user.UserId, user.FirstName + " " + user.LastName, New With {.user = [Enum].GetName(GetType(UserRoles), user.UserRoleId).ToString()}, False))
                    Next
                    companyDetail.SelectedUsers = InsertedUserList
                End If
                ' if a view model array of posted user ids exists and is not empty,
                ' save selected ids
                If Not (companyDetail.PostedUsers Is Nothing) Then
                    Dim selectedUserList As List(Of User_) = New List(Of User_)
                    Dim selectedUsers = utility.UserRepository.GetUsers.Where(Function(u) companyDetail.PostedUsers.UserIds.Contains(u.UserId) And u.Status = StatusE.Active).ToList()
                    'Dim selectedUsers = utility.UserRepository.GetUsers.Where(Function(u) companyDetail.PostedUsers.UserIds. And u.Status = StatusE.Active).ToList()
                    For Each user As User In selectedUsers
                        selectedUserList.Add(New User_(user.UserId, user.FirstName + " " + user.LastName, New With {.user = [Enum].GetName(GetType(UserRoles), user.UserRoleId).ToString()}, False))
                    Next
                    companyDetail.SelectedUsers = selectedUserList
                End If
                Logger.Log.Info(String.Format("PopulateUserList Function Successfully Executed"))
            Catch dataEx As DataException
                TempData("ErrorMessage") = String.Concat("Populate User List-->", dataEx.Message)
                Logger.Log.Error(String.Format(" Unable to Populate UserListfor  company id- {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2} ", companyDetail.CompanyId, dataEx.Message, dataEx.StackTrace))
                Throw
            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Populate User List-->", ex.Message)
                Logger.Log.Error(String.Format(" Unable to Populate User List for companyid- {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2} ", companyDetail.CompanyId, ex.Message, ex.StackTrace))
                Throw
            Finally
                Logger.Log.Info(String.Format("PopulateUserList Function Ended"))
            End Try
        End Sub

        Protected Overrides Sub Dispose(disposing As Boolean)
            companyRepository.Dispose()
            MyBase.Dispose(disposing)
        End Sub

        'Private Sub PopulateUserList(company As onlineAnalytics.Company)
        '    Throw New NotImplementedException
        'End Sub

    End Class
End Namespace
