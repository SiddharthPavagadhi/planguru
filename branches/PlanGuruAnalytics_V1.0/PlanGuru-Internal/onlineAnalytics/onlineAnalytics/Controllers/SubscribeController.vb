﻿Imports System
Imports System.Collections.Generic
Imports System.Configuration
Imports System.Linq
Imports System.Web.Mvc
Imports System.Security.Cryptography
Imports PagedList
Imports onlineAnalytics
Imports onlineAnalytics.Common
Imports Recurly
Imports System.Data.Entity.Validation

Namespace onlineAnalytics

    <CustAuthFilter()>
    Public Class SubscribeController
        Inherits System.Web.Mvc.Controller

        Private utility As New Utility()
        Private common As New Common()
        Private customerRepository As ICustomerRepository
        Private userRepository As IUserRepository
        Private UserType As UserRole
        Private LoginUserInfo As User, userInfo As User
        Private CustomerId As String = Nothing
        Private pageSize As Integer

        Public ReadOnly Property Page_Size As Integer

            Get
                If (System.Configuration.ConfigurationManager.AppSettings("PageSize") <> "") Then
                    pageSize = System.Configuration.ConfigurationManager.AppSettings("PageSize").ToString()
                Else
                    pageSize = 10
                End If
                Return Me.pageSize
            End Get
        End Property

        Public Sub New()
            Me.customerRepository = New CustomerRepository(New DataAccess())
            Me.userRepository = New UserRepository(New DataAccess())
        End Sub

        Public Sub New(customerRepository As ICustomerRepository)
            Me.customerRepository = customerRepository
        End Sub

        Public Sub New(userRepository As IUserRepository)
            Me.userRepository = userRepository
        End Sub

        '
        ' GET: /User
        <CustomActionFilter()>
        Function Subscribe() As ActionResult
            Try
                Dim subscription As String = String.Format("{0}={1}", "subscription%5Bplan_code%5D", "planguru-analytics")

                ViewBag.Signature = utility.SignWithParameters(subscription)

                ViewBag.AccountCode = utility.CheckAccountCodeOnRecurly((utility.CustomerRepository().GetCustomers().Max(Function(c) c.CustomerId) + 1))

                Session("AccountCode") = ViewBag.AccountCode
                Logger.Log.Info(String.Format("Subscribe Execution Done"))
                Return View()
            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Unable to Subscribe()-", ex.Message)
                Logger.Log.Error(String.Format("\n Unable to Subscribe() with Message- {0} " + Environment.NewLine + "Stack Trace: {1} ", ex.Message, ex.StackTrace))
                Return View()
            Finally
                Logger.Log.Info(String.Format("Subscribe() Execution Ended"))
            End Try

        End Function

        ' This CreateUser Function allow user to enter required information to Create User.      
        <CustomActionFilter()>
        Function CreateUser() As ActionResult
            Try
                PopulateMonthYearAndCardType()
                PopulateCountryList(0)
                PopulateStateList(0)
                Logger.Log.Info(String.Format("Subscribe CreateUser() Executed"))
                Return View()
            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Unable to Subscribe CreateUser()-", ex.Message)
                Logger.Log.Error(String.Format("\n Unable to Subscribe CreateUser() with Message- {0} " + Environment.NewLine + "Stack Trace: {1} ", ex.Message, ex.StackTrace))
                Return View()
            Finally
                Logger.Log.Info(String.Format("Subscribe CreateUser() Execution Ended"))
            End Try
        End Function

        Function ToUnixTimestamp(dt As DateTime) As Long
            Dim unixRef As New DateTime(1970, 1, 1, 0, 0, 0)
            Return (dt.Ticks - unixRef.Ticks) / 10000000
        End Function

        Private Sub SendEmail(customer As Customer, emailId As Integer)
            Try
                Dim objEmailInfo = utility.EmailRepository.GetEmailInfoById(emailId)
                objEmailInfo.EmailBody = common.MergeCustomerEmailBody(customer, objEmailInfo)
                common.SendCustomerEmail(customer, objEmailInfo)
                Logger.Log.Info(String.Format("SendEmail Executed"))
            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Unable to Send Email -", ex.Message)
                Logger.Log.Error(String.Format("Unable to Send Email to CustomerID - {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2} ", customer.CustomerId, ex.Message, ex.StackTrace))
            Finally
                Logger.Log.Info(String.Format("SendEmail Execution Ended"))
            End Try
        End Sub

        Private Sub SendEmail(User As User, emailId As Integer)
            Try
                Dim planGuruUrl As String = "http://" + HostName(Request) + If(Request.Url.IsDefaultPort, Request.ApplicationPath + "/", ":" + Request.Url.Port.ToString() + "/")
                Dim objEmailInfo = utility.EmailRepository.GetEmailInfoById(emailId)
                objEmailInfo.EmailBody = common.MergeUserEmailBody(User, objEmailInfo, planGuruUrl)
                common.SendUserEmail(User, objEmailInfo)
                Logger.Log.Info(String.Format("Email Sent Successfully to User: {0} for EmailId: {1}", User.UserId, emailId))
            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Unable to Send Email -", ex.Message)
                Logger.Log.Error(String.Format("Unable to Send Email, UserId - {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2} ", User.UserId, ex.Message, ex.StackTrace))
            Finally
                Logger.Log.Info(String.Format("SendEmail Execution Ended"))
            End Try
        End Sub

        Function Verify() As ActionResult
            Return View()
        End Function

        Function Receipt() As ViewResult
            Dim customer As Customer = Nothing
            Dim AccountCode As Integer
            Dim newUserAccountCode As Integer
            Dim userInfo As User = Nothing
            Dim username As String = String.Empty
            Try

                If (Not Session("UserType") Is Nothing) And (Not Session("UserInfo") Is Nothing) Then

                    UserType = DirectCast(Session("UserType"), UserRole)
                    LoginUserInfo = DirectCast(Session("UserInfo"), User)
                    AccountCode = Session("AccountCode")

                End If

                If RecurlyAccount.IsExist(AccountCode) = True Then
                    Dim subscription As RecurlySubscription = RecurlySubscription.Get(AccountCode)

                    If Not (subscription Is Nothing) Then

                        Dim accountInfo As RecurlyAccount = RecurlyAccount.Get(AccountCode)
                        Dim billingInfo As RecurlyBillingInfo = RecurlyBillingInfo.Get(AccountCode)

                        customer = New Customer()
                        customer.CustomerId = AccountCode
                        customer.Quantity = subscription.Quantity
                        customer.CustomerFirstName = accountInfo.FirstName
                        customer.CustomerLastName = accountInfo.LastName
                        customer.CustomerEmail = accountInfo.Email
                        customer.CustomerCompanyName = accountInfo.CompanyName

                        '******Comment below code because of PCI compliance ***********
                        customer.ContactFirstName = String.Empty
                        customer.ContactLastName = String.Empty
                        'Credit Card Information                       
                        customer.FirstNameOnCard = String.Empty  'billingInfo.FirstName
                        customer.LastNameOnCard = String.Empty 'billingInfo.LastName
                        customer.CreditCardNumber = String.Empty  'String.Concat("XXXX-XXXX-XXXX-", billingInfo.CreditCard.LastFour)
                        customer.CVV = String.Empty 'billingInfo.VerificationValue
                        customer.ExpirationMonth = 0 'billingInfo.CreditCard.ExpirationMonth
                        customer.ExpirationYear = 0  'billingInfo.CreditCard.ExpirationYear
                        customer.CustomerAddress1 = String.Empty  'billingInfo.Address1
                        customer.CustomerAddress2 = String.Empty  'billingInfo.Address2

                        '******Comment below code because of PCI compliance ***********
                        'If (customer.CustomerPostalCode.Length > 10) Then
                        '    customer.CustomerPostalCode = customer.CustomerPostalCode.ToString().Substring(0, 10)
                        'Else
                        '    customer.CustomerPostalCode = customer.CustomerPostalCode
                        'End If

                        customer.CustomerPostalCode = String.Empty

                        If (billingInfo.PhoneNumber.Length > 15) Then
                            customer.ContactTelephone = billingInfo.PhoneNumber.ToString().Substring(0, 15)
                        Else
                            customer.ContactTelephone = billingInfo.PhoneNumber
                        End If

                        customer.Country = billingInfo.Country
                        customer.State = String.Empty 'billingInfo.State
                        customer.City = String.Empty  'billingInfo.City

                        customer.SubscriptionStartAt = Convert.ToDateTime(subscription.CurrentPeriodStartedAt).ToString("MMM dd, yyyy")
                        customer.SubscriptionEndAt = Convert.ToDateTime(subscription.CurrentPeriodEndsAt).ToString("MMM dd, yyyy")
                        customer.SubscriptionAmount = (subscription.UnitAmountInCents / 100)

                        customer.CreatedBy = LoginUserInfo.UserId
                        customer.UpdatedBy = LoginUserInfo.UserId
                        utility.CustomerRepository.InsertCustomer(customer)
                        utility.CustomerRepository.Save()
                        TempData("Message") = "Subscription created successfully."

                        newUserAccountCode = utility.CheckAccountCodeOnRecurly((utility.UserRepository().GetUsers().Max(Function(c) c.UserId) + 1))

                        'SAU user account creation
                        username = utility.AutoGenerateUserName()
                        Dim password As String = common.Generate(8, 2)

                        Logger.Log.Info(String.Format("Home - Username {0} , Password {1}", username, password))

                        userInfo = New User()
                        userInfo.UserId = newUserAccountCode
                        userInfo.CustomerId = customer.CustomerId
                        userInfo.UserName = username
                        userInfo.UserRoleId = UserRoles.SAU
                        userInfo.FirstName = accountInfo.FirstName
                        userInfo.LastName = accountInfo.LastName
                        userInfo.UserEmail = accountInfo.Email
                        userInfo.Status = CType(StatusE.Pending, Integer)
                        userInfo.CreatedBy = LoginUserInfo.UserId
                        userInfo.UpdatedBy = LoginUserInfo.UserId

                        userInfo.Password = common.Encrypt(password)
                        userInfo.SecurityKey = Guid.NewGuid().ToString()

                        utility.UserRepository.Insert(userInfo)
                        utility.UserRepository.Save()

                        SendEmail(customer, EmailType.NewSubscriptionSignup)
                        SendEmail(userInfo, EmailType.NewUserSignUp)

                        ViewBag.SubscriptionInfo = customer
                        ViewBag.CreditCardLastFourDigit = billingInfo.CreditCard.LastFour.ToString()
                        ViewBag.CreditCardType = billingInfo.CreditCard.CreditCardType.ToString()
                    Else
                        TempData("ErrorMessage") = String.Concat("Account Code is missing !")
                        Logger.Log.Error(String.Format("Unable to Create Customer :- Account Code is missing."))
                    End If

                End If
                Logger.Log.Info(String.Format("Subscribe Receipt Function Executied"))

            Catch dbEntityEx As DbEntityValidationException

                Logger.Log.Info(String.Format("Home - Username {0}", username))

                For Each sError In dbEntityEx.EntityValidationErrors
                    Logger.Log.Error(String.Format("Entity of type ""{0}"" in state ""{1}"" has the following validation errors:", sError.Entry.Entity.[GetType].Name, sError.Entry.State))
                    For Each ve In sError.ValidationErrors
                        Logger.Log.Error(String.Format("-Property: {0}, Error: {1} ", ve.PropertyName, ve.ErrorMessage))
                    Next
                Next
            Catch dataEx As DataException
                TempData("ErrorMessage") = String.Concat("Unable to create customer-", dataEx.Message)
                Logger.Log.Error(String.Format("Unable to Create Customer id- {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2} ", customer.CustomerId, dataEx.Message, dataEx.StackTrace))

            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Unable to create customer-", ex.Message)
                Logger.Log.Error(String.Format("Unable to Create Customer id- {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2} ", customer.CustomerId, ex.Message, ex.StackTrace))

            Finally
                Logger.Log.Info(String.Format("Execution Ended"))
            End Try
            Return View(customer)

        End Function

        <CustomActionFilter()>
        Function Index(page As System.Nullable(Of Integer)) As ViewResult
            Try
                ' Dim customers = From cust In customerRepository.GetCustomers().Where(Function(c) c.Quantity <> 0 And c.CreditCardNumber <> String.Empty)

                Dim customers = (From u In utility.UserRepository.GetUsers
                       Where (From cust In customerRepository.GetCustomers().Where(Function(c) c.Quantity <> 0 And c.CreditCardNumber <> String.Empty) Select cust.CustomerId).Contains(u.CustomerId) _
                             And u.UserRoleId = UserRoles.SAU Select New Customer With {.UserName = u.UserName, .UserId = u.UserId, .Status = u.Status, .CustomerId = u.Customer.CustomerId, .CustomerEmail = u.Customer.CustomerEmail, .CustomerFirstName = u.Customer.CustomerFirstName, .CustomerLastName = u.Customer.CustomerLastName, .CustomerCompanyName = u.Customer.CustomerCompanyName, .Quantity = u.Customer.Quantity, .CreatedOn = u.Customer.CreatedOn}).ToList()

                'Dim customers = (From cust In utility.CustomerRepository.GetCustomers
                '                 Join u In utility.UserRepository.GetUsers On cust.CustomerId Equals u.CustomerId And cust.CustomerFirstName Equals u.FirstName And cust.CustomerLastName Equals u.LastName
                '                 Where cust.Quantity <> 0 And cust.CreditCardNumber <> String.Empty
                '               Select New Customer With {.UserName = u.UserName, .UserId = u.UserId, .CustomerId = u.Customer.CustomerId, .CustomerEmail = u.Customer.CustomerEmail, .CustomerFirstName = u.Customer.CustomerFirstName, .CustomerLastName = u.Customer.CustomerLastName, .CustomerCompanyName = u.Customer.CustomerCompanyName, .Quantity = u.Customer.Quantity, .ExpirationMonth = u.Customer.ExpirationMonth, .ExpirationYear = u.Customer.ExpirationYear, .CustomerPostalCode = u.Customer.CustomerPostalCode, .CreditCardNumber = u.Customer.CreditCardNumber, .CVV = u.Customer.CVV, .CreatedOn = u.Customer.CreatedOn}).Distinct().ToList()

                Logger.Log.Info(String.Format("Subscribe Index() Executed"))

                Dim pageNumber As Integer = (If(page, 1))
                If (customers Is Nothing) Then
                    Return View(customers)
                Else
                    Return View(customers.ToPagedList(pageNumber, Page_Size))
                End If
                Return View(customers.ToPagedList(pageNumber, Page_Size))
            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Unable to show Cutomers-", ex.Message)
                Logger.Log.Error(String.Format("Unable to Show Customers, with Message- {0} " + Environment.NewLine + "Stack Trace: {1} ", ex.Message, ex.StackTrace))
                Return View()
            Finally
                Logger.Log.Info(String.Format("Subscribe Index() Execution Ended"))
            End Try

        End Function
        <CustomActionFilter()>
        Function CancelSubscription(AccountCode As String) As ActionResult
            Logger.Log.Info(String.Format("Subscription cancellation started {0}", AccountCode))
            Dim deactivateUsers As IEnumerable(Of User)
            Try

                If (Not Session("UserType") Is Nothing) And (Not Session("UserInfo") Is Nothing) Then
                    UserType = DirectCast(Session("UserType"), UserRole)
                    LoginUserInfo = DirectCast(Session("UserInfo"), User)
                    userInfo = utility.UserRepository.GetUsers.Where(Function(u) u.UserId = LoginUserInfo.UserId).SingleOrDefault()
                    AccountCode = If(UserType.UserRoleId = UserRoles.SAU, LoginUserInfo.CustomerId, AccountCode)
                End If

                RecurlySubscription.TerminateSubscription(AccountCode)
                RecurlyAccount.CloseAccount(AccountCode)
                Logger.Log.Info(String.Format("Subscription deleted successfully on RECURLY of ACCOUNT-CODE - {0}", AccountCode))

                Dim customer = utility.CustomerRepository.GetCustomerById(AccountCode)

                Dim subscriberInfo As User = utility.UserRepository.GetUsers.Where(Function(u) u.CustomerId = AccountCode And u.UserRoleId = UserRoles.SAU).FirstOrDefault()

                subscriberInfo.Status = StatusE.Deactivated
                subscriberInfo.AnalysisId = 0
                utility.UserRepository.UpdateUser(subscriberInfo)
                utility.UserRepository.Save()
                Logger.Log.Info(String.Format("Account of user:{0} has been deactivated, UserId : {1} , UserName : {2} , CustomerId : {3}", userInfo.FirstName, userInfo.UserId, userInfo.UserName, userInfo.CustomerId))

                If (userInfo.UserRoleId = UserRoles.PGAAdmin Or userInfo.UserRoleId = UserRoles.PGASupport) Then
                    deactivateUsers = From user In utility.UserRepository.GetUsers.Where(Function(x) x.CustomerId = AccountCode)
                Else
                    deactivateUsers = From user In utility.UserRepository.GetUsers(AccountCode, userInfo.UserId)
                End If

                Dim users As List(Of Integer) = (From du In deactivateUsers Select du.UserId).ToList()

                Dim companyIds = utility.UserCompanyMappingRepository.GetDistinctCompanies(users)

                If Not (companyIds Is Nothing) Then
                    For Each companyId In companyIds
                        utility.CompaniesRepository.DeleteCompany(companyId)
                    Next
                    For Each User As User In deactivateUsers
                        User.Status = StatusE.Deactivated
                        utility.UserRepository.UpdateUser(User)
                        Logger.Log.Info(String.Format("Account of user:{0} has been deactivated, UserId : {1} , UserName : {2} , CustomerId : {3}", User.FirstName, User.UserId, User.UserName, User.CustomerId))
                    Next
                End If
                utility.UserRepository.Save()


                'utility.CustomerRepository.DeleteCustomer(AccountCode)

                SendEmail(customer, EmailType.SubcriptionCancellation)

                TempData("Message") = "Subscription cancelled successfully."
                Logger.Log.Info(String.Format("Subscription deleted successfully in PLANGURU database of ACCOUNT-CODE  {0}", AccountCode))

                If (userInfo.UserRoleId = UserRoles.SAU) Then

                    Response.Cookies("ASP.NET_SessionId").Value = String.Empty
                    Response.Cookies("ASP.NET_SessionId").Expires = DateTime.Now.AddMonths(-20)

                    HttpContext.Session.Clear()
                    HttpContext.Session.Abandon()
                    Return RedirectToAction("Index", "Home")

                ElseIf (userInfo.UserRoleId < UserRoles.SAU) Then
                    Return RedirectToAction("Index")
                End If

                Return RedirectToAction("Index")
            Catch dataEx As DataException
                TempData("ErrorMessage") = String.Concat("Unable to cancel Subscription - ", dataEx.Message)
                Logger.Log.Error(String.Format("Unable to cancel Subscription, AccountCode- {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2}", AccountCode, dataEx.Message, dataEx.StackTrace))
                Return RedirectToAction("Index", New System.Web.Routing.RouteValueDictionary() _
                                        From {{"AccountCode", AccountCode}, {"CancelChangesError", True}})

            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Unable to cancel Subscription-", ex.Message)
                Logger.Log.Error(String.Format("Unable to cancel Subscription, AccountCode- {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2}", AccountCode, ex.Message, ex.StackTrace))
                Return RedirectToAction("Index")
            Finally
                Logger.Log.Info(String.Format("Execution Ended"))
            End Try
        End Function

        <CustomActionFilter()>
        Function EditSubscription(AccountCode As String) As ViewResult
            Try
                Dim subscription As String = String.Format("{0}={1}", "subscription%5Bplan_code%5D", "planguru-analytics")
                ViewBag.Signature = utility.SignWithParameters(subscription)

                If (Not Session("UserType") Is Nothing) And (Not Session("UserInfo") Is Nothing) Then
                    UserType = DirectCast(Session("UserType"), UserRole)
                    LoginUserInfo = DirectCast(Session("UserInfo"), User)
                    AccountCode = If(LoginUserInfo.UserRoleId = UserRoles.SAU, LoginUserInfo.CustomerId, AccountCode)
                End If

                Dim customerDetails = customerRepository.GetCustomerById(AccountCode)

                Session("AccountCodeToEditSubscription") = AccountCode

                Return View(customerDetails)

            Catch dataEx As DataException
                TempData("ErrorMessage") = String.Concat("Unable to Edit Subscription-", dataEx.Message)
                Logger.Log.Error(String.Format("\n Unable to Edit Subscription, AccountCode - {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2} ", AccountCode, dataEx.Message, dataEx.StackTrace))

            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Unable to Edit Subscription-", ex.Message)
                Logger.Log.Error(String.Format("\n Unable to Edit Subscription, AccountCode - {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2} ", AccountCode, ex.Message, ex.StackTrace))

            Finally
                Logger.Log.Info(String.Format("Execution Ended"))
            End Try

            Return Nothing

        End Function
        <CustomActionFilter()>
        Function UpdateSubscription() As ActionResult
            Dim AccountCodeToEditSubscription As String = String.Empty
            Try

                If (Not Session("UserType") Is Nothing) And (Not Session("UserInfo") Is Nothing) Then
                    UserType = DirectCast(Session("UserType"), UserRole)
                    LoginUserInfo = DirectCast(Session("UserInfo"), User)
                    AccountCodeToEditSubscription = Session("AccountCodeToEditSubscription").ToString()
                End If

                Dim customer = customerRepository.GetCustomerById(AccountCodeToEditSubscription)

                If RecurlyAccount.IsExist(AccountCodeToEditSubscription) = True Then
                    Dim subscription As RecurlySubscription = RecurlySubscription.Get(AccountCodeToEditSubscription)
                    If Not (subscription Is Nothing) Then
                        Dim billingInfo As RecurlyBillingInfo = RecurlyBillingInfo.Get(AccountCodeToEditSubscription)

                        'Credit Card Information
                        customer.FirstNameOnCard = String.Empty 'billingInfo.FirstName
                        customer.LastNameOnCard = String.Empty 'billingInfo.LastName
                        customer.CreditCardNumber = String.Empty 'String.Concat("XXXX-XXXX-XXXX-", billingInfo.CreditCard.LastFour)
                        customer.CVV = String.Empty 'billingInfo.VerificationValue
                        customer.ExpirationMonth = 0 'billingInfo.CreditCard.ExpirationMonth
                        customer.ExpirationYear = 0 'billingInfo.CreditCard.ExpirationYear

                        customer.CustomerAddress1 = String.Empty  'billingInfo.Address1
                        customer.CustomerAddress2 = String.Empty 'billingInfo.Address2
                        customer.CustomerPostalCode = String.Empty 'billingInfo.PostalCode
                        customer.ContactTelephone = billingInfo.PhoneNumber
                        customer.Country = billingInfo.Country
                        customer.State = String.Empty 'billingInfo.State
                        customer.City = String.Empty 'billingInfo.City

                        customer.CreatedBy = customer.CreatedBy
                        customer.UpdatedBy = LoginUserInfo.UserId
                        utility.CustomerRepository.UpdateCustomer(customer)
                        utility.CustomerRepository.Save()


                        ViewBag.SubscriptionInfo = customer
                        ViewBag.CreditCardLastFourDigit = billingInfo.CreditCard.LastFour.ToString()
                        ViewBag.CreditCardType = billingInfo.CreditCard.CreditCardType.ToString()
                    Else
                        'To do show error message
                        ' Transaction unsuccessfull
                    End If
                End If
                Logger.Log.Info(String.Format("Subscribe Update Subscription Function Executed"))
                Return View(customer)
            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Unable to Update Subscription -", ex.Message)
                Logger.Log.Error(String.Format("Unable to Update Subscription, with Message- {0} " + Environment.NewLine + "Stack Trace: {1} ", ex.Message, ex.StackTrace))
                Return View()
            Finally
                Logger.Log.Info(String.Format("Subscribe Update Subscription Function Execution Ended"))
            End Try
        End Function

        <HttpGet()> _
        <CustomActionFilter()>
        Function SubscribeUser() As ActionResult
            'Using dataAccess As New DataAccess()
            '    dataAccess.Customers.Add(User)
            '    dataAccess.SaveChanges()
            'End Using
            Return View()
        End Function

        Private Sub PopulateMonthYearAndCardType()
            Try
                ViewBag.SelectedMonth = New SelectList({1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12})

                Dim currentYear As Date = Date.Now
                Dim yearlist As List(Of String) = New List(Of String)

                For value As Integer = 0 To 10
                    yearlist.Add(currentYear.Year.ToString())
                    currentYear = currentYear.AddYears(1)
                Next

                ViewBag.SelectedYear = New SelectList(yearlist)
                ViewBag.SelectedCardType = New SelectList({"Visa", "MasterCard", "American Express", "Discover"})
                Logger.Log.Info(String.Format("Populating MonthYearAndCardType Executed"))
            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Error In Populateing Month Year and Card Type -", ex.Message)
                Logger.Log.Error(String.Format("Unable to Populating MonthYearAndCardType, with Message- {0} " + Environment.NewLine + "Stack Trace: {1} ", ex.Message, ex.StackTrace))
            Finally
                Logger.Log.Info(String.Format("Populating MonthYearAndCardType Execution Ended"))
            End Try

        End Sub

        Private Sub PopulateCountryList(Optional SelectedCountry As Object = Nothing)
            Logger.Log.Info(String.Format("Populate Company List  started "))
            Try
                Dim countries = utility.CountryRepository.Get(orderBy:=Function(q) q.OrderBy(Function(d) d.CountryName))
                ViewBag.SelectedCountry = New SelectList(countries, "CountryId", "CountryName", SelectedCountry)
                Logger.Log.Info(String.Format("Populate Company List successfully Loaded"))

            Catch dataEx As DataException
                TempData("ErrorMessage") = String.Concat("Unable to Populate Company List -", dataEx.Message)
                Logger.Log.Error(String.Format("Unable to Populate Company List with Message- {0} " + Environment.NewLine + "Stack Trace: {1} ", dataEx.Message, dataEx.StackTrace))
                Throw
            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Unable to Populate Company List-", ex.Message)
                Logger.Log.Error(String.Format("Unable to populate company list with Message- {0} " + Environment.NewLine + "Stack Trace: {1}", ex.Message, ex.StackTrace))
                Throw
            Finally
                Logger.Log.Info(String.Format("Execution Ended"))
            End Try
        End Sub

        Private Sub PopulateStateList(Optional SelectedState As Object = Nothing)
            Logger.Log.Info(String.Format("Populate Company List  started "))
            Try
                Dim states = utility.StateRepository.Get(orderBy:=Function(q) q.OrderBy(Function(d) d.StateName))
                ViewBag.SelectedState = New SelectList(states, "StateId", "StateName", SelectedState)
                Logger.Log.Info(String.Format("Populate Company List successfully Loaded"))
            Catch dataEx As DataException
                TempData("ErrorMessage") = String.Concat("Unable to Populate Company List -", dataEx.Message)
                Logger.Log.Error(String.Format("Unable to Populate Company List with Message- {0} " + Environment.NewLine + "Stack Trace: {1} ", dataEx.Message, dataEx.StackTrace))
                Throw
            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Unable to Populate Company List-", ex.Message)
                Logger.Log.Error(String.Format("Unable to populate company list with Message- {0} " + Environment.NewLine + "Stack Trace: {1}", ex.Message, ex.StackTrace))
                Throw
            Finally
                Logger.Log.Info(String.Format("Execution Ended"))
            End Try
        End Sub

        <CustomActionFilter()>
        Function SearchCustomer() As ActionResult
            Try
                Dim customers = New List(Of Customer)
                'From cust In customerRepository.GetCustomers()()
                Logger.Log.Info(String.Format("SearchCustomer Executed"))
                Return View(customers.ToPagedList(1, 20))
            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Unable to SearchCustomer -", ex.Message)
                Logger.Log.Error(String.Format("Unable to SearchCustomer with Message- {0} " + Environment.NewLine + "Stack Trace: {1} ", ex.Message, ex.StackTrace))
                Return View()
            Finally
                Logger.Log.Info(String.Format("SearchCustomer Execution Ended"))
            End Try
        End Function

        <HttpPost()> _
       <ValidateAntiForgeryToken()> _
        <CustomActionFilter()>
        Function SearchCustomer(SubscribeUser As Customer) As ActionResult
            Dim customers As IEnumerable(Of Customer) = Enumerable.Empty(Of Customer)()
            Try
                If SubscribeUser.SearchCustId Is Nothing And SubscribeUser.SearchCustName Is Nothing And SubscribeUser.SAUName Is Nothing And SubscribeUser.SearchCustTelephone Is Nothing Then
                    TempData("ErrorMessage") = String.Concat("Please Enter value in Search Option")
                    Return View(customers.ToPagedList(1, 20))
                End If
                customers = From cust In customerRepository.SearchCustomers(SubscribeUser.SearchCustId, SubscribeUser.SearchCustName, SubscribeUser.SAUName, SubscribeUser.SearchCustTelephone)
                Logger.Log.Info(String.Format("SearchCustomer Executed"))
                Return View(customers.ToPagedList(1, 20))

            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Unable to Search Custgomer-", ex.Message)
                Logger.Log.Error(String.Format("Unable to SearchCustomer, CustomerId - {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2} ", SubscribeUser.CustomerId, ex.Message, ex.StackTrace))
                Return View(customers.ToPagedList(1, 20))
            Finally
                Logger.Log.Info(String.Format("SearchCustomer Execution Ended"))
            End Try

        End Function
    End Class
End Namespace
