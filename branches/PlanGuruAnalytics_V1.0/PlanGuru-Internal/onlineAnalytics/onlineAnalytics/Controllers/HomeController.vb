﻿Imports System
Imports System.Collections.Generic
Imports System.Configuration
Imports System.Linq
Imports System.Web.Mvc
Imports FarPoint.Mvc.Spread
Imports System.Data.Entity
Imports Recurly
Imports System.Data.Entity.Validation
Imports onlineAnalytics.Common

Namespace onlineAnalytics
    Public Class HomeController
        Inherits System.Web.Mvc.Controller
        Dim Format As Integer = 1
        Dim Period As Integer = 10
        Dim intFirstNumCol As Integer = 4
        Private utility As New Utility()
        Private common As New Common()
        Private userRepository As IUserRepository
        Private accountRepository As IAccountRepository
        Private chartFormatRepository As IChartFormatRepository
        Private chartTypeRepository As IChartTypeRepository
        Private balanceRepository As IBalanceRepository
        Private analysisRepository As IAnalysisRepository

        Public Sub New()
            Me.userRepository = New UserRepository(New DataAccess())
            Me.chartFormatRepository = New ChartFormatRepository(New DataAccess())
            Me.chartTypeRepository = New ChartTypeRepository(New DataAccess())
            Me.balanceRepository = New BalanceRepository(New DataAccess())
        End Sub

        Public Sub New(chartFormatRepository As IChartFormatRepository)
            Me.chartFormatRepository = chartFormatRepository
        End Sub

        Public Sub New(chartTypeRepository As IChartTypeRepository)
            Me.chartTypeRepository = chartTypeRepository
        End Sub

        Public Sub New(balanceRepository As IBalanceRepository)
            Me.balanceRepository = balanceRepository
        End Sub

        Public Sub New(userRepository As IUserRepository)
            Me.userRepository = userRepository
        End Sub

        Function Index() As ActionResult

            Dim str As String = common.Decrypt("f8DnZCDRLSa0bU4ySANm0Q==")
            'Database.SetInitializer(New SampleData())              
            ViewData("width") = 1000
            Return (View())
        End Function

        Function About() As ActionResult
            Return View()
        End Function

        Function Contact() As ActionResult
            ViewData("Message") = "Your contact page."
            Return View()
        End Function

        Function Subscribe() As ActionResult
            Try
                Dim subscription As String = String.Format("{0}={1}", "subscription%5Bplan_code%5D", "planguru-analytics")

                ViewBag.Signature = utility.SignWithParameters(subscription)
                ViewBag.AccountCode = utility.CheckAccountCodeOnRecurly((utility.CustomerRepository().GetCustomers().Max(Function(c) c.CustomerId) + 1))
                TempData("AccountCode") = ViewBag.AccountCode.ToString()
                Logger.Log.Info(String.Format("Subscribe Execution Done"))
                Return View()
            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Unable to Subscribe()-", ex.Message)
                Logger.Log.Error(String.Format("\n Unable to Subscribe() with Message- {0} " + Environment.NewLine + "Stack Trace: {1} ", ex.Message, ex.StackTrace))
                Return View()
            Finally
                Logger.Log.Info(String.Format("Subscribe() Execution Ended"))
            End Try

        End Function

        Function Receipt() As ViewResult
            Dim customer As Customer = Nothing
            Dim AccountCode As Integer
            Dim newUserAccountCode As Integer
            Dim userInfo As User = Nothing
            Dim username As String = String.Empty
            Try
                Logger.Log.Info(String.Format("Home - Subscription Execution Started"))
                If (Not TempData("AccountCode") Is Nothing) Then

                    AccountCode = TempData("AccountCode")

                End If

                If RecurlyAccount.IsExist(AccountCode) = True Then
                    Dim subscription As RecurlySubscription = RecurlySubscription.Get(AccountCode)

                    If Not (subscription Is Nothing) Then

                        Dim accountInfo As RecurlyAccount = RecurlyAccount.Get(AccountCode)
                        Dim billingInfo As RecurlyBillingInfo = RecurlyBillingInfo.Get(AccountCode)

                        customer = New Customer()
                        customer.CustomerId = AccountCode
                        customer.Quantity = subscription.Quantity
                        customer.CustomerFirstName = accountInfo.FirstName
                        customer.CustomerLastName = accountInfo.LastName
                        customer.CustomerEmail = accountInfo.Email
                        customer.CustomerCompanyName = accountInfo.CompanyName

                        '******Comment below code because of PCI compliance ***********
                        customer.ContactFirstName = String.Empty
                        customer.ContactLastName = String.Empty
                        'Credit Card Information                       
                        customer.FirstNameOnCard = String.Empty 'billingInfo.FirstName
                        customer.LastNameOnCard = String.Empty 'billingInfo.LastName
                        customer.CreditCardNumber = String.Empty 'String.Concat("XXXX-XXXX-XXXX-", billingInfo.CreditCard.LastFour)
                        customer.CVV = String.Empty 'billingInfo.VerificationValue
                        customer.ExpirationMonth = 0 'billingInfo.CreditCard.ExpirationMonth
                        customer.ExpirationYear = 0 'billingInfo.CreditCard.ExpirationYear
                        customer.CustomerAddress1 = String.Empty 'billingInfo.Address1
                        customer.CustomerAddress2 = String.Empty 'billingInfo.Address2

                        '******Comment below code because of PCI compliance ***********
                        'If (billingInfo.PostalCode.Length > 10) Then
                        '    customer.CustomerPostalCode = billingInfo.PostalCode.ToString().Substring(0, 10)
                        'Else
                        '    customer.CustomerPostalCode = billingInfo.PostalCode
                        'End If
                        customer.CustomerPostalCode = String.Empty

                        If (billingInfo.PhoneNumber.Length > 15) Then
                            customer.ContactTelephone = billingInfo.PhoneNumber.ToString().Substring(0, 15)
                        Else
                            customer.ContactTelephone = billingInfo.PhoneNumber
                        End If

                        customer.Country = billingInfo.Country
                        customer.State = String.Empty 'billingInfo.State
                        customer.City = String.Empty 'billingInfo.City

                        customer.SubscriptionStartAt = Convert.ToDateTime(subscription.CurrentPeriodStartedAt).ToString("MMM dd, yyyy")
                        customer.SubscriptionEndAt = Convert.ToDateTime(subscription.CurrentPeriodEndsAt).ToString("MMM dd, yyyy")
                        customer.SubscriptionAmount = (subscription.UnitAmountInCents / 100)
                        customer.CreatedBy = customer.CustomerId
                        customer.UpdatedBy = customer.CustomerId
                        utility.CustomerRepository.InsertCustomer(customer)
                        utility.CustomerRepository.Save()
                        Logger.Log.Info(String.Format("Home - Customer account created successfully"))
                        TempData("Message") = "Subscription created successfully."

                        newUserAccountCode = utility.CheckAccountCodeOnRecurly((utility.UserRepository().GetUsers().Max(Function(c) c.UserId) + 1))

                        'SAU user account creation

                        username = utility.AutoGenerateUserName()
                        Dim password As String = common.Generate(8, 2)

                        Logger.Log.Info(String.Format("Home - Username {0} , Password {1}", username, password))

                        userInfo = New User()
                        userInfo.UserId = newUserAccountCode
                        userInfo.CustomerId = customer.CustomerId
                        userInfo.UserName = username
                        userInfo.UserRoleId = UserRoles.SAU
                        userInfo.FirstName = accountInfo.FirstName
                        userInfo.LastName = accountInfo.LastName
                        userInfo.UserEmail = accountInfo.Email
                        userInfo.Status = CType(StatusE.Pending, Integer)
                        userInfo.CreatedBy = customer.CustomerId
                        userInfo.UpdatedBy = customer.CustomerId
                        userInfo.Password = common.Encrypt(password)
                        userInfo.SecurityKey = Guid.NewGuid().ToString()

                        utility.UserRepository.Insert(userInfo)
                        utility.UserRepository.Save()
                        Logger.Log.Info(String.Format("Home - User account created successfully"))

                        SendEmail(customer, EmailType.NewSubscriptionSignup)
                        SendEmail(userInfo, EmailType.NewUserSignUp)

                        ViewBag.SubscriptionInfo = customer
                        ViewBag.CreditCardLastFourDigit = billingInfo.CreditCard.LastFour.ToString()
                        ViewBag.CreditCardType = billingInfo.CreditCard.CreditCardType.ToString()
                    Else
                        TempData("ErrorMessage") = String.Concat("Account Code is missing !")
                        Logger.Log.Error(String.Format("Unable to Create Customer :- Account Code is missing."))
                    End If

                End If
                Logger.Log.Info(String.Format("Subscribe Receipt Function Executied"))
            Catch dbEntityEx As DbEntityValidationException

                Logger.Log.Info(String.Format("Home - Username {0}", username))

                For Each sError In dbEntityEx.EntityValidationErrors
                    Logger.Log.Error(String.Format("Entity of type ""{0}"" in state ""{1}"" has the following validation errors:", sError.Entry.Entity.[GetType].Name, sError.Entry.State))
                    For Each ve In sError.ValidationErrors
                        Logger.Log.Error(String.Format("-Property: {0}, Error: {1} ", ve.PropertyName, ve.ErrorMessage))
                    Next
                Next
            Catch dataEx As DataException
                TempData("ErrorMessage") = String.Concat("Unable to create customer-", dataEx.Message)
                Logger.Log.Error(String.Format("Unable to Create Customer id- {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2} ", customer.CustomerId, dataEx.Message, dataEx.StackTrace))
            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Unable to create customer-", ex.Message)
                Logger.Log.Error(String.Format("Unable to Create Customer id- {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2} ", customer.CustomerId, ex.Message, ex.StackTrace))

            Finally
                Logger.Log.Info(String.Format("Home - Subscription Execution Ended"))
            End Try
            Return View(customer)

        End Function

#Region "Subscribe Methods"
        Private Sub SendEmail(customer As Customer, emailId As Integer)
            Logger.Log.Info(String.Format("Customer SendEmail Execution Started"))
            Try
                Logger.Log.Info(String.Format("Trial sign-up confirmation - SendEmail Executed"))
                Dim objEmailInfo = utility.EmailRepository.GetEmailInfoById(emailId)
                objEmailInfo.EmailBody = common.MergeCustomerEmailBody(customer, objEmailInfo)
                common.SendCustomerEmail(customer, objEmailInfo)
                Logger.Log.Info(String.Format("Trial sign-up confirmation - SendEmail Ended"))
            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Unable to Send Email -", ex.Message)
                Logger.Log.Error(String.Format("Unable to Send Email to CustomerID - {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2} ", customer.CustomerId, ex.Message, ex.StackTrace))
            Finally
                Logger.Log.Info(String.Format("Customer SendEmail Execution Ended"))
            End Try
        End Sub

        Private Sub SendEmail(User As User, emailId As Integer)
            Logger.Log.Info(String.Format("User SendEmail Execution Started"))
            Try
                Logger.Log.Info("You have been added as a user - SendEmail Started")
                Dim planGuruUrl As String = "http://" + HostName(Request) + If(Request.Url.IsDefaultPort, Request.ApplicationPath + "/", ":" + Request.Url.Port.ToString() + "/")
                Dim objEmailInfo = utility.EmailRepository.GetEmailInfoById(emailId)
                objEmailInfo.EmailBody = common.MergeUserEmailBody(User, objEmailInfo, planGuruUrl)
                common.SendUserEmail(User, objEmailInfo)
                Logger.Log.Info("You have been added as a user - SendEmail Ended")
                Logger.Log.Info(String.Format("Email Sent Successfully to User: {0} for EmailId: {1}", User.UserId, emailId))
            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Unable to Send Email -", ex.Message)
                Logger.Log.Error(String.Format("Unable to Send Email, UserId - {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2} ", User.UserId, ex.Message, ex.StackTrace))
            Finally
                Logger.Log.Info(String.Format("User SendEmail Execution Ended"))
            End Try
        End Sub
#End Region

        Protected Overrides Sub Dispose(disposing As Boolean)
            userRepository.Dispose()
            chartFormatRepository.Dispose()
            chartTypeRepository.Dispose()
            balanceRepository.Dispose()
            MyBase.Dispose(disposing)
        End Sub

#Region "ForgotUsername Methods"
        <HttpGet()>
        Function ForgotUsername() As ActionResult
            TempData.Clear()
            Return View()
        End Function

        <HttpPost()>
        Function ForgotUsername(model As ForgotUsername) As ActionResult
            Logger.Log.Info(String.Format("ForgotUsername Function Execution Started"))
            Dim result As User = Nothing
            Try
                TempData.Clear()
                If ModelState.IsValid Then
                    result = utility.UserRepository.GetUsers.Where(Function(u) u.UserEmail = model.emailAddress).SingleOrDefault()
                    If (Not (result Is Nothing)) Then

                        If (SendMail_ForgotUserName(result, EmailType.ForgotUsername)) Then
                            TempData("Message") = "Email has been sent Successfully."
                            Logger.Log.Info(String.Format("ForgotUsername Reset Successfully of UserId : {0}", result.UserId))
                        End If
                    Else
                        TempData("InfoMessage") = "No user with the Email address entered was found."
                    End If
                End If
            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("There were no matching Planguru accounts found with the information you provided.-", ex.Message)
                Logger.Log.Error(String.Format("ForgotUsername Excecution failed- {0} " + Environment.NewLine + "Stack Trace: {1} ", ex.Message, ex.StackTrace))
            Finally
                Logger.Log.Info(String.Format("ForgotUsername Function Execution Ended"))
            End Try

            Return View()
        End Function

        Private Function SendMail_ForgotUserName(User As User, emailId As Integer) As Boolean
            Logger.Log.Info(String.Format("User SendEmail Execution Started"))
            Dim IsMailTrigger As Boolean = True
            Try
                Logger.Log.Info("You have been added as a user - SendMail_ForgotUserName Started")

                Dim objEmailInfo = utility.EmailRepository.GetEmailInfoById(emailId)
                objEmailInfo.EmailBody = common.MergeUserEmailBody(User, objEmailInfo, Nothing)
                common.SendUserEmail(User, objEmailInfo)
                Logger.Log.Info("You have been added as a user - SendEmail Ended")
                Logger.Log.Info(String.Format("Email Sent Successfully to User: {0} for EmailId: {1}", User.UserId, emailId))
            Catch ex As Exception
                IsMailTrigger = False
                TempData("ErrorMessage") = String.Concat("Unable to Send Email -", ex.Message)
                Logger.Log.Error(String.Format("Unable to Send Email, UserId - {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2} ", User.UserId, ex.Message, ex.StackTrace))
            Finally
                Logger.Log.Info(String.Format("SendMail_ForgotUserName Execution Ended"))
            End Try
            Return IsMailTrigger
        End Function
#End Region

#Region "ForgotPassword Methods"

        <HttpGet()>
        Function ForgotPassword() As ActionResult
            TempData.Clear()
            Return View()
        End Function

        Public Function ForgotPassword(userInput As ForgotPassword) As ActionResult
            Logger.Log.Info(String.Format("ForgotPassword Execution Started"))
            Dim userDetails As User = Nothing
            Try
                TempData.Clear()
                If ModelState.IsValid Then
                    userDetails = utility.UserRepository.GetUsers.Where(Function(u) u.UserName = userInput.userName And u.UserEmail = userInput.emailAddress).SingleOrDefault()

                    If (Not (userDetails Is Nothing)) Then
                        'userDetails = New User()
                        Dim password As String = common.Generate(8, 2)
                        userDetails.Password = common.Encrypt(password)
                        userDetails.SecurityKey = Guid.NewGuid().ToString()
                        userDetails.UpdatedBy = userDetails.UserId.ToString()
                        userDetails.UpdatedOn = DateTime.UtcNow()
                        utility.UserRepository.UpdateUser(userDetails)
                        utility.UserRepository.Save()

                        SendEmail(userDetails, EmailType.PasswordReset)
                        If (TempData("ErrorMessage") = Nothing) Then

                            TempData("Message") = "Reset Password link has been sent to your Email Id Successfully."
                        End If
                    Else
                        TempData("InfoMessage") = "No user with the Username and Email address entered was found."
                    End If
                End If
            Catch dbEntityEx As DbEntityValidationException
                TempData("ErrorMessage") = "Error occured, during reset password request - " + dbEntityEx.Message.ToString()
                For Each sError In dbEntityEx.EntityValidationErrors
                    Logger.Log.Error(String.Format("Entity of type ""{0}"" in state ""{1}"" has the following validation errors:", sError.Entry.Entity.[GetType].Name, sError.Entry.State))
                    For Each ve In sError.ValidationErrors
                        Logger.Log.Error(String.Format("-Property: {0}, Error: {1} ", ve.PropertyName, ve.ErrorMessage))
                    Next
                Next
            Catch ex As Exception
                TempData("ErrorMessage") = "Error occured, during reset password request - " + ex.Message.ToString()
                Logger.Log.Error(String.Format("Unable to Reset the password for {0} - " + Environment.NewLine + "{1}, Stack Trace: {2} ", userDetails.UserName, ex.Message, ex.StackTrace))
            Finally
                Logger.Log.Info(String.Format("ForgotPassword Execution Ended"))
            End Try
            Return View()
        End Function
#End Region
    End Class
End Namespace