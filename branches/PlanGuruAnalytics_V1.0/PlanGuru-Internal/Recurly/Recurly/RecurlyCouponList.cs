﻿namespace Recurly
{
    using System;
    using System.Collections.Generic;
    using System.Net;
    using System.Xml;

    public class RecurlyCouponList : List<RecurlyAccountCoupon>
    {
        internal RecurlyCouponList()
        {
        }

        public static RecurlyAccountCoupon[] GetCoupon()
        {
            RecurlyCouponList list = new RecurlyCouponList();
            if (RecurlyClient.PerformRequest(RecurlyClient.HttpRequestMethod.Get, "/v2/coupons", new RecurlyClient.ReadXmlDelegate(list.ReadXml)) == HttpStatusCode.NotFound)
            {
                return null;
            }
            return list.ToArray();
        }

        internal void ReadXml(XmlTextReader reader)
        {
            while (reader.Read())
            {
                if ((reader.Name == "coupon") && (reader.NodeType == XmlNodeType.EndElement))
                {
                    break;
                }
                if (reader.NodeType == XmlNodeType.Element)
                {
                    string name = reader.Name;
                    if ((name != null) && (name == "coupon"))
                    {
                        base.Add(new RecurlyAccountCoupon(reader));
                    }
                }
            }
        }
    }
}

