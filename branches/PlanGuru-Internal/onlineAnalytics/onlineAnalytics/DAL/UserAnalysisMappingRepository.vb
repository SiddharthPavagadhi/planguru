﻿Imports System.Collections.Generic
Imports System.Linq
Imports System.Data

Public Class UserAnalysisMappingRepository
    Inherits GenericRepository(Of UserAnalysisMapping)
    Implements IUserAnalysisMappingRepository
    Implements IDisposable
    'Private context As DataAccess
    Private disposed As Boolean = False

    Public Sub New(context As DataAccess)
        'Me.context = context
        MyBase.New(context)
    End Sub

    Public Function GetUserAnalysisMappingByAnalysisId(AnalysisId As Integer) As IEnumerable(Of UserAnalysisMapping) Implements IUserAnalysisMappingRepository.GetUserAnalysisMappingByAnalysisId
        Return context.UserAnalysisMappings.Where(Function(uc) uc.AnalysisId.Equals(AnalysisId))
    End Function

    Public Function GetUserAnalysisMapping() As IEnumerable(Of UserAnalysisMapping) Implements IUserAnalysisMappingRepository.GetUserAnalysisMapping
        Return context.UserAnalysisMappings.ToList()
    End Function
    Public Function GetUserAnalysisMappingById(UserAnalysisMappingId As Integer) As UserAnalysisMapping Implements IUserAnalysisMappingRepository.GetUserAnalysisMappingById
        Return context.UserAnalysisMappings.Find(UserAnalysisMappingId)
    End Function
    Public Sub UpdateUserAnalysisMapping(UserAnalysisDetails As UserAnalysisMapping) Implements IUserAnalysisMappingRepository.UpdateUserAnalysisMapping
        context.Entry(UserAnalysisDetails).State = EntityState.Modified
    End Sub

    Public Sub DeleteUserAnalysisMapping(UserAnalysisMappingId As Integer) Implements IUserAnalysisMappingRepository.DeleteUserAnalysisMapping
        Dim usercomp_map As UserAnalysisMapping = context.UserAnalysisMappings.Find(UserAnalysisMappingId)
        context.UserAnalysisMappings.Remove(usercomp_map)
    End Sub

    Public Function DeleteUserAnalysisMappingByUserIdAndAnalysisId(AnalysisId As Integer, UserId As String) As Integer Implements IUserAnalysisMappingRepository.DeleteUserAnalysisMappingByUserIdAndAnalysisId

        Dim deleteQuery As String = String.Format("Delete from UserAnalysisMapping where AnalysisId = {0} And UserId in ({1})", AnalysisId, UserId)
        Return context.Database.ExecuteSqlCommand(deleteQuery)
        context.SaveChanges()
    End Function

    Public Sub Save() Implements IUserAnalysisMappingRepository.Save
        context.SaveChanges()
    End Sub
    Protected Overridable Sub Dispose(disposing As Boolean)
        If Not Me.disposed Then
            If disposing Then
                context.Dispose()
            End If
        End If
        Me.disposed = True
    End Sub

    Public Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
End Class
