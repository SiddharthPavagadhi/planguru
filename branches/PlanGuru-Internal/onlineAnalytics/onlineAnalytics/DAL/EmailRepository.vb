﻿Imports System.Collections.Generic
Imports System.Linq
Imports System.Data

Public Class EmailRepository
    Inherits GenericRepository(Of EmailInfo)
    Implements IEmailRepository
    Implements IDisposable
    'Private context As DataAccess
    Private disposed As Boolean = False

    Public Sub New(context As DataAccess)
        'Me.context = context
        MyBase.New(context)
    End Sub

    Public Function GetEmailInfo() As IEnumerable(Of EmailInfo) Implements IEmailRepository.GetEmailInfo
        Return context.EmailInfos.ToList()
    End Function
    Public Function GetEmailInfoById(id As Integer) As EmailInfo Implements IEmailRepository.GetEmailInfoById
        Return context.EmailInfos.Find(id)
    End Function

    Public Sub Save() Implements IEmailRepository.Save
        context.SaveChanges()
    End Sub

    Protected Overridable Sub Dispose(disposing As Boolean)
        If Not Me.disposed Then
            If disposing Then
                context.Dispose()
            End If
        End If
        Me.disposed = True
    End Sub

    Public Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
End Class
