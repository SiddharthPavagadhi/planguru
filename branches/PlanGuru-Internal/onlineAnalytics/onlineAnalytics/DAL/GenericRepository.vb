﻿Imports System.Collections.Generic
Imports System.Linq
Imports System.Data
Imports System.Data.Entity
Imports System.Linq.Expressions

Public Class GenericRepository(Of TEntity As Class)
    Friend context As DataAccess
    Friend dbSet As DbSet(Of TEntity)

    Public Sub New(context As DataAccess)
        Me.context = context
        Me.dbSet = context.Set(Of TEntity)()
    End Sub

    Public Overridable Function [Get](
                                     Optional filter As Expression(Of Func(Of TEntity, Boolean)) = Nothing,
                                     Optional orderBy As Func(Of IQueryable(Of TEntity), IOrderedQueryable(Of TEntity)) = Nothing,
                                     Optional includeProperties As String = "") As IEnumerable(Of TEntity)
        Dim query As IQueryable(Of TEntity) = dbSet

        If filter IsNot Nothing Then
            query = query.Where(filter)
        End If

        For Each includeProperty As String In includeProperties.Split(New Char() {","c}, StringSplitOptions.RemoveEmptyEntries)
            query = query.Include(includeProperty)
        Next

        If orderBy IsNot Nothing Then
            Return orderBy(query).ToList()
        Else
            Return query.ToList()
        End If
    End Function

    Public Overridable Function GetByID(id As Object) As TEntity
        Return dbSet.Find(id)
    End Function

    Public Overridable Sub Insert(entity As TEntity)
        dbSet.Add(entity)
    End Sub

    Public Overridable Sub Delete(id As Object)
        Dim entityToDelete As TEntity = dbSet.Find(id)
        Delete(entityToDelete)
    End Sub

    Public Overridable Sub Delete(entityToDelete As TEntity)
        If context.Entry(entityToDelete).State = EntityState.Detached Then
            dbSet.Attach(entityToDelete)
        End If
        dbSet.Remove(entityToDelete)
    End Sub

    Public Overridable Sub Update(entityToUpdate As TEntity)
        dbSet.Attach(entityToUpdate)
        context.Entry(entityToUpdate).State = EntityState.Modified
    End Sub

    Public Overridable Function GetWithRawSql(query As String, ParamArray parameters As Object()) As IEnumerable(Of TEntity)
        Return dbSet.SqlQuery(query, parameters).ToList()
    End Function
End Class