﻿@ModelType onlineAnalytics.User
@Imports System.Collections.Generic
@Code
    ViewData("Title") = "CreateUser"
    Layout = "~/Views/Shared/AnalyticsMaster.vbhtml"
End Code

<script src="@Url.Content("~/Scripts/jquery.validate.min.js")" type="text/javascript"></script>
<script src="@Url.Content("~/Scripts/jquery.validate.unobtrusive.min.js")" type="text/javascript"></script>

<h2>User</h2><br />
<div style="float:right">
    @Html.ActionLink("View User List", "Index")
</div><br />
@Using Html.BeginForm("CreateUser", "User")
    @Html.AntiForgeryToken()   
    @Html.ValidationSummary(True)    
     
    @<fieldset >
            <legend>Add New User</legend>
        <div style="width: 100;">
            <div style="float: left; width: 33%;">
                <ol class="round">
                    <li>
                        @Html.LabelFor(Function(m) m.UserName)<br />
                        @Html.TextBoxFor(Function(m) m.UserName)<br />
                        @Html.ValidationMessageFor(Function(m) m.UserName)
                    </li>
                    <li>
                        @Html.LabelFor(Function(m) m.UserEmail)<br />
                        @Html.TextBoxFor(Function(m) m.UserEmail)<br />
                        @Html.ValidationMessageFor(Function(m) m.UserEmail)
                    </li>
                </ol>
            </div>
            <div style="float: left; width: 33%;">
                <ol class="round">
                    <li>
                        @Html.LabelFor(Function(m) m.FirstName)<br />
                        @Html.TextBoxFor(Function(m) m.FirstName)<br />
                        @Html.ValidationMessageFor(Function(m) m.FirstName)
                    </li>
                    <li>
                        @Html.LabelFor(Function(m) m.LastName)<br />
                        @Html.TextBoxFor(Function(m) m.LastName)<br />
                        @Html.ValidationMessageFor(Function(m) m.LastName)
                    </li>
                </ol>
            </div>
            <div style="float: left; width: 33%;">
                <ol class="round">
                    <li>
                        @Html.LabelFor(Function(m) m.UserRoleId)<br />
                        @Html.DropDownList("UserRoleId", DirectCast(ViewBag.SelectedRoles, SelectList), "Select User Role", New With {.Class = "select", .Style = "width:206px;"})<br />
                        @Html.ValidationMessageFor(Function(m) m.UserRoleId)
                    </li>
                    <li>
                        @Html.LabelFor(Function(m) m.CustomerId)<br />
                        @Html.DropDownList("CustomerId", DirectCast(ViewBag.SelectedCustomer, SelectList), "Select Customer", New With {.Class = "select", .Style = "width:206px;", .data = Url.Action("SubscriptionCost", "User")})<br />
                        @Html.ValidationMessageFor(Function(m) m.CustomerId)
                    </li>
                </ol>
            </div>
           
            <div class="sub_amount" >
            <div class="cost">
                
                Cost to AddOn new user to the subscription : <label id="AddOnAmountInCents"></label>
            </div>                   
            <div class="cost">
                Total cost to the subscription : <label id="SubsciptionUnitAmountInCents"></label>
            </div>                       
            </div>
            <div class="input-form">
                <input id="submit-button" type="submit" value="Create User" class="submit-button" />                              
            </div>
            <div style="float:left;margin-left:20px;">                              
                 @If Not (DirectCast(TempData("Message"), String) Is Nothing) Then
                    @<label class="message">@TempData("Message").ToString()
                    </label>                         
                End If
                @If Not (DirectCast(TempData("ErrorMessage"), String) Is Nothing) Then
                    @<label class="error">@TempData("ErrorMessage").ToString()
                    </label>                         
                End If
            </div>
        </div>
       <div id="loading" style="text-align:center;"></div>
    </fieldset>
    
End Using

<script type="text/javascript" language="javascript">

    var theDialog = $('#loading').dialog({
        open: function (event, ui) { $(".ui-dialog-titlebar-close").hide(); },        
        center_modal: true,
        autoOpen: false,
        modal: true,
        close: false,
        closeOnEscape: false,
        width: 200,
        modal: true,
        minHeight: 10,
        show: 'fade',
        hide: 'fade',
        resizable: 'false',
        draggable: false
    });


    $(document).ready(function () {

        //Input Mask for landline phone number
        $("#ContactTelephone").mask("(999)-999-9999");
        //$("#FiscalMonthStart").mask("9?9");        

        $('#AddOnAmountInCents').text("00.00").prepend("$").append(" USD");
        $('#SubsciptionUnitAmountInCents').text("00.00").prepend("$").append(" USD");

        $('#CustomerId').change(function (event) {

            event.preventDefault();

            // Get Dropdownlist seleted item text
            var customer = $("#CustomerId option:selected").text();
            // Get Dropdownlist selected item value
            var customerId = $("#CustomerId option:selected").val();

            var actionUrl = $(this).attr('data') + "?CustomerId=" + customerId;

            if (customerId.length > 0) {

                $.ajax({
                    type: "GET",
                    url: actionUrl,
                    global: false,
                    cache: false,
                    data: { CustomerId: customerId },
                    dataType: "json",
                    beforeSend: function () {

                        $(".ui-dialog-titlebar").hide();
                        $('#loading').html("Please wait.... <img src='../themes/default/images/loading.gif' />");
                        theDialog.dialog('open');
                    },
                    success: function (msg) {

                        $('#AddOnAmountInCents').text(msg.AddOnAmountInCents).prepend("$").append(" USD");
                        $('#SubsciptionUnitAmountInCents').text(msg.Subsciption_UnitAmountInCents).prepend("$").append(" USD");

                        theDialog.dialog('close');
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        var msg = JSON.parse(XMLHttpRequest.responseText);
                        theDialog.dialog('close');                        
                        //alert(msg.Message);                      
                    }
                });
            } else {
                $('#AddOnAmountInCents').text("00.00").prepend("$").append(" USD");
                $('#SubsciptionUnitAmountInCents').text("00.00").prepend("$").append(" USD");
            }
        })

    });
</script>
