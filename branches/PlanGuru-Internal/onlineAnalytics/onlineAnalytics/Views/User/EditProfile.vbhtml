﻿@ModelType onlineAnalytics.ResetPassword
@Imports System.Collections.Generic
@Code
    ViewData("Title") = "Edit Profile"
    Layout = "~/Views/Shared/AnalyticsMaster.vbhtml"
End Code
<script src="@Url.Content("~/Scripts/jquery.validate.min.js")" type="text/javascript"></script>
<script src="@Url.Content("~/Scripts/jquery.validate.unobtrusive.min.js")" type="text/javascript"></script>
<h2>
    Edit Profile</h2>
<br />
@Using Html.BeginForm("EditProfile", "User")
    @Html.AntiForgeryToken()   
    @Html.ValidationSummary(True)    
     
    @<fieldset>
        <legend>Change Password</legend>
        <div style="width: 100;">
            <div style="float: left; width: 33%; display: inline-block;">
                <ol class="round">
                    <li>
                        @Html.LabelFor(Function(m) m.UserName)<br />
                        @Html.TextBoxFor(Function(m) m.UserName)<br />
                        @Html.ValidationMessageFor(Function(m) m.UserName)
                    </li>
                    <li>
                        @Html.LabelFor(Function(m) m.FirstName)<br />
                        @Html.TextBoxFor(Function(m) m.FirstName)<br />
                        @Html.ValidationMessageFor(Function(m) m.FirstName)
                    </li>
                    <li>
                        @Html.LabelFor(Function(m) m.LastName)<br />
                        @Html.TextBoxFor(Function(m) m.LastName)<br />
                        @Html.ValidationMessageFor(Function(m) m.LastName)
                    </li>
                    <li>
                        <div style="padding-right: 10px; float: left;">@Html.Label("Do you want to Change Password ?")</div>@Html.CheckBoxFor(Function(m) m.ChangePassword)
                    </li>
                </ol>
            </div>
            <div id="ResetPassword" style="width: 33%; float: left; display: none;">
                <ol class="round">
                    <li>
                        @Html.LabelFor(Function(m) m.CurrentPassword)<br />
                        @Html.PasswordFor(Function(m) m.CurrentPassword)<br />
                        @Html.ValidationMessageFor(Function(m) m.CurrentPassword, "", New With {.id = "ValidateCurrentPassword"})
                    </li>
                    <li>
                        @Html.LabelFor(Function(m) m.NewPassword)<br />
                        @Html.PasswordFor(Function(m) m.NewPassword)<br />
                        @Html.ValidationMessageFor(Function(m) m.NewPassword, "", New With {.id = "ValidateNewPassword"})
                    </li>
                    <li>
                        @Html.LabelFor(Function(m) m.ConfirmNewPassword)<br />
                        @Html.PasswordFor(Function(m) m.ConfirmNewPassword)<br />
                        @Html.ValidationMessageFor(Function(m) m.ConfirmNewPassword,"", New With {.id = "ValidateConfirmPassword"})
                    </li>
                </ol>
            </div>
            <div class="input-form">
                <input id="Update" type="submit" value="Update" class="submit-button" />
            </div>
            <div style="float: left; margin-left: 20px;">
                @If Not (DirectCast(TempData("Message"), String) Is Nothing) Then
                    @<label class="message">@TempData("Message").ToString()
                    </label>                         
    End If
                @If Not (DirectCast(TempData("ErrorMessage"), String) Is Nothing) Then
                    @<label class="error">@TempData("ErrorMessage").ToString()
                    </label>                         
    End If
            </div>
            @Html.HiddenFor(Function(m) m.UserId)
        </div>
    </fieldset>                                  
End Using
<script type="text/javascript" language="javascript">

    $(document).ready(function () {

        //Input Mask for landline phone number
        $("#ContactTelephone").mask("(999)-999-9999");
        //$("#FiscalMonthStart").mask("9?9");

        ChangePassword();

        $("#ChangePassword").click(function (event) {
            ChangePassword();
        });

        function ChangePassword() {

            if ($("#ChangePassword").is(':checked')) {
                $("#ResetPassword").show();
            }
            else {
                $("#ResetPassword").hide();
            }
        }

        $('#Update').click(function (event) {

            var validate = true;

            if ($("#ChangePassword").is(':checked')) {

                validate = validateCurrentPassword(validate);
                validate = validateNewPassword(validate);
                validate = validateConfirmPassword(validate);
            }

            return validate;
        });

        $("#CurrentPassword").blur(function (event) {
            validateCurrentPassword(true);
        });

        function validateCurrentPassword(validate) {


            if ($("#CurrentPassword").val() == "") {
                $("#ValidateCurrentPassword").removeClass("field-validation-valid").addClass("field-validation-error");
                $("#ValidateCurrentPassword").text("Current Password is required.");
                return false;
            }
            else {
                $("#ValidateCurrentPassword").removeClass("field-validation-error").addClass("field-validation-valid");
                $("#ValidateCurrentPassword").text("");
            }

            if (($("#CurrentPassword").val() != "") && validate == false) {
                return false;
            }

            return true;
        }

        $("#NewPassword").blur(function (event) {
            validateNewPassword(true);
        });
        function validateNewPassword(validate) {

            if ($("#NewPassword").val() == "") {
                $("#ValidateNewPassword").removeClass("field-validation-valid").addClass("field-validation-error");
                $("#ValidateNewPassword").text("New Password is required.");
                return false;
            }
            else {
                $("#ValidateNewPassword").removeClass("field-validation-error").addClass("field-validation-valid");
                $("#ValidateNewPassword").text("");
            }

            if (($("#NewPassword").val() != "") && validate == false) {

                return false;
            }

            return true;
        }


        $("#ConfirmNewPassword").blur(function (event) {
            validateConfirmPassword(true);
        });

        function validateConfirmPassword(validate) {

            if ($("#ConfirmNewPassword").val() == "") {
                $("#ValidateConfirmPassword").removeClass("field-validation-valid").addClass("field-validation-error");
                $("#ValidateConfirmPassword").text("Confirm New Password is required.");
                return false;
            }
            else {
                $("#ValidateConfirmPassword").removeClass("field-validation-error").addClass("field-validation-valid");
               // $("#ValidateConfirmPassword").text("");
            }

            if (($("#ConfirmNewPassword").val() != "") && validate == false) {

                return false;
            }

            return true;
        }


    });
</script>
