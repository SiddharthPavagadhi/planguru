﻿@ModelType onlineAnalytics.User
@Imports System.Collections.Generic
@Code
    ViewData("Title") = "Edit User"
    Layout = "~/Views/Shared/AnalyticsMaster.vbhtml"
End Code

<script src="@Url.Content("~/Scripts/jquery.validate.min.js")" type="text/javascript"></script>
<script src="@Url.Content("~/Scripts/jquery.validate.unobtrusive.min.js")" type="text/javascript"></script>

<h2>User</h2><br />
<div style="float:right">
    @Html.ActionLink("User List", "Index")
</div><br />
@Using Html.BeginForm("Edit", "User")
    @Html.AntiForgeryToken()   
    @Html.ValidationSummary(True)    
     
    @<fieldset>
        <legend>Edit User</legend>
        <div style="width: 100;">
            <div style="float: left; width: 33%;">
                <ol class="round">
                     <li>
                        @Html.LabelFor(Function(m) m.UserName)<br />
                        @Html.TextBoxFor(Function(m) m.UserName)<br />
                        @Html.ValidationMessageFor(Function(m) m.UserName)
                    </li>
                   @* <li>
                        @Html.LabelFor(Function(m) m.Password)<br />
                        @Html.PasswordFor(Function(m) m.Password)<br />
                        @Html.ValidationMessageFor(Function(m) m.Password)
                    </li>*@
                    <li>
                        @Html.LabelFor(Function(m) m.UserEmail)<br />
                        @Html.TextBoxFor(Function(m) m.UserEmail)<br />
                        @Html.ValidationMessageFor(Function(m) m.UserEmail)
                    </li>
                </ol>
            </div>
            <div style="float: left; width: 33%;">
                <ol class="round">
                   <li>
                        @Html.LabelFor(Function(m) m.FirstName)<br />
                        @Html.TextBoxFor(Function(m) m.FirstName)<br />
                        @Html.ValidationMessageFor(Function(m) m.FirstName)
                    </li>
                    <li>
                        @Html.LabelFor(Function(m) m.LastName)<br />
                        @Html.TextBoxFor(Function(m) m.LastName)<br />
                        @Html.ValidationMessageFor(Function(m) m.LastName)
                    </li>
                </ol>
            </div>
            <div style="float: left; width: 33%;">
                <ol class="round">
                    <li>
                        @Html.LabelFor(Function(m) m.UserRoleId)<br />
                        @Html.DropDownList("UserRoleId", DirectCast(ViewBag.SelectedRoles, SelectList), "Select User Role", New With {.Class = "select", .Style = "width:206px;"})<br />
                        @Html.ValidationMessageFor(Function(m) m.UserRoleId)
                    </li>
                    <li>
                        @Html.LabelFor(Function(m) m.CustomerId)<br />
                        @Html.DropDownList("CustomerId", DirectCast(ViewBag.SelectedCustomer, SelectList), "Select Customer", New With {.Class = "select", .Style = "width:206px;"})<br />
                        @Html.ValidationMessageFor(Function(m) m.CustomerId)
                    </li>
                </ol>
            </div>
            <div class="input-form">
                <input id="Submit" type="submit" value="Update User details" class="submit-button" /><input id="Cancel" type="button" value="Cancel" class="submit-button button" data=@Url.Action("Index") />
            </div>
            <div style="float:left;margin-left:20px;">    
                @If Not (DirectCast(TempData("Message"), String) Is Nothing) Then
                    @<label class="message">@TempData("Message").ToString()
                    </label>                         
                End If
                @If Not (DirectCast(TempData("ErrorMessage"), String) Is Nothing) Then
                    @<label class="error">@TempData("ErrorMessage").ToString()
                    </label>                         
                End If
            </div>
             @Html.HiddenFor(Function(m) m.UserId)
        </div>
    </fieldset>                                  
End Using



<script type="text/javascript" language="javascript">

    $(document).ready(function () {
        var msg;
        //Input Mask for landline phone number
        $("#ContactTelephone").mask("(999) 999-9999");
        //$("#FiscalMonthStart").mask("9?9");


        $("#Cancel").click(function (event) {
            //event.preventDefault();
            var url = $(this).attr('data');
            window.location = url;
        });


    });
</script>
