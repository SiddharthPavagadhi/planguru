﻿@ModelType  PagedList.IPagedList(Of onlineAnalytics.User)
@Code
    ViewData("Title") = "View Companies"
    Layout = "~/Views/Shared/AnalyticsMaster.vbhtml"
End Code

<h2>User</h2>
<div style="float: right">
    @Html.ActionLink("Add New User", "CreateUser")
</div>
<br />
<fieldset>
    <legend>View User List</legend>
    <br />
    <table class="table-layout">
        <tr>
            <th class="left" style="width: 20%">
                User Name
            </th>
            <th class="left" style="width: 20%">
                User Full Name
            </th>
            <th class="left" style="width: 20%">
                User Email
            </th>
            <th class="center" style="width: 15%">
                User Role
            </th>         
            <th class="center" style="width:5%">
                Edit
            </th>
            <th class="center" style="width:5%">
                Delete
            </th>            
        </tr>
        @For Each item In Model
            Dim currentItem = item
            @<tr>
                <td class="left">
                    @Html.DisplayFor(Function(modelItem) currentItem.UserName)
                </td>
                <td class="left">
                    @Html.DisplayFor(Function(modelItem) currentItem.LastName)
                    @Html.DisplayFor(Function(modelItem) currentItem.FirstName)
                </td>
                <td class="left">
                    @Html.DisplayFor(Function(modelItem) currentItem.UserEmail)
                </td>
                <td class="center">
                    @Html.DisplayFor(Function(modelItem) currentItem.UserRole.RoleName)
                </td>               
                <td class="center">
                    @Html.ActionLink("Edit", "Edit", New With {.userAccountCode = currentItem.UserId})
                </td>
                <td class="center">
                    <a id=@currentItem.UserId class="Delete" href="#" data='@Url.Action("Delete", "User", New With {.userAccountCode = currentItem.UserId})' >
                        Delete</a>
                </td>               
            </tr>
        Next
    </table>    
    @If Not (DirectCast(TempData("Message"), String) Is Nothing) Then
        @<label class="success">@TempData("Message").ToString()
        </label>                         
    End If
    @If Not (DirectCast(TempData("ErrorMessage"), String) Is Nothing) Then
        @<label class="error">@TempData("ErrorMessage").ToString()
        </label>                         
    End If
</fieldset>
<script type="text/javascript" language="javascript">

    $(".Delete").click(function (event) {
        event.preventDefault();
        var url = $(this).attr('data');

        $.msgBox({
            title: "Confirm",
            content: "Are you sure you want to delete this user?",
            type: "confirm",
            buttons: [{ value: "Yes" }, { value: "No"}],
            success: function (result) {
                if (result == "Yes") {
                    window.location = url;
                }
            }
        });

    });

    $(document).ready(function () {

    });
</script>

