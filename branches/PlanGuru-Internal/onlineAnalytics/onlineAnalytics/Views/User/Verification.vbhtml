﻿@ModelType onlineAnalytics.UserVerification
@Imports System.Collections.Generic
@Code
    ViewData("Title") = "Verification"
End Code
<script src="@Url.Content("~/Scripts/jquery.validate.min.js")" type="text/javascript"></script>
<script src="@Url.Content("~/Scripts/jquery.validate.unobtrusive.min.js")" type="text/javascript"></script>
<h2>
    Verification</h2>
<br />
@Using Html.BeginForm("Verification", "User")
    @Html.AntiForgeryToken()   
    @Html.ValidationSummary(True)    
     
    @<fieldset>
        @Html.HiddenFor(Function(m) m.UserId)
        <legend>Reset Password</legend>
        <div style="width: 100;">
            <div style="float: left; width: 33%;">
                <ol class="round">
                    <li>
                        @Html.LabelFor(Function(m) m.CurrentPassword)<br />
                        @Html.PasswordFor(Function(m) m.CurrentPassword)<br />
                        @Html.ValidationMessageFor(Function(m) m.CurrentPassword)
                    </li>
                    <li>
                        @Html.LabelFor(Function(m) m.NewPassword)<br />
                        @Html.PasswordFor(Function(m) m.NewPassword)<br />
                        @Html.ValidationMessageFor(Function(m) m.NewPassword)
                    </li>
                    <li>
                        @Html.LabelFor(Function(m) m.ConfirmNewPassword)<br />
                        @Html.PasswordFor(Function(m) m.ConfirmNewPassword)<br />
                        @Html.ValidationMessageFor(Function(m) m.ConfirmNewPassword)
                    </li>
                </ol>
            </div>
            <div class="input-form">
                <input type="submit" value="Reset" class="submit-button" />
            </div>
            <div style="float: left; margin-left: 20px;">
                @If Not (DirectCast(TempData("Message"), String) Is Nothing) Then
                    @<label class="message">@TempData("Message").ToString()
                    </label>                         
    End If
                @If Not (DirectCast(TempData("ErrorMessage"), String) Is Nothing) Then
                    @<label class="error">@TempData("ErrorMessage").ToString()
                    </label>                         
    End If
            </div>
        </div>
    </fieldset>                                  
End Using
<script type="text/javascript" language="javascript">

    $(document).ready(function () {

        //Input Mask for landline phone number
        $("#ContactTelephone").mask("(999)-999-9999");
        //$("#FiscalMonthStart").mask("9?9");


    });
</script>
