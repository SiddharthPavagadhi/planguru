﻿<!DOCTYPE html>
<html>
<head>
    <title>@ViewData("Title")</title>
    <link href="@Url.Content("~/Content/Site.css")" rel="stylesheet" type="text/css" />
    <link href="@Url.Content("~/Content/jquery-ui-1.10.0.custom.css")" rel="stylesheet" type="text/css" />
    <script src="@Url.Content("~/Scripts/jquery-1.9.1.min.js")" type="text/javascript"></script>
    <script src="@Url.Content("~/Scripts/modernizr-1.7.min.js")" type="text/javascript"></script>
    <script src="@Url.Content("~/Scripts/jquery-ui-1.10.0.custom.js")" type="text/javascript"></script>
    @* <script src="@Url.Content("https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js")" type="text/javascript"></script>    
    <script src="@Url.Content("https://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js")" type="text/javascript"></script>    *@
</head>
<body>
    <style type="text/css">
        #loginPanel tr td a
        {
            color: White;
            cursor: pointer;
            text-align: center;
            width: auto;
            height: auto;
            text-decoration: none;
            padding:3px 20px;
            /*background-color: #107FC7; */
            background-color: #4D90FE;
            background-image: -moz-linear-gradient(center top , #4D90FE, #4787ED);
            background-repeat: repeat-x;
        }
        
        #loginPanel tr td a:hover
        {
            /*background-color: #95B8D3;*/
            background-color: #0072BB;
            background-image: -moz-linear-gradient(center top , #0072BB, #0072BB);
            background-repeat: repeat-x;
            cursor: pointer;
            color: White;
            text-align: center;
            cursor: pointer;
            width: auto;
            padding:3px 20px;
            height: auto !important;
        }
    </style>
    <table style="width: 100%; border: none;" id="loginPanel">
        <tr>
            <td align="left">
                <img  style="margin-left: -28px;" src="@Url.Content("~/Content/Images/logo-planguru-analytics.png")" alt="Analytics logo" />
            </td>
            <td align="right">
                <div>
                    User ID: @Html.TextBox("userid", "", New With {.Style = "width: 100px;"})
                    Password: @Html.TextBox("password", "", New With {.type = "password", .Style = "width: 100px;"})
                    <!--input type="button" value="Login" id="login" class="button_example" /-->  
                    <a href='#' id="login" >Login</a>
                </div>
            </td>
        </tr>
    </table>
    <div class="nav-panel" id="menucontainer">
        <ul>
            <li><a>@Html.ActionLink("How it works", "", "")</a></li>
            <li><a>@Html.ActionLink("Sample Dashboard", "Index", "Dashboard")</a></li>
            <li><a>@Html.ActionLink("Sample Revenue and Expenses View", "Index", "RE")</a></li>
            <li><a>@Html.ActionLink("Pricing", "", "")</a></li>
            <li><a>@Html.ActionLink("Sign Up", "Subscribe", "Subscribe")</a></li>
        </ul>
    </div>
    <br />
    <div>
        @RenderBody()
    </div>
    <div id="loading" style="text-align: center;">
    </div>
    <div id="validation" style="text-align: center;">
    </div>
</body>
</html>
<script type="text/javascript" language="javascript">

    var theDialog = $('#loading').dialog({
        open: function (event, ui) { $(".ui-dialog-titlebar-close").hide(); },
        center_modal: true,
        autoOpen: false,
        modal: true,
        close: false,
        closeOnEscape: false,
        width: 200,
        modal: true,
        minHeight: 10,
        show: 'fade',
        hide: 'fade',
        resizable: 'false',
        draggable: false
    });

    var validationDialog = $('#validation').dialog({
        title: "Login",
        open: function (event, ui) { $(".ui-dialog-titlebar-close").hide(); },
        center_modal: true,
        autoOpen: false,
        modal: true,
        close: true,
        closeOnEscape: true,
        width: 500,
        modal: true,
        minHeight: 10,
        show: 'fade',
        hide: 'fade',
        resizable: 'false',
        draggable: false,
        buttons: { "OK": function () {
            $(this).dialog("close");
        }
        }
    });


    $(document).ready(function (event) {

        $('#login').click(function (event) {

            event.preventDefault();

            if ($('#userid').val() == "") {

                $('#validation').html("Enter your Username.");
                validationDialog.dialog('open');
            }
            else if ($('#password').val() == "") {

                $(".ui-dialog-titlebar").show();
                $('#validation').html("Enter your Password.");
                validationDialog.dialog('open');
            }
            else if ($('#userid').val() != "" && $('#password').val() != "") {


                $.ajax({
                    type: "GET",
                    url: '@Url.Action("Authenticate", "User")',
                    global: false,
                    cache: false,
                    data: { userName: $('#userid').val(), password: $('#password').val(), returnUrl: "http://localhost:51542/" },
                    dataType: "json",
                    beforeSend: function () {

                        $(".ui-dialog-titlebar").hide();
                        $('#loading').html("Please wait.... <img src='../themes/default/images/loading.gif' />");
                        theDialog.dialog('open');
                    },
                    success: function (msg) {

                        theDialog.dialog('close');
                        if (msg.Success) {

                            //alert("Success");
                            //alert(msg.Url);
                            window.location = msg.Url;
                        }

                        if (msg.Error) {

                            $(".ui-dialog-titlebar").show();
                            $('#validation').html(msg.Error);
                            validationDialog.dialog('open');
                        }



                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        var msg = JSON.parse(XMLHttpRequest.responseText);
                        theDialog.dialog('close');
                        //alert(msg.Message);     

                        $(".ui-dialog-titlebar").show();
                        $(".ui-dialog-titlebar").text("Login");
                        $('#validation').html(msg);
                        validationDialog.dialog('open');
                    }
                });
            }


        });
    });
</script>
