﻿@Imports  onlineAnalytics
<!DOCTYPE html>
<html>
<head>
    <title>@ViewData("Title")</title>
    <link href="@Url.Content("~/Content/css/styles.css")" rel="stylesheet" type="text/css" />
    <script src="@Url.Content("~/Scripts/jquery-1.9.1.min.js")" type="text/javascript"></script>
    <script src="@Url.Content("~/Scripts/jquery-ui-1.10.0.custom.js")" type="text/javascript"></script>
    <script src="@Url.Content("~/Scripts/dropdown.js")" type="text/javascript"></script>
    <script src="@Url.Content("~/Scripts/jquery.msgBox.js")" type="text/javascript"></script>
    <link href="@Url.Content("~/Content/msgBoxLight.css")" rel="stylesheet" type="text/css" />
</head>
<body>
    @Code
        Dim UserInfo As User
        Dim UserName As String = String.Empty
        Dim ua As New UserAccess
        Dim Administrator As Boolean = False 
        If Not (Session("UserInfo") Is Nothing) Then
            UserInfo = DirectCast(Session("UserInfo"), User)
            UserName = String.Concat("Welcome <b>", UserInfo.LastName, " ", UserInfo.FirstName, "</b>")
            
            If (UserInfo.UserRoleId = UserRoles.PGAAdmin Or UserInfo.UserRoleId = UserRoles.PGASupport) Then
                Administrator = True 
            End If
        End If
            
        If Not (Session("UserAccess") Is Nothing) Then
                
            ua = DirectCast(Session("UserAccess"), UserAccess)
                
        End If
    End code
    <div class="wrapper">
        <div class="main-container">
            <!--Header Container -->
            <div class="header">
                <div class="logo-panel">
                    <a href="#">
                        <img src="../../Content/Images/planguru_logo.png" alt="Planguru Analytics" /></a></div>
                <div class="header-right">
                    <div class="user-panel">
                        <a href='@Url.Action("Logout", "User")' class="logout-section">Logout</a>
                        <div class="welcome-user">
                            <span>@MvcHtmlString.Create(UserName)</span>
                        </div>
                    </div>
                    <!--div class="select-panel">
                        <div class="topnav-panel">
                            <div class="top-nav">
                                <ul>
                                    <li class="dropdown"><a href="#">New Horizon Software Technologies</a>
                                        <div class="top-nav-dropdown">
                                            <ul>
                                                <li><a href="#">Cogent Technologies</a></li>
                                                <li><a href="#">Solar T Wind Energy</a></li>
                                                <li><a href="#">Moore Building Supplies Corp</a></li>
                                                <li><a href="#" class="sublink">Add /</a> <a href="#" class="sublink">Delete /</a> <a
                                                    href="#" class="sublink">Edit Companies</a></li>
                                            </ul>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div class="top-nav">
                                <ul>
                                    <li class="dropdown"><a href="#">2013 Budget Revised</a>
                                        <div class="top-nav-dropdown-additional">
                                            <ul>
                                                <li><a href="#">Consolidation</a></li>
                                                <li><a href="#">Manufacturing Division</a></li>
                                                <li><a href="#">Sales Division</a></li>
                                                <li><a href="#">2012 Budget</a></li>
                                                <li><a href="#">Support Division</a></li>
                                                <li><a href="#" class="sublink">Add /</a> <a href="#" class="sublink">Delete /</a> <a
                                                    href="#" class="sublink">Edit Analysis</a></li>
                                            </ul>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div class="top-nav">
                                <ul>
                                    <li class="dropdown-last"><a href="#">Manage Account</a>
                                        <div class="top-nav-dropdown-additional">
                                            <ul>
                                                <li><a href="#">A/D Users to analysis</a></li>
                                                <li><a href="#">A/C/D Companies</a></li>
                                                <li><a href="#">A/C/D Analysis</a></li>
                                                <li><a href="#">A/C/D Subscription Users</a></li>
                                                <li><a href="#">Edit Subscription</a></li>
                                                <li><a href="#">Cancel Subscription</a></li>
                                                <li><a href="#">Search customers</a></li>
                                            </ul>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div-->
                </div>
            </div>
            <!--Header Container -->
            <!-- Nav Panel -->
            <div class="nav-panel">
                <ul>
                    <li><a href="#">Dashboard</a></li>
                    <li><a href="#">Revenue &amp; Expenses</a></li>
                    <li><a href="#">Assets, Liabilities &amp; Equity</a></li>
                    <li><a href="#">Cash Flows</a></li>
                    <li><a href="#">Financial Ratios</a></li>
                    <li><a href="#">Other Metrics</a></li>
                </ul>              
                <ul class="top-nav" style="float:right;background:url(../Content/Images/smallDownArrow.gif) no-repeat right center;">
                    <li class="dropdown-last" style="padding-right:0px;"><a href="#" >Manage Account</a>
                        <div class="top-nav-dropdown-additional">
                            <ul>                                
                                <li><a href='@Url.Action("Index","Company")'>Companies</a></li>
                                <li><a href='@Url.Action("Index","Analysis")'>Analyses</a></li>
                                @Code
                                    If (Administrator) Then
                                         @<li><a href='@Url.Action("Index","Subscribe")'>Subscription Users</a></li>
                                    Else
                                         @<li><a href='@Url.Action("Edit","Subscribe")'>Edit Subscription</a></li>
                                         @<li><a href='@Url.Action("Cancel","Subscribe")'>Cancel Subscription</a></li>
                                    End If
                                End Code
                                <li><a href="#">Search customers</a></li>
                            </ul>
                        </div>
                    </li>
                </ul>
            </div>           
            <!-- Nav Panel -->
            <!-- Bredcrumb panel -->
            <div class="bredcurm-panel">
                <div class="home-icon">
                </div>
                <div class="bredcrumb-nav">                   
                    <ul>
                        <li><a href="#">Home</a></li>
                        <li class="last-arrow"><a href="#">Dashboard</a></li>
                    </ul>
                </div>
            </div>
            <!-- Bredcrumb panel -->
            <div class="center-panel">
                <div style="margin: 8px 0px;">
                    @RenderBody()
                </div>
            </div>
        </div>
        <div class="footer">
            <div class="footer-panel">
                &copy; Planguru Analytics 2013</div>
        </div>
    </div>
</body>
</html>
