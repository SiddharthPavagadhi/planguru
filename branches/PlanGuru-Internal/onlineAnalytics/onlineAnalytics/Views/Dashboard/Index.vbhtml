﻿@Imports Farpoint.Web.Spread
@Imports Farpoint.Mvc.Spread
@Imports  onlineAnalytics
@Code
    ViewData("Title") = "Index"
    Layout = "~/Views/Shared/AnalyticsMaster.vbhtml"
End Code
<script type="text/javascript">
    $(function () {
        $("#updatesetup")
            .button()
            .click(function (event) {
                document.location = '@Url.Action("Index", "DashboardSetup")';
            });
    });
       
</script>
@Code
    Dim UserInfo As User
    Dim UserName As String = String.Empty
    Dim ua As New UserAccess
    If Not (Session("UserInfo") Is Nothing) Then
        UserInfo = DirectCast(Session("UserInfo"), User)
        UserName = String.Concat("Welcome <b>", UserInfo.LastName, " ", UserInfo.FirstName, "</b>")
    End If
            
    If Not (Session("UserAccess") Is Nothing) Then
                
        ua = DirectCast(Session("UserAccess"), UserAccess)
                
    End If
End code
<table style="width: 83%;" class="ceter-top-select">
    <tr>
       
        <td>
            <span style="margin-left: 10px;">Analysis:</span>
        </td>
        <td>
            <span style="margin-left: 10px;">Period:</span>
        </td>
        <td>
        </td>
    </tr>
    <tr>
        <td>
            @If ((Not ViewBag.UserCompanies Is Nothing) And (Not ViewBag.CompanyAnalyses Is Nothing)) Then
    
                @Html.DropDownList("CompanyId", DirectCast(ViewBag.UserCompanies, SelectList), New With {.Class = "input-text"})   
                
                @Html.DropDownList("AnalysisId",DirectCast(ViewBag.CompanyAnalyses, SelectList),"Select Analysis", New With {.Class = "input-text"})    
            End If
        </td>      
        <!--td>
            @Html.DropDownList("intDept", DirectCast(ViewData("Dept"), SelectList), New With {.Class = "input-text"})
        </td-->
        <td>
            @Html.DropDownList("intPeriod", DirectCast(ViewData("Periods"), SelectList), New With {.Class = "input-text"})
        </td>
        <td>
            <a href="#" class="button_example" id="updatesetup">Change
Dashboard Setup</a>
        </td>
    </tr>
</table>
<div id="spread" style="padding-top: 1px;display:inline-block; ">
    @Html.FpSpread("spdDashboard", Sub(x)
                                       x.RowHeader.Visible = False
                                       x.ActiveSheetView.PageSize = 1000
                                   End Sub)
</div>
<script language="javascript" type="text/javascript">

    $("#CompanyId").change(function (event) {
        event.preventDefault();
        var url = '/Dashboard/Index?';
        var selectedCompany = $("#CompanyId").val();
        var selectedAnalysis = $("#AnalysisId").val();

        if (selectedCompany != "")
        { url = url + "SelectedCompany=" + selectedCompany; }

        if (selectedCompany != "")
        { url = url + "&SelectedAnalysis=" + selectedAnalysis; }

        window.location = url;
    }); 
</script>
