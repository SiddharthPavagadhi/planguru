﻿@ModelType onlineAnalytics.Customer
<div style="width: 100;">
            <div style="float: left; width: 33%;">
                <ol class="round">
                    <li>
                        @Html.LabelFor(Function(m) m.SearchCustId)<br />
                        @Html.TextBoxFor(Function(m) m.SearchCustId)<br />
                    </li>
                    <li>
                        @Html.LabelFor(Function(m) m.SearchCustName)<br />
                        @Html.TextBoxFor(Function(m) m.SearchCustName)<br />
                    </li>
                </ol>
            </div>
            <div style="float: left; width: 33%;">
                <ol class="round">
                    <li>
                        @Html.LabelFor(Function(m) m.SAUName)<br />
                        @Html.TextBoxFor(Function(m) m.SAUName)<br />
                    </li>
                    <li>
                        @Html.LabelFor(Function(m) m.SearchCustTelephone)<br />
                        @Html.TextBoxFor(Function(m) m.SearchCustTelephone)<br />
                    </li>
                </ol>
            </div>
            <div style="float: left; width: 33%;">
                <ol class="round">
                    <li>
                        <input type="submit" value="Search" class="submit-button" />
                    </li>
                    <li>
                       <input type="reset" value="Reset" class="reset-button" />   
                    </li>
                </ol>
            </div>
            
            <div style="float:left;margin-left:20px;">
                 @If Not (DirectCast(TempData("Message"), String) Is Nothing) Then
                    @<label class="message">@TempData("Message").ToString()
                    </label>                         
                End If
                @If Not (DirectCast(TempData("ErrorMessage"), String) Is Nothing) Then
                    @<label class="error">@TempData("ErrorMessage").ToString()
                    </label>                         
                End If
            </div>
        </div>