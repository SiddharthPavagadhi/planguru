﻿@ModelType  PagedList.IPagedList(Of onlineAnalytics.Customer)
@Code
    ViewData("Title") = "Index"
    Layout = "~/Views/Shared/AnalyticsMaster.vbhtml"
    Dim index As Integer = 1
End Code
<br />
<table class="tabSubscribeHeader">
    <tr>
        <td align="right" valign="top">
            <a href='@Url.Action("Subscribe", "Subscribe")' class="button_example">Create
                Subscription</a>
        </td>
    </tr>
</table>
<div class="center-panel">
    <div class="center-tablesection">
        <h1>
            View Subscriber List</h1>
        <div class="listtable">
            <table class="hovered">
                <thead>
                    <tr>
                        <th>
                            Customer Name
                        </th>
                        <th>
                            Email Id
                        </th>
                        <th>
                            Company
                        </th>
                        <th>
                            Plan
                        </th>
                        <th>
                            Quantity
                        </th>
                        <th>
                            Created On
                        </th>
                        <th>
                            Edit
                        </th>
                        <th>
                            Cancel
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @For Each item In Model
                        Dim currentItem = item
                        If (index Mod 2 <> 0) Then
                            @<tr>
                            <td class="left">
                                @Html.DisplayFor(Function(modelItem) currentItem.CustomerFullName)
                            </td>
                            <td class="left">
                                @Html.DisplayFor(Function(modelItem) currentItem.CustomerEmail)
                            </td>
                            <td class="left">
                                @Html.DisplayFor(Function(modelItem) currentItem.CustomerCompanyName)
                            </td>
                            <td>
                                PlanGuru Analytics
                            </td>
                            <td>
                                @Html.DisplayFor(Function(modelItem) currentItem.Quantity)
                            </td>
                            <td>
                                @Code
                            Dim createdon As String = String.Format("{0:MMM dd, yyyy}", currentItem.CreatedOn)
                                End Code
                                @Html.DisplayFor(Function(modelItem) createdon)
                            </td>
                            <td class="center">
                                @Html.ActionLink("Edit", "EditSubscription", New With {.AccountCode = currentItem.CustomerId})
                            </td>
                            <td class="center">
                                <a id='@currentItem.CustomerId' class="Delete" href="#" data='@Url.Action("CancelSubscription", "Subscribe", New With {.AccountCode = currentItem.CustomerId})' >
                                    Cancel</a>
                            </td>
                        </tr>           
                        Else
                            @<tr class="even">
                            <td class="left">
                                @Html.DisplayFor(Function(modelItem) currentItem.CustomerFullName)
                            </td>
                            <td class="left">
                                @Html.DisplayFor(Function(modelItem) currentItem.CustomerEmail)
                            </td>
                            <td class="left">
                                @Html.DisplayFor(Function(modelItem) currentItem.CustomerCompanyName)
                            </td>
                            <td>
                                PlanGuru Analytics
                            </td>
                            <td>
                                @Html.DisplayFor(Function(modelItem) currentItem.Quantity)
                            </td>
                            <td>
                                @Code
                            Dim createdon As String = String.Format("{0:MMM dd, yyyy}", currentItem.CreatedOn)
                                End Code
                                @Html.DisplayFor(Function(modelItem) createdon)
                            </td>
                            <td class="center">
                                @Html.ActionLink("Edit", "EditSubscription", New With {.AccountCode = currentItem.CustomerId})
                            </td>
                            <td class="center">
                                <a id='@currentItem.CustomerId' class="Delete" href="#" data='@Url.Action("CancelSubscription", "Subscribe", New With {.AccountCode = currentItem.CustomerId})' >
                                    Cancel</a>
                            </td>
                        </tr>       
                        End If
                        index += 1
                    Next
                </tbody>
            </table>
        </div>
        <div class="pagination-panel">
            <a href="#" class="prevous">Previous</a>
            <div class="pagination-list">
                <ul>
                    <li><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li><a href="#">5</a></li>
                </ul>
            </div>
            <a href="#" class=" next">Next</a>
        </div>
        @If Not (DirectCast(TempData("Message"), String) Is Nothing) Then
            @<label class="success">@TempData("Message").ToString()
            </label>                         
        End If
        @If Not (DirectCast(TempData("ErrorMessage"), String) Is Nothing) Then
            @<label class="error">@TempData("ErrorMessage").ToString()
            </label>                         
        End If
    </div>
</div>
<script type="text/javascript" language="javascript">

    $(".Delete").click(function (event) {
        event.preventDefault();
        var url = $(this).attr('data');

        $.msgBox({
            title: "Confirm",
            content: "Are you sure you want to cancel this Subscription?",
            type: "confirm",
            buttons: [{ value: "Yes" }, { value: "No"}],
            success: function (result) {
                if (result == "Yes") {
                    window.location = url;
                }
            }
        });

    }); 
</script>
