﻿@ModelType  IEnumerable(Of onlineAnalytics.Analysis)
@Code
    ViewData("Title") = "View Analyses"
    Layout = "~/Views/Shared/AnalyticsMaster.vbhtml"
    Dim index As Integer = 1
End Code
<br />
<div style="float: right">
    @Html.ActionLink("Add New Analysis", "Create")
</div>
<div class="center-panel">
    <div class="center-tablesection">
        <h1>
            View Analysis List</h1>
        <div class="listtable">
            <table class="hovered">
                <thead>
                    <tr>
                        <th class="left" style="width: 45%">
                            Analysis Name
                        </th>
                        <th class="left" style="width: 45%">
                            Company Name
                        </th>
                        <th class="center" style="width: 5%">
                            Edit
                        </th>
                        <th class="center" style="width: 5%">
                            Delete
                        </th>
                    </tr>
                </thead>
                @For Each item In Model
                    Dim currentItem = item                                            
                    If (index Mod 2 <> 0) Then
                        @<tr >
                        <td class="left">
                            @Html.DisplayFor(Function(modelItem) currentItem.AnalysisName)
                        </td>
                        <td class="left">
                            @Html.DisplayFor(Function(modelItem) currentItem.Company.CompanyName)
                        </td>
                        <td class="center">
                            @Html.ActionLink("Edit", "Edit", New With {.id = currentItem.AnalysisId})
                        </td>
                        <td class="center">
                            <a id=@currentItem.AnalysisId class="Delete" href="#" data='@Url.Action("Delete", "Analysis", New With {.id = currentItem.AnalysisId})' >
                                Delete</a>
                        </td>
                    </tr>
                    Else
                        @<tr class="even">
                        <td class="left">
                            @Html.DisplayFor(Function(modelItem) currentItem.AnalysisName)
                        </td>
                        <td class="left">
                            @Html.DisplayFor(Function(modelItem) currentItem.Company.CompanyName)
                        </td>
                        <td class="center">
                            @Html.ActionLink("Edit", "Edit", New With {.id = currentItem.AnalysisId})
                        </td>
                        <td class="center">
                            <a id=@currentItem.AnalysisId class="Delete" href="#" data='@Url.Action("Delete", "Analysis", New With {.id = currentItem.AnalysisId})' >
                                Delete</a>
                        </td>
                    </tr>
                    End If                                        
                    index += 1
                Next
            </table>
        </div>
        <div class="pagination-panel">
            <a href="#" class="prevous">Previous</a>
            <div class="pagination-list">
                <ul>
                    <li><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li><a href="#">5</a></li>
                </ul>
            </div>
            <a href="#" class=" next">Next</a>
        </div>
        @If Not (DirectCast(TempData("Message"), String) Is Nothing) Then
            @<label class="success">@TempData("Message").ToString()
            </label>                         
        End If
        @If Not (DirectCast(TempData("ErrorMessage"), String) Is Nothing) Then
            @<label class="error">@TempData("ErrorMessage").ToString()
            </label>                         
        End If
    </div>
</div>
<script type="text/javascript" language="javascript">

    $(".Delete").click(function (event) {
        event.preventDefault();
        var url = $(this).attr('data');

        $.msgBox({
            title: "Confirm",
            content: "Are you sure you want to delete this analysis?",
            type: "confirm",
            buttons: [{ value: "Yes" }, { value: "No"}],
            success: function (result) {
                if (result == "Yes") {
                    window.location = url;
                }
            }
        });

    }); 
</script>
