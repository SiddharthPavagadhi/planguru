﻿Imports System.ComponentModel.DataAnnotations
Imports System.Data.Entity
Imports System.Collections.Generic



'Master table schema, consist predefined user roles.
'UserRoles are define in UserRoles Enum.
<Table("UserRole")> _
Public Class UserRole
    <Key()> _
    <DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)> _
    Public Property UserRoleId As Integer
    <StringLength(50)> _
      Public Property RoleName As String
End Class
'It consists the permission of user's role specific functionality such as
'Subscription Management, 
'Add / Delete / Update User, 
'Add / Delete / Update Company, 
'Add / Delete / View /Print Analysis
<Table("UserRolePermission")> _
Public Class UserRolePermission
    <Key()> _
    <DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)> _
    Public Property UserRolePermissionId As Integer
    <StringLength(200)> _
    Public Property Description As String
End Class
<Table("CustomerRequestType")> _
Public Class CustomerRequestType
    <Key()> _
    <DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)> _
    Public Property RequestId As Integer
    <StringLength(200)> _
    Public Property Description As String
End Class

Public Enum UserRoles As Integer
    PGAAdmin = 1
    PGASupport = 2
    'Customer Administrative User (Super User) 
    SAU = 3
    'Company Administrative User
    CAU = 4
    'Company Regular User
    CRU = 5
End Enum


'Master table schema, consist predefined Country Codes.
<Table("CountryCode")> _
Public Class CountryCode
    <Key()> _
    <DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)> _
    Public Property CountryId() As Integer
    Public Property CountryName() As String
    Public Property Code() As String
End Class
'Master table schema, consist predefined States.
<Table("StateCode")> _
Public Class StateCode
    <Key()> _
    <DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)> _
    Public Property StateId As Integer
    Public Property StateName As String
    Public Property Code As String
    Public Property CountryId As Integer
End Class

<Table("Status")> _
Public Class Status
    <Key()> _
    <DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)> _
    Public Property StatusId As Integer
    Public Property StatusName As String
End Class

<Table("EmailInfo")> _
Public Class EmailInfo
    
    Public Property Id() As Integer
    Public Property EmailType() As String
    Public Property EmailSubject() As String
    Public Property EmailBody() As String
    Public Property CreatedDateTime() As DateTime
End Class
