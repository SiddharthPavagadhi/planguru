﻿Imports FarPoint.Mvc.Spread
Imports System.Data.Entity
Namespace onlineAnalytics
    Public Class REController
        Inherits System.Web.Mvc.Controller
        Dim Format As Integer = 1
        Dim Period As Integer = 10
        Dim intFirstNumCol As Integer = 4

        '
        ' GET: /RE
        <HttpGet()> _
        Function Index(<MvcSpread("spdAnalytics")> spdAnalytics As FpSpread, Values As FormValues) As ActionResult
            Dim intCtr As Integer

            Dim Periods As List(Of SelectListItem) = New List(Of SelectListItem)
            For intCtr = 0 To 11
                Periods.Add(New SelectListItem With {.Text = CommonProcedures.GetMonth(intCtr), .Value = intCtr + 1, .Selected = False})
            Next
            ViewData("Periods") = New SelectList(Periods, "value", "text", "May")

            Dim Formats As List(Of SelectListItem) = New List(Of SelectListItem)
            For intCtr = 0 To 8
                Formats.Add(New SelectListItem With {.Text = CommonProcedures.GetFormat(intCtr, "RE"), .Value = intCtr + 1, .Selected = False})
            Next
            ViewData("Formats") = New SelectList(Formats, "value", "text")

            Dim FilterOn As List(Of SelectListItem) = New List(Of SelectListItem) From {
                New SelectListItem With {.Text = "Current period amount", .Value = "Current", .Selected = False},
                New SelectListItem With {.Text = "Variance amount", .Value = "Variance", .Selected = False},
                New SelectListItem With {.Text = "Percent variance", .Value = "PercentVar", .Selected = True}
            }
            ViewData("FilterOn") = New SelectList(FilterOn, "value", "text", "selected")

            Dim ChartType As List(Of SelectListItem) = New List(Of SelectListItem)
            For intCtr = 0 To 5
                ChartType.Add(New SelectListItem With {.Text = CommonProcedures.GetChartDesc(intCtr), .Value = intCtr + 1, .Selected = False})
            Next
            ViewData("ChartType") = New SelectList(ChartType, "value", "text", "selected")
            Dim Dept As List(Of SelectListItem) = New List(Of SelectListItem) From {
              New SelectListItem With {.Text = "Consolidated", .Value = "Consolidated", .Selected = False},
              New SelectListItem With {.Text = "Dept 1", .Value = "Dept 1", .Selected = False},
              New SelectListItem With {.Text = "Dept 2", .Value = "Dept 2", .Selected = True}
            }
            ViewData("Dept") = New SelectList(Dept, "value", "text", "selected")
            ViewData("blnHighVar") = False
            ViewData("blnShowPercent") = 0
            ViewData("intPeriod") = Period + 1
            ViewData("intFormat") = Format + 1
            ViewData("intChartType") = 1
            ViewData("blnShowTrend") = False
            ViewData("blnUseFilter") = False
            ViewData("intDept") = 1
            ViewData("Postback") = False
            Return View()
        End Function
        'Post 
        <HttpPost()> _
        Function Index(<MvcSpread("spdAnalytics")> spdAnalytics As FpSpread, <MvcSpread("spdChart")> spdChart As FpSpread, Values As FormValues) As ActionResult
            Dim intCtr As Integer
 
            Try
                spdChart.Sheets(0).Charts.Remove(spdChart.Sheets(0).Charts(0))
            Catch ex As Exception

            End Try

            If Values.intChartType = 1 Then
                spdAnalytics.Sheets(0).RowCount = 0
                SetSpreadProperties(spdAnalytics, Values.intFormat - 1, Values.intPeriod - 1, Values.blnShowasPer)
            Else
                Charts.BuildChart(spdChart, spdAnalytics, Values.intChartType, Values.intFormat - 1, intFirstNumCol, Values.blnShowasPer, Values.blnShowTrend)
                spdAnalytics.Height = 400
                ViewData("blnUpdateChart") = True
            End If

            Dim Periods As List(Of SelectListItem) = New List(Of SelectListItem)
            For intCtr = 0 To 11
                Periods.Add(New SelectListItem With {.Text = CommonProcedures.GetMonth(intCtr), .Value = intCtr + 1, .Selected = False})
            Next
            ViewData("Periods") = New SelectList(Periods, "value", "text", "May")

            Dim Formats As List(Of SelectListItem) = New List(Of SelectListItem)
            For intCtr = 0 To 8
                Formats.Add(New SelectListItem With {.Text = CommonProcedures.GetFormat(intCtr, "RE"), .Value = intCtr + 1, .Selected = False})
            Next
            ViewData("Formats") = New SelectList(Formats, "value", "text")

            Dim FilterOn As List(Of SelectListItem) = New List(Of SelectListItem) From {
                New SelectListItem With {.Text = "Current period amount", .Value = "Current", .Selected = False},
                New SelectListItem With {.Text = "Variance amount", .Value = "Variance", .Selected = False},
                New SelectListItem With {.Text = "Percent variance", .Value = "PercentVar", .Selected = True}
            }
            ViewData("FilterOn") = New SelectList(FilterOn, "value", "text", "selected")

            Dim ChartType As List(Of SelectListItem) = New List(Of SelectListItem)
            For intCtr = 0 To 5
                ChartType.Add(New SelectListItem With {.Text = CommonProcedures.GetChartDesc(intCtr), .Value = intCtr + 1, .Selected = False})
            Next
            ViewData("ChartType") = New SelectList(ChartType, "value", "text", "selected")
            Dim Dept As List(Of SelectListItem) = New List(Of SelectListItem) From {
             New SelectListItem With {.Text = "Consolidated", .Value = "Consolidated", .Selected = False},
             New SelectListItem With {.Text = "Dept 1", .Value = "Dept 1", .Selected = False},
             New SelectListItem With {.Text = "Dept 2", .Value = "Dept 2", .Selected = True}
           }
            ViewData("Dept") = New SelectList(Dept, "value", "text", "selected")
            ViewData("blnHighVar") = False
            ViewData("blnShowPercent") = 0
            ViewData("Period") = Values.intPeriod - 1
            ViewData("intDept") = 1
            ViewData("postback") = True
            Return View()
        End Function

        <MvcSpreadEvent("Load", "spdAnalytics", DirectCast(Nothing, String()))> _
        Private Sub FpSpread_Load(sender As Object, e As EventArgs)
            Dim spread As FpSpread = DirectCast(sender, FpSpread)
            If Not spread.Page.IsPostBack Then

                SetSpreadProperties(spread, Format, Period, False)
                'Dim intSelectedRows As New ArrayList
                'intSelectedRows.Add(4)
                'BuildBarChart(intSelectedRows, spread)
                '   Dim intSelectedRows As New ArrayList
                '  intSelectedRows.Add(6)
                ' BuildBarChart(intSelectedRows, spread)
            End If

        End Sub
        <MvcSpreadEvent("PreRender", "spdAnalytics", DirectCast(Nothing, String()))> _
        Private Sub FpSpread_PreRender(sender As Object, e As EventArgs)
            Dim spread As FpSpread = DirectCast(sender, FpSpread)

        End Sub
        Public Sub SetSpreadProperties(spdAnalytics As FpSpread, intFormat As Integer, intPeriod As Integer, ByRef blnShowasPercent As Boolean)
            Dim chkbox As New FarPoint.Web.Spread.CheckBoxCellType
            Dim txtcell As New FarPoint.Web.Spread.GeneralCellType
            Dim intColCount As Integer = 0
            Dim intNumberCols As Integer = 0
            Dim intPercentCols As Integer = 0
            Dim intCtr As Integer
            Dim intColCtr As Integer

            spdAnalytics.BorderColor = Drawing.Color.White
            spdAnalytics.Sheets(0).GridLineColor = Drawing.Color.LightGray
            spdAnalytics.Sheets(0).DefaultStyle.Font.Size = FontSize.Large
            spdAnalytics.Sheets(0).DefaultStyle.ForeColor = Drawing.Color.Black
            spdAnalytics.Sheets(0).RowHeader.Visible = False
            spdAnalytics.CommandBar.Visible = False
            spdAnalytics.Sheets(0).SelectionBackColorStyle = FarPoint.Web.Spread.SelectionBackColorStyles.None
            spdAnalytics.VerticalScrollBarPolicy = FarPoint.Web.Spread.ScrollBarPolicy.Always
            'set column count
            Select Case intFormat
                Case 0 To 4
                    intColCount = 8
                    intNumberCols = 3
                    intPercentCols = 1
                Case 5 'current year trend
                    intNumberCols = intPeriod + 2
                    intPercentCols = intPeriod + 2
                    intColCount = 4 + intNumberCols
                Case 6
                    intNumberCols = 13
                    intPercentCols = 13
                    intColCount = 4 + 13
                Case 7, 8
                    intNumberCols = 6
                    intPercentCols = 6
                    intColCount = 4 + 6
            End Select
            spdAnalytics.Sheets(0).ColumnCount = intColCount
            spdAnalytics.Sheets(0).RowCount = 1

            'set row properties
            spdAnalytics.Sheets(0).Rows(-1).Height = 16
            spdAnalytics.Sheets(0).Rows(-1).VerticalAlign = VerticalAlign.Bottom
            'lock cells
            spdAnalytics.Sheets(0).Columns(0).Locked = True
            spdAnalytics.Sheets(0).Columns(2, intColCount - 1).Locked = True

            'set cell types for number cols
            Dim g As New FarPoint.Web.Spread.GeneralCellType
            Dim nfi As New System.Globalization.NumberFormatInfo
            nfi.NumberDecimalDigits = 0
            g.NumberFormat = nfi
            Dim pfi As New FarPoint.Web.Spread.PercentCellType
            pfi.DecimalDigits = 2
            If blnShowasPercent = False Then
                spdAnalytics.Sheets(0).Columns(intFirstNumCol, intFirstNumCol + intNumberCols - 1).CellType = g
                Select Case intFormat
                    Case 0 To 4
                        spdAnalytics.Sheets(0).Columns(intFirstNumCol + intNumberCols, intColCount - 1).CellType = pfi
                End Select
            Else
                spdAnalytics.Sheets(0).Columns(intFirstNumCol, intFirstNumCol + intNumberCols - 1).CellType = pfi
            End If
            spdAnalytics.Sheets(0).Columns(intFirstNumCol, intColCount - 1).HorizontalAlign = HorizontalAlign.Right

            'load rows
            Select Case intFormat
                Case 0 To 4
                    LoadSpread0to4(spdAnalytics, intFormat, intPeriod, intNumberCols, intFirstNumCol, intColCount, blnShowasPercent)
                    spdAnalytics.Sheets(0).Columns(intFirstNumCol, intFirstNumCol + 3).Visible = True
                Case 5, 6
                    LoadSpread5to6(spdAnalytics, intFormat, intPeriod, intNumberCols, intFirstNumCol, blnShowasPercent, intColCount)
                    spdAnalytics.Sheets(0).Columns(intFirstNumCol, intFirstNumCol + intNumberCols - 1).Visible = True
                Case 7, 8
                    LoadSpread7to8(spdAnalytics, intFormat, intPeriod, intNumberCols, intFirstNumCol, blnShowasPercent, intColCount)
                    Dim blnLoop As Boolean = True
                    intCtr = 0
                    Do While blnLoop = True
                        intCtr += 1
                        If spdAnalytics.Sheets(0).GetValue(intCtr, 2) = "T" Then
                            For intColCtr = intFirstNumCol To intFirstNumCol + 5
                                If spdAnalytics.Sheets(0).GetValue(intCtr, intColCtr) = 0 Then
                                    spdAnalytics.Sheets(0).Columns(intColCtr).Visible = False
                                    intColCount -= 1
                                    intNumberCols -= 1
                                Else
                                    blnLoop = False
                                    Exit For
                                End If
                            Next
                        End If
                    Loop
            End Select
            'set col widths
            spdAnalytics.Sheets(0).Columns(0, 1).Width = 22
            spdAnalytics.Sheets(0).Columns(2).Width = 0
            If intNumberCols > 3 Then
                spdAnalytics.Sheets(0).Columns(3).Width = 150 + 22
                spdAnalytics.Sheets(0).Columns(intFirstNumCol, intColCount - 1).Width = 65
            Else
                spdAnalytics.Sheets(0).Columns(3).Width = 250 + 22
                spdAnalytics.Sheets(0).Columns(intFirstNumCol, intColCount - 1).Width = 85
                Select Case intFormat
                    Case 7, 8
                        spdAnalytics.Sheets(0).Columns(intFirstNumCol, intColCount + 2).Width = 85
                    Case Else
                        spdAnalytics.Sheets(0).Columns(intFirstNumCol, intColCount - 1).Width = 85
                End Select
            End If
            If intNumberCols <= 3 Then
                Select Case intFormat
                    Case 7, 8
                        spdAnalytics.Width = 44 + 250 + (85 * intNumberCols) + 30
                    Case Else
                        spdAnalytics.Width = 44 + 250 + 340 + 20
                End Select
            Else
                Dim intWidth As Integer
                intWidth = 44 + 150 + (65 * intNumberCols) + 20
                If intWidth > 700 Then
                    '     intWidth = 700
                End If
                spdAnalytics.Width = intWidth
            End If
            'load column headers
            SetColumnHeader(spdAnalytics, intFormat, intPeriod, intColCount, intFirstNumCol)
            'hide last row
            spdAnalytics.Sheets(0).Rows(spdAnalytics.Sheets(0).RowCount - 1).Visible = False
            'hide checkbox col
            spdAnalytics.Sheets(0).Columns(1).Width = 0
            spdAnalytics.Sheets(0).ActiveColumn = 2
            spdAnalytics.Sheets(0).ActiveRow = 1
            spdAnalytics.Height = 600
        End Sub


        Public Sub LoadSpread0to4(spdAnalytics As FpSpread, intFormat As Integer, intPeriod As Integer, intNumberCols As Integer, intFirstNumCol As Integer, intColCount As Integer, ByRef blnShowasPercent As Boolean)
            Using context As New DataAccess
                context.AcctTypes.Load()
                context.Accounts.Load()
                context.Balances.Load()
                Dim intRowCtr As Integer = -1
                Dim intColCtr As Integer
                Dim strYearType(5) As String
                Dim strPeriods(2) As String
                Dim decAccumTotals(1) As Decimal
                Dim decPeriodAmts(11) As Decimal
                Dim decSaveAmts(4) As Decimal 
                Dim intSpanRows(1) As Integer
                Dim chkbox As New FarPoint.Web.Spread.CheckBoxCellType
                Dim blnTotalFlag As Boolean
                Dim decIncAccumTotals(1) As Decimal

                chkbox.OnClientClick = CommonProcedures.CheckboxClicked 

                BuildColsArray(intFormat, intPeriod, strYearType, strPeriods)
                'If amounts are to be show as % need to accumulate total
                If blnShowasPercent = True Then
                    Dim selectedAccumAccounts = (From a In context.Accounts.Local
                                            Where a.AcctType.TypeDesc = "Revenue" _
                                            Order By a.SortSequence _
                                            Select a)
                    For Each account As Account In selectedAccumAccounts
                        'save amounts                         
                        If account.AcctDescriptor <> "D" Then
                            For Each balance As Balance In account.Balances
                                If balance.YearType = strYearType(0) Then
                                    SaveBalances(balance, decPeriodAmts)
                                    CommonProcedures.AccumRunningTotals(intFormat, decAccumTotals, intPeriod, decPeriodAmts, 0)
                                End If
                                If balance.YearType = strYearType(1) Then
                                    SaveBalances(balance, decPeriodAmts)
                                    CommonProcedures.AccumRunningTotals(intFormat, decAccumTotals, intPeriod, decPeriodAmts, 1)
                                End If
                            Next
                        End If
                    Next
                End If

                Dim selectedAccounts = (From a In context.Accounts.Local
                                            Where a.AcctType.TypeDesc = "Revenue" Or a.AcctType.TypeDesc = "Expense" _
                                            Order By a.SortSequence _
                                            Select a)
                For Each account As Account In selectedAccounts
                    CommonProcedures.AddRow(spdAnalytics, intRowCtr, account.Description, account.AcctDescriptor, account.AcctType.TypeDesc)
                    Select Case account.AcctDescriptor
                        Case "H"
                            CommonProcedures.FormatHeaderRow(spdAnalytics, intRowCtr, intSpanRows, blnTotalFlag)
                        Case "T"
                            CommonProcedures.FormatTotalRow(spdAnalytics, intRowCtr, intSpanRows, blnTotalFlag, intFirstNumCol, intNumberCols, account.AcctType.ClassDesc)
                        Case "D"
                            spdAnalytics.Sheets(0).Cells(intRowCtr, 0).Border.BorderColorBottom = Drawing.Color.White
                    End Select
                    'save amounts                         
                    If account.AcctDescriptor <> "H" Then
                        spdAnalytics.Sheets(0).Cells(intRowCtr, 1).CellType = chkbox
                        For Each balance As Balance In account.Balances
                            If balance.YearType = strYearType(0) Then
                                SaveBalances(balance, decPeriodAmts)
                                decSaveAmts(0) = 0
                                Select Case intFormat
                                    Case 1, 4
                                        For intColCtr = 0 To intPeriod
                                            decSaveAmts(0) += decPeriodAmts(intColCtr)
                                        Next
                                    Case 0, 3
                                        decSaveAmts(0) = decPeriodAmts(intPeriod)
                                    Case 2
                                        If intPeriod = 0 Then
                                            decSaveAmts(0) = decPeriodAmts(11)
                                        End If
                                End Select
                            End If
                            If balance.YearType = strYearType(1) Then
                                SaveBalances(balance, decPeriodAmts)
                                decSaveAmts(1) = 0
                                Select Case intFormat
                                    Case 1, 4
                                        For intColCtr = 0 To intPeriod
                                            decSaveAmts(1) += decPeriodAmts(intColCtr)
                                        Next
                                    Case 0, 3
                                        decSaveAmts(1) = decPeriodAmts(intPeriod)
                                    Case 2
                                        If intPeriod = 0 Then
                                            decSaveAmts(1) = decPeriodAmts(intPeriod)
                                        Else
                                            decSaveAmts(0) = decPeriodAmts(intPeriod - 1)
                                            decSaveAmts(1) = decPeriodAmts(intPeriod)
                                        End If
                                End Select
                            End If
                        Next
                        'store values
                        If blnShowasPercent = False Then
                            spdAnalytics.Sheets(0).SetValue(intRowCtr, intFirstNumCol, decSaveAmts(0))
                            spdAnalytics.Sheets(0).SetValue(intRowCtr, intFirstNumCol + 1, decSaveAmts(1))
                        Else
                            If decAccumTotals(0) <> 0 Then
                                spdAnalytics.Sheets(0).SetValue(intRowCtr, intFirstNumCol, decSaveAmts(0) / decAccumTotals(0))
                                decSaveAmts(0) = decSaveAmts(0) / decAccumTotals(0)
                            Else
                                spdAnalytics.Sheets(0).SetValue(intRowCtr, intFirstNumCol, 0)
                                decSaveAmts(0) = 0
                            End If
                            If decAccumTotals(1) <> 0 Then
                                spdAnalytics.Sheets(0).SetValue(intRowCtr, intFirstNumCol + 1, decSaveAmts(1) / decAccumTotals(1))
                                decSaveAmts(1) = decSaveAmts(1) / decAccumTotals(1)
                            Else
                                spdAnalytics.Sheets(0).SetValue(intRowCtr, intFirstNumCol + 1, 0)
                                decSaveAmts(1) = 0
                            End If

                        End If
                        'compute variances
                        CommonProcedures.ComputeVariances(decSaveAmts, account.AcctType.TypeDesc, decAccumTotals, blnShowasPercent)
                        spdAnalytics.Sheets(0).SetValue(intRowCtr, intFirstNumCol + 2, decSaveAmts(2))
                        spdAnalytics.Sheets(0).SetValue(intRowCtr, intFirstNumCol + 3, decSaveAmts(3))
                    End If
                    If account.AcctDescriptor = "T" Then
                        AccumNI(spdAnalytics, account.AcctType.ClassDesc, intFormat, 2, decSaveAmts, intColCount, decIncAccumTotals, intRowCtr, intPeriod)
                    End If
                Next
                CommonProcedures.AddTotalRow(spdAnalytics, intFormat, intNumberCols, intFirstNumCol, intColCount, decIncAccumTotals, intRowCtr, "NI", "Net Income(Loss)", intPeriod)
            End Using


        End Sub
        Public Sub LoadSpread5to6(spdAnalytics As FpSpread, intFormat As Integer, intPeriod As Integer, intNumberCols As Integer, intFirstNumCol As Integer, ByRef blnShowasPercent As Boolean, ByVal intColCount As Integer)
            Using context As New DataAccess
                context.AcctTypes.Load()
                context.Accounts.Load()
                context.Balances.Load()
                Dim intRowCtr As Integer = -1
                Dim intColCtr As Integer
                Dim strYearType(5) As String
                Dim strPeriods(2) As String
                Dim decAccumTotals(12) As Decimal
                Dim decPeriodAmts(11) As Decimal
                Dim decSaveAmts(12) As Decimal
                Dim intSpanRows(1) As Integer 
                Dim blnTotalFlag As Boolean 
                Dim decIncAccumTotals(intNumberCols) As Decimal
                Dim chkbox As New FarPoint.Web.Spread.CheckBoxCellType

                chkbox.OnClientClick = CommonProcedures.CheckboxClicked

                BuildColsArray(intFormat, intPeriod, strYearType, strPeriods)

                If blnShowasPercent = True Then
                    Dim selectedAccumAccounts = (From a In context.Accounts.Local
                                            Where a.AcctType.TypeDesc = "Revenue" _
                                            Order By a.SortSequence _
                                            Select a)
                    For Each account As Account In selectedAccumAccounts
                        'save amounts total revenue amounts for computing %                      
                        If account.AcctDescriptor <> "D" Then
                            For Each balance As Balance In account.Balances
                                If balance.YearType = strYearType(0) Then
                                    SaveBalances(balance, decPeriodAmts)
                                    Select Case intFormat
                                        Case 5
                                            For intColCtr = 0 To intPeriod
                                                decAccumTotals(intColCtr) += decPeriodAmts(intColCtr)
                                                decAccumTotals(intPeriod + 1) += decPeriodAmts(intColCtr)
                                            Next
                                        Case 6
                                            If intPeriod < 11 Then
                                                For intColCtr = intPeriod + 1 To 11 
                                                    decAccumTotals(intColCtr) = decPeriodAmts(intColCtr)
                                                    decAccumTotals(12) += decPeriodAmts(intColCtr)
                                                Next
                                            End If
                                    End Select
                                End If
                                If balance.YearType = strYearType(1) Then
                                    SaveBalances(balance, decPeriodAmts)
                                    For intColCtr = 0 To intPeriod
                                        decAccumTotals(intColCtr) = decPeriodAmts(intColCtr)
                                        decAccumTotals(12) += decPeriodAmts(intColCtr)
                                    Next
                                End If
                            Next
                        End If
                    Next
                End If

                Dim selectedAccounts = (From a In context.Accounts.Local
                                            Where a.AcctType.TypeDesc = "Revenue" Or a.AcctType.TypeDesc = "Expense" _
                                            Order By a.SortSequence _
                                            Select a)
                For Each account As Account In selectedAccounts
                     CommonProcedures.AddRow(spdAnalytics, intRowCtr, account.Description, account.AcctDescriptor, account.AcctType.TypeDesc)
                    Select Case account.AcctDescriptor
                        Case "H"
                            CommonProcedures.FormatHeaderRow(spdAnalytics, intRowCtr, intSpanRows, blnTotalFlag)
                        Case "T"
                            CommonProcedures.FormatTotalRow(spdAnalytics, intRowCtr, intSpanRows, blnTotalFlag, intFirstNumCol, intNumberCols, account.AcctType.ClassDesc)
                        Case "D"
                            spdAnalytics.Sheets(0).Cells(intRowCtr, 0).Border.BorderColorBottom = Drawing.Color.White
                    End Select
                    'save amounts
                    decSaveAmts(12) = 0
                    If account.AcctDescriptor <> "H" Then
                        spdAnalytics.Sheets(0).Cells(intRowCtr, 1).CellType = chkbox
                        For Each balance As Balance In account.Balances
                            If balance.YearType = strYearType(0) Then
                                SaveBalances(balance, decPeriodAmts)
                                Select Case intFormat
                                    Case 5
                                        decSaveAmts(intPeriod + 1) = 0
                                        If blnShowasPercent = False Then
                                            For intColCtr = 0 To intPeriod
                                                spdAnalytics.Sheets(0).SetValue(intRowCtr, intFirstNumCol + intColCtr, decPeriodAmts(intColCtr))
                                                decSaveAmts(intColCtr) = decPeriodAmts(intColCtr)
                                                decSaveAmts(intPeriod + 1) += decPeriodAmts(intColCtr)
                                            Next
                                            spdAnalytics.Sheets(0).SetValue(intRowCtr, intFirstNumCol + intPeriod + 1, decSaveAmts(intPeriod + 1))
                                        Else
                                            For intColCtr = 0 To intPeriod
                                                If decAccumTotals(intColCtr) <> 0 Then
                                                    spdAnalytics.Sheets(0).SetValue(intRowCtr, intFirstNumCol + intColCtr, decPeriodAmts(intColCtr) / decAccumTotals(intColCtr))
                                                    decSaveAmts(intColCtr) = decPeriodAmts(intColCtr) / decAccumTotals(intColCtr)
                                                Else
                                                    spdAnalytics.Sheets(0).SetValue(intRowCtr, intFirstNumCol + intColCtr, 0)
                                                    decSaveAmts(intColCtr) = 0
                                                End If

                                                decSaveAmts(intPeriod + 1) += decPeriodAmts(intColCtr)
                                            Next
                                            If decAccumTotals(intPeriod + 1) <> 0 Then
                                                decSaveAmts(intPeriod + 1) = decSaveAmts(intPeriod + 1) / decAccumTotals(intPeriod + 1)
                                            Else
                                                decSaveAmts(intPeriod + 1) = 0
                                            End If
                                            If decAccumTotals(intPeriod + 1) <> 0 Then
                                                spdAnalytics.Sheets(0).SetValue(intRowCtr, intFirstNumCol + intPeriod + 1, decSaveAmts(intPeriod + 1))
                                            Else
                                                spdAnalytics.Sheets(0).SetValue(intRowCtr, intFirstNumCol + intPeriod + 1, 0)
                                            End If
                                        End If
                                    Case 6
                                            If intPeriod < 11 Then
                                                For intColCtr = intPeriod + 1 To 11
                                                    If blnShowasPercent = False Then
                                                        spdAnalytics.Sheets(0).SetValue(intRowCtr, intFirstNumCol + intColCtr - (intPeriod + 1), decPeriodAmts(intColCtr))
                                                        decSaveAmts(intColCtr) = decPeriodAmts(intColCtr)
                                                        decSaveAmts(12) += decPeriodAmts(intColCtr)
                                                    Else
                                                    If decAccumTotals(intColCtr) <> 0 Then
                                                        spdAnalytics.Sheets(0).SetValue(intRowCtr, intFirstNumCol + intColCtr - (intPeriod + 1), decPeriodAmts(intColCtr) / decAccumTotals(intColCtr))
                                                        decSaveAmts(intColCtr) = decPeriodAmts(intColCtr) / decAccumTotals(intColCtr)
                                                    Else
                                                        spdAnalytics.Sheets(0).SetValue(intRowCtr, intFirstNumCol + intColCtr - (intPeriod + 1), 0)
                                                        decSaveAmts(intColCtr) = 0
                                                    End If
                                                        decSaveAmts(12) += decPeriodAmts(intColCtr)
                                                    End If
                                                Next
                                            End If
                                End Select
                            End If
                            If balance.YearType = strYearType(1) Then
                                Array.Clear(decPeriodAmts, 0, 11)
                                SaveBalances(balance, decPeriodAmts)
                                For intColCtr = 0 To intPeriod
                                    If blnShowasPercent = False Then
                                        spdAnalytics.Sheets(0).SetValue(intRowCtr, intFirstNumCol + intColCtr + (11 - intPeriod), decPeriodAmts(intColCtr))
                                        decSaveAmts(intColCtr) = decPeriodAmts(intColCtr)
                                        decSaveAmts(12) += decPeriodAmts(intColCtr)
                                    Else
                                        If decAccumTotals(intColCtr) <> 0 Then
                                            spdAnalytics.Sheets(0).SetValue(intRowCtr, intFirstNumCol + intColCtr + (11 - intPeriod), decPeriodAmts(intColCtr) / decAccumTotals(intColCtr))
                                            decSaveAmts(intColCtr) = decPeriodAmts(intColCtr) / decAccumTotals(intColCtr)
                                        Else
                                            spdAnalytics.Sheets(0).SetValue(intRowCtr, intFirstNumCol + intColCtr + (11 - intPeriod), 0)
                                            decSaveAmts(intColCtr) = 0
                                        End If
                                        decSaveAmts(12) += decPeriodAmts(intColCtr)
                                    End If
                                Next
                            End If
                        Next
                        If intFormat = 6 Then
                            If blnShowasPercent = False Then
                                spdAnalytics.Sheets(0).SetValue(intRowCtr, intFirstNumCol + 12, decSaveAmts(12))
                            Else
                                If decAccumTotals(12) <> 0 Then
                                    spdAnalytics.Sheets(0).SetValue(intRowCtr, intFirstNumCol + 12, decSaveAmts(12) / decAccumTotals(12))
                                    decSaveAmts(12) = decSaveAmts(12) / decAccumTotals(12)
                                Else
                                    spdAnalytics.Sheets(0).SetValue(intRowCtr, intFirstNumCol + 12, 0)
                                    decSaveAmts(12) = 0
                                End If

                            End If
                        End If
                        
                    End If
                    If account.AcctDescriptor = "T" Then
                        AccumNI(spdAnalytics, account.AcctType.ClassDesc, intFormat, intNumberCols, decSaveAmts, intColCount, decIncAccumTotals, intRowCtr, intPeriod)
                    End If
                Next
                CommonProcedures.AddTotalRow(spdAnalytics, intFormat, intNumberCols, intFirstNumCol, intColCount, decIncAccumTotals, intRowCtr, "NI", "Net Income(Loss)", intPeriod)
            End Using

        End Sub

        Public Sub LoadSpread7to8(spdAnalytics As FpSpread, intFormat As Integer, intPeriod As Integer, intNumberCols As Integer, intFirstNumCol As Integer, ByRef blnShowasPercent As Boolean, ByVal intColCount As Integer)
            Using context As New DataAccess
                context.AcctTypes.Load()
                context.Accounts.Load()
                context.Balances.Load()
                Dim intRowCtr As Integer = -1
                Dim intColCtr As Integer
                Dim strYearType(5) As String
                Dim strPeriods(2) As String
                Dim decPeriodAmts(11) As Decimal
                Dim decAccumTotals(5) As Decimal
                Dim decSaveAmts(12) As Decimal
                Dim intSpanRows(1) As Integer
                Dim blnTotalFlag As Boolean
                Dim decIncAccumTotals(intNumberCols) As Decimal
                Dim chkbox As New FarPoint.Web.Spread.CheckBoxCellType

                chkbox.OnClientClick = CommonProcedures.CheckboxClicked

                BuildColsArray(intFormat, intPeriod, strYearType, strPeriods)

                If blnShowasPercent = True Then
                    Dim selectedAccumAccounts = (From a In context.Accounts.Local
                                            Where a.AcctType.TypeDesc = "Revenue" _
                                            Order By a.SortSequence _
                                            Select a)
                    For Each account As Account In selectedAccumAccounts
                        'save amounts total revenue amounts for computing % of revenue                      
                        If account.AcctDescriptor = "T" Then
                            For Each balance As Balance In account.Balances
                                Select Case balance.YearType
                                    Case strYearType(0)
                                        SaveBalances(balance, decPeriodAmts)
                                        CommonProcedures.AccumRunningTotals(intFormat, decAccumTotals, intPeriod, decPeriodAmts, 0)
                                    Case strYearType(1)
                                        SaveBalances(balance, decPeriodAmts)
                                        CommonProcedures.AccumRunningTotals(intFormat, decAccumTotals, intPeriod, decPeriodAmts, 1)
                                    Case strYearType(2)
                                        SaveBalances(balance, decPeriodAmts)
                                        CommonProcedures.AccumRunningTotals(intFormat, decAccumTotals, intPeriod, decPeriodAmts, 2)
                                    Case strYearType(3)
                                        SaveBalances(balance, decPeriodAmts)
                                        CommonProcedures.AccumRunningTotals(intFormat, decAccumTotals, intPeriod, decPeriodAmts, 3)
                                    Case strYearType(4)
                                        SaveBalances(balance, decPeriodAmts)
                                        CommonProcedures.AccumRunningTotals(intFormat, decAccumTotals, intPeriod, decPeriodAmts, 4)
                                    Case strYearType(5)
                                        SaveBalances(balance, decPeriodAmts)
                                        CommonProcedures.AccumRunningTotals(intFormat, decAccumTotals, intPeriod, decPeriodAmts, 5)
                                End Select
                            Next
                        End If
                    Next
                End If
                Dim selectedAccounts = (From a In context.Accounts.Local
                                            Where a.AcctType.TypeDesc = "Revenue" Or a.AcctType.TypeDesc = "Expense" _
                                            Order By a.SortSequence _
                                            Select a)
                For Each account As Account In selectedAccounts
                    CommonProcedures.AddRow(spdAnalytics, intRowCtr, account.Description, account.AcctDescriptor, account.AcctType.TypeDesc)
                    Select Case account.AcctDescriptor
                        Case "H"
                            CommonProcedures.FormatHeaderRow(spdAnalytics, intRowCtr, intSpanRows, blnTotalFlag)
                        Case "T"
                            CommonProcedures.FormatTotalRow(spdAnalytics, intRowCtr, intSpanRows, blnTotalFlag, intFirstNumCol, intNumberCols, account.AcctType.ClassDesc)
                        Case "D"
                            spdAnalytics.Sheets(0).Cells(intRowCtr, 0).Border.BorderColorBottom = Drawing.Color.White
                    End Select
                    'save amounts
                    decSaveAmts(12) = 0
                    If account.AcctDescriptor <> "H" Then
                        spdAnalytics.Sheets(0).Cells(intRowCtr, 1).CellType = chkbox
                        For Each balance As Balance In account.Balances
                            If balance.YearType = strYearType(0) Then
                                SaveBalances(balance, decPeriodAmts)
                                Select Case intFormat
                                    Case 7
                                        If blnShowasPercent = False Then
                                            spdAnalytics.Sheets(0).SetValue(intRowCtr, intFirstNumCol, decPeriodAmts(intPeriod))
                                            decSaveAmts(0) = decPeriodAmts(intPeriod)
                                        Else
                                            If decAccumTotals(0) <> 0 Then
                                                spdAnalytics.Sheets(0).SetValue(intRowCtr, intFirstNumCol, decPeriodAmts(intPeriod) / decAccumTotals(0))
                                                decSaveAmts(0) = decPeriodAmts(intPeriod) / decAccumTotals(0)
                                            Else
                                                spdAnalytics.Sheets(0).SetValue(intRowCtr, intFirstNumCol, 0)
                                                decSaveAmts(0) = 0
                                            End If
                                        End If
                                    Case 8
                                        decSaveAmts(0) = 0
                                        For intColCtr = 0 To intPeriod
                                            decSaveAmts(0) += decPeriodAmts(intColCtr)
                                        Next
                                        If blnShowasPercent = False Then
                                            spdAnalytics.Sheets(0).SetValue(intRowCtr, intFirstNumCol, decSaveAmts(0))
                                        Else
                                            If decAccumTotals(0) <> 0 Then
                                                spdAnalytics.Sheets(0).SetValue(intRowCtr, intFirstNumCol, decSaveAmts(0) / decAccumTotals(0))
                                                decSaveAmts(0) = decSaveAmts(0) / decAccumTotals(0)
                                            Else
                                                spdAnalytics.Sheets(0).SetValue(intRowCtr, intFirstNumCol, 0)
                                                decSaveAmts(0) = 0
                                            End If
                                        End If
                                End Select
                            End If
                            If balance.YearType = strYearType(1) Then
                                SaveBalances(balance, decPeriodAmts)
                                Select Case intFormat
                                    Case 7
                                        If blnShowasPercent = False Then
                                            spdAnalytics.Sheets(0).SetValue(intRowCtr, intFirstNumCol + 1, decPeriodAmts(intPeriod))
                                            decSaveAmts(1) = decPeriodAmts(intPeriod)
                                        Else
                                            If decAccumTotals(1) <> 0 Then
                                                spdAnalytics.Sheets(0).SetValue(intRowCtr, intFirstNumCol + 1, decPeriodAmts(intPeriod) / decAccumTotals(1))
                                                decSaveAmts(1) = decPeriodAmts(intPeriod) / decAccumTotals(1)
                                            Else
                                                spdAnalytics.Sheets(0).SetValue(intRowCtr, intFirstNumCol + 1, 0)
                                                decSaveAmts(1) = 0
                                            End If
                                        End If
                                    Case 8
                                        decSaveAmts(1) = 0
                                        For intColCtr = 0 To intPeriod
                                            decSaveAmts(1) += decPeriodAmts(intColCtr)
                                        Next
                                        If blnShowasPercent = False Then
                                            spdAnalytics.Sheets(0).SetValue(intRowCtr, intFirstNumCol + 1, decSaveAmts(1))
                                        Else
                                            If decAccumTotals(1) <> 0 Then
                                                spdAnalytics.Sheets(0).SetValue(intRowCtr, intFirstNumCol + 1, decSaveAmts(1) / decAccumTotals(1))
                                                decSaveAmts(1) = decSaveAmts(1) / decAccumTotals(1)
                                            Else
                                                spdAnalytics.Sheets(0).SetValue(intRowCtr, intFirstNumCol + 1, 0)
                                                decSaveAmts(1) = 0
                                            End If
                                        End If
                                End Select
                            End If
                            If balance.YearType = strYearType(2) Then
                                SaveBalances(balance, decPeriodAmts)
                                Select Case intFormat
                                    Case 7
                                        If blnShowasPercent = False Then
                                            spdAnalytics.Sheets(0).SetValue(intRowCtr, intFirstNumCol + 2, decPeriodAmts(intPeriod))
                                            decSaveAmts(2) = decPeriodAmts(intPeriod)
                                        Else
                                            If decAccumTotals(2) <> 0 Then
                                                spdAnalytics.Sheets(0).SetValue(intRowCtr, intFirstNumCol + 2, decPeriodAmts(intPeriod) / decAccumTotals(2))
                                                decSaveAmts(2) = decPeriodAmts(intPeriod) / decAccumTotals(2)
                                            Else
                                                spdAnalytics.Sheets(0).SetValue(intRowCtr, intFirstNumCol + 2, 0)
                                                decSaveAmts(2) = 0
                                            End If
                                        End If
                                    Case 8
                                        decSaveAmts(2) = 0
                                        For intColCtr = 0 To intPeriod
                                            decSaveAmts(2) += decPeriodAmts(intColCtr)
                                        Next
                                        If blnShowasPercent = False Then
                                            spdAnalytics.Sheets(0).SetValue(intRowCtr, intFirstNumCol + 2, decSaveAmts(2))
                                        Else
                                            If decAccumTotals(2) <> 0 Then
                                                spdAnalytics.Sheets(0).SetValue(intRowCtr, intFirstNumCol + 2, decSaveAmts(2) / decAccumTotals(2))
                                                decSaveAmts(2) = decSaveAmts(2) / decAccumTotals(2)
                                            Else
                                                spdAnalytics.Sheets(0).SetValue(intRowCtr, intFirstNumCol + 2, 0)
                                                decSaveAmts(2) = 0
                                            End If
                                        End If
                                End Select
                            End If
                            If balance.YearType = strYearType(3) Then
                                SaveBalances(balance, decPeriodAmts)
                                Select Case intFormat
                                    Case 7
                                        If blnShowasPercent = False Then
                                            spdAnalytics.Sheets(0).SetValue(intRowCtr, intFirstNumCol + 3, decPeriodAmts(intPeriod))
                                            decSaveAmts(3) = decPeriodAmts(intPeriod)
                                        Else
                                            If decAccumTotals(3) <> 0 Then
                                                spdAnalytics.Sheets(0).SetValue(intRowCtr, intFirstNumCol + 3, decPeriodAmts(intPeriod) / decAccumTotals(3))
                                                decSaveAmts(3) = decPeriodAmts(intPeriod) / decAccumTotals(3)
                                            Else
                                                spdAnalytics.Sheets(0).SetValue(intRowCtr, intFirstNumCol + 3, 0)
                                                decSaveAmts(3) = 0
                                            End If
                                        End If
                                    Case 8
                                        decSaveAmts(3) = 0
                                        For intColCtr = 0 To intPeriod
                                            decSaveAmts(3) += decPeriodAmts(intColCtr)
                                        Next
                                        If blnShowasPercent = False Then
                                            spdAnalytics.Sheets(0).SetValue(intRowCtr, intFirstNumCol + 3, decSaveAmts(3))
                                        Else
                                            If decAccumTotals(3) <> 0 Then
                                                spdAnalytics.Sheets(0).SetValue(intRowCtr, intFirstNumCol + 3, decSaveAmts(3) / decAccumTotals(3))
                                                decSaveAmts(3) = decSaveAmts(3) / decAccumTotals(3)
                                            Else
                                                spdAnalytics.Sheets(0).SetValue(intRowCtr, intFirstNumCol + 3, 0)
                                                decSaveAmts(3) = 0
                                            End If
                                        End If
                                End Select
                            End If
                            If balance.YearType = strYearType(4) Then
                                SaveBalances(balance, decPeriodAmts)
                                Select Case intFormat
                                    Case 7
                                        If blnShowasPercent = False Then
                                            spdAnalytics.Sheets(0).SetValue(intRowCtr, intFirstNumCol + 4, decPeriodAmts(intPeriod))
                                            decSaveAmts(4) = decPeriodAmts(intPeriod)
                                        Else
                                            If decAccumTotals(4) <> 0 Then
                                                spdAnalytics.Sheets(0).SetValue(intRowCtr, intFirstNumCol + 4, decPeriodAmts(intPeriod) / decAccumTotals(4))
                                                decSaveAmts(4) = decPeriodAmts(intPeriod) / decAccumTotals(4)
                                            Else
                                                spdAnalytics.Sheets(0).SetValue(intRowCtr, intFirstNumCol + 4, 0)
                                                decSaveAmts(4) = 0
                                            End If
                                        End If
                                    Case 8
                                        decSaveAmts(4) = 0
                                        For intColCtr = 0 To intPeriod
                                            decSaveAmts(4) += decPeriodAmts(intColCtr)
                                        Next
                                        If blnShowasPercent = False Then
                                            spdAnalytics.Sheets(0).SetValue(intRowCtr, intFirstNumCol + 4, decSaveAmts(4))
                                        Else
                                            If decAccumTotals(4) <> 0 Then
                                                spdAnalytics.Sheets(0).SetValue(intRowCtr, intFirstNumCol + 4, decSaveAmts(4) / decAccumTotals(4))
                                                decSaveAmts(4) = decSaveAmts(4) / decAccumTotals(4)
                                            Else
                                                spdAnalytics.Sheets(0).SetValue(intRowCtr, intFirstNumCol + 4, 0)
                                                decSaveAmts(4) = 4
                                            End If
                                        End If
                                End Select
                            End If
                            If balance.YearType = strYearType(5) Then
                               SaveBalances(balance, decPeriodAmts)
                                Select Case intFormat
                                    Case 7
                                        If blnShowasPercent = False Then
                                            spdAnalytics.Sheets(0).SetValue(intRowCtr, intFirstNumCol + 5, decPeriodAmts(intPeriod))
                                            decSaveAmts(5) = decPeriodAmts(intPeriod)
                                        Else
                                            If decAccumTotals(5) <> 0 Then
                                                spdAnalytics.Sheets(0).SetValue(intRowCtr, intFirstNumCol + 5, decPeriodAmts(intPeriod) / decAccumTotals(5))
                                                decSaveAmts(5) = decPeriodAmts(intPeriod) / decAccumTotals(5)
                                            Else
                                                spdAnalytics.Sheets(0).SetValue(intRowCtr, intFirstNumCol + 5, 0)
                                                decSaveAmts(5) = 0
                                            End If
                                        End If
                                    Case 8
                                        decSaveAmts(5) = 0
                                        For intColCtr = 0 To intPeriod
                                            decSaveAmts(5) += decPeriodAmts(intColCtr)
                                        Next
                                        If blnShowasPercent = False Then
                                            spdAnalytics.Sheets(0).SetValue(intRowCtr, intFirstNumCol + 5, decSaveAmts(5))
                                        Else
                                            If decAccumTotals(5) <> 0 Then
                                                spdAnalytics.Sheets(0).SetValue(intRowCtr, intFirstNumCol + 5, decSaveAmts(5) / decAccumTotals(5))
                                                decSaveAmts(5) = decSaveAmts(5) / decAccumTotals(5)
                                            Else
                                                spdAnalytics.Sheets(0).SetValue(intRowCtr, intFirstNumCol + 5, 0)
                                                decSaveAmts(5) = 5
                                            End If
                                        End If
                                End Select
                            End If
                        Next
                    End If
                     If account.AcctDescriptor = "T" Then
                        AccumNI(spdAnalytics, account.AcctType.ClassDesc, intFormat, intNumberCols, decSaveAmts, intColCount, decIncAccumTotals, intRowCtr, intPeriod)
                    End If
                Next
                CommonProcedures.AddTotalRow(spdAnalytics, intFormat, intNumberCols, intFirstNumCol, intColCount, decIncAccumTotals, intRowCtr, "NI", "Net Income(Loss)", intPeriod)
            End Using

        End Sub

        Public Sub SetColumnHeader(spdAnalytics As FpSpread, intFormat As Integer, intPeriod As Integer, intColCount As Integer, intFirstNumCol As Integer)
            Dim strHeadingText As String = ""
            Dim intPeriodCtr As Integer
            Dim intFirstPeriod As Integer
            Dim intCurrentYear As Integer = 2012


            spdAnalytics.Sheets(0).ColumnHeaderAutoText = FarPoint.Web.Spread.HeaderAutoText.Blank
           

            spdAnalytics.Sheets(0).ColumnHeader.RowCount = 0
            spdAnalytics.Sheets(0).ColumnHeader.RowCount = 2
            spdAnalytics.Sheets(0).ColumnHeaderSpanModel.Add(0, 0, 1, 4)



            '  spdAnalytics.Sheets(0).ColumnHeaderSpanModel.Add(0, intFirstNumCol, 1, intColCount - intFirstNumCol)
            spdAnalytics.Sheets(0).ColumnHeader.Cells(0, 0, 0, intColCount - 1).HorizontalAlign = HorizontalAlign.Left
            spdAnalytics.Sheets(0).ColumnHeader.Cells(0, 0, 0, intColCount - 1).VerticalAlign = VerticalAlign.Top
            spdAnalytics.Sheets(0).ColumnHeader.Cells(0, intFirstNumCol, 1, intColCount - 1).HorizontalAlign = HorizontalAlign.Center
            spdAnalytics.Sheets(0).ColumnHeader.Cells(0, intFirstNumCol, 1, intColCount - 1).VerticalAlign = VerticalAlign.Bottom
            '' spdAnalytics.Sheets(0).ColumnHeader.Cells(1, 0, 1, intColCount - 1).VerticalAlign = VerticalAlign.Top
            spdAnalytics.Sheets(0).ColumnHeader.Rows(0).Font.Size = FontSize.Large
            spdAnalytics.Sheets(0).ColumnHeader.Rows(1).Font.Size = FontSize.Large
            spdAnalytics.Sheets(0).ColumnHeader.Rows(0).Font.Bold = True
            spdAnalytics.Sheets(0).ColumnHeader.Height = 40
            spdAnalytics.Sheets(0).ColumnHeader.Rows(0).Height = 20
            spdAnalytics.Sheets(0).ColumnHeader.Rows(1).Height = 20
            spdAnalytics.Sheets(0).ColumnHeader.Cells(1, 1).Text = "Cht"
            'set column headings for row 1 
            Select Case intFormat
                Case 0
                    strHeadingText = strHeadingText & CommonProcedures.GetMonth(intPeriod)
                Case 1
                    strHeadingText = "Year to date through " & CommonProcedures.GetMonth(intPeriod)
                Case 2
                    strHeadingText = "Current vs prior period - " & CommonProcedures.GetMonth(intPeriod)
                Case 3
                    strHeadingText = "Current vs prior year - " & CommonProcedures.GetMonth(intPeriod)
                Case 4
                    strHeadingText = "Current vs prior year year to date through " & CommonProcedures.GetMonth(intPeriod)
                Case 5
                    strHeadingText = "Current year trend"
                Case 6
                    strHeadingText = "12 month trend"
                Case 7
                    strHeadingText = "Multi-year trend for " & CommonProcedures.GetMonth(intPeriod)
                Case 8
                    strHeadingText = "Multi-year year to date trend through " & CommonProcedures.GetMonth(intPeriod)
            End Select
            Select Case intFormat
                Case 7, 8
                    For intPeriodCtr = intFirstNumCol To 9
                        If spdAnalytics.Sheets(0).Columns(intPeriodCtr).Visible = True Then
                            spdAnalytics.Sheets(0).ColumnHeaderSpanModel.Add(0, intPeriodCtr, 1, (9 - intPeriodCtr) + 1) 
                            spdAnalytics.Sheets(0).ColumnHeader.Cells(0, intPeriodCtr).Text = strHeadingText
                            Exit For
                        End If
                    Next
                Case Else
                    spdAnalytics.Sheets(0).ColumnHeaderSpanModel.Add(0, intFirstNumCol, 1, intColCount - intFirstNumCol)
                    spdAnalytics.Sheets(0).ColumnHeader.Cells(0, intFirstNumCol).Text = strHeadingText
            End Select

            'set column headings for row 2
            Select Case intFormat
                Case 0 To 4
                    spdAnalytics.Sheets(0).ColumnHeader.Cells(1, intFirstNumCol + 2).Text = "Variance"
                    spdAnalytics.Sheets(0).ColumnHeader.Cells(1, intFirstNumCol + 3).Text = "% Var"
                    Select Case intFormat
                        Case 0, 1
                            spdAnalytics.Sheets(0).ColumnHeader.Cells(1, intFirstNumCol).Text = "Budget"
                            spdAnalytics.Sheets(0).ColumnHeader.Cells(1, intFirstNumCol + 1).Text = "Actual"
                        Case 2 To 4
                            spdAnalytics.Sheets(0).ColumnHeader.Cells(1, intFirstNumCol).Text = "Prior"
                            spdAnalytics.Sheets(0).ColumnHeader.Cells(1, intFirstNumCol + 1).Text = "Current"
                    End Select
                Case 5 'current year trend
                    For intPeriodCtr = 0 To intPeriod
                        spdAnalytics.Sheets(0).ColumnHeader.Cells(1, intFirstNumCol + intPeriodCtr).Text = CommonProcedures.GetMonth(intPeriodCtr)
                    Next
                    spdAnalytics.Sheets(0).ColumnHeader.Cells(1, intFirstNumCol + intPeriod + 1).Text = CommonProcedures.GetMonth(12)
                Case 6 'last 12 months
                    If intPeriod < 11 Then
                        intFirstPeriod = intPeriod - 11
                    End If
                    For intPeriodCtr = intFirstPeriod To intFirstPeriod + 11
                        Dim intTest As Integer
                        If intPeriodCtr < 0 Then
                            intTest = intFirstNumCol + intPeriodCtr - intFirstPeriod
                            spdAnalytics.Sheets(0).ColumnHeader.Cells(1, intFirstNumCol + intPeriodCtr + (intFirstPeriod * -1)).Text = CommonProcedures.GetMonth(intPeriodCtr + 12)
                        Else
                            intTest = intFirstNumCol + intPeriodCtr + (intFirstPeriod * -1)
                            spdAnalytics.Sheets(0).ColumnHeader.Cells(1, intFirstNumCol + intPeriodCtr + (intFirstPeriod * -1)).Text = CommonProcedures.GetMonth(intPeriodCtr)
                        End If
                    Next
                    spdAnalytics.Sheets(0).ColumnHeader.Cells(1, intFirstNumCol + 12).Text = CommonProcedures.GetMonth(12)
                Case 7, 8
                    For intPeriodCtr = -5 To 0
                        spdAnalytics.Sheets(0).ColumnHeader.Cells(1, intFirstNumCol + intPeriodCtr + 5).Text = intCurrentYear + intPeriodCtr
                    Next

            End Select


        End Sub


       

        Private Sub AccumNI(ByRef spdAnalytics As FpSpread, ByVal strClassDesc As String, ByVal intFormat As Integer, ByVal intNumberCols As Integer, ByVal decSaveAmts() As Decimal, ByVal intColCount As Integer, ByRef decIncAccumTotals() As Decimal, ByRef intRowCtr As Integer, ByVal intPeriod As Integer)
            Select Case strClassDesc
                Case "Revenue"
                    For intColCtr = 0 To intNumberCols - 1
                        decIncAccumTotals(intColCtr) = decSaveAmts(intColCtr)
                    Next
                Case "Cost of sales"
                    For intColCtr = 0 To intNumberCols - 1
                        decIncAccumTotals(intColCtr) -= decSaveAmts(intColCtr)
                    Next
                    CommonProcedures.AddTotalRow(spdAnalytics, intFormat, intNumberCols, intFirstNumCol, intColCount, decIncAccumTotals, intRowCtr, "GP", "Gross profit", intPeriod)
                Case Else
                    For intColCtr = 0 To intNumberCols - 1
                        decIncAccumTotals(intColCtr) -= decSaveAmts(intColCtr)
                    Next
            End Select
        End Sub
    End Class
End Namespace