﻿Imports FarPoint.Mvc.Spread
Imports System.Data.Entity
Namespace onlineAnalytics
    Public Class DashboardController
        Inherits System.Web.Mvc.Controller

        '
        ' GET: /Dashboard
        Private utility As New Utility()
        Private userRepository As IUserRepository
        Dim UserType As UserRole
        Dim UserInfo As User
        Dim CustomerId As String = Nothing

        Function Index(<MvcSpread("spdDashboard")> spdDashboard As FpSpread, Optional SelectedCompany As Integer = Nothing, Optional SelectedAnalysis As Integer = Nothing) As ActionResult
            Dim Periods As List(Of SelectListItem) = New List(Of SelectListItem)
            For intCtr = 0 To 11
                Periods.Add(New SelectListItem With {.Text = CommonProcedures.GetMonth(intCtr), .Value = intCtr + 1, .Selected = False})
            Next
            ViewData("Periods") = New SelectList(Periods, "value", "text", "May")

            Dim Dept As List(Of SelectListItem) = New List(Of SelectListItem) From {
               New SelectListItem With {.Text = "Consolidated", .Value = "Consolidated", .Selected = False},
               New SelectListItem With {.Text = "Dept 1", .Value = "Dept 1", .Selected = False},
               New SelectListItem With {.Text = "Dept 2", .Value = "Dept 2", .Selected = True}
            }
            ViewData("Dept") = New SelectList(Dept, "value", "text", "selected")
            ViewData("intPeriod") = 11
            ViewData("intDept") = 1


            If (Not Session("UserType") Is Nothing) And (Not Session("UserInfo") Is Nothing) Then
                UserType = DirectCast(Session("UserType"), UserRole)
                UserInfo = DirectCast(Session("UserInfo"), User)
                CustomerId = If(UserType.UserRoleId = UserRoles.SAU, UserInfo.CustomerId, Nothing)


                If (SelectedAnalysis > 0) And (SelectedAnalysis <> UserInfo.AnalysisId) Then

                    Dim user = utility.UserRepository.GetUserByID(UserInfo.UserId)
                    user.AnalysisId = SelectedAnalysis
                    utility.UserRepository.Update(user)
                    utility.Save()
                End If

                'When user logged in, it will show the company list and analysis list based on analysisId
                If (SelectedCompany = 0) And (SelectedAnalysis = 0) Then

                    SelectedAnalysis = UserInfo.AnalysisId
                    SelectedCompany = Convert.ToInt64(utility.AnalysisRepository().GetAnalyses().Where(Function(b) b.AnalysisId = UserInfo.AnalysisId).Select(Function(a) a.CompanyId).FirstOrDefault())

                End If

            End If

            Dim companyAnalysisList As Hashtable = utility.PopulateCompanyAnalysisList(UserType, UserInfo, CustomerId, SelectedCompany, SelectedAnalysis)
            ViewBag.UserCompanies = companyAnalysisList.Item("CompanyList")
            ViewBag.CompanyAnalyses = companyAnalysisList.Item("AnalysisList")

            Return View()
        End Function

        <MvcSpreadEvent("Load", "spdDashboard", DirectCast(Nothing, String()))> _
        Private Sub FpSpread_Load(sender As Object, e As EventArgs)
            Dim spread As FpSpread = DirectCast(sender, FpSpread)
            If Not spread.Page.IsPostBack Then
                SetDashboardProperties(spread)
            End If

        End Sub

        Private Sub SetDashboardProperties(spdDashboard As FpSpread)
            Dim intChartCount As Integer = 6
            Dim intColCtr As Integer
            Dim intRowCtr As Integer
            Dim decValues(,) As Decimal
            Dim strXLabels(11) As String

            spdDashboard.RowHeader.Visible = False
            spdDashboard.ColumnHeader.Visible = False
            spdDashboard.CommandBar.Visible = False
            spdDashboard.BorderColor = Drawing.Color.White
            spdDashboard.Width = 930
            spdDashboard.Height = 500
            spdDashboard.VerticalScrollBarPolicy = FarPoint.Web.Spread.ScrollBarPolicy.Never
            spdDashboard.HorizontalScrollBarPolicy = FarPoint.Web.Spread.ScrollBarPolicy.Never
            spdDashboard.Sheets(0).DefaultStyle.Font.Size = FontSize.XXLarge
            spdDashboard.Sheets(0).SelectionBackColorStyle = FarPoint.Web.Spread.SelectionBackColorStyles.None
            spdDashboard.Sheets(0).RowCount = 5
            spdDashboard.Sheets(0).ColumnCount = 5
            spdDashboard.Sheets(0).Columns(0).Width = 300 
            spdDashboard.Sheets(0).Columns(1).Width = 15
            
            spdDashboard.Sheets(0).Columns(2).Width = 300
            spdDashboard.Sheets(0).Columns(3).Width = 15
            spdDashboard.Sheets(0).Columns(4).Width = 300
            spdDashboard.Sheets(0).Rows(0).Height = 20
            spdDashboard.Sheets(0).Rows(1).Height = 200
            spdDashboard.Sheets(0).Rows(2).Height = 20
            spdDashboard.Sheets(0).Rows(3).Height = 20
            spdDashboard.Sheets(0).Rows(4).Height = 200
            For intColCtr = 0 To 4
                For intRowCtr = 0 To 4
                    spdDashboard.Sheets(0).Cells(intRowCtr, intColCtr).Border.BorderColorRight = Drawing.Color.White
                    spdDashboard.Sheets(0).Cells(intRowCtr, intColCtr).Border.BorderColorLeft = Drawing.Color.White
                    spdDashboard.Sheets(0).Cells(intRowCtr, intColCtr).Border.BorderColorBottom = Drawing.Color.White
                Next  
                Select Case intColCtr
                    Case 1, 3
                        'spdDashboard.Sheets(0).Cells(0, intColCtr).Border.BorderColorBottom = Drawing.Color.White
                        'spdDashboard.Sheets(0).Cells(2, intColCtr).Border.BorderColorBottom = Drawing.Color.White
                    Case 0, 2, 4 
                        spdDashboard.Sheets(0).Cells(0, intColCtr).BackColor = Drawing.Color.FromArgb(39, 111, 166)
                        spdDashboard.Sheets(0).Cells(0, intColCtr).ForeColor = Drawing.Color.White
                        spdDashboard.Sheets(0).Cells(0, intColCtr).HorizontalAlign = HorizontalAlign.Center
                        spdDashboard.Sheets(0).Cells(0, intColCtr).VerticalAlign = VerticalAlign.Middle
                        spdDashboard.Sheets(0).Cells(3, intColCtr).BackColor = Drawing.Color.FromArgb(39, 111, 166)
                        spdDashboard.Sheets(0).Cells(3, intColCtr).ForeColor = Drawing.Color.White
                        spdDashboard.Sheets(0).Cells(3, intColCtr).HorizontalAlign = HorizontalAlign.Center
                        spdDashboard.Sheets(0).Cells(3, intColCtr).VerticalAlign = VerticalAlign.Middle
                End Select
            Next
            'Total revenue
            spdDashboard.Sheets(0).Cells(0, 0).Text = "Total Revenue"
            ReDim decValues(0, 11)
            decValues(0, 0) = 24016
            decValues(0, 1) = 12967
            decValues(0, 2) = 14742
            decValues(0, 3) = 45555
            decValues(0, 4) = 36746
            decValues(0, 5) = 27049
            decValues(0, 6) = 21241
            decValues(0, 7) = 36814
            decValues(0, 8) = 28056
            decValues(0, 9) = 39632
            decValues(0, 10) = 36696
            strXLabels(0) = "J"
            strXLabels(1) = "F"
            strXLabels(2) = "M"
            strXLabels(3) = "A"
            strXLabels(4) = "M"
            strXLabels(5) = "J"
            strXLabels(6) = "J"
            strXLabels(7) = "A"
            strXLabels(8) = "S"
            strXLabels(9) = "O"
            strXLabels(10) = "N"
            BuildLinewTrendChart(spdDashboard, 10, decValues, strXLabels, 0, 0, False, False, True)
            'Total net income
            spdDashboard.Sheets(0).Cells(0, 2).Text = "Net Income"
            ReDim decValues(0, 11)
            decValues(0, 0) = -4152
            decValues(0, 1) = -5976
            decValues(0, 2) = -1023
            decValues(0, 3) = 21177
            decValues(0, 4) = 9918
            decValues(0, 5) = -1139
            decValues(0, 6) = -8184
            decValues(0, 7) = 1126
            decValues(0, 8) = -6123
            decValues(0, 9) = 854
            decValues(0, 10) = 3567
            strXLabels(0) = "J"
            strXLabels(1) = "F"
            strXLabels(2) = "M"
            strXLabels(3) = "A"
            strXLabels(4) = "M"
            strXLabels(5) = "J"
            strXLabels(6) = "J"
            strXLabels(7) = "A"
            strXLabels(8) = "S"
            strXLabels(9) = "O"
            strXLabels(10) = "N"
            BuildBarChart(spdDashboard, 10, decValues, strXLabels, 1)

            'Total revenue
            spdDashboard.Sheets(0).Cells(0, 4).Text = "Percent discounts allowed"
            ReDim decValues(0, 11)
            decValues(0, 0) = 13.3
            decValues(0, 1) = 11.6
            decValues(0, 2) = 8.1
            decValues(0, 3) = 20.1
            decValues(0, 4) = 17.1
            decValues(0, 5) = 13.2
            decValues(0, 6) = 8.2
            decValues(0, 7) = 11.6
            decValues(0, 8) = 12.3
            decValues(0, 9) = 14.5
            decValues(0, 10) = 18.2
            strXLabels(0) = "J"
            strXLabels(1) = "F"
            strXLabels(2) = "M"
            strXLabels(3) = "A"
            strXLabels(4) = "M"
            strXLabels(5) = "J"
            strXLabels(6) = "J"
            strXLabels(7) = "A"
            strXLabels(8) = "S"
            strXLabels(9) = "O"
            strXLabels(10) = "N"
            BuildLinewTrendChart(spdDashboard, 10, decValues, strXLabels, 2, 0, True, True, True)


            spdDashboard.Sheets(0).Cells(3, 0).Text = "Unique visitors"
            ReDim decValues(0, 11)
            decValues(0, 0) = 2411
            decValues(0, 1) = 2234
            decValues(0, 2) = 2590
            decValues(0, 3) = 2630
            decValues(0, 4) = 2806
            decValues(0, 5) = 2179
            decValues(0, 6) = 2550
            decValues(0, 7) = 2699
            decValues(0, 8) = 2621
            decValues(0, 9) = 3864
            decValues(0, 10) = 3517
            strXLabels(0) = "J"
            strXLabels(1) = "F"
            strXLabels(2) = "M"
            strXLabels(3) = "A"
            strXLabels(4) = "M"
            strXLabels(5) = "J"
            strXLabels(6) = "J"
            strXLabels(7) = "A"
            strXLabels(8) = "S"
            strXLabels(9) = "O"
            strXLabels(10) = "N"
            BuildLinewTrendChart(spdDashboard, 10, decValues, strXLabels, 3, 0, False, True, True)


            spdDashboard.Sheets(0).Cells(3, 2).Text = "Trial downloads"
            ReDim decValues(0, 11)
            decValues(0, 0) = 183
            decValues(0, 1) = 149
            decValues(0, 2) = 183
            decValues(0, 3) = 206
            decValues(0, 4) = 217
            decValues(0, 5) = 188
            decValues(0, 6) = 230
            decValues(0, 7) = 221
            decValues(0, 8) = 234
            decValues(0, 9) = 334
            decValues(0, 10) = 243
            strXLabels(0) = "J"
            strXLabels(1) = "F"
            strXLabels(2) = "M"
            strXLabels(3) = "A"
            strXLabels(4) = "M"
            strXLabels(5) = "J"
            strXLabels(6) = "J"
            strXLabels(7) = "A"
            strXLabels(8) = "S"
            strXLabels(9) = "O"
            strXLabels(10) = "N"
            BuildLinewTrendChart(spdDashboard, 10, decValues, strXLabels, 4, 0, False, True, True)

            spdDashboard.Sheets(0).Cells(3, 4).Text = "Trial conversion rate"
            ReDim decValues(0, 11)
            decValues(0, 0) = 14
            decValues(0, 1) = 10
            decValues(0, 2) = 11
            decValues(0, 3) = 13
            decValues(0, 4) = 14
            decValues(0, 5) = 11
            decValues(0, 6) = 7
            decValues(0, 7) = 13
            decValues(0, 8) = 13
            decValues(0, 9) = 9
            decValues(0, 10) = 12
            strXLabels(0) = "J"
            strXLabels(1) = "F"
            strXLabels(2) = "M"
            strXLabels(3) = "A"
            strXLabels(4) = "M"
            strXLabels(5) = "J"
            strXLabels(6) = "J"
            strXLabels(7) = "A"
            strXLabels(8) = "S"
            strXLabels(9) = "O"
            strXLabels(10) = "N"
            BuildLinewTrendChart(spdDashboard, 10, decValues, strXLabels, 5, 0, True, True, True)

         
        End Sub

        Private Sub BuildLinewTrendChart(ByRef spdDashboard As FpSpread, intColCount As Integer, decValues(,) As Decimal, strXLabels() As String, intChartIndex As Integer, intFormat As Integer, blnShowasPerc As Boolean, blnSmoothed As Boolean, blnMarkers As Boolean)


            Dim intCol As Integer = 8
            Dim intColCtr As Integer
            Dim intRowCount As Integer = 1
            Dim decIntercept As Decimal = 0
            Dim decSlope As Decimal = 0
            Dim dblCoeff As Double = 0



            '' Dim cseries As New FarPoint.Web.Chart.cl
            Dim sseries As New FarPoint.Web.Chart.LineSeries
            Dim tseries As New FarPoint.Web.Chart.LineSeries
            Dim plotArea As New FarPoint.Web.Chart.YPlotArea()
            Dim chart As New FarPoint.Web.Spread.Chart.SpreadChart()
            Dim model As New FarPoint.Web.Chart.ChartModel()
            Dim NoMarker As New FarPoint.Web.Chart.NoMarker()

            With sseries
                .SeriesName = "Actual"
                For intColCtr = 0 To intColCount
                    .Values.Add(decValues(0, intColCtr))
                    .CategoryNames.Add(strXLabels(intColCtr))
                Next
            End With
            If blnMarkers = False Then
                sseries.PointMarker = NoMarker
            Else
                sseries.PointMarker = New FarPoint.Web.Chart.BuiltinMarker(FarPoint.Web.Chart.MarkerShape.Circle, 3.0F)
            End If
            If blnSmoothed = True Then
                sseries.SmoothedLine = True
            End If
            sseries.LineBorder = New FarPoint.Web.Chart.SolidLine(Drawing.Color.Blue, 0.5F)
            'get trend amounts
            ComputeTrend(decIntercept, decSlope, dblCoeff, decValues, intColCount)
            With tseries
                .SeriesName = "Trend"
                For intColCtr = 0 To intColCount
                    .Values.Add(decIntercept + (intColCtr * decSlope))
                Next
            End With
            tseries.PointMarker = NoMarker
            tseries.LineBorder = New FarPoint.Web.Chart.SolidLine(Drawing.Color.Red, 0.5F)
            Dim slope As New FarPoint.Web.Chart.LabelArea
            Dim strText As String
            If decSlope > 0 Then
                strText = "Trend - Increasing at rate of "
            Else
                strText = "Trend - Decreasing at rate of "
            End If
            If blnShowasPerc = False Then
                strText = strText & Format(Double.Parse(Math.Round(decSlope, 0)), "###,###,###")
                Select Case intFormat
                    Case 0, 2, 3, 5, 5
                        strText = strText & " per month"
                    Case Else
                        strText = strText & " per year"
                End Select
            Else
                strText = strText & Math.Round(decSlope, 2)
                Select Case intFormat
                    Case 0, 2, 3, 5
                        strText = strText & "% per month"
                    Case Else
                        strText = strText & "% per year"
                End Select
            End If
            slope.Text = strText
            slope.Location = New System.Drawing.PointF(0.1F, 0.93F)
            Dim LabFont As System.Drawing.Font
            LabFont = New System.Drawing.Font("Arial", 8)
            slope.TextFont = LabFont
            sseries.LabelTextFont = LabFont

            tseries.LabelTextFont = LabFont
            model.LabelAreas.Add(slope)
            'set properties of plot area
            plotArea.Location = New System.Drawing.PointF(0.2F, 0.1F)
            plotArea.Size = New System.Drawing.SizeF(0.7F, 0.7F)

            'End Select
            plotArea.Series.Add(sseries)
            plotArea.Series.Add(tseries)

            model.PlotAreas.Add(plotArea)
            chart.Model = model
            chart.Width = 300
            chart.Height = 200
            chart.Left = GetLeftPos(intChartIndex)
            chart.Top = GetTopPos(intChartIndex)
            chart.BackColor = Drawing.Color.LightGray
            chart.BorderColor = Drawing.Color.White
            chart.CanMove = False
            chart.CanSize = False
            chart.CanSelect = False

            spdDashboard.Sheets(0).Charts.Add(chart)

        End Sub
        Private Sub BuildBarChart(ByVal spdDashboard As FpSpread, ByVal intColCount As Integer, ByVal decValues(,) As Decimal, ByVal strXLabels() As String, ByVal intChartIndex As Integer)


            Dim intCol As Integer = 8
            Dim intColCtr As Integer
            Dim intRowCount As Integer = 1
            Dim intSelRowCtr As Integer
            Dim cseries As New FarPoint.Web.Chart.ClusteredBarSeries
            Dim sseries As New FarPoint.Web.Chart.StackedBarSeries
            Dim plotArea As New FarPoint.Web.Chart.YPlotArea()

            Dim chart As New FarPoint.Web.Spread.Chart.SpreadChart()
            Dim model As New FarPoint.Web.Chart.ChartModel()
            Dim aSeries As FarPoint.Web.Chart.BarSeries
            aSeries = New FarPoint.Web.Chart.BarSeries
            With aSeries
                .SeriesName = "Actual"
                For intColCtr = 0 To intColCount
                    .Values.Add(decValues(0, intColCtr))
                    If intSelRowCtr = 0 Then
                        .CategoryNames.Add(strXLabels(intColCtr))
                    End If
                    If decValues(0, intColCtr) > 1 Then
                        .BarFills.Add(New FarPoint.Web.Chart.SolidFill(Drawing.Color.Green))
                    Else
                        .BarFills.Add(New FarPoint.Web.Chart.SolidFill(Drawing.Color.Red))
                    End If
                Next
            End With
            'set properties of plot area
            plotArea.Location = New System.Drawing.PointF(0.2F, 0.1F)
            plotArea.Size = New System.Drawing.SizeF(0.7F, 0.7F)
            plotArea.Series.Add(aSeries)
            model.PlotAreas.Add(plotArea)
            chart.Model = model
            chart.Width = 300
            chart.Height = 200
            chart.Left = GetLeftPos(intChartIndex)
            chart.Top = GetTopPos(intChartIndex)
            chart.CanMove = False
            chart.CanSelect = False
            chart.BorderColor = Drawing.Color.White
            spdDashboard.Sheets(0).Charts.Add(chart)

        End Sub

        Private Function GetLeftPos(ByVal intChartIndex As Integer) As Integer
            Select Case intChartIndex
                Case 0, 3
                    GetLeftPos = 0
                Case 1, 4
                    GetLeftPos = 315
                Case Else
                    GetLeftPos = 630
            End Select
        End Function
        Private Function GetTopPos(ByVal intChartIndex As Integer) As Integer
            GetTopPos = 0
            Select Case intChartIndex
                Case 0 To 2
                    GetTopPos = 25  '21
                Case 3 To 5
                    GetTopPos = 280 '265
            End Select
        End Function


        Private Sub ComputeTrend(ByRef decIntercept As Decimal, ByRef decSlope As Decimal, ByRef dblCoeff As Double, ByVal decValues(,) As Decimal, intNumbColCount As Integer)
            Dim intDataPoints As Integer = intNumbColCount
            Dim XFactors(intNumbColCount - 1) As Decimal
            Dim YFactors(intNumbColCount - 1) As Decimal
            Dim intCtr As Integer
            Dim sx, sy, sxy, sx2, sy2, XX, YY, temp As Single

            'load values into arrays
            For intCtr = 0 To intNumbColCount - 1
                XFactors(intCtr) = intCtr
                YFactors(intCtr) = decValues(0, intCtr)
            Next
            'compute slope, intercept and coeff
            For intCtr = 0 To intDataPoints - 1
                XX = XFactors(intCtr)
                YY = YFactors(intCtr)
                sx += XX
                sy += YY
                sxy = sxy + XX * YY
                sx2 = sx2 + XX * XX
                sy2 = sy2 + YY * YY
            Next
            If (intDataPoints * sx2 - sx * sx) <> 0 Then
                decSlope = (intDataPoints * sxy - sx * sy) / (intDataPoints * sx2 - sx * sx)
            Else
                decSlope = 0
            End If
            If intDataPoints <> 0 Then
                decIntercept = (sy - decSlope * sx) / intDataPoints
            Else
                decIntercept = 0
            End If
            temp = (intDataPoints * sx2 - sx * sx) * (intDataPoints * sy2 - sy * sy)
            If temp <> 0 Then
                dblCoeff = (intDataPoints * sxy - sx * sy) / Math.Sqrt(temp)
            End If

        End Sub

    End Class

    
End Namespace