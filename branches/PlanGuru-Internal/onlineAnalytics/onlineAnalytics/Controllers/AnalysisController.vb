﻿Imports System.Data.Entity
Imports PagedList
Imports onlineAnalytics


Namespace onlineAnalytics
    Public Class AnalysisController
        Inherits System.Web.Mvc.Controller

        Private utility As New Utility()
        '
        ' GET: /Analysis
        Private analysisRepository As IAnalysisRepository
        Private userRepository As IUserRepository
        Private useranalysisMapping As IUserAnalysisMappingRepository
        Dim UserType As UserRole
        Dim UserInfo As User
        Dim CustomerId As String = Nothing

        Public Sub New()
            Me.analysisRepository = New AnalysisRepository(New DataAccess())
        End Sub

        Public Sub New(analysisRepository As IAnalysisRepository)
            Me.analysisRepository = analysisRepository
        End Sub

        Function Index() As ViewResult
            Try
                If (Not Session("UserType") Is Nothing) And (Not Session("UserInfo") Is Nothing) Then
                    UserType = DirectCast(Session("UserType"), UserRole)
                    UserInfo = DirectCast(Session("UserInfo"), User)
                    CustomerId = If(UserType.UserRoleId = UserRoles.SAU, UserInfo.CustomerId, Nothing)
                End If

                'Dim analysis = utility.AnalysisRepository.Get(orderBy:=Function(q) q.OrderBy(Function(d) d.CompanyId), includeProperties:="Company"               

                Dim analysis = From a In utility.AnalysisRepository.GetAnalyses()                   

                If (Not CustomerId Is Nothing) Then
                    analysis = (From a In analysis
                               Join us In utility.CompaniesRepository.GetCompanies() On a.CompanyId Equals us.CompanyId
                                Join u In utility.UserRepository.GetUsers() On u.CustomerId Equals us.CustomerId
                                Where u.CustomerId.Equals(CustomerId, StringComparison.OrdinalIgnoreCase) Select a).Distinct()
                End If


                Logger.Log.Info(String.Format("Analysis Index Execution Done"))
                Return View(analysis)
            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Unable to Index() Analysis-", ex.Message)
                Logger.Log.Error(String.Format("\n Unable to Index() Anlaysis with Message- {0} " + Environment.NewLine + "Stack Trace: {1} ", ex.Message, ex.StackTrace))
                Return View()
            Finally
                Logger.Log.Info(String.Format("Index Analysis Execution Ended"))
            End Try
        End Function

        Function Create(SelectedCompany As System.Nullable(Of Integer)) As ViewResult
            Dim analysisDetail As New Analysis()
            Try
               
                Dim companyId As Integer = SelectedCompany.GetValueOrDefault()
                PopulateCompanyList(companyId)
                PopulateUserList(analysisDetail)
                Logger.Log.Info(String.Format("Analysis Create Execution Done"))
                Return View(analysisDetail)
            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Unable to Create Analysis-", ex.Message)
                Logger.Log.Error(String.Format("\n Unable to Create() Analysis with Message- {0} " + Environment.NewLine + "Stack Trace: {1} ", ex.Message, ex.StackTrace))
                Return View()
            Finally
                Logger.Log.Info(String.Format("Create Analysis Execution Ended"))
            End Try

        End Function

        <HttpPost()>
        Function Create(analysisDetail As Analysis) As ActionResult
            Try
                If ModelState.IsValid Then

                    Dim AnalysisNameIsUnique = utility.AnalysisRepository.ValidateAnalysis(analysisDetail.AnalysisName, analysisDetail.CompanyId)

                    If (AnalysisNameIsUnique.Count > 0) Then
                        ModelState.AddModelError("AnalysisName", "Analysis name already present for selected company. Please provide distinct analysis name.")
                    Else
                        analysisDetail.CreatedBy = 1
                        analysisDetail.UpdatedBy = 1
                        utility.AnalysisRepository.InsertAnalysis(analysisDetail)
                        utility.Save()
                        Logger.Log.Info(String.Format("Analysis Created successfully with id {0} ", analysisDetail.AnalysisId))

                        If Not (analysisDetail.PostedUsers Is Nothing) Then
                            Dim useranalysis As New UserAnalysisMapping()
                            For Each selecteduser As String In analysisDetail.PostedUsers.UserIds
                                useranalysis.AnalysisId = analysisDetail.AnalysisId
                                useranalysis.UserId = selecteduser.ToString()
                                useranalysis.CreatedBy = 1
                                useranalysis.UpdatedBy = 1
                                utility.UserAnalysisMappingRepository.Insert(useranalysis)
                                utility.UserAnalysisMappingRepository.Save()
                            Next
                            Logger.Log.Info(String.Format("Users are mapped with analysis successfully with Analysis ID : {0} , Analysis Name {1} ", analysisDetail.AnalysisId, analysisDetail.AnalysisName))
                        End If

                        TempData("Message") = "Analysis created successfully."
                        Return RedirectToAction("Index")

                    End If
                End If

                    PopulateCompanyList(analysisDetail.CompanyId)
                    PopulateUserList(analysisDetail)
                    Logger.Log.Info(String.Format("Create Analysis Successfully Executed"))

            Catch dataEx As DataException
                TempData("ErrorMessage") = String.Concat("Unable to create analysis-", dataEx.Message)
                Logger.Log.Error(String.Format("Unable to Create Analysis id- {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2} ", analysisDetail.AnalysisId, dataEx.Message, dataEx.StackTrace))
            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Unable to create analysis-", ex.Message)
                Logger.Log.Error(String.Format("Unable to Create Analysis id- {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2} ", analysisDetail.AnalysisId, ex.Message, ex.StackTrace))
            Finally
                Logger.Log.Info(String.Format("Create Analysis Execution Ended"))
            End Try
            Return View(analysisDetail)
        End Function

        Private Sub PopulateCompanyList(Optional SelectedCompany As Object = Nothing)
            Logger.Log.Info(String.Format("Populate Company List  started "))
            Try

                If (Not Session("UserType") Is Nothing) And (Not Session("UserInfo") Is Nothing) Then
                    UserType = DirectCast(Session("UserType"), UserRole)
                    UserInfo = DirectCast(Session("UserInfo"), User)
                    CustomerId = If(UserType.UserRoleId = UserRoles.SAU, UserInfo.CustomerId, Nothing)

                End If

                Dim companies = utility.CompanyRepository.Get(orderBy:=Function(q) q.OrderBy(Function(d) d.CompanyName))

                If (Not CustomerId Is Nothing) Then
                    companies = companies.Where(Function(c) c.CustomerId = CustomerId)
                End If

                If (Not UserType Is Nothing And Not UserInfo Is Nothing) Then
                    If (UserType.UserRoleId = UserRoles.CAU Or UserType.UserRoleId = UserRoles.CRU) Then
                        companies = From c In companies
                                      Join ucm In utility.UserCompanyMappingRepository.GetUserCompanyMapping() On c.CompanyId Equals ucm.CompanyId
                                      Join u In utility.UserRepository.GetUsers() On u.UserId Equals ucm.UserId
                                      Where u.UserId.Equals(UserInfo.UserId, StringComparison.OrdinalIgnoreCase) Select c

                    End If
                End If

                ViewBag.SelectedCompany = New SelectList(companies, "CompanyId", "CompanyName", SelectedCompany)
                Logger.Log.Info(String.Format("Populate Company List successfully Loaded"))
            Catch dataEx As DataException
                TempData("ErrorMessage") = String.Concat("Unable to Populate Company List -", dataEx.Message)
                Logger.Log.Error(String.Format("Unable to Populate Company List with Message- {0} " + Environment.NewLine + "Stack Trace: {1} ", dataEx.Message, dataEx.StackTrace))
                Throw
            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Unable to Populate Company List-", ex.Message)
                Logger.Log.Error(String.Format("Unable to populate company list with Message- {0} " + Environment.NewLine + "Stack Trace: {1}", ex.Message, ex.StackTrace))
                Throw
            Finally
                Logger.Log.Info(String.Format("Execution Ended"))
            End Try
        End Sub

        Private Sub PopulateUserList(analysisDetail As Analysis)
            Try

                If (Not Session("UserType") Is Nothing) And (Not Session("UserInfo") Is Nothing) Then
                    UserType = DirectCast(Session("UserType"), UserRole)
                    UserInfo = DirectCast(Session("UserInfo"), User)
                    CustomerId = If(Not UserInfo.CustomerId Is Nothing, UserInfo.CustomerId, Nothing)

                End If


                Dim users = utility.UserRepository.GetUsers().Where(Function(u) (u.UserRoleId <> UserRoles.PGAAdmin) And (u.UserRoleId <> UserRoles.PGASupport))

                If (Not CustomerId Is Nothing And (Not UserType Is Nothing) And (Not UserInfo Is Nothing)) Then

                    users = From c In users.Where(Function(a) a.CustomerId = CustomerId And a.UserRoleId >= UserInfo.UserRoleId)

                End If

                Dim userAnalysis = utility.UserAnalysisMappingRepository.GetUserAnalysisMappingByAnalysisId(analysisDetail.AnalysisId)
                Dim userList As List(Of User_) = New List(Of User_)
                'if an array of posted user ids exists and is not empty,
                'save selected ids
                For Each user As User In users
                    userList.Add(New User_(user.UserId, user.FirstName, 1, False))
                Next
                analysisDetail.AvailableUsers = userList
                If Not (userAnalysis Is Nothing) Then
                    Dim InsertedUserList As List(Of User_) = New List(Of User_)
                    Dim InsertedUsers = users.Where(Function(b) userAnalysis.Any(Function(a) a.UserId = b.UserId))
                    For Each user As User In InsertedUsers
                        InsertedUserList.Add(New User_(user.UserId, user.FirstName, 1, False))
                    Next
                    analysisDetail.SelectedUsers = InsertedUserList
                End If
                ' if a view model array of posted user ids exists and is not empty,
                ' save selected ids
                If Not (analysisDetail.PostedUsers Is Nothing) Then
                    Dim selectedUserList As List(Of User_) = New List(Of User_)
                    Dim selectedUsers = utility.UserRepository.GetUsers.Where(Function(u) analysisDetail.PostedUsers.UserIds.Any(Function(s) u.UserId.ToString().Equals(s))).ToList()
                    For Each user As User In selectedUsers
                        selectedUserList.Add(New User_(user.UserId, user.FirstName, 1, False))
                    Next
                    analysisDetail.SelectedUsers = selectedUserList
                End If
                Logger.Log.Info(String.Format("Populate User List Successfully Executed"))
            Catch dataEx As DataException
                TempData("ErrorMessage") = String.Concat("Populate User List-->", dataEx.Message)
                Logger.Log.Error(String.Format("Unable to Populate User List with Analysis id- {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2} ", analysisDetail.AnalysisId, dataEx.Message, dataEx.StackTrace))
                Throw
            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Populate User List-->", ex.Message)
                Logger.Log.Error(String.Format("Unable to Populate User List with Analysis id- {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2} ", analysisDetail.AnalysisId, ex.Message, ex.StackTrace))
                Throw
            Finally
                Logger.Log.Info(String.Format("Populate UserList Execution Ended"))
            End Try
        End Sub

        Function Edit(Id As Integer) As ViewResult
            Try
                Dim analysis = analysisRepository.GetAnalysisById(Id)

                If (Not analysis Is Nothing) Then
                    analysis.CompanyName = utility.CompanyRepository.Get(filter:=Function(c) c.CompanyId = analysis.CompanyId).Select(Function(s) s.CompanyName).FirstOrDefault().ToString()
                End If

                'PopulateCompanyList(analysis.CompanyId)

                PopulateUserList(analysis)

                Logger.Log.Info(String.Format("Analysis  updated successfully id- {0} ", Id))
                Return View(analysis)

            Catch dataEx As DataException
                TempData("ErrorMessage") = String.Concat("Unable to Edit analysis-", dataEx.Message)
                Logger.Log.Error(String.Format("Unable to Edit Analysis id- {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2} ", Id, dataEx.Message, dataEx.StackTrace))
                'Log the error (add a variable name after DataException)
                ModelState.AddModelError("", "Unable to update analysis-." + dataEx.Message)

                'Return RedirectToAction("Index")
            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Unable to Edit analysis-", ex.Message)
                Logger.Log.Error(String.Format("Unable to Edit Analysis id- {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2} ", Id, ex.Message, ex.StackTrace))
                ModelState.AddModelError("", "Unable to update analysis." + ex.Message)
            Finally
                Logger.Log.Info(String.Format("Execution Ended"))
                'Return RedirectToAction("Index")
            End Try

            Return Nothing
        End Function

        <HttpPost()>
        Function Edit(AnalysisDetail As Analysis) As ActionResult
            Try
                If ModelState.IsValid Then

                    AnalysisDetail.CreatedBy = 1
                    AnalysisDetail.UpdatedBy = 1
                    analysisRepository.UpdateAnalysis(AnalysisDetail)
                    analysisRepository.Save()

                    'Get list of User mapped with the selected company
                    Dim selectMappedUserWithAnalysis As Dictionary(Of String, Integer) = utility.UserAnalysisMappingRepository.GetUserAnalysisMappingByAnalysisId(AnalysisDetail.AnalysisId).ToDictionary(Function(a) a.UserId, Function(a) a.UserAnalysisMappingId)

                    Dim useranalysis As New UserAnalysisMapping()

                    If Not (AnalysisDetail.PostedUsers Is Nothing) Then
                        For Each selecteduser As String In AnalysisDetail.PostedUsers.UserIds

                            'If New
                            If selectMappedUserWithAnalysis.ContainsKey(selecteduser) = False Then

                                useranalysis.AnalysisId = AnalysisDetail.AnalysisId
                                useranalysis.UserId = selecteduser.ToString()
                                useranalysis.CreatedBy = 1
                                useranalysis.UpdatedBy = 1
                                utility.UserAnalysisMappingRepository.Insert(useranalysis)
                                utility.UserAnalysisMappingRepository.Save()
                                Logger.Log.Info(String.Format("User is successfully mapped with company, Company ID : {0} , UserId {1} ", AnalysisDetail.AnalysisId, useranalysis.UserId))
                            End If

                            selectMappedUserWithAnalysis.Remove(selecteduser)
                        Next
                    End If
                    

                    'Delete the users, which are not found within redefine selected list.
                    If (selectMappedUserWithAnalysis.Count > 0) Then

                        Logger.Log.Info(String.Format("Total {0} users found to be delete of AnalysisId {1} from UserAnalysisMapping ", selectMappedUserWithAnalysis.Count, AnalysisDetail.AnalysisId))
                        Dim getSelectMappedUser = String.Concat("'", String.Join("','", selectMappedUserWithAnalysis.Select(Function(a) a.Key.ToString())), "'")
                        Dim result = utility.UserAnalysisMappingRepository.DeleteUserAnalysisMappingByUserIdAndAnalysisId(AnalysisDetail.AnalysisId, getSelectMappedUser)

                        If (result = selectMappedUserWithAnalysis.Count) Then
                            Logger.Log.Info(String.Format("Users delete successfully, which is not found within redefine selected list with analysis, AnalysisId : {0} , Users {1} ", AnalysisDetail.AnalysisId, getSelectMappedUser))
                        Else
                            'Logger.Log.Info(String.Format("Delete users list mismatch --> FoundUsersToDelete {0} : , ActualUsersDeleted : {1} "))
                            Logger.Log.Info(String.Format("Users delete successfully, which is not found within redefine selected list with analysis, AnalysisId : {0} , Users {1} ", AnalysisDetail.AnalysisId, getSelectMappedUser))
                        End If

                    End If


                    TempData("Message") = "Analysis updated successfully."
                    Logger.Log.Info(String.Format("Analysis Updated Successfully id- {0}", AnalysisDetail.AnalysisId))
                    Return RedirectToAction("Index")

                End If
            Catch dataEx As DataException
                TempData("ErrorMessage") = String.Concat("Unable to update Analysis-", dataEx.Message)
                Logger.Log.Error(String.Format("Unable to Update Analysis id- {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2}", AnalysisDetail.AnalysisId, dataEx.Message, dataEx.StackTrace))
                'Log the error (add a variable name after DataException)
                'ModelState.AddModelError("", "Unable to update company-." + dataEx.Message)

            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Unable to update Analysis-", ex.Message)
                Logger.Log.Error(String.Format("Unable to Update Analysis id- {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2}", AnalysisDetail.AnalysisId, ex.Message, ex.StackTrace))
                'ModelState.AddModelError("", "Unable to update company." + ex.Message)
            Finally
                Logger.Log.Info(String.Format("Execution Ended"))
            End Try

            PopulateCompanyList(AnalysisDetail.CompanyId)
            PopulateUserList(AnalysisDetail)
            Return View(AnalysisDetail)
        End Function

        Function Delete(Id As Integer) As ActionResult
            Logger.Log.Info(String.Format("Analysis Delete started {0}", Id))
            Try
                analysisRepository.DeleteAnalysis(Id)
                analysisRepository.Save()

                TempData("Message") = "Analysis deleted successfully."
                Logger.Log.Info(String.Format("Analysis  deleted successfully id- {0}", Id))
            Catch dataEx As DataException
                TempData("ErrorMessage") = String.Concat("Unable to delete analysis - ", dataEx.Message)
                Logger.Log.Error(String.Format("Unable to Delete Analysis id- {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2}", Id, dataEx.Message, dataEx.StackTrace))
                Return RedirectToAction("Index", New System.Web.Routing.RouteValueDictionary() _
                                        From {{"id", Id}, {"deleteChangesError", True}})

            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Unable to delete analysis-", ex.Message)
                Logger.Log.Error(String.Format("Unable to Delete Analysis id- {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2}", Id, ex.Message, ex.StackTrace))
                ModelState.AddModelError("Index", "Unable to delete analysis." + ex.Message)

                Return RedirectToAction("Index")
            Finally
                Logger.Log.Info(String.Format("Execution Ended"))
            End Try
            Return RedirectToAction("Index")
        End Function

        Protected Overrides Sub Dispose(disposing As Boolean)
            analysisRepository.Dispose()
            MyBase.Dispose(disposing)
        End Sub

    End Class
End Namespace
