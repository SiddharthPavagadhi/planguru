﻿Imports System
Imports System.Collections.Generic
Imports System.Configuration
Imports System.Linq
Imports System.Web.Mvc
Imports System.Data.Entity
Imports Recurly

Namespace onlineAnalytics
    Public Class HomeController
        Inherits System.Web.Mvc.Controller

        Function Index() As ActionResult


            Database.SetInitializer(New SampleData())
            TestforData()
            GetAllRecords()
            ViewData("width") = 1000

            'Dim plan As RecurlyPlan = RecurlyPlan.Get("planguru-analytics")

            'Dim subscription As RecurlySubscription = RecurlySubscription.Get("1cfd23ee-f19c-4a6f-8721-098696abe7c9")
            'Dim account As RecurlyAccount = RecurlyAccount.Get("1cfd23ee-f19c-4a6f-8721-098696abe7c9")

            'Dim started As String = Convert.ToDateTime(subscription.CurrentPeriodStartedAt).ToString("MMM dd, yyyy")
            'Dim ended As String = Convert.ToDateTime(subscription.CurrentPeriodEndsAt).ToString("MMM dd, yyyy")
            'Dim amount As Double = (subscription.UnitAmountInCents / 100)

            'Dim SubscribeUser = utility.CustomerRepository.GetCustomerById("1cfd23ee-f19c-4a6f-8721-098696abe7c9")
            'SendEmail(SubscribeUser, EmailType.NewSubscriptionSignup)

            'Dim User As User = utility.UserRepository.GetUserByID("cdc0b0b9-d04d-41cc-abc3-d7d6db0cb384")
            ' SendEmail(User, EmailType.NewUserAdded)

            'Dim localDBUserInfo = utility.UserRepository.GetUserByID("7c301c8f-5a6c-49d6-9af5-51a4c3599ab1")
            'DeleteUserEmail(localDBUserInfo, EmailType.UserDeleted)

            'Dim userInfo = utility.UserRepository.GetUserByID("0b44385a-3897-40ce-a5ed-3b70aaadf8de")
            'NewUserSignUp(userInfo, EmailType.NewUserSignUp)

            'Dim Customer = utility.CustomerRepository.GetCustomerById("9de3811b-463b-498f-98f2-8d2d43aee745")
            'NewsubscriptionSignup(Customer, EmailType.NewSubscriptionSignup)

            'Dim subscri = Recurly.RecurlySubscription.Get("9de3811b-463b-498f-98f2-8d2d43aee745")
            'Dim acc = Recurly.RecurlyAccount.Get("9de3811b-463b-498f-98f2-8d2d43aee745")
            'Dim bill = Recurly.RecurlyBillingInfo.Get("9de3811b-463b-498f-98f2-8d2d43aee745")

            'Dim invs As Recurly.RecurlyInvoice() = Recurly.RecurlyInvoiceList.GetInvoices("9de3811b-463b-498f-98f2-8d2d43aee745", 1)
            'Dim invNo As Integer
            'For Each item In invs
            '    invNo = item.Number
            'Next
            'Dim inv = Recurly.RecurlyInvoice.Get(invNo)

            'Dim Customer = utility.CustomerRepository.GetCustomerById("9de3811b-463b-498f-98f2-8d2d43aee745")
            'CancelSubscription(Customer, EmailType.SubcriptionCancellation)

            'Return RedirectToAction("Login", "User", New With {.returnUrl = HttpContext.Request.Url.ToString()})
            Return View()
        End Function

        Private utility As New Utility()
        Private common As New Common()
        Private Sub CancelSubscription(customer As Customer, emailId As Integer)
            Try
                Dim objEmailInfo = utility.EmailRepository.GetEmailInfoById(emailId)
                objEmailInfo.EmailBody = common.MergeCustomerEmailBody(customer, objEmailInfo)
                common.SendCustomerEmail(customer, objEmailInfo)
                Logger.Log.Info(String.Format("SendEmail Executed"))
            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Unable to Send Email -", ex.Message)
                Logger.Log.Error(String.Format("Unable to Send Email to CustomerID - {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2} ", customer.CustomerId, ex.Message, ex.StackTrace))
            Finally
                Logger.Log.Info(String.Format("SendEmail Execution Ended"))
            End Try
        End Sub
        Private Sub NewsubscriptionSignup(customer As Customer, emailId As Integer)
            Try
                Dim objEmailInfo = utility.EmailRepository.GetEmailInfoById(emailId)
                objEmailInfo.EmailBody = common.MergeCustomerEmailBody(customer, objEmailInfo)
                common.SendCustomerEmail(customer, objEmailInfo)
                Logger.Log.Info(String.Format("SendEmail Executed"))
            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Unable to Send Email -", ex.Message)
                Logger.Log.Error(String.Format("Unable to Send Email to CustomerID - {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2} ", customer.CustomerId, ex.Message, ex.StackTrace))
            Finally
                Logger.Log.Info(String.Format("SendEmail Execution Ended"))
            End Try
        End Sub
        Private Sub NewUserSignUp(User As User, emailId As Integer)
            Try
                Dim PlanGuruUrl As String = "http://" + Request.Url.Host + If(Request.Url.IsDefaultPort, "", ":" + Request.Url.Port.ToString() + "/")
                Dim objEmailInfo = utility.EmailRepository.GetEmailInfoById(emailId)
                objEmailInfo.EmailBody = common.MergeUserEmailBody(User, objEmailInfo, PlanGuruUrl)
                common.SendUserEmail(User, objEmailInfo)
                Logger.Log.Info(String.Format("Email Sent Successfully to User: {0} for EmailId: {1}", User.UserId, emailId))
            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Unable to Send Email -", ex.Message)
                Logger.Log.Error(String.Format("Unable to Send Email, UserId - {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2} ", User.UserId, ex.Message, ex.StackTrace))
            Finally
                Logger.Log.Info(String.Format("SendEmail Execution Ended"))
            End Try
        End Sub
        Private Sub SendEmail(customer As Customer, emailId As Integer)
            'Dim PlanGuruUrl As String = "http://" + Request.Url.Host + If(Request.Url.IsDefaultPort, "", ":" + Request.Url.Port.ToString() + "/")
            Dim objEmailInfo = utility.EmailRepository.GetEmailInfoById(emailId)
            objEmailInfo.EmailBody = common.MergeCustomerEmailBody(customer, objEmailInfo)
            common.SendCustomerEmail(customer, objEmailInfo)
        End Sub
        Private Sub SendEmail(User As User, emailId As Integer)
            Dim PlanGuruUrl As String = "http://" + Request.Url.Host + If(Request.Url.IsDefaultPort, "", ":" + Request.Url.Port.ToString() + "/")
            Dim objEmailInfo = utility.EmailRepository.GetEmailInfoById(emailId)
            objEmailInfo.EmailBody = common.MergeUserEmailBody(User, objEmailInfo, PlanGuruUrl)
            common.SendUserEmail(User, objEmailInfo)
        End Sub
        Private Sub DeleteUserEmail(User As User, emailId As Integer)
            Try
                Dim PlanGuruUrl As String = "http://" + Request.Url.Host + If(Request.Url.IsDefaultPort, "", ":" + Request.Url.Port.ToString() + "/")
                Dim objEmailInfo = utility.EmailRepository.GetEmailInfoById(emailId)
                objEmailInfo.EmailBody = common.MergeUserEmailBody(User, objEmailInfo, PlanGuruUrl)
                common.SendUserEmail(User, objEmailInfo)
                Logger.Log.Info(String.Format("Email Sent Successfully to User: {0} for EmailId: {1}", User.UserId, emailId))
            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Unable to Send Email -", ex.Message)
                Logger.Log.Error(String.Format("Unable to Send Email, UserId - {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2} ", User.UserId, ex.Message, ex.StackTrace))
            Finally
                Logger.Log.Info(String.Format("SendEmail Execution Ended"))
            End Try
        End Sub
        Function About() As ActionResult
            Return View()
        End Function

        Function Contact() As ActionResult
            ViewData("Message") = "Your contact page."

            Return View()
        End Function

        Private Sub TestforData()
            Logger.Log.Info(String.Format("Inside HomeController TestforData() Started"))
            Try
                Using context = New DataAccess()
                    For Each account As Account In context.Accounts
                        'ViewData("Message") = account.AcctType.ClassDesc
                    Next
                End Using
                Logger.Log.Info(String.Format("Inside HomeController TestforData() Ended"))
            Catch ex As Exception
                Logger.Log.Error(String.Format("Exception occurred inside HomeController for TestforData() with Message- {0} - {1} ", ex.Message, ex.StackTrace))
            Finally
                Logger.Log.Info(String.Format("Inside HomeController TestforData() Finally Ended"))
            End Try
        End Sub

        Private Sub GetAllRecords()
            Logger.Log.Info(String.Format("Inside HomeController GetAllRecords() Started"))
            Try
                Using context As New DataAccess
                    context.AcctTypes.Load()
                    context.Accounts.Load()
                    context.Balances.Load()
                    Dim selectedAccounts = (From a In context.Accounts.Local
                                                Where a.AcctType.TypeDesc = "Revenue" Or a.AcctType.TypeDesc = "Expense" _
                                                Order By a.SortSequence _
                                                Select a)
                    For Each account As Account In selectedAccounts
                        Debug.WriteLine(account.Description)
                    Next

                End Using
                Logger.Log.Info(String.Format("Inside HomeController GetAllRecords() Ended"))
            Catch ex As Exception
                Logger.Log.Error(String.Format("Exception occurred inside HomeController for GetAllRecords() with Message- {0} - {1} ", ex.Message, ex.StackTrace))
            Finally
                Logger.Log.Info(String.Format("Inside HomeController GetAllRecords() Finally Ended"))
            End Try
        End Sub
    End Class
End Namespace