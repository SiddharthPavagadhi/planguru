﻿Imports System.Diagnostics.CodeAnalysis
Imports System.Security.Principal
Imports System.Web.Routing

Imports onlineAnalytics
Imports System.Collections
Imports System.Configuration
Imports System.Linq
Imports System.Data.Entity
Imports PagedList
Imports System.Web.Mvc
Imports onlineAnalytics.Common
Imports Recurly

Namespace onlineAnalytics

    <Authorize()>
    Public Class UserController
        Inherits System.Web.Mvc.Controller

        Private utility As New Utility()
        Private userRepository As IUserRepository
        Private common As New Common()
        Dim UserType As UserRole
        Dim UserInfo As User
        Dim CustomerId As String = Nothing

        Public Sub New()
            Me.userRepository = New UserRepository(New DataAccess())
        End Sub

        Public Sub New(userRepository As IUserRepository)
            Me.userRepository = userRepository
        End Sub

        Function Index() As ViewResult
            Try
                If (Not Session("UserType") Is Nothing) And (Not Session("UserInfo") Is Nothing) Then
                    UserType = DirectCast(Session("UserType"), UserRole)
                    UserInfo = DirectCast(Session("UserInfo"), User)
                    CustomerId = If(UserType.UserRoleId = UserRoles.SAU, UserInfo.CustomerId, Nothing)
                End If

                Dim users = From usr In userRepository.GetUsers()

                If (Not CustomerId Is Nothing) Then
                    users = users.Where(Function(c) c.CustomerId = CustomerId)
                End If

                Logger.Log.Info(String.Format("User Loaded Successfully"))
                Return View(users.ToPagedList(1, 20))
            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Unable to Load Users-", ex.Message)
                Logger.Log.Error(String.Format("Unable to Load Users Message- {0} " + Environment.NewLine + "Stack Trace: {1} ", ex.Message, ex.StackTrace))
                Return View()
            Finally
                Logger.Log.Info(String.Format("User Index Function Execution Ended"))
            End Try
            Return View()
        End Function

        Function Login(returnUrl As String) As ActionResult
            ViewBag.ReturnUrl = returnUrl
            Return View()
        End Function

        Function Authenticate(userName As String, password As String, returnUrl As String) As JsonResult
            Dim data As New Hashtable()
            Try


                If (AuthenticateUser(userName, password)) Then

                    Logger.Log.Info(String.Format("Successfully Login"))

                    data.Add("Success", "Authenticated")
                    data.Add("Url", Url.Action("Index", "Dashboard").ToString())

                Else
                    data.Add("Error", TempData("ErrorMessage").ToString())
                End If

            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Unable to Login with User-", ex.Message)
                Logger.Log.Error(String.Format("Unable to Login with Message- {0} " + Environment.NewLine + "Stack Trace: {1} ", ex.Message, ex.StackTrace))
                data.Add("Error", ex.Message.ToString())
            Finally
                Logger.Log.Info(String.Format("Login Function Execution Ended"))
            End Try

            Return Json(data, JsonRequestBehavior.AllowGet)
        End Function

        <HttpPost()>
        Function Login(model As LoginModel, returnUrl As String) As ActionResult
            Try
                If ModelState.IsValid Then
                    'Check user credentials
                    If (AuthenticateUser(model.UserName, model.Password)) Then

                        Logger.Log.Info(String.Format("Successfully Login"))

                        Return RedirectToAction("Index", "Dashboard")

                    End If

                End If

            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Unable to Login with User-", ex.Message)
                Logger.Log.Error(String.Format("Unable to Login with Message- {0} " + Environment.NewLine + "Stack Trace: {1} ", ex.Message, ex.StackTrace))

            Finally
                Logger.Log.Info(String.Format("Login Function Execution Ended"))
            End Try

            Return View()
        End Function

        Private Function AuthenticateUser(userName As String, password As String) As Boolean

            Dim flag As Boolean = False

            Dim result = utility.UserRepository.ValidateUserAccount(userName, common.Encrypt(password))

            If (Not (result Is Nothing)) Then

                If (result.Status <> StatusE.Active) Then
                    TempData("ErrorMessage") = "Your account has not been activated. <br /> Please click on the link, send to your email-id. <br /> Or Contact Administrator."
                    Return flag
                End If

                Dim userRole = utility.UserRoleRepository.GetByID(result.UserRoleId)
                Dim state As String = [Enum].GetName(GetType(StatusE), StatusE.Active)
                If (result.UserName.Equals("Administrator", StringComparison.OrdinalIgnoreCase) Or result.UserName.Equals("SupportAdmin", StringComparison.OrdinalIgnoreCase)) Then

                    'Dim companyAnalysisList As Hashtable = utility.PopulateCompanyAnalysisList()
                    'Session("UserCompanies") = companyAnalysisList.Item("CompanyList")
                    'Session("CompanyAnalyses") = companyAnalysisList.Item("AnalysisList")
                    Session("UserInfo") = result
                    Session("UserType") = userRole
                    GetUserRolePermission(userRole)
                    flag = True
                    Return flag
                End If

                Dim account As RecurlyAccount = If(userRole.UserRoleId = UserRoles.SAU, RecurlyAccount.Get(result.CustomerId), RecurlyAccount.Get(result.UserId))

                If ((account.Closed = False) And (account.State.Equals(state, StringComparison.OrdinalIgnoreCase))) Then

                    Session("UserInfo") = result
                    Session("UserType") = userRole

                    'Dim companyAnalysisList As Hashtable = utility.PopulateCompanyAnalysisList()
                    'Session("UserCompanies") = companyAnalysisList.Item("CompanyList")
                    'Session("CompanyAnalyses") = companyAnalysisList.Item("AnalysisList")

                    If (userRole.UserRoleId = UserRoles.SAU) Then

                        Dim customerInfo = utility.CustomerRepository.GetCustomerById(result.CustomerId)
                        Session("CustomerInfo") = customerInfo

                    ElseIf (userRole.UserRoleId = UserRoles.CAU) Then

                        Dim companyInfo = utility.CompaniesRepository.GetCompanies().Where(Function(c) c.CustomerId = result.CustomerId).FirstOrDefault()
                        Session("CompanyInfo") = companyInfo.CompanyId

                    ElseIf (userRole.UserRoleId = UserRoles.CRU) Then

                        Dim cru_Information = utility.UserRepository.GetUsers().Where(Function(u) u.CustomerId = result.CustomerId).FirstOrDefault()

                    End If

                    GetUserRolePermission(userRole)

                    flag = True

                Else
                    TempData("ErrorMessage") = "Your account has been " + account.State + " by the administrator."
                End If
            Else
                TempData("ErrorMessage") = "The Username or Password you entered is incorrect."
            End If

            Return flag
        End Function

        Private Sub GetUserRolePermission(userRole As UserRole)

            Dim userRolePermission = From urp In utility.UserRolePermissionRepository.GetUserRolePermission()
                                     Join urpm In utility.UserRolePermissionMappingRepository.GetUserRolePermissionMapping()
                                     On urpm.UserRolePermissionId Equals urp.UserRolePermissionId Where urpm.UserRoleId = userRole.UserRoleId
                                     Order By urp.UserRolePermissionId Select urp

            Dim UserAccess As New UserAccess()

            For Each itemPermission In userRolePermission

                Select Case itemPermission.Description.ToString()
                    Case "Subscription Management"
                        UserAccess.SubscriptionManagement = "block"
                    Case "User Management"
                        UserAccess.UserManagement = "block"
                    Case "Search Customer"
                        UserAccess.SearchCustomer = "block"
                    Case "View Subscription"
                        UserAccess.ViewSubscription = "block"
                    Case "Add Subscription"
                        UserAccess.AddSubscription = "block"
                    Case "Update Subscription"
                        UserAccess.UpdateSubscription = "block"
                    Case "Cancel Subscription"
                        UserAccess.CancelSubscription = "block"
                    Case "Add User"
                        UserAccess.AddUser = "block"
                    Case "Delete User"
                        UserAccess.DeleteUser = "block"
                    Case "View User"
                        UserAccess.ViewUser = "block"
                    Case "Update User"
                        UserAccess.UpdateUser = "block"
                    Case "Add Company"
                        UserAccess.AddCompany = "block"
                    Case "Delete Company"
                        UserAccess.DeleteCompany = "block"
                    Case "View Companies"
                        UserAccess.ViewCompanies = "block"
                    Case "Update Company"
                        UserAccess.UpdateCompany = "block"
                    Case "User Company Mapping"
                        UserAccess.UserCompanyMapping = "block"
                    Case "Add Analysis"
                        UserAccess.AddAnalysis = "block"
                    Case "Delete Analysis"
                        UserAccess.DeleteAnalysis = "block"
                    Case "View Analyses"
                        UserAccess.ViewAnalyses = "block"
                    Case "Update Analysis"
                        UserAccess.UpdateAnalysis = "block"
                    Case "User Analysis Mapping"
                        UserAccess.UserAnalysisMapping = "block"
                    Case "View Analysis Info"
                        UserAccess.ViewAnalysisInfo = "block"
                    Case "Print Analysis Info"
                        UserAccess.PrintAnalysisInfo = "block"
                    Case "Modify Dashboard"
                        UserAccess.ModifyDashboard = "block"
                End Select
            Next

            Session("UserAccess") = UserAccess

        End Sub

        <AcceptVerbs(HttpVerbs.Get)> _
        Public Function SubscriptionCost(CustomerId As String) As JsonResult

            Dim data As New Hashtable()
            If Not String.IsNullOrEmpty(CustomerId) Then
                data = GetSubscriptionCost(CustomerId, data)
            End If

            Return Json(data, JsonRequestBehavior.AllowGet)
        End Function

        Private Function GetSubscriptionCost(CustomerId As String, data As Hashtable) As Hashtable

            If Not String.IsNullOrEmpty(CustomerId) Then

                Dim subscription = RecurlySubscription.Get(CustomerId)
                Dim Quantity As Integer
                Dim AmountInCents As Double
                Dim TotalAmountInCents As Double

                If (subscription.AddOn.Addons.Count = 0) And (Not String.IsNullOrEmpty(utility.RecurlyPlan)) Then

                    Dim plan = RecurlyPlan.Get(utility.RecurlyPlan)
                    If (plan.AddOn.Addons.Count > 0) Then
                        Quantity = plan.AddOn.Addons(0).quantity
                        AmountInCents = plan.AddOn.Addons(0).unit_amount_in_cents / 100
                        TotalAmountInCents = (Quantity * AmountInCents)

                        data.Add("AddOnAmountInCents", AmountInCents.ToString("N2"))
                        Session("NewUserAddon") = DirectCast(plan.AddOn.Addons(0), RecurlyAddOn)
                    End If
                Else

                    AmountInCents = subscription.AddOn.Addons(0).unit_amount_in_cents / 100
                    Quantity = subscription.AddOn.Addons(0).quantity + 1
                    TotalAmountInCents = (Quantity * AmountInCents)

                    data.Add("AddOnAmountInCents", AmountInCents.ToString("N2"))
                    Session("NewUserAddon") = DirectCast(subscription.AddOn.Addons(0), RecurlyAddOn)


                End If

                data.Add("Subsciption_UnitAmountInCents", TotalAmountInCents + (subscription.UnitAmountInCents / 100))

                ViewBag.Subsciption_UnitAmountInCents = TotalAmountInCents + (subscription.UnitAmountInCents / 100)
            End If

            Return data
        End Function

        ' This CreateUser Function allow user to enter required information to Create User.      
        Function CreateUser() As ActionResult

            PopulateCustomerList()
            PopulateUserRoleList()
            Return View()

        End Function

        Private Sub SendEmail(User As User, emailId As Integer)
            Try
                Dim PlanGuruUrl As String = "http://" + Request.Url.Host + If(Request.Url.IsDefaultPort, "", ":" + Request.Url.Port.ToString() + "/")
                Dim objEmailInfo = utility.EmailRepository.GetEmailInfoById(emailId)
                objEmailInfo.EmailBody = common.MergeUserEmailBody(User, objEmailInfo, PlanGuruUrl)
                common.SendUserEmail(User, objEmailInfo)
                Logger.Log.Info(String.Format("Email Sent Successfully to User: {0} for EmailId: {1}", User.UserId, emailId))
            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Unable to Send Email -", ex.Message)
                Logger.Log.Error(String.Format("Unable to Send Email, UserId - {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2} ", User.UserId, ex.Message, ex.StackTrace))
            Finally
                Logger.Log.Info(String.Format("SendEmail Execution Ended"))
            End Try
        End Sub

        <HttpPost()> _
        Function CreateUser(User As User) As ActionResult
            Dim newUserAccountCode As String = Guid.NewGuid.ToString()
            Dim customerInfo As Customer
            Try
                If ModelState.IsValid Then
                    Dim subscription = RecurlySubscription.Get(User.CustomerId)
                    If Not (subscription Is Nothing) Then
                        Dim newRecurlyAccount = New RecurlyAccount(newUserAccountCode)
                        Dim Addon = DirectCast(Session("NewUserAddon"), RecurlyAddOn)
                        newRecurlyAccount.Username = User.UserName
                        newRecurlyAccount.FirstName = User.FirstName
                        newRecurlyAccount.LastName = User.LastName
                        newRecurlyAccount.Email = User.UserEmail
                        newRecurlyAccount.Create()
                        subscription.AddOns = New List(Of RecurlyAddOn)
                        If (subscription.AddOn.Addons.Count > 0) Then
                            Addon.quantity += 1
                        End If
                        subscription.AddOns.Add(Addon)
                        subscription.ChangeSubscription(RecurlySubscription.ChangeTimeframe.Now)
                        customerInfo = utility.CustomerRepository.GetCustomerById(User.CustomerId)
                        customerInfo.Quantity = subscription.Quantity
                        utility.CustomerRepository.UpdateCustomer(customerInfo)
                        utility.Save()
                        User.UserId = newUserAccountCode
                        User.CreatedBy = 1
                        User.UpdatedBy = 1
                        User.Status = CType(StatusE.Pending, Integer)
                        'Generate random password while creating new account
                        'and send mail to the user to reset it.
                        Dim password As String = common.Generate(8, 2)
                        User.Password = common.Encrypt(password)
                        User.SecurityKey = Guid.NewGuid().ToString()
                        Using dataAccess As New DataAccess()
                            dataAccess.Users.Add(User)
                            dataAccess.SaveChanges()
                            TempData("Message") = "User created successfully."
                        End Using

                        SendEmail(User, EmailType.NewUserAdded)

                        Return RedirectToAction("Index")
                    End If
                End If
                PopulateCustomerList(User.CustomerId)
                PopulateUserRoleList(User.UserRoleId)
                Logger.Log.Info(String.Format("CreateUser Successfully Executed"))
            Catch dataEx As DataException
                TempData("ErrorMessage") = String.Concat("Unable to create user account -", dataEx.Message)
                Logger.Log.Error(String.Format("Unable to create user account, UserId - {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2} ", newUserAccountCode, dataEx.Message, dataEx.StackTrace))
            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Unable to create user account -", ex.Message)
                Logger.Log.Error(String.Format("Unable to create user account, UserId - {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2} ", newUserAccountCode, ex.Message, ex.StackTrace))
            Finally
                Logger.Log.Info(String.Format("CreateUser Execution Ended"))
            End Try
            Return View(User)
        End Function

        <HttpPost()>
        Function Edit(UserDetails As User) As ActionResult
            Logger.Log.Info(String.Format("\n Company Update started {0}", UserDetails.UserId))
            Try
                If ModelState.IsValid Then

                    Dim accountInfo = RecurlyAccount.Get(UserDetails.UserId)

                    accountInfo.Username = UserDetails.UserName
                    accountInfo.FirstName = UserDetails.FirstName
                    accountInfo.LastName = UserDetails.LastName
                    accountInfo.Email = UserDetails.UserEmail
                    accountInfo.Update()

                    UserDetails.CreatedBy = 1
                    UserDetails.UpdatedBy = 1
                    userRepository.UpdateUser(UserDetails)
                    userRepository.Save()

                    TempData("Message") = "User updated successfully."
                    Logger.Log.Info(String.Format("User Updated successfully with id {0}", UserDetails.UserId))
                    Return RedirectToAction("Index")
                End If
            Catch dataEx As DataException
                TempData("ErrorMessage") = String.Concat("Unable to update User-", dataEx.Message)
                Logger.Log.Error(String.Format("\n Unable to Update User id- {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2} ", UserDetails.UserId, dataEx.Message, dataEx.StackTrace))
            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Unable to update User-", ex.Message)
                Logger.Log.Error(String.Format("\n Unable to Update User id- {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2} ", UserDetails.UserId, ex.Message, ex.StackTrace))
            Finally
                Logger.Log.Info(String.Format("Execution Ended"))
            End Try

            PopulateCustomerList(UserDetails.CustomerId)
            PopulateUserRoleList(UserDetails.UserRoleId)
            Return View(UserDetails)
        End Function

        Public Function Edit(userAccountCode As String) As ActionResult
            Try
                Dim userInfo = userRepository.GetUserById(userAccountCode)
                PopulateCustomerList()
                PopulateUserRoleList()
                Return View(userInfo)
            Catch dataEx As DataException
                TempData("ErrorMessage") = String.Concat("Unable to view user details to Edit User Information - ", dataEx.Message)
                Logger.Log.Error(String.Format("\n Unable to view user details to Edit User Information, UserID - {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2} ", userAccountCode, dataEx.Message, dataEx.StackTrace))
            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Unable to view user details to Edit User Information - ", ex.Message)
                Logger.Log.Error(String.Format("\n Unable to Edit company id- {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2} ", userAccountCode, ex.Message, ex.StackTrace))
            Finally
                Logger.Log.Info(String.Format("Edit User Execution Ended"))
            End Try
            Return Nothing
        End Function

        Private Sub PopulateCustomerList(Optional SelectedCustomer As Object = Nothing)
            Logger.Log.Info(String.Format("Populate Company List  started "))
            Try
                If (Not Session("UserType") Is Nothing) And (Not Session("UserInfo") Is Nothing) Then
                    UserType = DirectCast(Session("UserType"), UserRole)
                    UserInfo = DirectCast(Session("UserInfo"), User)
                    CustomerId = If(UserType.UserRoleId = UserRoles.SAU, UserInfo.CustomerId, Nothing)
                End If

                Dim customers = utility.CustomerRepository.Get(filter:=Function(q) (q.Quantity > 0), orderBy:=Function(q) q.OrderBy(Function(d) d.CustomerLastName))

                If (Not CustomerId Is Nothing) Then
                    customers = customers.Where(Function(c) c.CustomerId = CustomerId)
                End If


                ViewBag.SelectedCustomer = New SelectList(customers, "CustomerId", "CustomerFullName", SelectedCustomer)
                Logger.Log.Info(String.Format("Populate Company List successfully Loaded"))
            Catch dataEx As DataException
                TempData("ErrorMessage") = String.Concat("Unable to Populate Company List -", dataEx.Message)
                Logger.Log.Error(String.Format("Unable to Populate Company List with Message- {0} " + Environment.NewLine + "Stack Trace: {1} ", dataEx.Message, dataEx.StackTrace))
                Throw
            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Unable to Populate Company List-", ex.Message)
                Logger.Log.Error(String.Format("Unable to populate company list with Message- {0} " + Environment.NewLine + "Stack Trace: {1}", ex.Message, ex.StackTrace))
                Throw
            Finally
                Logger.Log.Info(String.Format("Execution Ended"))
            End Try
        End Sub

        Public Function Delete(userAccountCode As String) As ActionResult
            Logger.Log.Info(String.Format("User Account Deletion Started {0}", userAccountCode))
            Try
                Dim state As String = [Enum].GetName(GetType(StatusE), StatusE.Active)
                'Get Subscription AccountCode from UserInfo table from local database.
                Dim localDBUserInfo = utility.UserRepository.GetUserByID(userAccountCode)
                'Get Recurly Subscription Information using Subscription AccountCode.
                Dim recurlySubscriptionInfo = RecurlySubscription.Get(localDBUserInfo.CustomerId)

                'Get Customer Information from Local Database.
                'Dim customerInfo = utility.CustomerRepository.GetCustomerById(localDBUserInfo.CustomerId)
                If (recurlySubscriptionInfo.State.Equals(state, StringComparison.OrdinalIgnoreCase)) Then
                    'To do Add-On functionality
                    'Decrease quantity on recurly subscription
                    If (recurlySubscriptionInfo.AddOn.Addons.Count > 0) Then


                        Dim addOn As New RecurlyAddOn()
                        addOn = recurlySubscriptionInfo.AddOn.Addons(0)

                        'Check if quantity , if quantity is equal to 1 then Addon update is not required.
                        'It will automatically remove the last addon.
                        If (addOn.quantity > 1) Then
                            recurlySubscriptionInfo.AddOns = New List(Of RecurlyAddOn)
                            addOn.quantity -= 1
                            recurlySubscriptionInfo.AddOns.Add(addOn)
                        End If

                        recurlySubscriptionInfo.ChangeSubscription(RecurlySubscription.ChangeTimeframe.Now)

                    End If

                    'utility.CustomerRepository.UpdateCustomer(customerInfo)
                    'utility.Save()

                    Logger.Log.Info(String.Format("Update Quantity successfully of Customer in local database using UserId - {0}", userAccountCode))

                    'Closing account on recurly
                    RecurlyAccount.CloseAccount(userAccountCode)
                    Logger.Log.Info(String.Format("Close Account successfully on RECURLY using UserId - {0}", userAccountCode))
                    utility.UserRepository.DeleteUser(userAccountCode)
                    utility.UserRepository.Save()
                    SendEmail(localDBUserInfo, EmailType.UserDeleted)
                    Logger.Log.Info(String.Format("Delete Account successfully on local database using UserId - {0}", userAccountCode))
                End If
                TempData("Message") = "User account deleted successfully."
                Logger.Log.Info(String.Format("User account deleted successfully using  {0}", userAccountCode))
            Catch dataEx As DataException
                TempData("ErrorMessage") = String.Concat("Unable to delete User - ", dataEx.Message)
                Logger.Log.Error(String.Format(Environment.NewLine + " Unable to Delete User id- {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2}", userAccountCode, dataEx.Message, dataEx.StackTrace))
                Return RedirectToAction("Index", New System.Web.Routing.RouteValueDictionary() _
                                        From {{"id", userAccountCode}, {"deleteChangesError", True}})
            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Unable to delete User-", ex.Message)
                Logger.Log.Error(String.Format(" Unable to Delete User id- {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2} ", userAccountCode, ex.Message, ex.StackTrace))
            Finally
                Logger.Log.Info(String.Format("Execution Ended"))
            End Try
            Return RedirectToAction("Index")
        End Function

        Private Sub PopulateUserRoleList(Optional SelectedUserRole As Object = Nothing)
            Logger.Log.Info(String.Format("Populate Company List  started "))
            Try
                Dim userroles = utility.UserRoleRepository.Get(filter:=Function(q) (q.UserRoleId > 3), orderBy:=Function(q) q.OrderBy(Function(d) d.RoleName))
                ViewBag.SelectedRoles = New SelectList(userroles, "UserRoleId", "RoleName", SelectedUserRole)
                Logger.Log.Info(String.Format("Populate Company List successfully Loaded"))
            Catch dataEx As DataException
                TempData("ErrorMessage") = String.Concat("Unable to Populate Company List -", dataEx.Message)
                Logger.Log.Error(String.Format("Unable to Populate Company List with Message- {0} " + Environment.NewLine + "Stack Trace: {1} ", dataEx.Message, dataEx.StackTrace))
                Throw
            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Unable to Populate Company List-", ex.Message)
                Logger.Log.Error(String.Format("Unable to populate company list with Message- {0} " + Environment.NewLine + "Stack Trace: {1}", ex.Message, ex.StackTrace))
                Throw
            Finally
                Logger.Log.Info(String.Format("Populate User Role List Execution Ended"))
            End Try
        End Sub

        Function Verification(id As String, securitykey As String) As ActionResult
            Dim user As User
            Dim ResetPassword As New ResetPassword
            Try
                user = userRepository.GetUserById(id)
                Dim str As String = securitykey

                If (user Is Nothing) Then
                    TempData("ErrorMessage") = String.Concat("It seems this user does not exist, Contact Admin.")
                    Return View()
                End If

                If (user.SecurityKey = securitykey) Then
                    ResetPassword.UserId = user.UserId
                    Return View(ResetPassword)
                Else
                    TempData("ErrorMessage") = String.Concat("SecurityToken does not match, Contact Admin.")
                    Return View()
                End If

            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Unable to Process Reset Password -", ex.Message)
                Logger.Log.Error(String.Format("Unable to Process Reset Password of User Id - {0} with Message - {1} " + Environment.NewLine + "Stack Trace: {1}", id, ex.Message, ex.StackTrace))
                Return View()
            Finally
                Logger.Log.Info(String.Format("Verifiacation Function Execution Ended"))
            End Try

        End Function

        <HttpPost()> _
        Function Verification(User As ResetPassword) As ActionResult
            Try

                If ModelState.IsValid Then
                    Dim usr = userRepository.GetUserById(User.UserId)
                    If (usr.Password = common.Encrypt(User.CurrentPassword)) Then
                        If (User.NewPassword.Equals(User.ConfirmNewPassword)) Then
                            usr.Password = common.Encrypt(User.NewPassword)
                            usr.Status = CType(StatusE.Active, Integer)
                            userRepository.UpdateUser(usr)
                            userRepository.Save()
                            SendEmail(usr, EmailType.PasswordReset)
                        Else
                            TempData("ErrorMessage") = String.Concat("New & Confirm Password does not match.")
                            Return View()
                        End If
                    Else
                        TempData("ErrorMessage") = String.Concat("Old password does not match.")
                        Return View()
                    End If
                    TempData("Message") = "Password Reset Successfully."
                    Return RedirectToAction("Index")
                End If

                Return View(User)
            Catch ex As Exception
                TempData("ErrorMessage") = "Error in Password Reset."
                Logger.Log.Error(String.Format("Unable to Process Reset Password of User Id - {0} with Message - {1} " + Environment.NewLine + "Stack Trace: {1}", User.UserId, ex.Message, ex.StackTrace))
                Return View()
            Finally
                Logger.Log.Info(String.Format("Verification Function Ended"))
            End Try
        End Function

        <HttpPost()>
        Function EditProfile(User As ResetPassword) As ActionResult

            Logger.Log.Info(String.Format("\n User Profile update started {0}", User.UserId))
            Try

                If (ModelState.IsValid) Then

                    If (ValidateChangePassword(User) = False) Then
                        Return View(User)
                    End If

                    Dim errorMessage As String = String.Empty
                    Dim usr = userRepository.GetUserById(User.UserId)

                    If (User.ChangePassword = True) And Not (User.CurrentPassword Is Nothing And User.NewPassword Is Nothing And User.ConfirmNewPassword Is Nothing) Then
                        'ValidateUserInput(User)
                        If (usr.Password = common.Encrypt(User.CurrentPassword)) Then
                            If (User.NewPassword.Equals(User.ConfirmNewPassword)) Then
                                usr.Password = common.Encrypt(User.NewPassword)
                                'userRepository.UpdateUser(usr)
                                'userRepository.Save()
                            Else
                                TempData("ErrorMessage") = String.Concat("New & Confirm Password does not match.")
                                Return View()
                            End If
                        Else
                            TempData("ErrorMessage") = String.Concat("Old password does not match.")
                            Return View(User)
                        End If
                    End If

                    usr.UserName = User.UserName
                    usr.FirstName = User.FirstName
                    usr.LastName = User.LastName

                    userRepository.UpdateUser(usr)
                    userRepository.Save()

                    Session("UserInfo") = usr

                    TempData("Message") = "Profile Edited Successfully."
                    Return RedirectToAction("Index")
                End If

            Catch dataEx As DataException
                TempData("ErrorMessage") = String.Concat("Unable to update User-", dataEx.Message)
                Logger.Log.Error(String.Format("\n Unable to Update User id- {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2} ", User.UserId, dataEx.Message, dataEx.StackTrace))
            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Unable to update User-", ex.Message)
                Logger.Log.Error(String.Format("\n Unable to Update User id- {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2} ", User.UserId, ex.Message, ex.StackTrace))
            Finally
                Logger.Log.Info(String.Format("Execution Ended"))
            End Try

            Return View(User)
        End Function

        Public Function EditProfile() As ActionResult
            Dim UserId = String.Empty
            Dim User As New ResetPassword()
            Try
                If Not (Session("UserInfo") Is Nothing) Then

                    UserInfo = DirectCast(Session("UserInfo"), User)
                    UserInfo = utility.UserRepository.GetUserByID(UserInfo.UserId)

                    UserId = User.UserId = UserInfo.UserId
                    User.UserName = UserInfo.UserName
                    User.FirstName = UserInfo.FirstName
                    User.LastName = UserInfo.LastName
                    User.CurrentPassword = UserInfo.Password
                    User.UserId = UserInfo.UserId

                Else
                    'To do : Session timeout 
                End If

                Return View(User)
            Catch dataEx As DataException
                TempData("ErrorMessage") = String.Concat("Unable to view user details to Edit User Information - ", dataEx.Message)
                Logger.Log.Error(String.Format("\n Unable to view user details to Edit User Information, UserID - {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2} ", UserId, dataEx.Message, dataEx.StackTrace))
            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Unable to view user details to Edit User Information - ", ex.Message)
                Logger.Log.Error(String.Format("\n Unable to Edit company id- {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2} ", UserId, ex.Message, ex.StackTrace))
            Finally
                Logger.Log.Info(String.Format("Edit User Execution Ended"))
            End Try
            Return Nothing
        End Function

        Private Function ValidateChangePassword(User As ResetPassword)

            Dim Flag = True
            If (User.ChangePassword = True) And (User.CurrentPassword Is Nothing) Then
                ModelState.AddModelError("CurrentPassword", "Current Password is required.")
                Flag = False
            ElseIf (User.ChangePassword = True) And (User.NewPassword Is Nothing) Then
                ModelState.AddModelError("NewPassword", "New Password is required.")
                Flag = False
            ElseIf (User.ChangePassword = True) And (User.ConfirmNewPassword Is Nothing) Then
                ModelState.AddModelError("ConfirmNewPassword", "Confirm Password is required.")
                Flag = False
            ElseIf (User.ChangePassword = True) And (Not (User.NewPassword Is Nothing And User.ConfirmNewPassword Is Nothing)) Then

                If (Not User.NewPassword.Equals(User.ConfirmNewPassword)) Then
                    'ModelState.AddModelError("ConfirmNewPassword", "Confirm New Password and New Password do not match.")
                    Flag = False
                End If

            End If

            Return Flag
        End Function

        Function Logout()

            Session.Clear()
            Session.Abandon()

            Return RedirectToAction("Login", "User")
        End Function
    End Class
End Namespace
