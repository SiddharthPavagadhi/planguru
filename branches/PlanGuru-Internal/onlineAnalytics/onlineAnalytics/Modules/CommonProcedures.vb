﻿Imports FarPoint.Mvc.Spread
Imports System.Data.Entity
Public Module CommonProcedures
    Public Function GetFormat(intCtr As String, strStmt As String) As String
        Select Case intCtr
            Case 0
                GetFormat = "Budget vs Actual"
            Case 1
                GetFormat = "Budget vs Actual YTD"
            Case 2
                GetFormat = "This period vs last period"
            Case 3
                GetFormat = "This period vs same period prior year"
            Case 4
                GetFormat = "This period vs same period prior year YTD"
            Case 5
                GetFormat = "Current year trend"
            Case 6
                GetFormat = "Trend for the last 12 months"
            Case 7
                GetFormat = "Multi-year trend"
            Case 8
                GetFormat = "Year to date multi-year trend"
            Case Else
                GetFormat = ""
        End Select
    End Function

    Public Function GetMonth(intMonth As Integer) As String
        Select Case intMonth
            Case 0
                GetMonth = "January"
            Case 1
                GetMonth = "February"
            Case 2
                GetMonth = "March"
            Case 3
                GetMonth = "April"
            Case 4
                GetMonth = "May"
            Case 5
                GetMonth = "June"
            Case 6
                GetMonth = "July"
            Case 7
                GetMonth = "August"
            Case 8
                GetMonth = "September"
            Case 9
                GetMonth = "October"
            Case 10
                GetMonth = "November"
            Case 11
                GetMonth = "December"
            Case Else
                GetMonth = "Total"
        End Select
    End Function
    Public Function GetChartDesc(intCtr As Integer) As String
        GetChartDesc = ""
        Select Case intCtr
            Case 0
                GetChartDesc = "None"
            Case 1
                GetChartDesc = "Bar"
            Case 2
                GetChartDesc = "Stacked bar"
            Case 3
                GetChartDesc = "Line"
            Case 4
                GetChartDesc = "Area"
            Case 5
                GetChartDesc = "Pie"
        End Select

    End Function

    Public Sub BuildColsArray(intFormat As Integer, intPeriod As Integer, ByVal strYearType() As String, ByVal strPeriods() As String)

        Select Case intFormat
            Case 0, 1 'budget vs actual
                strYearType(0) = "CB"
                strYearType(1) = "CA"
            Case 2
                strYearType(0) = "H1"
                strYearType(1) = "CA"
            Case 3, 4
                strYearType(0) = "H1"
                strYearType(1) = "CA"
            Case 5 'Current year trend
                strYearType(0) = "CA"
            Case 6 ' Trend for the last 12 months
                strYearType(0) = "H1"
                strYearType(1) = "CA"
            Case 7, 8 'Multi-year trend for the selected period
                strYearType(0) = "H5"
                strYearType(1) = "H4"
                strYearType(2) = "H3"
                strYearType(3) = "H2"
                strYearType(4) = "H1"
                strYearType(5) = "CA"
        End Select

    End Sub
    Public Function CheckboxClicked() As String
        CheckboxClicked = "var spread = document.getElementById('spdAnalytics');" _
                        & "var rc = spread.GetRowCount();" _
                        & "var count = 0;" _
                        & "for (var i = 0; i < rc - 1; i++) {" _
                            & "var cellval = spread.GetValue(i,1);" _
                            & "if (cellval == 'true') {" _
                                & "count++;" _
                            & "};" _
                        & "};" _
                        & "if (count == 1) {" _
                            & "var selectedindex = $('#intChartType').get(0).selectedIndex;" _
                            & "if (selectedindex == 3) {" _
                                & "$('#ShowTrend').show();" _
                            & "}" _
                            & "$('#UpdateChart').show();" _
                        & "}" _
                        & "else if (count > 1) {" _
                            & "$('#ShowTrend').hide();" _
                             & "$('#UpdateChart').show();" _
                        & "}" _
                        & "else {" _
                            & "$('#ShowTrend').hide();" _
                             & "$('#UpdateChart').hide();" _
                        & "}"

    End Function
    Public Function DSChkBoxClicked() As String
        DSChkBoxClicked = "var spread = document.getElementById('spdRE');" _
                        & "var rc = spread.GetRowCount();" _
                        & "var count = 0;" _
                        & "for (var i = 0; i < rc - 1; i++) {" _
                            & "var cellval = spread.GetValue(i,1);" _
                            & "if (cellval == 'true') {" _
                                & "count++;" _
                                & "if (count > 1) {" _
                                    & "alert('Only one item can be included in a dashboard chart');" _
                                    & "spread.SetValue(i,1,false);" _
                                    & "break;" _
                                & "}" _
                            & "};" _
                        & "};"
 

    End Function

    Public Sub AddTotalRow(ByRef spdAnalytics As FpSpread, ByVal intFormat As Integer, ByVal intNumberCols As Integer, ByVal intFirstNumCol As Integer, ByVal intColCount As Integer, ByVal decIncTotalAccum() As Decimal, ByRef intRowCtr As Integer, ByVal strRowType As String, ByVal strDesc As String, ByVal intPeriod As Integer)
        Dim intColCtr As Integer

        intRowCtr += 1
        spdAnalytics.Sheets(0).AddRows(intRowCtr, 1)
        spdAnalytics.Sheets(0).Rows(intRowCtr).Height = 18
        spdAnalytics.Sheets(0).SetValue(intRowCtr, 3, strDesc)
        spdAnalytics.Sheets(0).Cells(intRowCtr, 3).Font.Bold = True
        spdAnalytics.Sheets(0).Cells(intRowCtr, 3).Font.Size = FontSize.Large
        spdAnalytics.Sheets(0).SetValue(intRowCtr, 2, strRowType)
        For intColCtr = intFirstNumCol To intColCount - 1
            Select Case intFormat
                Case 0 To 4
                    Select Case intColCtr
                        Case 4, 5
                            spdAnalytics.Sheets(0).SetValue(intRowCtr, intColCtr, decIncTotalAccum(intColCtr - intFirstNumCol))
                        Case 6
                            spdAnalytics.Sheets(0).SetValue(intRowCtr, intColCtr, decIncTotalAccum(0) - decIncTotalAccum(1))
                        Case 7
                            Try
                                spdAnalytics.Sheets(0).SetValue(intRowCtr, intColCtr, (decIncTotalAccum(0) - decIncTotalAccum(1)) / decIncTotalAccum(0))
                            Catch ex As Exception

                            End Try
                    End Select
                Case 6
                    If intPeriod < 11 Then
                        If (intColCtr - intFirstNumCol) < (11 - intPeriod) Then
                            spdAnalytics.Sheets(0).SetValue(intRowCtr, intColCtr, decIncTotalAccum((intColCtr - intFirstNumCol) + intPeriod + 1))
                        Else
                            If (intColCtr - intFirstNumCol) = 12 Then
                                spdAnalytics.Sheets(0).SetValue(intRowCtr, intColCtr, decIncTotalAccum(intColCtr - intFirstNumCol))
                            Else
                                spdAnalytics.Sheets(0).SetValue(intRowCtr, intColCtr, decIncTotalAccum((intColCtr - intFirstNumCol) - (11 - intPeriod)))
                            End If
                        End If
                    Else
                        spdAnalytics.Sheets(0).SetValue(intRowCtr, intColCtr, decIncTotalAccum(intColCtr))
                    End If
                Case Else
                    spdAnalytics.Sheets(0).SetValue(intRowCtr, intColCtr, decIncTotalAccum(intColCtr - intFirstNumCol))
            End Select
            spdAnalytics.Sheets(0).Cells(intRowCtr, intColCtr).Border.BorderStyleTop = BorderStyle.Double
            spdAnalytics.Sheets(0).Cells(intRowCtr, intColCtr).Border.BorderColorTop = Drawing.Color.Gray
            spdAnalytics.Sheets(0).Rows(intRowCtr).Height = 18
        Next


    End Sub
    Public Sub AddRow(ByRef spdAnalytics As FpSpread, ByRef intRowCtr As Integer, ByVal strAcctDesc As String, ByVal strAcctDescriptor As String, ByVal strAcctType As String)
        intRowCtr += 1
        spdAnalytics.Sheets(0).AddRows(intRowCtr, 1)
        spdAnalytics.Sheets(0).Rows(intRowCtr).Height = 18
        spdAnalytics.Sheets(0).SetValue(intRowCtr, 3, strAcctDesc)
        spdAnalytics.Sheets(0).SetValue(intRowCtr, 2, strAcctDescriptor)
        strAcctType = strAcctType
    End Sub
    Public Sub FormatHeaderRow(ByRef spdAnalytics As FpSpread, ByVal intRowCtr As Integer, ByRef intSpanRows() As Integer, ByRef blnTotalFlag As Boolean)
        spdAnalytics.Sheets(0).Cells(intRowCtr, 0).Font.Bold = True
        spdAnalytics.Sheets(0).Cells(intRowCtr, 0).Font.Size = FontSize.Large
        spdAnalytics.Sheets(0).Cells(intRowCtr, 0).HorizontalAlign = HorizontalAlign.Center
        spdAnalytics.Sheets(0).Cells(intRowCtr, 0).Border.BorderColorBottom = Drawing.Color.White
        spdAnalytics.Sheets(0).Cells(intRowCtr, 3).Font.Bold = True
        spdAnalytics.Sheets(0).Cells(intRowCtr, 3).Font.Size = FontSize.Large
        spdAnalytics.Sheets(0).SetValue(intRowCtr, 0, "+")
        spdAnalytics.Sheets(0).AddSpanCell(intRowCtr, 3, 1, spdAnalytics.Sheets(0).ColumnCount - 3)
        intSpanRows(0) = intRowCtr
        blnTotalFlag = False
    End Sub
    Public Sub FormatTotalRow(ByRef spdAnalytics As FpSpread, ByVal intRowCtr As Integer, ByRef intSpanRows() As Integer, ByRef blnTotalFlag As Boolean, ByVal intFirstNumCol As Integer, ByVal intNumberCols As Integer, ByVal strClassDesc As String)
        If intFirstNumCol <> 0 Then
            For intColCtr = intFirstNumCol To spdAnalytics.Sheets(0).ColumnCount - 1
                spdAnalytics.Sheets(0).Cells(intRowCtr - 1, intColCtr).Border.BorderStyleBottom = BorderStyle.Double
                spdAnalytics.Sheets(0).Cells(intRowCtr - 1, intColCtr).Border.BorderColorBottom = Drawing.Color.Gray
            Next
        End If
        spdAnalytics.Sheets(0).Cells(intRowCtr, 0).Font.Bold = True
        spdAnalytics.Sheets(0).Cells(intRowCtr, 0).Font.Size = FontSize.Large
        spdAnalytics.Sheets(0).Cells(intRowCtr, 0).HorizontalAlign = HorizontalAlign.Center
        spdAnalytics.Sheets(0).Cells(intRowCtr, 3).Font.Bold = True
        spdAnalytics.Sheets(0).Cells(intRowCtr, 3).Font.Size = FontSize.Large
        spdAnalytics.Sheets(0).SetValue(intRowCtr, 0, "+")
        intSpanRows(1) = intRowCtr
        Select Case strClassDesc
            Case "Cost of sales"
                blnTotalFlag = True
        End Select
    End Sub
    Public Sub AccumRunningTotals(ByVal intFormat As Integer, ByRef decAccumTotals() As Decimal, ByVal intPeriod As Integer, ByVal decPeriodAmts() As Decimal, intIndex As Integer)
        Dim intColCtr As Integer

        Select Case intFormat
            Case 0, 3, 7
                decAccumTotals(intIndex) += decPeriodAmts(intPeriod)
            Case 1, 4, 8
                For intColCtr = 0 To intPeriod
                    decAccumTotals(intIndex) += decPeriodAmts(intColCtr)
                Next
            Case 2
                Select Case intIndex
                    Case 0
                        If intPeriod = 0 Then
                            decAccumTotals(intIndex) += decPeriodAmts(11)
                        End If
                    Case Else
                        If intPeriod = 0 Then
                            decAccumTotals(1) += decPeriodAmts(intPeriod)
                        Else
                            decAccumTotals(0) += decPeriodAmts(intPeriod - 1)
                            decAccumTotals(1) += decPeriodAmts(intPeriod)
                        End If
                End Select

        End Select
    End Sub
    Public Sub ComputeVariances(ByVal decSaveAmts() As Decimal, strAcctType As String, ByRef decAccumTotals() As Decimal, ByRef blnShowasPercent As Boolean)

        decSaveAmts(0) = Math.Round(decSaveAmts(0), 4)
        decSaveAmts(1) = Math.Round(decSaveAmts(1), 4)
        decSaveAmts(2) = decSaveAmts(0) - decSaveAmts(1)
        If decSaveAmts(0) <> 0 Then
            decSaveAmts(3) = decSaveAmts(2) / decSaveAmts(0)
        Else
            decSaveAmts(3) = 0
        End If
        Select Case strAcctType
            Case "Revenue"
                decSaveAmts(2) = decSaveAmts(2) * -1
                decSaveAmts(3) = decSaveAmts(3) * -1
        End Select
    End Sub
    Public Sub SaveBalances(ByVal balance As Balance, ByRef decPeriodAmts() As Decimal)
        Array.Clear(decPeriodAmts, 0, 11)
        decPeriodAmts(0) = balance.Balance1
        decPeriodAmts(1) = balance.Balance2
        decPeriodAmts(2) = balance.Balance3
        decPeriodAmts(3) = balance.Balance4
        decPeriodAmts(4) = balance.Balance5
        decPeriodAmts(5) = balance.Balance6
        decPeriodAmts(6) = balance.Balance7
        decPeriodAmts(7) = balance.Balance8
        decPeriodAmts(8) = balance.Balance9
        decPeriodAmts(9) = balance.Balance10
        decPeriodAmts(10) = balance.Balance11
        decPeriodAmts(11) = balance.Balance12
    End Sub
End Module
