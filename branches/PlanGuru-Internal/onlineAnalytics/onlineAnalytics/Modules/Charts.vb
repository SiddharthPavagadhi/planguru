﻿Imports FarPoint.Mvc.Spread
Module Charts
    Public Sub BuildChart(ByVal spdChart As FpSpread, ByRef spdAnalytics As FpSpread, ByRef intChartType As Integer, ByRef intFormat As Integer, ByRef intFirstNumbCol As Integer, ByVal blnShowasPerc As Boolean, ByVal blnShowTrend As Boolean)
        Dim intSelectedRows As New ArrayList
        Dim intRowCtr As Integer
        Dim intColCtr As Integer
        Dim intStartCol As Integer
        Dim intNumbofCols As Integer
        Dim strSave As String
        Dim decChartValues(,) As Decimal

        spdChart.BorderColor = Drawing.Color.White
        'spdChart.BorderStyle = BorderStyle.None
        'spdChart.BorderWidth = System.Web.UI.WebControls.Unit.Pixel(0)
        'spdChart.BorderColor = Drawing.Color.Transparent
        spdChart.CommandBar.Visible = False
        spdChart.Sheets(0).RowCount = 1
        spdChart.Sheets(0).ColumnCount = 1
        'spdChart.Sheets(0).Cells(0, 0).Border.BorderColorLeft = Drawing.Color.Aqua
        spdChart.VerticalScrollBarPolicy = FarPoint.Web.Spread.ScrollBarPolicy.Never
        spdChart.HorizontalScrollBarPolicy = FarPoint.Web.Spread.ScrollBarPolicy.Never
        spdChart.Width = spdAnalytics.Width
        Try
            spdChart.Sheets(0).Charts.Remove(spdChart.Sheets(0).Charts(0))
        Catch ex As Exception

        End Try
        'get selected rows
        For intRowCtr = 0 To spdAnalytics.Sheets(0).RowCount - 1
            strSave = Convert.ToString(spdAnalytics.GetEditValue(intRowCtr, 1))
            If strSave = "True" Then
                intSelectedRows.Add(intRowCtr)
            End If
        Next
        'store values to chart
        Select Case intFormat
            Case 0 To 4
                ReDim decChartValues(intSelectedRows.Count - 1, 1)
                intNumbofCols = 2
                intStartCol = intFirstNumbCol
            Case 5, 6
                intNumbofCols = spdAnalytics.Sheets(0).ColumnCount - intFirstNumbCol - 1
                ReDim decChartValues(intSelectedRows.Count - 1, (spdAnalytics.Sheets(0).ColumnCount - 1) - intFirstNumbCol)
                intStartCol = intFirstNumbCol
            Case Else
                For intColCtr = intFirstNumbCol To spdAnalytics.Sheets(0).ColumnCount - 1
                    If spdAnalytics.Columns(intColCtr).Visible = True Then
                        intStartCol = intColCtr
                        Exit For
                    End If
                Next
                intNumbofCols = spdAnalytics.Sheets(0).ColumnCount - intColCtr
                ReDim decChartValues(intSelectedRows.Count - 1, intNumbofCols - 1)
        End Select
        For intRowCtr = 0 To intSelectedRows.Count - 1
            For intColCtr = 0 To intNumbofCols - 1
                Dim decSaveVal As Decimal = spdAnalytics.Sheets(0).GetValue(intSelectedRows(intRowCtr), intStartCol + intColCtr)
                If blnShowasPerc = True Then
                    decSaveVal = decSaveVal * 100
                End If
                decChartValues(intRowCtr, intColCtr) = decSaveVal
            Next
        Next

        Select Case intChartType
            Case 2, 3 'bar
                BuildBarChart(intSelectedRows, spdAnalytics, spdChart, intChartType, intNumbofCols, decChartValues, intFormat)
            Case 4  'Line
                If blnShowTrend = False Then
                    BuildLineChart(intSelectedRows, spdAnalytics, spdChart, intChartType, intNumbofCols, decChartValues, intFormat)
                Else
                    BuildLinewTrendChart(intSelectedRows, spdAnalytics, spdChart, intNumbofCols, decChartValues, intFormat, intStartCol, blnShowasPerc)
                End If
            Case 5  'Stacked line
                BuildAreaChart(intSelectedRows, spdAnalytics, spdChart, intNumbofCols, decChartValues, intFormat)
            Case 6  'Pie


        End Select

    End Sub

    Private Sub BuildBarChart(ByRef intSelectedRows As ArrayList, ByVal spdAnalytics As FpSpread, ByVal spdChart As FpSpread, ByRef intChartType As Integer, ByRef intNumbofCols As Integer, ByRef decChartValues(,) As Decimal, ByRef intFormat As Integer)

        Dim intRow As Integer = intSelectedRows(0)
        Dim intCol As Integer = 8
        Dim intColCtr As Integer
        Dim intRowCount As Integer = 1
        Dim intColCount As Integer = 2
        Dim intSelRowCtr As Integer
        Dim cseries As New FarPoint.Web.Chart.ClusteredBarSeries
        Dim sseries As New FarPoint.Web.Chart.StackedBarSeries
        Dim plotArea As New FarPoint.Web.Chart.YPlotArea()
        '    Dim yseries As New FarPoint.Web.Chart.XYPointSeries

        Dim chart As New FarPoint.Web.Spread.Chart.SpreadChart()
        Dim model As New FarPoint.Web.Chart.ChartModel()

        'set title for chart
        If intSelectedRows.Count = 1 Then
            Dim label As New FarPoint.Web.Chart.LabelArea
            label.Text = spdAnalytics.Sheets(0).GetValue(intSelectedRows(0), 3)
            label.Location = New System.Drawing.PointF(0.5F, 0.0F)
            Dim LabFont As System.Drawing.Font
            LabFont = New System.Drawing.Font("Arial", 10)
            label.TextFont = LabFont
            model.LabelAreas.Add(label)
        Else
            'set properties for legend
            Dim legend As New FarPoint.Web.Chart.LegendArea
            Dim LegFont As System.Drawing.Font
            Select Case intFormat
                Case 0 To 4
                    legend.Location = New System.Drawing.PointF(0.75F, 0.2F)
                Case 5, 6
                    legend.Location = New System.Drawing.PointF(0.8F, 0.2F)
                Case Else
                    legend.Location = New System.Drawing.PointF(0.75F, 0.2F)
            End Select

            LegFont = New System.Drawing.Font("Arial", 8)
            legend.TextFont = LegFont
            model.LegendAreas.Add(legend)
            chart.Model.LegendAreas.Add(legend)
        End If
        For intSelRowCtr = 0 To intSelectedRows.Count - 1
            Dim aSeries(intSelRowCtr) As FarPoint.Web.Chart.BarSeries
            aSeries(intSelRowCtr) = New FarPoint.Web.Chart.BarSeries
            Select Case intSelRowCtr
                Case 0
                    aSeries(intSelRowCtr).BarFill = New FarPoint.Web.Chart.SolidFill(System.Drawing.Color.LightGreen)
                Case 1
                    aSeries(intSelRowCtr).BarFill = New FarPoint.Web.Chart.SolidFill(System.Drawing.Color.LightCoral)
            End Select
            With aSeries(intSelRowCtr)
                .SeriesName = spdAnalytics.Sheets(0).GetValue(intSelectedRows(intSelRowCtr), 3)
                For intColCtr = 0 To intNumbofCols - 1
                    .Values.Add(decChartValues(intSelRowCtr, intColCtr))
                    If intSelRowCtr = 0 Then
                        .CategoryNames.Add(spdAnalytics.Sheets(0).ColumnHeader.Cells(1, 4 + intColCtr).Text)
                    End If
                Next
            End With
            Select Case intChartType
                Case 2
                    cseries.Series.Add(aSeries(intSelRowCtr))
                Case 3
                    sseries.Series.Add(aSeries(intSelRowCtr))
            End Select
        Next
        'set properties of plot area
        plotArea.Location = New System.Drawing.PointF(0.125F, 0.2F)
        If intSelectedRows.Count = 1 Then
            plotArea.Size = New System.Drawing.SizeF(0.8F, 0.6F)
        Else
            Select Case intFormat
                Case 0 To 4
                    plotArea.Size = New System.Drawing.SizeF(0.6F, 0.6F)
                Case 5, 6
                    plotArea.Location = New System.Drawing.PointF(0.05F, 0.2F)
                    plotArea.Size = New System.Drawing.SizeF(0.75F, 0.6F)
                Case Else
                    plotArea.Size = New System.Drawing.SizeF(0.6F, 0.6F)
            End Select

        End If
        Select Case intChartType
            Case 2
                plotArea.Series.Add(cseries)
            Case 3
                plotArea.Series.Add(sseries)
        End Select
        model.PlotAreas.Add(plotArea)
        chart.Model = model
        chart.Width = spdAnalytics.Width
        chart.Height = 200
        chart.Left = 0
        chart.Top = 0
        chart.BorderColor = Drawing.Color.White
        chart.CanMove = False
        chart.CanSelect = False
        spdChart.Sheets(0).Charts.Add(chart)


    End Sub

    Private Sub BuildLineChart(ByRef intSelectedRows As ArrayList, ByVal spdAnalytics As FpSpread, ByVal spdChart As FpSpread, ByRef intChartType As Integer, ByRef intNumbofCols As Integer, ByRef decChartValues(,) As Decimal, ByRef intFormat As Integer)

        Dim intRow As Integer = intSelectedRows(0)
        Dim intCol As Integer = 8
        Dim intColCtr As Integer
        Dim intRowCount As Integer = 1
        Dim intColCount As Integer = 2
        Dim intSelRowCtr As Integer
        '' Dim cseries As New FarPoint.Web.Chart.cl
        Dim sseries As New FarPoint.Web.Chart.StackedLineSeries
        Dim areaseries As New FarPoint.Web.Chart.StackedAreaSeries
        Dim plotArea As New FarPoint.Web.Chart.YPlotArea()
        Dim blnSmoothed As Boolean = True

        Dim chart As New FarPoint.Web.Spread.Chart.SpreadChart()
        Dim model As New FarPoint.Web.Chart.ChartModel()

        'set title for chart
        If intSelectedRows.Count = 1 Then
            Dim label As New FarPoint.Web.Chart.LabelArea
            label.Text = spdAnalytics.Sheets(0).GetValue(intSelectedRows(0), 3)
            label.Location = New System.Drawing.PointF(0.4F, 0.0F)
            Dim LabFont As System.Drawing.Font
            LabFont = New System.Drawing.Font("Arial", 10)
            label.TextFont = LabFont
            model.LabelAreas.Add(label)
        Else
            'set properties for legend
            Dim legend As New FarPoint.Web.Chart.LegendArea
            Dim LegFont As System.Drawing.Font
            Select Case intFormat
                Case 0 To 4
                    legend.Location = New System.Drawing.PointF(0.75F, 0.2F)
                Case 5, 6
                    legend.Location = New System.Drawing.PointF(0.8F, 0.2F)
                Case Else
                    legend.Location = New System.Drawing.PointF(0.75F, 0.2F)
            End Select

            LegFont = New System.Drawing.Font("Arial", 8)
            legend.TextFont = LegFont
            model.LegendAreas.Add(legend)
            chart.Model.LegendAreas.Add(legend)
        End If
        For intSelRowCtr = 0 To intSelectedRows.Count - 1
            Dim aSeries(intSelRowCtr) As FarPoint.Web.Chart.LineSeries
            aSeries(intSelRowCtr) = New FarPoint.Web.Chart.LineSeries
            If blnSmoothed = True Then
                aSeries(intSelRowCtr).SmoothedLine = True
            End If
            Select Case intSelRowCtr
                Case 0
                    aSeries(intSelRowCtr).LineBorder = New FarPoint.Web.Chart.SolidLine(Drawing.Color.Blue, 0.5F)
                    aSeries(intSelRowCtr).PointMarker = New FarPoint.Web.Chart.BuiltinMarker(FarPoint.Web.Chart.MarkerShape.Diamond, 3.5F)
                Case 1
                    aSeries(intSelRowCtr).LineBorder = New FarPoint.Web.Chart.SolidLine(Drawing.Color.Red, 0.5F)
                    aSeries(intSelRowCtr).PointMarker = New FarPoint.Web.Chart.BuiltinMarker(FarPoint.Web.Chart.MarkerShape.Square, 3.5F)
            End Select
            With aSeries(intSelRowCtr)
                .SeriesName = spdAnalytics.Sheets(0).GetValue(intSelectedRows(intSelRowCtr), 3)
                For intColCtr = 0 To intNumbofCols - 1
                    .Values.Add(decChartValues(intSelRowCtr, intColCtr))
                    If intSelRowCtr = 0 Then
                        .CategoryNames.Add(spdAnalytics.Sheets(0).ColumnHeader.Cells(1, 4 + intColCtr).Text)
                    End If
                Next
            End With
            sseries.Series.Add(aSeries(intSelRowCtr))

        Next

        'set properties of plot area
        plotArea.Location = New System.Drawing.PointF(0.125F, 0.2F)
        If intSelectedRows.Count = 1 Then
            plotArea.Size = New System.Drawing.SizeF(0.8F, 0.6F)
        Else
            Select Case intFormat
                Case 0 To 4
                    plotArea.Size = New System.Drawing.SizeF(0.6F, 0.6F)
                Case 5, 6
                    plotArea.Location = New System.Drawing.PointF(0.05F, 0.2F)
                    plotArea.Size = New System.Drawing.SizeF(0.75F, 0.6F)
                Case Else
                    plotArea.Size = New System.Drawing.SizeF(0.6F, 0.6F)
            End Select

        End If
        plotArea.Series.Add(sseries)
        model.PlotAreas.Add(plotArea)
        chart.Model = model
        chart.Width = spdAnalytics.Width
        chart.Height = 200
        chart.Left = 0
        chart.Top = 0
        chart.BorderColor = Drawing.Color.White
        chart.CanMove = False
        chart.CanSelect = False
        spdChart.Sheets(0).Charts.Add(chart)


    End Sub
    Private Sub BuildLinewTrendChart(ByRef intSelectedRows As ArrayList, ByVal spdAnalytics As FpSpread, ByVal spdChart As FpSpread, ByRef intNumbofCols As Integer, ByVal decChartValues(,) As Decimal, ByVal intFormat As Integer, ByVal intFirstNumCol As Integer, ByVal blnShowasPerc As Boolean)

        Dim intRow As Integer = intSelectedRows(0)
        Dim intCol As Integer = 8
        Dim intColCtr As Integer
        Dim intRowCount As Integer = 1
        Dim intColCount As Integer = 2
        Dim intSelRowCtr As Integer
        Dim decIntercept As Decimal = 0
        Dim decSlope As Decimal = 0
        Dim dblCoeff As Double = 0
        Dim blnSmoothed As Boolean = True



        '' Dim cseries As New FarPoint.Web.Chart.cl
        Dim sseries As New FarPoint.Web.Chart.LineSeries
        Dim tseries As New FarPoint.Web.Chart.LineSeries
        Dim plotArea As New FarPoint.Web.Chart.YPlotArea()

        Dim chart As New FarPoint.Web.Spread.Chart.SpreadChart()
        Dim model As New FarPoint.Web.Chart.ChartModel()

        'set title for chart
        ' If intSelectedRows.Count = 1 Then
        Dim label As New FarPoint.Web.Chart.LabelArea
        If blnShowasPerc = False Then
            label.Text = spdAnalytics.Sheets(0).GetValue(intSelectedRows(0), 3)
        Else
            label.Text = spdAnalytics.Sheets(0).GetValue(intSelectedRows(0), 3) & " as a % of total revenue"
        End If
        label.Location = New System.Drawing.PointF(0.4F, 0.0F)
        Dim LabFont As System.Drawing.Font
        LabFont = New System.Drawing.Font("Arial", 10)
        label.TextFont = LabFont
        model.LabelAreas.Add(label)
        If blnSmoothed = True Then
            sseries.SmoothedLine = True
        End If 
        tseries.PointMarker = New FarPoint.Web.Chart.NoMarker()
        sseries.PointMarker = New FarPoint.Web.Chart.BuiltinMarker(FarPoint.Web.Chart.MarkerShape.Circle, 4.0F)
        sseries.LineBorder = New FarPoint.Web.Chart.SolidLine(Drawing.Color.Blue, 0.5F)
        tseries.LineBorder = New FarPoint.Web.Chart.SolidLine(Drawing.Color.Red, 0.5F)
        'set properties for legend
        Dim legend As New FarPoint.Web.Chart.LegendArea
        Dim LegFont As System.Drawing.Font
        Select Case intFormat
            Case 0 To 4
                legend.Location = New System.Drawing.PointF(0.77F, 0.2F)
            Case 5, 6
                legend.Location = New System.Drawing.PointF(0.82F, 0.2F)
            Case Else
                legend.Location = New System.Drawing.PointF(0.77F, 0.2F)
        End Select

        LegFont = New System.Drawing.Font("Arial", 8)
        legend.TextFont = LegFont
        model.LegendAreas.Add(legend)
        chart.Model.LegendAreas.Add(legend)
        With sseries
            .SeriesName = "Actual"
            For intColCtr = 0 To intNumbofCols - 1
                .Values.Add(decChartValues(intSelRowCtr, intColCtr))
                If intSelRowCtr = 0 Then
                    .CategoryNames.Add(spdAnalytics.Sheets(0).ColumnHeader.Cells(1, intFirstNumCol + intColCtr).Text)
                End If
            Next
        End With
        'get trend amounts
        ComputeTrend(decIntercept, decSlope, dblCoeff, decChartValues, intNumbofCols)
        With tseries
            .SeriesName = "Trend"
            For intColCtr = 0 To intNumbofCols - 1
                .Values.Add(decIntercept + (intColCtr * decSlope))
            Next
        End With
       
        Dim slope As New FarPoint.Web.Chart.LabelArea
        Dim strText As String
        If blnShowasPerc = False Then
            strText = "Trend - Increasing at rate" & vbCrLf & "of " & Math.Round(decSlope, 0)
            Select Case intFormat
                Case 0, 2, 3, 5, 5
                    strText = strText & " per month"
                Case Else
                    strText = strText & " per year"
            End Select
        Else
            strText = "Trend - Increasing at rate" & vbCrLf & "of " & Math.Round(decSlope * 100, 2)
            Select Case intFormat
                Case 0, 2, 3, 5, 5
                    strText = strText & "% per month"
                Case Else
                    strText = strText & "% per year"
            End Select
        End If
        slope.Text = strText
        Select Case intFormat
            Case 0 To 4
                slope.Location = New System.Drawing.PointF(0.77F, 0.6F)
            Case 5, 6
                slope.Location = New System.Drawing.PointF(0.82F, 0.6F)
            Case Else
                slope.Location = New System.Drawing.PointF(0.77F, 0.6F)
        End Select
        LabFont = New System.Drawing.Font("Arial", 8)
        slope.TextFont = LabFont
        model.LabelAreas.Add(slope)
        'set properties of plot area
        plotArea.Location = New System.Drawing.PointF(0.125F, 0.2F)
        Select Case intFormat
            Case 0 To 4
                plotArea.Size = New System.Drawing.SizeF(0.6F, 0.6F)
            Case 5, 6
                plotArea.Location = New System.Drawing.PointF(0.05F, 0.2F)
                plotArea.Size = New System.Drawing.SizeF(0.75F, 0.6F)
            Case Else
                plotArea.Size = New System.Drawing.SizeF(0.6F, 0.6F)
        End Select
        plotArea.Series.Add(sseries)
        plotArea.Series.Add(tseries)
        model.PlotAreas.Add(plotArea)
        chart.Model = model
        chart.Width = spdAnalytics.Width
        chart.Height = 200
        chart.Left = 0
        chart.Top = 0
        chart.BorderColor = Drawing.Color.White
        chart.CanMove = False
        chart.CanSelect = False
        spdChart.Sheets(0).Charts.Add(chart)


    End Sub
    Private Sub BuildAreaChart(ByRef intSelectedRows As ArrayList, ByVal spdAnalytics As FpSpread, ByVal spdChart As FpSpread, ByRef intNumbofCols As Integer, ByRef decChartValues(,) As Decimal, ByRef intFormat As Integer)

        Dim intRow As Integer = intSelectedRows(0)
        Dim intCol As Integer = 8
        Dim intColCtr As Integer
        Dim intRowCount As Integer = 1
        Dim intColCount As Integer = 2
        Dim intSelRowCtr As Integer
        Dim areaseries As New FarPoint.Web.Chart.StackedAreaSeries
        Dim plotArea As New FarPoint.Web.Chart.YPlotArea()
        Dim chart As New FarPoint.Web.Spread.Chart.SpreadChart()
        Dim model As New FarPoint.Web.Chart.ChartModel()

        'set title for chart
        If intSelectedRows.Count = 1 Then
            Dim label As New FarPoint.Web.Chart.LabelArea
            label.Text = spdAnalytics.Sheets(0).GetValue(intSelectedRows(0), 2)
            label.Location = New System.Drawing.PointF(0.5F, 0.0F)
            Dim LabFont As System.Drawing.Font
            LabFont = New System.Drawing.Font("Arial", 10)
            label.TextFont = LabFont
            model.LabelAreas.Add(label)
        Else
            'set properties for legend
            Dim legend As New FarPoint.Web.Chart.LegendArea
            Dim LegFont As System.Drawing.Font
            Select Case intFormat
                Case 0 To 4
                    legend.Location = New System.Drawing.PointF(0.75F, 0.2F)
                Case 5, 6
                    legend.Location = New System.Drawing.PointF(0.8F, 0.2F)
                Case Else
                    legend.Location = New System.Drawing.PointF(0.75F, 0.2F)
            End Select

            LegFont = New System.Drawing.Font("Arial", 8)
            legend.TextFont = LegFont
            model.LegendAreas.Add(legend)
            chart.Model.LegendAreas.Add(legend)
        End If
        For intSelRowCtr = 0 To intSelectedRows.Count - 1
            Dim aSeries(intSelRowCtr) As FarPoint.Web.Chart.AreaSeries
            aSeries(intSelRowCtr) = New FarPoint.Web.Chart.AreaSeries


            'Select Case intSelRowCtr
            '    Case 0
            '        aSeries(intSelRowCtr).BarFill = New FarPoint.Web.Chart.SolidFill(System.Drawing.Color.LightGreen)
            '    Case 1
            '        aSeries(intSelRowCtr).BarFill = New FarPoint.Web.Chart.SolidFill(System.Drawing.Color.LightCoral)
            'End Select


            With aSeries(intSelRowCtr)
                .SeriesName = spdAnalytics.Sheets(0).GetValue(intSelectedRows(intSelRowCtr), 2)
                For intColCtr = 0 To intNumbofCols - 1
                    .Values.Add(decChartValues(intSelRowCtr, intColCtr))
                    If intSelRowCtr = 0 Then
                        .CategoryNames.Add(spdAnalytics.Sheets(0).ColumnHeader.Cells(1, 4 + intColCtr).Text)
                    End If
                Next
            End With
            areaseries.Series.Add(aSeries(intSelRowCtr))
        Next
        'set properties of plot area
        plotArea.Location = New System.Drawing.PointF(0.125F, 0.2F)
        If intSelectedRows.Count = 1 Then
            plotArea.Size = New System.Drawing.SizeF(0.8F, 0.6F)
        Else
            Select Case intFormat
                Case 0 To 4
                    plotArea.Size = New System.Drawing.SizeF(0.6F, 0.6F)
                Case 5, 6
                    plotArea.Location = New System.Drawing.PointF(0.05F, 0.2F)
                    plotArea.Size = New System.Drawing.SizeF(0.75F, 0.6F)
                Case Else
                    plotArea.Size = New System.Drawing.SizeF(0.6F, 0.6F)
            End Select

        End If
        plotArea.Series.Add(areaseries)
        model.PlotAreas.Add(plotArea)
        chart.Model = model
        chart.Width = spdAnalytics.Width
        chart.Height = 200
        chart.Left = 0
        chart.Top = 0
        chart.BorderColor = Drawing.Color.White
        chart.CanMove = False
        chart.CanSelect = False
        spdChart.Sheets(0).Charts.Add(chart)


    End Sub

    Private Sub ComputeTrend(ByRef decIntercept As Decimal, ByRef decSlope As Decimal, ByRef dblCoeff As Double, ByVal decValues(,) As Decimal, intNumbColCount As Integer)
        Dim intDataPoints As Integer = intNumbColCount
        Dim XFactors(intNumbColCount - 1) As Decimal
        Dim YFactors(intNumbColCount - 1) As Decimal
        Dim intCtr As Integer
        Dim sx, sy, sxy, sx2, sy2, XX, YY, temp As Single

        'load values into arrays
        For intCtr = 0 To intNumbColCount - 1
            XFactors(intCtr) = intCtr
            YFactors(intCtr) = decValues(0, intCtr)
        Next
        'compute slope, intercept and coeff
        For intCtr = 0 To intDataPoints - 1
            XX = XFactors(intCtr)
            YY = YFactors(intCtr)
            sx += XX
            sy += YY
            sxy = sxy + XX * YY
            sx2 = sx2 + XX * XX
            sy2 = sy2 + YY * YY
        Next
        If (intDataPoints * sx2 - sx * sx) <> 0 Then
            decSlope = (intDataPoints * sxy - sx * sy) / (intDataPoints * sx2 - sx * sx)
        Else
            decSlope = 0
        End If
        If intDataPoints <> 0 Then
            decIntercept = (sy - decSlope * sx) / intDataPoints
        Else
            decIntercept = 0
        End If
        temp = (intDataPoints * sx2 - sx * sx) * (intDataPoints * sy2 - sy * sy)
        If temp <> 0 Then
            dblCoeff = (intDataPoints * sxy - sx * sy) / Math.Sqrt(temp)
        End If

    End Sub



End Module
