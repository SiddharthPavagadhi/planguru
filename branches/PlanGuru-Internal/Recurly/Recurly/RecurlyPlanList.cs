﻿namespace Recurly
{
    using System;
    using System.Collections.Generic;
    using System.Net;
    using System.Xml;

    public class RecurlyPlanList : List<RecurlyPlan>
    {
        private const string UrlPrefix = "/company/plans/";

        internal RecurlyPlanList()
        {
        }

        public static RecurlyPlan[] GetPlans()
        {
            RecurlyPlanList list = new RecurlyPlanList();
            if (RecurlyClient.PerformRequest(RecurlyClient.HttpRequestMethod.Get, "/company/plans/", new RecurlyClient.ReadXmlDelegate(list.ReadXml)) == HttpStatusCode.NotFound)
            {
                return null;
            }
            return list.ToArray();
        }

        internal void ReadXml(XmlTextReader reader)
        {
            while (reader.Read())
            {
                if ((reader.Name == "plan") && (reader.NodeType == XmlNodeType.EndElement))
                {
                    break;
                }
                if (reader.NodeType == XmlNodeType.Element)
                {
                    string name = reader.Name;
                    if ((name != null) && (name == "plan"))
                    {
                        base.Add(new RecurlyPlan(reader));
                    }
                }
            }
        }
    }
}

