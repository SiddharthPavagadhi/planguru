﻿@Imports  onlineAnalytics
<!DOCTYPE html>
<html>
<head>
    <!-- to get the absolute path for the images into alert used inside the jquery.msgBox.js file-->
    <script type="text/javascript" language="javascript">
        var isSessionAliveCalled = 'false';
        var msgBoxImagePath = '@Url.Content("~/Content/MsgBoxImages/")';                
    </script>
    <title>@ViewData("Title")</title>
    <link href="@Url.Content("~/Content/jquery-ui-1.10.0.custom.css")" rel="stylesheet" type="text/css" />
    <link href="@Url.Content("~/Content/css/styles.css")" rel="stylesheet" type="text/css" />
    <link href="@Url.Content("~/Content/msgBoxLight.css")" rel="stylesheet" type="text/css" />
    <script src="@Url.Content("~/Scripts/jquery-1.9.1.min.js")" type="text/javascript"></script>   
     <script src="@Url.Content("~/Scripts/jquery-ui-1.10.0.custom.js")" type="text/javascript"></script>
    <script src="@Url.Content("~/Scripts/dropdown.js")" type="text/javascript"></script>
    <script src="@Url.Content("~/Scripts/jquery.maskedinput.min.js")" type="text/javascript"></script>
    <script src="@Url.Content("~/Scripts/jquery.msgBox.js")" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        window.ViewerControlId = "";
    </script>
</head>
<body>
    @Code
        Dim UserInfo As New User()
        Dim UserName As String = String.Empty
        Dim ua As New UserAccess
        Dim Administrator As Boolean = False
        If Not (Session("UserInfo") Is Nothing) Then
            UserInfo = DirectCast(Session("UserInfo"), User)
            UserName = String.Concat("Welcome <b><a href=", Url.Action("EditProfile", "User").ToString(), ">", UserInfo.FirstName, " ", UserInfo.LastName, "</a>", "</b>")
            
            If (UserInfo.UserRoleId = UserRoles.PGAAdmin Or UserInfo.UserRoleId = UserRoles.PGASupport) Then
                Administrator = True
            End If
           
        End If
            
        If Not (Session("UserAccess") Is Nothing) Then
            ua = DirectCast(Session("UserAccess"), UserAccess)
        End If
    End code
    <div class="wrapper">
        <div class="main-container">
            <!--Header Container
-->
            <div class="header">
                <div class="logo-panel">
                    <a href="#">
                        <img src='@Url.Content("~/Content/Images/planguru_logo.png")'
alt="Planguru Analytics" /></a></div>
                <div class="header-right">
                    <div class="user-panel" style="font-size: 12px;">
                        <div class="welcome-user" >
                            <span>@MvcHtmlString.Create(UserName)</span> <a href='@Url.Action("Logout", "User")' class="logout-section"><img src='@Url.Content("~/Content/Images/Log-out.png")'  /></a>
                        </div>
                    </div>
                    <div class="select-panel topnav-panel">                       
                        <div class="top-nav">
                            <ul>
                                <li class='dropdown-last'><a id="SelectedCompany" href="#"></a>
                                    <div class='top-nav-dropdown-additional'>
                                        <ul id="CompanyId">
                                        </ul>
                                    </div>
                            </li>
                            </ul>
                        </div>
                        <div class="top-nav">
                            <ul>
                                <li class='dropdown-last'><a id="SelectedAnalysis" href="#"></a>
                                    <div class='top-nav-dropdown-additional'>
                                        <ul id="AnalysisId">
                                        </ul>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        @Code
                            If UserInfo.UserRoleId <> UserRoles.CRU Then
                            @<div class="top-nav">
                                <ul>
                                    <li class="dropdown-last" style="padding-right: 0px;"><a href="#">Manage Account</a>
                                        <div class="top-nav-dropdown-additional">
                                            <ul>
                                                @If (ua.ViewCompanies) Then
                                                    @<li><a href='@Url.Action("Index", "Company")'>Companies</a></li>
                                                    End If
                                                @If (ua.ViewAnalyses) Then
                                                    @<li><a href='@Url.Action("Index", "Analysis")'>Analyses</a></li>
                                                    End If
                                                @If (ua.ViewUser) Then
                                                    @<li><a href='@Url.Action("Index", "User")'>Users</a></li>
                                                    End If
                                                @Code
                                                        If (Administrator) Then
                                                    @<li><a href='@Url.Action("Index", "Subscribe")'>Subscription</a></li>
                                                    @<li><a href='@Url.Action("SearchCustomer", "Subscribe")'>Search customers</a></li>
                                                        ElseIf (UserInfo.UserRoleId = UserRoles.SAU) Then
                                                    @<li class="dropdown-last"><a href="#" >Edit Subscription<img style="margin-left:5px;vertical-align:middle;" src="/Content/Images/rightarrow.png" /></a>
                                                            <div class="top-nav-sub-dropdown">
                                                                <ul>
                                                                    <li><a href='@Url.Action("EditSubscription", "Subscribe")'>Change Billing Info</a></li>
                                                                    <li><a href="#" class="CancelSubscription"  data='@Url.Action("CancelSubscription", "Subscribe")'>Cancel Subscription</a></li>
                                                                    <li><a href='@Url.Action("EditProfile", "User")'>Change Other Info</a></li>
                                                                </ul>
                                                            </div>
                                                        </li>                                                                                                                                                             
                                                        End If
                                                End Code
                                            </ul>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            End If
                        End Code
                    </div>
                </div>
            </div>
            <!--Header Container -->
            <!--
Nav Panel -->
            <div class="nav-panel">
                <ul>
                    <li><a href='@Url.Action("Index", "Dashboard")'>Dashboard</a></li>
                    <li><a href='@Url.Action("Index", "RE")'>Revenue &amp; Expenses</a></li>
                    <li><a href='@Url.Action("Index", "ALC")'>Assets, Liabilities &amp; Equity</a></li>
                    <li><a href="#">Cash Flows</a></li>
                    <li><a href="#">Financial Ratios</a></li>
                    <li><a href='@Url.Action("Index", "OM")'>Other Metrics</a></li>
                </ul>
            </div>
            <!-- Nav Panel
-->
            <div class="center-panel">
                <div style="margin: 0px 0px;">
                    @RenderBody()
                </div>
            </div>
        </div>
        <div class="plangurufooter" id="footerpanel" style="font-size: 11px;">
            <div class="footer-panel">
                &copy; Planguru Analytics 2013</div>
        </div>
    </div>
    <div id="loading" style="text-align: center;">
    </div>
    <div id="validation" style="text-align: center;">
    </div>
</body>
</html>
<script language="javascript" type="text/javascript">

    var pathArray = window.location.href.split('/');
    var url;
    if (pathArray.length > 2) {
        var protocol = pathArray[0];
        var host = pathArray[2];
        var app = pathArray[3];
        url = protocol + '//' + host + "/" + app;

    } else {
        var protocol = pathArray[0];
        var host = pathArray[2];
        url = protocol + '//' + host;
    }

    $(".CancelSubscription").click(function (event) {
        if (isSessionAliveCalled == 'false') { IsSessionAlive(); }
        event.preventDefault();
        var url = $(this).attr('data');
       
        $.msgBox({
            title: "Confirm",
            content: "Cancellation of this subscription will close your account, and not able to login again. Are you sure you want to cancel this subscription?",
            type: "confirm",
            buttons: [{ value: "Yes" }, { value: "No"}],
            success: function (result) {                               
                if (result == "Yes") {
                    window.location = url;
                }
            }
        });

    });

    var theDialog = $('#loading').dialog({
        open: function (event, ui) { $(".ui-dialog-titlebar-close").hide(); },
        center_modal: true,
        autoOpen: false,
        modal: true,
        close: false,
        closeOnEscape: false,
        width: 200,
        modal: true,
        minHeight: 10,
        show: 'fade',
        hide: 'fade',
        resizable: 'false'        
    });

    var validationDialog = $('#validation').dialog({
        title: "Login",
        open: function (event, ui) { $(".ui-dialog-titlebar-close").hide(); },
        center_modal: true,
        autoOpen: false,
        modal: true,
        close: true,
        closeOnEscape: true,
        width: 500,
        modal: true,
        minHeight: 10,
        show: 'fade',
        hide: 'fade',
        resizable: 'false',       
        buttons: { "OK": function () {
            $(this).dialog("close");
        }
        }
    });


    function getCompanies(_selectedCompanyId) {

        if (isSessionAliveCalled == 'false') { IsSessionAlive(); } 

        $.ajax({
            type: "GET",
            url: '@Url.Action("GetCompanies", "Dashboard")',
            global: false,
            cache: false,
            data: { SelectedCompany: 0 },
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (msg) {

                if (msg !== undefined && (msg != "")) {

                    if (msg.length > 0) {
                        $("#SelectedCompany").html("Select Company");
                        $("#SelectedCompany").attr("data", 0);
                        _selectedCompanyId = msg[0].Value;
                    }
                    for (var company_index = 0; company_index < msg.length; company_index++) {

                        if (msg[company_index].Selected == true) {
                            $("#SelectedCompany").html(msg[company_index].Text);
                            $("#SelectedCompany").attr("data", msg[company_index].Value);
                            _selectedCompanyId = msg[company_index].Value;
                        }

                        $("#CompanyId").append("<li><a class='companyList' href='#' data=" + msg[company_index].Value + " >" + msg[company_index].Text + "</a></li>");
                    }
                    getAnalyses(_selectedCompanyId);
                    companyListEvent();
                }
                else {
                    $("#SelectedCompany").html("No Company Assigned");
                    $("#SelectedCompany").attr("data", 0);

                    $("#SelectedAnalysis").html("No Analysis Assigned");
                    $("#SelectedAnalysis").attr("data", 0);

                    $("#CompanyId").html("");
                    $("#AnalysisId").html("");
                }

            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                // var msg = JSON.parse(XMLHttpRequest.responseText);
                //theDialog.dialog('close');
            }
        });
    }

    function companyListEvent() {

        if (isSessionAliveCalled == 'false') { IsSessionAlive(); }

        $(".companyList").click(function (event) {

            event.preventDefault();

            _selectedCompanyId = $(this).attr('data');
            _selectedCompany = $(this).text();

            if (_selectedCompany != "" && _selectedCompanyId != "") {
                $("#SelectedCompany").html(_selectedCompany);
                $("#SelectedCompany").attr("data", _selectedCompanyId);
            }

            getAnalyses(_selectedCompanyId, _selectedCompany);

        });
    }

    function getAnalyses(_selectedCompanyId, _selectedCompany) {

        if (isSessionAliveCalled == 'false') { IsSessionAlive(); } 

        $.ajax({
            type: "GET",
            url: '@Url.Action("GetAnalyses", "Dashboard")',
            global: false,
            cache: false,
            data: { SelectedCompany: _selectedCompanyId },
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            beforeSend: function () {
                var imgSrc = '@Url.Content("~/Content/Images/loading.gif")';
                $(".ui-dialog-titlebar").hide();
                $('#loading').html("Please wait.... <img src=" + imgSrc + " />");
                theDialog.dialog('open');
            },
            success: function (msg) {
                theDialog.dialog('close');
                if (msg !== undefined && (msg != "")) {

                    if (msg.length > 0) {

                        $("#SelectedAnalysis").html("");
                        $("#AnalysisId").html("");

                        $("#SelectedAnalysis").html("Select Analysis");
                        $("#SelectedAnalysis").attr("data", 0);
                        //_selectedAnalysisId = msg[0].Value;

                    }

                    for (analysis_index = 0; analysis_index < msg.length; analysis_index++) {

                        if (msg[analysis_index].Selected == true)
                        { $("#SelectedAnalysis").html(msg[analysis_index].Text); _selectedAnalysisId = msg[analysis_index].Value; }

                        $("#AnalysisId").append("<li><a class='saveAnalysis' href='#' data=" + msg[analysis_index].Value + " >" + msg[analysis_index].Text + "</a></li>");
                    }

                    saveAnalysisEvent();

                    if (msg.length > 0 && ($("#SelectedAnalysis").html() == "Select Analysis")) {
                       
                        $(window.ViewerControlId).attr("style", "display:none");
                        $("#validationMessage").html('Please select analysis of the selected company, to get the dashboard view.');
                        $("#validationMessage").attr("style", "display:inline-block");
                    } else {
                        $(window.ViewerControlId).attr("style", "display:block");
                        if ($("#countOfDataUploaded").val() == "0") {
                            $("#validationMessage").html('Data has not been uploaded for selected analysis.')
                            $("#validationMessage").show();
                        }
                        else { $("#validationMessage").hide(); }
                    }
                }
                else {
                    $("#SelectedAnalysis").html("No Analysis Assigned");
                    $("#SelectedAnalysis").attr("data", 0);

                    $("#AnalysisId").html("");

                    $(window.ViewerControlId).attr("style", "display:none");
                    $("#validationMessage").html('You do not have access to any analysis of the selected company.');
                    $("#validationMessage").attr("style", "display:inline-block");
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                // var msg = JSON.parse(XMLHttpRequest.responseText);
                theDialog.dialog('close');
            }
        });
    }

    function saveAnalysisEvent() {

        if (isSessionAliveCalled == 'false') { IsSessionAlive(); } 

        $(".saveAnalysis").click(function (event) {

            event.preventDefault();

            _selectedAnalysisId = $(this).attr('data');
            _selectedAnalysis = $(this).text();

            if (_selectedAnalysis != "" && _selectedAnalysisId != "") {
                $("#SelectedAnalysis").html(_selectedAnalysis);
                $("#SelectedAnalysis").attr("data", _selectedAnalysisId);
            }

            saveAnalysis(_selectedAnalysisId);

        });
    }


    function saveAnalysis(_selectedAnalysisId) {

        if (isSessionAliveCalled == 'false') { IsSessionAlive(); } 

        $.ajax({
            type: "GET",
            url: '@Url.Action("SetDefaultAnalysis", "Dashboard")',
            global: false,
            cache: false,
            data: { SelectedAnalysis: _selectedAnalysisId },
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (msg) {
                if (msg.Result = "success") {
                    //window.location = '@Url.Action("Index", "Dashboard")';                            
                    window.location.reload();
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                // var msg = JSON.parse(XMLHttpRequest.responseText);
                //theDialog.dialog('close');
            }
        });
    }

    function IsSessionAlive() {

       
        if (isSessionAliveCalled == 'false') {
            isSessionAliveCalled = 'true';
            $.ajax({
                type: "GET",
                url: '@Url.Action("IsSessionAlive", "Dashboard")',
                global: false,
                cache: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (msg) {
                    
                    if (msg == undefined || (msg == "") || msg == false) {

                        window.location = '@Url.Action("Index", "Home", New With {.session = "-1"})';
                    }

                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    // var msg = JSON.parse(XMLHttpRequest.responseText);
                    //theDialog.dialog('close');
                }
            });
        }        
    }

    $(document).ready(function () {
        var _selectedCompanyId

        if (isSessionAliveCalled == 'false') {
            IsSessionAlive();
        }

        if ($('#SelectedCompany').text() == "") {
            getCompanies(_selectedCompanyId);
        }


        $("#updatesetup").addClass("secondbutton_example").removeClass("ui-button ui-widget ui-state-default ui-corner-all");
        $("#updatesetup").hover(function () {
            $(this).removeClass("ui-state-hover");
            $(this).addClass("secondbutton_example");
        });
        $("#updatesetup").click(function () {
            IsSessionAlive();
            $(this).removeClass("ui-state-focus");
            $(this).addClass("secondbutton_example");
        });
    });
        
</script>
