﻿@ModelType onlineAnalytics.User
@Imports System.Collections.Generic
@Imports  onlineAnalytics
@Code
    ViewData("Title") = "CreateUser"
    Layout = "~/Views/Shared/AnalyticsMaster.vbhtml"
    Dim ua As New UserAccess
    Dim UserInfo As New User()
    If Not (Session("UserAccess") Is Nothing) Then
        ua = DirectCast(Session("UserAccess"), UserAccess)
    End If
    If Not (Session("UserInfo") Is Nothing) Then
        UserInfo = DirectCast(Session("UserInfo"), User)                      
    End If
End Code
<script src="@Url.Content("~/Scripts/jquery.validate.min.js")" type="text/javascript"></script>
<script src="@Url.Content("~/Scripts/jquery.validate.unobtrusive.min.js")" type="text/javascript"></script>

<div class="headerTable" style="float: right">
    <table>
        <tr>
            <td align="left" width="70%">
                Create User
            </td>
            @code   
                If (ua.AddUser = True) Then
                @<td align="right" width="12%" class="add">
                    <img src='@Url.Content("~/Content/Images/1369484457_users.png")' class="usersIcon"/>
                    <a href='@Url.Action("Index", "User")' class="button_example">Users List</a>
                </td>
                End If
            End Code
        </tr>
    </table>
</div>
<br />
<br />
<div class="center-panel">
    @Using Html.BeginForm("CreateUser", "User")
        @Html.AntiForgeryToken()   
        @Html.ValidationSummary(True)    
     
        @<fieldset>
            <div style="width: 100;">
                <div style="float: left; width: 33%;">
                    <ol class="round">
                        <li>
                            @Html.LabelFor(Function(m) m.UserName)<br />
                            @Html.TextBoxFor(Function(m) m.UserName)<br />
                            @Html.ValidationMessageFor(Function(m) m.UserName)
                        </li>
                        <li>
                            @Html.LabelFor(Function(m) m.UserEmail)<br />
                            @Html.TextBoxFor(Function(m) m.UserEmail)<br />
                            @Html.ValidationMessageFor(Function(m) m.UserEmail)
                        </li>
                    </ol>
                </div>
                <div style="float: left; width: 33%;">
                    <ol class="round">
                        <li>
                            @Html.LabelFor(Function(m) m.FirstName)<br />
                            @Html.TextBoxFor(Function(m) m.FirstName)<br />
                            @Html.ValidationMessageFor(Function(m) m.FirstName)
                        </li>
                        <li>
                            @Html.LabelFor(Function(m) m.LastName)<br />
                            @Html.TextBoxFor(Function(m) m.LastName)<br />
                            @Html.ValidationMessageFor(Function(m) m.LastName)
                        </li>
                    </ol>
                </div>
                <div style="float: left; width: 33%;">
                    <ol class="round">
                        <li>
                            @Html.LabelFor(Function(m) m.UserRoleId)<br />
                            @Html.DropDownList("UserRoleId", DirectCast(ViewBag.SelectedRoles, SelectList), "Select User Role", New With {.Class = "select", .Style = "width:206px;"})<br />
                            @Html.ValidationMessageFor(Function(m) m.UserRoleId)
                        </li>
                        <li>
                            @Code
                            If (UserInfo.UserRoleId = UserRoles.SAU) Then
                                @Html.Label("Subscriber Name")@<br />
                                @Html.EditorFor(Function(m) m.Customer.CustomerCompanyName)@<br />
                                @Html.HiddenFor(Function(m) m.CustomerId)                              
                            ElseIf (UserInfo.UserRoleId = UserRoles.PGAAdmin Or UserInfo.UserRoleId = UserRoles.PGASupport) Then
                                @Html.LabelFor(Function(m) m.CustomerId)@<br />
                                @Html.DropDownList("CustomerId", DirectCast(ViewBag.SelectedCustomer, SelectList), "Select Subscriber", New With {.Class = "select", .Style = "width:206px;", .data = Url.Action("SubscriptionCost", "User")})@<br />
                                @Html.ValidationMessageFor(Function(m) m.CustomerId)                                                       
                            End If
                            End Code
                        </li>
                    </ol>
                </div>
                <div class="sub_amount">
                    <div class="cost">
                        Additional cost to add new user to the subscription :
                        @Code
                        If (Not ViewBag.AddOnAmountInCents Is Nothing) Then
                            @<label id="AddOnAmountInCents">@ViewBag.AddOnAmountInCents</label>
                        Else
                            @<label id="AddOnAmountInCents"></label>
                        End If
                        End Code                       
                    </div>
                    <div class="cost">
                        Total cost to the subscription after new user is added :
                        @Code
                        If (Not ViewBag.AddOnAmountInCents Is Nothing) Then
                            @<label id="SubsciptionUnitAmountInCents">@ViewBag.SubsciptionUnitAmountInCents</label>
                        Else
                            @<label id="SubsciptionUnitAmountInCents"></label>
                        End If
                        End Code   
                        
                    </div>
                </div>
                <div class="input-form">
                    <input id="submit-button" type="submit" value="Create User" class="secondbutton_example" />
                    <input id="cancel-button" type="button" value="Cancel" class="secondbutton_example" />
                </div>
                <div style="float: left; margin-left: 20px;">
                    @If Not (DirectCast(TempData("Message"), String) Is Nothing) Then
                        @<label class="success">@TempData("Message").ToString()
                        </label>                         
        End If
                    @If Not (DirectCast(TempData("ErrorMessage"), String) Is Nothing) Then
                        @<label class="error">@TempData("ErrorMessage").ToString()
                        </label>                         
        End If
                </div>
            </div>                 
        </fieldset>
    
    End Using
</div>
<script type="text/javascript" language="javascript">

    $(document).ready(function () {
        IsSessionAlive();
        //Input Mask for landline phone number
        $("#ContactTelephone").mask("(999)-999-9999");
        //$("#FiscalMonthStart").mask("9?9");        

        if ($("#Customer_CustomerCompanyName")) {
            $("#Customer_CustomerCompanyName").attr("disabled", true);
        }

        if ($('#AddOnAmountInCents').text().length == 0) {

            $('#AddOnAmountInCents').text("00.00").prepend("$").append(" USD");
            $('#SubsciptionUnitAmountInCents').text("00.00").prepend("$").append(" USD");
        }
        else {
            $('#AddOnAmountInCents').prepend("$").append(" USD");
            $('#SubsciptionUnitAmountInCents').prepend("$").append(" USD");
        }

        if ($("#CustomerId option:selected").val() != "") {
            GetSubscriptionCost($('#CustomerId'));
        }

        function GetSubscriptionCost(Customer) {

            // Get Dropdownlist seleted item text
            var customer = $("#CustomerId option:selected").text();
            // Get Dropdownlist selected item value
            var customerId = $("#CustomerId option:selected").val();

            var actionUrl = Customer.attr('data') + "?CustomerId=" + customerId;
            if (customerId != undefined && customerId.length > 0) {
                $("#submit-button").removeAttr('disabled');
                $.ajax({
                    type: "GET",
                    url: actionUrl,
                    global: false,
                    cache: false,
                    data: { CustomerId: customerId },
                    dataType: "json",
                    beforeSend: function () {

                        $(".ui-dialog-titlebar").hide();
                        $('#loading').html("Please wait.... <img src='../Content/Images/loading.gif' />");
                        theDialog.dialog('open');
                    },
                    success: function (msg) {

                        if (msg.Error != "" && msg.Error != undefined) {

                            $('#AddOnAmountInCents').text("00.00").prepend("$").append(" USD");
                            $('#SubsciptionUnitAmountInCents').text("00.00").prepend("$").append(" USD");
                            //    validationDialog.dialog('open');

                            $("#submit-button").attr('disabled', 'disabled');

                            $.msgBox({
                                title: "User",
                                content: msg.Error,
                                type: "Error",
                                buttons: [{ value: "Ok"}]
                            });
                        }
                        else {

                            $("#submit-button").removeAttr('disabled');
                            $('#AddOnAmountInCents').text(msg.AddOnAmountInCents).prepend("$").append(" USD");
                            $('#SubsciptionUnitAmountInCents').text(msg.Subsciption_UnitAmountInCents).prepend("$").append(" USD");
                        }
                        theDialog.dialog('close');
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        var msg = JSON.parse(XMLHttpRequest.responseText);
                        theDialog.dialog('close');
                        //alert(msg.Message);                      
                    }
                });
            } else if($("#CustomerId").val() == "") {
                $("#submit-button").removeAttr('disabled');
                $('#AddOnAmountInCents').text("00.00").prepend("$").append(" USD");
                $('#SubsciptionUnitAmountInCents').text("00.00").prepend("$").append(" USD");
            }
        }

        $('#CustomerId').change(function (event) {

            event.preventDefault();
            GetSubscriptionCost($('#CustomerId'));            
        })

    });

    $("#cancel-button").click(function () {
        window.location = '@Url.Action("Index", "User")';
    });
</script>
