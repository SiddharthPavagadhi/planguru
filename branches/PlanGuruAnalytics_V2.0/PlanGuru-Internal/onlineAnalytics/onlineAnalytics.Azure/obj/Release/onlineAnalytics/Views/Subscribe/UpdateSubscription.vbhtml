﻿@ModelType onlineAnalytics.Customer
@Code
    ViewData("Title") = "Update Subscription"
    Layout = "~/Views/Shared/AnalyticsMaster.vbhtml"
End Code
<header>    
<link rel="stylesheet" href="@Url.Content("~/themes/default/recurly.css")" type="text/css" />
    <link rel="stylesheet" href="@Url.Content("~/themes/default/examples.css")" type="text/css" />
    </header>    
<h2>Subscription updated successfully</h2>

<div id="billing-info" class="section">
    <div id="recurly-subscribe">
        <h2 class="heading">
            Billing Information</h2>
        <!-- table: billing info -->
        <table style="width: 100%;">
            <tbody>
                <tr>
                    <th scope="row">
                        Subscription Plan:
                    </th>
                    <td>
                        PlanGuru Analytics
                    </td>
                </tr>
                <tr>
                    <th scope="row">
                        First Name:
                    </th>
                    <td>
                        @Model.FirstNameOnCard
                    </td>
                </tr>
                <tr>
                    <th scope="row">
                        Last Name:
                    </th>
                    <td>
                       @Model.LastNameOnCard 
                    </td>
                </tr>
                <tr>
                    <th scope="row">
                        Email Id:
                    </th>
                    <td>
                        @Model.CustomerEmail 
                    </td>
                </tr>
                <tr>
                    <th scope="row">
                        Credit Card:
                    </th>
                    <td>
                        XXXX-XXXX-XXXX-@ViewBag.CreditCardLastFourDigit (@ViewBag.CreditCardType)
                    </td>
                </tr>
            </tbody>
        </table>
      <p class="receipt">
      </p>
    </div>
</div>
