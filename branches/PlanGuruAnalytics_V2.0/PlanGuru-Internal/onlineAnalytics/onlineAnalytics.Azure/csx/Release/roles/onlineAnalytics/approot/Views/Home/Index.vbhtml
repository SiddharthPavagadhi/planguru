﻿@Code
    ViewData("Title") = "Home Page"
    Layout = "~/Views/Shared/_Layout.vbhtml"
End Code
 <div>
    PlanGuru Analytics is a tool that enables PlanGuru users to create a dashboard of your company's key performance indicators and analyze both financial and non-financial data in a flexible and easy to use environment.
 </div>
<div>
@If Not (DirectCast(TempData("Message"), String) Is Nothing) Then
            @<label class="success">@TempData("Message").ToString()
            </label>                         
        End If
        @If Not (DirectCast(TempData("ErrorMessage"), String) Is Nothing) Then
            @<label class="error">@TempData("ErrorMessage").ToString()
            </label>                         
        End If
</div>
@*<div  style="display: none;">
            @Html.TextBox("intBrowserWidth")
</div>*@ 