﻿@Imports Farpoint.Web.Spread
@Imports Farpoint.Mvc.Spread
@Imports  onlineAnalytics
@Code
    ViewData("Title") = "Index"
    Layout = "~/Views/Shared/AnalyticsMaster.vbhtml"
End Code
<script type="text/javascript">
    window.ViewerControlId = "#dashboardView";
    $(function () {
        $("#updatesetup")
            .button()
            .click(function (event) {

                if (isSessionAliveCalled == 'false') { IsSessionAlive(); }

                document.location = '@Url.Action("Index", "DashboardSetup")';
            });
    });
        $(document).ready(function () {

        if (isSessionAliveCalled == 'false') { IsSessionAlive(); }

        $("#footerpanel").removeClass("plangurufooter");
        $("#footerpanel").addClass("plangurufooterfordashboard");
    });
</script>
@Code
    Dim UserInfo As User = Nothing
    Dim ua As New UserAccess
    
    Dim objSetup As New SetupCount
    
    If Not (Session("UserInfo") Is Nothing) Then
        UserInfo = DirectCast(Session("UserInfo"), User)
    End If
    
    If (Not ViewBag.Setup Is Nothing) Then
        objSetup = DirectCast(ViewBag.Setup, SetupCount)
    End If
    
End code
<div class="center-panel">
    @If Not (DirectCast(TempData("Message"), String) Is Nothing) Then
        @<label class="success">@TempData("Message").ToString()
        </label>                         
    End If
    @If Not (DirectCast(TempData("ErrorMessage"), String) Is Nothing) Then
        @<label class="error">@TempData("ErrorMessage").ToString()
        </label>                         
    End If
    @Code
        
        @Html.Hidden("countOfDataUploaded", objSetup.countDataUploadedOfSelectedAnalysis)        
        @<label id="validationMessage" class="info" style="display: none;">
            @If (objSetup.AanlysisCount > 0 And objSetup.selectedAnalysis = 0) Then
                @MvcHtmlString.Create("Please select company & one of the respective analysis from top-menu selection, to get the dashboard view.")
            ElseIf (objSetup.selectedAnalysis > 0 And objSetup.countDataUploadedOfSelectedAnalysis = 0) Then
                @MvcHtmlString.Create("Data has not been uploaded for selected analysis.")
            End If
        </label> 
        
        If (Not UserInfo Is Nothing) Then
                   
            If (UserInfo.UserRoleId <= UserRoles.SAU) Then
                    
                If (objSetup.CompanyCount = 0 Or objSetup.AanlysisCount = 0) Then
        @<label class="info">You have to create below listed action item, to get the dashboard
            view.</label> 
                End If
                If (objSetup.CompanyCount = 0) Then
        @<div class="secondbutton_example" style='margin: 20px; width: 200px;'>
            <a href='@Url.Action("Create", "Company")' style="text-decoration:none;color: #FFFFFF;">
                Add Company</a>
        </div>  
                End If
                If (objSetup.CompanyCount > 0 And objSetup.AanlysisCount = 0) Then
        @<div class="secondbutton_example" style='margin: 20px; width: 200px;'>
            <a href='@Url.Action("Create", "Analysis")'  style="text-decoration:none;color: #FFFFFF;">
                Add Analysis</a>
        </div>  
                End If
                If (objSetup.CompanyCount = 0 Or objSetup.AanlysisCount = 0) Then
        @<div class="secondbutton_example" style='margin: 20px; width: 200px;'>
            <a href='@Url.Action("CreateUser", "User")'  style="text-decoration:none;color: #FFFFFF;">
                Add User</a>
        </div>  
                End If
            ElseIf (UserInfo.UserRoleId > UserRoles.SAU) Then
                
                If (objSetup.CompanyCount = 0) Then
        @<label class="info">Administrator haven't assigned access to you of any company / analysis.</label>
                ElseIf (objSetup.AanlysisCount = 0) Then
        @<label class="info">Administrator haven't assigned access to you of any analysis.</label> 
                End If
                
            End If
        End If
        If (objSetup.CompanyCount > 0 And objSetup.AanlysisCount > 0 And objSetup.selectedAnalysis > 0 And objSetup.countDataUploadedOfSelectedAnalysis > 0) Then
           
        @<div id="dashboardView" style="display: none;">
            @Using Html.BeginForm("Index", "Dashboard")
                    @<table style="width: 100%; margin-top: 7px;" class="center-top-select">
                        <tr>
                            <td>
                                <span style="margin-left: 50px;">Period</span>
                                    @Html.DropDownList("intPeriod", DirectCast(ViewData("Periods"), SelectList), New With {.Class = "select"})
                            </td>
                            <td>
                                <a href='@Url.Action("Index", "DashboardSetup")' style="margin-left: 460px; color: #276fa6; font-weight:bold;">
                                    Change Dashboard Setup</a>
                            </td>
                        </tr>
                    </table>
              End Using
            <div id="spread" style="padding-top: 1px; display: inline-block; margin-left: 50px;">
                @Html.FpSpread("spdDashboard", Sub(x)
                                                   x.RowHeader.Visible = False
                                                   x.ActiveSheetView.PageSize = 1000
                                               End Sub)
            </div>
        </div>           
            
        End If
        
    End Code
</div>
<script type="text/javascript">

    $('#intPeriod').change(function () {

        this.form.submit();
    });
</script>
