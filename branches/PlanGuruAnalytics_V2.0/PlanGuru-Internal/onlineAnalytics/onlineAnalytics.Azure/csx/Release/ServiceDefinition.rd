﻿<?xml version="1.0" encoding="utf-8"?>
<serviceModel xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" name="onlineAnalytics.Azure" generation="1" functional="0" release="0" Id="59ec482c-2d3d-4ce8-a47a-d456011a3751" dslVersion="1.2.0.0" xmlns="http://schemas.microsoft.com/dsltools/RDSM">
  <groups>
    <group name="onlineAnalytics.AzureGroup" generation="1" functional="0" release="0">
      <componentports>
        <inPort name="onlineAnalytics:Endpoint1" protocol="http">
          <inToChannel>
            <lBChannelMoniker name="/onlineAnalytics.Azure/onlineAnalytics.AzureGroup/LB:onlineAnalytics:Endpoint1" />
          </inToChannel>
        </inPort>
        <inPort name="onlineAnalytics:Microsoft.WindowsAzure.Plugins.RemoteForwarder.RdpInput" protocol="tcp">
          <inToChannel>
            <lBChannelMoniker name="/onlineAnalytics.Azure/onlineAnalytics.AzureGroup/LB:onlineAnalytics:Microsoft.WindowsAzure.Plugins.RemoteForwarder.RdpInput" />
          </inToChannel>
        </inPort>
      </componentports>
      <settings>
        <aCS name="Certificate|onlineAnalytics:Microsoft.WindowsAzure.Plugins.RemoteAccess.PasswordEncryption" defaultValue="">
          <maps>
            <mapMoniker name="/onlineAnalytics.Azure/onlineAnalytics.AzureGroup/MapCertificate|onlineAnalytics:Microsoft.WindowsAzure.Plugins.RemoteAccess.PasswordEncryption" />
          </maps>
        </aCS>
        <aCS name="onlineAnalytics:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" defaultValue="">
          <maps>
            <mapMoniker name="/onlineAnalytics.Azure/onlineAnalytics.AzureGroup/MaponlineAnalytics:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" />
          </maps>
        </aCS>
        <aCS name="onlineAnalytics:Microsoft.WindowsAzure.Plugins.RemoteAccess.AccountEncryptedPassword" defaultValue="">
          <maps>
            <mapMoniker name="/onlineAnalytics.Azure/onlineAnalytics.AzureGroup/MaponlineAnalytics:Microsoft.WindowsAzure.Plugins.RemoteAccess.AccountEncryptedPassword" />
          </maps>
        </aCS>
        <aCS name="onlineAnalytics:Microsoft.WindowsAzure.Plugins.RemoteAccess.AccountExpiration" defaultValue="">
          <maps>
            <mapMoniker name="/onlineAnalytics.Azure/onlineAnalytics.AzureGroup/MaponlineAnalytics:Microsoft.WindowsAzure.Plugins.RemoteAccess.AccountExpiration" />
          </maps>
        </aCS>
        <aCS name="onlineAnalytics:Microsoft.WindowsAzure.Plugins.RemoteAccess.AccountUsername" defaultValue="">
          <maps>
            <mapMoniker name="/onlineAnalytics.Azure/onlineAnalytics.AzureGroup/MaponlineAnalytics:Microsoft.WindowsAzure.Plugins.RemoteAccess.AccountUsername" />
          </maps>
        </aCS>
        <aCS name="onlineAnalytics:Microsoft.WindowsAzure.Plugins.RemoteAccess.Enabled" defaultValue="">
          <maps>
            <mapMoniker name="/onlineAnalytics.Azure/onlineAnalytics.AzureGroup/MaponlineAnalytics:Microsoft.WindowsAzure.Plugins.RemoteAccess.Enabled" />
          </maps>
        </aCS>
        <aCS name="onlineAnalytics:Microsoft.WindowsAzure.Plugins.RemoteForwarder.Enabled" defaultValue="">
          <maps>
            <mapMoniker name="/onlineAnalytics.Azure/onlineAnalytics.AzureGroup/MaponlineAnalytics:Microsoft.WindowsAzure.Plugins.RemoteForwarder.Enabled" />
          </maps>
        </aCS>
        <aCS name="onlineAnalyticsInstances" defaultValue="[1,1,1]">
          <maps>
            <mapMoniker name="/onlineAnalytics.Azure/onlineAnalytics.AzureGroup/MaponlineAnalyticsInstances" />
          </maps>
        </aCS>
      </settings>
      <channels>
        <lBChannel name="LB:onlineAnalytics:Endpoint1">
          <toPorts>
            <inPortMoniker name="/onlineAnalytics.Azure/onlineAnalytics.AzureGroup/onlineAnalytics/Endpoint1" />
          </toPorts>
        </lBChannel>
        <lBChannel name="LB:onlineAnalytics:Microsoft.WindowsAzure.Plugins.RemoteForwarder.RdpInput">
          <toPorts>
            <inPortMoniker name="/onlineAnalytics.Azure/onlineAnalytics.AzureGroup/onlineAnalytics/Microsoft.WindowsAzure.Plugins.RemoteForwarder.RdpInput" />
          </toPorts>
        </lBChannel>
        <sFSwitchChannel name="SW:onlineAnalytics:Microsoft.WindowsAzure.Plugins.RemoteAccess.Rdp">
          <toPorts>
            <inPortMoniker name="/onlineAnalytics.Azure/onlineAnalytics.AzureGroup/onlineAnalytics/Microsoft.WindowsAzure.Plugins.RemoteAccess.Rdp" />
          </toPorts>
        </sFSwitchChannel>
      </channels>
      <maps>
        <map name="MapCertificate|onlineAnalytics:Microsoft.WindowsAzure.Plugins.RemoteAccess.PasswordEncryption" kind="Identity">
          <certificate>
            <certificateMoniker name="/onlineAnalytics.Azure/onlineAnalytics.AzureGroup/onlineAnalytics/Microsoft.WindowsAzure.Plugins.RemoteAccess.PasswordEncryption" />
          </certificate>
        </map>
        <map name="MaponlineAnalytics:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" kind="Identity">
          <setting>
            <aCSMoniker name="/onlineAnalytics.Azure/onlineAnalytics.AzureGroup/onlineAnalytics/Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" />
          </setting>
        </map>
        <map name="MaponlineAnalytics:Microsoft.WindowsAzure.Plugins.RemoteAccess.AccountEncryptedPassword" kind="Identity">
          <setting>
            <aCSMoniker name="/onlineAnalytics.Azure/onlineAnalytics.AzureGroup/onlineAnalytics/Microsoft.WindowsAzure.Plugins.RemoteAccess.AccountEncryptedPassword" />
          </setting>
        </map>
        <map name="MaponlineAnalytics:Microsoft.WindowsAzure.Plugins.RemoteAccess.AccountExpiration" kind="Identity">
          <setting>
            <aCSMoniker name="/onlineAnalytics.Azure/onlineAnalytics.AzureGroup/onlineAnalytics/Microsoft.WindowsAzure.Plugins.RemoteAccess.AccountExpiration" />
          </setting>
        </map>
        <map name="MaponlineAnalytics:Microsoft.WindowsAzure.Plugins.RemoteAccess.AccountUsername" kind="Identity">
          <setting>
            <aCSMoniker name="/onlineAnalytics.Azure/onlineAnalytics.AzureGroup/onlineAnalytics/Microsoft.WindowsAzure.Plugins.RemoteAccess.AccountUsername" />
          </setting>
        </map>
        <map name="MaponlineAnalytics:Microsoft.WindowsAzure.Plugins.RemoteAccess.Enabled" kind="Identity">
          <setting>
            <aCSMoniker name="/onlineAnalytics.Azure/onlineAnalytics.AzureGroup/onlineAnalytics/Microsoft.WindowsAzure.Plugins.RemoteAccess.Enabled" />
          </setting>
        </map>
        <map name="MaponlineAnalytics:Microsoft.WindowsAzure.Plugins.RemoteForwarder.Enabled" kind="Identity">
          <setting>
            <aCSMoniker name="/onlineAnalytics.Azure/onlineAnalytics.AzureGroup/onlineAnalytics/Microsoft.WindowsAzure.Plugins.RemoteForwarder.Enabled" />
          </setting>
        </map>
        <map name="MaponlineAnalyticsInstances" kind="Identity">
          <setting>
            <sCSPolicyIDMoniker name="/onlineAnalytics.Azure/onlineAnalytics.AzureGroup/onlineAnalyticsInstances" />
          </setting>
        </map>
      </maps>
      <components>
        <groupHascomponents>
          <role name="onlineAnalytics" generation="1" functional="0" release="0" software="D:\SVN\PlanGuru\trunk\PlanGuru-Internal\onlineAnalytics\onlineAnalytics.Azure\csx\Release\roles\onlineAnalytics" entryPoint="base\x64\WaHostBootstrapper.exe" parameters="base\x64\WaIISHost.exe " memIndex="1792" hostingEnvironment="frontendadmin" hostingEnvironmentVersion="2">
            <componentports>
              <inPort name="Endpoint1" protocol="http" portRanges="80" />
              <inPort name="Microsoft.WindowsAzure.Plugins.RemoteForwarder.RdpInput" protocol="tcp" />
              <inPort name="Microsoft.WindowsAzure.Plugins.RemoteAccess.Rdp" protocol="tcp" portRanges="3389" />
              <outPort name="onlineAnalytics:Microsoft.WindowsAzure.Plugins.RemoteAccess.Rdp" protocol="tcp">
                <outToChannel>
                  <sFSwitchChannelMoniker name="/onlineAnalytics.Azure/onlineAnalytics.AzureGroup/SW:onlineAnalytics:Microsoft.WindowsAzure.Plugins.RemoteAccess.Rdp" />
                </outToChannel>
              </outPort>
            </componentports>
            <settings>
              <aCS name="Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" defaultValue="" />
              <aCS name="Microsoft.WindowsAzure.Plugins.RemoteAccess.AccountEncryptedPassword" defaultValue="" />
              <aCS name="Microsoft.WindowsAzure.Plugins.RemoteAccess.AccountExpiration" defaultValue="" />
              <aCS name="Microsoft.WindowsAzure.Plugins.RemoteAccess.AccountUsername" defaultValue="" />
              <aCS name="Microsoft.WindowsAzure.Plugins.RemoteAccess.Enabled" defaultValue="" />
              <aCS name="Microsoft.WindowsAzure.Plugins.RemoteForwarder.Enabled" defaultValue="" />
              <aCS name="__ModelData" defaultValue="&lt;m role=&quot;onlineAnalytics&quot; xmlns=&quot;urn:azure:m:v1&quot;&gt;&lt;r name=&quot;onlineAnalytics&quot;&gt;&lt;e name=&quot;Endpoint1&quot; /&gt;&lt;e name=&quot;Microsoft.WindowsAzure.Plugins.RemoteAccess.Rdp&quot; /&gt;&lt;e name=&quot;Microsoft.WindowsAzure.Plugins.RemoteForwarder.RdpInput&quot; /&gt;&lt;/r&gt;&lt;/m&gt;" />
            </settings>
            <resourcereferences>
              <resourceReference name="DiagnosticStore" defaultAmount="[4096,4096,4096]" defaultSticky="true" kind="Directory" />
              <resourceReference name="EventStore" defaultAmount="[1000,1000,1000]" defaultSticky="false" kind="LogStore" />
            </resourcereferences>
            <storedcertificates>
              <storedCertificate name="Stored0Microsoft.WindowsAzure.Plugins.RemoteAccess.PasswordEncryption" certificateStore="My" certificateLocation="System">
                <certificate>
                  <certificateMoniker name="/onlineAnalytics.Azure/onlineAnalytics.AzureGroup/onlineAnalytics/Microsoft.WindowsAzure.Plugins.RemoteAccess.PasswordEncryption" />
                </certificate>
              </storedCertificate>
            </storedcertificates>
            <certificates>
              <certificate name="Microsoft.WindowsAzure.Plugins.RemoteAccess.PasswordEncryption" />
            </certificates>
          </role>
          <sCSPolicy>
            <sCSPolicyIDMoniker name="/onlineAnalytics.Azure/onlineAnalytics.AzureGroup/onlineAnalyticsInstances" />
            <sCSPolicyUpdateDomainMoniker name="/onlineAnalytics.Azure/onlineAnalytics.AzureGroup/onlineAnalyticsUpgradeDomains" />
            <sCSPolicyFaultDomainMoniker name="/onlineAnalytics.Azure/onlineAnalytics.AzureGroup/onlineAnalyticsFaultDomains" />
          </sCSPolicy>
        </groupHascomponents>
      </components>
      <sCSPolicy>
        <sCSPolicyUpdateDomain name="onlineAnalyticsUpgradeDomains" defaultPolicy="[5,5,5]" />
        <sCSPolicyFaultDomain name="onlineAnalyticsFaultDomains" defaultPolicy="[2,2,2]" />
        <sCSPolicyID name="onlineAnalyticsInstances" defaultPolicy="[1,1,1]" />
      </sCSPolicy>
    </group>
  </groups>
  <implements>
    <implementation Id="bb922848-b549-429a-99a4-b4a5d5f617c6" ref="Microsoft.RedDog.Contract\ServiceContract\onlineAnalytics.AzureContract@ServiceDefinition">
      <interfacereferences>
        <interfaceReference Id="8265dc75-932a-410e-a38e-5e3379978cca" ref="Microsoft.RedDog.Contract\Interface\onlineAnalytics:Endpoint1@ServiceDefinition">
          <inPort>
            <inPortMoniker name="/onlineAnalytics.Azure/onlineAnalytics.AzureGroup/onlineAnalytics:Endpoint1" />
          </inPort>
        </interfaceReference>
        <interfaceReference Id="9061f7c8-0ace-4b4f-b785-aed560621889" ref="Microsoft.RedDog.Contract\Interface\onlineAnalytics:Microsoft.WindowsAzure.Plugins.RemoteForwarder.RdpInput@ServiceDefinition">
          <inPort>
            <inPortMoniker name="/onlineAnalytics.Azure/onlineAnalytics.AzureGroup/onlineAnalytics:Microsoft.WindowsAzure.Plugins.RemoteForwarder.RdpInput" />
          </inPort>
        </interfaceReference>
      </interfacereferences>
    </implementation>
  </implements>
</serviceModel>