﻿Imports System.Collections.Generic
Imports System.Linq
Imports System.Data

Public Class CustomerRepository
    Inherits GenericRepository(Of Customer)
    Implements ICustomerRepository
    Implements IDisposable
    'Private context As DataAccess
    Private disposed As Boolean = False

    Public Sub New(context As DataAccess)
        MyBase.New(context)
    End Sub

    Public Function GetCustomers() As IEnumerable(Of Customer) Implements ICustomerRepository.GetCustomers
        Return context.Customers.ToList()
    End Function

    Public Function GetCustomerById(customerId As Integer) As Customer Implements ICustomerRepository.GetCustomerById
        Return context.Customers.Find(customerId)
    End Function

    Public Function GetCustomer(customerId As Integer) As IEnumerable(Of Customer) Implements ICustomerRepository.GetCustomer
        Return context.Customers.Where(Function(c) c.CustomerId = customerId).ToList()
    End Function

    Public Sub InsertCustomer(customer As Customer) Implements ICustomerRepository.InsertCustomer
        context.Customers.Add(customer)
    End Sub
    Public Sub DeleteCustomer(customerId As String) Implements ICustomerRepository.DeleteCustomer
        Dim customer As Customer = context.Customers.Find(customerId)
        context.Customers.Remove(customer)
    End Sub

    Public Sub UpdateCustomer(customer As Customer) Implements ICustomerRepository.UpdateCustomer
        context.Entry(customer).State = EntityState.Modified
    End Sub

    Public Sub Save() Implements ICustomerRepository.Save
        context.SaveChanges()
    End Sub

    Protected Overridable Sub Dispose(disposing As Boolean)
        If Not Me.disposed Then
            If disposing Then
                context.Dispose()
            End If
        End If
        Me.disposed = True
    End Sub

    Public Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
    Public Function SearchCustomers(customerId As Integer, customerName As String, sauName As String, telephonenumber As String) As IEnumerable(Of Customer) Implements ICustomerRepository.SearchCustomers

        Dim query As IEnumerable(Of Customer)

        If sauName IsNot Nothing Then

            query = From cust In context.Customers _
                    Join us In context.Users On cust.CustomerId Equals us.CustomerId
                    Where (us.FirstName.ToLower().Contains(sauName.ToLower().Trim()) Or us.LastName.ToLower().Contains(sauName.ToLower().Trim()) Or us.UserName.ToLower().Contains(sauName.ToLower().Trim()))
                    Select cust
            If customerId > 0 Then
                query = query.Where(Function(c) c.CustomerId.Equals(customerId)).ToList()
            End If

            If customerName IsNot Nothing Then
                query = query.Where(Function(c) c.CustomerFirstName.ToLower().Contains(customerName.ToLower().Trim()) Or c.CustomerLastName.ToLower().Contains(customerName.ToLower().Trim())).ToList()
            End If

            If telephonenumber IsNot Nothing Then
                query = query.Where(Function(c) c.ContactTelephone.Equals(telephonenumber.ToLower().Trim())).ToList()
            End If


        Else
            query = context.Customers.ToList()

            If customerId > 0 Then
                query = query.Where(Function(c) c.CustomerId.Equals(customerId)).ToList()
            End If

            If customerName IsNot Nothing Then
                query = query.Where(Function(c) (c.CustomerFirstName.ToLower().Contains(customerName.ToLower().Trim()) Or c.CustomerLastName.ToLower().Contains(customerName.ToLower().Trim()))).ToList()
            End If
            If telephonenumber IsNot Nothing Then
                query = query.Where(Function(c) c.ContactTelephone.ToLower().Contains(telephonenumber.ToLower().Trim())).ToList()
            End If
        End If

        Return query

    End Function
End Class
