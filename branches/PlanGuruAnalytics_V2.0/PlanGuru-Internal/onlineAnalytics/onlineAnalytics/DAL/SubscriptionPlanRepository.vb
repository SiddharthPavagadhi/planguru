﻿Imports System.Collections.Generic
Imports System.Linq
Imports System.Data
Imports System.Data.SqlClient


Public Class SubscriptionPlanRepository
    Inherits GenericRepository(Of SubscriptionPlan)
    Implements ISubscriptionPlanRepository
    Implements IDisposable
    'Private context As DataAccess
    Private disposed As Boolean = False

    Public Sub New(context As DataAccess)
        'Me.context = context
        MyBase.New(context)
    End Sub

    Public Function GetSubscriptionPlans() As IEnumerable(Of SubscriptionPlan) Implements ISubscriptionPlanRepository.GetSubscriptionPlans
        Return context.SubscriptionPlans.OrderBy(Function(sp) sp.PlanTypeId).ToList()
    End Function

    Public Function GetSubscriptionPlan(planType As Integer) As SubscriptionPlan Implements ISubscriptionPlanRepository.GetSubscriptionPlan

        Return (From subscriptionPlan In context.SubscriptionPlans.Where(Function(sc) sc.PlanTypeId = planType) Select subscriptionPlan).SingleOrDefault()

    End Function
    Public Function GetSubscriptionPlan(plan As String) As SubscriptionPlan Implements ISubscriptionPlanRepository.GetSubscriptionPlan

        Return (From subscriptionPlan In context.SubscriptionPlans.Where(Function(sc) sc.Plan = plan) Select subscriptionPlan).SingleOrDefault()

    End Function


    Public Sub Save() Implements ISubscriptionPlanRepository.Save
        context.SaveChanges()
    End Sub
    Protected Overridable Sub Dispose(disposing As Boolean)
        If Not Me.disposed Then
            If disposing Then
                context.Dispose()
            End If
        End If
        Me.disposed = True
    End Sub

    Public Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
End Class
