﻿Imports System.Collections.Generic
Imports System.Linq
Imports System.Web
'Interface for Company, CompanyRepository.vb implementes all methods of this Interface.
Public Interface ICompanyRepository
    Inherits IDisposable
    Function GetCompanies() As IEnumerable(Of Company)
    Function GetCompanyById(id As Integer) As Company
    Function GetCompanyByIdandUserId(companyId As Integer, userId As Integer) As Company
    Sub InsertCompany(company As Company)
    Sub DeleteCompany(companyId As Integer)
    Sub UpdateCompany(company As Company)
    Sub Save()
End Interface

'Interface for Analysis, AnalysisRepository.vb implementes all methods of this Interface.
Public Interface IAnalysisRepository
    Inherits IDisposable

    Function GetAnalyses() As IEnumerable(Of Analysis)
    Function GetAnalysisById(id As Integer) As Analysis
    Function GetAnalysisByIdandUserId(analysisId As Integer, userId As Integer) As Analysis
    Function ValidateAnalysis(Analysis As String, CompanyId As Integer) As IQueryable(Of Analysis)
    Sub InsertAnalysis(analysis As Analysis)
    Sub UpdateAnalysis(analysis As Analysis)
    Sub DeleteAnalysis(analysisId As Integer)
    'Function DeleteAccountTypeForAnalysis(analysisId As Integer)
    'Function DeleteBalanceForAnalysis(analysisId As Integer)
    Function DeleteAnalysisByCompany(companyId As Integer)
    Sub Save()
End Interface

'Interface for Customer, CustomerRepository.vb implementes all methods of this Interface.
Public Interface ICustomerRepository
    Inherits IDisposable

    Function GetCustomers() As IEnumerable(Of Customer)
    Function GetCustomerById(customerId As Integer) As Customer
    Function GetCustomer(customerId As Integer) As IEnumerable(Of Customer)
    Sub InsertCustomer(customer As Customer)
    Sub DeleteCustomer(customerId As String)
    Sub UpdateCustomer(customer As Customer)
    Function SearchCustomers(customerId As Integer, customerName As String, sauName As String, telephonenumber As String) As IEnumerable(Of Customer)
    Sub Save()
End Interface


'Interface for CountryCode, CountryStateRepository.vb implementes all methods of this Interface.
Public Interface ICountryCodeRepository
    Inherits IDisposable
    Function GetCountryCodes() As IEnumerable(Of CountryCode)
End Interface
'Interface for State, StateRepository.vb implementes all methods of this Interface.
Public Interface IStateCodeRepository
    Inherits IDisposable
    Function GetStateCodes() As IEnumerable(Of StateCode)
End Interface

'Interface for State, StateRepository.vb implementes all methods of this Interface.
Public Interface IUserRoleRepository
    Inherits IDisposable
    Function GetUserRoles() As IEnumerable(Of UserRole)
End Interface

Public Interface IUserRolePermissionRepository
    Inherits IDisposable
    Function GetUserRolePermission() As IEnumerable(Of UserRolePermission)
End Interface


Public Interface IUserRolePermissionMappingRepository
    Inherits IDisposable
    Function GetUserRolePermissionMapping() As IEnumerable(Of UserRolePermissionMapping)
End Interface

'Interface for State, StateRepository.vb implementes all methods of this Interface.
Public Interface IUserRepository
    Inherits IDisposable

    Function GetUser(customerId As Integer, UserRoleId As Integer) As Integer
    Function ValidateUserAccount(UserName As String, Password As String) As User
    Function UsernameAvailibility(UserName As String) As IQueryable(Of User)
    Function EmailAddressAvailibility(EmailAddress As String) As IQueryable(Of User)
    Function PreceededUsers(customerId As Integer, userRoleId As Integer) As IEnumerable(Of User)
    Function GetUsers() As IEnumerable(Of User)
    Function GetUserById(userId As Integer) As User
    Sub UpdateUser(user As User)
    Sub DeleteUser(userId As Integer)
    Sub DeleteNonSAUUser(userId As Integer)
    Sub Save()
End Interface

Public Interface IUserCompanyMappingRepository
    Inherits IDisposable

    Function GetUserCompanyMappingByUserId(CompanyId As Integer) As IEnumerable(Of UserCompanyMapping)
    Function GetUserCompanyMappingByCompanyId(CompanyId As Integer) As IEnumerable(Of UserCompanyMapping)
    Function GetUserCompanyMapping() As IEnumerable(Of UserCompanyMapping)
    Function GetUserCompanyMappingById(userCompanyMappingId As Integer) As UserCompanyMapping
    Sub UpdateUserCompanyMapping(userCompanyDetails As UserCompanyMapping)
    Sub DeleteUserCompanyMapping(userCompanyMappingId As Integer)
    Function DeleteUserCompanyMappingByUserIdAndCompanyId(companyId As Integer, userIdList As String) As Integer
    Function DeleteUserCompanyMappingByCompany(companyId As Integer)
    Function DeleteUserCompanyMappingByUserId(userId As Integer)
    Function GetDistinctCompanies(Users As List(Of Integer)) As List(Of Integer)
    Sub Save()
End Interface

Public Interface IUserAnalysisMappingRepository
    Inherits IDisposable

    Function GetUserAnalysisMappingByUserId(AnalysisId As Integer) As IEnumerable(Of UserAnalysisMapping)
    Function GetUserAnalysisMappingByAnalysisId(AnalysisId As Integer) As IEnumerable(Of UserAnalysisMapping)
    Function GetUserAnalysisMapping() As IEnumerable(Of UserAnalysisMapping)
    Function GetUserAnalysisMappingById(UserAnalysisMappingId As Integer) As UserAnalysisMapping
    Sub UpdateUserAnalysisMapping(UserAnalysisDetails As UserAnalysisMapping)
    Sub DeleteUserAnalysisMapping(UserAnalysisMappingId As Integer)
    Function DeleteUserAnalysisMappingByUserIdAndAnalysisId(AnalysisId As Integer, UserIdList As String) As Integer
    Function DeleteUserAnalysisMappingByAnalysis(analysisId As Integer)
    Function DeleteUserAnalysisMappingByUserId(userId As Integer)
    Sub Save()
End Interface
'Interface for EmailInfo, EmailRepository.vb implementes all methods of this Interface.
Public Interface IEmailRepository
    Inherits IDisposable

    'Function ValidateUserAccount(UserName As String, Password As String) As EmailInfo
    Function GetEmailInfo() As IEnumerable(Of EmailInfo)
    Function GetEmailInfoById(id As Integer) As EmailInfo
    'Sub UpdateEmailInfo(email As EmailInfo)
    'Sub DeleteEmailInfo(emailId As Integer)
    Sub Save()
End Interface

Public Interface IAccountRepository
    Inherits IDisposable
    Function GetAccounts() As IEnumerable(Of Account)
    Function GetAccounts(accountId As Integer, acctTypeId As Integer, analysisId As Integer) As Account
    Function GetAccounts(accountId As Integer, analysisId As Integer) As Account
    Function GetAccountsAndAccTypes(analysisId As Integer, typeDescList As String) As IEnumerable(Of Account)
    Function GetAccountRecordsByAnalysisId(analysisId As Integer, Optional acctDescriptor() As String = Nothing) As IEnumerable(Of Account)
    Function GeAccountById(accountId As Integer) As Account
    Function DeleteAccountByAnalysis(analysisId As Integer)
    Sub Save()
End Interface

Public Interface IAccountTypeRepository
    Inherits IDisposable
    Function GetAccountTypes() As IEnumerable(Of AcctType)
    Function GetAccountTypeRecordsByAnalysisId(analysisId As Integer, Optional typeDesc() As String = Nothing) As IEnumerable(Of AcctType)
    Function GeAccountTypeById(accountId As Integer) As AcctType
    Function DeleteAccountTypeByAnalysis(analysisId As Integer)
    Sub Save()
End Interface

Public Interface IBalanceRepository
    Inherits IDisposable

    Function GetBalances() As IEnumerable(Of Balance)
    Function GetBalanceRecordsByAnalysisId(analysisId As Integer) As IEnumerable(Of Balance)
    Function GeBalanceById(balanceId As Integer) As Balance
    Function DeleteBalanceByAnalysis(analysisId As Integer)
    Sub Save()
End Interface

Public Interface IChartFormatRepository
    Inherits IDisposable
    Function GetChartFormats() As IEnumerable(Of ChartFormat)
    Function GetChartFormatById(chartFormatId As Integer) As ChartFormat
    Sub Save()
End Interface

Public Interface IChartTypeRepository
    Inherits IDisposable
    Function GetChartTypes() As IEnumerable(Of ChartType)
    Function GetChartTypeById(chartTypeId As Integer) As ChartType
    Sub Save()
End Interface

Public Interface IDashboardRepository
    Inherits IDisposable
    Function GetDashboardDetail() As IEnumerable(Of Dashboard)
    Function GetDashboardDetail(userId As Integer, analysisId As Integer, isValid As Boolean) As IEnumerable(Of Dashboard)
    Function GetDashboardDetailById(DashboardId As Integer) As Dashboard
    Sub UpdateDashboardDetail(DashboardDetail As Dashboard)
    Sub ReOrderDashboardItems(dashboardId As Integer, userId As Integer)
    Sub DeleteDashboardDetail(dashboardId As Integer)
    Function DeleteDashboardItemsByAnalysis(analysisId As Integer)
    Function DeleteDashboardItemsByUserId(userId As Integer)
    Sub Save()
End Interface

Public Interface IScorecardRepository
    Inherits IDisposable
    Function GetScorecardDetail() As IEnumerable(Of Scorecard)
    Function GetScorecardDetail(analysisId As Integer, isValid As Boolean) As IEnumerable(Of Scorecard)
    Function GetScorecardDetailById(ScorecardId As Integer) As Scorecard
    Sub UpdateScorecardDetail(ScorecardDetail As Scorecard)
    Function DeleteScorecardItemsByAnalysis(analysisId As Integer)
    Sub DeleteScorecardDetail(ScorecardId As Integer)        
    'Function DeleteDashboardItemsByUserId(userId As Integer)
    Sub Save()
End Interface


Public Interface ISaveViewRepository
    Inherits IDisposable
    Function GetSavedViews(analysisID As Integer) As IEnumerable(Of SaveView)
    Function GetSavedViewDetail(id As Integer) As SaveView
    Function DeleteSavedViewItemsByAnalysis(analysisId As Integer)
    Sub DeleteSavedView(savedViewID As Integer)
    Sub InsertSavedView(savedview As SaveView)
    Sub UpdateSavedViewName(savedview As SaveView)
    Sub Save()
End Interface


Public Interface ISubscriptionPlanRepository
    Inherits IDisposable
    Function GetSubscriptionPlans() As IEnumerable(Of SubscriptionPlan)
    Function GetSubscriptionPlan(planType As Integer) As SubscriptionPlan
    Function GetSubscriptionPlan(plan As String) As SubscriptionPlan
    Sub Save()
End Interface