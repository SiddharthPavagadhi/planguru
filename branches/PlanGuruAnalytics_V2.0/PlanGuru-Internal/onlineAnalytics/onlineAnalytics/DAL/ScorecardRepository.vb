﻿Imports System.Collections.Generic
Imports System.Linq
Imports System.Data


Public Class ScorecardRepository
    Inherits GenericRepository(Of Scorecard)
    Implements IScorecardRepository
    Private disposed As Boolean = False

    Public Sub New(context As DataAccess)
        MyBase.New(context)
    End Sub

    Public Function GetScorecardDetail() As System.Collections.Generic.IEnumerable(Of Scorecard) Implements IScorecardRepository.GetScorecardDetail
        Return context.Scorecards.ToList()
    End Function

    Public Function GetScorecardDetail(analysisId As Integer, isValid As Boolean) As System.Collections.Generic.IEnumerable(Of Scorecard) Implements IScorecardRepository.GetScorecardDetail
        Return context.Scorecards.Where(Function(d) d.AnalysisId = analysisId And d.IsValid = isValid).ToList()
    End Function

    Public Function GetScorecardDetailById(ScorecardId As Integer) As Scorecard Implements IScorecardRepository.GetScorecardDetailById
        Return context.Scorecards.Where(Function(d) d.ScorecardId = ScorecardId).SingleOrDefault()
    End Function

    Public Sub UpdateScorecardDetail(scorecardDetail As Scorecard) Implements IScorecardRepository.UpdateScorecardDetail
        context.Entry(scorecardDetail).State = EntityState.Modified
    End Sub

    Public Sub DeleteScorecardDetail(ScorecardId As Integer) Implements IScorecardRepository.DeleteScorecardDetail
        Dim scorecard As Scorecard = context.Scorecards.Find(ScorecardId)
        context.Scorecards.Remove(scorecard)
    End Sub

    Public Function DeleteScorecardItemsByAnalysis(analysisId As Integer) Implements IScorecardRepository.DeleteScorecardItemsByAnalysis
        Logger.Log.Info(("Start Query : Get the list of Accounts by AnalysisId: " & analysisId))
        Dim scorecard = (From d In context.Scorecards.Where(Function(a) a.AnalysisId = analysisId)
                                       Select d)
        If Not (scorecard Is Nothing And scorecard.Count > 0) Then
            Logger.Log.Info(("Scorecard items count : " & scorecard.Count))
            Logger.Log.Info("Start deleting : Scorecard items")
            scorecard.ToList().ForEach(Sub(item) context.Scorecards.Remove(item))
            context.SaveChanges()
            Logger.Log.Info(("End deleting : Scorecard items"))
        End If
        Return scorecard.Count
    End Function

    Public Sub Save() Implements IScorecardRepository.Save
        context.SaveChanges()
    End Sub

    Protected Overridable Sub Dispose(disposing As Boolean)
        If Not Me.disposed Then
            If disposing Then
                context.Dispose()
            End If
        End If
        Me.disposed = True
    End Sub

    Public Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

End Class
