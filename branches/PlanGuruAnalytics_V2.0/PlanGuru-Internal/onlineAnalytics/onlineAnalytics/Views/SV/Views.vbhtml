﻿@ModelType  PagedList.IPagedList(Of onlineAnalytics.SaveView)
@Imports Farpoint.Web.Spread
@Imports Farpoint.Mvc.Spread
@Imports  onlineAnalytics
@Code
    ViewData("Title") = "PlanGuru Analytics | Saved Reports"
    If Not Session("action") Is Nothing Then
        
        If (Session("action") = "SetDefaultAnalysis" Or Session("action") = "GetAnalyses") Then
            Layout = Nothing
            Session.Remove("action")
        End If
    Else
        Layout = "~/Views/Shared/AnalyticsMaster.vbhtml"
    End If
    
    Dim UserInfo As User = Nothing
    Dim ua As New UserAccess
    
    Dim objSetup As New SetupCount
    
    If Not (Session("UserInfo") Is Nothing) Then
        UserInfo = DirectCast(Session("UserInfo"), User)
    End If
    
    If (Not ViewBag.Setup Is Nothing) Then
        objSetup = DirectCast(ViewBag.Setup, SetupCount)
    End If
    
    Dim index As Integer = 1
    
    If Not (Session("UserAccess") Is Nothing) Then
        ua = DirectCast(Session("UserAccess"), UserAccess)
    End If
End Code
<div class="headerTable" style="float: right">
    <table>
        <tr>
            <td align="left" width="70%">
                Saved Reports List
            </td>             
        </tr>
    </table>
</div>
<div class="center-panel">
    <div class="center-tablesection">
        <div class="CSSTableGenerator">
            <table>
                @code
                    If Model.TotalItemCount > 0 Then
                    @<thead><tr>
                        <th class="left" style="width: 80%">
                            Saved Report Name
                        </th>                        
                        @Code
                                If (ua.UpdateSavedViews  = True) Then
                            @<th style="width: 10%">
                                Edit
                            </th>
                                End If
                        End Code
                        @Code
                                If (ua.DeleteSavedViews  = True) Then
                            @<th style="width: 10%">
                                Delete
                            </th>
                                End If
                        End Code                        
                    </tr>
                    </thead>
                        For Each item In Model
                            Dim currentItem = item
                            Dim cssClass As String = If(index Mod 2 = 0, "even", "")
                    @<tr class='@cssClass'>
                        <td>
                            @Html.DisplayFor(Function(modelItem) currentItem.viewname)
                        </td>                       
                        @Code
                                    If (ua.UpdateSavedViews = True) Then
                            @<td>
                                <a href='@Url.Action("Edit", "SV", New With {.id = currentItem.ID})'>
                                    <img alt="Edit Saved Report" src='@Url.Content("~/Content/Images/edit.png")' title="Edit Saved Report" />
                                </a>
                            </td>
                                    End If
                        End Code
                        @Code
                                    If (ua.DeleteSavedViews = True) Then
                            @<td>
                                <a id='@currentItem.ID' class="Delete" href="#" data='@Url.Action("Delete", "SV", New With {.savedViewID = currentItem.ID})' >
                                    <img alt="Delete Saved Report" src='@Url.Content("~/Content/Images/delete.png")' title="Delete Saved Report" /></a>
                            </td>
                                    End If
                        End Code                        
                    </tr>                             
                            index += 1
                        Next
                    Else
                    @<tr>
                        <td style="text-align: center;">
                            No saved reports found.
                        </td>
                    </tr>                    
                    End If
                End Code
            </table>
        </div>
        @code
            If Model.TotalItemCount > 0 Then
            @<div class="pagination-panel">
                @If Model.HasPreviousPage Then
                    @<a href='@Url.Action("Views", "SV", New With {.page = Model.PageNumber - 1}) ' class="prevous">Previous</a>
                    Else
                    @<a class="no-prevous">Previous</a>
                    End If
                <div class="pagination-list">
                    <ul>
                        @For index = 1 To Model.PageCount
                            @<li><a href='@Url.Action("Views", "SV", New With {.page = index})' >@index</a></li>
                            Next
                    </ul>
                </div>
                @If Model.HasNextPage Then
                    @<a href='@Url.Action("Views", "SV", New With {.page = Model.PageNumber + 1}) ' class="next">Next</a>
                    Else
                    @<a class="no-next">Next</a>
                    End If
            </div>
            End If
        End Code
        
        @If Not (DirectCast(TempData("Message"), String) Is Nothing) Then
            @<label class="success">@TempData("Message").ToString()
            </label>                         
        End If
        @If Not (DirectCast(TempData("InfoMessage"), String) Is Nothing) Then
            @<label class="info">@TempData("InfoMessage").ToString()
            </label>                         
        End If
        @If Not (DirectCast(TempData("ErrorMessage"), String) Is Nothing) Then
            @<label class="error">@TempData("ErrorMessage").ToString()
            </label>                         
        End If
    </div>
</div>

<script type="text/javascript" language="javascript">

    $(".Delete").click(function (event) {
        IsSessionAlive();
        event.preventDefault();
        var url = $(this).attr('data');

        $.msgBox({
            title: "Confirm",
            content: "Are you sure you want to delete this saved report?",
            type: "confirm",
            buttons: [{ value: "Yes" }, { value: "No"}],
            success: function (result) {
                if (result == "Yes") {
                    window.location = url;
                }
            },
            afterShow: function () { $('[name=No]').focus(); }
        });

    });

    function IsSessionAlive() {
        $.ajax({
            type: "GET",
            url: '@Url.Action("IsSessionAlive", "Dashboard")',
            global: false,
            cache: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (msg) {

                if (msg == undefined || (msg == "") || msg == false) {

                    window.location = '@Url.Action("Index", "Home", New With {.session = "-1"})';
                }

            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {                
            }
        });
    }
</script>