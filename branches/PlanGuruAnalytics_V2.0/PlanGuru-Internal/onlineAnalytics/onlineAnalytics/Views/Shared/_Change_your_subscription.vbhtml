﻿@Imports onlineAnalytics
@Code
    
    Dim CustomerInfo As Customer = Nothing 
    If Not (Session("UserInfo") Is Nothing) Then      
        CustomerInfo = DirectCast(Session("CustomerInfo"),Customer)
    End If
    
End Code
<div id="choose_subscription_container" style="display: none; font-family: Arial;
    font-size: 14px;">
    <br />
    <h3>
        PlanGuru Analytics Subscription Plans</h3>
    <p style="margin: 15px 0px;">
        We offer two types of subscription plan</p>
    <ul style="margin: 0px 0px 0px 40px;">
        <li style="list-style: disc; padding: 0px 0px 0px 13px;"><b>Per User Plan</b>
            <div style="margin: 10px 0px 0px 0px;">
                This plan starts at $19.95 a month and includes one user. Additional users can be
                added to the plan at a cost of $19.95 per user.</div>
        </li>
        <br />
        <li style="list-style: disc; padding: 0px 0px 0px 13px;"><b>Premium Group Plan</b>
            <div style="margin: 5px 0px 0px 0px;">
                This plan includes 4 users and start at $59.95 per month. After 4th user, additional
                users can be added to the subscription at a cost of $14.95 per month per user. In
                addition to a lower cost per user, this plan also includes our "Branding" feature.
                This feature allows you to brand your PlanGuru Analytics subscription with your
                firm's logo, custom footnote and theme color selection.
            </div>
        </li>
        <br />
    </ul>
    <div>
        <p>Your current plan selection is : <b>@CustomerInfo.PlanType.ToString()</b></p>    
    </div>
    <br />
    <div>
        <p>
            To change your subscription plan call us at 888-822-6300.</p>
    </div>
</div>
