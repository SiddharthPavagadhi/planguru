﻿Imports System.ComponentModel.DataAnnotations
Imports System.Data.Entity


Public Class Analysis   
    Public Property AnalysisId As Integer

    <Required(ErrorMessage:="Analysis Name is required.")> _
    <MaxLength(50)> _
    <Display(Name:="Analysis Name")> _
    Public Property AnalysisName() As String

    <Required(ErrorMessage:="Select Company")> _
    <Display(Name:="Company")> _
    Public Property CompanyId() As Integer

    <NotMapped()>
    Public Property CompanyName() As String

    <NotMapped()>
    Public Property AvailableUsers() As IList(Of User_)

    <NotMapped()>
    Public Property SelectedUsers() As IList(Of User_)

    <NotMapped()> _
    Public Property PostedUsers() As PostedUsers

    Public Property ShowBS() As Boolean

    Public Property ShowRatios() As Boolean

    Public Property Option1() As Boolean

    Public Property CreatedBy() As String

    Public Property CreatedOn() As DateTime = DateTime.Now()

    Public Property UpdatedBy() As String

    Public Property UpdatedOn() As DateTime = DateTime.Now()

    Public Overridable Property Company() As Company

End Class


