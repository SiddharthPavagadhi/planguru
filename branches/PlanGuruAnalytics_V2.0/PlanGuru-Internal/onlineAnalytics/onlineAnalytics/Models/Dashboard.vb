﻿Imports System.ComponentModel.DataAnnotations

Public Class Dashboard

    Public Property DashboardId As Integer

    Public Property Order As Integer

    <StringLength(50)>
    Public Property DashDescription As String

    <Required(ErrorMessage:="Select Chart.")>
    <Display(Name:="Format")> _
    Public Property ChartFormatId As Integer

    <Required(ErrorMessage:="Select Chart Type.")>
    <Display(Name:="Chart Type")> _
    Public Property ChartTypeId As Integer

    <Display(Name:="Show As Percent")> _
    Public Property ShowAsPercent As Boolean

    <Display(Name:="Show Trend")> _
    Public Property ShowTrendline As Boolean

    <Display(Name:="Show Goal")> _
    Public Property ShowGoal As Boolean

    <Display(Name:="Show Budget")> _
    Public Property ShowBudget As Boolean

    '<RegularExpression("^-?[0-9]+(\.[0-9]{1,5})?$")>
    <Display(Name:="Goal")> _
    Public Property Period1Goal As Decimal

    '<RegularExpression("^-?[0-9]+(\.[0-9]{1,5})?$")>
    <Display(Name:="Goal Growth Rate")> _
    Public Property GoalGrowthRate As Decimal

    <RegularExpression("^[1-9]+[0-9]*$", ErrorMessage:="Please select the item to be listed in the dashboard.")>
    <Required(ErrorMessage:="Please select the item to be listed in the dashboard.")>
    Public Property AccountId As Integer

    <NotMapped()>
    Public Property AcctTypeId As Integer
    Public Property UserId As Integer
    Public Property AnalysisId As Integer



    Public Property Option1 As Integer

    Public Property Option2 As Integer

    Public Property Option3 As Integer

    Public Property IsValid As Boolean

    Public Property CreatedBy() As String

    Public Property CreatedOn() As DateTime = DateTime.Now()

    Public Property UpdatedBy() As String

    Public Property UpdatedOn() As DateTime = DateTime.Now()

    <NotMapped()>
    Public Property selectedCheckboxId As String
    <NotMapped()>
    Public Property selectedCheckboxDescription As String
    <NotMapped()>
    Public Property selectedTabId As String
  

    Public Overridable Property ChartType() As ChartType
    Public Overridable Property ChartFormat() As ChartFormat


End Class

Public Class SaveView
    Public Property ID() As Integer
    Public Property analysisID() As Integer
    <Required(ErrorMessage:="View Name is required.")> _
    <MaxLength(50)> _
    <Display(Name:="View Name")>
    Public Property viewname() As String
    <StringLength(3)>
    Public Property controller() As String
    Public Property format() As Integer
    Public Property showaspercent() As Boolean
    Public Property showbudget() As Boolean
    Public Property highlightva() As Boolean
    Public Property posvar() As Nullable(Of Boolean)
    Public Property posvaramt() As Nullable(Of Decimal)
    Public Property negvar() As Nullable(Of Boolean)
    Public Property negvaramt() As Nullable(Of Decimal)
    Public Property filter() As Boolean
    Public Property filteron() As Nullable(Of Integer)
    Public Property filteramt() As Nullable(Of Decimal)
    Public Property charttype() As Integer
    Public Property trend() As Nullable(Of Boolean)
    Public Property account1() As Nullable(Of Integer)
    Public Property account2() As Nullable(Of Integer)
    Public Property account3() As Nullable(Of Integer)
    Public Property account4() As Nullable(Of Integer)
    Public Property account5() As Nullable(Of Integer)
    Public Property account6() As Nullable(Of Integer)
    Public Property account7() As Nullable(Of Integer)
    Public Property account8() As Nullable(Of Integer)
    Public Property account9() As Nullable(Of Integer)
    Public Property account10() As Nullable(Of Integer)
End Class

Public Enum FilterOn
    Current = 0
    Variance = 1
    PercentVar = 2
End Enum