﻿Imports System.Dynamic
Imports System.Data.Entity
Imports System.Web
Imports System.Web.Mvc
Imports onlineAnalytics
Imports MvcCheckBoxList.Model
Imports System.Data.Entity.Validation
Imports System.IO
Imports System.Text.RegularExpressions

Namespace onlineAnalytics

    Public Class BrandingController
        Inherits System.Web.Mvc.Controller

        '
        ' GET: /Branding
        Private utility As New Utility()
        Private customerRepository As ICustomerRepository
        Private userRepository As IUserRepository

        Private UserInfo As User

        Public Sub New()
            Me.customerRepository = New CustomerRepository(New DataAccess())
        End Sub
        Public Sub New(userRepository As IUserRepository)
            Me.userRepository = userRepository
        End Sub

        Function Index() As ActionResult

            Dim customer As Customer = Nothing
            Dim BrandingOptions As New Branding
            Try
                If Not (Session("UserInfo") Is Nothing) Then
                    UserInfo = DirectCast(Session("UserInfo"), User)
                End If
                customer = utility.CustomerRepository.GetCustomerById(UserInfo.CustomerId)

                BrandingOptions.PrimaryColor = customer.PrimaryColor
                BrandingOptions.SecondaryColor = customer.SecondaryColor
                BrandingOptions.Footnote = customer.FootNote

            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Error occured while showing branding - ", ex.Message)
                Logger.Log.Error(String.Format("\n Error occured while showing branding - with Message- {0} " + Environment.NewLine + "Stack Trace: {1} ", ex.Message, ex.StackTrace))
            End Try
            Return View(BrandingOptions)
        End Function

        <HttpPost()>
        <CustomActionFilter()> _
        Function Index(BrandingOptions As Branding) As ActionResult

            Dim customer As Customer = Nothing
            Try
                If ModelState.IsValid Then

                    If Not (Session("UserInfo") Is Nothing) Then
                        UserInfo = DirectCast(Session("UserInfo"), User)
                    End If

                    customer = utility.CustomerRepository.GetCustomerById(UserInfo.CustomerId)

                    'Update New filepath with old filepath
                    customer.PrimaryColor = BrandingOptions.PrimaryColor
                    customer.SecondaryColor = BrandingOptions.SecondaryColor
                    customer.FootNote = IIf(BrandingOptions.Footnote Is Nothing, String.Empty, BrandingOptions.Footnote)

                    customer.UpdatedBy = UserInfo.UserId
                    utility.CustomerRepository.UpdateCustomer(customer)
                    utility.CustomerRepository.Save()

                    UserInfo.Customer.PrimaryColor = BrandingOptions.PrimaryColor
                    UserInfo.Customer.SecondaryColor = BrandingOptions.SecondaryColor
                    UserInfo.Customer.FootNote = BrandingOptions.Footnote

                    Session("UserInfo") = UserInfo

                    TempData("Message") = "Branding Options updated successfully."
                End If
            Catch ex As Exception

                TempData("ErrorMessage") = String.Concat("Error occured while updating branding - ", ex.Message)
                Logger.Log.Error(String.Format("\n Error occured while updating branding - with Message- {0} " + Environment.NewLine + "Stack Trace: {1} ", ex.Message, ex.StackTrace))

            End Try
            Return View(BrandingOptions)
        End Function

        <HttpPost()>
          <CustomActionFilter()>
        Function UploadFile() As JsonResult

            Dim customer As Customer = Nothing
            Dim OldFilePath As String
            Dim data = Nothing
            Dim hpf As HttpPostedFileBase = Nothing
            Dim savedFileName As String = String.Empty
            Dim filename As String = String.Empty
            Dim specialCharacterRegEx As String = "[^a-zA-Z0-9.]+"

            Try
                If Not (Session("UserInfo") Is Nothing) Then
                    UserInfo = DirectCast(Session("UserInfo"), User)
                End If

                For Each File As String In Request.Files

                    customer = utility.CustomerRepository.GetCustomerById(UserInfo.CustomerId)
                    OldFilePath = customer.LogoImageFile


                    hpf = TryCast(Request.Files(File), HttpPostedFileBase)
                    filename = Path.GetFileNameWithoutExtension(hpf.FileName)                   
                    filename = Regex.Replace(filename, specialCharacterRegEx, "")
                    filename = String.Concat(filename, "_", DateTime.UtcNow.ToString("dd-MM-yyyy_HHmmss").ToString(), Path.GetExtension(hpf.FileName))

                    savedFileName = Path.Combine(Server.MapPath("~/Branding_Logos"), filename)

                    hpf.SaveAs(savedFileName)

                    'Update New filepath with old filepath
                    customer.LogoImageFile = filename
                    customer.UpdatedBy = UserInfo.UserId
                    utility.CustomerRepository.UpdateCustomer(customer)
                    utility.CustomerRepository.Save()

                    UserInfo.Customer.LogoImageFile = filename

                    Session("UserInfo") = UserInfo

                    'Delete old saved file
                    If (System.IO.File.Exists(OldFilePath)) Then
                        System.IO.File.Delete(OldFilePath)
                    End If


                Next

                data = New With {.status = "success", .src = String.Concat("././Branding_Logos/", filename)}

                Return Json(data, JsonRequestBehavior.AllowGet)
            Catch ex As Exception
                data = New With {.status = "error", .error = ex.Message.ToString()}
                Return Json(data, JsonRequestBehavior.AllowGet)
            End Try

        End Function

        <HttpPost()>
        <CustomActionFilter()>
        Function ResetMenuColors() As JsonResult
            Dim data = Nothing
            Dim customer As Customer = Nothing
            Try
                If Not (Session("UserInfo") Is Nothing) Then
                    UserInfo = DirectCast(Session("UserInfo"), User)
                End If

                customer = utility.CustomerRepository.GetCustomerById(UserInfo.CustomerId)

                customer.PrimaryColor = "#276FA6"
                customer.SecondaryColor = "#1B3E70"
                customer.UpdatedBy = UserInfo.UserId
                utility.CustomerRepository.UpdateCustomer(customer)
                utility.CustomerRepository.Save()
                Session("UserInfo") = UserInfo

                UserInfo.Customer.PrimaryColor = "#276FA6"
                UserInfo.Customer.SecondaryColor = "#1B3E70"

                data = New With {.status = "success"}
                Return Json(data, JsonRequestBehavior.AllowGet)
            Catch ex As Exception
                data = New With {.status = "error", .error = ex.Message.ToString()}
                Return Json(data, JsonRequestBehavior.AllowGet)
            End Try
        End Function
    End Class
End Namespace
