﻿Imports FarPoint.Mvc.Spread
Imports System.Data.Entity
Namespace onlineAnalytics
    <CustAuthFilter()>
    Public Class CFController
        Inherits System.Web.Mvc.Controller
        Dim sFormat As Integer = 1
        Dim sPeriod As Integer = Val(DateTime.Today.ToString("MM")) - 2
        Dim intFirstNumberColumn As Integer = 6
        Private intUserID As Integer
        Private intAnalysisID As Integer
        Private intFirstFiscal As Integer
        Private intNumberofPeriods As Integer
        Private isViewer As Boolean = False
        Private accountRepository As IAccountRepository
        Private accountTypeRepository As IAccountTypeRepository
        Private balanceRepository As IBalanceRepository
        Private chartFormatRepository As IChartFormatRepository
        Private chartTypeRepository As IChartTypeRepository
        Private saveViewRepository As ISaveViewRepository
        Private UserInfo As User
        Private typeDescList() As String = {"cashflow"}
        Private blnArchBudget As Boolean

        Public Sub New()
            Me.chartFormatRepository = New ChartFormatRepository(New DataAccess())
            Me.chartTypeRepository = New ChartTypeRepository(New DataAccess())
            Me.accountRepository = New AccountRepository(New DataAccess())
            Me.accountTypeRepository = New AccountTypeRepository(New DataAccess())
            Me.balanceRepository = New BalanceRepository(New DataAccess())
            Me.saveViewRepository = New SaveViewRepository(New DataAccess())
        End Sub


        Public Sub New(chartFormatRepository As IChartFormatRepository)
            Me.chartFormatRepository = chartFormatRepository
        End Sub

        Public Sub New(chartTypeRepository As IChartTypeRepository)
            Me.chartTypeRepository = chartTypeRepository
        End Sub

        Public Sub New(accountRepository As IAccountRepository)
            Me.accountRepository = accountRepository
        End Sub

        Public Sub New(accountTypeRepository As IAccountTypeRepository)
            Me.accountTypeRepository = accountTypeRepository
        End Sub

        Public Sub New(balanceRepository As IBalanceRepository)
            Me.balanceRepository = balanceRepository
        End Sub

        Public Sub New(saveViewRepository As ISaveViewRepository)
            Me.saveViewRepository = saveViewRepository
        End Sub

        'Display Account type & Account items with options.
        'List of Companies, List of Month, List of Format & Chart Type
        'Options to select (positive / negative) variance, percentage
        ' GET: /RE
        <HttpGet()> _
        <CustomActionFilter()>
        Function Index(<MvcSpread("spdAnalytics")> ByVal spdAnalytics As FpSpread, ByVal formValues As FormValues) As ActionResult
            Dim setupCountObj As SetupCount
            Dim selectedAnalysis As Integer
            Try
                Logger.Log.Info(String.Format("CFController Index (HttpGet) method execution starts"))
                blnArchBudget = DirectCast(Session("BudgetArch"), Boolean)
                If (Not Session("UserType") Is Nothing) And (Not Session("UserInfo") Is Nothing) Then
                    UserInfo = DirectCast(Session("UserInfo"), User)
                    sPeriod = Session("sPeriod")
                    sFormat = Session("sFormat")


                    If (UserInfo.AnalysisId = 0) Then
                        Return RedirectToAction("Index", "Dashboard")
                    Else
                        intAnalysisID = UserInfo.AnalysisId
                        intFirstFiscal = UserInfo.fiscalMonthOfSelectedCompany
                    End If
                End If

                If (UserInfo.AnalysisId > 0) Then
                    selectedAnalysis = UserInfo.AnalysisId
                Else
                    If (Not Session("SelectedAnalysisFromDropdown") Is Nothing) Then
                        selectedAnalysis = Session("SelectedAnalysisFromDropdown")
                    Else
                        selectedAnalysis = -1
                    End If
                End If
                setupCountObj = Utility.CheckRequiredSetupCount(UserInfo, selectedAnalysis)
                ViewBag.Setup = setupCountObj


                Dim intCtr As Integer
                Dim periods As List(Of SelectListItem) = New List(Of SelectListItem)
                intNumberofPeriods = NumberofPeriods(sPeriod, intFirstFiscal)
                'For intCtr = 0 To intNumberofPeriods
                For intCtr = 0 To 11
                    periods.Add(New SelectListItem With {.Text = CommonProcedures.GetMonth(intCtr, intFirstFiscal), .Value = intCtr + 1, .Selected = False})
                Next
                ViewData("Periods") = New SelectList(periods, "value", "text", "May")

                Dim Formats As List(Of SelectListItem) = (From chartFormat In chartFormatRepository.GetChartFormats() Select New SelectListItem With {.Text = chartFormat.Format, .Value = chartFormat.ChartFormatId, .Selected = False}).ToList()

                ViewData("Formats") = New SelectList(Formats, "value", "text")

                Dim filterOn As List(Of SelectListItem) = New List(Of SelectListItem) From {
                    New SelectListItem With {.Text = "Current period amount", .Value = "Current", .Selected = False},
                    New SelectListItem With {.Text = "Variance amount", .Value = "Variance", .Selected = False},
                    New SelectListItem With {.Text = "Percent variance", .Value = "PercentVar", .Selected = True}
                }
                ViewData("FilterOn") = New SelectList(filterOn, "value", "text", "selected")

                Dim chartTypes As List(Of SelectListItem) = (From chartType In chartTypeRepository.GetChartTypes() Select New SelectListItem With {.Text = chartType.Type, .Value = chartType.ChartTypeId, .Selected = False}).ToList()

                ViewData("ChartType") = New SelectList(chartTypes, "value", "text", "selected")
                ViewData("blnHighVar") = False
                ViewData("blnShowPercent") = 0
                ViewData("intPeriod") = sPeriod + 1
                ViewData("intFormat") = sFormat + 1
                ViewData("intChartType") = 1
                ViewData("blnShowTrend") = False
                ViewData("blnShowBudget") = True
                ViewData("blnUseFilter") = False
                ViewData("intDept") = 1
                ViewData("Postback") = False
                If blnArchBudget = True Then
                    ViewData("strCheckboxCaption") = "Show forecast amounts"
                Else
                    ViewData("strCheckboxCaption") = "Show budget amounts"
                End If
                Logger.Log.Info(String.Format("CFController Index (HttpGet) method execution Ends"))
            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Error occured in Quick Reports --> Cash Flow", ex.Message)
                Logger.Log.Error(String.Format("CFController Index (HttpGet) method execution ends with Error message: {0}, StackTrace: {1}", ex.Message, ex.StackTrace))
            End Try
            Return View()
        End Function

        'Post 
        'It will render the chart based on selected options available for accounts.
        <HttpPost()> _
        <CustomActionFilter()>
        Function Index(<MvcSpread("spdAnalytics")> ByVal spdAnalytics As FpSpread, <MvcSpread("spdChart")> ByVal spdChart As FpSpread, ByVal formValues As FormValues) As ActionResult
            ' Dim intCounter As Integer
            Dim setupCountObj As SetupCount
            Dim selectedAnalysis As Integer
            Logger.Log.Info(String.Format("CFController Index (HttpPost) method execution Starts"))
            Try
                blnArchBudget = DirectCast(Session("BudgetArch"), Boolean)
                If (Not Session("UserType") Is Nothing) And (Not Session("UserInfo") Is Nothing) Then
                    UserInfo = DirectCast(Session("UserInfo"), User)
                    If (UserInfo.AnalysisId = 0) Then
                        Return RedirectToAction("Index", "Dashboard")
                    Else
                        intAnalysisID = UserInfo.AnalysisId
                        intFirstFiscal = UserInfo.fiscalMonthOfSelectedCompany
                    End If
                End If

                If (UserInfo.AnalysisId > 0) Then
                    selectedAnalysis = UserInfo.AnalysisId
                Else
                    If (Not Session("SelectedAnalysisFromDropdown") Is Nothing) Then
                        selectedAnalysis = Session("SelectedAnalysisFromDropdown")
                    Else
                        selectedAnalysis = -1
                    End If
                End If
                setupCountObj = Utility.CheckRequiredSetupCount(UserInfo, selectedAnalysis)
                ViewBag.Setup = setupCountObj

                If (spdChart.Sheets(0).Charts.Count > 0) Then
                    spdChart.Sheets(0).Charts.Remove(spdChart.Sheets(0).Charts(0))
                End If

            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Error occured in viewing Quick Reports --> Cash Flow", ex.Message)
                Logger.Log.Error(String.Format("CFController Index (HttpPost) method execution ends with Error message: {0}, StackTrace: {1}", ex.Message, ex.StackTrace))
            End Try
            Try
                spdAnalytics.Sheets(0).RowCount = 0
                'Select Case formValues.intFormat - 1
                '    Case 5
                '        'do nothing
                '    Case Else
                '        If formValues.intPeriod - 1 > sPeriod Then
                '            formValues.intPeriod = sPeriod + 1
                '        End If
                'End Select
                SetSpreadProperties(spdAnalytics, formValues.intFormat - 1, formValues.intPeriod - 1, formValues.blnShowasPer, formValues.blnShowBudget)
                If formValues.intChartType <> 1 Then
                    Charts.BuildChart(spdChart, spdAnalytics, formValues.intChartType, formValues.intFormat - 1, intFirstNumberColumn, formValues.blnShowasPer, formValues.blnShowTrend, False)
                    ViewData("blnUpdateChart") = True
                End If

                Dim periods As List(Of SelectListItem) = New List(Of SelectListItem)
                intNumberofPeriods = NumberofPeriods(sPeriod, intFirstFiscal)
                'For intCtr = 0 To intNumberofPeriods
                For intCtr = 0 To 11
                    periods.Add(New SelectListItem With {.Text = CommonProcedures.GetMonth(intCtr, intFirstFiscal), .Value = intCtr + 1, .Selected = False})
                Next
                ViewData("Periods") = New SelectList(periods, "value", "text", "May")

                Dim formats As List(Of SelectListItem) = New List(Of SelectListItem)
                For Each chartFormat As ChartFormat In chartFormatRepository.GetChartFormats()
                    formats.Add(New SelectListItem With {.Text = chartFormat.Format, .Value = chartFormat.ChartFormatId, .Selected = False})
                Next
                ViewData("Formats") = New SelectList(formats, "value", "text")

                Dim filterOn As List(Of SelectListItem) = New List(Of SelectListItem) From {
                    New SelectListItem With {.Text = "Current period amount", .Value = "Current", .Selected = False},
                    New SelectListItem With {.Text = "Variance amount", .Value = "Variance", .Selected = False},
                    New SelectListItem With {.Text = "Percent variance", .Value = "PercentVar", .Selected = True}
                }
                ViewData("FilterOn") = New SelectList(filterOn, "value", "text", "selected")

                Dim chartTypes As List(Of SelectListItem) = New List(Of SelectListItem)
                For Each chartType As ChartType In chartTypeRepository.GetChartTypes()
                    chartTypes.Add(New SelectListItem With {.Text = chartType.Type, .Value = chartType.ChartTypeId, .Selected = False})
                Next
                ViewData("ChartType") = New SelectList(chartTypes, "value", "text", "selected")
                ViewData("blnHighVar") = False
                ViewData("blnShowPercent") = 0
                ViewData("Period") = formValues.intPeriod - 1
                ViewData("intDept") = 1
                ViewData("intFormat") = formValues.intFormat
                If blnArchBudget = True Then
                    ViewData("strCheckboxCaption") = "Show forecasted amounts"
                Else
                    ViewData("strCheckboxCaption") = "Show budgeted amounts"
                End If
                ViewData("postback") = True
                Session("sPeriod") = formValues.intPeriod - 1
                Session("sFormat") = formValues.intFormat - 1

                Logger.Log.Info(String.Format("REController Index (HttpPost) method execution Ends"))
            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Error occured in viewing ty Quick Reports --> Cash Flow ", ex.Message)
                Logger.Log.Error(String.Format("REController Index (HttpPost) method execution ends with Error message: {0}, StackTrace: {1}", ex.Message, ex.StackTrace))
            End Try
            Return View()
        End Function

        <MvcSpreadEvent("Load", "spdAnalytics", DirectCast(Nothing, String()))> _
        Private Sub FpSpread_Load(ByVal sender As Object, ByVal e As EventArgs)
            Try
                Logger.Log.Info(String.Format("REController:FpSpread_Load method execution Starts"))
                Dim spread As FpSpread = DirectCast(sender, FpSpread)
                If Not spread.Page.IsPostBack Then
                    SetSpreadProperties(spread, sFormat, intNumberofPeriods, False, False)
                End If
                Logger.Log.Info(String.Format("REController:FpSpread_Load method execution Ends"))
            Catch ex As Exception
                Logger.Log.Error(String.Format("REController FpSpread_Load method execution ends with Error message: {0}, StackTrace: {1}", ex.Message, ex.StackTrace))
            End Try

        End Sub

        <MvcSpreadEvent("PreRender", "spdAnalytics", DirectCast(Nothing, String()))> _
        Private Sub FpSpread_PreRender(ByVal sender As Object, ByVal e As EventArgs)
            Try
                Logger.Log.Info(String.Format("REController:FpSpread_PreRender method execution Starts"))
                Dim spread As FpSpread = DirectCast(sender, FpSpread)
                Logger.Log.Info(String.Format("REController:FpSpread_PreRender method execution Ends"))
            Catch ex As Exception
                Logger.Log.Error(String.Format("REController FpSpread_PreRender method execution ends with Error message: {0}, StackTrace: {1}", ex.Message, ex.StackTrace))
            End Try
        End Sub

        Private Sub SetSpreadProperties(ByVal spdAnalytics As FpSpread, ByVal intFormat As Integer, ByVal intPeriod As Integer, ByRef blnShowasPercent As Boolean, ByVal blnShowBudget As Boolean)
            Try
                Logger.Log.Info(String.Format("REController:SetSpreadProperties method execution Starts"))
                'Dim chkbox As New FarPoint.Web.Spread.CheckBoxCellType
                'Dim txtcell As New FarPoint.Web.Spread.GeneralCellType
                Dim intColCount As Integer = 0
                Dim intNumberColumn As Integer = 0
                Dim intPercentColumn As Integer = 0
                Dim intCtr As Integer
                Dim intColCtr As Integer

                spdAnalytics.HorizontalScrollBarPolicy = FarPoint.Web.Spread.ScrollBarPolicy.Never
                spdAnalytics.Sheets(0).GridLineColor = Drawing.Color.LightGray
                ''  spdAnalytics.Sheets(0).GridLines = GridLines.None
                'set column count
                Select Case intFormat
                    Case 0 To 4
                        intColCount = intFirstNumberColumn + 4
                        intNumberColumn = 3
                        intPercentColumn = 1
                    Case 5 'current year trend
                        If blnShowBudget = True Then
                            intNumberColumn = 11 + 1
                            intPercentColumn = 11 + 1
                        Else
                            intNumberColumn = intPeriod + 1
                            intPercentColumn = intPeriod + 1
                        End If
                        intColCount = intFirstNumberColumn + intNumberColumn
                    Case 6, 9
                        intNumberColumn = 12
                        intPercentColumn = 12
                        intColCount = intFirstNumberColumn + 12
                    Case 7, 8
                        intNumberColumn = 6
                        intPercentColumn = 6
                        intColCount = intFirstNumberColumn + 6
                End Select
                CommonProcedures.SetCommonProperties(spdAnalytics, intColCount, blnShowasPercent, intFirstNumberColumn, intNumberColumn, intFormat)
                '  spdAnalytics.Sheets(0).Columns(2).Visible = False
                'load rows
                Select Case intFormat
                    Case 0 To 4
                        LoadSpread0to4(spdAnalytics, intFormat, intPeriod, intNumberColumn, intFirstNumberColumn, intColCount, blnShowasPercent)
                        spdAnalytics.Sheets(0).Columns(intFirstNumberColumn, intFirstNumberColumn + 3).Visible = True
                    Case 5
                        LoadSpread5(spdAnalytics, intFormat, intPeriod, intNumberColumn, intFirstNumberColumn, blnShowasPercent, intColCount, blnShowBudget)
                        spdAnalytics.Sheets(0).Columns(intFirstNumberColumn, intFirstNumberColumn + intNumberColumn - 1).Visible = True
                    Case 6
                        LoadSpread6(spdAnalytics, intFormat, intPeriod, intNumberColumn, intFirstNumberColumn, blnShowasPercent, intColCount)
                        spdAnalytics.Sheets(0).Columns(intFirstNumberColumn, intFirstNumberColumn + intNumberColumn - 1).Visible = True
                    Case 7, 8
                        LoadSpread7to8(spdAnalytics, intFormat, intPeriod, intNumberColumn, intFirstNumberColumn, blnShowasPercent, intColCount)
                        Dim blnLoop As Boolean = True
                        intCtr = 0
                        intNumberColumn = 6
                        For intColCtr = intFirstNumberColumn To intFirstNumberColumn + 5
                            spdAnalytics.Sheets(0).Columns(intColCtr).Visible = True
                        Next
                        intCtr = -1
                        Do While blnLoop = True
                            intCtr += 1
                            If spdAnalytics.Sheets(0).GetValue(intCtr, 2) = "Total" Then
                                For intColCtr = intFirstNumberColumn To intFirstNumberColumn + 5
                                    If spdAnalytics.Sheets(0).GetValue(intCtr, intColCtr) = 0 Then
                                        spdAnalytics.Sheets(0).Columns(intColCtr).Visible = False
                                        intColCount -= 1
                                        intNumberColumn -= 1
                                    Else
                                        blnLoop = False
                                        Exit For
                                    End If
                                Next
                            End If
                            If intCtr = spdAnalytics.Sheets(0).RowCount - 1 Then
                                blnLoop = False
                            End If
                        Loop
                    Case 9
                        LoadSpread9(spdAnalytics, intFormat, intPeriod, intNumberColumn, intFirstNumberColumn, blnShowasPercent, intColCount)
                        spdAnalytics.Sheets(0).Columns(intFirstNumberColumn, intFirstNumberColumn + intNumberColumn - 1).Visible = True
                End Select
                'set col widths
                CommonProcedures.SetColumnWidths(spdAnalytics, intColCount, intFirstNumberColumn, intFormat, intNumberColumn, False, 4)
                spdAnalytics.Sheets(0).Columns(1).Visible = False
                'load column headers
                Dim strHeadingTextBudget As String
                If blnArchBudget = False Then
                    strHeadingTextBudget = "Budget"
                Else
                    strHeadingTextBudget = "Forecast"
                End If
                CommonProcedures.SetColumnHeader(spdAnalytics, intFormat, intPeriod, intColCount, intFirstNumberColumn, intFirstFiscal, False, strHeadingTextBudget)
                Logger.Log.Info(String.Format("REController:SetSpreadProperties method execution Ends"))
            Catch ex As Exception
                Logger.Log.Error(String.Format("REController:SetSpreadProperties method execution ends with Error message: {0}, StackTrace: {1}", ex.Message, ex.StackTrace))
            End Try

        End Sub

        <CustomActionFilter()>
        Public Sub LoadSpread0to4(ByVal spdAnalytics As FpSpread, ByVal intFormat As Integer, ByVal intPeriod As Integer, ByVal intNumberColumn As Integer, ByVal intFirstNumColumn As Integer, ByVal intColumnCount As Integer, ByRef blnShowasPercent As Boolean)
            Try
                Logger.Log.Info(String.Format("REController:LoadSpread0to4 method execution Starts"))
                Using utility As New Utility
                    Dim intRowCounter As Integer = -1
                    Dim intColumnCounter As Integer = 0
                    Dim strYearType(5) As String
                    Dim strPeriods(2) As String
                    Dim decAccumTotals(1) As Decimal
                    Dim decPeriodAmts(11) As Decimal
                    Dim decSaveAmts(4) As Decimal
                    Dim intSpanRows(1) As Integer
                    Dim chkbox As New FarPoint.Web.Spread.CheckBoxCellType
                    Dim blnTotalFlag As Boolean
                    Dim decIncAccumTotals(1) As Decimal
                    Dim strBrowser As String = Request.Browser.Browser
                    If (isViewer) Then
                        chkbox.OnClientClick = "return false"
                    Else
                        chkbox.OnClientClick = CommonProcedures.CheckboxClicked(2)
                    End If



                    Logger.Log.Info(String.Format("REController:LoadSpread0to4 --> selectedAccounts Query execution Starts"))

                    Dim selectedAccounts = (From a In utility.AccountRepository.GetAccountRecordsByAnalysisId(intAnalysisID)
                                           Join ac In utility.AccountTypeRepository.GetAccountTypeRecordsByAnalysisId(intAnalysisID, typeDescList)
                                           On a.AcctTypeId Equals ac.AcctTypeId Where a.AnalysisId = ac.AnalysisId
                                           Order By a.SortSequence
                                           Select a.AccountId, a.AcctTypeId, a.AnalysisId, a.AcctDescriptor, a.SortSequence, a.Description, a.Subgrouping, a.TotalType, a.NumberFormat, ac.TypeDesc, ac.ClassDesc
                                         ).ToList()

                    Logger.Log.Info(String.Format("REController:LoadSpread0to4 --> selectedAccounts Query execution Ends"))

                    Dim balanceAcc = (From b In utility.BalanceRepository.GetBalanceRecordsByAnalysisId(intAnalysisID)
                                    Join acc In selectedAccounts On b.AccountId Equals acc.AccountId
                                    Where b.AnalysisId = acc.AnalysisId
                                    Select b).ToList()


                    Logger.Log.Info(String.Format("REController:LoadSpread0to4 --> selectedAccounts For Each execution Starts"))

                    For Each account In selectedAccounts
                        'Dim accountTypeId As Integer = account.AcctTypeId
                        'Dim acctType = (From a In utility.AccountTypeRepository.GetAccountTypes.Where(Function(at) at.AcctTypeId = accountTypeId) Select a).FirstOrDefault()
                        With account
                            AddRow(spdAnalytics, intRowCounter, .Description, .AcctDescriptor, .TypeDesc, strBrowser, 4)
                            Select Case .AcctDescriptor
                                Case "Heading"
                                    FormatHeaderRow(spdAnalytics, intRowCounter, intSpanRows, blnTotalFlag, 4)
                                Case "Total"
                                    FormatTotalRow(spdAnalytics, intRowCounter, intSpanRows, blnTotalFlag, intFirstNumColumn, intNumberColumn, .ClassDesc, strBrowser, 4)
                                    'Case "Detail"
                                    '    spdAnalytics.Sheets(0).Cells(intRowCounter, 0).Border.BorderColorBottom = Drawing.Color.White
                                Case "NetIncrease", "EndCash"
                                    Array.Clear(intSpanRows, 0, 1)
                                    FormatGrandTotalRow(spdAnalytics, intRowCounter, blnTotalFlag, intFirstNumberColumn, intNumberColumn, .ClassDesc, strBrowser, 4)
                            End Select
                        End With
                        'save accountID 
                        spdAnalytics.Sheets(0).SetValue(intRowCounter, 5, account.AccountId)
                        'save amounts                         
                        If account.AcctDescriptor <> "Heading" Then
                            Array.Clear(decPeriodAmts, 0, 12)
                            Array.Clear(decSaveAmts, 0, 5)
                            Dim accountId As Integer = account.AccountId
                            spdAnalytics.Sheets(0).Cells(intRowCounter, 2).CellType = chkbox
                            For Each balance As Balance In (From b In balanceAcc.Where(Function(a) a.AccountId = accountId And a.AnalysisId = intAnalysisID) Select b)
                                Select Case intFormat
                                    Case 0
                                        If blnArchBudget = False Then
                                            GetBudgetAmts(balance, decPeriodAmts)
                                        Else
                                            GetArchivedBudgetAmts(balance, decPeriodAmts)
                                        End If
                                        decSaveAmts(0) = decPeriodAmts(intPeriod)
                                        GetActualAmts(balance, decPeriodAmts)
                                        decSaveAmts(1) = decPeriodAmts(intPeriod)
                                    Case 1
                                        If blnArchBudget = False Then
                                            GetBudgetAmts(balance, decPeriodAmts)
                                        Else
                                            GetArchivedBudgetAmts(balance, decPeriodAmts)
                                        End If
                                        Select Case account.AcctDescriptor
                                            Case "BegCash"
                                                decSaveAmts(0) = decPeriodAmts(0)
                                            Case "EndCash"
                                                decSaveAmts(0) = decPeriodAmts(intPeriod)
                                            Case Else
                                                For intColumnCounter = 0 To intPeriod
                                                    decSaveAmts(0) += decPeriodAmts(intColumnCounter)
                                                Next
                                        End Select
                                        GetActualAmts(balance, decPeriodAmts)
                                        Select Case account.AcctDescriptor
                                            Case "BegCash"
                                                decSaveAmts(1) = decPeriodAmts(0)
                                            Case "EndCash"
                                                decSaveAmts(1) = decPeriodAmts(intPeriod)
                                            Case Else
                                                For intColumnCounter = 0 To intPeriod
                                                    decSaveAmts(1) += decPeriodAmts(intColumnCounter)
                                                Next
                                        End Select
                                    Case 2
                                        GetActualAmts(balance, decPeriodAmts)
                                        If intPeriod = 0 Then
                                            decSaveAmts(1) = decPeriodAmts(intPeriod)
                                            GetH5Amts(balance, decPeriodAmts)
                                            decSaveAmts(0) = decPeriodAmts(11)
                                        Else
                                            decSaveAmts(0) = decPeriodAmts(intPeriod - 1)
                                            decSaveAmts(1) = decPeriodAmts(intPeriod)
                                        End If
                                    Case 3
                                        GetH5Amts(balance, decPeriodAmts)
                                        decSaveAmts(0) = decPeriodAmts(intPeriod)
                                        GetActualAmts(balance, decPeriodAmts)
                                        decSaveAmts(1) = decPeriodAmts(intPeriod)
                                    Case 4
                                        GetH5Amts(balance, decPeriodAmts)
                                        Select Case account.AcctDescriptor
                                            Case "BegCash"
                                                decSaveAmts(0) = decPeriodAmts(0)
                                            Case "EndCash"
                                                decSaveAmts(0) = decPeriodAmts(intPeriod)
                                            Case Else
                                                For intColumnCounter = 0 To intPeriod
                                                    decSaveAmts(0) += decPeriodAmts(intColumnCounter)
                                                Next
                                        End Select
                                        GetActualAmts(balance, decPeriodAmts)
                                        Select Case account.AcctDescriptor
                                            Case "BegCash"
                                                decSaveAmts(1) = decPeriodAmts(0)
                                            Case "EndCash"
                                                decSaveAmts(1) = decPeriodAmts(intPeriod)
                                            Case Else
                                                For intColumnCounter = 0 To intPeriod
                                                    decSaveAmts(1) += decPeriodAmts(intColumnCounter)
                                                Next
                                        End Select
                                End Select
                            Next
                            'store values
                            If blnShowasPercent = False Then
                                spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumColumn, decSaveAmts(0))
                                spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumColumn + 1, decSaveAmts(1))
                            Else
                                If decAccumTotals(0) <> 0 Then
                                    spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumColumn, decSaveAmts(0) / decAccumTotals(0))
                                    decSaveAmts(0) = decSaveAmts(0) / decAccumTotals(0)
                                Else
                                    spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumColumn, 0)
                                    decSaveAmts(0) = 0
                                End If
                                If decAccumTotals(1) <> 0 Then
                                    spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumColumn + 1, decSaveAmts(1) / decAccumTotals(1))
                                    decSaveAmts(1) = decSaveAmts(1) / decAccumTotals(1)
                                Else
                                    spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumColumn + 1, 0)
                                    decSaveAmts(1) = 0
                                End If
                            End If
                            'compute(variances)
                            CommonProcedures.ComputeVariances(decSaveAmts, account.TypeDesc, decAccumTotals, blnShowasPercent)
                            spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumColumn + 2, decSaveAmts(2))
                            spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumColumn + 3, decSaveAmts(3))
                        End If
                        'If account.AcctDescriptor = "Total" Then
                        '    AccumNI(spdAnalytics, account.AcctType.ClassDesc, intFormat, 2, decSaveAmts, intColumnCount, decIncAccumTotals, intRowCounter, intPeriod)
                        'End If
                    Next
                    Logger.Log.Info(String.Format("REController:LoadSpread0to4 --> selectedAccounts For Each execution Ends"))
                    '    CommonProcedures.AddTotalRow(spdAnalytics, intFormat, intNumberColumn, intFirstNumColumn, intColumnCount, decIncAccumTotals, intRowCounter, "NI", "Net Income(Loss)", intPeriod)
                End Using
                Logger.Log.Info(String.Format("REController:LoadSpread0to4 method execution Ends"))
            Catch ex As Exception
                Logger.Log.Error(String.Format("REController:LoadSpread0to4 method execution ends with Error message: {0}, StackTrace: {1}", ex.Message, ex.StackTrace))
            End Try

        End Sub

        <CustomActionFilter()>
        Public Sub LoadSpread5(ByVal spdAnalytics As FpSpread, ByVal intFormat As Integer, ByRef intCurrentMonth As Integer, ByVal intNumberColumn As Integer, ByVal intFirstNumberColumn As Integer, ByRef blnShowasPercent As Boolean, ByVal intColumnCount As Integer, ByVal blnShowBudget As Boolean)
            Try
                Logger.Log.Info(String.Format("REController:LoadSpread5to6 method execution Starts"))

                Using utility As New Utility()
                    Dim intRowCounter As Integer = -1
                    Dim intColumnCounter As Integer
                    Dim intLastActualCol As Integer
                    Dim strYearType(5) As String
                    Dim strPeriods(2) As String
                    Dim decAccumTotals(12) As Decimal
                    Dim decPeriodAmts(11) As Decimal
                    Dim decSaveAmts(12) As Decimal
                    Dim intSpanRows(1) As Integer
                    Dim blnTotalFlag As Boolean
                    Dim intPeriods As Integer
                    Dim decIncAccumTotals(intNumberColumn) As Decimal
                    Dim chkbox As New FarPoint.Web.Spread.CheckBoxCellType
                    Dim strBrowser As String = Request.Browser.Browser

                    If (isViewer) Then
                        chkbox.OnClientClick = "return false"
                    Else
                        chkbox.OnClientClick = CommonProcedures.CheckboxClicked(1)
                    End If
                    If blnShowBudget = False Then
                        intPeriods = intCurrentMonth
                    Else
                        intPeriods = 11
                    End If

                    Dim selectedAccounts = (From a In utility.AccountRepository.GetAccountRecordsByAnalysisId(intAnalysisID)
                                           Join ac In utility.AccountTypeRepository.GetAccountTypeRecordsByAnalysisId(intAnalysisID, typeDescList)
                                           On a.AcctTypeId Equals ac.AcctTypeId Where a.AnalysisId = ac.AnalysisId
                                           Order By a.SortSequence
                                           Select a.AccountId, a.AcctTypeId, a.AnalysisId, a.AcctDescriptor, a.SortSequence, a.Description, a.Subgrouping, a.TotalType, a.NumberFormat, ac.TypeDesc, ac.ClassDesc
                                         ).ToList()


                    Dim balanceAcc = (From b In utility.BalanceRepository.GetBalanceRecordsByAnalysisId(intAnalysisID)
                                    Join acc In selectedAccounts On b.AccountId Equals acc.AccountId
                                    Where b.AnalysisId = acc.AnalysisId
                                    Select b).ToList()

                    If blnShowBudget = True Then
                        For Each account In selectedAccounts
                            'save amounts total revenue amounts for computing %                      
                            If account.AcctDescriptor = "EndCash" Then
                                Dim accountId As Integer = account.AccountId
                                For Each balance As Balance In (From b In utility.BalanceRepository.GetBalances.Where(Function(a) a.AccountId = accountId And a.AnalysisId = intAnalysisID) Select b)
                                    GetBudgetAmts(balance, decPeriodAmts)
                                    For intColumnCounter = 0 To intPeriods
                                        decAccumTotals(intColumnCounter) = decPeriodAmts(intColumnCounter)
                                        decAccumTotals(intPeriods + 1) += decPeriodAmts(intColumnCounter)
                                    Next
                                    intLastActualCol = -1
                                    GetActualAmts(balance, decPeriodAmts)
                                    For intColumnCounter = intCurrentMonth To 0 Step -1
                                        If intLastActualCol = -1 Then
                                            If decPeriodAmts(intColumnCounter) <> 0 Then
                                                intLastActualCol = intColumnCounter
                                                decAccumTotals(intColumnCounter) += decPeriodAmts(intColumnCounter)
                                            End If
                                        Else
                                            decAccumTotals(intColumnCounter) += decPeriodAmts(intColumnCounter)
                                        End If
                                    Next
                                Next
                                Exit For
                            End If
                        Next
                        If intLastActualCol < intCurrentMonth Then
                            intCurrentMonth = intLastActualCol
                        End If
                    End If

                    For Each account In selectedAccounts
                        CommonProcedures.AddRow(spdAnalytics, intRowCounter, account.Description, account.AcctDescriptor, account.TypeDesc, strBrowser, 4)
                        Select Case account.AcctDescriptor
                            Case "Heading"
                                CommonProcedures.FormatHeaderRow(spdAnalytics, intRowCounter, intSpanRows, blnTotalFlag, 4)
                            Case "Total"
                                CommonProcedures.FormatTotalRow(spdAnalytics, intRowCounter, intSpanRows, blnTotalFlag, intFirstNumberColumn, intNumberColumn, account.ClassDesc, strBrowser, 4)
                                'Case "Detail"
                                '    spdAnalytics.Sheets(0).Cells(intRowCounter, 0).Border.BorderColorBottom = Drawing.Color.White
                            Case "NetIncrease", "EndCash"
                                Array.Clear(intSpanRows, 0, 1)
                                CommonProcedures.FormatGrandTotalRow(spdAnalytics, intRowCounter, blnTotalFlag, intFirstNumberColumn, intNumberColumn, account.ClassDesc, strBrowser, 4)
                        End Select
                        'save accountID 
                        spdAnalytics.Sheets(0).SetValue(intRowCounter, 5, account.AccountId)
                        'save amounts                         
                        If account.AcctDescriptor <> "Heading" Then
                            Array.Clear(decPeriodAmts, 0, 12)
                            Array.Clear(decSaveAmts, 0, 13)
                            Dim accountId As Integer = account.AccountId
                            spdAnalytics.Sheets(0).Cells(intRowCounter, 2).CellType = chkbox
                            For Each balance As Balance In (From b In balanceAcc.Where(Function(a) a.AccountId = accountId And a.AnalysisId = intAnalysisID) Select b)
                                GetActualAmts(balance, decPeriodAmts)
                                If blnShowasPercent = False Then
                                    For intColumnCounter = 0 To intCurrentMonth
                                        spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumberColumn + intColumnCounter, decPeriodAmts(intColumnCounter))
                                        decSaveAmts(intColumnCounter) = decPeriodAmts(intColumnCounter)
                                        'decSaveAmts(intPeriods + 1) += decPeriodAmts(intColumnCounter)
                                    Next
                                Else
                                    For intColumnCounter = 0 To intCurrentMonth
                                        If decAccumTotals(intColumnCounter) <> 0 Then
                                            spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumberColumn + intColumnCounter, decPeriodAmts(intColumnCounter) / decAccumTotals(intColumnCounter))
                                            decSaveAmts(intColumnCounter) = decPeriodAmts(intColumnCounter) / decAccumTotals(intColumnCounter)
                                        Else
                                            spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumberColumn + intColumnCounter, 0)
                                            decSaveAmts(intColumnCounter) = 0
                                        End If
                                        'decSaveAmts(intPeriods + 1) += decPeriodAmts(intColumnCounter)
                                    Next
                                End If
                                If blnShowBudget = True Then 
                                    GetBudgetAmts(balance, decPeriodAmts)
                                    For intColumnCounter = intCurrentMonth + 1 To intPeriods
                                        If blnShowasPercent = False Then
                                            spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumberColumn + intColumnCounter, decPeriodAmts(intColumnCounter))
                                            decSaveAmts(intColumnCounter) = decPeriodAmts(intColumnCounter)
                                            'decSaveAmts(intPeriods + 1) += decPeriodAmts(intColumnCounter)
                                        Else
                                            If decAccumTotals(intColumnCounter) <> 0 Then
                                                spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumberColumn + intColumnCounter, decPeriodAmts(intColumnCounter) / decAccumTotals(intColumnCounter))
                                                decSaveAmts(intColumnCounter) = decPeriodAmts(intColumnCounter) / decAccumTotals(intColumnCounter)
                                            Else
                                                spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumberColumn + intColumnCounter, 0)
                                                decSaveAmts(intColumnCounter) = 0
                                            End If
                                            'decSaveAmts(intPeriods + 1) += decPeriodAmts(intColumnCounter)
                                        End If
                                        spdAnalytics.Sheets(0).Cells(intRowCounter, intFirstNumberColumn + intColumnCounter).ForeColor = Drawing.Color.FromArgb(5, 51, 97)
                                    Next
                                End If
                                'save amounts for total column
                                'Select Case account.AcctDescriptor
                                '    Case "BegCash"
                                '        spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumberColumn + intPeriods + 1, decSaveAmts(0))
                                '    Case "EndCash"
                                '        spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumberColumn + intPeriods + 1, decSaveAmts(intPeriods))
                                '    Case Else
                                '        spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumberColumn + intPeriods + 1, decSaveAmts(intPeriods + 1))
                                'End Select
                                'If intCurrentMonth < intPeriods Then
                                '    spdAnalytics.Sheets(0).Cells(intRowCounter, intFirstNumberColumn + intPeriods + 1).ForeColor = Drawing.Color.FromArgb(5, 51, 97)
                                'End If
                            Next
                        End If
                    Next
                End Using
                Logger.Log.Info(String.Format("REController:LoadSpread5to6 method execution Ends"))
            Catch ex As Exception
                Logger.Log.Error(String.Format("REController:LoadSpread5to6 method execution ends with Error message: {0}, StackTrace: {1}", ex.Message, ex.StackTrace))
            End Try


        End Sub

        <CustomActionFilter()>
        Public Sub LoadSpread6(ByVal spdAnalytics As FpSpread, ByVal intFormat As Integer, ByVal intPeriod As Integer, ByVal intNumberColumn As Integer, ByVal intFirstNumberColumn As Integer, ByRef blnShowasPercent As Boolean, ByVal intColumnCount As Integer)
            Try
                Logger.Log.Info(String.Format("REController:LoadSpread5to6 method execution Starts"))

                Using utility As New Utility()
                    Dim intRowCounter As Integer = -1
                    Dim intColumnCounter As Integer
                    Dim strYearType(5) As String
                    Dim strPeriods(2) As String
                    Dim decAccumTotals(12) As Decimal
                    Dim decPeriodAmts(11) As Decimal
                    Dim decSaveAmts(12) As Decimal
                    Dim intSpanRows(1) As Integer
                    Dim blnTotalFlag As Boolean
                    Dim decIncAccumTotals(intNumberColumn) As Decimal
                    Dim chkbox As New FarPoint.Web.Spread.CheckBoxCellType
                    Dim intCurrentMonth As Integer = Val(DateTime.Today.ToString("MM")) - 2
                    Dim strBrowser As String = Request.Browser.Browser
                    If (isViewer) Then
                        chkbox.OnClientClick = "return false"
                    Else
                        chkbox.OnClientClick = CommonProcedures.CheckboxClicked(1)
                    End If

                    Dim selectedAccounts = (From a In utility.AccountRepository.GetAccountRecordsByAnalysisId(intAnalysisID)
                                           Join ac In utility.AccountTypeRepository.GetAccountTypeRecordsByAnalysisId(intAnalysisID, typeDescList)
                                           On a.AcctTypeId Equals ac.AcctTypeId Where a.AnalysisId = ac.AnalysisId
                                           Order By a.SortSequence
                                           Select a.AccountId, a.AcctTypeId, a.AnalysisId, a.AcctDescriptor, a.SortSequence, a.Description, a.Subgrouping, a.TotalType, a.NumberFormat, ac.TypeDesc, ac.ClassDesc
                                         ).ToList()

                    Dim balanceAcc = (From b In utility.BalanceRepository.GetBalanceRecordsByAnalysisId(intAnalysisID)
                                   Join acc In selectedAccounts On b.AccountId Equals acc.AccountId
                                   Where b.AnalysisId = acc.AnalysisId
                                   Select b).ToList()

                    For Each account In selectedAccounts
                        CommonProcedures.AddRow(spdAnalytics, intRowCounter, account.Description, account.AcctDescriptor, account.TypeDesc, strBrowser, 4)
                        Select Case account.AcctDescriptor
                            Case "Heading"
                                CommonProcedures.FormatHeaderRow(spdAnalytics, intRowCounter, intSpanRows, blnTotalFlag, 4)
                            Case "Total"
                                CommonProcedures.FormatTotalRow(spdAnalytics, intRowCounter, intSpanRows, blnTotalFlag, intFirstNumberColumn, intNumberColumn, account.ClassDesc, strBrowser, 4)
                                'Case "Detail"
                                '    spdAnalytics.Sheets(0).Cells(intRowCounter, 0).Border.BorderColorBottom = Drawing.Color.White
                            Case "NetIncrease", "EndCash"
                                Array.Clear(intSpanRows, 0, 1)
                                CommonProcedures.FormatGrandTotalRow(spdAnalytics, intRowCounter, blnTotalFlag, intFirstNumberColumn, intNumberColumn, account.ClassDesc, strBrowser, 4)
                        End Select
                        'save accountID 
                        spdAnalytics.Sheets(0).SetValue(intRowCounter, 5, account.AccountId)
                        'save amounts
                        If account.AcctDescriptor <> "Heading" Then
                            Array.Clear(decPeriodAmts, 0, 12)
                            Array.Clear(decSaveAmts, 0, 13)
                            Dim accountId As Integer = account.AccountId
                            spdAnalytics.Sheets(0).Cells(intRowCounter, 2).CellType = chkbox
                            For Each balance As Balance In (From b In balanceAcc.Where(Function(a) a.AccountId = accountId And a.AnalysisId = intAnalysisID) Select b)
                                If intPeriod < 11 Then
                                    GetH5Amts(balance, decPeriodAmts)
                                    For intColumnCounter = intPeriod + 1 To 11
                                        If blnShowasPercent = False Then
                                            spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumberColumn + intColumnCounter - (intPeriod + 1), decPeriodAmts(intColumnCounter))
                                            decSaveAmts(intColumnCounter) = decPeriodAmts(intColumnCounter)
                                            decSaveAmts(12) += decPeriodAmts(intColumnCounter)
                                        Else
                                            If decAccumTotals(intColumnCounter) <> 0 Then
                                                spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumberColumn + intColumnCounter - (intPeriod + 1), decPeriodAmts(intColumnCounter) / decAccumTotals(intColumnCounter))
                                                decSaveAmts(intColumnCounter) = decPeriodAmts(intColumnCounter) / decAccumTotals(intColumnCounter)
                                            Else
                                                spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumberColumn + intColumnCounter - (intPeriod + 1), 0)
                                                decSaveAmts(intColumnCounter) = 0
                                            End If
                                            decSaveAmts(12) += decPeriodAmts(intColumnCounter)
                                        End If
                                    Next
                                End If
                                GetActualAmts(balance, decPeriodAmts)
                                For intColumnCounter = 0 To intPeriod
                                    If blnShowasPercent = False Then
                                        spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumberColumn + intColumnCounter + (11 - intPeriod), decPeriodAmts(intColumnCounter))
                                        decSaveAmts(intColumnCounter) = decPeriodAmts(intColumnCounter)
                                        decSaveAmts(12) += decPeriodAmts(intColumnCounter)
                                    Else
                                        If decAccumTotals(intColumnCounter) <> 0 Then
                                            spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumberColumn + intColumnCounter + (11 - intPeriod), decPeriodAmts(intColumnCounter) / decAccumTotals(intColumnCounter))
                                            decSaveAmts(intColumnCounter) = decPeriodAmts(intColumnCounter) / decAccumTotals(intColumnCounter)
                                        Else
                                            spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumberColumn + intColumnCounter + (11 - intPeriod), 0)
                                            decSaveAmts(intColumnCounter) = 0
                                        End If
                                        decSaveAmts(12) += decPeriodAmts(intColumnCounter)
                                    End If
                                Next
                            Next
                            Select Case account.AcctDescriptor
                                Case "BegCash"
                                    spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumberColumn + 12, decSaveAmts(0))
                                Case "EndCash"
                                    spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumberColumn + 12, decSaveAmts(11))
                                Case Else
                                    spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumberColumn + 12, decSaveAmts(12))
                            End Select
                        End If
                    Next
                End Using
                Logger.Log.Info(String.Format("REController:LoadSpread5to6 method execution Ends"))
            Catch ex As Exception
                Logger.Log.Error(String.Format("REController:LoadSpread5to6 method execution ends with Error message: {0}, StackTrace: {1}", ex.Message, ex.StackTrace))
            End Try


        End Sub
        Public Sub LoadSpread9(ByVal spdAnalytics As FpSpread, ByVal intFormat As Integer, ByVal intPeriod As Integer, ByVal intNumberColumn As Integer, ByVal intFirstNumberColumn As Integer, ByRef blnShowasPercent As Boolean, ByVal intColumnCount As Integer)
            Try
                Logger.Log.Info(String.Format("REController:LoadSpread5to6 method execution Starts"))

                Using utility As New Utility()
                    Dim intRowCounter As Integer = -1
                    Dim intColumnCounter As Integer
                    Dim strYearType(5) As String
                    Dim strPeriods(2) As String
                    Dim decAccumTotals(12) As Decimal
                    Dim decPeriodAmts(11) As Decimal
                    Dim decSaveAmts(12) As Decimal
                    Dim intSpanRows(1) As Integer
                    Dim blnTotalFlag As Boolean
                    Dim decIncAccumTotals(intNumberColumn) As Decimal
                    Dim chkbox As New FarPoint.Web.Spread.CheckBoxCellType
                    Dim intCurrentMonth As Integer = Val(DateTime.Today.ToString("MM")) - 2
                    Dim strBrowser As String = Request.Browser.Browser
                    If (isViewer) Then
                        chkbox.OnClientClick = "return false"
                    Else
                        chkbox.OnClientClick = CommonProcedures.CheckboxClicked(1)
                    End If

                    Dim selectedAccounts = (From a In utility.AccountRepository.GetAccountRecordsByAnalysisId(intAnalysisID)
                                           Join ac In utility.AccountTypeRepository.GetAccountTypeRecordsByAnalysisId(intAnalysisID, typeDescList)
                                           On a.AcctTypeId Equals ac.AcctTypeId Where a.AnalysisId = ac.AnalysisId
                                           Order By a.SortSequence
                                           Select a.AccountId, a.AcctTypeId, a.AnalysisId, a.AcctDescriptor, a.SortSequence, a.Description, a.Subgrouping, a.TotalType, a.NumberFormat, ac.TypeDesc, ac.ClassDesc
                                         ).ToList()

                    Dim balanceAcc = (From b In utility.BalanceRepository.GetBalanceRecordsByAnalysisId(intAnalysisID)
                                   Join acc In selectedAccounts On b.AccountId Equals acc.AccountId
                                   Where b.AnalysisId = acc.AnalysisId
                                   Select b).ToList()

                    For Each account In selectedAccounts
                        CommonProcedures.AddRow(spdAnalytics, intRowCounter, account.Description, account.AcctDescriptor, account.TypeDesc, strBrowser, 4)
                        Select Case account.AcctDescriptor
                            Case "Heading"
                                CommonProcedures.FormatHeaderRow(spdAnalytics, intRowCounter, intSpanRows, blnTotalFlag, 4)
                            Case "Total"
                                CommonProcedures.FormatTotalRow(spdAnalytics, intRowCounter, intSpanRows, blnTotalFlag, intFirstNumberColumn, intNumberColumn, account.ClassDesc, strBrowser, 4)
                                'Case "Detail"
                                '    spdAnalytics.Sheets(0).Cells(intRowCounter, 0).Border.BorderColorBottom = Drawing.Color.White
                            Case "NetIncrease", "EndCash"
                                Array.Clear(intSpanRows, 0, 1)
                                CommonProcedures.FormatGrandTotalRow(spdAnalytics, intRowCounter, blnTotalFlag, intFirstNumberColumn, intNumberColumn, account.ClassDesc, strBrowser, 4)
                        End Select
                        'save accountID 
                        spdAnalytics.Sheets(0).SetValue(intRowCounter, 5, account.AccountId)
                        'save amounts
                        If account.AcctDescriptor <> "Heading" Then
                            Array.Clear(decPeriodAmts, 0, 12)
                            Array.Clear(decSaveAmts, 0, 13)
                            Dim accountId As Integer = account.AccountId
                            spdAnalytics.Sheets(0).Cells(intRowCounter, 2).CellType = chkbox
                            For Each balance As Balance In (From b In balanceAcc.Where(Function(a) a.AccountId = accountId And a.AnalysisId = intAnalysisID) Select b)
                                If intPeriod < 11 Then 
                                    GetBudgetAmts(balance, decPeriodAmts)
                                    For intColumnCounter = intPeriod + 1 To 11
                                        If blnShowasPercent = False Then
                                            spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumberColumn + intColumnCounter - (intPeriod + 1), decPeriodAmts(intColumnCounter))
                                            decSaveAmts(intColumnCounter) = decPeriodAmts(intColumnCounter)
                                            decSaveAmts(12) += decPeriodAmts(intColumnCounter)
                                        Else
                                            If decAccumTotals(intColumnCounter) <> 0 Then
                                                spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumberColumn + intColumnCounter - (intPeriod + 1), decPeriodAmts(intColumnCounter) / decAccumTotals(intColumnCounter))
                                                decSaveAmts(intColumnCounter) = decPeriodAmts(intColumnCounter) / decAccumTotals(intColumnCounter)
                                            Else
                                                spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumberColumn + intColumnCounter - (intPeriod + 1), 0)
                                                decSaveAmts(intColumnCounter) = 0
                                            End If
                                            decSaveAmts(12) += decPeriodAmts(intColumnCounter)
                                        End If
                                    Next
                                End If
                                GetBudget2Amts(balance, decPeriodAmts)
                                For intColumnCounter = 0 To intPeriod
                                    If blnShowasPercent = False Then
                                        spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumberColumn + intColumnCounter + (11 - intPeriod), decPeriodAmts(intColumnCounter))
                                        decSaveAmts(intColumnCounter) = decPeriodAmts(intColumnCounter)
                                        decSaveAmts(12) += decPeriodAmts(intColumnCounter)
                                    Else
                                        If decAccumTotals(intColumnCounter) <> 0 Then
                                            spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumberColumn + intColumnCounter + (11 - intPeriod), decPeriodAmts(intColumnCounter) / decAccumTotals(intColumnCounter))
                                            decSaveAmts(intColumnCounter) = decPeriodAmts(intColumnCounter) / decAccumTotals(intColumnCounter)
                                        Else
                                            spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumberColumn + intColumnCounter + (11 - intPeriod), 0)
                                            decSaveAmts(intColumnCounter) = 0
                                        End If
                                        decSaveAmts(12) += decPeriodAmts(intColumnCounter)
                                    End If
                                Next
                            Next
                            Select Case account.AcctDescriptor
                                Case "BegCash"
                                    spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumberColumn + 12, decSaveAmts(0))
                                Case "EndCash"
                                    spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumberColumn + 12, decSaveAmts(11))
                                Case Else
                                    spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumberColumn + 12, decSaveAmts(12))
                            End Select
                        End If
                    Next
                End Using
                Logger.Log.Info(String.Format("REController:LoadSpread5to6 method execution Ends"))
            Catch ex As Exception
                Logger.Log.Error(String.Format("REController:LoadSpread5to6 method execution ends with Error message: {0}, StackTrace: {1}", ex.Message, ex.StackTrace))
            End Try


        End Sub

        <CustomActionFilter()>
        Public Sub LoadSpread7to8(ByVal spdAnalytics As FpSpread, ByVal intFormat As Integer, ByVal intPeriod As Integer, ByVal intNumberColumn As Integer, ByVal intFirstNumberColumn As Integer, ByRef blnShowasPercent As Boolean, ByVal intColumnCount As Integer)
            Try
                Logger.Log.Info(String.Format("REController:LoadSpread7to8 method execution Starts"))
                Using utility As New Utility

                    Dim intRowCounter As Integer = -1
                    Dim intColumnCounter As Integer
                    Dim intTypeCol As Integer
                    Dim strYearType(5) As String
                    Dim strPeriods(2) As String
                    Dim decPeriodAmts(11) As Decimal
                    Dim decAccumTotals(5) As Decimal
                    Dim decSaveAmts(12) As Decimal
                    Dim intSpanRows(1) As Integer
                    Dim blnTotalFlag As Boolean
                    Dim decIncAccumTotals(intNumberColumn) As Decimal
                    Dim chkbox As New FarPoint.Web.Spread.CheckBoxCellType
                    Dim strBrowser As String = Request.Browser.Browser
                    If (isViewer) Then
                        chkbox.OnClientClick = "return false"
                    Else
                        chkbox.OnClientClick = CommonProcedures.CheckboxClicked(1)
                    End If

                    Dim selectedAccounts = (From a In utility.AccountRepository.GetAccountRecordsByAnalysisId(intAnalysisID)
                                           Join ac In utility.AccountTypeRepository.GetAccountTypeRecordsByAnalysisId(intAnalysisID, typeDescList)
                                           On a.AcctTypeId Equals ac.AcctTypeId Where a.AnalysisId = ac.AnalysisId
                                           Order By a.SortSequence
                                           Select a.AccountId, a.AcctTypeId, a.AnalysisId, a.AcctDescriptor, a.SortSequence, a.Description, a.Subgrouping, a.TotalType, a.NumberFormat, ac.TypeDesc, ac.ClassDesc
                                         ).ToList()

                    Dim balanceAcc = (From b In utility.BalanceRepository.GetBalanceRecordsByAnalysisId(intAnalysisID)
                                   Join acc In selectedAccounts On b.AccountId Equals acc.AccountId
                                   Where b.AnalysisId = acc.AnalysisId
                                   Select b).ToList()


                    For Each account In selectedAccounts

                        'Dim accountTypeId As Integer = account.AcctTypeId
                        'Dim acctType = (From a In utility.AccountTypeRepository.GetAccountTypes.Where(Function(at) at.AcctTypeId = accountTypeId) Select a).FirstOrDefault()

                        CommonProcedures.AddRow(spdAnalytics, intRowCounter, account.Description, account.AcctDescriptor, account.TypeDesc, strBrowser, 4)
                        Select Case account.AcctDescriptor
                            Case "Heading"
                                CommonProcedures.FormatHeaderRow(spdAnalytics, intRowCounter, intSpanRows, blnTotalFlag, 4)
                            Case "Total"
                                CommonProcedures.FormatTotalRow(spdAnalytics, intRowCounter, intSpanRows, blnTotalFlag, intFirstNumberColumn, intNumberColumn, account.ClassDesc, strBrowser, 4)
                                'Case "Detail"
                                '    spdAnalytics.Sheets(0).Cells(intRowCounter, 0).Border.BorderColorBottom = Drawing.Color.White
                            Case "NetIncrease", "EndCash"
                                Array.Clear(intSpanRows, 0, 1)
                                CommonProcedures.FormatGrandTotalRow(spdAnalytics, intRowCounter, blnTotalFlag, intFirstNumberColumn, intNumberColumn, account.ClassDesc, strBrowser, 4)
                        End Select
                        'save accountID 
                        spdAnalytics.Sheets(0).SetValue(intRowCounter, 5, account.AccountId)
                        'save amounts 
                        If account.AcctDescriptor <> "Heading" Then
                            Array.Clear(decPeriodAmts, 0, 12)
                            Array.Clear(decSaveAmts, 0, 5)
                            Dim accountId As Integer = account.AccountId
                            spdAnalytics.Sheets(0).Cells(intRowCounter, 2).CellType = chkbox
                            For Each balance As Balance In (From b In balanceAcc.Where(Function(a) a.AccountId = accountId And a.AnalysisId = intAnalysisID) Select b)
                                For intTypeCol = 0 To 5
                                    Select Case intTypeCol
                                        Case 0
                                            GetH1Amts(balance, decPeriodAmts)
                                        Case 1
                                            GetH2Amts(balance, decPeriodAmts)
                                        Case 2
                                            GetH3Amts(balance, decPeriodAmts)
                                        Case 3
                                            GetH4Amts(balance, decPeriodAmts)
                                        Case 4
                                            GetH5Amts(balance, decPeriodAmts)
                                        Case 5
                                            GetActualAmts(balance, decPeriodAmts)
                                    End Select
                                    Select Case intFormat
                                        Case 7
                                            spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumberColumn + intTypeCol, decPeriodAmts(intPeriod))
                                            decSaveAmts(intTypeCol) = decPeriodAmts(intPeriod)
                                        Case 8
                                            decSaveAmts(intTypeCol) = 0
                                            Select Case account.AcctDescriptor
                                                Case "BegCash"
                                                    decSaveAmts(intTypeCol) += decPeriodAmts(0)
                                                Case "EndCash"
                                                    decSaveAmts(intTypeCol) += decPeriodAmts(intPeriod)
                                                Case Else
                                                    For intColumnCounter = 0 To intPeriod
                                                        decSaveAmts(intTypeCol) += decPeriodAmts(intColumnCounter)
                                                    Next
                                            End Select
                                            spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumberColumn + intTypeCol, decSaveAmts(intTypeCol))
                                    End Select
                                Next
                            Next
                        End If
                    Next
                End Using
                Logger.Log.Info(String.Format("REController:LoadSpread7to8 method execution Ends"))
            Catch ex As Exception
                Logger.Log.Error(String.Format("REController:LoadSpread7to8 method execution ends with Error message: {0}, StackTrace: {1}", ex.Message, ex.StackTrace))
            End Try


        End Sub

        <HttpPost()>
        <CustomActionFilter()>
        Function AddViews(viewname As String, accountid As String, ByVal formValues As FormValues) As ActionResult
            Try

                Dim selectedAnalysis As Integer
                Logger.Log.Info(String.Format("CFController:AddViews method execution Starts"))
                If (Not Session("UserType") Is Nothing) And (Not Session("UserInfo") Is Nothing) Then
                    UserInfo = DirectCast(Session("UserInfo"), User)
                Else
                    Return RedirectToAction("Index", "Home")
                End If

                If (UserInfo.AnalysisId > 0) Then
                    selectedAnalysis = UserInfo.AnalysisId
                Else
                    If (Not Session("SelectedAnalysisFromDropdown") Is Nothing) Then
                        selectedAnalysis = Session("SelectedAnalysisFromDropdown")
                    Else
                        selectedAnalysis = -1
                    End If
                End If
                Dim controller As String = "CF"
                Dim result As Integer = Utility.SaveViews(viewname, controller, accountid, selectedAnalysis, formValues)
                Logger.Log.Info(String.Format("CFController:AddViews method execution Ends"))
                Return RedirectToAction("Index")
            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Cash Flow : Error occured while Add View - ", ex.Message)
                Logger.Log.Error(String.Format("CFController : AddViews method execution ends with Error message: {0}, StackTrace: {1}", ex.Message, ex.StackTrace))
                Return RedirectToAction("Index")
            End Try

        End Function

        <HttpGet()>
        <CustomActionFilter()>
        Function Viewer(Id As Integer, <MvcSpread("spdAnalytics")> ByVal spdAnalytics As FpSpread, ByVal formValues As FormValues) As ActionResult
            isViewer = True
            Dim savedView As SaveView = saveViewRepository.GetSavedViewDetail(Id)
            Dim setupCountObj As SetupCount = Nothing
            Dim selectedAnalysis As Integer = savedView.analysisID
            Try
                Logger.Log.Info(String.Format("CFController Viewer (HttpGet) method execution starts"))
                If (Not Session("UserType") Is Nothing) And (Not Session("UserInfo") Is Nothing) Then
                    UserInfo = DirectCast(Session("UserInfo"), User)
                    sPeriod = Session("sPeriod")
                    sFormat = Session("sFormat")

                    If (UserInfo.AnalysisId = 0) Then
                        Return RedirectToAction("Index", "Dashboard")
                    Else
                        intAnalysisID = savedView.analysisID 'UserInfo.AnalysisId
                        intFirstFiscal = UserInfo.fiscalMonthOfSelectedCompany
                    End If
                End If

                setupCountObj = Utility.CheckRequiredSetupCount(UserInfo, selectedAnalysis)
                ViewBag.Setup = setupCountObj

                Dim intCtr As Integer
                Dim periods As List(Of SelectListItem) = New List(Of SelectListItem)
                intNumberofPeriods = NumberofPeriods(sPeriod, intFirstFiscal)
                For intCtr = 0 To 11
                    periods.Add(New SelectListItem With {.Text = CommonProcedures.GetMonth(intCtr, intFirstFiscal), .Value = intCtr + 1, .Selected = False})
                Next
                ViewData("Periods") = New SelectList(periods, "value", "text", "May")

                ViewData("blnHighVar") = savedView.highlightva
                ViewData("blnShowPercent") = savedView.showaspercent
                ViewData("intPeriod") = intNumberofPeriods + 1
                ViewData("intFormat") = savedView.format + 1
                ViewData("intChartType") = savedView.charttype
                ViewData("blnShowTrend") = savedView.trend
                ViewData("blnShowBudget") = savedView.showbudget
                ViewData("blnUseFilter") = savedView.filter
                ViewData("intDept") = 1
                ViewData("Postback") = False
                Session("intPeriod") = sPeriod + 1
                Dim accID As String = String.Empty
                Dim array() As Nullable(Of Integer) = {savedView.account1, savedView.account2, savedView.account3, savedView.account4, savedView.account5, savedView.account6, savedView.account7, savedView.account8, savedView.account9, savedView.account10}
                For AcId As Nullable(Of Integer) = 0 To array.Length - 1
                    If (Not IsNothing(array(AcId))) Then
                        accID = accID & array(AcId).ToString() & " "
                    End If
                Next
                accID = accID.TrimEnd(" ")
                ViewData("AccountID") = accID
                Logger.Log.Info(String.Format("CFController Viewer (HttpGet) method execution Ends"))
            Catch ex As Exception                
                TempData("ErrorMessage") = String.Concat("Cash Flow Viewer : Error occured while loading Chart - ", ex.Message)
                Logger.Log.Error(String.Format("CFController Viewer (HttpGet) method execution ends with Error message: {0}, StackTrace: {1}", ex.Message, ex.StackTrace))
            End Try
            Return View("Viewer")
        End Function

        <HttpPost()> _
        <CustomActionFilter()>
        Function Viewer(Id As Integer, <MvcSpread("spdAnalytics")> ByVal spdAnalytics As FpSpread, <MvcSpread("spdChart")> ByVal spdChart As FpSpread, ByVal formValues As FormValues) As ActionResult
            Dim setupCountObj As SetupCount
            isViewer = True
            Dim savedView As SaveView = saveViewRepository.GetSavedViewDetail(Id)
            Dim selectedAnalysis As Integer = savedView.analysisID
            Dim strFilteron As String = savedView.filteron

            Logger.Log.Info(String.Format("CFController Viewer (HttpPost) method execution Starts"))
            Try
                If (Not Session("UserType") Is Nothing) And (Not Session("UserInfo") Is Nothing) Then
                    UserInfo = DirectCast(Session("UserInfo"), User)
                    If (UserInfo.AnalysisId = 0) Then
                        Return RedirectToAction("Index", "Dashboard")
                    Else
                        intAnalysisID = savedView.analysisID 'UserInfo.AnalysisId
                        intFirstFiscal = UserInfo.fiscalMonthOfSelectedCompany
                    End If
                End If

                setupCountObj = Utility.CheckRequiredSetupCount(UserInfo, selectedAnalysis)
                ViewBag.Setup = setupCountObj

                If (spdChart.Sheets(0).Charts.Count > 0) Then
                    spdChart.Sheets(0).Charts.Remove(spdChart.Sheets(0).Charts(0))
                End If

            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Cash Flow Viewer : Error occured while loading Chart - ", ex.Message)
                Logger.Log.Error(String.Format("CFController Viewer (HttpPost) method execution ends with Error message: {0}, StackTrace: {1}", ex.Message, ex.StackTrace))
            End Try
            Try
                spdAnalytics.Sheets(0).RowCount = 0
                SetSpreadProperties(spdAnalytics, savedView.format - 1, formValues.intPeriod - 1, savedView.showaspercent, savedView.showbudget)
                If (savedView.charttype <> 1 And Not (savedView.account1 Is Nothing)) Then
                    Charts.BuildChart(spdChart, spdAnalytics, savedView.charttype, savedView.format - 1, intFirstNumberColumn, savedView.showaspercent, savedView.trend, True)
                    ViewData("blnUpdateChart") = True
                End If

                Dim periods As List(Of SelectListItem) = New List(Of SelectListItem)
                intNumberofPeriods = NumberofPeriods(sPeriod, intFirstFiscal)
                'For intCtr = 0 To intNumberofPeriods
                For intCtr = 0 To 11
                    periods.Add(New SelectListItem With {.Text = CommonProcedures.GetMonth(intCtr, intFirstFiscal), .Value = intCtr + 1, .Selected = False})
                Next
                ViewData("Periods") = New SelectList(periods, "value", "text", "May")

                ViewData("FilterOn") = savedView.filteron
                ViewData("ChartType") = savedView.charttype
                ViewData("blnHighVar") = savedView.highlightva
                ViewData("blnShowPercent") = savedView.showaspercent
                ViewData("intPeriod") = intNumberofPeriods - 1
                ViewData("Period") = intNumberofPeriods - 1
                ViewData("intFormat") = savedView.format
                ViewData("intChartType") = savedView.charttype
                ViewData("blnShowTrend") = savedView.trend
                ViewData("blnShowBudget") = savedView.showbudget
                ViewData("blnUseFilter") = savedView.filter
                ViewData("intDept") = 1
                ViewData("postback") = True
                Dim accID As String = String.Empty
                Dim array() As Nullable(Of Integer) = {savedView.account1, savedView.account2, savedView.account3, savedView.account4, savedView.account5, savedView.account6, savedView.account7, savedView.account8, savedView.account9, savedView.account10}
                For AcId As Nullable(Of Integer) = 0 To array.Length - 1
                    If (Not IsNothing(array(AcId))) Then
                        accID = accID & array(AcId).ToString() & " "
                    End If
                Next
                accID = accID.TrimEnd(" ")
                ViewData("AccountID") = accID

                Session("sPeriod") = formValues.intPeriod - 1
                Session("sFormat") = savedView.format - 1
                Logger.Log.Info(String.Format("CFController Viewer (HttpPost) method execution Ends"))
            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Cash Flow Viewer : Error occured while loading Chart - ", ex.Message)
                Logger.Log.Error(String.Format("CFController Viewer (HttpPost) method execution ends with Error message: {0}, StackTrace: {1}", ex.Message, ex.StackTrace))
            End Try
            Return View("Viewer")
        End Function

        Protected Overrides Sub Dispose(disposing As Boolean)
            accountRepository.Dispose()
            accountTypeRepository.Dispose()
            balanceRepository.Dispose()
            chartFormatRepository.Dispose()
            chartTypeRepository.Dispose()

            MyBase.Dispose(disposing)
        End Sub
    End Class
End Namespace
