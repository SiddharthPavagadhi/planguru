﻿Imports System.Data.SqlClient
Imports System.ComponentModel
Imports System.Configuration
Imports System.IO
Imports System.String
Imports System.Text


Public Class Customer

    Dim _connectionString As SqlConnection
    Dim _sda As SqlDataAdapter
    Dim _filePath As String
    Dim userDataSet As DataSet
    Dim insertScript As New StringBuilder()

    Private ReadOnly Property ExportScriptFilePath() As String
        Get
            If (Me._filePath Is Nothing) Then
                Me._filePath = ConfigurationManager.AppSettings("ExportScriptFilePath").ToString()
            End If
            Return Me._filePath
        End Get        
    End Property


    Private ReadOnly Property connection() As String
        Get
            If (Me._connectionString Is Nothing) Then
                Me._connectionString = New SqlConnection(ConfigurationManager.ConnectionStrings("DataAccess").ConnectionString.ToString())
            End If

            Return Me._connectionString.ConnectionString()
        End Get       
    End Property

    Private Function ValidateControl(ByVal ParamArray ctl() As Object) As Boolean
        For i As Integer = 0 To UBound(ctl)
            If ctl(i).text = "" Then
                CustErrorProvider.SetError(ctl(i), ctl(i).tag)
                Return False
                Exit Function
            End If
            If Not IsNumeric(ctl(i).text) Then
                CustErrorProvider.SetError(ctl(i), "Please provide only numeric value.")
                Return False
                Exit Function
            End If
            If (Me.ExportScriptFilePath() = "") Then
                CustErrorProvider.SetError(ctl(i), "Please provide filepath in App.Config.")
                lblError.Text = "Please provide filepath in App.Config."
                Return False
                Exit Function
            End If
        Next
        Return True
    End Function

    Private Sub btnExportData_Click(sender As System.Object, e As System.EventArgs) Handles btnExportData.Click

            Dim customerquery As String = "Select count(CustomerId) from [user] where customerid = @customerId"

            Dim response As Integer = -1
            lblError.Text = ""
            CustErrorProvider.Clear()
            Try
                If ValidateControl(txtCustomerId) = True Then
                    Dim customerId As Int32 = txtCustomerId.Text
                    Using connection As New SqlConnection(Me.connection())
                        Dim cmd As New SqlCommand(customerquery, connection)
                        cmd.CommandType = CommandType.Text
                        cmd.Connection = connection
                        cmd.Parameters.Add("@customerId", SqlDbType.Int).Value = customerId
                        Try
                            connection.Open()
                            response = Convert.ToInt16(cmd.ExecuteScalar())
                            lblError.Visible = True
                            If (response = 0) Then
                                lblError.Text = "Invalid Customer Id."
                            Else
                                insertScript.Clear()


                                If (Me.ExportScriptFilePath() Is Nothing) Then
                                    lblError.Text = "Please provide the filepath to export the data."
                                Else
                                    Me.GetUserInfo(customerId)

                                    If Not Directory.Exists(Me._filePath) Then
                                        Directory.CreateDirectory(Me._filePath)
                                    End If
                                    Dim filname As String = String.Concat("CustomerInfo_", DateTime.Now.ToString("dd-MM-yyyy_HHmmss"), ".sql")
                                    Dim customerInfoScript As String = String.Concat(Me._filePath, "\", filname)
                                    If (insertScript.Length > 0) Then

                                        Dim sw As StreamWriter
                                        sw = File.CreateText(customerInfoScript)
                                        sw.WriteLine(insertScript)
                                        sw.Flush()
                                        sw.Close()
                                        MsgBox("Succesfully export customer information in " + filname.ToString(), MsgBoxStyle.Information, "Customer Info")
                                    End If
                                End If

                            End If
                        Catch ex As Exception
                            lblError.Text = String.Concat(ex.Message.ToString())
                        Finally
                            connection.Close()
                            connection.Dispose()
                        End Try
                    End Using
                Else
                    Exit Sub
                End If
            Catch ex As Exception
                lblError.Text = String.Concat(ex.Message.ToString())
            End Try
    End Sub

    Private Sub GetUserInfo(customerid As Integer)

        Dim userquery As String = String.Concat("Select * from [user] where customerid =", customerid, " Order By UserId")
        Dim custquery As String = String.Concat("Select * from [customer] where customerid =", customerid, "Order By CustomerId")
        Dim companyquery As String = String.Concat("Select * from [Company] where customerid =", customerid, " Order By CompanyId")
        Dim common_analysisquery As String = "Select * from [Analysis] where companyid in (#companyIds#) Order By AnalysisId"

        Dim common_usercompanymapping As String = "Select * from [UserCompanyMapping] where CompanyId in (#companyIds#) and UserId in (#UserIds#)"
        Dim common_useranalysismapping As String = "Select * from [UserAnalysisMapping] where AnalysisId in (#AnalysisIds#) and UserId in (#UserIds#)"

        Dim common_acctTypequery As String = "Select * from [AcctType] where AnalysisId in (#AnalysisIds#) Order By AcctTypeId"
        Dim common_acctquery As String = "Select * from [Account] where AnalysisId in (#AnalysisIds#) Order By AccountId"
        Dim common_balancequery As String = "Select * from [Balance] where AnalysisId in (#AnalysisIds#) Order By BalanceId"
        Dim common_dashboardquery As String = "Select * from [Dashboard] where AnalysisId in (#AnalysisIds#) Order By DashboardId"
        Dim common_saveviewquery As String = "Select * from [SaveView] where AnalysisId in (#AnalysisIds#) Order By Id"
        Dim common_scorecardquery As String = "Select * from [Scorecard] where AnalysisId in (#AnalysisIds#) Order By ScorecardId"


        Dim response As Integer = -1
        Using connection As New SqlConnection(_connectionString.ConnectionString.ToString())

            Dim custAdapter As SqlDataAdapter = New SqlDataAdapter(custquery, connection)
            Dim userAdapter As SqlDataAdapter = New SqlDataAdapter(userquery, connection)
            Dim companyAdapter As SqlDataAdapter = New SqlDataAdapter(companyquery, connection)

            Dim commonAdapter As SqlDataAdapter
            Try
                userDataSet = New DataSet()

                custAdapter.Fill(userDataSet, "Customer")
                userAdapter.Fill(userDataSet, "User")
                companyAdapter.Fill(userDataSet, "Company")


                '------------------------User Ids----------------------------------------------------
                Dim UserIds As String = String.Empty
                For Each row As DataRow In userDataSet.Tables("User").Rows
                    If (UserIds = String.Empty) Then
                        UserIds = row.Item("UserId").ToString()
                    Else
                        UserIds = String.Concat(UserIds, ",", row.Item("UserId").ToString())
                    End If
                Next
                '-------------------------------------------------------------------------------------

                '------------------------Company Ids----------------------------------------------------
                Dim companyIds As String = String.Empty
                For Each row As DataRow In userDataSet.Tables("Company").Rows
                    If (companyIds = String.Empty) Then
                        companyIds = row.Item("CompanyId").ToString()
                    Else
                        companyIds = String.Concat(companyIds, ",", row.Item("CompanyId").ToString())
                    End If
                Next

                If (companyIds.Length > 0) Then
                    common_analysisquery = common_analysisquery.Replace("#companyIds#", companyIds)
                    commonAdapter = New SqlDataAdapter(common_analysisquery, connection)
                    commonAdapter.Fill(userDataSet, "Analysis")
                End If

                '-------------------------------------------------------------------------------------

                '------------------------Get Analysis Ids---------------------------------------------  
                Dim analysis As String = String.Empty
                If (userDataSet.Tables.Contains("Analysis")) Then
                    For Each row As DataRow In userDataSet.Tables("Analysis").Rows
                        If (analysis = String.Empty) Then
                            analysis = Convert.ToInt64(row.Item("AnalysisId").ToString())
                        Else
                            analysis = String.Concat(analysis, ",", Convert.ToInt16(row.Item("AnalysisId").ToString()))
                        End If
                    Next
                End If


                '--------------------------------------------------------------------------------------

                If (analysis.Length > 0) Then

                    '-------------------------UserCompanyMapping-------------------------------------------------
                    If (UserIds.Length > 0) Then
                        common_usercompanymapping = common_usercompanymapping.Replace("#companyIds#", companyIds).Replace("#UserIds#", UserIds)
                        commonAdapter = New SqlDataAdapter(common_usercompanymapping, connection)
                        commonAdapter.Fill(userDataSet, "UserCompanyMapping")
                    End If

                    '-------------------------UserAnalysisMapping-------------------------------------------------
                    If (UserIds.Length > 0) Then
                        common_useranalysismapping = common_useranalysismapping.Replace("#AnalysisIds#", analysis).Replace("#UserIds#", UserIds)
                        commonAdapter = New SqlDataAdapter(common_useranalysismapping, connection)
                        commonAdapter.Fill(userDataSet, "UserAnalysisMapping")
                    End If

                    '-------------------------Account Type-------------------------------------------------
                    common_acctTypequery = common_acctTypequery.Replace("#AnalysisIds#", analysis)
                    commonAdapter = New SqlDataAdapter(common_acctTypequery, connection)
                    commonAdapter.Fill(userDataSet, "AcctType")
                    '--------------------------------------------------------------------------------------

                    '-------------------------Account------------------------------------------------------
                    common_acctquery = common_acctquery.Replace("#AnalysisIds#", analysis)
                    commonAdapter = New SqlDataAdapter(common_acctquery, connection)
                    commonAdapter.Fill(userDataSet, "Account")
                    '-----------------------------------------------------------------------------


                    '-------------------------Balance-----------------------------------------
                    common_balancequery = common_balancequery.Replace("#AnalysisIds#", analysis)
                    commonAdapter = New SqlDataAdapter(common_balancequery, connection)
                    commonAdapter.Fill(userDataSet, "Balance")
                    '-----------------------------------------------------------------------------

                    '-------------------------Dashboard-----------------------------------------
                    common_dashboardquery = common_dashboardquery.Replace("#AnalysisIds#", analysis)
                    commonAdapter = New SqlDataAdapter(common_dashboardquery, connection)
                    commonAdapter.Fill(userDataSet, "Dashboard")
                    '-----------------------------------------------------------------------------

                    '-------------------------SaveView-----------------------------------------
                    common_saveviewquery = common_saveviewquery.Replace("#AnalysisIds#", analysis)
                    commonAdapter = New SqlDataAdapter(common_saveviewquery, connection)
                    commonAdapter.Fill(userDataSet, "SaveView")
                    '-----------------------------------------------------------------------------

                    '-------------------------Scorecard-----------------------------------------
                    common_scorecardquery = common_scorecardquery.Replace("#AnalysisIds#", analysis)
                    commonAdapter = New SqlDataAdapter(common_scorecardquery, connection)
                    commonAdapter.Fill(userDataSet, "Scorecard")
                    '-----------------------------------------------------------------------------
                End If


                lblError.Visible = True
                If (response = 0) Then
                    lblError.Text = "Invalid Customer Id."
                End If

                Dim columns As String
                For Each table As DataTable In userDataSet.Tables
                    Dim tablename As String = table.TableName.ToString().ToLower()


                    insertScript.Append(Environment.NewLine)
                    If (tablename = "company" Or tablename = "analysis" Or tablename = "dashboard" Or tablename = "saveview" Or tablename = "scorecard" _
                        Or tablename = "usercompanymapping" Or tablename = "useranalysismapping") Then
                        insertScript.Append(Environment.NewLine)
                        insertScript.Append("SET IDENTITY_INSERT [dbo].[" + table.TableName + "] ON")
                    End If
                    columns = GetColumns(table)
                    GetRows(table, columns)
                    If (tablename = "company" Or tablename = "analysis" Or tablename = "dashboard" Or tablename = "saveview" Or tablename = "scorecard" _
                        Or tablename = "usercompanymapping" Or tablename = "useranalysismapping") Then
                        insertScript.Append(Environment.NewLine)
                        insertScript.Append("SET IDENTITY_INSERT [dbo].[" + table.TableName + "] OFF")
                    End If
                Next

            Catch ex As Exception
                lblError.Text = String.Concat(ex.Message.ToString())
            Finally
                connection.Close()
                connection.Dispose()
            End Try
        End Using

    End Sub

    Private Function GetColumns(table As DataTable)

        Dim columnString As String = String.Empty

        For Each col As DataColumn In table.Columns

            If (columnString = String.Empty) Then
                columnString = String.Concat("[", col.ColumnName.ToString(), "]")
            Else
                columnString = String.Concat(columnString, ", [", col.ColumnName.ToString(), "]")
            End If

        Next

        Return String.Concat(Environment.NewLine, " INSERT INTO [", table.TableName.ToString(), "](", columnString, ")")

    End Function

    Private Function GetRows(table As DataTable, columns As String)

        For Each row As DataRow In table.Rows
            Dim values As New StringBuilder(" VALUES (")
            For i = 0 To table.Columns.Count - 1

                If (row.Item(i).GetType().Name().Equals("DBNull")) Then
                    values.Append("NULL")
                Else
                    Select Case table.Columns(i).DataType.Name
                        Case "Int16", "Int32", "Int64", "Decimal", "SByte"
                            values.Append(row.Item(i))
                            Exit Select
                        Case "Boolean"
                            values.Append(Convert.ToInt16(row.Item(i)))
                            Exit Select
                        Case "DateTime"
                            values.Append("CAST('" + row.Item(i).ToString() + "' AS DateTime)")
                            Exit Select
                        Case "String"
                            values.Append(String.Concat("'", row.Item(i).ToString().Replace("'", "''"), "'"))
                            Exit Select
                        Case Else
                            Exit Select
                    End Select
                End If

                If (i <> table.Columns.Count - 1) Then
                    values.Append(",")
                End If
            Next

            values.Append(")")
            insertScript.Append(columns)
            insertScript.Append(values)
        Next


        Return insertScript

    End Function
End Class
