USE [onlineAnalytics.DataAccess]
GO

/****** Object:  Table [dbo].[Scorecard]    Script Date: 03/11/2014 13:11:34 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Scorecard]') AND type in (N'U'))
DROP TABLE [dbo].[Scorecard]
GO

/****** Object:  Table [dbo].[Scorecard]    Script Date: 03/11/2014 13:11:34 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Scorecard](
	[ScorecardId] [int] IDENTITY(1,1) NOT NULL,
	[Order] [int] NOT NULL,
	[AccountId] [int] NOT NULL,
	[AnalysisId] [int] NOT NULL,
	[ScorecardDesc] [nvarchar](50) NULL,
	[Missed] [int] NOT NULL,
	[Outperform] [int] NOT NULL,
	[ShowasPercent] [bit] NOT NULL,
	[VarIsFavorable] [bit] NOT NULL,
	[Option1] [int] NOT NULL,
	[Option2] [int] NOT NULL,
	[Option3] [int] NOT NULL,
	[IsValid] [bit] NOT NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedOn] [datetime] NOT NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedOn] [datetime] NOT NULL,
 CONSTRAINT [PK_Scorecard] PRIMARY KEY CLUSTERED 
(
	[scorecardID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


