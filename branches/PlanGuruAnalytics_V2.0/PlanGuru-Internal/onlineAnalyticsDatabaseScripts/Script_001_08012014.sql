USE [onlineAnalytics.DataAccess]
GO

/****** Object:  Table [dbo].[SaveView]    Script Date: 01/08/2014 18:38:22 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SaveView]') AND type in (N'U'))
DROP TABLE [dbo].[SaveView]
GO

USE [onlineAnalytics.DataAccess]
GO

/****** Object:  Table [dbo].[SaveView]    Script Date: 01/08/2014 18:38:22 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[SaveView](
	[analysisID] [int] NOT NULL,
	[viewname] [nvarchar](50) NOT NULL,
	[controller] [nvarchar](3) NOT NULL,
	[format] [int] NOT NULL,
	[showaspercent] [bit] NOT NULL,
	[showbudget] [bit] NOT NULL,
	[highlightva] [bit] NOT NULL,
	[posvar] [bit] NULL,
	[posvaramt] [decimal](18, 2) NULL,
	[negvar] [bit] NULL,
	[negvaramt] [decimal](18, 2) NULL,
	[filter] [bit] NOT NULL,
	[filteron] [int] NULL,
	[filteramt] [decimal](18, 2) NULL,
	[charttype] [int] NOT NULL,
	[trend] [bit] NULL,
	[account1] [int] NULL,
	[account2] [int] NULL,
	[account3] [int] NULL,
	[account4] [int] NULL,
	[account5] [int] NULL,
	[account6] [int] NULL,
	[account7] [int] NULL,
	[account8] [int] NULL,
	[account9] [int] NULL,
	[account10] [int] NULL,
 CONSTRAINT [PK_SaveView] PRIMARY KEY CLUSTERED 
(
	[analysisID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


