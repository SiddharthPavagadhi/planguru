USE [PGFileUpload]
GO
/****** Object:  Table [dbo].[UserRolePermission]    Script Date: 06/10/2013 11:19:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserRolePermission](
	[UserRolePermissionId] [int] IDENTITY(1,1) NOT NULL,
	[Description] [nvarchar](200) NULL,
PRIMARY KEY CLUSTERED 
(
	[UserRolePermissionId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[UserRolePermission] ON
INSERT [dbo].[UserRolePermission] ([UserRolePermissionId], [Description]) VALUES (1, N'Subscription Management')
INSERT [dbo].[UserRolePermission] ([UserRolePermissionId], [Description]) VALUES (2, N'User Management')
INSERT [dbo].[UserRolePermission] ([UserRolePermissionId], [Description]) VALUES (3, N'Search Customer')
INSERT [dbo].[UserRolePermission] ([UserRolePermissionId], [Description]) VALUES (4, N'View Subscription')
INSERT [dbo].[UserRolePermission] ([UserRolePermissionId], [Description]) VALUES (5, N'Add Subscription')
INSERT [dbo].[UserRolePermission] ([UserRolePermissionId], [Description]) VALUES (6, N'Update Subscription')
INSERT [dbo].[UserRolePermission] ([UserRolePermissionId], [Description]) VALUES (7, N'Cancel Subscription')
INSERT [dbo].[UserRolePermission] ([UserRolePermissionId], [Description]) VALUES (8, N'Add User')
INSERT [dbo].[UserRolePermission] ([UserRolePermissionId], [Description]) VALUES (9, N'Delete User')
INSERT [dbo].[UserRolePermission] ([UserRolePermissionId], [Description]) VALUES (10, N'View User')
INSERT [dbo].[UserRolePermission] ([UserRolePermissionId], [Description]) VALUES (11, N'Update User')
INSERT [dbo].[UserRolePermission] ([UserRolePermissionId], [Description]) VALUES (12, N'Add Company')
INSERT [dbo].[UserRolePermission] ([UserRolePermissionId], [Description]) VALUES (13, N'Delete Company')
INSERT [dbo].[UserRolePermission] ([UserRolePermissionId], [Description]) VALUES (14, N'View Companies')
INSERT [dbo].[UserRolePermission] ([UserRolePermissionId], [Description]) VALUES (15, N'Update Company')
INSERT [dbo].[UserRolePermission] ([UserRolePermissionId], [Description]) VALUES (16, N'User Company Mapping')
INSERT [dbo].[UserRolePermission] ([UserRolePermissionId], [Description]) VALUES (17, N'Add Analysis')
INSERT [dbo].[UserRolePermission] ([UserRolePermissionId], [Description]) VALUES (18, N'Delete Analysis')
INSERT [dbo].[UserRolePermission] ([UserRolePermissionId], [Description]) VALUES (19, N'View Analyses')
INSERT [dbo].[UserRolePermission] ([UserRolePermissionId], [Description]) VALUES (20, N'Update Analysis')
INSERT [dbo].[UserRolePermission] ([UserRolePermissionId], [Description]) VALUES (21, N'User Analysis Mapping')
INSERT [dbo].[UserRolePermission] ([UserRolePermissionId], [Description]) VALUES (22, N'View Analysis Info')
INSERT [dbo].[UserRolePermission] ([UserRolePermissionId], [Description]) VALUES (23, N'Print Analysis Info')
INSERT [dbo].[UserRolePermission] ([UserRolePermissionId], [Description]) VALUES (24, N'Modify Dashboard')
SET IDENTITY_INSERT [dbo].[UserRolePermission] OFF
/****** Object:  Table [dbo].[UserRole]    Script Date: 06/10/2013 11:19:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserRole](
	[UserRoleId] [int] IDENTITY(1,1) NOT NULL,
	[RoleName] [nvarchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[UserRoleId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[UserRole] ON
INSERT [dbo].[UserRole] ([UserRoleId], [RoleName]) VALUES (1, N'PGAU')
INSERT [dbo].[UserRole] ([UserRoleId], [RoleName]) VALUES (2, N'PGSU')
INSERT [dbo].[UserRole] ([UserRoleId], [RoleName]) VALUES (3, N'SAU')
INSERT [dbo].[UserRole] ([UserRoleId], [RoleName]) VALUES (4, N'CAU')
INSERT [dbo].[UserRole] ([UserRoleId], [RoleName]) VALUES (5, N'CRU')
SET IDENTITY_INSERT [dbo].[UserRole] OFF
/****** Object:  Table [dbo].[UserCompanyMapping]    Script Date: 06/10/2013 11:19:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserCompanyMapping](
	[UserCompanyMappingId] [int] IDENTITY(1,1) NOT NULL,
	[CompanyId] [int] NOT NULL,
	[UserId] [int] NULL,
	[CratedBy] [nvarchar](max) NULL,
	[CratedOn] [datetime] NOT NULL,
	[UpdatedBy] [nvarchar](max) NULL,
	[UpdatedOn] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[UserCompanyMappingId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[UserCompanyMapping] ON
INSERT [dbo].[UserCompanyMapping] ([UserCompanyMappingId], [CompanyId], [UserId], [CratedBy], [CratedOn], [UpdatedBy], [UpdatedOn]) VALUES (1, 1, 1, N'1', CAST(0x0000A1CD00CCE54E AS DateTime), N'1', CAST(0x0000A1CD00CCE54E AS DateTime))
INSERT [dbo].[UserCompanyMapping] ([UserCompanyMappingId], [CompanyId], [UserId], [CratedBy], [CratedOn], [UpdatedBy], [UpdatedOn]) VALUES (2, 2, 1, N'1', CAST(0x0000A1CD00CCFA75 AS DateTime), N'1', CAST(0x0000A1CD00CCFA75 AS DateTime))
SET IDENTITY_INSERT [dbo].[UserCompanyMapping] OFF
/****** Object:  Table [dbo].[UserAnalysisMapping]    Script Date: 06/10/2013 11:19:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserAnalysisMapping](
	[UserAnalysisMappingId] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NULL,
	[AnalysisId] [int] NOT NULL,
	[CratedBy] [nvarchar](max) NULL,
	[CratedOn] [datetime] NOT NULL,
	[UpdatedBy] [nvarchar](max) NULL,
	[UpdatedOn] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[UserAnalysisMappingId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[UserAnalysisMapping] ON
INSERT [dbo].[UserAnalysisMapping] ([UserAnalysisMappingId], [UserId], [AnalysisId], [CratedBy], [CratedOn], [UpdatedBy], [UpdatedOn]) VALUES (1, 0, 1, N'1', CAST(0x0000A1CD00CD0B83 AS DateTime), N'1', CAST(0x0000A1CD00CD0B83 AS DateTime))
INSERT [dbo].[UserAnalysisMapping] ([UserAnalysisMappingId], [UserId], [AnalysisId], [CratedBy], [CratedOn], [UpdatedBy], [UpdatedOn]) VALUES (2, 1, 2, N'1', CAST(0x0000A1CD00CD1839 AS DateTime), N'1', CAST(0x0000A1CD00CD1839 AS DateTime))
INSERT [dbo].[UserAnalysisMapping] ([UserAnalysisMappingId], [UserId], [AnalysisId], [CratedBy], [CratedOn], [UpdatedBy], [UpdatedOn]) VALUES (3, 1, 3, N'1', CAST(0x0000A1CD00CD20C3 AS DateTime), N'1', CAST(0x0000A1CD00CD20C3 AS DateTime))
INSERT [dbo].[UserAnalysisMapping] ([UserAnalysisMappingId], [UserId], [AnalysisId], [CratedBy], [CratedOn], [UpdatedBy], [UpdatedOn]) VALUES (4, 1, 4, N'1', CAST(0x0000A1CD00CD2C5D AS DateTime), N'1', CAST(0x0000A1CD00CD2C5D AS DateTime))
SET IDENTITY_INSERT [dbo].[UserAnalysisMapping] OFF
/****** Object:  Table [dbo].[User]    Script Date: 06/10/2013 11:19:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User](
	[UserId] [int] NOT NULL,
	[UserName] [nvarchar](50) NOT NULL,
	[Password] [nvarchar](max) NULL,
	[FirstName] [nvarchar](50) NOT NULL,
	[LastName] [nvarchar](50) NOT NULL,
	[UserEmail] [nvarchar](50) NOT NULL,
	[Status] [int] NOT NULL,
	[SecurityKey] [nvarchar](max) NULL,
	[UserRoleId] [int] NOT NULL,
	[CustomerId] [nvarchar](128) NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[UpdatedBy] [int] NOT NULL,
	[UpdatedOn] [datetime] NOT NULL,
	[AnalysisId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[User] ([UserId], [UserName], [Password], [FirstName], [LastName], [UserEmail], [Status], [SecurityKey], [UserRoleId], [CustomerId], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn], [AnalysisId]) VALUES (1, N'Sanjay Malvi', N'8nWCZFIxkhg+oI/ZHD65LA==', N'Sanjay', N'Malvi', N'sanjay.m@sigmainfo.net', 2, N'5b15d827-0745-4571-baba-1a0c93878588', 4, N'88cf36f6-192b-41e9-bcf3-dcd81af41940', 1, CAST(0x0000A1CD00CC97A6 AS DateTime), 1, CAST(0x0000A1CD00CC97A6 AS DateTime), 0)
/****** Object:  Table [dbo].[SubscriptionTransactionRequestResponse]    Script Date: 06/10/2013 11:19:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SubscriptionTransactionRequestResponse](
	[SubscriptionRequestResponseId] [int] IDENTITY(1,1) NOT NULL,
	[Signature] [nvarchar](max) NULL,
	[PlanCode] [nvarchar](max) NULL,
	[AccountCode] [nvarchar](max) NULL,
	[TimeStamp] [nvarchar](max) NULL,
	[Nonce] [nvarchar](max) NULL,
	[UUID] [nvarchar](max) NULL,
	[State] [nvarchar](max) NULL,
	[Result] [nvarchar](max) NULL,
	[CratedBy] [nvarchar](max) NULL,
	[CratedOn] [datetime] NOT NULL,
	[UpdatedBy] [nvarchar](max) NULL,
	[UpdatedOn] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[SubscriptionRequestResponseId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Status]    Script Date: 06/10/2013 11:19:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Status](
	[StatusId] [int] IDENTITY(1,1) NOT NULL,
	[StatusName] [nvarchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[StatusId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Status] ON
INSERT [dbo].[Status] ([StatusId], [StatusName]) VALUES (1, N'Pending')
INSERT [dbo].[Status] ([StatusId], [StatusName]) VALUES (2, N'Active')
INSERT [dbo].[Status] ([StatusId], [StatusName]) VALUES (3, N'Suspended')
INSERT [dbo].[Status] ([StatusId], [StatusName]) VALUES (4, N'Deactivated')
SET IDENTITY_INSERT [dbo].[Status] OFF
/****** Object:  Table [dbo].[StateCode]    Script Date: 06/10/2013 11:19:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StateCode](
	[StateId] [int] IDENTITY(1,1) NOT NULL,
	[StateName] [nvarchar](max) NULL,
	[Code] [nvarchar](max) NULL,
	[CountryId] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[StateId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[StateCode] ON
INSERT [dbo].[StateCode] ([StateId], [StateName], [Code], [CountryId]) VALUES (1, N'Alabama', N'AL', 250)
INSERT [dbo].[StateCode] ([StateId], [StateName], [Code], [CountryId]) VALUES (2, N'Alaska', N'AK', 250)
INSERT [dbo].[StateCode] ([StateId], [StateName], [Code], [CountryId]) VALUES (3, N'Arizona', N'AZ', 250)
INSERT [dbo].[StateCode] ([StateId], [StateName], [Code], [CountryId]) VALUES (4, N'Arkansas', N'AR', 250)
INSERT [dbo].[StateCode] ([StateId], [StateName], [Code], [CountryId]) VALUES (5, N'California', N'CA', 250)
INSERT [dbo].[StateCode] ([StateId], [StateName], [Code], [CountryId]) VALUES (6, N'Colorado', N'CO', 250)
INSERT [dbo].[StateCode] ([StateId], [StateName], [Code], [CountryId]) VALUES (7, N'Connecticut', N'CT', 250)
INSERT [dbo].[StateCode] ([StateId], [StateName], [Code], [CountryId]) VALUES (8, N'Delaware', N'DE', 250)
INSERT [dbo].[StateCode] ([StateId], [StateName], [Code], [CountryId]) VALUES (9, N'District of Columbia', N'DC', 250)
INSERT [dbo].[StateCode] ([StateId], [StateName], [Code], [CountryId]) VALUES (10, N'Florida', N'FL', 250)
INSERT [dbo].[StateCode] ([StateId], [StateName], [Code], [CountryId]) VALUES (11, N'Georgia', N'GA', 250)
INSERT [dbo].[StateCode] ([StateId], [StateName], [Code], [CountryId]) VALUES (12, N'Hawaii', N'HI', 250)
INSERT [dbo].[StateCode] ([StateId], [StateName], [Code], [CountryId]) VALUES (13, N'Idaho', N'ID', 250)
INSERT [dbo].[StateCode] ([StateId], [StateName], [Code], [CountryId]) VALUES (14, N'Illinois', N'IL', 250)
INSERT [dbo].[StateCode] ([StateId], [StateName], [Code], [CountryId]) VALUES (15, N'Indiana', N'IN', 250)
INSERT [dbo].[StateCode] ([StateId], [StateName], [Code], [CountryId]) VALUES (16, N'Iowa', N'IA', 250)
INSERT [dbo].[StateCode] ([StateId], [StateName], [Code], [CountryId]) VALUES (17, N'Kansas', N'KS', 250)
INSERT [dbo].[StateCode] ([StateId], [StateName], [Code], [CountryId]) VALUES (18, N'Kentucky', N'KY', 250)
INSERT [dbo].[StateCode] ([StateId], [StateName], [Code], [CountryId]) VALUES (19, N'Louisiana', N'LA', 250)
INSERT [dbo].[StateCode] ([StateId], [StateName], [Code], [CountryId]) VALUES (20, N'Maine', N'ME', 250)
INSERT [dbo].[StateCode] ([StateId], [StateName], [Code], [CountryId]) VALUES (21, N'Maryland', N'MD', 250)
INSERT [dbo].[StateCode] ([StateId], [StateName], [Code], [CountryId]) VALUES (22, N'Massachusetts', N'MA', 250)
INSERT [dbo].[StateCode] ([StateId], [StateName], [Code], [CountryId]) VALUES (23, N'Michigan', N'MI', 250)
INSERT [dbo].[StateCode] ([StateId], [StateName], [Code], [CountryId]) VALUES (24, N'Minnesota', N'MN', 250)
INSERT [dbo].[StateCode] ([StateId], [StateName], [Code], [CountryId]) VALUES (25, N'Mississippi', N'MS', 250)
INSERT [dbo].[StateCode] ([StateId], [StateName], [Code], [CountryId]) VALUES (26, N'Missouri', N'MO', 250)
INSERT [dbo].[StateCode] ([StateId], [StateName], [Code], [CountryId]) VALUES (27, N'Montana', N'MT', 250)
INSERT [dbo].[StateCode] ([StateId], [StateName], [Code], [CountryId]) VALUES (28, N'Nebraska', N'NE', 250)
INSERT [dbo].[StateCode] ([StateId], [StateName], [Code], [CountryId]) VALUES (29, N'Nevada', N'NV', 250)
INSERT [dbo].[StateCode] ([StateId], [StateName], [Code], [CountryId]) VALUES (30, N'New Hampshire', N'NH', 250)
INSERT [dbo].[StateCode] ([StateId], [StateName], [Code], [CountryId]) VALUES (31, N'New Jersey', N'NJ', 250)
INSERT [dbo].[StateCode] ([StateId], [StateName], [Code], [CountryId]) VALUES (32, N'New Mexico', N'NM', 250)
INSERT [dbo].[StateCode] ([StateId], [StateName], [Code], [CountryId]) VALUES (33, N'New York', N'NY', 250)
INSERT [dbo].[StateCode] ([StateId], [StateName], [Code], [CountryId]) VALUES (34, N'North Carolina', N'NC', 250)
INSERT [dbo].[StateCode] ([StateId], [StateName], [Code], [CountryId]) VALUES (35, N'North Dakota', N'ND', 250)
INSERT [dbo].[StateCode] ([StateId], [StateName], [Code], [CountryId]) VALUES (36, N'Ohio', N'OH', 250)
INSERT [dbo].[StateCode] ([StateId], [StateName], [Code], [CountryId]) VALUES (37, N'Oklahoma', N'OK', 250)
INSERT [dbo].[StateCode] ([StateId], [StateName], [Code], [CountryId]) VALUES (38, N'Oregon', N'OR', 250)
INSERT [dbo].[StateCode] ([StateId], [StateName], [Code], [CountryId]) VALUES (39, N'Pennsylvania', N'PA', 250)
INSERT [dbo].[StateCode] ([StateId], [StateName], [Code], [CountryId]) VALUES (40, N'Rhode Island', N'RI', 250)
INSERT [dbo].[StateCode] ([StateId], [StateName], [Code], [CountryId]) VALUES (41, N'South Carolina', N'SC', 250)
INSERT [dbo].[StateCode] ([StateId], [StateName], [Code], [CountryId]) VALUES (42, N'South Dakota', N'SD', 250)
INSERT [dbo].[StateCode] ([StateId], [StateName], [Code], [CountryId]) VALUES (43, N'Tennessee', N'TN', 250)
INSERT [dbo].[StateCode] ([StateId], [StateName], [Code], [CountryId]) VALUES (44, N'Texas', N'TX', 250)
INSERT [dbo].[StateCode] ([StateId], [StateName], [Code], [CountryId]) VALUES (45, N'Utah', N'UT', 250)
INSERT [dbo].[StateCode] ([StateId], [StateName], [Code], [CountryId]) VALUES (46, N'Vermont', N'VT', 250)
INSERT [dbo].[StateCode] ([StateId], [StateName], [Code], [CountryId]) VALUES (47, N'Virginia', N'VA', 250)
INSERT [dbo].[StateCode] ([StateId], [StateName], [Code], [CountryId]) VALUES (48, N'Washington', N'WA', 250)
INSERT [dbo].[StateCode] ([StateId], [StateName], [Code], [CountryId]) VALUES (49, N'West Virginia', N'WV', 250)
INSERT [dbo].[StateCode] ([StateId], [StateName], [Code], [CountryId]) VALUES (50, N'Wisconsin', N'WI', 250)
INSERT [dbo].[StateCode] ([StateId], [StateName], [Code], [CountryId]) VALUES (51, N'Wyoming', N'WY', 250)
SET IDENTITY_INSERT [dbo].[StateCode] OFF
/****** Object:  Table [dbo].[FileUploadedResult]    Script Date: 06/10/2013 11:19:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FileUploadedResult](
	[FileId] [int] IDENTITY(1,1) NOT NULL,
	[TableId] [int] NOT NULL,
	[SuccessCount] [int] NOT NULL,
	[FailCount] [int] NOT NULL,
	[TotalCount] [int] NOT NULL,
	[FileProcessId] [int] NULL,
 CONSTRAINT [PK_FileUploadedResult] PRIMARY KEY CLUSTERED 
(
	[FileId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[FileProcess]    Script Date: 06/10/2013 11:19:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FileProcess](
	[FileProcessId] [int] IDENTITY(1,1) NOT NULL,
	[FileName] [varchar](50) NULL,
	[AnalysisId] [int] NOT NULL,
	[UserId] [int] NOT NULL,
	[UploadedDate] [datetime] NULL,
	[CompanyId] [int] NOT NULL,
 CONSTRAINT [PK_FileProcess] PRIMARY KEY CLUSTERED 
(
	[FileProcessId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[EmailInfo]    Script Date: 06/10/2013 11:19:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EmailInfo](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[EmailType] [nvarchar](max) NULL,
	[EmailSubject] [nvarchar](max) NULL,
	[EmailBody] [nvarchar](max) NULL,
	[CreatedDateTime] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[EmailInfo] ON
INSERT [dbo].[EmailInfo] ([Id], [EmailType], [EmailSubject], [EmailBody], [CreatedDateTime]) VALUES (1, N'NewSubscriptionSignup', N'New Subscription Signup', N'<html>
	
	<body>

			<strong>New subscription Signup</strong></p>
		<p>
			Thank you for your PlanGuru Online Analytics subscription order. Your credit card has been billed for your first subscription payment of $##AMOUNT##.</p>
		<p>
			<u>Subscription details</u></p>
		<p>
			Email Address: ##NewCustomerEmail##</p>
		<p>
			Billing Address:</p>
		<p>
			##NewCustomerName##<br />
			##NewCustomerStreet##<br />
			##NewCustomerCityStateEtc##</p>
		<p>
			Subscription Schedule:##SUBSCRIPTIONSCHEDULE##</p>
		<p>
			Your credit card on file will be billed monthly on the ##DayoftheMonth## in the amount of $##AMOUNT##.</p>
		<p>
		<p>
			<u>To manage your subscription on-line</u></p>
			&lt;Instructions for managing the account on-line&gt;</p>
		<p>
			<u>Getting started with PlanGuru Online Analytics</u></p>
		<p>
			&lt;Provide links and text to a Getting Started Guide and a Users Guide&gt;</p>
	</body>
</html>
', CAST(0x0000A1CD00CB185E AS DateTime))
INSERT [dbo].[EmailInfo] ([Id], [EmailType], [EmailSubject], [EmailBody], [CreatedDateTime]) VALUES (2, N'NewUserAdded', N'New User Added', N'<html>	
	<body>
		<p>
			<strong>New User Added</strong></p>		
		
			This email confirms that ##USERID## has been added to PlanGuru Online Analytics.</p>
		<p>
			<u>User Details</u></p>
		<p>
			Email: ##EMAIL##<br />
			User ID: ##USERID##<br />
			User&rsquo;s Full Name: ##USERFULLNAME##<br />
			Temporary Password: ##TEMPPASSWORD##</p>
		<p>
			To reset your temporary password, ##PASSWORDRESETLINK##.</p>
		<p> &lt;include instructions for resetting their password when they sign into Online Analytics&gt;.</p>
		<p>
			<u>Getting started with PlanGuru Online Analytics</u></p>
		<p>
			&lt;Provide links and text to a Getting Started Guide and a User&rsquo;s Guide&gt;</p>
	</body>
</html>
', CAST(0x0000A1CD00CB185E AS DateTime))
INSERT [dbo].[EmailInfo] ([Id], [EmailType], [EmailSubject], [EmailBody], [CreatedDateTime]) VALUES (3, N'NewUserSignUp', N'New User Sign Up', N'<html>
	<head>
		<title></title>
	</head>
	<body>

			<strong>Password Reset </strong></p>
		<p>
			A password reset has been requested for your PlanGuru Analytics user account. &nbsp;You have been assigned the new temporary password shown below.</p>
		<p>
			<u>User Details</u></p>
		<p>
			Email: ##EMAIL##<br />
			User ID: ##USERID##<br />
			User&rsquo;s Full Name: ##USERFULLNAME##</p>
		<p>
			Temporary Password: ##TEMPPASSWORD##</p>
		<p>
			To reset your temporary password, ##PASSWORDRESETLINK## &lt;include instructions for resetting their password when they sign into Online Analytics&gt;.</p>
		<p>
			If you did not request this password reset, contact PlanGuru Customer Support at ##PLANGURUTELEPHONENUMBER##.</p>
	</body>
</html>
', CAST(0x0000A1CD00CB185E AS DateTime))
INSERT [dbo].[EmailInfo] ([Id], [EmailType], [EmailSubject], [EmailBody], [CreatedDateTime]) VALUES (4, N'PasswordReset', N'Password Reset', N'<html>
	
	<body>

			<strong>New subscription Signup</strong></p>
		<p>
			Thank you for your PlanGuru Online Analytics subscription order. Your credit card has been billed for your first subscription payment of $##AMOUNT##.</p>
		<p>
			<u>Subscription details</u></p>
		<p>
			Email Address: ##NewCustomerEmail##</p>
		<p>
			Billing Address:</p>
		<p>
			##NewCustomerName##<br />
			##NewCustomerStreet##<br />
			##NewCustomerCityStateEtc##</p>
		<p>
			Subscription Schedule:##SUBSCRIPTIONSCHEDULE##</p>
		<p>
			Your credit card on file will be billed monthly on the ##DayoftheMonth## in the amount of $##AMOUNT##.</p>
		<p>
		<p>
			<u>To manage your subscription on-line</u></p>
			&lt;Instructions for managing the account on-line&gt;</p>
		<p>
			<u>Getting started with PlanGuru Online Analytics</u></p>
		<p>
			&lt;Provide links and text to a Getting Started Guide and a Users Guide&gt;</p>
	</body>
</html>
', CAST(0x0000A1CD00CB185E AS DateTime))
INSERT [dbo].[EmailInfo] ([Id], [EmailType], [EmailSubject], [EmailBody], [CreatedDateTime]) VALUES (5, N'SubcriptionCancellation', N'Subcription Cancellation', N'<html>
	<head>
		<title></title>
	</head>
	<body>

			<strong>Subscription Cancellation</strong></p>
		<p>
			You have successfully cancelled your PlanGuru Online Analytics subscription. PlanGuru will no longer bill you. If you did not request this cancellation, contact PlanGuru Customer Support at ##TelephoneNumber##.</p>
		<p>
			<u>Subscription Details</u></p>
		<p>
			Customer ID: ##CUSTOMERID##<br />
			Subscription Start Date: ##SUBSCRIPTIONSTARTDATE##<br />
			Payment Cycle: ##PAYMENTCYCLE##<br />
			Amount per Month: $##AMOUNT##</p>
	</body>
</html>
', CAST(0x0000A1CD00CB185E AS DateTime))
INSERT [dbo].[EmailInfo] ([Id], [EmailType], [EmailSubject], [EmailBody], [CreatedDateTime]) VALUES (6, N'UserDeleted', N'User Deleted', N'<html>
	<head>
		<title></title>
	</head>
	<body>
		
			<strong>User Deleted</strong></p>

			This email confirms that ##USERID## has been deleted from PlanGuru Online Analytics on ##TODAYDATE##.</p>
		<p>
			Your subscription will no longer be billed $19.95 for this user.</p>
		<p>
			<u>User Details</u></p>
		<p>
			Email: ##EMAIL##<br />
			User ID: ##USERID##<br />
			User&rsquo;s Full Name:##USERFULLNAME##</p>
	</body>
</html>
', CAST(0x0000A1CD00CB185E AS DateTime))
SET IDENTITY_INSERT [dbo].[EmailInfo] OFF
/****** Object:  Table [dbo].[EdmMetadata]    Script Date: 06/10/2013 11:19:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EdmMetadata](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ModelHash] [nvarchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[EdmMetadata] ON
INSERT [dbo].[EdmMetadata] ([Id], [ModelHash]) VALUES (1, N'8FCA75486FAE78F5D963EC0BED594D4AC7E524F453D46B7FA6248DC3311FE876')
SET IDENTITY_INSERT [dbo].[EdmMetadata] OFF
/****** Object:  Table [dbo].[Dashboard]    Script Date: 06/10/2013 11:19:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Dashboard](
	[DashboardId] [int] IDENTITY(1,1) NOT NULL,
	[DashDescription] [nvarchar](50) NOT NULL,
	[ChartFormat] [int] NOT NULL,
	[ChartType] [int] NOT NULL,
	[ShowAsPercent] [bit] NOT NULL,
	[ShowTrendline] [bit] NOT NULL,
	[ShowGoal] [bit] NOT NULL,
	[Period1Goal] [decimal](18, 2) NOT NULL,
	[GoalGrowthRate] [decimal](18, 2) NOT NULL,
	[Account_AccountId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[DashboardId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CustomerRequestType]    Script Date: 06/10/2013 11:19:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CustomerRequestType](
	[RequestId] [int] IDENTITY(1,1) NOT NULL,
	[Description] [nvarchar](200) NULL,
PRIMARY KEY CLUSTERED 
(
	[RequestId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Customer]    Script Date: 06/10/2013 11:19:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Customer](
	[CustomerId] [nvarchar](128) NOT NULL,
	[CustomerFirstName] [nvarchar](50) NULL,
	[CustomerLastName] [nvarchar](50) NULL,
	[CustomerCompanyName] [nvarchar](150) NULL,
	[Quantity] [int] NOT NULL,
	[ContactFirstName] [nvarchar](50) NULL,
	[ContactLastName] [nvarchar](50) NULL,
	[CustomerAddress1] [nvarchar](100) NULL,
	[CustomerAddress2] [nvarchar](100) NULL,
	[Country] [nvarchar](50) NULL,
	[State] [nvarchar](50) NULL,
	[City] [nvarchar](50) NULL,
	[CustomerPostalCode] [nvarchar](10) NULL,
	[ContactTelephone] [nvarchar](15) NULL,
	[CustomerEmail] [nvarchar](200) NULL,
	[FirstNameOnCard] [nvarchar](max) NULL,
	[LastNameOnCard] [nvarchar](max) NULL,
	[CreditCardNumber] [nvarchar](max) NULL,
	[CVV] [nvarchar](max) NULL,
	[ExpirationYear] [int] NOT NULL,
	[ExpirationMonth] [int] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[UpdatedBy] [int] NOT NULL,
	[UpdatedOn] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[CustomerId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[Customer] ([CustomerId], [CustomerFirstName], [CustomerLastName], [CustomerCompanyName], [Quantity], [ContactFirstName], [ContactLastName], [CustomerAddress1], [CustomerAddress2], [Country], [State], [City], [CustomerPostalCode], [ContactTelephone], [CustomerEmail], [FirstNameOnCard], [LastNameOnCard], [CreditCardNumber], [CVV], [ExpirationYear], [ExpirationMonth], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (N'1cfd23ee-f19c-4a6f-8721-098696abe7c9', N'Paul', N'Demalo', N'SIGMA INFOSOLUTIONS LTD', 1, N'Paul', N'Demalo', N'Prahalad Nagar', N'Safal Peagasus', N'India', N'Gujarat', N'Ahmedabad', N'380015', N'1234567890', N'pratik.p@sigmainfo.net', N'JOHN', N'BOSCO', N'41111111111123458904', N'123', 2020, 12, 1, CAST(0x0000A1CD00CC0FF5 AS DateTime), 1, CAST(0x0000A1CD00CC0FF5 AS DateTime))
INSERT [dbo].[Customer] ([CustomerId], [CustomerFirstName], [CustomerLastName], [CustomerCompanyName], [Quantity], [ContactFirstName], [ContactLastName], [CustomerAddress1], [CustomerAddress2], [Country], [State], [City], [CustomerPostalCode], [ContactTelephone], [CustomerEmail], [FirstNameOnCard], [LastNameOnCard], [CreditCardNumber], [CVV], [ExpirationYear], [ExpirationMonth], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (N'88cf36f6-192b-41e9-bcf3-dcd81af41940', N'JOHN', N'BOSCO', N'SIGMA INFOSOLUTIONS LTD', 1, N'Haresh', N'Prajapati', N'Prahalad Nagar', N'Safal Peagasus', N'India', N'Gujarat', N'Ahmedabad', N'380015', N'1234567890', N'pratik.p@sigmainfo.net', N'JOHN', N'BOSCO', N'41111111111123458904', N'123', 2020, 12, 1, CAST(0x0000A1CD00CC0FF5 AS DateTime), 1, CAST(0x0000A1CD00CC0FF5 AS DateTime))
/****** Object:  Table [dbo].[CountryCode]    Script Date: 06/10/2013 11:19:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CountryCode](
	[CountryId] [int] IDENTITY(1,1) NOT NULL,
	[CountryName] [nvarchar](max) NULL,
	[Code] [nvarchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[CountryId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[CountryCode] ON
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (1, N'Afghanistan', N'AF')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (2, N'Albania', N'AL')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (3, N'Algeria', N'DZ')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (4, N'American Samoa', N'AS')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (5, N'Andorra', N'AD')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (6, N'Angola', N'AO')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (7, N'Anguilla', N'AI')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (8, N'Antarctica', N'AQ')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (9, N'Antigua and Barbuda', N'AG')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (10, N'Argentina', N'AR')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (11, N'Armenia', N'AM')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (12, N'Aruba', N'AW')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (13, N'Australia', N'AU')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (14, N'Austria', N'AT')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (15, N'Azerbaijan', N'AZ')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (16, N'Bahamas', N'BS')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (17, N'Bahrain', N'BH')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (18, N'Bangladesh', N'BD')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (19, N'Barbados', N'BB')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (20, N'Belarus', N'BY')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (21, N'Belgium', N'BE')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (22, N'Belize', N'BZ')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (23, N'Benin', N'BJ')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (24, N'Bermuda', N'BM')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (25, N'Bhutan', N'BT')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (26, N'Bolivia', N'BO')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (27, N'Bosnia and Herzegovina', N'BA')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (28, N'Botswana', N'BW')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (29, N'Bouvet Island', N'BV')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (30, N'Brazil', N'BR')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (31, N'British Antarctic Territory', N'BQ')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (32, N'British Indian Ocean Territory', N'IO')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (33, N'British Virgin Islands', N'VG')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (34, N'Brunei', N'BN')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (35, N'Bulgaria', N'BG')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (36, N'Burkina Faso', N'BF')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (37, N'Burundi', N'BI')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (38, N'Cambodia', N'KH')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (39, N'Cameroon', N'CM')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (40, N'Canada', N'CA')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (41, N'Canton and Enderbury Islands', N'CT')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (42, N'Cape Verde', N'CV')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (43, N'Cayman Islands', N'KY')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (44, N'Central African Republic', N'CF')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (45, N'Chad', N'TD')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (46, N'Chile', N'CL')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (47, N'China', N'CN')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (48, N'Christmas Island', N'CX')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (49, N'Cocos [Keeling] Islands', N'CC')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (50, N'Colombia', N'CO')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (51, N'Comoros', N'KM')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (52, N'Congo - Brazzaville', N'CG')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (53, N'Congo - Kinshasa', N'CD')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (54, N'Cook Islands', N'CK')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (55, N'Costa Rica', N'CR')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (56, N'Croatia', N'HR')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (57, N'Cuba', N'CU')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (58, N'Cyprus', N'CY')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (59, N'Czech Republic', N'CZ')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (60, N'Côte d Ivoire', N'CI')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (61, N'Denmark', N'DK')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (62, N'Djibouti', N'DJ')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (63, N'Dominica', N'DM')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (64, N'Dominican Republic', N'DO')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (65, N'Dronning Maud Land', N'NQ')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (66, N'East Germany', N'DD')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (67, N'Ecuador', N'EC')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (68, N'Egypt', N'EG')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (69, N'El Salvador', N'SV')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (70, N'Equatorial Guinea', N'GQ')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (71, N'Eritrea', N'ER')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (72, N'Estonia', N'EE')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (73, N'Ethiopia', N'ET')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (74, N'Falkland Islands', N'FK')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (75, N'Faroe Islands', N'FO')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (76, N'Fiji', N'FJ')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (77, N'Finland', N'FI')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (78, N'France', N'FR')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (79, N'French Guiana', N'GF')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (80, N'French Polynesia', N'PF')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (81, N'French Southern Territories', N'TF')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (82, N'French Southern and Antarctic Territories', N'FQ')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (83, N'Gabon', N'GA')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (84, N'Gambia', N'GM')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (85, N'Georgia', N'GE')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (86, N'Germany', N'DE')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (87, N'Ghana', N'GH')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (88, N'Gibraltar', N'GI')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (89, N'Greece', N'GR')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (90, N'Greenland', N'GL')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (91, N'Grenada', N'GD')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (92, N'Guadeloupe', N'GP')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (93, N'Guam', N'GU')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (94, N'Guatemala', N'GT')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (95, N'Guernsey', N'GG')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (96, N'Guinea', N'GN')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (97, N'Guinea-Bissau', N'GW')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (98, N'Guyana', N'GY')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (99, N'Haiti', N'HT')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (100, N'Heard Island and McDonald Islands', N'HM')
GO
print 'Processed 100 total records'
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (101, N'Honduras', N'HN')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (102, N'Hong Kong SAR China', N'HK')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (103, N'Hungary', N'HU')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (104, N'Iceland', N'IS')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (105, N'India', N'IN')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (106, N'Indonesia', N'ID')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (107, N'Iran', N'IR')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (108, N'Iraq', N'IQ')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (109, N'Ireland', N'IE')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (110, N'Isle of Man', N'IM')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (111, N'Israel', N'IL')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (112, N'Italy', N'IT')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (113, N'Jamaica', N'JM')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (114, N'Japan', N'JP')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (115, N'Jersey', N'JE')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (116, N'Johnston Island', N'JT')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (117, N'Jordan', N'JO')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (118, N'Kazakhstan', N'KZ')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (119, N'Kenya', N'KE')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (120, N'Kiribati', N'KI')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (121, N'Kuwait', N'KW')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (122, N'Kyrgyzstan', N'KG')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (123, N'Laos', N'LA')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (124, N'Latvia', N'LV')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (125, N'Lebanon', N'LB')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (126, N'Lesotho', N'LS')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (127, N'Liberia', N'LR')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (128, N'Libya', N'LY')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (129, N'Liechtenstein', N'LI')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (130, N'Lithuania', N'LT')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (131, N'Luxembourg', N'LU')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (132, N'Macau SAR China', N'MO')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (133, N'Macedonia', N'MK')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (134, N'Madagascar', N'MG')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (135, N'Malawi', N'MW')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (136, N'Malaysia', N'MY')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (137, N'Maldives', N'MV')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (138, N'Mali', N'ML')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (139, N'Malta', N'MT')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (140, N'Marshall Islands', N'MH')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (141, N'Martinique', N'MQ')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (142, N'Mauritania', N'MR')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (143, N'Mauritius', N'MU')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (144, N'Mayotte', N'YT')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (145, N'Metropolitan France', N'FX')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (146, N'Mexico', N'MX')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (147, N'Micronesia', N'FM')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (148, N'Midway Islands', N'MI')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (149, N'Moldova', N'MD')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (150, N'Monaco', N'MC')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (151, N'Mongolia', N'MN')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (152, N'Montenegro', N'ME')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (153, N'Montserrat', N'MS')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (154, N'Morocco', N'MA')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (155, N'Mozambique', N'MZ')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (156, N'Myanmar [Burma]', N'MM')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (157, N'Namibia', N'NA')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (158, N'Nauru', N'NR')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (159, N'Nepal', N'NP')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (160, N'Netherlands', N'NL')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (161, N'Netherlands Antilles', N'AN')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (162, N'Neutral Zone', N'NT')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (163, N'New Caledonia', N'NC')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (164, N'New Zealand', N'NZ')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (165, N'Nicaragua', N'NI')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (166, N'Niger', N'NE')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (167, N'Nigeria', N'NG')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (168, N'Niue', N'NU')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (169, N'Norfolk Island', N'NF')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (170, N'North Korea', N'KP')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (171, N'North Vietnam', N'VD')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (172, N'Northern Mariana Islands', N'MP')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (173, N'Norway', N'NO')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (174, N'Oman', N'OM')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (175, N'Pacific Islands Trust Territory', N'PC')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (176, N'Pakistan', N'PK')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (177, N'Palau', N'PW')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (178, N'Palestinian Territories', N'PS')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (179, N'Panama', N'PA')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (180, N'Panama Canal Zone', N'PZ')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (181, N'Papua New Guinea', N'PG')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (182, N'Paraguay', N'PY')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (183, N'People"s Democratic Republic of Yemen', N'YD')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (184, N'Peru', N'PE')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (185, N'Philippines', N'PH')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (186, N'Pitcairn Islands', N'PN')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (187, N'Poland', N'PL')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (188, N'Portugal', N'PT')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (189, N'Puerto Rico', N'PR')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (190, N'Qatar', N'QA')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (191, N'Romania', N'RO')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (192, N'Russia', N'RU')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (193, N'Rwanda', N'RW')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (194, N'Réunion', N'RE')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (195, N'Saint Barthélemy', N'BL')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (196, N'Saint Helena', N'SH')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (197, N'Saint Kitts and Nevis', N'KN')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (198, N'Saint Lucia', N'LC')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (199, N'Saint Martin', N'MF')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (200, N'Saint Pierre and Miquelon', N'PM')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (201, N'Saint Vincent and the Grenadines', N'VC')
GO
print 'Processed 200 total records'
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (202, N'Samoa', N'WS')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (203, N'San Marino', N'SM')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (204, N'Saudi Arabia', N'SA')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (205, N'Senegal', N'SN')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (206, N'Serbia', N'RS')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (207, N'Serbia and Montenegro', N'CS')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (208, N'Seychelles', N'SC')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (209, N'Sierra Leone', N'SL')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (210, N'Singapore', N'SG')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (211, N'Slovakia', N'SK')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (212, N'Slovenia', N'SI')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (213, N'Solomon Islands', N'SB')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (214, N'Somalia', N'SO')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (215, N'South Africa', N'ZA')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (216, N'South Georgia and the South Sandwich Islands', N'GS')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (217, N'South Korea', N'KR')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (218, N'Spain', N'ES')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (219, N'Sri Lanka', N'LK')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (220, N'Sudan', N'SD')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (221, N'Suriname', N'SR')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (222, N'Svalbard and Jan Mayen', N'SJ')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (223, N'Swaziland', N'SZ')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (224, N'Sweden', N'SE')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (225, N'Switzerland', N'CH')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (226, N'Syria', N'SY')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (227, N'São Tomé and Príncipe', N'ST')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (228, N'Taiwan', N'TW')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (229, N'Tajikistan', N'TJ')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (230, N'Tanzania', N'TZ')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (231, N'Thailand', N'TH')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (232, N'Timor-Leste', N'TL')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (233, N'Togo', N'TG')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (234, N'Tokelau', N'TK')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (235, N'Tonga', N'TO')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (236, N'Trinidad and Tobago', N'TT')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (237, N'Tunisia', N'TN')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (238, N'Turkey', N'TR')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (239, N'Turkmenistan', N'TM')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (240, N'Turks and Caicos Islands', N'TC')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (241, N'Tuvalu', N'TV')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (242, N'U.S. Minor Outlying Islands', N'UM')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (243, N'U.S. Miscellaneous Pacific Islands', N'PU')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (244, N'U.S. Virgin Islands', N'VI')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (245, N'Uganda', N'UG')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (246, N'Ukraine', N'UA')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (247, N'Union of Soviet Socialist Republics', N'SU')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (248, N'United Arab Emirates', N'AE')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (249, N'United Kingdom', N'GB')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (250, N'United States', N'US')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (251, N'Unknown or Invalid Region', N'ZZ')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (252, N'Uruguay', N'UY')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (253, N'Uzbekistan', N'UZ')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (254, N'Vanuatu', N'VU')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (255, N'Vatican City', N'VA')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (256, N'Venezuela', N'VE')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (257, N'Vietnam', N'VN')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (258, N'Wake Island', N'WK')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (259, N'Wallis and Futuna', N'WF')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (260, N'Western Sahara', N'EH')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (261, N'Yemen', N'YE')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (262, N'Zambia', N'ZM')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (263, N'Zimbabwe', N'ZW')
INSERT [dbo].[CountryCode] ([CountryId], [CountryName], [Code]) VALUES (264, N'Åland Islands', N'AX')
SET IDENTITY_INSERT [dbo].[CountryCode] OFF
/****** Object:  Table [dbo].[Company]    Script Date: 06/10/2013 11:19:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Company](
	[CompanyId] [int] IDENTITY(1,1) NOT NULL,
	[CompanyName] [nvarchar](50) NOT NULL,
	[FiscalMonthStart] [nvarchar](max) NOT NULL,
	[FiscalMonthName] [nvarchar](max) NULL,
	[ContactFirstName] [nvarchar](50) NOT NULL,
	[ContactLastName] [nvarchar](50) NOT NULL,
	[ContactEmail] [nvarchar](50) NOT NULL,
	[ContactTelephone] [nvarchar](15) NOT NULL,
	[CustomerId] [int] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[UpdatedBy] [int] NOT NULL,
	[UpdatedOn] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[CompanyId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Company] ON
INSERT [dbo].[Company] ([CompanyId], [CompanyName], [FiscalMonthStart], [FiscalMonthName], [ContactFirstName], [ContactLastName], [ContactEmail], [ContactTelephone], [CustomerId], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (1, N'Sigma Infosolutions Ltd.', N'2', NULL, N'FSigma', N'LSigma', N'sanjay.m@sigmainfo.net', N'(665)-645-6645', 1, 1, CAST(0x0000A1CD00CCE546 AS DateTime), 1, CAST(0x0000A1CD00CCE546 AS DateTime))
INSERT [dbo].[Company] ([CompanyId], [CompanyName], [FiscalMonthStart], [FiscalMonthName], [ContactFirstName], [ContactLastName], [ContactEmail], [ContactTelephone], [CustomerId], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (2, N'IBM', N'3', NULL, N'IBMFN', N'IBMLN', N'sanjay.m@sigmainfo.net', N'(886)-682-3571', 1, 1, CAST(0x0000A1CD00CCFA71 AS DateTime), 1, CAST(0x0000A1CD00CCFA71 AS DateTime))
SET IDENTITY_INSERT [dbo].[Company] OFF
/****** Object:  Table [dbo].[Balance]    Script Date: 06/10/2013 11:19:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Balance](
	[BalanceId] [int] NOT NULL,
	[YearType] [nvarchar](2) NOT NULL,
	[Balance1] [decimal](18, 2) NOT NULL,
	[Balance2] [decimal](18, 2) NOT NULL,
	[Balance3] [decimal](18, 2) NOT NULL,
	[Balance4] [decimal](18, 2) NOT NULL,
	[Balance5] [decimal](18, 2) NOT NULL,
	[Balance6] [decimal](18, 2) NOT NULL,
	[Balance7] [decimal](18, 2) NOT NULL,
	[Balance8] [decimal](18, 2) NOT NULL,
	[Balance9] [decimal](18, 2) NOT NULL,
	[Balance10] [decimal](18, 2) NOT NULL,
	[Balance11] [decimal](18, 2) NOT NULL,
	[Balance12] [decimal](18, 2) NOT NULL,
	[AccountId] [int] NOT NULL,
	[AnalysisId] [int] NOT NULL,
 CONSTRAINT [Balance_Account_Analysis] PRIMARY KEY CLUSTERED 
(
	[AccountId] ASC,
	[BalanceId] ASC,
	[AnalysisId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AcctType]    Script Date: 06/10/2013 11:19:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AcctType](
	[AcctTypeId] [int] NOT NULL,
	[TypeDesc] [nvarchar](50) NOT NULL,
	[ClassDesc] [nvarchar](50) NULL,
	[SubclassDesc] [nvarchar](50) NULL,
	[AnalysisId] [int] NOT NULL,
 CONSTRAINT [Analysis_AcctType] PRIMARY KEY CLUSTERED 
(
	[AcctTypeId] ASC,
	[AnalysisId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Account]    Script Date: 06/10/2013 11:19:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Account](
	[AccountId] [int] NOT NULL,
	[AcctDescriptor] [nvarchar](50) NOT NULL,
	[SortSequence] [int] NOT NULL,
	[Description] [nvarchar](50) NOT NULL,
	[Subgrouping] [nvarchar](50) NULL,
	[AcctTypeId] [int] NOT NULL,
	[AnalysisId] [int] NOT NULL,
 CONSTRAINT [Account_Analysis_AcctType] PRIMARY KEY CLUSTERED 
(
	[AcctTypeId] ASC,
	[AnalysisId] ASC,
	[AccountId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserTables]    Script Date: 06/10/2013 11:19:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[UserTables](
	[UserTableId] [int] IDENTITY(1,1) NOT NULL,
	[UserTableName] [varchar](50) NOT NULL,
 CONSTRAINT [PK_UserTables] PRIMARY KEY CLUSTERED 
(
	[UserTableId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[UserTables] ON
INSERT [dbo].[UserTables] ([UserTableId], [UserTableName]) VALUES (1, N'AcctType')
INSERT [dbo].[UserTables] ([UserTableId], [UserTableName]) VALUES (2, N'Account')
INSERT [dbo].[UserTables] ([UserTableId], [UserTableName]) VALUES (3, N'Balance')
SET IDENTITY_INSERT [dbo].[UserTables] OFF
/****** Object:  Table [dbo].[UserRolePermissionMapping]    Script Date: 06/10/2013 11:19:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserRolePermissionMapping](
	[UserRolePermissionMappingId] [int] IDENTITY(1,1) NOT NULL,
	[UserRoleId] [int] NOT NULL,
	[UserRolePermissionId] [int] NOT NULL,
	[CratedBy] [nvarchar](max) NULL,
	[CratedOn] [datetime] NOT NULL,
	[UpdatedBy] [nvarchar](max) NULL,
	[UpdatedOn] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[UserRolePermissionMappingId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Analysis]    Script Date: 06/10/2013 11:19:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Analysis](
	[AnalysisId] [int] IDENTITY(1,1) NOT NULL,
	[AnalysisName] [nvarchar](50) NOT NULL,
	[CompanyId] [int] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[UpdatedBy] [int] NOT NULL,
	[UpdatedOn] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[AnalysisId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Analysis] ON
INSERT [dbo].[Analysis] ([AnalysisId], [AnalysisName], [CompanyId], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (1, N'Sigma Analysis One', 1, 1, CAST(0x0000A1CD00CD0B19 AS DateTime), 1, CAST(0x0000A1CD00CD0B19 AS DateTime))
INSERT [dbo].[Analysis] ([AnalysisId], [AnalysisName], [CompanyId], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (2, N'Sigma Analysis Two', 1, 1, CAST(0x0000A1CD00CD1836 AS DateTime), 1, CAST(0x0000A1CD00CD1836 AS DateTime))
INSERT [dbo].[Analysis] ([AnalysisId], [AnalysisName], [CompanyId], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (3, N'IBM A1', 2, 1, CAST(0x0000A1CD00CD20C1 AS DateTime), 1, CAST(0x0000A1CD00CD20C1 AS DateTime))
INSERT [dbo].[Analysis] ([AnalysisId], [AnalysisName], [CompanyId], [CreatedBy], [CreatedOn], [UpdatedBy], [UpdatedOn]) VALUES (4, N'IBM A2', 2, 1, CAST(0x0000A1CD00CD2C5B AS DateTime), 1, CAST(0x0000A1CD00CD2C5B AS DateTime))
SET IDENTITY_INSERT [dbo].[Analysis] OFF
/****** Object:  Table [dbo].[CustomerResponseMapping]    Script Date: 06/10/2013 11:19:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CustomerResponseMapping](
	[CustomerResponseMappingId] [int] IDENTITY(1,1) NOT NULL,
	[RequestId] [int] NOT NULL,
	[CustomerId] [int] NOT NULL,
	[CratedBy] [nvarchar](max) NULL,
	[CratedOn] [datetime] NOT NULL,
	[UpdatedBy] [nvarchar](max) NULL,
	[UpdatedOn] [datetime] NOT NULL,
	[Customer_CustomerId] [nvarchar](128) NULL,
PRIMARY KEY CLUSTERED 
(
	[CustomerResponseMappingId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  ForeignKey [Analysis_Company]    Script Date: 06/10/2013 11:19:42 ******/
ALTER TABLE [dbo].[Analysis]  WITH CHECK ADD  CONSTRAINT [Analysis_Company] FOREIGN KEY([CompanyId])
REFERENCES [dbo].[Company] ([CompanyId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Analysis] CHECK CONSTRAINT [Analysis_Company]
GO
/****** Object:  ForeignKey [CustomerResponseMapping_Customer]    Script Date: 06/10/2013 11:19:42 ******/
ALTER TABLE [dbo].[CustomerResponseMapping]  WITH CHECK ADD  CONSTRAINT [CustomerResponseMapping_Customer] FOREIGN KEY([Customer_CustomerId])
REFERENCES [dbo].[Customer] ([CustomerId])
GO
ALTER TABLE [dbo].[CustomerResponseMapping] CHECK CONSTRAINT [CustomerResponseMapping_Customer]
GO
/****** Object:  ForeignKey [CustomerResponseMapping_CustomerRequestType]    Script Date: 06/10/2013 11:19:42 ******/
ALTER TABLE [dbo].[CustomerResponseMapping]  WITH CHECK ADD  CONSTRAINT [CustomerResponseMapping_CustomerRequestType] FOREIGN KEY([RequestId])
REFERENCES [dbo].[CustomerRequestType] ([RequestId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[CustomerResponseMapping] CHECK CONSTRAINT [CustomerResponseMapping_CustomerRequestType]
GO
/****** Object:  ForeignKey [UserRolePermissionMapping_UserRole]    Script Date: 06/10/2013 11:19:42 ******/
ALTER TABLE [dbo].[UserRolePermissionMapping]  WITH CHECK ADD  CONSTRAINT [UserRolePermissionMapping_UserRole] FOREIGN KEY([UserRoleId])
REFERENCES [dbo].[UserRole] ([UserRoleId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[UserRolePermissionMapping] CHECK CONSTRAINT [UserRolePermissionMapping_UserRole]
GO
/****** Object:  ForeignKey [UserRolePermissionMapping_UserRolePermission]    Script Date: 06/10/2013 11:19:42 ******/
ALTER TABLE [dbo].[UserRolePermissionMapping]  WITH CHECK ADD  CONSTRAINT [UserRolePermissionMapping_UserRolePermission] FOREIGN KEY([UserRolePermissionId])
REFERENCES [dbo].[UserRolePermission] ([UserRolePermissionId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[UserRolePermissionMapping] CHECK CONSTRAINT [UserRolePermissionMapping_UserRolePermission]
GO
