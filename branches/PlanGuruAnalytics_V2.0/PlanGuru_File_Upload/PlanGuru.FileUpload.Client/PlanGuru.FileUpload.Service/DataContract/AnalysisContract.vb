﻿<System.Runtime.Serialization.DataContractAttribute()> _
Public Class AnalysisContract
    Inherits BaseContract
 
    <DataMember()> _
    Public Property AnalysisId() As Integer
 
    <DataMember()> _
    Public Property AnalysisName() As String
 
End Class

