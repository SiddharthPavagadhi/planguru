﻿Imports System.Text
Imports PlanGuru.FileUpload.TestApp.PlanGuruService
Imports System.IO

Imports PlanGuru.FileUpload.BusinessLayer

<TestClass()>
Public Class PlanGuruTestClass

    'Private testContextInstance As TestContext

    ' '''<summary>
    ' '''Gets or sets the test context which provides
    ' '''information about and functionality for the current test run.
    ' '''</summary>
    'Public Property TestContext() As TestContext
    '    Get
    '        Return testContextInstance
    '    End Get
    '    Set(ByVal value As TestContext)
    '        testContextInstance = value
    '    End Set
    'End Property

#Region "ServiceClient"
    ''' <summary>
    ''' This is Service Client object Instanciate method.
    ''' </summary>
    ''' <returns>Service Client Object with instantiation</returns>
    ''' <remarks></remarks>
    Public Function ServiceClient() As PlanGuruFileUploadServiceClient
        Return New PlanGuruFileUploadServiceClient
    End Function
#End Region

#Region "Login"
    ''' <summary>
    ''' This method should give Result as Passed
    ''' </summary>
    ''' <remarks></remarks>
    <TestMethod()>
    Public Sub LoginPositve()
        Dim objserviceClient = ServiceClient()
        Dim objBaseContract As BaseContract = New BaseContract()
        objBaseContract.UserName = "sanjay malvi"
        objBaseContract.Password = "2pratik"
        Dim objloginContract = objserviceClient.Login(objBaseContract)
        Dim isValidLogin As Boolean = False
        If (objloginContract.ErrorMessage Is Nothing) Then
            isValidLogin = True
        End If

        Assert.IsTrue(isValidLogin)
    End Sub

    ''' <summary>
    ''' This method should give Result as Failed
    ''' </summary>
    ''' <remarks></remarks>
    <TestMethod()>
    Public Sub LoginNegative()
        Dim objserviceClient = ServiceClient()
        Dim objBaseContract As BaseContract = New BaseContract()
        objBaseContract.UserName = "sanjaymalvi"
        objBaseContract.Password = "2pratik"
        Dim objloginContract = objserviceClient.Login(objBaseContract)

        Dim isValidLogin As Boolean = False
        If (objloginContract.ErrorMessage Is Nothing) Then
            isValidLogin = True
        End If

        Assert.IsTrue(isValidLogin)
    End Sub
#End Region

#Region "Company"
    ''' <summary>
    ''' This method should give Result as Passed
    ''' </summary>
    ''' <remarks></remarks>
    <TestMethod()>
    Public Sub GetCompaniesPositive()
        Dim client = ServiceClient()
        Dim objRequestContract As CompanyRequestContract = New CompanyRequestContract()
        objRequestContract.UserId = 1
        objRequestContract.UserName = "sanjay malvi"
        objRequestContract.Password = "2pratik"
        Dim company = client.GetCompaniesByUserId(objRequestContract)

        Assert.IsTrue(company.Length > 0)
    End Sub

    ''' <summary>
    ''' This method should give Result as Failed
    ''' </summary>
    ''' <remarks></remarks>
    <TestMethod()>
    Public Sub GetCompaniesNegative()
        Dim client = ServiceClient()
        Dim objRequestContract As CompanyRequestContract = New CompanyRequestContract()
        objRequestContract.UserId = 2
        objRequestContract.UserName = "sanjay malvi"
        objRequestContract.Password = "2pratik"
        Dim company = client.GetCompaniesByUserId(objRequestContract)

        Assert.IsTrue(company.Length > 0)
    End Sub
#End Region

#Region "Analysis"
    ''' <summary>
    ''' This method should give Result as Passed
    ''' </summary>
    ''' <remarks></remarks>
    <TestMethod()>
    Public Sub GetAnalysisPositive()
        Dim client = ServiceClient()
        Dim objAnalysisRequest As AnalysisRequest = New AnalysisRequest()
        objAnalysisRequest.UserId = 1
        objAnalysisRequest.UserName = "sanjay malvi"
        objAnalysisRequest.Password = "2pratik"
        objAnalysisRequest.CompanyId = 1
        Dim analysis = client.GetAnalysisByCompanyIdUserId(objAnalysisRequest)
        Assert.IsTrue(analysis.Length > 0)
    End Sub

    ''' <summary>
    ''' Failed
    ''' </summary>
    ''' <remarks></remarks>
    <TestMethod()>
    Public Sub GetAnalysisNegative()
        Dim client = ServiceClient()
        Dim objAnalysisRequest As AnalysisRequest = New AnalysisRequest()
        objAnalysisRequest.UserId = 2
        objAnalysisRequest.UserName = "sanjay malvi"
        objAnalysisRequest.Password = "2pratik"
        objAnalysisRequest.CompanyId = 1
        Dim analysis = client.GetAnalysisByCompanyIdUserId(objAnalysisRequest)
        Assert.IsFalse(analysis.Length > 0)
    End Sub

#End Region

#Region "Upload File"

    ''' <summary>
    ''' This method should give Result as Passed
    ''' </summary>
    ''' <remarks></remarks>
    <TestMethod()>
    Public Sub UploadFilesPositive()
        Dim client = ServiceClient()
        Dim objExcelHelper As ExcelHelper = New ExcelHelper()
        GlobalSettings.UserName = "sanjay malvi"
        GlobalSettings.Password = "2pratik"
        GlobalSettings.UserId = 1

        Dim txtFilePath As String = "D:\Workspace\PlanGuru\Upload\ExcelFiles\UploadFileInvalidData.xls"
        Dim strMessage = objExcelHelper.ValidatExcel(txtFilePath)
        Dim FileData = ExcelHelper.GetDataFromExcel(txtFilePath)
        If strMessage.Equals(Common.SUCCESS) Then

            Dim objUploadBL As UploadBL = New UploadBL()

            If (strMessage.Equals(Common.SUCCESS)) Then
                Assert.IsTrue(True)
            Else
                Assert.IsTrue(False)
            End If
        Else
            Assert.IsTrue(False)
            Return
        End If

    End Sub

    ''' <summary>
    ''' This method should give Result as Failed
    ''' </summary>
    ''' <remarks></remarks>
    <TestMethod()>
    Public Sub UploadFilesNegative()
        Dim client = ServiceClient()
        Dim objExcelHelper As ExcelHelper = New ExcelHelper()
        GlobalSettings.UserName = "sanjay malvi"
        GlobalSettings.Password = "2pratik"
        GlobalSettings.UserId = 2

        Dim txtFilePath As String = "D:\Workspace\PlanGuru\Upload\ExcelFiles\UploadFileInvalidData.xls"
        Dim strMessage = objExcelHelper.ValidatExcel(txtFilePath)
        Dim FileData = ExcelHelper.GetDataFromExcel(txtFilePath)
        If strMessage.Equals(Common.SUCCESS) Then

            Dim objUploadBL As UploadBL = New UploadBL()

            If (strMessage.Equals(Common.SUCCESS)) Then
                Assert.IsTrue(True)
            Else
                Assert.IsTrue(False)
            End If
        Else
            Assert.IsTrue(False)
            Return
        End If

    End Sub

#End Region

End Class
