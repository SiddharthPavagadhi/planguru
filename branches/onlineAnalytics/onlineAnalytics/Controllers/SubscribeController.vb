﻿Imports System
Imports System.Collections.Generic
Imports System.Configuration
Imports System.Linq
Imports System.Web.Mvc
Imports System.Security.Cryptography
Imports PagedList
Imports onlineAnalytics
Imports Recurly

Namespace onlineAnalytics
    Public Class SubscribeController
        Inherits System.Web.Mvc.Controller

        Private utility As New Utility()
        Private common As New Common()
        Private customerRepository As ICustomerRepository
        Private userRepository As IUserRepository

        Public Sub New()
            Me.customerRepository = New CustomerRepository(New DataAccess())
        End Sub

        Public Sub New(customerRepository As ICustomerRepository)
            Me.customerRepository = customerRepository
        End Sub

        '
        ' GET: /User

        Function Subscribe() As ActionResult
            Try
                Dim subscription As String = String.Format("{0}={1}", "subscription%5Bplan_code%5D", "planguru-analytics")

                ViewBag.Signature = SignWithParameters(subscription)
                ViewBag.AccountCode = Guid.NewGuid().ToString()
                Session("AccountCode") = ViewBag.AccountCode.ToString()
                Logger.Log.Info(String.Format("Subscribe Execution Done"))
                Return View()
            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Unable to Subscribe()-", ex.Message)
                Logger.Log.Error(String.Format("\n Unable to Subscribe() with Message- {0} " + Environment.NewLine + "Stack Trace: {1} ", ex.Message, ex.StackTrace))
                Return View()
            Finally
                Logger.Log.Info(String.Format("Subscribe() Execution Ended"))
            End Try
           
        End Function

        ' This CreateUser Function allow user to enter required information to Create User.      
        Function CreateUser() As ActionResult
            Try
                PopulateMonthYearAndCardType()
                PopulateCountryList(0)
                PopulateStateList(0)
                Logger.Log.Info(String.Format("Subscribe CreateUser() Executed"))
                Return View()
            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Unable to Subscribe CreateUser()-", ex.Message)
                Logger.Log.Error(String.Format("\n Unable to Subscribe CreateUser() with Message- {0} " + Environment.NewLine + "Stack Trace: {1} ", ex.Message, ex.StackTrace))
                Return View()
            Finally
                Logger.Log.Info(String.Format("Subscribe CreateUser() Execution Ended"))
            End Try
        End Function

        Function ToUnixTimestamp(dt As DateTime) As Long
            Dim unixRef As New DateTime(1970, 1, 1, 0, 0, 0)
            Return (dt.Ticks - unixRef.Ticks) / 10000000
        End Function

        Private Sub SendEmail(customer As Customer, emailId As Integer)
            Try
                Dim objEmailInfo = utility.EmailRepository.GetEmailInfoById(emailId)
                objEmailInfo.EmailBody = common.MergeCustomerEmailBody(customer, objEmailInfo)
                common.SendCustomerEmail(customer, objEmailInfo)
                Logger.Log.Info(String.Format("SendEmail Executed"))
            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Unable to Send Email -", ex.Message)
                Logger.Log.Error(String.Format("Unable to Send Email to CustomerID - {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2} ", customer.CustomerId, ex.Message, ex.StackTrace))
            Finally
                Logger.Log.Info(String.Format("SendEmail Execution Ended"))
            End Try
        End Sub

        Function Verify() As ActionResult
            Return View()
        End Function

        Function Receipt() As ViewResult
            Dim customer As Customer = New Customer()
            Try
                Dim AccountCode As String = Session("AccountCode").ToString()

                If RecurlyAccount.IsExist(AccountCode) = True Then
                    Dim subscription As RecurlySubscription = RecurlySubscription.Get(AccountCode)

                    If Not (subscription Is Nothing) Then

                        Dim accountInfo As RecurlyAccount = RecurlyAccount.Get(AccountCode)
                        Dim billingInfo As RecurlyBillingInfo = RecurlyBillingInfo.Get(AccountCode)
                        Dim userInfo As New User()

                        customer.CustomerId = AccountCode
                        customer.Quantity = subscription.Quantity
                        customer.CustomerFirstName = accountInfo.FirstName
                        customer.CustomerLastName = accountInfo.LastName
                        customer.CustomerEmail = accountInfo.Email
                        customer.CustomerCompanyName = accountInfo.CompanyName
                        customer.ContactFirstName = String.Empty
                        customer.ContactLastName = String.Empty
                        'Credit Card Information                       
                        customer.FirstNameOnCard = billingInfo.FirstName
                        customer.LastNameOnCard = billingInfo.LastName
                        customer.CreditCardNumber = String.Concat("XXXX-XXXX-XXXX-", billingInfo.CreditCard.LastFour)
                        customer.CVV = billingInfo.VerificationValue
                        customer.ExpirationMonth = billingInfo.CreditCard.ExpirationMonth
                        customer.ExpirationYear = billingInfo.CreditCard.ExpirationYear
                        customer.CustomerAddress1 = billingInfo.Address1
                        customer.CustomerAddress2 = billingInfo.Address2
                        customer.CustomerPostalCode = billingInfo.PostalCode
                        customer.ContactTelephone = billingInfo.PhoneNumber
                        customer.Country = billingInfo.Country
                        customer.State = billingInfo.State
                        customer.City = billingInfo.City

                        customer.SubscriptionStartAt = Convert.ToDateTime(subscription.CurrentPeriodStartedAt).ToString("MMM dd, yyyy")
                        customer.SubscriptionEndAt = Convert.ToDateTime(subscription.CurrentPeriodEndsAt).ToString("MMM dd, yyyy")
                        customer.SubscriptionAmount = (subscription.UnitAmountInCents / 100)

                        customer.CreatedBy = 1
                        customer.UpdatedBy = 1
                        utility.CustomerRepository.InsertCustomer(customer)
                        utility.Save()

                        SendEmail(customer, EmailType.NewSubscriptionSignup)

                        'SAU user account creation
                        Dim password As String = common.Generate(8, 2)
                        userInfo.UserId = Guid.NewGuid().ToString()
                        userInfo.CustomerId = customer.CustomerId
                        userInfo.UserName = String.Concat(accountInfo.FirstName, " ", accountInfo.LastName)
                        userInfo.UserRoleId = UserRoles.SAU
                        userInfo.FirstName = accountInfo.FirstName
                        userInfo.LastName = accountInfo.LastName
                        userInfo.UserEmail = accountInfo.Email
                        userInfo.Status = CType(StatusE.Pending, Integer)
                        userInfo.CreatedBy = 1
                        userInfo.UpdatedBy = 1

                        userInfo.Password = common.Encrypt(password)
                        userInfo.SecurityKey = Guid.NewGuid().ToString()

                        utility.UserRepository.Insert(userInfo)
                        utility.UserRepository.Save()

                        SendEmail(customer, EmailType.NewUserSignUp)

                        ViewBag.SubscriptionInfo = customer
                        ViewBag.CreditCardLastFourDigit = billingInfo.CreditCard.LastFour.ToString()
                        ViewBag.CreditCardType = billingInfo.CreditCard.CreditCardType.ToString()
                    Else
                        TempData("ErrorMessage") = String.Concat("Account Code is missing !")
                        Logger.Log.Error(String.Format("Unable to Create Customer :- Account Code is missing."))
                    End If

                End If
                Logger.Log.Info(String.Format("Subscribe Receipt Function Executied"))
            Catch dataEx As DataException
                TempData("ErrorMessage") = String.Concat("Unable to create customer-", dataEx.Message)
                Logger.Log.Error(String.Format("Unable to Create Customer id- {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2} ", customer.CustomerId, dataEx.Message, dataEx.StackTrace))

            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Unable to create customer-", ex.Message)
                Logger.Log.Error(String.Format("Unable to Create Customer id- {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2} ", customer.CustomerId, ex.Message, ex.StackTrace))

            Finally
                Logger.Log.Info(String.Format("Execution Ended"))
            End Try
            Return View(customer)

        End Function

        Function Index() As ViewResult
            Try
                Dim customers = From cust In customerRepository.GetCustomers()
                Logger.Log.Info(String.Format("Subscribe Index() Executed"))
                Return View(customers.ToPagedList(1, 20))
            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Unable to show Cutomers-", ex.Message)
                Logger.Log.Error(String.Format("Unable to Show Customers, with Message- {0} " + Environment.NewLine + "Stack Trace: {1} ", ex.Message, ex.StackTrace))
                Return View()
            Finally
                Logger.Log.Info(String.Format("Subscribe Index() Execution Ended"))
            End Try
            
        End Function

        Function CancelSubscription(AccountCode As String) As ActionResult
            Logger.Log.Info(String.Format("Subscription cancellation started {0}", AccountCode))
            Try
                RecurlySubscription.TerminateSubscription(AccountCode)
                Logger.Log.Info(String.Format("Subscription deleted successfully on RECURLY of ACCOUNT-CODE - {0}", AccountCode))
                Dim customer = utility.CustomerRepository.GetCustomerById(AccountCode)
                utility.CustomerRepository.DeleteCustomer(AccountCode)
                utility.Save()

                SendEmail(customer, EmailType.SubcriptionCancellation)

                TempData("Message") = "Subscription cancelled successfully."
                Logger.Log.Info(String.Format("Subscription deleted successfully in PLANGURU database of ACCOUNT-CODE  {0}", AccountCode))

            Catch dataEx As DataException
                TempData("ErrorMessage") = String.Concat("Unable to cancel Subscription - ", dataEx.Message)
                Logger.Log.Error(String.Format("Unable to cancel Subscription, AccountCode- {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2}", AccountCode, dataEx.Message, dataEx.StackTrace))
                Return RedirectToAction("Index", New System.Web.Routing.RouteValueDictionary() _
                                        From {{"AccountCode", AccountCode}, {"CancelChangesError", True}})

            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Unable to cancel Subscription-", ex.Message)
                Logger.Log.Error(String.Format("Unable to cancel Subscription, AccountCode- {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2}", AccountCode, ex.Message, ex.StackTrace))
                ModelState.AddModelError("Index", "Unable to cancel Subscription." + ex.Message)

                Return RedirectToAction("Index")
            Finally
                Logger.Log.Info(String.Format("Execution Ended"))
            End Try

            Return RedirectToAction("Index")
        End Function

        Function EditSubscription(AccountCode As String) As ViewResult
            Try
                Dim subscription As String = String.Format("{0}={1}", "subscription%5Bplan_code%5D", "planguru-analytics")
                ViewBag.Signature = SignWithParameters(subscription)

                Dim customerDetails = customerRepository.GetCustomerById(AccountCode)

                Session("AccountCodeToEditSubscription") = AccountCode

                Return View(customerDetails)

            Catch dataEx As DataException
                TempData("ErrorMessage") = String.Concat("Unable to Edit Subscription-", dataEx.Message)
                Logger.Log.Error(String.Format("\n Unable to Edit Subscription, AccountCode - {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2} ", AccountCode, dataEx.Message, dataEx.StackTrace))

            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Unable to Edit Subscription-", ex.Message)
                Logger.Log.Error(String.Format("\n Unable to Edit Subscription, AccountCode - {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2} ", AccountCode, ex.Message, ex.StackTrace))

            Finally
                Logger.Log.Info(String.Format("Execution Ended"))
            End Try

            Return Nothing

        End Function

        Function UpdateSubscription() As ActionResult
            Try
                Dim AccountCodeToEditSubscription As String = Session("AccountCodeToEditSubscription").ToString()
                Dim customer = customerRepository.GetCustomerById(AccountCodeToEditSubscription)

                If RecurlyAccount.IsExist(AccountCodeToEditSubscription) = True Then
                    Dim subscription As RecurlySubscription = RecurlySubscription.Get(AccountCodeToEditSubscription)
                    If Not (subscription Is Nothing) Then
                        Dim billingInfo As RecurlyBillingInfo = RecurlyBillingInfo.Get(AccountCodeToEditSubscription)

                        'Credit Card Information
                        customer.FirstNameOnCard = billingInfo.FirstName
                        customer.LastNameOnCard = billingInfo.LastName
                        customer.CreditCardNumber = String.Concat("XXXX-XXXX-XXXX-", billingInfo.CreditCard.LastFour)
                        customer.CVV = billingInfo.VerificationValue
                        customer.ExpirationMonth = billingInfo.CreditCard.ExpirationMonth
                        customer.ExpirationYear = billingInfo.CreditCard.ExpirationYear

                        customer.CustomerAddress1 = billingInfo.Address1
                        customer.CustomerAddress2 = billingInfo.Address2
                        customer.CustomerPostalCode = billingInfo.PostalCode
                        customer.ContactTelephone = billingInfo.PhoneNumber
                        customer.Country = billingInfo.Country
                        customer.State = billingInfo.State
                        customer.City = billingInfo.City

                        customer.CreatedBy = 1
                        customer.UpdatedBy = 1
                        utility.CustomerRepository.UpdateCustomer(customer)
                        utility.Save()


                        ViewBag.SubscriptionInfo = customer
                        ViewBag.CreditCardLastFourDigit = billingInfo.CreditCard.LastFour.ToString()
                        ViewBag.CreditCardType = billingInfo.CreditCard.CreditCardType.ToString()
                    Else
                        'To do show error message
                        ' Transaction unsuccessfull
                    End If
                End If
                Logger.Log.Info(String.Format("Subscribe Update Subscription Function Executed"))
                Return View(customer)
            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Unable to Update Subscription -", ex.Message)
                Logger.Log.Error(String.Format("Unable to Update Subscription, with Message- {0} " + Environment.NewLine + "Stack Trace: {1} ", ex.Message, ex.StackTrace))
                Return View()
            Finally
                Logger.Log.Info(String.Format("Subscribe Update Subscription Function Execution Ended"))
            End Try
        End Function

        <HttpGet()> _
        Function SubscribeUser() As ActionResult
            'Using dataAccess As New DataAccess()
            '    dataAccess.Customers.Add(User)
            '    dataAccess.SaveChanges()
            'End Using
            Return View()
        End Function

        Private Sub PopulateMonthYearAndCardType()
            Try
                ViewBag.SelectedMonth = New SelectList({1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12})

                Dim currentYear As Date = Date.Now
                Dim yearlist As List(Of String) = New List(Of String)

                For value As Integer = 0 To 10
                    yearlist.Add(currentYear.Year.ToString())
                    currentYear = currentYear.AddYears(1)
                Next

                ViewBag.SelectedYear = New SelectList(yearlist)
                ViewBag.SelectedCardType = New SelectList({"Visa", "MasterCard", "American Express", "Discover"})
                Logger.Log.Info(String.Format("Populating MonthYearAndCardType Executed"))
            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Error In Populateing Month Year and Card Type -", ex.Message)
                Logger.Log.Error(String.Format("Unable to Populating MonthYearAndCardType, with Message- {0} " + Environment.NewLine + "Stack Trace: {1} ", ex.Message, ex.StackTrace))
            Finally
                Logger.Log.Info(String.Format("Populating MonthYearAndCardType Execution Ended"))
            End Try
            
        End Sub

        Private Sub PopulateCountryList(Optional SelectedCountry As Object = Nothing)
            Logger.Log.Info(String.Format("Populate Company List  started "))
            Try
                Dim countries = utility.CountryRepository.Get(orderBy:=Function(q) q.OrderBy(Function(d) d.CountryName))
                ViewBag.SelectedCountry = New SelectList(countries, "CountryId", "CountryName", SelectedCountry)
                Logger.Log.Info(String.Format("Populate Company List successfully Loaded"))

            Catch dataEx As DataException
                TempData("ErrorMessage") = String.Concat("Unable to Populate Company List -", dataEx.Message)
                Logger.Log.Error(String.Format("Unable to Populate Company List with Message- {0} " + Environment.NewLine + "Stack Trace: {1} ", dataEx.Message, dataEx.StackTrace))
                Throw
            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Unable to Populate Company List-", ex.Message)
                Logger.Log.Error(String.Format("Unable to populate company list with Message- {0} " + Environment.NewLine + "Stack Trace: {1}", ex.Message, ex.StackTrace))
                Throw
            Finally
                Logger.Log.Info(String.Format("Execution Ended"))
            End Try
        End Sub

        Private Sub PopulateStateList(Optional SelectedState As Object = Nothing)
            Logger.Log.Info(String.Format("Populate Company List  started "))
            Try
                Dim states = utility.StateRepository.Get(orderBy:=Function(q) q.OrderBy(Function(d) d.StateName))
                ViewBag.SelectedState = New SelectList(states, "StateId", "StateName", SelectedState)
                Logger.Log.Info(String.Format("Populate Company List successfully Loaded"))
            Catch dataEx As DataException
                TempData("ErrorMessage") = String.Concat("Unable to Populate Company List -", dataEx.Message)
                Logger.Log.Error(String.Format("Unable to Populate Company List with Message- {0} " + Environment.NewLine + "Stack Trace: {1} ", dataEx.Message, dataEx.StackTrace))
                Throw
            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Unable to Populate Company List-", ex.Message)
                Logger.Log.Error(String.Format("Unable to populate company list with Message- {0} " + Environment.NewLine + "Stack Trace: {1}", ex.Message, ex.StackTrace))
                Throw
            Finally
                Logger.Log.Info(String.Format("Execution Ended"))
            End Try
        End Sub

        Private Shared Function SignWithParameters(ParamArray parameters As String()) As String
            Try
                Dim nonce = String.Format("{0}={1}", "nonce", Guid.NewGuid().ToString())
                Dim timestamp = String.Format("{0}={1}", "timestamp", GetUnixTimeStamp(DateTime.UtcNow))

                Dim signatureParameters As New List(Of String)()
                signatureParameters.Add(nonce)
                signatureParameters.AddRange(parameters)
                signatureParameters.Add(timestamp)

                Dim protectedString = [String].Join("&", signatureParameters)
                Logger.Log.Info(String.Format("SignWithParameters Executed"))
                Return GenerateHMAC(protectedString) & "|" & Convert.ToString(protectedString)
            Catch ex As Exception
                Logger.Log.Error(String.Format("Unable to SignWithParameters with Message- {0} " + Environment.NewLine + "Stack Trace: {1} ", ex.Message, ex.StackTrace))
                Return String.Empty
            Finally
                Logger.Log.Info(String.Format("SignWithParameters Execution Ended"))
            End Try
        End Function

        Private Shared Function GetUnixTimeStamp(timestamp As DateTime) As Integer
            Try
                Dim referenceDate = New DateTime(1970, 1, 1)
                Dim ts = New TimeSpan(timestamp.Ticks - referenceDate.Ticks)
                Logger.Log.Info(String.Format("GetUnixTimeStamp Executed"))
                Return (Convert.ToInt32(ts.TotalSeconds))
            Catch ex As Exception
                Logger.Log.Error(String.Format("Unable to GetUnixTimeStamp, with Message- {0} " + Environment.NewLine + "Stack Trace: {1} ", ex.Message, ex.StackTrace))
                Return 0
            Finally
                Logger.Log.Info(String.Format("GetUnixTimeStamp Execution Ended"))
            End Try
        End Function

        Private Shared Function GenerateHMAC(stringToHash As String) As String
            Try
                Dim privatekey = "ae389bb1e1b94e6a84fb57c940694a17"
                Dim hasher = New HMACSHA1(UTF8Encoding.UTF8.GetBytes(privatekey))
                Dim hashBytes = hasher.ComputeHash(UTF8Encoding.UTF8.GetBytes(stringToHash))
                Dim hexDigest = BitConverter.ToString(hashBytes).Replace("-", "").ToLower()
                Logger.Log.Info(String.Format("GenerateHMAC Executed"))
                Return hexDigest
            Catch ex As Exception
                Logger.Log.Error(String.Format("Unable to GenerateHMAC with Message- {0} " + Environment.NewLine + "Stack Trace: {1} ", ex.Message, ex.StackTrace))
                Return String.Empty
            Finally
                Logger.Log.Info(String.Format("GenerateHMAC Execution Ended"))
            End Try

        End Function

        Function SearchCustomer() As ActionResult
            Try
                Dim customers = New List(Of Customer)
                'From cust In customerRepository.GetCustomers()()
                Logger.Log.Info(String.Format("SearchCustomer Executed"))
                Return View(customers.ToPagedList(1, 20))
            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Unable to SearchCustomer -", ex.Message)
                Logger.Log.Error(String.Format("Unable to SearchCustomer with Message- {0} " + Environment.NewLine + "Stack Trace: {1} ", ex.Message, ex.StackTrace))
                Return View()
            Finally
                Logger.Log.Info(String.Format("SearchCustomer Execution Ended"))
            End Try
        End Function

        <HttpPost()> _
       <ValidateAntiForgeryToken()> _
        Function SearchCustomer(SubscribeUser As Customer) As ActionResult
            Dim customers As IEnumerable(Of Customer) = Enumerable.Empty(Of Customer)()
            Try
                If SubscribeUser.SearchCustId Is Nothing And SubscribeUser.SearchCustName Is Nothing And SubscribeUser.SAUName Is Nothing And SubscribeUser.SearchCustTelephone Is Nothing Then
                    TempData("ErrorMessage") = String.Concat("Please Enter value in Search Option")
                    Return View(customers.ToPagedList(1, 20))
                End If
                customers = From cust In customerRepository.SearchCustomers(SubscribeUser.SearchCustId, SubscribeUser.SearchCustName, SubscribeUser.SAUName, SubscribeUser.SearchCustTelephone)
                Logger.Log.Info(String.Format("SearchCustomer Executed"))
                Return View(customers.ToPagedList(1, 20))

            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Unable to Search Custgomer-", ex.Message)
                Logger.Log.Error(String.Format("Unable to SearchCustomer, CustomerId - {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2} ", SubscribeUser.CustomerId, ex.Message, ex.StackTrace))
                Return View(customers.ToPagedList(1, 20))
            Finally
                Logger.Log.Info(String.Format("SearchCustomer Execution Ended"))
            End Try

        End Function
    End Class
End Namespace
