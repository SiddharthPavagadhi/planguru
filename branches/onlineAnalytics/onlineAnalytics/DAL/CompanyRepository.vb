﻿Imports System.Collections.Generic
Imports System.Linq
Imports System.Data

Public Class CompanyRepository
    Implements ICompanyRepository
    Implements IDisposable
    Private context As DataAccess
    Private disposed As Boolean = False

    Public Sub New(context As DataAccess)
        Me.context = context
    End Sub

    Public Function GetCompanies() As IEnumerable(Of Company) Implements ICompanyRepository.GetCompanies
        Return context.Companies.ToList()
    End Function

    Public Function GetCompanyByID(companyId As Integer) As Company Implements ICompanyRepository.GetCompanyById
        Return context.Companies.Find(companyId)
    End Function

    Public Sub InsertCompany(company As Company) Implements ICompanyRepository.InsertCompany
        context.Companies.Add(company)
    End Sub
    Public Sub DeleteCompany(companyID As Integer) Implements ICompanyRepository.DeleteCompany
        Dim company As Company = context.Companies.Find(companyID)
        context.Companies.Remove(company)
    End Sub

    Public Sub UpdateCompany(company As Company) Implements ICompanyRepository.UpdateCompany
        context.Entry(company).State = EntityState.Modified
    End Sub

    Public Sub Save() Implements ICompanyRepository.Save
        context.SaveChanges()
    End Sub
    Protected Overridable Sub Dispose(disposing As Boolean)
        If Not Me.disposed Then
            If disposing Then
                context.Dispose()
            End If
        End If
        Me.disposed = True
    End Sub

    Public Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

End Class
