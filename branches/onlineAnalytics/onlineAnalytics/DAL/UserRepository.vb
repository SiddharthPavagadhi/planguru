﻿Imports System.Collections.Generic
Imports System.Linq
Imports System.Data
Public Class UserRepository
    Inherits GenericRepository(Of User)
    Implements IUserRepository
    Implements IDisposable
    'Private context As DataAccess
    Private disposed As Boolean = False

    Public Sub New(context As DataAccess)
        'Me.context = context
        MyBase.New(context)
    End Sub

    Public Function ValidateUserAccount(UserName As String, Password As String) As User Implements IUserRepository.ValidateUserAccount

        Return (From user In context.Users.Where(Function(u) u.UserName.Equals(UserName) And u.Password.Equals(Password)) Select user).SingleOrDefault()

    End Function

    'This GetCountryCodes Function will return list of Country code with Name.
    Public Function GetUsers() As IEnumerable(Of User) Implements IUserRepository.GetUsers
        Return context.Users.ToList()
    End Function
    Public Function GetUserByID(userId As String) As User Implements IUserRepository.GetUserById
        Return context.Users.Find(userId)
    End Function
    Public Sub UpdateUser(user As User) Implements IUserRepository.UpdateUser
        context.Entry(user).State = EntityState.Modified
    End Sub
    Public Sub DeleteUser(userId As String) Implements IUserRepository.DeleteUser
        Dim user As User = context.Users.Find(userId)
        context.Users.Remove(user)
    End Sub

    Public Sub Save() Implements IUserRepository.Save
        context.SaveChanges()
    End Sub
    Protected Overridable Sub Dispose(disposing As Boolean)
        If Not Me.disposed Then
            If disposing Then
                context.Dispose()
            End If
        End If
        Me.disposed = True
    End Sub
    
    Public Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

End Class
