﻿Imports System.Collections.Generic
Imports System.Linq
Imports System.Data

Public Class AnalysisRepository
    Inherits GenericRepository(Of Analysis)
    Implements IAnalysisRepository
    Implements IDisposable
    'Private context As DataAccess
    Private disposed As Boolean = False

    Public Sub New(context As DataAccess)
        'Me.context = context
        MyBase.New(context)
    End Sub

    Public Function GetAnalyses() As IEnumerable(Of Analysis) Implements IAnalysisRepository.GetAnalyses
        Return context.Analyses.ToList()
    End Function

    Public Function GetAnalysisById(companyId As Integer) As Analysis Implements IAnalysisRepository.GetAnalysisById
        Return context.Analyses.Find(companyId)
    End Function

    Public Function ValidateAnalysis(Analysis As String, CompanyId As Integer) As IQueryable(Of Analysis) Implements IAnalysisRepository.ValidateAnalysis

        Return context.Analyses.Where(Function(an) an.AnalysisName.Equals(Analysis) And an.CompanyId.Equals(CompanyId))

    End Function

    Public Sub InsertAnalysis(analysis As Analysis) Implements IAnalysisRepository.InsertAnalysis
        context.Analyses.Add(analysis)
    End Sub
    Public Sub DeleteAnalysis(analysisId As Integer) Implements IAnalysisRepository.DeleteAnalysis
        Dim analysis As Analysis = context.Analyses.Find(analysisId)
        context.Analyses.Remove(analysis)
    End Sub

    Public Sub UpdateAnalysis(company As Analysis) Implements IAnalysisRepository.UpdateAnalysis
        context.Entry(company).State = EntityState.Modified
    End Sub

    Public Sub Save() Implements IAnalysisRepository.Save
        context.SaveChanges()
    End Sub

    Protected Overridable Sub Dispose(disposing As Boolean)
        If Not Me.disposed Then
            If disposing Then
                context.Dispose()
            End If
        End If
        Me.disposed = True
    End Sub

    Public Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
End Class
