﻿Imports Recurly

Public Class Utility
    Implements IDisposable
    Private context As New DataAccess()
    Private plan As String = String.Empty
    Private _companyRepository As GenericRepository(Of Company)
    Private _companiesRepository As CompanyRepository
    Private _customerRepository As CustomerRepository
    Private _userRepository As IUserRepository
    Private _analysisRepository As AnalysisRepository
    Private _countryRepository As CountryRepository
    Private _StateRepository As StateRepository
    Private _userRoleRepository As UserRoleRepository
    Private _userRolePermissionRepository As UserRolePermissionRepository
    Private _userRolePermissionMappingRepository As IUserRolePermissionMappingRepository
    Private _userCompanyMappingRepository As UserCompanyMappingRepository
    Private _userAnalysisMappingRepository As UserAnalysisMappingRepository
    Private _emailInfoRepository As EmailRepository
    Public ReadOnly Property RecurlyPlan() As String
        Get
            Dim plan As RecurlyPlan = RecurlyPlanList.GetPlans().First()

            If Not String.IsNullOrEmpty(plan.PlanCode) Then
                Me.plan = plan.PlanCode
            End If            
            Return Me.plan
        End Get
    End Property

    'The CompanyRepository property will provide access to get Company information from Company Table.
    Public ReadOnly Property CompanyRepository() As GenericRepository(Of Company)
        Get

            If Me._companyRepository Is Nothing Then
                Me._companyRepository = New GenericRepository(Of Company)(context)
            End If
            Return _companyRepository

        End Get
    End Property

    Public ReadOnly Property CompaniesRepository() As CompanyRepository
        Get

            If Me._companiesRepository Is Nothing Then
                Me._companiesRepository = New CompanyRepository(context)
            End If
            Return _companiesRepository

        End Get
    End Property

    'The CustomerRepository property will provide access to get Customer information from Customer Table.
    Public ReadOnly Property CustomerRepository() As CustomerRepository
        Get

            If Me._customerRepository Is Nothing Then
                Me._customerRepository = New CustomerRepository(context)
            End If
            Return _customerRepository

        End Get
    End Property
    'The UserRepository property will provide access to get User information from User Table.
    Public ReadOnly Property UserRepository() As UserRepository
        Get

            If Me._userRepository Is Nothing Then
                Me._userRepository = New UserRepository(context)
            End If
            Return _userRepository
        End Get
    End Property
    'The AnalysisRepository Property will provide access to get Analysis information from Analysis Table.
    Public ReadOnly Property AnalysisRepository() As AnalysisRepository
        Get

            If Me._analysisRepository Is Nothing Then
                Me._analysisRepository = New AnalysisRepository(context)
            End If
            Return _analysisRepository
        End Get
    End Property
    'The AnalysisRepository Property will provide access to get Analysis information from Analysis Table.
    Public ReadOnly Property EmailRepository() As EmailRepository
        Get

            If Me._emailInfoRepository Is Nothing Then
                Me._emailInfoRepository = New EmailRepository(context)
            End If
            Return _emailInfoRepository
        End Get
    End Property
    'The CountryRepository Property will provide access to get Country information from CountryCode Table.
    Public ReadOnly Property CountryRepository() As CountryRepository
        Get

            If Me._countryRepository Is Nothing Then
                Me._countryRepository = New CountryRepository(context)
            End If
            Return _countryRepository
        End Get
    End Property
    'The StateRepository Property will provide access to get State information from StateCode Table.
    Public ReadOnly Property StateRepository() As StateRepository
        Get

            If Me._StateRepository Is Nothing Then
                Me._StateRepository = New StateRepository(context)
            End If
            Return _StateRepository
        End Get
    End Property
    'The UserRoleRepository Property will provide access to get User Role information from UserRole Table.
    Public ReadOnly Property UserRoleRepository() As UserRoleRepository
        Get

            If Me._userRoleRepository Is Nothing Then
                Me._userRoleRepository = New UserRoleRepository(context)
            End If
            Return _userRoleRepository
        End Get
    End Property

    Public ReadOnly Property UserRolePermissionRepository() As UserRolePermissionRepository
        Get

            If Me._userRolePermissionRepository Is Nothing Then
                Me._userRolePermissionRepository = New UserRolePermissionRepository(context)
            End If
            Return _userRolePermissionRepository
        End Get
    End Property

    Public ReadOnly Property UserRolePermissionMappingRepository() As UserRolePermissionMappingRepository
        Get

            If Me._userRolePermissionMappingRepository Is Nothing Then
                Me._userRolePermissionMappingRepository = New UserRolePermissionMappingRepository(context)
            End If
            Return _userRolePermissionMappingRepository
        End Get
    End Property

    Public ReadOnly Property UserCompanyMappingRepository() As UserCompanyMappingRepository
        Get

            If Me._userCompanyMappingRepository Is Nothing Then
                Me._userCompanyMappingRepository = New UserCompanyMappingRepository(context)
            End If
            Return _userCompanyMappingRepository
        End Get
    End Property

    Public ReadOnly Property UserAnalysisMappingRepository() As UserAnalysisMappingRepository
        Get

            If Me._userAnalysisMappingRepository Is Nothing Then
                Me._userAnalysisMappingRepository = New UserAnalysisMappingRepository(context)
            End If
            Return _userAnalysisMappingRepository
        End Get
    End Property

    'This GetMonth function will return the list of Month as List items for dropdown with id and Text.
    Public Shared Function GetMonth(Optional SelectedMonth As Object = Nothing) As SelectList
        Dim Periods As List(Of SelectListItem) = New List(Of SelectListItem)

        Periods.Add(New SelectListItem With {.Text = MonthName(1), .Value = 1, .Selected = False})
        Periods.Add(New SelectListItem With {.Text = MonthName(2), .Value = 2, .Selected = False})
        Periods.Add(New SelectListItem With {.Text = MonthName(3), .Value = 3, .Selected = False})
        Periods.Add(New SelectListItem With {.Text = MonthName(4), .Value = 4, .Selected = False})
        Periods.Add(New SelectListItem With {.Text = MonthName(5), .Value = 5, .Selected = False})
        Periods.Add(New SelectListItem With {.Text = MonthName(6), .Value = 6, .Selected = False})
        Periods.Add(New SelectListItem With {.Text = MonthName(7), .Value = 7, .Selected = False})
        Periods.Add(New SelectListItem With {.Text = MonthName(8), .Value = 8, .Selected = False})
        Periods.Add(New SelectListItem With {.Text = MonthName(9), .Value = 9, .Selected = False})
        Periods.Add(New SelectListItem With {.Text = MonthName(10), .Value = 10, .Selected = False})
        Periods.Add(New SelectListItem With {.Text = MonthName(11), .Value = 11, .Selected = False})
        Periods.Add(New SelectListItem With {.Text = MonthName(12), .Value = 12, .Selected = False})

        Dim months As SelectList = New SelectList(Periods, "Value", "Text", SelectedMonth)

        Return months
    End Function

    Public Sub Save()
        context.SaveChanges()
    End Sub
#Region "IDisposable Support"
    Private disposed As Boolean ' To detect redundant calls

    ' IDisposable
    Protected Overridable Sub Dispose(disposing As Boolean)
        If Not Me.disposed Then
            If disposing Then
                context.Dispose()
            End If
        End If
        Me.disposed = True
    End Sub

    ' This code added by Visual Basic to correctly implement the disposable pattern.
    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
#End Region

End Class
