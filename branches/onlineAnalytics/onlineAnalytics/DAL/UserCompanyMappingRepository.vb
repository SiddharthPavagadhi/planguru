﻿Imports System.Collections.Generic
Imports System.Linq
Imports System.Data

Public Class UserCompanyMappingRepository

    Inherits GenericRepository(Of UserCompanyMapping)
    Implements IUserCompanyMappingRepository
    Implements IDisposable
    'Private context As DataAccess
    Private disposed As Boolean = False

    Public Sub New(context As DataAccess)
        'Me.context = context
        MyBase.New(context)
    End Sub

    Public Function GetUserCompanyMappingByCompanyId(CompanyId As Integer) As IEnumerable(Of UserCompanyMapping) Implements IUserCompanyMappingRepository.GetUserCompanyMappingByCompanyId
        Return context.UserCompanyMappings.Where(Function(uc) uc.CompanyId.Equals(CompanyId))
    End Function
    'This GetCountryCodes Function will return list of Country code with Name.
    Public Function GetUserCompanyMapping() As IEnumerable(Of UserCompanyMapping) Implements IUserCompanyMappingRepository.GetUserCompanyMapping
        Return context.UserCompanyMappings.ToList()
    End Function
    Public Function GetUserCompanyMappingById(UserCompanyMappingId As Integer) As UserCompanyMapping Implements IUserCompanyMappingRepository.GetUserCompanyMappingById
        Return context.UserCompanyMappings.Find(UserCompanyMappingId)
    End Function
    Public Sub UpdateUserCompanyMapping(UserCompanyDetails As UserCompanyMapping) Implements IUserCompanyMappingRepository.UpdateUserCompanyMapping
        context.Entry(UserCompanyDetails).State = EntityState.Modified
    End Sub

    Public Sub DeleteUserCompanyMapping(UserCompanyMappingId As Integer) Implements IUserCompanyMappingRepository.DeleteUserCompanyMapping
        Dim usercomp_map As UserCompanyMapping = context.UserCompanyMappings.Find(UserCompanyMappingId)
        context.UserCompanyMappings.Remove(usercomp_map)
    End Sub
    Public Function DeleteUserCompanyMappingByUserIdAndCompanyId(CompanyId As Integer, UserId As String) As Integer Implements IUserCompanyMappingRepository.DeleteUserCompanyMappingByUserIdAndCompanyId

        Dim deleteQuery As String = String.Format("Delete from UserCompanyMapping where CompanyId = {0} And UserId in ({1})", CompanyId, UserId)
        Return context.Database.ExecuteSqlCommand(deleteQuery)

    End Function
    Public Sub Save() Implements IUserCompanyMappingRepository.Save
        context.SaveChanges()
    End Sub
    Protected Overridable Sub Dispose(disposing As Boolean)
        If Not Me.disposed Then
            If disposing Then
                context.Dispose()
            End If
        End If
        Me.disposed = True
    End Sub

    Public Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

End Class
