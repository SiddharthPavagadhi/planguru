﻿Imports System.Security
Imports System.Security.Cryptography
Imports System.IO
Imports System.Runtime.InteropServices
Imports System.Text.RegularExpressions
Imports System.Text
Imports System.Net
Imports System.Net.Mail

Public Class Common

    Public Shared Function Encrypt(ByVal plainText As String) As String

        Dim passPhrase As String = "yourPassPhrase"
        Dim saltValue As String = "mySaltValue"
        Dim hashAlgorithm As String = "SHA1"

        Dim passwordIterations As Integer = 2
        Dim initVector As String = "@1B2c3D4e5F6g7H8"
        Dim keySize As Integer = 256

        Dim initVectorBytes As Byte() = Encoding.ASCII.GetBytes(initVector)
        Dim saltValueBytes As Byte() = Encoding.ASCII.GetBytes(saltValue)

        Dim plainTextBytes As Byte() = Encoding.UTF8.GetBytes(plainText)


        Dim password As New PasswordDeriveBytes(passPhrase, saltValueBytes, hashAlgorithm, passwordIterations)

        Dim keyBytes As Byte() = password.GetBytes(keySize / 8)
        Dim symmetricKey As New RijndaelManaged()

        symmetricKey.Mode = CipherMode.CBC

        Dim encryptor As ICryptoTransform = symmetricKey.CreateEncryptor(keyBytes, initVectorBytes)

        Dim memoryStream As New MemoryStream()
        Dim cryptoStream As New CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write)

        cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length)
        cryptoStream.FlushFinalBlock()
        Dim cipherTextBytes As Byte() = memoryStream.ToArray()
        memoryStream.Close()
        cryptoStream.Close()
        Dim cipherText As String = Convert.ToBase64String(cipherTextBytes)
        Return cipherText
    End Function

    Public Shared Function Decrypt(ByVal cipherText As String) As String
        Dim passPhrase As String = "yourPassPhrase"
        Dim saltValue As String = "mySaltValue"
        Dim hashAlgorithm As String = "SHA1"

        Dim passwordIterations As Integer = 2
        Dim initVector As String = "@1B2c3D4e5F6g7H8"
        Dim keySize As Integer = 256
        ' Convert strings defining encryption key characteristics into byte
        ' arrays. Let us assume that strings only contain ASCII codes.
        ' If strings include Unicode characters, use Unicode, UTF7, or UTF8
        ' encoding.
        Dim initVectorBytes As Byte() = Encoding.ASCII.GetBytes(initVector)
        Dim saltValueBytes As Byte() = Encoding.ASCII.GetBytes(saltValue)

        ' Convert our ciphertext into a byte array.
        Dim cipherTextBytes As Byte() = Convert.FromBase64String(cipherText)

        ' First, we must create a password, from which the key will be 
        ' derived. This password will be generated from the specified 
        ' passphrase and salt value. The password will be created using
        ' the specified hash algorithm. Password creation can be done in
        ' several iterations.
        Dim password As New PasswordDeriveBytes(passPhrase, saltValueBytes, hashAlgorithm, passwordIterations)

        ' Use the password to generate pseudo-random bytes for the encryption
        ' key. Specify the size of the key in bytes (instead of bits).
        Dim keyBytes As Byte() = password.GetBytes(keySize / 8)

        ' Create uninitialized Rijndael encryption object.
        Dim symmetricKey As New RijndaelManaged()

        ' It is reasonable to set encryption mode to Cipher Block Chaining
        ' (CBC). Use default options for other symmetric key parameters.
        symmetricKey.Mode = CipherMode.CBC

        ' Generate decryptor from the existing key bytes and initialization 
        ' vector. Key size will be defined based on the number of the key 
        ' bytes.
        Dim decryptor As ICryptoTransform = symmetricKey.CreateDecryptor(keyBytes, initVectorBytes)

        ' Define memory stream which will be used to hold encrypted data.
        Dim memoryStream As New MemoryStream(cipherTextBytes)

        ' Define cryptographic stream (always use Read mode for encryption).
        Dim cryptoStream As New CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read)

        ' Since at this point we don't know what the size of decrypted data
        ' will be, allocate the buffer long enough to hold ciphertext;
        ' plaintext is never longer than ciphertext.
        Dim plainTextBytes As Byte() = New Byte(cipherTextBytes.Length - 1) {}

        ' Start decrypting.
        Dim decryptedByteCount As Integer = cryptoStream.Read(plainTextBytes, 0, plainTextBytes.Length)

        ' Close both streams.
        memoryStream.Close()
        cryptoStream.Close()

        ' Convert decrypted data into a string. 
        ' Let us assume that the original plaintext string was UTF8-encoded.
        Dim plainText As String = Encoding.UTF8.GetString(plainTextBytes, 0, decryptedByteCount)

        ' Return decrypted string.   
        Return plainText
    End Function

    Public Shared Function GetEmailBodyfromFile(sourceFile As String) As String
        Dim BodyText As String = String.Empty

        Dim FILE_NAME As String = AppDomain.CurrentDomain.BaseDirectory() & System.Configuration.ConfigurationManager.AppSettings("EmailTeplateFolder") & "\" & sourceFile & ".html"
        If System.IO.File.Exists(FILE_NAME) Then
            Dim objReader As New System.IO.StreamReader(FILE_NAME)
            Do While objReader.Peek() <> -1
                BodyText = BodyText & objReader.ReadLine() & vbNewLine
            Loop
        End If
        Return BodyText
    End Function

    Public Sub SendUserEmail(User As User, emailinfo As EmailInfo)
        Logger.Log.Info(String.Format("SendEmail started "))
        Dim smtp As String = System.Configuration.ConfigurationManager.AppSettings("Email_SMTP")
        Dim port As Integer = System.Configuration.ConfigurationManager.AppSettings("Email_PORT")
        Dim EMAILFROM As String = System.Configuration.ConfigurationManager.AppSettings("Email_FROM")
        Dim EMAILPASS As String = System.Configuration.ConfigurationManager.AppSettings("Email_PASS")
        Dim Email_EnableSsl As Boolean = System.Configuration.ConfigurationManager.AppSettings("Email_EnableSsl")

        Dim myEnum As StatusE
        User.Password = Common.Decrypt(User.Password)
        myEnum = CType(User.Status, StatusE)
        Dim mailMsg As MailMessage = New MailMessage()
        mailMsg.To.Add(New MailAddress(User.UserEmail))
        mailMsg.From = New MailAddress(EMAILFROM)
        mailMsg.Subject = emailinfo.EmailSubject
        mailMsg.IsBodyHtml = True
        mailMsg.Body = emailinfo.EmailBody
        Dim emailClient As SmtpClient = New SmtpClient()
        emailClient.Port = port
        emailClient.EnableSsl = Email_EnableSsl
        emailClient.Host = smtp
        emailClient.Timeout = 3600000
        emailClient.Credentials = New System.Net.NetworkCredential(EMAILFROM, EMAILPASS)
        emailClient.Send(mailMsg)
        Logger.Log.Info(String.Format("SendEmail   Ended "))
    End Sub

    Public Sub SendCustomerEmail(customer As Customer, emailinfo As EmailInfo)
        Logger.Log.Info(String.Format("SendEmail started "))
        Dim smtp As String = System.Configuration.ConfigurationManager.AppSettings("Email_SMTP")
        Dim port As Integer = System.Configuration.ConfigurationManager.AppSettings("Email_PORT")
        Dim EMAILFROM As String = System.Configuration.ConfigurationManager.AppSettings("Email_FROM")
        Dim EMAILPASS As String = System.Configuration.ConfigurationManager.AppSettings("Email_PASS")
        Dim Email_EnableSsl As Boolean = System.Configuration.ConfigurationManager.AppSettings("Email_EnableSsl")

        'Dim myEnum As StatusE
        'User.Password = Common.Decrypt(User.Password)
        'myEnum = CType(User.Status, StatusE)
        Dim mailMsg As MailMessage = New MailMessage()
        mailMsg.To.Add(New MailAddress(customer.CustomerEmail))
        mailMsg.From = New MailAddress(EMAILFROM)
        mailMsg.Subject = emailinfo.EmailSubject
        mailMsg.IsBodyHtml = True
        mailMsg.Body = emailinfo.EmailBody
        Dim emailClient As SmtpClient = New SmtpClient()
        emailClient.Port = port
        emailClient.EnableSsl = Email_EnableSsl
        emailClient.Host = smtp
        emailClient.Timeout = 3600000
        emailClient.Credentials = New System.Net.NetworkCredential(EMAILFROM, EMAILPASS)
        emailClient.Send(mailMsg)
        Logger.Log.Info(String.Format("SendEmail   Ended "))
    End Sub
    Public Shared Function Generate(minLength As Integer, _
                                    maxLength As Integer) _
        As String
        ' Define supported password characters divided into groups.
        ' You can add (or remove) characters to (from) these groups.
        Dim PASSWORD_CHARS_LCASE As String = "abcdefgijkmnopqrstwxyz"
        Dim PASSWORD_CHARS_UCASE As String = "ABCDEFGHJKLMNPQRSTWXYZ"
        Dim PASSWORD_CHARS_NUMERIC As String = "23456789"
        Dim PASSWORD_CHARS_SPECIAL As String = "*$-+?_&=!%{}/"
        ' Make sure that input parameters are valid.
        If (minLength <= 0 Or maxLength <= 0 Or minLength > maxLength) Then
            Generate = Nothing
        End If

        ' Create a local array containing supported password characters
        ' grouped by types. You can remove character groups from this
        ' array, but doing so will weaken the password strength.
        Dim charGroups As Char()() = New Char()() _
        { _
            PASSWORD_CHARS_LCASE.ToCharArray(), _
            PASSWORD_CHARS_UCASE.ToCharArray(), _
            PASSWORD_CHARS_NUMERIC.ToCharArray(), _
            PASSWORD_CHARS_SPECIAL.ToCharArray() _
        }

        ' Use this array to track the number of unused characters in each
        ' character group.
        Dim charsLeftInGroup As Integer() = New Integer(charGroups.Length - 1) {}

        ' Initially, all characters in each group are not used.
        Dim I As Integer
        For I = 0 To charsLeftInGroup.Length - 1
            charsLeftInGroup(I) = charGroups(I).Length
        Next

        ' Use this array to track (iterate through) unused character groups.
        Dim leftGroupsOrder As Integer() = New Integer(charGroups.Length - 1) {}

        ' Initially, all character groups are not used.
        For I = 0 To leftGroupsOrder.Length - 1
            leftGroupsOrder(I) = I
        Next

        ' Because we cannot use the default randomizer, which is based on the
        ' current time (it will produce the same "random" number within a
        ' second), we will use a random number generator to seed the
        ' randomizer.

        ' Use a 4-byte array to fill it with random bytes and convert it then
        ' to an integer value.
        Dim randomBytes As Byte() = New Byte(3) {}

        ' Generate 4 random bytes.
        Dim rng As RNGCryptoServiceProvider = New RNGCryptoServiceProvider()

        rng.GetBytes(randomBytes)

        ' Convert 4 bytes into a 32-bit integer value.
        Dim seed As Integer = ((randomBytes(0) And &H7F) << 24 Or _
                                randomBytes(1) << 16 Or _
                                randomBytes(2) << 8 Or _
                                randomBytes(3))

        ' Now, this is real randomization.
        Dim random As Random = New Random(seed)

        ' This array will hold password characters.
        Dim password As Char() = Nothing

        ' Allocate appropriate memory for the password.
        If (minLength < maxLength) Then
            password = New Char(random.Next(minLength - 1, maxLength)) {}
        Else
            password = New Char(minLength - 1) {}
        End If

        ' Index of the next character to be added to password.
        Dim nextCharIdx As Integer

        ' Index of the next character group to be processed.
        Dim nextGroupIdx As Integer

        ' Index which will be used to track not processed character groups.
        Dim nextLeftGroupsOrderIdx As Integer

        ' Index of the last non-processed character in a group.
        Dim lastCharIdx As Integer

        ' Index of the last non-processed group.
        Dim lastLeftGroupsOrderIdx As Integer = leftGroupsOrder.Length - 1

        ' Generate password characters one at a time.
        For I = 0 To password.Length - 1

            ' If only one character group remained unprocessed, process it;
            ' otherwise, pick a random character group from the unprocessed
            ' group list. To allow a special character to appear in the
            ' first position, increment the second parameter of the Next
            ' function call by one, i.e. lastLeftGroupsOrderIdx + 1.
            If (lastLeftGroupsOrderIdx = 0) Then
                nextLeftGroupsOrderIdx = 0
            Else
                nextLeftGroupsOrderIdx = random.Next(0, lastLeftGroupsOrderIdx)
            End If

            ' Get the actual index of the character group, from which we will
            ' pick the next character.
            nextGroupIdx = leftGroupsOrder(nextLeftGroupsOrderIdx)

            ' Get the index of the last unprocessed characters in this group.
            lastCharIdx = charsLeftInGroup(nextGroupIdx) - 1

            ' If only one unprocessed character is left, pick it; otherwise,
            ' get a random character from the unused character list.
            If (lastCharIdx = 0) Then
                nextCharIdx = 0
            Else
                nextCharIdx = random.Next(0, lastCharIdx + 1)
            End If

            ' Add this character to the password.
            password(I) = charGroups(nextGroupIdx)(nextCharIdx)

            ' If we processed the last character in this group, start over.
            If (lastCharIdx = 0) Then
                charsLeftInGroup(nextGroupIdx) = _
                                charGroups(nextGroupIdx).Length
                ' There are more unprocessed characters left.
            Else
                ' Swap processed character with the last unprocessed character
                ' so that we don't pick it until we process all characters in
                ' this group.
                If (lastCharIdx <> nextCharIdx) Then
                    Dim temp As Char = charGroups(nextGroupIdx)(lastCharIdx)
                    charGroups(nextGroupIdx)(lastCharIdx) = _
                                charGroups(nextGroupIdx)(nextCharIdx)
                    charGroups(nextGroupIdx)(nextCharIdx) = temp
                End If

                ' Decrement the number of unprocessed characters in
                ' this group.
                charsLeftInGroup(nextGroupIdx) = _
                           charsLeftInGroup(nextGroupIdx) - 1
            End If

            ' If we processed the last group, start all over.
            If (lastLeftGroupsOrderIdx = 0) Then
                lastLeftGroupsOrderIdx = leftGroupsOrder.Length - 1
                ' There are more unprocessed groups left.
            Else
                ' Swap processed group with the last unprocessed group
                ' so that we don't pick it until we process all groups.
                If (lastLeftGroupsOrderIdx <> nextLeftGroupsOrderIdx) Then
                    Dim temp As Integer = _
                                leftGroupsOrder(lastLeftGroupsOrderIdx)
                    leftGroupsOrder(lastLeftGroupsOrderIdx) = _
                                leftGroupsOrder(nextLeftGroupsOrderIdx)
                    leftGroupsOrder(nextLeftGroupsOrderIdx) = temp
                End If

                ' Decrement the number of unprocessed groups.
                lastLeftGroupsOrderIdx = lastLeftGroupsOrderIdx - 1
            End If
        Next

        ' Convert password characters into a string and return the result.
        Generate = New String(password)
    End Function

    Public Shared Function MergeUserEmailBody(User As User, emailinfo As EmailInfo, resetUrl As String) As String
        Dim mergeBody As String = String.Empty
        If emailinfo.Id = EmailType.NewUserAdded Then
            mergeBody = emailinfo.EmailBody.Replace("##USERID##", User.UserId.ToString())
            mergeBody = mergeBody.Replace("##EMAIL##", User.UserEmail)
            mergeBody = mergeBody.Replace("##USERFULLNAME##", User.FirstName & " " & User.LastName)
            mergeBody = mergeBody.Replace("##TEMPPASSWORD##", Decrypt(User.Password))
            mergeBody = mergeBody.Replace("##PASSWORDRESETLINK##", resetUrl & "User/Verification?id=" & User.UserId.ToString() & "&securitykey=" & User.SecurityKey)


        ElseIf emailinfo.Id = EmailType.PasswordReset Then
            mergeBody = emailinfo.EmailBody.Replace("##USERID##", User.UserId.ToString())
            mergeBody = mergeBody.Replace("##EMAIL##", User.UserEmail)
            mergeBody = mergeBody.Replace("##USERFULLNAME##", User.FirstName & " " & User.LastName)
            mergeBody = mergeBody.Replace("##TEMPPASSWORD##", Decrypt(User.Password))
            mergeBody = mergeBody.Replace("##PASSWORDRESETLINK##", resetUrl & "User/Verification?id=" & User.UserId.ToString() & "&securitykey=" & User.SecurityKey)
            mergeBody = mergeBody.Replace("##PLANGURUTELEPHONENUMBER##", System.Configuration.ConfigurationManager.AppSettings("PLANGURUSUPPORTNO"))

        ElseIf emailinfo.Id = EmailType.NewUserSignUp Then
            mergeBody = emailinfo.EmailBody.Replace("##USERID##", User.UserId.ToString())
            mergeBody = mergeBody.Replace("##EMAIL##", User.UserEmail)
            mergeBody = mergeBody.Replace("##USERFULLNAME##", User.FirstName & " " & User.LastName)
            mergeBody = mergeBody.Replace("##AddedDate##", DateTime.Now.ToString())
            mergeBody = mergeBody.Replace("##DayoftheMonth##", GetDayoftheMonth())
            mergeBody = mergeBody.Replace("##TEMPPASSWORD##", Decrypt(User.Password))
            mergeBody = mergeBody.Replace("##PASSWORDRESETLINK##", resetUrl & "User/Verification?id=" & User.UserId.ToString() & "&securitykey=" & User.SecurityKey)

        ElseIf emailinfo.Id = EmailType.UserDeleted Then
            mergeBody = emailinfo.EmailBody.Replace("##USERID##", User.UserId.ToString())
            mergeBody = mergeBody.Replace("##EMAIL##", User.UserEmail)
            mergeBody = mergeBody.Replace("##USERFULLNAME##", User.FirstName & " " & User.LastName)
            mergeBody = mergeBody.Replace("##TODAYDATE##", DateTime.Now.ToString())
        End If

        Return mergeBody
    End Function

    Public Shared Function MergeCustomerEmailBody(customer As Customer, emailinfo As EmailInfo) As String

        Dim mergeBody As String = String.Empty

        If emailinfo.Id = EmailType.NewSubscriptionSignup Then
            mergeBody = emailinfo.EmailBody.Replace("##CUSTOMERID##", customer.CustomerId.ToString())
            mergeBody = mergeBody.Replace("##AMOUNT##", customer.SubscriptionAmount)
            mergeBody = mergeBody.Replace("##NewCustomerName##", customer.CustomerFullName)
            mergeBody = mergeBody.Replace("##NewCustomerStreet##", customer.CustomerAddress1 & " " & customer.CustomerAddress2)
            mergeBody = mergeBody.Replace("##NewCustomerCityStateEtc##", customer.City & ", " & customer.State)
            mergeBody = mergeBody.Replace("##SUBSCRIPTIONSCHEDULE##", String.Concat(customer.SubscriptionStartAt, " - ", customer.SubscriptionEndAt))
            mergeBody = mergeBody.Replace("##NewCustomerEmail##", customer.CustomerEmail)
            mergeBody = mergeBody.Replace("##DayoftheMonth##", GetDayoftheMonth())

        ElseIf emailinfo.Id = EmailType.SubcriptionCancellation Then
            mergeBody = emailinfo.EmailBody.Replace("##CUSTOMERID##", customer.CustomerId.ToString())
            'mergeBody = mergeBody.Replace("##AMOUNT##", customer)
            mergeBody = mergeBody.Replace("##TelephoneNumber##", customer.ContactTelephone)
            'mergeBody = mergeBody.Replace("##PAYMENTCYCLE##", customer.PaymentCycle)
            mergeBody = mergeBody.Replace("##SUBSCRIPTIONSTARTDATE##", customer.SubscriptionStartAt)

        End If

        Return mergeBody
    End Function

    Private Shared Function GetDayoftheMonth() As String
        Dim dayofMonth As String = String.Empty
        Dim currentday As Integer = DateTime.Now.Day

        Dim ones = currentday Mod 10
        Dim tens = Math.Floor(currentday / 10.0F) Mod 10
        If tens = 1 Then
            dayofMonth = currentday & "th"
            Return dayofMonth
        End If
        Select Case currentday Mod 10
            Case 1
                dayofMonth = currentday & "st"
            Case 2
                dayofMonth = currentday & "nd"
            Case 3
                dayofMonth = currentday & "rd"
            Case Else
                dayofMonth = currentday & "th"
        End Select

        Return dayofMonth
    End Function

End Class
