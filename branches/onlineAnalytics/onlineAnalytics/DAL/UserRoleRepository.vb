﻿Imports System.Collections.Generic
Imports System.Linq
Imports System.Data
Public Class UserRoleRepository
    Inherits GenericRepository(Of UserRole)
    Implements IUserRoleRepository
    Implements IDisposable
    'Private context As DataAccess
    Private disposed As Boolean = False

    Public Sub New(context As DataAccess)
        'Me.context = context
        MyBase.New(context)
    End Sub
    'This GetCountryCodes Function will return list of Country code with Name.
    Public Function GetUserRoles() As IEnumerable(Of UserRole) Implements IUserRoleRepository.GetUserRoles
        Return context.UserRoles.ToList()
    End Function

    Protected Overridable Sub Dispose(disposing As Boolean)
        If Not Me.disposed Then
            If disposing Then
                context.Dispose()
            End If
        End If
        Me.disposed = True
    End Sub

    Public Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
End Class
