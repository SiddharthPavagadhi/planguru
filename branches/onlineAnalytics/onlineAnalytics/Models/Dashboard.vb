﻿Imports System.ComponentModel.DataAnnotations

Public Class Dashboard

    Public Property DashboardId As Integer
    <Required(ErrorMessage:="A description for this dashboard item is required")>
    <StringLength(50)>
    Public Property DashDescription As String
    <Required()>
    Public Property ChartFormat As Integer
    <Required()>
    Public Property ChartType As Integer
    Public Property ShowAsPercent As Boolean
    Public Property ShowTrendline As Boolean
    Public Property ShowGoal As Boolean
    <RegularExpression("^-?[0-9]+(\.[0-9]{1,5})?$")>
    Public Property Period1Goal As Decimal
    <RegularExpression("^-?[0-9]+(\.[0-9]{1,5})?$")>
    Public Property GoalGrowthRate As Decimal
    Public Property Account As Account

End Class
