﻿Imports System.Collections.Generic
Imports System.Linq
Imports System.Web
Imports System.Data.Entity

Public Class SampleData
    'Inherits DropCreateDatabaseAlways(Of DataAccess)
    Inherits DropCreateDatabaseIfModelChanges(Of DataAccess)
    Protected Overrides Sub Seed(context As DataAccess)

        ''Dummy Data of Customer
        'Dim customers = New List(Of Customer)() From { _
        '     New Customer() With {.CustomerName = "Lynn Taylor", .ContactFirstName = "James", .ContactLastName = "Fernandez", .CustomerAddress1 = "3781 Stockert Hollow Road", .CustomerAddress2 = "Seattle, WA", .CustomerCountry = "United States", .CustomerState = "California", .CustomerCity = "Los Angeles", .CustomerPostalCode = "27834", .ContactTelephone = "425-939-1639", .CustomerEmail = "LynnWTaylor@teleworm.us", .CreatedBy = 1, .CreatedOn = DateTime.Now(), .UpdatedBy = 1, .UpdatedOn = DateTime.Now()}, _
        '     New Customer() With {.CustomerName = "Jacquelin Smith", .ContactFirstName = "Keith", .ContactLastName = "Moreno", .CustomerAddress1 = "162 Gandy Street", .CustomerAddress2 = "Syracuse, NY", .CustomerCountry = "United States", .CustomerState = "Florida", .CustomerCity = "Miami", .CustomerPostalCode = "32301", .ContactTelephone = "252-561-4801", .CustomerEmail = "KeithBMoreno@teleworm.us", .CreatedBy = 2, .CreatedOn = DateTime.Now(), .UpdatedBy = 2, .UpdatedOn = DateTime.Now()}, _
        '     New Customer() With {.CustomerName = "John Jones", .ContactFirstName = "Jacquelin", .ContactLastName = "Smith", .CustomerAddress1 = "4165 Karen Lane", .CustomerAddress2 = "Louisville, KY", .CustomerCountry = "United States", .CustomerState = "Illinois", .CustomerCity = "Chicago", .CustomerPostalCode = "61259", .ContactTelephone = "502-851-2124", .CustomerEmail = "JohnMJones@armyspy.com", .CreatedBy = 3, .CreatedOn = DateTime.Now(), .UpdatedBy = 3, .UpdatedOn = DateTime.Now()}, _
        '     New Customer() With {.CustomerName = "Patricia Richard", .ContactFirstName = "John", .ContactLastName = "Jones", .CustomerAddress1 = "4671 Shearwood Forest Drive", .CustomerAddress2 = "Manchester, NH", .CustomerCountry = "United States", .CustomerState = "Michigan", .CustomerCity = "Detroit", .CustomerPostalCode = "48201", .ContactTelephone = "603-287-0019", .CustomerEmail = "PatriciaMRichard@armyspy.com", .CreatedBy = 4, .CreatedOn = DateTime.Now(), .UpdatedBy = 4, .UpdatedOn = DateTime.Now()}, _
        '     New Customer() With {.CustomerName = "James Fernandez", .ContactFirstName = "Patricia", .ContactLastName = "Richard", .CustomerAddress1 = "2727 Concord Street", .CustomerAddress2 = "Charlotte, NC", .CustomerCountry = "United States", .CustomerState = "Nevada", .CustomerCity = "Las Vegas", .CustomerPostalCode = "89044", .ContactTelephone = "704-519-6104", .CustomerEmail = "JamesRFernandez@dayrep.com", .CreatedBy = 5, .CreatedOn = DateTime.Now(), .UpdatedBy = 5, .UpdatedOn = DateTime.Now()}
        '}
        'customers.ForEach(Function(u) context.Customers.Add(u))

        'Master records of UserRole
        Dim userRoles = New List(Of UserRole)() From { _
             New UserRole() With {.RoleName = "PGAU"}, _
             New UserRole() With {.RoleName = "PGSU"}, _
             New UserRole() With {.RoleName = "SAU"}, _
             New UserRole() With {.RoleName = "CAU"}, _
             New UserRole() With {.RoleName = "CRU"} _
        }
        userRoles.ForEach(Function(u) context.UserRoles.Add(u))

        Dim status = New List(Of Status)() From { _
             New Status() With {.StatusName = "Pending"}, _
             New Status() With {.StatusName = "Active"}, _
             New Status() With {.StatusName = "Suspended"}, _
             New Status() With {.StatusName = "Deactivated"} _
        }
        status.ForEach(Function(u) context.Status.Add(u))


        ' Dim customer = New List(Of Customer)() From { _
        '     New Customer() With {.ContactFirstName = "Contact First Name", .ContactLastName = "Contact Last Name", _
        '                          .ContactTelephone = "12323423", .CustomerAddress1 = " Customer ADD 1", .CustomerAddress2 = "Customer ADD 2", _
        '                          .CreatedBy = 1, .CreatedOn = Date.Now, .UpdatedBy = 1, .UpdatedOn = Date.Now} _
        '}
        ' customer.ForEach(Function(u) context.Customers.Add(u))

        'Email Info table sample data

        Dim emailBodySubSignup As String = Common.GetEmailBodyfromFile("NewsubscriptionSignup")
        Dim emailBodyNewUserAdded As String = Common.GetEmailBodyfromFile("NewUserAdded")
        Dim emailBodyNewUserSignUp As String = Common.GetEmailBodyfromFile("NewUserSignUp")
        Dim emailBodyPasswordReset As String = Common.GetEmailBodyfromFile("PasswordReset")
        Dim emailBodySubcriptionCancellation As String = Common.GetEmailBodyfromFile("SubcriptionCancellation")
        Dim emailBodyUserDeleted As String = Common.GetEmailBodyfromFile("UserDeleted")

        Dim ei = New List(Of EmailInfo)() From { _
            New EmailInfo() With {.Id = CType(EmailType.NewSubscriptionSignup, Integer), .EmailType = EmailType.NewSubscriptionSignup.ToString(), .EmailSubject = "New Subscription Signup", .EmailBody = emailBodySubSignup, .CreatedDateTime = DateTime.Now}, _
            New EmailInfo() With {.Id = CType(EmailType.NewUserAdded, Integer), .EmailType = EmailType.NewUserAdded.ToString(), .EmailSubject = "New User Added", .EmailBody = emailBodyNewUserAdded, .CreatedDateTime = DateTime.Now}, _
            New EmailInfo() With {.Id = CType(EmailType.NewUserSignUp, Integer), .EmailType = EmailType.NewUserSignUp.ToString(), .EmailSubject = "New User Sign Up", .EmailBody = emailBodyPasswordReset, .CreatedDateTime = DateTime.Now}, _
            New EmailInfo() With {.Id = CType(EmailType.PasswordReset, Integer), .EmailType = EmailType.PasswordReset.ToString(), .EmailSubject = "Password Reset", .EmailBody = emailBodySubSignup, .CreatedDateTime = DateTime.Now}, _
            New EmailInfo() With {.Id = CType(EmailType.SubcriptionCancellation, Integer), .EmailType = EmailType.SubcriptionCancellation.ToString(), .EmailSubject = "Subcription Cancellation", .EmailBody = emailBodySubcriptionCancellation, .CreatedDateTime = DateTime.Now}, _
            New EmailInfo() With {.Id = CType(EmailType.UserDeleted, Integer), .EmailType = EmailType.UserDeleted.ToString(), .EmailSubject = "User Deleted", .EmailBody = emailBodyUserDeleted, .CreatedDateTime = DateTime.Now} _
       }
        ei.ForEach(Function(u) context.EmailInfos.Add(u))


        'Master records of UserRolePermission
        Dim UserRolePermissions = New List(Of UserRolePermission)() From { _
             New UserRolePermission() With {.Description = "Subscription Management"}, _
             New UserRolePermission() With {.Description = "User Management"}, _
             New UserRolePermission() With {.Description = "Search Customer"}, _
             New UserRolePermission() With {.Description = "View Subscription"}, _
             New UserRolePermission() With {.Description = "Add Subscription"}, _
             New UserRolePermission() With {.Description = "Update Subscription"}, _
             New UserRolePermission() With {.Description = "Cancel Subscription"}, _
             New UserRolePermission() With {.Description = "Add User"}, _
             New UserRolePermission() With {.Description = "Delete User"}, _
             New UserRolePermission() With {.Description = "View User"}, _
             New UserRolePermission() With {.Description = "Update User"}, _
             New UserRolePermission() With {.Description = "Add Company"}, _
             New UserRolePermission() With {.Description = "Delete Company"}, _
             New UserRolePermission() With {.Description = "View Companies"}, _
             New UserRolePermission() With {.Description = "Update Company"}, _
             New UserRolePermission() With {.Description = "User Company Mapping"}, _
             New UserRolePermission() With {.Description = "Add Analysis"}, _
             New UserRolePermission() With {.Description = "Delete Analysis"}, _
             New UserRolePermission() With {.Description = "View Analyses"}, _
             New UserRolePermission() With {.Description = "Update Analysis"}, _
             New UserRolePermission() With {.Description = "User Analysis Mapping"}, _
             New UserRolePermission() With {.Description = "View Analysis Info"}, _
             New UserRolePermission() With {.Description = "Print Analysis Info"}, _
             New UserRolePermission() With {.Description = "Modify Dashboard"}
        }
        UserRolePermissions.ForEach(Function(u) context.UserRolePermissions.Add(u))

        'Dim UserRolePermissionsMapping = New List(Of UserRolePermissionMapping)() From { _
        '    New UserRolePermissionMapping() With {.Description = "Subscription Management"}
        '    }


        Dim acctType = New AcctType() With {.TypeDesc = "Assets", .ClassDesc = "Current assets", .SubclassDesc = "Current assets"}
        context.AcctTypes.Add(acctType)

        Dim acctList As New List(Of Account)() From { _
         New Account() With {.AcctDescriptor = "D", .SortSequence = 1, .Description = "Cash", .AcctType = acctType}, _
         New Account() With {.AcctDescriptor = "D", .SortSequence = 2, .Description = "Checking", .AcctType = acctType} _
        }
        acctList.ForEach(Function(u) context.Accounts.Add(u))

        For Each acctItem As Account In acctList
            Select Case acctItem.Description
                Case "Cash"
                    Dim balList As New List(Of Balance)() From { _
                        New Balance() With {.YearType = "H1", .Balance1 = 100, .Balance2 = 150, .Balance3 = 200, .Balance4 = 250, _
                    .Balance5 = 300, .Balance6 = 350, .Balance7 = 400, .Balance8 = 450, .Balance9 = 500, .Balance10 = 550, .Balance11 = 600, _
                    .Balance12 = 650, .Account = acctItem}, _
                        New Balance() With {.YearType = "CA", .Balance1 = 100, .Balance2 = 150, .Balance3 = 200, .Balance4 = 250, _
                    .Balance5 = 300, .Balance6 = 350, .Balance7 = 400, .Balance8 = 450, .Balance9 = 500, .Balance10 = 550, .Balance11 = 600, _
                    .Balance12 = 650, .Account = acctItem}
                    }
                    balList.ForEach(Function(u) context.Balances.Add(u))
                Case "Checking"
                    Dim balList As New List(Of Balance)() From { _
                        New Balance() With {.YearType = "H1", .Balance1 = 100, .Balance2 = 150, .Balance3 = 200, .Balance4 = 250, _
                    .Balance5 = 300, .Balance6 = 350, .Balance7 = 400, .Balance8 = 450, .Balance9 = 500, .Balance10 = 550, .Balance11 = 600, _
                    .Balance12 = 650, .Account = acctItem}
                    }
                    balList.ForEach(Function(u) context.Balances.Add(u))
            End Select

        Next

        acctType = New AcctType() With {.TypeDesc = "Revenue", .ClassDesc = "Revenue", .SubclassDesc = "Revenue"}
        context.AcctTypes.Add(acctType)

        acctList = New List(Of Account)() From { _
         New Account() With {.AcctDescriptor = "H", .SortSequence = 3, .Description = "Revenue", .AcctType = acctType}, _
         New Account() With {.AcctDescriptor = "D", .SortSequence = 4, .Description = "Total new sales", .AcctType = acctType}, _
         New Account() With {.AcctDescriptor = "D", .SortSequence = 5, .Description = "Total upgrade sales", .AcctType = acctType}, _
         New Account() With {.AcctDescriptor = "D", .SortSequence = 6, .Description = "Cloud sales", .AcctType = acctType}, _
         New Account() With {.AcctDescriptor = "D", .SortSequence = 7, .Description = "Consulting revenue", .AcctType = acctType}, _
         New Account() With {.AcctDescriptor = "D", .SortSequence = 8, .Description = "Shipping, returns & discounts", .AcctType = acctType}, _
         New Account() With {.AcctDescriptor = "T", .SortSequence = 9, .Description = "Total revenue", .AcctType = acctType} _
        }
        acctList.ForEach(Function(u) context.Accounts.Add(u))
        'Country Code table
        Dim countryCode As New List(Of CountryCode)() From { _
        New CountryCode() With {.Code = "AF", .CountryName = "Afghanistan"}, _
        New CountryCode() With {.Code = "AL", .CountryName = "Albania"}, _
        New CountryCode() With {.Code = "DZ", .CountryName = "Algeria"}, _
        New CountryCode() With {.Code = "AS", .CountryName = "American Samoa"}, _
        New CountryCode() With {.Code = "AD", .CountryName = "Andorra"}, _
        New CountryCode() With {.Code = "AO", .CountryName = "Angola"}, _
        New CountryCode() With {.Code = "AI", .CountryName = "Anguilla"}, _
        New CountryCode() With {.Code = "AQ", .CountryName = "Antarctica"}, _
        New CountryCode() With {.Code = "AG", .CountryName = "Antigua and Barbuda"}, _
        New CountryCode() With {.Code = "AR", .CountryName = "Argentina"}, _
        New CountryCode() With {.Code = "AM", .CountryName = "Armenia"}, _
        New CountryCode() With {.Code = "AW", .CountryName = "Aruba"}, _
        New CountryCode() With {.Code = "AU", .CountryName = "Australia"}, _
        New CountryCode() With {.Code = "AT", .CountryName = "Austria"}, _
        New CountryCode() With {.Code = "AZ", .CountryName = "Azerbaijan"}, _
        New CountryCode() With {.Code = "BS", .CountryName = "Bahamas"}, _
        New CountryCode() With {.Code = "BH", .CountryName = "Bahrain"}, _
        New CountryCode() With {.Code = "BD", .CountryName = "Bangladesh"}, _
        New CountryCode() With {.Code = "BB", .CountryName = "Barbados"}, _
        New CountryCode() With {.Code = "BY", .CountryName = "Belarus"}, _
        New CountryCode() With {.Code = "BE", .CountryName = "Belgium"}, _
        New CountryCode() With {.Code = "BZ", .CountryName = "Belize"}, _
        New CountryCode() With {.Code = "BJ", .CountryName = "Benin"}, _
        New CountryCode() With {.Code = "BM", .CountryName = "Bermuda"}, _
        New CountryCode() With {.Code = "BT", .CountryName = "Bhutan"}, _
        New CountryCode() With {.Code = "BO", .CountryName = "Bolivia"}, _
        New CountryCode() With {.Code = "BA", .CountryName = "Bosnia and Herzegovina"}, _
        New CountryCode() With {.Code = "BW", .CountryName = "Botswana"}, _
        New CountryCode() With {.Code = "BV", .CountryName = "Bouvet Island"}, _
        New CountryCode() With {.Code = "BR", .CountryName = "Brazil"}, _
        New CountryCode() With {.Code = "BQ", .CountryName = "British Antarctic Territory"}, _
        New CountryCode() With {.Code = "IO", .CountryName = "British Indian Ocean Territory"}, _
        New CountryCode() With {.Code = "VG", .CountryName = "British Virgin Islands"}, _
        New CountryCode() With {.Code = "BN", .CountryName = "Brunei"}, _
        New CountryCode() With {.Code = "BG", .CountryName = "Bulgaria"}, _
        New CountryCode() With {.Code = "BF", .CountryName = "Burkina Faso"}, _
        New CountryCode() With {.Code = "BI", .CountryName = "Burundi"}, _
        New CountryCode() With {.Code = "KH", .CountryName = "Cambodia"}, _
        New CountryCode() With {.Code = "CM", .CountryName = "Cameroon"}, _
        New CountryCode() With {.Code = "CA", .CountryName = "Canada"}, _
        New CountryCode() With {.Code = "CT", .CountryName = "Canton and Enderbury Islands"}, _
        New CountryCode() With {.Code = "CV", .CountryName = "Cape Verde"}, _
        New CountryCode() With {.Code = "KY", .CountryName = "Cayman Islands"}, _
        New CountryCode() With {.Code = "CF", .CountryName = "Central African Republic"}, _
        New CountryCode() With {.Code = "TD", .CountryName = "Chad"}, _
        New CountryCode() With {.Code = "CL", .CountryName = "Chile"}, _
        New CountryCode() With {.Code = "CN", .CountryName = "China"}, _
        New CountryCode() With {.Code = "CX", .CountryName = "Christmas Island"}, _
        New CountryCode() With {.Code = "CC", .CountryName = "Cocos [Keeling] Islands"}, _
        New CountryCode() With {.Code = "CO", .CountryName = "Colombia"}, _
        New CountryCode() With {.Code = "KM", .CountryName = "Comoros"}, _
        New CountryCode() With {.Code = "CG", .CountryName = "Congo - Brazzaville"}, _
        New CountryCode() With {.Code = "CD", .CountryName = "Congo - Kinshasa"}, _
        New CountryCode() With {.Code = "CK", .CountryName = "Cook Islands"}, _
        New CountryCode() With {.Code = "CR", .CountryName = "Costa Rica"}, _
        New CountryCode() With {.Code = "HR", .CountryName = "Croatia"}, _
        New CountryCode() With {.Code = "CU", .CountryName = "Cuba"}, _
        New CountryCode() With {.Code = "CY", .CountryName = "Cyprus"}, _
        New CountryCode() With {.Code = "CZ", .CountryName = "Czech Republic"}, _
        New CountryCode() With {.Code = "CI", .CountryName = "Côte d Ivoire"}, _
        New CountryCode() With {.Code = "DK", .CountryName = "Denmark"}, _
        New CountryCode() With {.Code = "DJ", .CountryName = "Djibouti"}, _
        New CountryCode() With {.Code = "DM", .CountryName = "Dominica"}, _
        New CountryCode() With {.Code = "DO", .CountryName = "Dominican Republic"}, _
        New CountryCode() With {.Code = "NQ", .CountryName = "Dronning Maud Land"}, _
        New CountryCode() With {.Code = "DD", .CountryName = "East Germany"}, _
        New CountryCode() With {.Code = "EC", .CountryName = "Ecuador"}, _
        New CountryCode() With {.Code = "EG", .CountryName = "Egypt"}, _
        New CountryCode() With {.Code = "SV", .CountryName = "El Salvador"}, _
        New CountryCode() With {.Code = "GQ", .CountryName = "Equatorial Guinea"}, _
        New CountryCode() With {.Code = "ER", .CountryName = "Eritrea"}, _
        New CountryCode() With {.Code = "EE", .CountryName = "Estonia"}, _
        New CountryCode() With {.Code = "ET", .CountryName = "Ethiopia"}, _
        New CountryCode() With {.Code = "FK", .CountryName = "Falkland Islands"}, _
        New CountryCode() With {.Code = "FO", .CountryName = "Faroe Islands"}, _
        New CountryCode() With {.Code = "FJ", .CountryName = "Fiji"}, _
        New CountryCode() With {.Code = "FI", .CountryName = "Finland"}, _
        New CountryCode() With {.Code = "FR", .CountryName = "France"}, _
        New CountryCode() With {.Code = "GF", .CountryName = "French Guiana"}, _
        New CountryCode() With {.Code = "PF", .CountryName = "French Polynesia"}, _
        New CountryCode() With {.Code = "TF", .CountryName = "French Southern Territories"}, _
        New CountryCode() With {.Code = "FQ", .CountryName = "French Southern and Antarctic Territories"}, _
        New CountryCode() With {.Code = "GA", .CountryName = "Gabon"}, _
        New CountryCode() With {.Code = "GM", .CountryName = "Gambia"}, _
        New CountryCode() With {.Code = "GE", .CountryName = "Georgia"}, _
        New CountryCode() With {.Code = "DE", .CountryName = "Germany"}, _
        New CountryCode() With {.Code = "GH", .CountryName = "Ghana"}, _
        New CountryCode() With {.Code = "GI", .CountryName = "Gibraltar"}, _
        New CountryCode() With {.Code = "GR", .CountryName = "Greece"}, _
        New CountryCode() With {.Code = "GL", .CountryName = "Greenland"}, _
        New CountryCode() With {.Code = "GD", .CountryName = "Grenada"}, _
        New CountryCode() With {.Code = "GP", .CountryName = "Guadeloupe"}, _
        New CountryCode() With {.Code = "GU", .CountryName = "Guam"}, _
        New CountryCode() With {.Code = "GT", .CountryName = "Guatemala"}, _
        New CountryCode() With {.Code = "GG", .CountryName = "Guernsey"}, _
        New CountryCode() With {.Code = "GN", .CountryName = "Guinea"}, _
        New CountryCode() With {.Code = "GW", .CountryName = "Guinea-Bissau"}, _
        New CountryCode() With {.Code = "GY", .CountryName = "Guyana"}, _
        New CountryCode() With {.Code = "HT", .CountryName = "Haiti"}, _
        New CountryCode() With {.Code = "HM", .CountryName = "Heard Island and McDonald Islands"}, _
        New CountryCode() With {.Code = "HN", .CountryName = "Honduras"}, _
        New CountryCode() With {.Code = "HK", .CountryName = "Hong Kong SAR China"}, _
        New CountryCode() With {.Code = "HU", .CountryName = "Hungary"}, _
        New CountryCode() With {.Code = "IS", .CountryName = "Iceland"}, _
        New CountryCode() With {.Code = "IN", .CountryName = "India"}, _
        New CountryCode() With {.Code = "ID", .CountryName = "Indonesia"}, _
        New CountryCode() With {.Code = "IR", .CountryName = "Iran"}, _
        New CountryCode() With {.Code = "IQ", .CountryName = "Iraq"}, _
        New CountryCode() With {.Code = "IE", .CountryName = "Ireland"}, _
        New CountryCode() With {.Code = "IM", .CountryName = "Isle of Man"}, _
        New CountryCode() With {.Code = "IL", .CountryName = "Israel"}, _
        New CountryCode() With {.Code = "IT", .CountryName = "Italy"}, _
        New CountryCode() With {.Code = "JM", .CountryName = "Jamaica"}, _
        New CountryCode() With {.Code = "JP", .CountryName = "Japan"}, _
        New CountryCode() With {.Code = "JE", .CountryName = "Jersey"}, _
        New CountryCode() With {.Code = "JT", .CountryName = "Johnston Island"}, _
        New CountryCode() With {.Code = "JO", .CountryName = "Jordan"}, _
        New CountryCode() With {.Code = "KZ", .CountryName = "Kazakhstan"}, _
        New CountryCode() With {.Code = "KE", .CountryName = "Kenya"}, _
        New CountryCode() With {.Code = "KI", .CountryName = "Kiribati"}, _
        New CountryCode() With {.Code = "KW", .CountryName = "Kuwait"}, _
        New CountryCode() With {.Code = "KG", .CountryName = "Kyrgyzstan"}, _
        New CountryCode() With {.Code = "LA", .CountryName = "Laos"}, _
        New CountryCode() With {.Code = "LV", .CountryName = "Latvia"}, _
        New CountryCode() With {.Code = "LB", .CountryName = "Lebanon"}, _
        New CountryCode() With {.Code = "LS", .CountryName = "Lesotho"}, _
        New CountryCode() With {.Code = "LR", .CountryName = "Liberia"}, _
        New CountryCode() With {.Code = "LY", .CountryName = "Libya"}, _
        New CountryCode() With {.Code = "LI", .CountryName = "Liechtenstein"}, _
        New CountryCode() With {.Code = "LT", .CountryName = "Lithuania"}, _
        New CountryCode() With {.Code = "LU", .CountryName = "Luxembourg"}, _
        New CountryCode() With {.Code = "MO", .CountryName = "Macau SAR China"}, _
        New CountryCode() With {.Code = "MK", .CountryName = "Macedonia"}, _
        New CountryCode() With {.Code = "MG", .CountryName = "Madagascar"}, _
        New CountryCode() With {.Code = "MW", .CountryName = "Malawi"}, _
        New CountryCode() With {.Code = "MY", .CountryName = "Malaysia"}, _
        New CountryCode() With {.Code = "MV", .CountryName = "Maldives"}, _
        New CountryCode() With {.Code = "ML", .CountryName = "Mali"}, _
        New CountryCode() With {.Code = "MT", .CountryName = "Malta"}, _
        New CountryCode() With {.Code = "MH", .CountryName = "Marshall Islands"}, _
        New CountryCode() With {.Code = "MQ", .CountryName = "Martinique"}, _
        New CountryCode() With {.Code = "MR", .CountryName = "Mauritania"}, _
        New CountryCode() With {.Code = "MU", .CountryName = "Mauritius"}, _
        New CountryCode() With {.Code = "YT", .CountryName = "Mayotte"}, _
        New CountryCode() With {.Code = "FX", .CountryName = "Metropolitan France"}, _
        New CountryCode() With {.Code = "MX", .CountryName = "Mexico"}, _
        New CountryCode() With {.Code = "FM", .CountryName = "Micronesia"}, _
        New CountryCode() With {.Code = "MI", .CountryName = "Midway Islands"}, _
        New CountryCode() With {.Code = "MD", .CountryName = "Moldova"}, _
        New CountryCode() With {.Code = "MC", .CountryName = "Monaco"}, _
        New CountryCode() With {.Code = "MN", .CountryName = "Mongolia"}, _
        New CountryCode() With {.Code = "ME", .CountryName = "Montenegro"}, _
        New CountryCode() With {.Code = "MS", .CountryName = "Montserrat"}, _
        New CountryCode() With {.Code = "MA", .CountryName = "Morocco"}, _
        New CountryCode() With {.Code = "MZ", .CountryName = "Mozambique"}, _
        New CountryCode() With {.Code = "MM", .CountryName = "Myanmar [Burma]"}, _
        New CountryCode() With {.Code = "NA", .CountryName = "Namibia"}, _
        New CountryCode() With {.Code = "NR", .CountryName = "Nauru"}, _
        New CountryCode() With {.Code = "NP", .CountryName = "Nepal"}, _
        New CountryCode() With {.Code = "NL", .CountryName = "Netherlands"}, _
        New CountryCode() With {.Code = "AN", .CountryName = "Netherlands Antilles"}, _
        New CountryCode() With {.Code = "NT", .CountryName = "Neutral Zone"}, _
        New CountryCode() With {.Code = "NC", .CountryName = "New Caledonia"}, _
        New CountryCode() With {.Code = "NZ", .CountryName = "New Zealand"}, _
        New CountryCode() With {.Code = "NI", .CountryName = "Nicaragua"}, _
        New CountryCode() With {.Code = "NE", .CountryName = "Niger"}, _
        New CountryCode() With {.Code = "NG", .CountryName = "Nigeria"}, _
        New CountryCode() With {.Code = "NU", .CountryName = "Niue"}, _
        New CountryCode() With {.Code = "NF", .CountryName = "Norfolk Island"}, _
        New CountryCode() With {.Code = "KP", .CountryName = "North Korea"}, _
        New CountryCode() With {.Code = "VD", .CountryName = "North Vietnam"}, _
        New CountryCode() With {.Code = "MP", .CountryName = "Northern Mariana Islands"}, _
        New CountryCode() With {.Code = "NO", .CountryName = "Norway"}, _
        New CountryCode() With {.Code = "OM", .CountryName = "Oman"}, _
        New CountryCode() With {.Code = "PC", .CountryName = "Pacific Islands Trust Territory"}, _
        New CountryCode() With {.Code = "PK", .CountryName = "Pakistan"}, _
        New CountryCode() With {.Code = "PW", .CountryName = "Palau"}, _
        New CountryCode() With {.Code = "PS", .CountryName = "Palestinian Territories"}, _
        New CountryCode() With {.Code = "PA", .CountryName = "Panama"}, _
        New CountryCode() With {.Code = "PZ", .CountryName = "Panama Canal Zone"}, _
        New CountryCode() With {.Code = "PG", .CountryName = "Papua New Guinea"}, _
        New CountryCode() With {.Code = "PY", .CountryName = "Paraguay"}, _
        New CountryCode() With {.Code = "YD", .CountryName = "People""s Democratic Republic of Yemen"}, _
        New CountryCode() With {.Code = "PE", .CountryName = "Peru"}, _
        New CountryCode() With {.Code = "PH", .CountryName = "Philippines"}, _
        New CountryCode() With {.Code = "PN", .CountryName = "Pitcairn Islands"}, _
        New CountryCode() With {.Code = "PL", .CountryName = "Poland"}, _
        New CountryCode() With {.Code = "PT", .CountryName = "Portugal"}, _
        New CountryCode() With {.Code = "PR", .CountryName = "Puerto Rico"}, _
        New CountryCode() With {.Code = "QA", .CountryName = "Qatar"}, _
        New CountryCode() With {.Code = "RO", .CountryName = "Romania"}, _
        New CountryCode() With {.Code = "RU", .CountryName = "Russia"}, _
        New CountryCode() With {.Code = "RW", .CountryName = "Rwanda"}, _
        New CountryCode() With {.Code = "RE", .CountryName = "Réunion"}, _
        New CountryCode() With {.Code = "BL", .CountryName = "Saint Barthélemy"}, _
        New CountryCode() With {.Code = "SH", .CountryName = "Saint Helena"}, _
        New CountryCode() With {.Code = "KN", .CountryName = "Saint Kitts and Nevis"}, _
        New CountryCode() With {.Code = "LC", .CountryName = "Saint Lucia"}, _
        New CountryCode() With {.Code = "MF", .CountryName = "Saint Martin"}, _
        New CountryCode() With {.Code = "PM", .CountryName = "Saint Pierre and Miquelon"}, _
        New CountryCode() With {.Code = "VC", .CountryName = "Saint Vincent and the Grenadines"}, _
        New CountryCode() With {.Code = "WS", .CountryName = "Samoa"}, _
        New CountryCode() With {.Code = "SM", .CountryName = "San Marino"}, _
        New CountryCode() With {.Code = "SA", .CountryName = "Saudi Arabia"}, _
        New CountryCode() With {.Code = "SN", .CountryName = "Senegal"}, _
        New CountryCode() With {.Code = "RS", .CountryName = "Serbia"}, _
        New CountryCode() With {.Code = "CS", .CountryName = "Serbia and Montenegro"}, _
        New CountryCode() With {.Code = "SC", .CountryName = "Seychelles"}, _
        New CountryCode() With {.Code = "SL", .CountryName = "Sierra Leone"}, _
        New CountryCode() With {.Code = "SG", .CountryName = "Singapore"}, _
        New CountryCode() With {.Code = "SK", .CountryName = "Slovakia"}, _
        New CountryCode() With {.Code = "SI", .CountryName = "Slovenia"}, _
        New CountryCode() With {.Code = "SB", .CountryName = "Solomon Islands"}, _
        New CountryCode() With {.Code = "SO", .CountryName = "Somalia"}, _
        New CountryCode() With {.Code = "ZA", .CountryName = "South Africa"}, _
        New CountryCode() With {.Code = "GS", .CountryName = "South Georgia and the South Sandwich Islands"}, _
        New CountryCode() With {.Code = "KR", .CountryName = "South Korea"}, _
        New CountryCode() With {.Code = "ES", .CountryName = "Spain"}, _
        New CountryCode() With {.Code = "LK", .CountryName = "Sri Lanka"}, _
        New CountryCode() With {.Code = "SD", .CountryName = "Sudan"}, _
        New CountryCode() With {.Code = "SR", .CountryName = "Suriname"}, _
        New CountryCode() With {.Code = "SJ", .CountryName = "Svalbard and Jan Mayen"}, _
        New CountryCode() With {.Code = "SZ", .CountryName = "Swaziland"}, _
        New CountryCode() With {.Code = "SE", .CountryName = "Sweden"}, _
        New CountryCode() With {.Code = "CH", .CountryName = "Switzerland"}, _
        New CountryCode() With {.Code = "SY", .CountryName = "Syria"}, _
        New CountryCode() With {.Code = "ST", .CountryName = "São Tomé and Príncipe"}, _
        New CountryCode() With {.Code = "TW", .CountryName = "Taiwan"}, _
        New CountryCode() With {.Code = "TJ", .CountryName = "Tajikistan"}, _
        New CountryCode() With {.Code = "TZ", .CountryName = "Tanzania"}, _
        New CountryCode() With {.Code = "TH", .CountryName = "Thailand"}, _
        New CountryCode() With {.Code = "TL", .CountryName = "Timor-Leste"}, _
        New CountryCode() With {.Code = "TG", .CountryName = "Togo"}, _
        New CountryCode() With {.Code = "TK", .CountryName = "Tokelau"}, _
        New CountryCode() With {.Code = "TO", .CountryName = "Tonga"}, _
        New CountryCode() With {.Code = "TT", .CountryName = "Trinidad and Tobago"}, _
        New CountryCode() With {.Code = "TN", .CountryName = "Tunisia"}, _
        New CountryCode() With {.Code = "TR", .CountryName = "Turkey"}, _
        New CountryCode() With {.Code = "TM", .CountryName = "Turkmenistan"}, _
        New CountryCode() With {.Code = "TC", .CountryName = "Turks and Caicos Islands"}, _
        New CountryCode() With {.Code = "TV", .CountryName = "Tuvalu"}, _
        New CountryCode() With {.Code = "UM", .CountryName = "U.S. Minor Outlying Islands"}, _
        New CountryCode() With {.Code = "PU", .CountryName = "U.S. Miscellaneous Pacific Islands"}, _
        New CountryCode() With {.Code = "VI", .CountryName = "U.S. Virgin Islands"}, _
        New CountryCode() With {.Code = "UG", .CountryName = "Uganda"}, _
        New CountryCode() With {.Code = "UA", .CountryName = "Ukraine"}, _
        New CountryCode() With {.Code = "SU", .CountryName = "Union of Soviet Socialist Republics"}, _
        New CountryCode() With {.Code = "AE", .CountryName = "United Arab Emirates"}, _
        New CountryCode() With {.Code = "GB", .CountryName = "United Kingdom"}, _
        New CountryCode() With {.Code = "US", .CountryName = "United States"}, _
        New CountryCode() With {.Code = "ZZ", .CountryName = "Unknown or Invalid Region"}, _
        New CountryCode() With {.Code = "UY", .CountryName = "Uruguay"}, _
        New CountryCode() With {.Code = "UZ", .CountryName = "Uzbekistan"}, _
        New CountryCode() With {.Code = "VU", .CountryName = "Vanuatu"}, _
        New CountryCode() With {.Code = "VA", .CountryName = "Vatican City"}, _
        New CountryCode() With {.Code = "VE", .CountryName = "Venezuela"}, _
        New CountryCode() With {.Code = "VN", .CountryName = "Vietnam"}, _
        New CountryCode() With {.Code = "WK", .CountryName = "Wake Island"}, _
        New CountryCode() With {.Code = "WF", .CountryName = "Wallis and Futuna"}, _
        New CountryCode() With {.Code = "EH", .CountryName = "Western Sahara"}, _
        New CountryCode() With {.Code = "YE", .CountryName = "Yemen"}, _
        New CountryCode() With {.Code = "ZM", .CountryName = "Zambia"}, _
        New CountryCode() With {.Code = "ZW", .CountryName = "Zimbabwe"}, _
        New CountryCode() With {.Code = "AX", .CountryName = "Åland Islands"} _
        }
        countryCode.ForEach(Function(u) context.CountryCodes.Add(u))

        'State Code table creation
        Dim stateCode As New List(Of StateCode)() From { _
       New StateCode() With {.StateName = "Alabama", .Code = "AL", .CountryId = 250}, _
        New StateCode() With {.StateName = "Alaska", .Code = "AK", .CountryId = 250}, _
        New StateCode() With {.StateName = "Arizona", .Code = "AZ", .CountryId = 250}, _
        New StateCode() With {.StateName = "Arkansas", .Code = "AR", .CountryId = 250}, _
        New StateCode() With {.StateName = "California", .Code = "CA", .CountryId = 250}, _
        New StateCode() With {.StateName = "Colorado", .Code = "CO", .CountryId = 250}, _
        New StateCode() With {.StateName = "Connecticut", .Code = "CT", .CountryId = 250}, _
        New StateCode() With {.StateName = "Delaware", .Code = "DE", .CountryId = 250}, _
        New StateCode() With {.StateName = "District of Columbia", .Code = "DC", .CountryId = 250}, _
        New StateCode() With {.StateName = "Florida", .Code = "FL", .CountryId = 250}, _
        New StateCode() With {.StateName = "Georgia", .Code = "GA", .CountryId = 250}, _
        New StateCode() With {.StateName = "Hawaii", .Code = "HI", .CountryId = 250}, _
        New StateCode() With {.StateName = "Idaho", .Code = "ID", .CountryId = 250}, _
        New StateCode() With {.StateName = "Illinois", .Code = "IL", .CountryId = 250}, _
        New StateCode() With {.StateName = "Indiana", .Code = "IN", .CountryId = 250}, _
        New StateCode() With {.StateName = "Iowa", .Code = "IA", .CountryId = 250}, _
        New StateCode() With {.StateName = "Kansas", .Code = "KS", .CountryId = 250}, _
        New StateCode() With {.StateName = "Kentucky", .Code = "KY", .CountryId = 250}, _
        New StateCode() With {.StateName = "Louisiana", .Code = "LA", .CountryId = 250}, _
        New StateCode() With {.StateName = "Maine", .Code = "ME", .CountryId = 250}, _
        New StateCode() With {.StateName = "Maryland", .Code = "MD", .CountryId = 250}, _
        New StateCode() With {.StateName = "Massachusetts", .Code = "MA", .CountryId = 250}, _
        New StateCode() With {.StateName = "Michigan", .Code = "MI", .CountryId = 250}, _
        New StateCode() With {.StateName = "Minnesota", .Code = "MN", .CountryId = 250}, _
        New StateCode() With {.StateName = "Mississippi", .Code = "MS", .CountryId = 250}, _
        New StateCode() With {.StateName = "Missouri", .Code = "MO", .CountryId = 250}, _
        New StateCode() With {.StateName = "Montana", .Code = "MT", .CountryId = 250}, _
        New StateCode() With {.StateName = "Nebraska", .Code = "NE", .CountryId = 250}, _
        New StateCode() With {.StateName = "Nevada", .Code = "NV", .CountryId = 250}, _
        New StateCode() With {.StateName = "New Hampshire", .Code = "NH", .CountryId = 250}, _
        New StateCode() With {.StateName = "New Jersey", .Code = "NJ", .CountryId = 250}, _
        New StateCode() With {.StateName = "New Mexico", .Code = "NM", .CountryId = 250}, _
        New StateCode() With {.StateName = "New York", .Code = "NY", .CountryId = 250}, _
        New StateCode() With {.StateName = "North Carolina", .Code = "NC", .CountryId = 250}, _
        New StateCode() With {.StateName = "North Dakota", .Code = "ND", .CountryId = 250}, _
        New StateCode() With {.StateName = "Ohio", .Code = "OH", .CountryId = 250}, _
        New StateCode() With {.StateName = "Oklahoma", .Code = "OK", .CountryId = 250}, _
        New StateCode() With {.StateName = "Oregon", .Code = "OR", .CountryId = 250}, _
        New StateCode() With {.StateName = "Pennsylvania", .Code = "PA", .CountryId = 250}, _
        New StateCode() With {.StateName = "Rhode Island", .Code = "RI", .CountryId = 250}, _
        New StateCode() With {.StateName = "South Carolina", .Code = "SC", .CountryId = 250}, _
        New StateCode() With {.StateName = "South Dakota", .Code = "SD", .CountryId = 250}, _
        New StateCode() With {.StateName = "Tennessee", .Code = "TN", .CountryId = 250}, _
        New StateCode() With {.StateName = "Texas", .Code = "TX", .CountryId = 250}, _
        New StateCode() With {.StateName = "Utah", .Code = "UT", .CountryId = 250}, _
        New StateCode() With {.StateName = "Vermont", .Code = "VT", .CountryId = 250}, _
        New StateCode() With {.StateName = "Virginia", .Code = "VA", .CountryId = 250}, _
        New StateCode() With {.StateName = "Washington", .Code = "WA", .CountryId = 250}, _
        New StateCode() With {.StateName = "West Virginia", .Code = "WV", .CountryId = 250}, _
        New StateCode() With {.StateName = "Wisconsin", .Code = "WI", .CountryId = 250}, _
        New StateCode() With {.StateName = "Wyoming", .Code = "WY", .CountryId = 250} _
       }
        stateCode.ForEach(Function(u) context.StateCodes.Add(u))

        For Each acctItem As Account In acctList
            Select Case acctItem.Description
                Case "Total new sales"
                    Dim balList As New List(Of Balance)() From { _
                         New Balance() With {.YearType = "H2", .Balance1 = 6259, .Balance2 = 5678, .Balance3 = 6995, .Balance4 = 7344, _
                    .Balance5 = 24305, .Balance6 = 16770, .Balance7 = 27823, .Balance8 = 22134, .Balance9 = 18725, .Balance10 = 14995, .Balance11 = 14847, _
                    .Balance12 = 14847, .Account = acctItem}, _
                        New Balance() With {.YearType = "H1", .Balance1 = 23842, .Balance2 = 14397, .Balance3 = 10557, .Balance4 = 16244, _
                    .Balance5 = 5045, .Balance6 = 21047, .Balance7 = 20202, .Balance8 = 34245, .Balance9 = 29846, .Balance10 = 24246, .Balance11 = 37945, _
                    .Balance12 = 27996, .Account = acctItem}, _
                        New Balance() With {.YearType = "CA", .Balance1 = 27755, .Balance2 = 12647, .Balance3 = 16671, .Balance4 = 24840, _
                    .Balance5 = 27046, .Balance6 = 21246, .Balance7 = 18223, .Balance8 = 31795, .Balance9 = 31094, .Balance10 = 40751, .Balance11 = 38444, _
                    .Balance12 = 0, .Account = acctItem}, _
                        New Balance() With {.YearType = "CB", .Balance1 = 35126, .Balance2 = 25000, .Balance3 = 15000, .Balance4 = 1200, _
                    .Balance5 = 20000, .Balance6 = 25000, .Balance7 = 30000, .Balance8 = 33000, .Balance9 = 36000, .Balance10 = 39000, .Balance11 = 42000, _
                    .Balance12 = 45000, .Account = acctItem}
                    }
                    balList.ForEach(Function(u) context.Balances.Add(u))
                Case "Total upgrade sales"
                    Dim balList As New List(Of Balance)() From { _
                        New Balance() With {.YearType = "H2", .Balance1 = 0, .Balance2 = 0, .Balance3 = 0, .Balance4 = 0, _
                    .Balance5 = 0, .Balance6 = 0, .Balance7 = 0, .Balance8 = 0, .Balance9 = 0, .Balance10 = 0, .Balance11 = 0, _
                    .Balance12 = 0, .Account = acctItem}, _
                        New Balance() With {.YearType = "H1", .Balance1 = 4882, .Balance2 = 2948, .Balance3 = 2162, .Balance4 = 3327, _
                    .Balance5 = 57388, .Balance6 = 9708, .Balance7 = 10678, .Balance8 = 3829, .Balance9 = 2709, .Balance10 = 4429, .Balance11 = 1890, _
                    .Balance12 = 4669, .Account = acctItem}, _
                        New Balance() With {.YearType = "CA", .Balance1 = 1360, .Balance2 = 4366, .Balance3 = 720, .Balance4 = 32619, _
                    .Balance5 = 18246, .Balance6 = 9808, .Balance7 = 5678, .Balance8 = 10248, .Balance9 = 1199, .Balance10 = 5399, .Balance11 = 6528, _
                    .Balance12 = 0, .Account = acctItem}, _
                        New Balance() With {.YearType = "CB", .Balance1 = 0, .Balance2 = 0, .Balance3 = 0, .Balance4 = 7500, _
                    .Balance5 = 3000, .Balance6 = 12500, .Balance7 = 5000, .Balance8 = 3000, .Balance9 = 30000, .Balance10 = 3000, .Balance11 = 3000, _
                    .Balance12 = 3000, .Account = acctItem}
                    }
                    balList.ForEach(Function(u) context.Balances.Add(u))
                Case "Cloud sales"
                    Dim balList As New List(Of Balance)() From { _
                        New Balance() With {.YearType = "H2", .Balance1 = 0, .Balance2 = 0, .Balance3 = 0, .Balance4 = 0, _
                    .Balance5 = 0, .Balance6 = 0, .Balance7 = 0, .Balance8 = 0, .Balance9 = 0, .Balance10 = 0, .Balance11 = 0, _
                    .Balance12 = 0, .Account = acctItem}, _
                        New Balance() With {.YearType = "H1", .Balance1 = 0, .Balance2 = 0, .Balance3 = 0, .Balance4 = 0, _
                    .Balance5 = 0, .Balance6 = 0, .Balance7 = 0, .Balance8 = 0, .Balance9 = 0, .Balance10 = 0, .Balance11 = 0, _
                    .Balance12 = 0, .Account = acctItem}, _
                        New Balance() With {.YearType = "CA", .Balance1 = 1, .Balance2 = 0, .Balance3 = 0, .Balance4 = 0, _
                    .Balance5 = 0, .Balance6 = 0, .Balance7 = 0, .Balance8 = 0, .Balance9 = 0, .Balance10 = 430, .Balance11 = 0, _
                    .Balance12 = 0, .Account = acctItem}, _
                        New Balance() With {.YearType = "CB", .Balance1 = 1, .Balance2 = 0, .Balance3 = 0, .Balance4 = 0, _
                    .Balance5 = 0, .Balance6 = 0, .Balance7 = 0, .Balance8 = 0, .Balance9 = 0, .Balance10 = 0, .Balance11 = 0, _
                    .Balance12 = 0, .Account = acctItem}
                    }
                    balList.ForEach(Function(u) context.Balances.Add(u))
                Case "Consulting revenue"
                    Dim balList As New List(Of Balance)() From { _
                        New Balance() With {.YearType = "H2", .Balance1 = 0, .Balance2 = 0, .Balance3 = 0, .Balance4 = 0, _
                    .Balance5 = 0, .Balance6 = 0, .Balance7 = 0, .Balance8 = 0, .Balance9 = 0, .Balance10 = 0, .Balance11 = 0, _
                    .Balance12 = 0, .Account = acctItem}, _
                        New Balance() With {.YearType = "H1", .Balance1 = 1, .Balance2 = 0, .Balance3 = 0, .Balance4 = 0, _
                    .Balance5 = 0, .Balance6 = 0, .Balance7 = 0, .Balance8 = 0, .Balance9 = 0, .Balance10 = 0, .Balance11 = 0, _
                    .Balance12 = 0, .Account = acctItem}, _
                        New Balance() With {.YearType = "CA", .Balance1 = 1, .Balance2 = 0, .Balance3 = 0, .Balance4 = 0, _
                    .Balance5 = 0, .Balance6 = 0, .Balance7 = 80, .Balance8 = 160, .Balance9 = 400, .Balance10 = 640, .Balance11 = 0, _
                    .Balance12 = 0, .Account = acctItem}, _
                        New Balance() With {.YearType = "CB", .Balance1 = 1, .Balance2 = 0, .Balance3 = 0, .Balance4 = 0, _
                    .Balance5 = 0, .Balance6 = 0, .Balance7 = 0, .Balance8 = 0, .Balance9 = 0, .Balance10 = 0, .Balance11 = 0, _
                    .Balance12 = 0, .Account = acctItem}
                    }
                    balList.ForEach(Function(u) context.Balances.Add(u))
                Case "Shipping, returns & discounts"
                    Dim balList As New List(Of Balance)() From { _
                        New Balance() With {.YearType = "H2", .Balance1 = 0, .Balance2 = 0, .Balance3 = -414, .Balance4 = 0, _
                    .Balance5 = 0, .Balance6 = -560, .Balance7 = -255, .Balance8 = -178, .Balance9 = 0, .Balance10 = -250, .Balance11 = -250, _
                    .Balance12 = 0, .Account = acctItem}, _
                        New Balance() With {.YearType = "H1", .Balance1 = -4317, .Balance2 = -2605, .Balance3 = -1910, .Balance4 = -2940, _
                    .Balance5 = -8829, .Balance6 = -8567, .Balance7 = -10825, .Balance8 = -4421, .Balance9 = -6400, .Balance10 = -6282, .Balance11 = -2445, _
                    .Balance12 = -6480, .Account = acctItem}, _
                        New Balance() With {.YearType = "CA", .Balance1 = -5099, .Balance2 = -4046, .Balance3 = -2649, .Balance4 = -11904, _
                    .Balance5 = -8546, .Balance6 = -4004, .Balance7 = -2740, .Balance8 = -5389, .Balance9 = -4634, .Balance10 = -7588, .Balance11 = -7967, _
                    .Balance12 = 0, .Account = acctItem}, _
                        New Balance() With {.YearType = "CB", .Balance1 = -1756, .Balance2 = -1250, .Balance3 = -750, .Balance4 = -600, _
                    .Balance5 = -1000, .Balance6 = -1250, .Balance7 = -1500, .Balance8 = -1650, .Balance9 = -1800, .Balance10 = -1950, .Balance11 = -2100, _
                    .Balance12 = -2250, .Account = acctItem}
                    }
                    balList.ForEach(Function(u) context.Balances.Add(u))
                Case "Total revenue"
                    Dim balList As New List(Of Balance)() From { _
                        New Balance() With {.YearType = "H2", .Balance1 = 6259, .Balance2 = 5678, .Balance3 = 6561, .Balance4 = 7344, _
                    .Balance5 = 24305, .Balance6 = 16210, .Balance7 = 27568, .Balance8 = 21956, .Balance9 = 18725, .Balance10 = 14745, .Balance11 = 14597, _
                    .Balance12 = 14863, .Account = acctItem}, _
                        New Balance() With {.YearType = "H1", .Balance1 = 24652, .Balance2 = 14888, .Balance3 = 10918, .Balance4 = 16799, _
                    .Balance5 = 53604, .Balance6 = 22188, .Balance7 = 20055, .Balance8 = 33653, .Balance9 = 26155, .Balance10 = 22393, .Balance11 = 37390, _
                    .Balance12 = 28185, .Account = acctItem}, _
                        New Balance() With {.YearType = "CA", .Balance1 = 24016, .Balance2 = 12967, .Balance3 = 14742, .Balance4 = 45555, _
                    .Balance5 = 36746, .Balance6 = 27050, .Balance7 = 21241, .Balance8 = 36814, .Balance9 = 28056, .Balance10 = 39632, .Balance11 = 36696, _
                    .Balance12 = 0, .Account = acctItem}, _
                        New Balance() With {.YearType = "CB", .Balance1 = 33370, .Balance2 = 23750, .Balance3 = 14250, .Balance4 = 18900, _
                    .Balance5 = 49000, .Balance6 = 36250, .Balance7 = 33500, .Balance8 = 34350, .Balance9 = 37200, .Balance10 = 40050, .Balance11 = 42900, _
                    .Balance12 = 45750, .Account = acctItem}
                    }
                    balList.ForEach(Function(u) context.Balances.Add(u))
                Case "Save"
                    Dim balList As New List(Of Balance)() From { _
                        New Balance() With {.YearType = "H1", .Balance1 = 0, .Balance2 = 0, .Balance3 = 0, .Balance4 = 0, _
                    .Balance5 = 0, .Balance6 = 0, .Balance7 = 0, .Balance8 = 0, .Balance9 = 0, .Balance10 = 0, .Balance11 = 0, _
                    .Balance12 = 0, .Account = acctItem}, _
                        New Balance() With {.YearType = "CA", .Balance1 = 0, .Balance2 = 0, .Balance3 = 0, .Balance4 = 0, _
                    .Balance5 = 0, .Balance6 = 0, .Balance7 = 0, .Balance8 = 0, .Balance9 = 0, .Balance10 = 0, .Balance11 = 0, _
                    .Balance12 = 0, .Account = acctItem}, _
                        New Balance() With {.YearType = "CB", .Balance1 = 0, .Balance2 = 0, .Balance3 = 0, .Balance4 = 0, _
                    .Balance5 = 0, .Balance6 = 0, .Balance7 = 0, .Balance8 = 0, .Balance9 = 0, .Balance10 = 0, .Balance11 = 0, _
                    .Balance12 = 0, .Account = acctItem}
                    }
                    balList.ForEach(Function(u) context.Balances.Add(u))
            End Select

        Next

        acctType = New AcctType() With {.TypeDesc = "Expense", .ClassDesc = "Cost of sales", .SubclassDesc = "Cost of sales"}
        context.AcctTypes.Add(acctType)

        acctList = New List(Of Account)() From { _
          New Account() With {.AcctDescriptor = "H", .SortSequence = 10, .Description = "Cost of sales", .AcctType = acctType}, _
         New Account() With {.AcctDescriptor = "D", .SortSequence = 11, .Description = "Solo server license fees", .AcctType = acctType}, _
         New Account() With {.AcctDescriptor = "D", .SortSequence = 12, .Description = "Credit card processing fees", .AcctType = acctType}, _
         New Account() With {.AcctDescriptor = "D", .SortSequence = 13, .Description = "Packaging and shipping", .AcctType = acctType}, _
         New Account() With {.AcctDescriptor = "D", .SortSequence = 14, .Description = "Commissions and referral fees", .AcctType = acctType}, _
         New Account() With {.AcctDescriptor = "T", .SortSequence = 15, .Description = "Total Cost of sales", .AcctType = acctType} _
        }
        acctList.ForEach(Function(u) context.Accounts.Add(u))

        For Each acctItem As Account In acctList
            Select Case acctItem.Description
                Case "Solo server license fees"
                    Dim balList As New List(Of Balance)() From { _
                        New Balance() With {.YearType = "H2", .Balance1 = 0, .Balance2 = 0, .Balance3 = 0, .Balance4 = 0, _
                    .Balance5 = 0, .Balance6 = 0, .Balance7 = 0, .Balance8 = 0, .Balance9 = 0, .Balance10 = 0, .Balance11 = 0, _
                    .Balance12 = 0, .Account = acctItem}, _
                        New Balance() With {.YearType = "H1", .Balance1 = 0, .Balance2 = 0, .Balance3 = 0, .Balance4 = 0, _
                    .Balance5 = 0, .Balance6 = 0, .Balance7 = 0, .Balance8 = 579, .Balance9 = 394, .Balance10 = 535, .Balance11 = 415, _
                    .Balance12 = 0, .Account = acctItem}, _
                        New Balance() With {.YearType = "CA", .Balance1 = 648, .Balance2 = 623, .Balance3 = 617, .Balance4 = 614, _
                    .Balance5 = 688, .Balance6 = 667, .Balance7 = 593, .Balance8 = 703, .Balance9 = 677, .Balance10 = 642, .Balance11 = 641, _
                    .Balance12 = 0, .Account = acctItem}, _
                        New Balance() With {.YearType = "CB", .Balance1 = 584, .Balance2 = 416, .Balance3 = 249, .Balance4 = 331, _
                    .Balance5 = 858, .Balance6 = 634, .Balance7 = 586, .Balance8 = 601, .Balance9 = 651, .Balance10 = 701, .Balance11 = 751, _
                    .Balance12 = 801, .Account = acctItem}
                    }
                    balList.ForEach(Function(u) context.Balances.Add(u))
                Case "Credit card processing fees"
                    Dim balList As New List(Of Balance)() From { _
                        New Balance() With {.YearType = "H2", .Balance1 = 54, .Balance2 = 0, .Balance3 = 0, .Balance4 = 0, _
                    .Balance5 = 0, .Balance6 = 0, .Balance7 = 0, .Balance8 = 0, .Balance9 = 0, .Balance10 = 0, .Balance11 = 0, _
                    .Balance12 = 0, .Account = acctItem}, _
                        New Balance() With {.YearType = "H1", .Balance1 = 725, .Balance2 = 432, .Balance3 = 322, .Balance4 = 560, _
                    .Balance5 = 1329, .Balance6 = 662, .Balance7 = 0, .Balance8 = 875, .Balance9 = 713, .Balance10 = 682, .Balance11 = 1022, _
                    .Balance12 = 0, .Account = acctItem}, _
                        New Balance() With {.YearType = "CA", .Balance1 = 744, .Balance2 = 437, .Balance3 = 451, .Balance4 = 1346, _
                    .Balance5 = 1071, .Balance6 = 731, .Balance7 = 547, .Balance8 = 1089, .Balance9 = 731, .Balance10 = 1188, .Balance11 = 957, _
                    .Balance12 = 0, .Account = acctItem}, _
                        New Balance() With {.YearType = "CB", .Balance1 = 918, .Balance2 = 653, .Balance3 = 392, .Balance4 = 420, _
                    .Balance5 = 1348, .Balance6 = 997, .Balance7 = 921, .Balance8 = 945, .Balance9 = 1023, .Balance10 = 1101, .Balance11 = 1180, _
                    .Balance12 = 1258, .Account = acctItem}
                    }
                    balList.ForEach(Function(u) context.Balances.Add(u))
                Case "Packaging and shipping"
                    Dim balList As New List(Of Balance)() From { _
                        New Balance() With {.YearType = "H2", .Balance1 = 252, .Balance2 = 75, .Balance3 = 32, .Balance4 = 63, _
                    .Balance5 = 26, .Balance6 = 101, .Balance7 = 593, .Balance8 = 132, .Balance9 = 310, .Balance10 = 387, .Balance11 = 162, _
                    .Balance12 = 42, .Account = acctItem}, _
                        New Balance() With {.YearType = "H1", .Balance1 = 47, .Balance2 = 26, .Balance3 = 78, .Balance4 = 59, _
                    .Balance5 = 10, .Balance6 = 77, .Balance7 = 0, .Balance8 = 669, .Balance9 = 82, .Balance10 = 26, .Balance11 = 91, _
                    .Balance12 = 0, .Account = acctItem}, _
                        New Balance() With {.YearType = "CA", .Balance1 = 66, .Balance2 = 66, .Balance3 = 96, .Balance4 = 66, _
                    .Balance5 = 0, .Balance6 = 360, .Balance7 = 208, .Balance8 = 158, .Balance9 = 0, .Balance10 = 0, .Balance11 = 0, _
                    .Balance12 = 0, .Account = acctItem}, _
                        New Balance() With {.YearType = "CB", .Balance1 = 167, .Balance2 = 119, .Balance3 = 71, .Balance4 = 95, _
                    .Balance5 = 245, .Balance6 = 181, .Balance7 = 168, .Balance8 = 172, .Balance9 = 186, .Balance10 = 200, .Balance11 = 215, _
                    .Balance12 = 229, .Account = acctItem}
                    }
                    balList.ForEach(Function(u) context.Balances.Add(u))
                Case "Commissions and referral fees"
                    Dim balList As New List(Of Balance)() From { _
                        New Balance() With {.YearType = "H2", .Balance1 = 0, .Balance2 = 0, .Balance3 = 0, .Balance4 = 0, _
                    .Balance5 = 0, .Balance6 = 0, .Balance7 = 0, .Balance8 = 0, .Balance9 = 0, .Balance10 = 0, .Balance11 = 0, _
                    .Balance12 = 0, .Account = acctItem}, _
                        New Balance() With {.YearType = "H1", .Balance1 = 0, .Balance2 = 0, .Balance3 = 0, .Balance4 = 0, _
                    .Balance5 = 0, .Balance6 = 0, .Balance7 = 0, .Balance8 = 0, .Balance9 = 0, .Balance10 = 0, .Balance11 = 0, _
                    .Balance12 = 0, .Account = acctItem}, _
                        New Balance() With {.YearType = "CA", .Balance1 = 0, .Balance2 = 0, .Balance3 = 588, .Balance4 = 1639, _
                    .Balance5 = 1037, .Balance6 = 728, .Balance7 = 1189, .Balance8 = 2128, .Balance9 = 1339, .Balance10 = 3056, .Balance11 = 1132, _
                    .Balance12 = 0, .Account = acctItem}, _
                        New Balance() With {.YearType = "CB", .Balance1 = 0, .Balance2 = 0, .Balance3 = 0, .Balance4 = 0, _
                    .Balance5 = 0, .Balance6 = 0, .Balance7 = 0, .Balance8 = 0, .Balance9 = 0, .Balance10 = 0, .Balance11 = 0, _
                    .Balance12 = 0, .Account = acctItem}
                    }
                    balList.ForEach(Function(u) context.Balances.Add(u))
                Case "Total Cost of sales"
                    Dim balList As New List(Of Balance)() From { _
                        New Balance() With {.YearType = "H2", .Balance1 = 306, .Balance2 = 75, .Balance3 = 32, .Balance4 = 63, _
                    .Balance5 = 26, .Balance6 = 101, .Balance7 = 593, .Balance8 = 132, .Balance9 = 310, .Balance10 = 387, .Balance11 = 162, _
                    .Balance12 = 42, .Account = acctItem}, _
                        New Balance() With {.YearType = "H1", .Balance1 = 772, .Balance2 = 458, .Balance3 = 400, .Balance4 = 619, _
                    .Balance5 = 1339, .Balance6 = 739, .Balance7 = 0, .Balance8 = 2123, .Balance9 = 1189, .Balance10 = 1243, .Balance11 = 1523, _
                    .Balance12 = 0, .Account = acctItem}, _
                        New Balance() With {.YearType = "CA", .Balance1 = 1458, .Balance2 = 1126, .Balance3 = 1752, .Balance4 = 2796, _
                    .Balance5 = 2796, .Balance6 = 2486, .Balance7 = 2537, .Balance8 = 4078, .Balance9 = 2747, .Balance10 = 4886, .Balance11 = 3096, _
                    .Balance12 = 0, .Account = acctItem}, _
                        New Balance() With {.YearType = "CB", .Balance1 = 1669, .Balance2 = 1188, .Balance3 = 712, .Balance4 = 946, _
                    .Balance5 = 2451, .Balance6 = 1812, .Balance7 = 1675, .Balance8 = 1718, .Balance9 = 1860, .Balance10 = 2002, .Balance11 = 2146, _
                    .Balance12 = 2288, .Account = acctItem}
                    }
                    balList.ForEach(Function(u) context.Balances.Add(u))

            End Select

        Next

        acctType = New AcctType() With {.TypeDesc = "Expense", .ClassDesc = "Sales and marketing expenses", .SubclassDesc = "Sales and marketing expenses"}
        context.AcctTypes.Add(acctType)

        acctList = New List(Of Account)() From { _
         New Account() With {.AcctDescriptor = "H", .SortSequence = 16, .Description = "Sales and marketing expenses", .AcctType = acctType}, _
        New Account() With {.AcctDescriptor = "D", .SortSequence = 17, .Description = "Online advertising", .AcctType = acctType}, _
        New Account() With {.AcctDescriptor = "D", .SortSequence = 18, .Description = "Brafton content development", .AcctType = acctType}, _
        New Account() With {.AcctDescriptor = "D", .SortSequence = 19, .Description = "Other sales and marketing", .AcctType = acctType}, _
        New Account() With {.AcctDescriptor = "D", .SortSequence = 20, .Description = "Trade show expenses", .AcctType = acctType}, _
        New Account() With {.AcctDescriptor = "D", .SortSequence = 21, .Description = "Direct mail", .AcctType = acctType}, _
        New Account() With {.AcctDescriptor = "T", .SortSequence = 22, .Description = "Total sales and marketing", .AcctType = acctType} _
       }
        acctList.ForEach(Function(u) context.Accounts.Add(u))

        For Each acctItem As Account In acctList
            Select Case acctItem.Description
                Case "Online advertising"
                    Dim balList As New List(Of Balance)() From { _
                        New Balance() With {.YearType = "H2", .Balance1 = 507, .Balance2 = 567, .Balance3 = 1014, .Balance4 = 952, _
                    .Balance5 = 1043, .Balance6 = 1421, .Balance7 = 1147, .Balance8 = 3628, .Balance9 = 1454, .Balance10 = 1312, .Balance11 = 3307, _
                    .Balance12 = 2233, .Account = acctItem}, _
                        New Balance() With {.YearType = "H1", .Balance1 = 2467, .Balance2 = 6822, .Balance3 = 3604, .Balance4 = 2981, _
                    .Balance5 = 3055, .Balance6 = 1976, .Balance7 = 2263, .Balance8 = 2296, .Balance9 = 2612, .Balance10 = 6590, .Balance11 = 2650, _
                    .Balance12 = 3500, .Account = acctItem}, _
                        New Balance() With {.YearType = "CA", .Balance1 = 3224, .Balance2 = 3226, .Balance3 = 3225, .Balance4 = 3225, _
                    .Balance5 = 3224, .Balance6 = 3326, .Balance7 = 3325, .Balance8 = 4068, .Balance9 = 4018, .Balance10 = 4657, .Balance11 = 4426, _
                    .Balance12 = 0, .Account = acctItem}, _
                        New Balance() With {.YearType = "CB", .Balance1 = 3000, .Balance2 = 3000, .Balance3 = 3000, .Balance4 = 3000, _
                    .Balance5 = 5000, .Balance6 = 5000, .Balance7 = 5000, .Balance8 = 5000, .Balance9 = 5000, .Balance10 = 5000, .Balance11 = 5000, _
                    .Balance12 = 0, .Account = acctItem}
                    }
                    balList.ForEach(Function(u) context.Balances.Add(u))
                Case "Brafton content development"
                    Dim balList As New List(Of Balance)() From { _
                        New Balance() With {.YearType = "H2", .Balance1 = 0, .Balance2 = 0, .Balance3 = 0, .Balance4 = 0, _
                    .Balance5 = 0, .Balance6 = 0, .Balance7 = 0, .Balance8 = 0, .Balance9 = 0, .Balance10 = 0, .Balance11 = 0, _
                    .Balance12 = 0, .Account = acctItem}, _
                        New Balance() With {.YearType = "H1", .Balance1 = 0, .Balance2 = 0, .Balance3 = 0, .Balance4 = 0, _
                    .Balance5 = 0, .Balance6 = 0, .Balance7 = 0, .Balance8 = 0, .Balance9 = 0, .Balance10 = 0, .Balance11 = 0, _
                    .Balance12 = 0, .Account = acctItem}, _
                        New Balance() With {.YearType = "CA", .Balance1 = 0, .Balance2 = 0, .Balance3 = 0, .Balance4 = 0, _
                    .Balance5 = 1000, .Balance6 = 2200, .Balance7 = 2200, .Balance8 = 2200, .Balance9 = 2200, .Balance10 = 1469, .Balance11 = 1000, _
                    .Balance12 = 0, .Account = acctItem}, _
                        New Balance() With {.YearType = "CB", .Balance1 = 0, .Balance2 = 0, .Balance3 = 0, .Balance4 = 0, _
                    .Balance5 = 0, .Balance6 = 0, .Balance7 = 0, .Balance8 = 0, .Balance9 = 0, .Balance10 = 0, .Balance11 = 0, _
                    .Balance12 = 0, .Account = acctItem}
                    }
                    balList.ForEach(Function(u) context.Balances.Add(u))
                Case "Other sales and marketing"
                    Dim balList As New List(Of Balance)() From { _
                        New Balance() With {.YearType = "H2", .Balance1 = 0, .Balance2 = 0, .Balance3 = 0, .Balance4 = 0, _
                    .Balance5 = 0, .Balance6 = 283, .Balance7 = 2105, .Balance8 = 2947, .Balance9 = 4111, .Balance10 = 2680, .Balance11 = 1272, _
                    .Balance12 = 1318, .Account = acctItem}, _
                        New Balance() With {.YearType = "H1", .Balance1 = 272, .Balance2 = 713, .Balance3 = 326, .Balance4 = 316, _
                    .Balance5 = 465, .Balance6 = 675, .Balance7 = 1249, .Balance8 = 490, .Balance9 = 533, .Balance10 = 804, .Balance11 = 575, _
                    .Balance12 = 0, .Account = acctItem}, _
                        New Balance() With {.YearType = "CA", .Balance1 = 572, .Balance2 = 447, .Balance3 = 391, .Balance4 = 410, _
                    .Balance5 = 201, .Balance6 = 510, .Balance7 = 341, .Balance8 = 951, .Balance9 = 1241, .Balance10 = 2404, .Balance11 = 1550, _
                    .Balance12 = 0, .Account = acctItem}, _
                        New Balance() With {.YearType = "CB", .Balance1 = 770, .Balance2 = 770, .Balance3 = 770, .Balance4 = 770, _
                    .Balance5 = 770, .Balance6 = 770, .Balance7 = 770, .Balance8 = 770, .Balance9 = 770, .Balance10 = 770, .Balance11 = 770, _
                    .Balance12 = 770, .Account = acctItem}
                    }
                    balList.ForEach(Function(u) context.Balances.Add(u))
                Case "Trade show expenses"
                    Dim balList As New List(Of Balance)() From { _
                        New Balance() With {.YearType = "H2", .Balance1 = 0, .Balance2 = 0, .Balance3 = 0, .Balance4 = 0, _
                    .Balance5 = 0, .Balance6 = 0, .Balance7 = 0, .Balance8 = 0, .Balance9 = 0, .Balance10 = 0, .Balance11 = 0, _
                    .Balance12 = 0, .Account = acctItem}, _
                        New Balance() With {.YearType = "H1", .Balance1 = 0, .Balance2 = 0, .Balance3 = 0, .Balance4 = 0, _
                    .Balance5 = 0, .Balance6 = 2761, .Balance7 = 2043, .Balance8 = 2065, .Balance9 = 321, .Balance10 = 5617, .Balance11 = 173, _
                    .Balance12 = 0, .Account = acctItem}, _
                        New Balance() With {.YearType = "CA", .Balance1 = 3200, .Balance2 = 0, .Balance3 = 0, .Balance4 = 0, _
                    .Balance5 = 0, .Balance6 = 0, .Balance7 = 0, .Balance8 = 0, .Balance9 = 598, .Balance10 = 1853, .Balance11 = 867, _
                    .Balance12 = 0, .Account = acctItem}, _
                        New Balance() With {.YearType = "CB", .Balance1 = 0, .Balance2 = 0, .Balance3 = 0, .Balance4 = 0, _
                    .Balance5 = 5000, .Balance6 = 0, .Balance7 = 0, .Balance8 = 0, .Balance9 = 5000, .Balance10 = 0, .Balance11 = 0, _
                    .Balance12 = 0, .Account = acctItem}
                    }
                    balList.ForEach(Function(u) context.Balances.Add(u))
                Case "Direct mail"
                    Dim balList As New List(Of Balance)() From { _
                        New Balance() With {.YearType = "H2", .Balance1 = 0, .Balance2 = 0, .Balance3 = 801, .Balance4 = 0, _
                    .Balance5 = 1437, .Balance6 = 998, .Balance7 = 1957, .Balance8 = 1014, .Balance9 = 0, .Balance10 = 785, .Balance11 = 0, _
                    .Balance12 = 0, .Account = acctItem}, _
                        New Balance() With {.YearType = "H1", .Balance1 = 0, .Balance2 = 0, .Balance3 = 0, .Balance4 = 0, _
                    .Balance5 = 4438, .Balance6 = 1373, .Balance7 = 1047, .Balance8 = 255, .Balance9 = 0, .Balance10 = 0, .Balance11 = 0, _
                    .Balance12 = 0, .Account = acctItem}, _
                        New Balance() With {.YearType = "CA", .Balance1 = 0, .Balance2 = 0, .Balance3 = 0, .Balance4 = 0, _
                    .Balance5 = 4054, .Balance6 = -267, .Balance7 = -195, .Balance8 = 0, .Balance9 = 0, .Balance10 = 0, .Balance11 = 66, _
                    .Balance12 = 0, .Account = acctItem}, _
                        New Balance() With {.YearType = "CB", .Balance1 = 0, .Balance2 = 0, .Balance3 = 0, .Balance4 = 2000, _
                    .Balance5 = 5000, .Balance6 = 0, .Balance7 = 0, .Balance8 = 0, .Balance9 = 0, .Balance10 = 0, .Balance11 = 0, _
                    .Balance12 = 0, .Account = acctItem}
                    }
                    balList.ForEach(Function(u) context.Balances.Add(u))
                Case "Total sales and marketing"
                    Dim balList As New List(Of Balance)() From { _
                        New Balance() With {.YearType = "H2", .Balance1 = 507, .Balance2 = 567, .Balance3 = 1815, .Balance4 = 952, _
                    .Balance5 = 2480, .Balance6 = 2702, .Balance7 = 5209, .Balance8 = 7589, .Balance9 = 5565, .Balance10 = 4777, .Balance11 = 4579, _
                    .Balance12 = 3543, .Account = acctItem}, _
                        New Balance() With {.YearType = "H1", .Balance1 = 2739, .Balance2 = 7535, .Balance3 = 3930, .Balance4 = 3297, _
                    .Balance5 = 7988, .Balance6 = 6785, .Balance7 = 6602, .Balance8 = 5107, .Balance9 = 3463, .Balance10 = 13011, .Balance11 = 3393, _
                    .Balance12 = 3503, .Account = acctItem}, _
                        New Balance() With {.YearType = "CA", .Balance1 = 6996, .Balance2 = 3673, .Balance3 = 3616, .Balance4 = 3635, _
                    .Balance5 = 8479, .Balance6 = 5769, .Balance7 = 5671, .Balance8 = 7219, .Balance9 = 8055, .Balance10 = 10383, .Balance11 = 7909, _
                    .Balance12 = 0, .Account = acctItem}, _
                        New Balance() With {.YearType = "CB", .Balance1 = 3770, .Balance2 = 3770, .Balance3 = 3770, .Balance4 = 5770, _
                    .Balance5 = 15770, .Balance6 = 5770, .Balance7 = 5770, .Balance8 = 5770, .Balance9 = 10770, .Balance10 = 5770, .Balance11 = 5770, _
                    .Balance12 = 5770, .Account = acctItem}
                    }
                    balList.ForEach(Function(u) context.Balances.Add(u))
            End Select
        Next

        acctType = New AcctType() With {.TypeDesc = "Expense", .ClassDesc = "Support expenses", .SubclassDesc = "Support expenses"}
        context.AcctTypes.Add(acctType)

        acctList = New List(Of Account)() From { _
         New Account() With {.AcctDescriptor = "H", .SortSequence = 23, .Description = "Support expenses", .AcctType = acctType}, _
         New Account() With {.AcctDescriptor = "D", .SortSequence = 24, .Description = "Zendesk", .AcctType = acctType}, _
         New Account() With {.AcctDescriptor = "T", .SortSequence = 25, .Description = "Total support expenses", .AcctType = acctType} _
       }
        acctList.ForEach(Function(u) context.Accounts.Add(u))

        For Each acctItem As Account In acctList
            Select Case acctItem.Description
                Case "Zendesk"
                    Dim balList As New List(Of Balance)() From { _
                        New Balance() With {.YearType = "H2", .Balance1 = 0, .Balance2 = 0, .Balance3 = 0, .Balance4 = 0, _
                    .Balance5 = 0, .Balance6 = 0, .Balance7 = 0, .Balance8 = 0, .Balance9 = 0, .Balance10 = 0, .Balance11 = 0, _
                    .Balance12 = 0, .Account = acctItem}, _
                         New Balance() With {.YearType = "H1", .Balance1 = 0, .Balance2 = 0, .Balance3 = 0, .Balance4 = 0, _
                    .Balance5 = 0, .Balance6 = 0, .Balance7 = 0, .Balance8 = 0, .Balance9 = 290, .Balance10 = 145, .Balance11 = 145, _
                    .Balance12 = 0, .Account = acctItem}, _
                        New Balance() With {.YearType = "CA", .Balance1 = 0, .Balance2 = 145, .Balance3 = 145, .Balance4 = 131, _
                    .Balance5 = 116, .Balance6 = 255, .Balance7 = 145, .Balance8 = 220, .Balance9 = 220, .Balance10 = 220, .Balance11 = 0, _
                    .Balance12 = 0, .Account = acctItem}, _
                       New Balance() With {.YearType = "CB", .Balance1 = 145, .Balance2 = 145, .Balance3 = 145, .Balance4 = 145, _
                    .Balance5 = 145, .Balance6 = 145, .Balance7 = 145, .Balance8 = 145, .Balance9 = 145, .Balance10 = 145, .Balance11 = 145, _
                    .Balance12 = 145, .Account = acctItem}
                    }
                    balList.ForEach(Function(u) context.Balances.Add(u))
                Case "Total support expenses"
                    Dim balList As New List(Of Balance)() From { _
                        New Balance() With {.YearType = "H2", .Balance1 = 0, .Balance2 = 0, .Balance3 = 0, .Balance4 = 0, _
                    .Balance5 = 0, .Balance6 = 0, .Balance7 = 0, .Balance8 = 0, .Balance9 = 0, .Balance10 = 0, .Balance11 = 0, _
                    .Balance12 = 0, .Account = acctItem}, _
                          New Balance() With {.YearType = "H1", .Balance1 = 0, .Balance2 = 0, .Balance3 = 0, .Balance4 = 0, _
                    .Balance5 = 0, .Balance6 = 0, .Balance7 = 0, .Balance8 = 0, .Balance9 = 290, .Balance10 = 145, .Balance11 = 145, _
                    .Balance12 = 0, .Account = acctItem}, _
                        New Balance() With {.YearType = "CA", .Balance1 = 0, .Balance2 = 145, .Balance3 = 145, .Balance4 = 131, _
                    .Balance5 = 116, .Balance6 = 255, .Balance7 = 145, .Balance8 = 220, .Balance9 = 220, .Balance10 = 220, .Balance11 = 0, _
                    .Balance12 = 0, .Account = acctItem}, _
                        New Balance() With {.YearType = "CB", .Balance1 = 145, .Balance2 = 145, .Balance3 = 145, .Balance4 = 145, _
                    .Balance5 = 145, .Balance6 = 145, .Balance7 = 145, .Balance8 = 145, .Balance9 = 145, .Balance10 = 145, .Balance11 = 145, _
                    .Balance12 = 145, .Account = acctItem}
                    }
                    balList.ForEach(Function(u) context.Balances.Add(u))
            End Select
        Next

        acctType = New AcctType() With {.TypeDesc = "Expense", .ClassDesc = "Operating expenses", .SubclassDesc = "Operating expenses"}
        context.AcctTypes.Add(acctType)

        acctList = New List(Of Account)() From { _
         New Account() With {.AcctDescriptor = "H", .SortSequence = 26, .Description = "Operating expenses", .AcctType = acctType}, _
         New Account() With {.AcctDescriptor = "D", .SortSequence = 27, .Description = "Employee compensation", .AcctType = acctType}, _
         New Account() With {.AcctDescriptor = "D", .SortSequence = 28, .Description = "Home office expense", .AcctType = acctType}, _
         New Account() With {.AcctDescriptor = "D", .SortSequence = 29, .Description = "Health insurance", .AcctType = acctType}, _
         New Account() With {.AcctDescriptor = "D", .SortSequence = 30, .Description = "Life insurance", .AcctType = acctType}, _
         New Account() With {.AcctDescriptor = "D", .SortSequence = 31, .Description = "Payroll taxes", .AcctType = acctType}, _
         New Account() With {.AcctDescriptor = "D", .SortSequence = 32, .Description = "Professional services", .AcctType = acctType}, _
         New Account() With {.AcctDescriptor = "D", .SortSequence = 33, .Description = "Internet services", .AcctType = acctType}, _
         New Account() With {.AcctDescriptor = "D", .SortSequence = 34, .Description = "Other office expense", .AcctType = acctType}, _
         New Account() With {.AcctDescriptor = "D", .SortSequence = 35, .Description = "Telephone", .AcctType = acctType}, _
         New Account() With {.AcctDescriptor = "D", .SortSequence = 36, .Description = "Utilities", .AcctType = acctType}, _
         New Account() With {.AcctDescriptor = "D", .SortSequence = 37, .Description = "Call center", .AcctType = acctType}, _
         New Account() With {.AcctDescriptor = "D", .SortSequence = 38, .Description = "Corporate taxes", .AcctType = acctType}, _
         New Account() With {.AcctDescriptor = "D", .SortSequence = 39, .Description = "Travel", .AcctType = acctType}, _
         New Account() With {.AcctDescriptor = "T", .SortSequence = 40, .Description = "Total operating expenses expenses", .AcctType = acctType} _
       }
        acctList.ForEach(Function(u) context.Accounts.Add(u))

        For Each acctItem As Account In acctList
            Select Case acctItem.Description
                Case "Employee compensation"
                    Dim balList As New List(Of Balance)() From { _
                        New Balance() With {.YearType = "H2", .Balance1 = 3400, .Balance2 = 3400, .Balance3 = 3400, .Balance4 = 3400, _
                    .Balance5 = 3400, .Balance6 = 3400, .Balance7 = 5100, .Balance8 = 6800, .Balance9 = 6800, .Balance10 = 6800, .Balance11 = 8900, _
                    .Balance12 = 8900, .Account = acctItem}, _
                         New Balance() With {.YearType = "H1", .Balance1 = 12800, .Balance2 = 14900, .Balance3 = 8900, .Balance4 = 8900, _
                    .Balance5 = 19400, .Balance6 = 11900, .Balance7 = 13408, .Balance8 = 10600, .Balance9 = 10600, .Balance10 = 10600, .Balance11 = 10600, _
                    .Balance12 = 10600, .Account = acctItem}, _
                        New Balance() With {.YearType = "CA", .Balance1 = 10600, .Balance2 = 8500, .Balance3 = 8500, .Balance4 = 8500, _
                    .Balance5 = 9500, .Balance6 = 12495, .Balance7 = 12495, .Balance8 = 12492, .Balance9 = 10492, .Balance10 = 10492, .Balance11 = 10492, _
                    .Balance12 = 0, .Account = acctItem}, _
                        New Balance() With {.YearType = "CB", .Balance1 = 8500, .Balance2 = 8500, .Balance3 = 8500, .Balance4 = 5100, _
                    .Balance5 = 10500, .Balance6 = 10500, .Balance7 = 10500, .Balance8 = 10500, .Balance9 = 13500, .Balance10 = 13500, .Balance11 = 13500, _
                    .Balance12 = 13500, .Account = acctItem}
                    }
                    balList.ForEach(Function(u) context.Balances.Add(u))
                Case "Home office expense"
                    Dim balList As New List(Of Balance)() From { _
                        New Balance() With {.YearType = "H2", .Balance1 = 0, .Balance2 = 0, .Balance3 = 0, .Balance4 = 0, _
                    .Balance5 = 0, .Balance6 = 0, .Balance7 = 0, .Balance8 = 0, .Balance9 = 0, .Balance10 = 0, .Balance11 = 0, _
                    .Balance12 = 0, .Account = acctItem}, _
                         New Balance() With {.YearType = "H1", .Balance1 = 0, .Balance2 = 0, .Balance3 = 0, .Balance4 = 0, _
                    .Balance5 = 0, .Balance6 = 2000, .Balance7 = 1500, .Balance8 = 4500, .Balance9 = 0, .Balance10 = 3000, .Balance11 = 3000, _
                    .Balance12 = 0, .Account = acctItem}, _
                        New Balance() With {.YearType = "CA", .Balance1 = 2500, .Balance2 = 2500, .Balance3 = 0, .Balance4 = 2500, _
                    .Balance5 = 2500, .Balance6 = 2500, .Balance7 = 2500, .Balance8 = 2500, .Balance9 = 2500, .Balance10 = 2500, .Balance11 = 2500, _
                    .Balance12 = 2500, .Account = acctItem}, _
                        New Balance() With {.YearType = "CB", .Balance1 = 2500, .Balance2 = 2500, .Balance3 = 2500, .Balance4 = 2500, _
                    .Balance5 = 2500, .Balance6 = 2500, .Balance7 = 2500, .Balance8 = 2500, .Balance9 = 2500, .Balance10 = 2500, .Balance11 = 2500, _
                    .Balance12 = 2500, .Account = acctItem}
                    }
                    balList.ForEach(Function(u) context.Balances.Add(u))
                Case "Health insurance"
                    Dim balList As New List(Of Balance)() From { _
                        New Balance() With {.YearType = "H2", .Balance1 = 533, .Balance2 = 533, .Balance3 = 533, .Balance4 = 533, _
                    .Balance5 = 533, .Balance6 = 533, .Balance7 = 533, .Balance8 = 533, .Balance9 = 533, .Balance10 = 533, .Balance11 = 585, _
                    .Balance12 = 644, .Account = acctItem}, _
                         New Balance() With {.YearType = "H1", .Balance1 = 644, .Balance2 = 644, .Balance3 = 644, .Balance4 = 1094, _
                    .Balance5 = 644, .Balance6 = 982, .Balance7 = 2139, .Balance8 = 1290, .Balance9 = 1290, .Balance10 = 1290, .Balance11 = 1290, _
                    .Balance12 = 1290, .Account = acctItem}, _
                        New Balance() With {.YearType = "CA", .Balance1 = 1275, .Balance2 = 854, .Balance3 = 1084, .Balance4 = 1065, _
                    .Balance5 = 1164, .Balance6 = 1164, .Balance7 = 1206, .Balance8 = 1775, .Balance9 = 1776, .Balance10 = 1783, .Balance11 = 756, _
                    .Balance12 = 0, .Account = acctItem}, _
                        New Balance() With {.YearType = "CB", .Balance1 = 1100, .Balance2 = 1100, .Balance3 = 1100, .Balance4 = 1100, _
                    .Balance5 = 1100, .Balance6 = 1100, .Balance7 = 1100, .Balance8 = 1100, .Balance9 = 1100, .Balance10 = 1100, .Balance11 = 1100, _
                    .Balance12 = 1100, .Account = acctItem}
                    }
                    balList.ForEach(Function(u) context.Balances.Add(u))
                Case "Total support expenses"
                    Dim balList As New List(Of Balance)() From { _
                        New Balance() With {.YearType = "H2", .Balance1 = 0, .Balance2 = 0, .Balance3 = 0, .Balance4 = 0, _
                    .Balance5 = 0, .Balance6 = 0, .Balance7 = 0, .Balance8 = 0, .Balance9 = 0, .Balance10 = 0, .Balance11 = 0, _
                    .Balance12 = 0, .Account = acctItem}, _
                         New Balance() With {.YearType = "H1", .Balance1 = 0, .Balance2 = 0, .Balance3 = 0, .Balance4 = 0, _
                    .Balance5 = 0, .Balance6 = 0, .Balance7 = 0, .Balance8 = 0, .Balance9 = 0, .Balance10 = 0, .Balance11 = 0, _
                    .Balance12 = 0, .Account = acctItem}, _
                        New Balance() With {.YearType = "CA", .Balance1 = 0, .Balance2 = 0, .Balance3 = 0, .Balance4 = 0, _
                    .Balance5 = 0, .Balance6 = 0, .Balance7 = 0, .Balance8 = 0, .Balance9 = 0, .Balance10 = 0, .Balance11 = 0, _
                    .Balance12 = 0, .Account = acctItem}, _
                        New Balance() With {.YearType = "CB", .Balance1 = 0, .Balance2 = 0, .Balance3 = 0, .Balance4 = 0, _
                    .Balance5 = 0, .Balance6 = 0, .Balance7 = 0, .Balance8 = 0, .Balance9 = 0, .Balance10 = 0, .Balance11 = 0, _
                    .Balance12 = 0, .Account = acctItem}
                    }
                    balList.ForEach(Function(u) context.Balances.Add(u))
                Case "Life insurance"
                    Dim balList As New List(Of Balance)() From { _
                        New Balance() With {.YearType = "H2", .Balance1 = 0, .Balance2 = 0, .Balance3 = 0, .Balance4 = 0, _
                    .Balance5 = 0, .Balance6 = 0, .Balance7 = 0, .Balance8 = 0, .Balance9 = 0, .Balance10 = 0, .Balance11 = 0, _
                    .Balance12 = 0, .Account = acctItem}, _
                         New Balance() With {.YearType = "H1", .Balance1 = 0, .Balance2 = 0, .Balance3 = 0, .Balance4 = 0, _
                    .Balance5 = 0, .Balance6 = 0, .Balance7 = 0, .Balance8 = 0, .Balance9 = 0, .Balance10 = 0, .Balance11 = 0, _
                    .Balance12 = 0, .Account = acctItem}, _
                        New Balance() With {.YearType = "CA", .Balance1 = 0, .Balance2 = 0, .Balance3 = 0, .Balance4 = 0, _
                    .Balance5 = 0, .Balance6 = 0, .Balance7 = 0, .Balance8 = 0, .Balance9 = 0, .Balance10 = 0, .Balance11 = 0, _
                    .Balance12 = 0, .Account = acctItem}, _
                        New Balance() With {.YearType = "CB", .Balance1 = 0, .Balance2 = 0, .Balance3 = 0, .Balance4 = 0, _
                    .Balance5 = 0, .Balance6 = 0, .Balance7 = 0, .Balance8 = 0, .Balance9 = 0, .Balance10 = 0, .Balance11 = 0, _
                    .Balance12 = 0, .Account = acctItem}
                    }
                    balList.ForEach(Function(u) context.Balances.Add(u))

            End Select
        Next

        MyBase.Seed(context)

    End Sub
End Class
