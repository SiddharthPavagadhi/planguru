﻿Imports System.ComponentModel.DataAnnotations

Public Class Balance

    Public Property BalanceId As Integer
    <Required()>
    <StringLength(2)>
    Public Property YearType As String
    <RegularExpression("^-?[0-9]+(\.[0-9]{1,5})?$")>
    Public Property Balance1 As Decimal
    <RegularExpression("^-?[0-9]+(\.[0-9]{1,5})?$")>
    Public Property Balance2 As Decimal
    <RegularExpression("^-?[0-9]+(\.[0-9]{1,5})?$")>
    Public Property Balance3 As Decimal
    <RegularExpression("^-?[0-9]+(\.[0-9]{1,5})?$")>
    Public Property Balance4 As Decimal
    <RegularExpression("^-?[0-9]+(\.[0-9]{1,5})?$")>
    Public Property Balance5 As Decimal
    <RegularExpression("^-?[0-9]+(\.[0-9]{1,5})?$")>
    Public Property Balance6 As Decimal
    <RegularExpression("^-?[0-9]+(\.[0-9]{1,5})?$")>
    Public Property Balance7 As Decimal
    <RegularExpression("^-?[0-9]+(\.[0-9]{1,5})?$")>
    Public Property Balance8 As Decimal
    <RegularExpression("^-?[0-9]+(\.[0-9]{1,5})?$")>
    Public Property Balance9 As Decimal
    <RegularExpression("^-?[0-9]+(\.[0-9]{1,5})?$")>
    Public Property Balance10 As Decimal
    <RegularExpression("^-?[0-9]+(\.[0-9]{1,5})?$")>
    Public Property Balance11 As Decimal
    <RegularExpression("^-?[0-9]+(\.[0-9]{1,5})?$")>
    Public Property Balance12 As Decimal
    Public Property AccountId As Integer ' FK Id for the Account
    Public Overridable Property Account() As Account

End Class
