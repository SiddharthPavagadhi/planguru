﻿Imports System.ComponentModel.DataAnnotations
Imports System.Data.Entity


Public Class Analysis   
    Public Property AnalysisId As Integer

    <Required(ErrorMessage:="Analysis Name is required.")> _
    <MaxLength(50)> _
    <Display(Name:="Analysis Name")> _
    Public Property AnalysisName() As String

    <Required(ErrorMessage:="Select Company")> _
    <Display(Name:="Company")> _
    Public Property CompanyId() As Integer

    <NotMapped()>
    Public Property AvailableUsers() As IList(Of User_)

    <NotMapped()>
    Public Property SelectedUsers() As IList(Of User_)

    <NotMapped()> _
    Public Property PostedUsers() As PostedUsers

    Public Property CreatedBy() As Integer

    Public Property CreatedOn() As DateTime = DateTime.Now()

    Public Property UpdatedBy() As Integer

    Public Property UpdatedOn() As DateTime = DateTime.Now()

    Public Overridable Property Company() As Company
End Class


