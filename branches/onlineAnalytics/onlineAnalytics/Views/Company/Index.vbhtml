﻿@ModelType  PagedList.IPagedList(Of onlineAnalytics.Company)
@Code
    ViewData("Title") = "View Companies"
    Layout = "~/Views/Shared/AnalyticsMaster.vbhtml"
End Code
<h2>
    Company</h2>
<br />
<div style="float: right">
    @Html.ActionLink("Add New Company", "Create")
</div>
<br />
<fieldset>
    <legend>View Company List</legend>
    <br />
    <table class="table-layout">
        <tr>
            <th class="left" style="width: 18%">
                Company Name
            </th>
            <th class="left" style="width: 18%">
                Contact Name
            </th>
            <th class="left" style="width: 15%">
                Contact Email
            </th>
            <th class="center" style="width: 15%">
                Contact No.
            </th>
            <th class="center" style="width: 12%">
                Fis. Mth Start
            </th>
            <th class="center" style="width:5%">
                Edit
            </th>
            <th class="center" style="width:5%">
                Delete
            </th>
             <th class="center" style="width:12%">
                Add Analysis
            </th>
        </tr>
        @For Each item In Model
            Dim currentItem = item
            @<tr>
                <td class="left">
                    @Html.DisplayFor(Function(modelItem) currentItem.CompanyName)
                </td>
                <td class="left">
                    @Html.DisplayFor(Function(modelItem) currentItem.ContactLastName)
                    @Html.DisplayFor(Function(modelItem) currentItem.ContactFirstName)
                </td>
                <td class="left">
                    @Html.DisplayFor(Function(modelItem) currentItem.ContactEmail)
                </td>
                <td class="center">
                    @Html.DisplayFor(Function(modelItem) currentItem.ContactTelephone)
                </td>
                <td>
                    @Html.DisplayFor(Function(modelItem) currentItem.FiscalMonthName)
                </td>
                <td class="center">
                    @Html.ActionLink("Edit", "Edit", New With {.id = currentItem.CompanyId})
                </td>
                <td class="center">
                    <a id=@currentItem.CompanyId class="Delete" href="#" data='@Url.Action("Delete", "Company", New With {.id = currentItem.CompanyId})' >
                        Delete</a>
                </td>
                <td class="center">
                    <a id=@currentItem.CompanyId  href='@Url.Action("Create", "Analysis", New With {.SelectedCompany = currentItem.CompanyId}) ' >
                        Add Analysis</a>
                </td>
            </tr>
        Next
    </table>    
    @If Not (DirectCast(TempData("Message"), String) Is Nothing) Then
        @<label class="success">@TempData("Message").ToString()
        </label>                         
    End If
    @If Not (DirectCast(TempData("ErrorMessage"), String) Is Nothing) Then
        @<label class="error">@TempData("ErrorMessage").ToString()
        </label>                         
    End If
</fieldset>
<script type="text/javascript" language="javascript">

    $(".Delete").click(function (event) {
        event.preventDefault();
        var url = $(this).attr('data');

        $.msgBox({
            title: "Confirm",
            content: "Are you sure you want to delete this company?",
            type: "confirm",
            buttons: [{ value: "Yes" }, { value: "No"}],
            success: function (result) {
                if (result == "Yes") {
                    window.location = url;
                }
            }
        });

    });

    $(document).ready(function () {

    });
</script>
