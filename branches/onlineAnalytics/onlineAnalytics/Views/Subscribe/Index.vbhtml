﻿@ModelType  PagedList.IPagedList(Of onlineAnalytics.Customer)
@Code
    ViewData("Title") = "Index"
    Layout = "~/Views/Shared/AnalyticsMaster.vbhtml"
End Code
<h2>Subscribe</h2>
<br />
<div style="float: right">
    @Html.ActionLink("Create Subscription", "Subscribe")
</div>
<br />
<fieldset>
    <legend>View Subscriber List</legend>
    <br />
    <table class="table-layout">
        <tr>
            <th class="left" style="width: 20%">
                Customer Name
            </th>        
            <th class="left" style="width: 20%">
                Email Id
            </th>
            <th class="left" style="width: 20%">
                Company 
            </th>                          
             <th class="center" >
                Plan
            </th>
            <th class="right">
                Quantity
            </th>
            <th class="center" style="width: 15%">
                Created On
            </th>
            <th class="center" style="width:5%">
                Edit
            </th>
            <th class="center" style="width:5%">
                Cancel
            </th>             
        </tr>
        @For Each item In Model
            Dim currentItem = item
            @<tr>
                <td class="left">
                    @Html.DisplayFor(Function(modelItem) currentItem.CustomerFullName)
                </td>
                <td class="left">
                    @Html.DisplayFor(Function(modelItem) currentItem.CustomerEmail)                    
                </td>
                <td class="left">
                    @Html.DisplayFor(Function(modelItem) currentItem.CustomerCompanyName)        
                </td>                            
                 <td>
                    PlanGuru Analytics
                </td>
                 <td class="right">
                    @Html.DisplayFor(Function(modelItem) currentItem.Quantity)
                </td>
                 <td>
                    @Code
                    Dim createdon As String = String.Format("{0:MMM dd, yyyy}", currentItem.CreatedOn)
                    End Code
                    @Html.DisplayFor(Function(modelItem) createdon)
                </td>
                <td class="center">
                    @Html.ActionLink("Edit", "EditSubscription", New With {.AccountCode = currentItem.CustomerId})
                </td>
                <td class="center">
                    <a id=@currentItem.CustomerId class="Delete" href="#" data='@Url.Action("CancelSubscription", "Subscribe", New With {.AccountCode = currentItem.CustomerId})' >
                        Cancel</a>
                </td>               
            </tr>
        Next
    </table>    
    @If Not (DirectCast(TempData("Message"), String) Is Nothing) Then
        @<label class="success">@TempData("Message").ToString()
        </label>                         
    End If
    @If Not (DirectCast(TempData("ErrorMessage"), String) Is Nothing) Then
        @<label class="error">@TempData("ErrorMessage").ToString()
        </label>                         
    End If
</fieldset>
<script type="text/javascript" language="javascript">

    $(".Delete").click(function (event) {
        event.preventDefault();
        var url = $(this).attr('data');

        $.msgBox({
            title: "Confirm",
            content: "Are you sure you want to cancel this Subscription?",
            type: "confirm",
            buttons: [{ value: "Yes" }, { value: "No"}],
            success: function (result) {
                if (result == "Yes") {
                    window.location = url;
                }
            }
        });

    }); 
</script>