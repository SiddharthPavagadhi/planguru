﻿@ModelType onlineAnalytics.Customer
@Code
    ViewData("Title") = "Receipt"
    Layout = "~/Views/Shared/AnalyticsMaster.vbhtml"
End Code
<header>    
<link rel="stylesheet" href="@Url.Content("~/themes/default/recurly.css")" type="text/css" />
    <link rel="stylesheet" href="@Url.Content("~/themes/default/examples.css")" type="text/css" />
    </header>
<h2>
    Subscription</h2>
<div id="billing-info" class="section">
    <div id="recurly-subscribe" >
        <h2 class="heading">
            Signup Confirmation</h2>
        <div style="width:90%;margin:0px 40px;"><br />
            <p>Thank you for your PlanGuru Analytics trial signup.  Shortly you’ll be receiving a signup confirmation email.  In addition, you receive your temporary password which will enable you to log into PlanGuru Analytics in the future.</p><br />  
            <p>The next step is to add a company.  Like the PlanGuru budgeting and forecasting software, you can add as many companies as you want at no additional cost.  Plus, within each company you can have as many analyses, department or divisions as you need.</p><br />
            <p>So let’s get started:  @Html.ActionLink("Add a new company", "Create","Company")</p><br /><br />
        </div>
        <!-- table: billing info -->
        <!--table style="width: 100%;">
            <tbody>
                <tr>
                    <th scope="row">
                        Subscription Plan:
                    </th>
                    <td>
                        PlanGuru Analytics
                    </td>
                </tr>
                <tr>
                    <th scope="row">
                        First Name:
                    </th>
                    <td>
                       Pratik @*@Model.CustomerFirstName*@
                    </td>
                </tr>
                <tr>
                    <th scope="row">
                        Last Name:
                    </th>
                    <td>
                       Panchal @*@Model.CustomerLastName*@
                    </td>
                </tr>
                <tr>
                    <th scope="row">
                        Email Id:
                    </th>
                    <td>
                      pratik4you@gmail.com @* @Model.CustomerEmail *@
                    </td>
                </tr>
                <tr>
                    <th scope="row">
                        Credit Card:
                    </th>
                    <td>
                       XXXX-XXXX-XXXX-1234@*@ViewBag.CreditCardLastFourDigit (@ViewBag.CreditCardType)*@
                    </td>
                </tr>
            </tbody>
        </table-->
        
        <!--p style="margin:10px; padding:10px;">
            <a class="button " href="/account/billing_info/edit">Update Billing Information
            </a>
        </p-->
    </div>
    @code
        TempData("Message") = "Subscription created successfully."
    End Code    
    <div style="float: left; margin-left: 20px;">
        @If Not (DirectCast(TempData("Message"), String) Is Nothing) Then
            @<label class="success">@TempData("Message").ToString()
            </label>                         
    End If
        @If Not (DirectCast(TempData("ErrorMessage"), String) Is Nothing) Then
            @<label class="error">@TempData("ErrorMessage").ToString()
            </label>                         
    End If
</div>
