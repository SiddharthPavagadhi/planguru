﻿<!DOCTYPE html>

<html>
<head>
    <meta charset="utf-8" />
    <title>@ViewData("Title")</title>
    <link href="@Url.Content("~/Content/Site.css")" rel="stylesheet" type="text/css" />
    <!--script src="@Url.Content("~/Scripts/jquery-1.5.1.min.js")" type="text/javascript"></script-->
    <script src="@Url.Content("~/Scripts/modernizr-1.7.min.js")" type="text/javascript"></script>
    <link href="@Url.Content("~/Content/jquery-ui-1.10.0.custom.css")" rel="stylesheet" type="text/css" />

    <script src="@Url.Content("~/Scripts/jquery-1.9.0.js")" type="text/javascript"></script>
    <script src="@Url.Content("~/Scripts/jquery-ui-1.10.0.custom.js")" type="text/javascript"></script>

   @* <script src="@Url.Content("https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js")" type="text/javascript"></script>    
    <script src="@Url.Content("https://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js")" type="text/javascript"></script>    *@
</head>
<body>
        <div id="header" >
             <img src="@Url.Content("~/Content/Images/logo-planguru-analytics.png")" alt="Analytics logo" />   
        </div>
        <div style = "font-size: 11px; margin-top:20px; margin-bottom:0px;  float: right;">
            User ID: @Html.TextBox("userid","" , New With {.Style = "width: 100px;  border-radius: 5px; "})
            Password: @Html.TextBox("password", "", New With {.type = "password", .Style = "width: 100px;  border-radius: 5px; "})
            <input type="button"  value = "Login"   id="login" style = "background-color: #FFF;
                    height: 22px;  
		            border: 1px solid #DDD;
		            color: #2c8eb9;
		            border-radius: 5px;" />
            @*<input type="button"  value = "Sign Up"   id="signup" style = "background-color: #FFF;
                    height: 22px;  
		            border: 1px solid #DDD;
		            color: #2c8eb9;
		            border-radius: 5px;" /> *@
        </div>
        <div id="menucontainer" style = "font-size: 11px; margin-top:50px; margin-bottom:0px;"> 
            <ul id="menu">
                <li>@Html.ActionLink("How it works", "", "")</li>
                <li>@Html.ActionLink("Sample Dashboard", "Index", "Dashboard")</li>
                <li>@Html.ActionLink("Sample Revenue and Expenses View", "Index", "RE")</li>
                <li>@Html.ActionLink("Pricing", "", "")</li>     
                <li>@Html.ActionLink("Sign Up", "Subscribe", "Subscribe")</li>               
            </ul>    
         </div>     
         <br /> 
    <div>
        @RenderBody()
    </div>
    <div id="loading" style="text-align:center;"></div>
    <div id="validation" style="text-align:center;"></div>
</body>
</html>
<script type="text/javascript" language="javascript">

    var theDialog = $('#loading').dialog({
        open: function (event, ui) { $(".ui-dialog-titlebar-close").hide(); },        
        center_modal: true,
        autoOpen: false,
        modal: true,
        close: false,
        closeOnEscape: false,
        width: 200,
        modal: true,
        minHeight: 10,
        show: 'fade',
        hide: 'fade',
        resizable: 'false',
        draggable: false
    });

    var validationDialog = $('#validation').dialog({
        title: "Login",
        open: function (event, ui) { $(".ui-dialog-titlebar-close").hide(); },
        center_modal: true,
        autoOpen: false,
        modal: true,
        close: true,
        closeOnEscape: true,
        width: 500,
        modal: true,
        minHeight: 10,
        show: 'fade',
        hide: 'fade',
        resizable: 'false',
        draggable: false,
        buttons: { "OK": function () {
            $(this).dialog("close");
                } 
        }
    });


    $(document).ready(function (event) {

        $('#login').click(function (event) {

            event.preventDefault();

            if ($('#userid').val() == "") {

                $('#validation').html("Enter your Username.");
                validationDialog.dialog('open');
            }
            else if ($('#password').val() == "") {

                $(".ui-dialog-titlebar").show();
                $('#validation').html("Enter your Password.");
                validationDialog.dialog('open');
            }
            else if ($('#userid').val() != "" && $('#password').val() != "") {


                $.ajax({
                    type: "GET",
                    url: '@url.Action("Login","User")',
                    global: false,
                    cache: false,
                    data: { userName: $('#userid').val(), password: $('#password').val(), returnUrl: "http://localhost:51542/" },
                    dataType: "json",
                    beforeSend: function () {

                        $(".ui-dialog-titlebar").hide();
                        $('#loading').html("Please wait.... <img src='../themes/default/images/loading.gif' />");
                        theDialog.dialog('open');
                    },
                    success: function (msg) {

                        theDialog.dialog('close');
                        if (msg.Success) {

                            //alert("Success");
                            //alert(msg.Url);
                            window.location = msg.Url;
                        }

                        if (msg.Error) {

                            $(".ui-dialog-titlebar").show();
                            $('#validation').html(msg.Error);
                            validationDialog.dialog('open');
                        }



                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        var msg = JSON.parse(XMLHttpRequest.responseText);
                        theDialog.dialog('close');
                        //alert(msg.Message);     

                        $(".ui-dialog-titlebar").show();
                        $(".ui-dialog-titlebar").text("Login");
                        $('#validation').html(msg);
                        validationDialog.dialog('open');
                    }
                });
            }


        });
    });
</script>