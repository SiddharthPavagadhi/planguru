﻿@Imports  onlineAnalytics
<!DOCTYPE html>

<html>
<head>
    <title>@ViewData("Title")</title>
    <link href="@Url.Content("~/Content/Site.css")" rel="stylesheet" type="text/css" />
  @* <script src="@Url.Content("~/Scripts/jquery-1.5.1.js")" type="text/javascript"></script>*@
    <link href="@Url.Content("~/Content/jquery-ui-1.10.0.custom.css")" rel="stylesheet" type="text/css" />

    <script src="@Url.Content("~/Scripts/jquery-1.9.0.js")" type="text/javascript"></script>
    <script src="@Url.Content("~/Scripts/jquery-ui-1.10.0.custom.js")" type="text/javascript"></script>

    <!--script src="@Url.Content("https://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js")" type="text/javascript"></script>
    <!--script src="@Url.Content("http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.0/jquery-ui.min.js")" type="text/javascript"></script-->

    <script src="@Url.Content("~/Scripts/jquery.maskedinput.min.js")" type="text/javascript"></script>
    @*<script src="@Url.Content("~/Scripts/jquery-ui.min.js")" type="text/javascript"></script>*@

    <script src="@Url.Content("~/Scripts/jquery.msgBox.js")" type="text/javascript"></script>

    <link href="@Url.Content("~/Content/msgBoxLight.css")" rel="stylesheet" type="text/css" />

    
    	<!-- internal styles -->
	<style>

	* {

		margin: 0px;
		padding: 0px;

		}
        
		
	body {
		font-family: arial;
		background-color: #FFF;
		}
	

	/* rules for navigation menu */

	/* ========================================== */


	ul#navmenu, ul.sub1, ul.sub2 {
        list-style-type: none;
		font-size: 12px;
        display: block;
		}

		

	ul#navmenu li {
		width: auto;
		text-align: center;
		position: relative;
		float: left;
		margin-right: 4px;
		}

		
	ul#navmenu a {
		text-decoration: none;		
		display: block;
		width: 160px;
		height: 20px;
		line-height: 20px;
		background-color: #FFF;
		border: 1px solid #DDD;
		border-radius: 5px;
		color: #2c8eb9;

		}

		

	ul#navmenu .sub1 li {
		font-size: 11px;
		
		}

		

	ul#navmenu .sub1 a {

		margin-top: 0px;

		}

		

	ul#navmenu .sub2 a {
        position: relative; 
		margin-left: 35px;

		}    

	ul#navmenu li:hover > a {

		background-color: #33aadf;
		color:#FFF;

		}

		

	ul#navmenu li:hover a:hover {

		background-color: #33aadf;

		}

		

	ul#navmenu ul.sub1 {

		display: none;
		position: absolute; 
		left: 0px; 
		}

		

	ul#navmenu ul.sub2 {

		display: none;
		position: absolute; 
		top: 0px;
		left: 126px;

		}

		

	ul#navmenu li:hover .sub1 {

		display: block;

		}

		

	ul#navmenu .sub1 li:hover .sub2 {

		display: block;

		}

		

	.darrow {

		font-size: 9px;
		margin-top: 2px;
		position: absolute;
		color:#CCC;
		top: 5px;
		right: 4px;
        
		}

		

	.rarrow {

		font-size: 13pt;
		position: absolute;
		top: 6px;
		right: 4px;
		}

	</style>
    <!-- end internal styles -->

</head>
<body>
        @*<div id="header">
             <img src="/Content/images/logo-planguru-analytics.png" alt="Analytics logo" />
             <input type="button"  value = "Log Out"   id="logout" style = "font-size: 11px; margin-top:10px; margin-bottom:0px;  float: right;" />                              
        </div>*@
        @Code
            Dim UserInfo As User
            Dim UserName As String = String.Empty
            Dim ua As New UserAccess
            If Not (Session("UserInfo") Is Nothing) Then
                UserInfo = DirectCast(Session("UserInfo"), User)
                UserName = String.Concat("Welcome <b>", UserInfo.LastName, " ", UserInfo.FirstName,"</b>")
            End If
            
            If Not (Session("UserAccess") Is Nothing) Then
                
                ua = DirectCast(Session("UserAccess"), UserAccess)
                
            End If
        End code
                
        <div id="header" style="height: 55px; position:relative; font-size: 11px; ">        
                                           
             <div style="float:left; position: relative; z-index:3; ">                                   
               <div class="username">@MvcHtmlString.Create(UserName)</div>
                <ul id="navmenu" style="padding-top: 10px;">                   
				    <li><a href="#">File</a><span class="darrow">&#9660;</span>
                        <ul class="sub1" >                            
                            <li>@Html.ActionLink("Open A Company", "Index", "")</li>
                            <li><a href="#">Work on Companies</a><span class="rarrow">&#9654;</span>
                                <ul class="sub2">
                                    <li style="display:@ua.AddCompany" >@Html.ActionLink("Add a New Company", "Create", "Company")</li>
                                    <li style="display:@ua.UpdateCompany">@Html.ActionLink("Edit Current Company", "Index", "")</li>
                                    <li style="display:@ua.DeleteCompany">@Html.ActionLink("Delete Current Company", "Index", "")</li>
                                    <li style="display:@ua.ViewCompanies">@Html.ActionLink("A/C/D Company Users", "Index", "")</li>
                                </ul>
                            </li>
                            <li id="workonanal"><a href="#">Work on Analyses</a><span class="rarrow">&#9654;</span>
                                <ul class="sub2">
                                    <li style="display:@ua.ViewAnalyses">@Html.ActionLink("View Analyses", "Index", "")</li>
                                    <li style="display:@ua.AddAnalysis">@Html.ActionLink("Add New Analysis", "Create", "Analysis")</li>
                                    <li style="display:@ua.UpdateAnalysis">@Html.ActionLink("Edit Analysis", "Index", "")</li>
                                    <li style="display:@ua.DeleteAnalysis">@Html.ActionLink("Delete Current Analysis", "Index", "")</li>
                                    <li style="display:@ua.ViewAnalyses">@Html.ActionLink("A/C/D Analysis Users", "Index", "")</li>
                                    <li style="display:@ua.ViewAnalysisInfo">@Html.ActionLink("View AnalysisInfo", "Index", "")</li>
                                </ul>
                            </li>   
                        </ul>
                    </li>
				   
                    <li style=""><a href="#">Manage Account</a><span class="darrow">&#9660;</span>
                        <ul class="sub1">
                            <li id="manageusers"><a href="#">Manage Users</a><span class="rarrow">&#9654;</span>
                                <ul class="sub2">
                                    <li style="display:@ua.ViewUser">@Html.ActionLink("View Users", "Index", "User")</li>
                                    <li style="display:@ua.AddUser">@Html.ActionLink("Add a New User", "CreateUser", "User")</li>
                                    <li style="display:@ua.DeleteUser">@Html.ActionLink("Delete a User", "Index", "")</li>
                                    <li style="display:@ua.UpdateUser">@Html.ActionLink("Edit User", "Index", "")</li>
                                </ul>
                            </li>                               
                            <li style="display:@ua.ViewSubscription">@Html.ActionLink("View Subscription", "Index", "Subscribe")</li>
                            <li style="display:@ua.UpdateSubscription">@Html.ActionLink("Edit Subscription", "Index", "")</li>
                            <li style="display:@ua.CancelSubscription">@Html.ActionLink("Cancel Subscription", "Index", "")</li>
                            <li style="display:@ua.SearchCustomer">@Html.ActionLink("Search Customer", "SearchCustomer", "Subscribe")</li>
                        </ul>
                    </li>
                    <li style="">@Html.ActionLink("Logout", "Index", "Home")@*<span  >&#9660;</span>*@
                        
                    </li>
                    
                </ul>
               @* <input type="button"  value = "Log Out"   id="logout" style = "font-size: 12px; margin-top:40px;height: 22px; width: 75px; float:right; background-color: #FFF;
		            border: 1px solid #DDD;
		            color: #2c8eb9;
		            border-radius: 5px; " />  *@
            </div>
            <div >            
                <img src="@Url.Content("~/Content/Images/logo-planguru-analytics.png")" alt="Analytics logo" style="float:right;" />                 
            </div> 
            
        </div>                               
                       
       @*<div id="menucontainer" style="position: relative; z-index:2;"  > 
           <ul id="menu">
                    <li>@Html.ActionLink("Dashboard", "Index", "Dashboard")</li>
                    <li>@Html.ActionLink("Revenue and Expenses", "Index", "RE")</li>
                    <li>@Html.ActionLink("Assets, Liabilities and Equity", "Index", "")</li>
                    <li>@Html.ActionLink("Cash Flow", "Index", "")</li>
                    <li>@Html.ActionLink("Financial Ratios", "Index", "")</li> 
                    <li>@Html.ActionLink("Other Metrics", "Index", "")</li>
             </ul>    
         </div>*@                 
    <div >               
            @RenderBody()
    </div>
</body>
</html>
