﻿@Imports Farpoint.Web.Spread
@Imports Farpoint.Mvc.Spread

@Code
    ViewData("Title") = "Index"
    Layout = "~/Views/Shared/AnalyticsMaster.vbhtml"
End Code

 <script type="text/javascript">
     $(function () {
         $("#updatesetup")               
            .button()
            .click(function (event) { 
                document.location = '@Url.Action("Index", "DashboardSetup")'; 
            });
     });
       
</script>

 <div class= "dropdown" style="display:inline-block;">
       Analysis
       @Html.DropDownList("intDept", DirectCast(ViewData("Dept"), SelectList), New With {.Style = "width: 175px;font-size: 11px; margin-right: 50px"})
       Period
        @Html.DropDownList("intPeriod", DirectCast(ViewData("Periods"), SelectList), New With {.Style = "width: 175px;font-size: 11px;"})  
       <input type="button"  value = "Change Dashboard Setup" style = "font-size: 11px;"  id="updatesetup" />
 </div>
 <div id="spread" style="padding-top: 10px;" >
       @Html.FpSpread("spdDashboard", Sub(x)
                                          x.RowHeader.Visible = False
                                          x.ActiveSheetView.PageSize = 1000
                                      End Sub)
 </div>    
