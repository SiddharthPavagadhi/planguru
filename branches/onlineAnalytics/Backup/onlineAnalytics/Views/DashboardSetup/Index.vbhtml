﻿@Imports Farpoint.Web.Spread
@Imports Farpoint.Mvc.Spread

@Code
    ViewData("Title") = "Index"
    Layout = "~/Views/Shared/AnalyticsMaster.vbhtml"
End Code

  <script type="text/javascript">
     
    
      $(function () {
          $("#tabs").tabs();

          $('#intChartType').change(function () {
              var selectedindex = $('#intChartType').get(0).selectedIndex;
              if (selectedindex == 2) {
                  $('#ShowTrend').show();
                  $('#Smoothed').show();
              }
              else {
                  $('#ShowTrend').hide();
                  $('#Smoothed').hide();
              }
          }); 

          $(document).ready(function () {
              //hide detail rows  
              var spread = document.getElementById("spdRE");
              var rc = spread.GetRowCount();
              var showclass = false
              for (var i = 0; i < rc; i++) {
                  var cellval = spread.GetValue(i, 2);
                  if (cellval == "D") {
                      if (showclass == false) {
                          spread.Rows(i).style.display = "none";
                      }
                  }
                  if (cellval == "H") {
                      var cellvalue = spread.GetValue(i, 0);
                      if (cellvalue == "-") {
                          showclass = true
                          spread.Rows(i).style.display = "table-row";
                      }
                      else {
                          showclass = false
                          spread.Rows(i).style.display = "none";
                      }
                  }
              }

          });

          window.onload = function () {
              var spread = document.getElementById("spdRE");
              if (document.all) {
                  if (spread.addEventListener) {
                      spread.addEventListener("ActiveCellChanged", cellChanged, false);
                  }
                  else {
                      spread.onActiveCellChanged = cellChanged;
                  }
              }
              else {
                  spread.addEventListener("ActiveCellChanged", cellChanged, false);
              }
          }

          function cellChanged(event) {
              if (event.col == 0) {
                  var spread = document.getElementById("spdRE");
                  var cellval = spread.GetValue(event.row, 0);
                  var rc = spread.GetRowCount();
                  if (cellval == "-") {
                      for (var i = event.row; i < rc; i++) {
                          var rowtype = spread.GetValue(i, 2);
                          if (rowtype == "T") {
                              cell = document.getElementById("spdRE").GetCellByRowCol(i, 0);
                              cell.removeAttribute("FpCellType");
                              spread.SetValue(i, 0, "+", false);
                              cell.setAttribute("FpCellType", "readonly");
                              spread.Rows(i).style.display = "table-row";
                              break;
                          }
                          else if (rowtype == "H") {
                              cell = document.getElementById("spdRE").GetCellByRowCol(i, 0);
                              cell.removeAttribute("FpCellType");
                              spread.SetValue(i, 0, "+", false);
                              cell.setAttribute("FpCellType", "readonly");
                              spread.Rows(i).style.display = "none";
                          }
                          else {

                              spread.Rows(i).style.display = "none";
                          }
                      }
                  }
                  else if (cellval == "+") {
                      for (var i = event.row; i > -1; i--) {
                          var rowtype = spread.GetValue(i, 2);
                          if (rowtype == "T") { 
                              cell = document.getElementById("spdRE").GetCellByRowCol(i, 0);
                              cell.removeAttribute("FpCellType");
                              spread.SetValue(i, 0, "", false);
                              cell.setAttribute("FpCellType", "readonly");
                              if ($('#blnUseFilter').is(':checked')) {
                                  spread.Rows(i).style.display = "none";
                              }
                          }
                          else if (rowtype == "H") {
                              cell = document.getElementById("spdRE").GetCellByRowCol(i, 0);
                              cell.removeAttribute("FpCellType");
                              spread.SetValue(i, 0, "-", false);
                              cell.setAttribute("FpCellType", "readonly");
                              spread.Rows(i).style.display = "table-row";
                              break
                          }
                          
                          else {
                              spread.Rows(i).style.display = "table-row";
                          }
                      }
                  }
                  spread.SetActiveCell(event.row, 1)
              }
          }

      });

     
</script>


<div style = "font-size: 11px;">
    Select Dashboard Item
</div>
<div id="tabs" style = "font-size: 11px; width: 400px;">
    <ul>
        <li><a href="#tabs-1">Revenue and expenses</a></li>
        <li><a href="#tabs-2">Assets, liabilties and capital</a></li>
        <li><a href="#tabs-3">Cash flow</a></li>
        <li><a href="#tabs-3">Financial ratios</a></li>
        <li><a href="#tabs-3">Other metrics</a></li>
    </ul>
    <div id="tabs-1">
       <div id="REspread" >
                  @Html.FpSpread("spdRE", Sub(x)
                                              x.RowHeader.Visible = False
                                              x.Height = 300
                                              x.Width = 360
                                              
                                              
                                          End Sub)
        </div>
    </div>
    <div id="tabs-2">
      <div id="BSspread" >
                  @Html.FpSpread("spdBS", Sub(x)
                                              x.RowHeader.Visible = False
                                              x.Height = 300
                                              x.Width = 350
                                              
                                          End Sub)
        </div>
    </div>
   
</div>
<div style = "font-size:11px;">
<div style="margin-top: 10px;">
     @Html.Label("Format") 
     @Html.DropDownList("intFormat", DirectCast(ViewData("Formats"), SelectList), New With {.Style = "width: 175px;font-size: 11px; padding-top:2px; border-radius: 5px; border: 1px solid #DDD; color: #2c8eb9; "})  
</div>
<div style = "padding-top:10px;">
     @Html.Label("Chart type") 
     @Html.DropDownList("intChartType", DirectCast(ViewData("ChartType"), SelectList), New With {.Style = "width: 175px;font-size: 11px; padding-top:2px; border-radius: 5px; border: 1px solid #DDD; color: #2c8eb9; "})  
</div>
<div id = "ShowTrend" style="margin-top: 10px; margin-left:10px; display: none;">
     @Html.CheckBox("blnShowTrend", New With {.Style = "border: 1px solid Silver; background-color:Silver; margin-right: 5px;"}) 
     @Html.Label("Show trend") 
</div>
<div id = "Smoothed" style="margin-top: 10px; margin-left:10px; display: none;">
     @Html.CheckBox("blnSmoothed", New With {.Style = "border: 1px solid Silver; background-color:Silver; margin-right: 5px;"}) 
      @Html.Label("Smooth line") 
</div>
</div>
</br>
<div id = "AddDashboardItem" class = "button"  >
     <input id="causepost" type="submit"  value="Add Dashboard Item" />
</div>  
 

