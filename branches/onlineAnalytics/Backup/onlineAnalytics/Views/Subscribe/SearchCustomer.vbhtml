﻿@ModelType PagedList.IPagedList(Of onlineAnalytics.Customer)
@Code
    ViewData("Title") = "Search Customer"
    Layout = "~/Views/Shared/AnalyticsMaster.vbhtml"
End Code

<h2>
    Search Customer</h2>
<br />
@*Search Criteria*@
@Using Html.BeginForm("SearchCustomer", "Subscribe")
    @Html.AntiForgeryToken()   
    @Html.ValidationSummary(True)    
     
    @<fieldset>
            <legend>Search Options & Result</legend>
        @Html.Partial("_SearchCustomerPartial", New onlineAnalytics.Customer)
        <table class="table-layout">
        <tr>
            <th class="left" style="width: 20%">
                Customer No
            </th>
            <th class="left" style="width: 20%">
                Customer Full Name
            </th>
            <th class="left" style="width: 20%">
                Telephone Number
            </th>
            <th class="center" style="width: 15%">
                Company Name
            </th>         
                      
        </tr>
        @For Each item In Model
            Dim currentItem = item
            @<tr>
                <td class="left">
                    @Html.DisplayFor(Function(modelItem) currentItem.CustomerId)
                </td>
                <td class="left">
                    @Html.DisplayFor(Function(modelItem) currentItem.CustomerFullName)
                </td>
                <td class="left">
                    @Html.DisplayFor(Function(modelItem) currentItem.ContactTelephone)
                </td>
                <td class="center">
                    @Html.DisplayFor(Function(modelItem) currentItem.CustomerCompanyName)
                </td>               
                             
            </tr>
        Next
    </table>    
   @* @If Not (DirectCast(TempData("Message"), String) Is Nothing) Then
        @<label class="success">@TempData("Message").ToString()
        </label>                         
    End If
    @If Not (DirectCast(TempData("ErrorMessage"), String) Is Nothing) Then
        @<label class="error">@TempData("ErrorMessage").ToString()
        </label>                         
    End If*@
    </fieldset>                                  
End Using

@*View Result *@

@*<fieldset>
    <legend>Search Customer Result</legend>
    <br />*@
    @*<table class="table-layout">
        <tr>
            <th class="left" style="width: 20%">
                Customer No
            </th>
            <th class="left" style="width: 20%">
                Customer Full Name
            </th>
            <th class="left" style="width: 20%">
                Telephone Number
            </th>
            <th class="center" style="width: 15%">
                SAU Name
            </th>         
                      
        </tr>
        @For Each item In Model
            Dim currentItem = item
            @<tr>
                <td class="left">
                    @Html.DisplayFor(Function(modelItem) currentItem.CustomerId)
                </td>
                <td class="left">
                    @Html.DisplayFor(Function(modelItem) currentItem.CustomerFullName)
                </td>
                <td class="left">
                    @Html.DisplayFor(Function(modelItem) currentItem.ContactTelephone)
                </td>
                <td class="center">
                    @Html.DisplayFor(Function(modelItem) currentItem.SAUName)
                </td>               
                             
            </tr>
        Next
    </table>    
    @If Not (DirectCast(TempData("Message"), String) Is Nothing) Then
        @<label class="success">@TempData("Message").ToString()
        </label>                         
    End If
    @If Not (DirectCast(TempData("ErrorMessage"), String) Is Nothing) Then
        @<label class="error">@TempData("ErrorMessage").ToString()
        </label>                         
    End If*@
@*</fieldset>*@