﻿@ModelType onlineAnalytics.Customer
@Imports onlineAnalytics
@Imports System.Collections.Generic
@Code
    ViewData("Title") = "Subscribe"
    Layout = "~/Views/Shared/AnalyticsMaster.vbhtml"
End Code
<h2>
    Subscribe</h2>
<header>    
<link rel="stylesheet" href="@Url.Content("~/themes/default/recurly.css")" type="text/css" />
    <link rel="stylesheet" href="@Url.Content("~/themes/default/examples.css")" type="text/css" />
    <script type="text/javascript" language=javascript src="@Url.Content("~/Recurly/recurly.js")"></script>
    </header>
<script>

    $(document).ready(function () {

        function SubscriptionResponse(response) {

            Recurly.postResult('@Url.Action("Receipt", "Subscribe")', response, null);
        }

        Recurly.config({
            subdomain: 'new-horizon-software-technologies-inc',
            currency: 'USD'
        });

        Recurly.buildSubscriptionForm({
            target: '#recurly-subscribe',
            // Signature must be generated server-side with a utility method provided in client libraries.
            signature: '@ViewBag.Signature'.replace(/&amp;/g, '&'),
            successHandler: SubscriptionResponse,
            //successURL: 'confirm.aspx',                
            planCode: 'planguru-analytics',
            distinguishContactFromBillingInfo: true,
            collectCompany: true,
            termsOfServiceURL: 'http://example.com/tos',
            acceptedCards: ['mastercard',
                    'discover',
                    'american_express',
                    'visa'],
            account: {
                accountCode: '@ViewBag.AccountCode',
                firstName: '',
                lastName: '',
                email: '',
                companyName: ''

            },
            billingInfo: {
                firstName: '',
                lastName: '',
                phone: '',
                address1: '',
                address2: '',
                city: '',
                zip: '',
                state: '',
                country: '',
                cardNumber: '4111-1111-1111-1111',
                CVV: '123'
            }
        });       
    });
</script>
<form id="Recurly-Subscription" runat="server" style="padding-bottom: 20px;">
<div id="recurly-subscribe">
</div>
</form>
