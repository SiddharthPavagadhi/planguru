﻿@Imports Farpoint.Web.Spread
@Imports Farpoint.Mvc.Spread

@Code
    ViewData("Title") = "Index"
    Layout = "~/Views/Shared/AnalyticsMaster.vbhtml"
End Code

<script type="text/javascript">
    $(function () {

        $('#blnHighVar').change(function () {
            if ($(this).is(':checked')) {
                $('#HighlightVarControls').show()
                $('#UseFilterControls').hide()
                $('#blnUseFilter').removeAttr('checked');
                RemoveFilter()
                AddHighlight()
            }
            else {
                $('#HighlightVarControls').hide()
                RemoveBackgroundColor()
                //                AddBackgroundColor()

            }
        });

        $('#blnHighPosVar').change(function () {
            //            if ($(this).is(':checked')) {                
            //                AddHighlight()
            //            }
            //            else { 
            //                RemoveBackgroundColor()
            //            }
            AddHighlight()
        })

        $('#blnHighNegVar').change(function () {
            AddHighlight()
        })

        $('#intHighPosVar').change(function () {
            AddHighlight()
        })

        $('#intHighNegVar').change(function () {
            AddHighlight()
        })

        function AddHighlight() {
            var spread = document.getElementById("spdAnalytics");
            if ($("#blnHighPosVar").is(':checked')) {
                var highposvar = "checked"
            }
            if ($("#blnHighNegVar").is(':checked')) {
                var highnegvar = "checked"
            }
            var posvaramt = parseFloat($("#intHighPosVar").val());
            var negvaramt = parseFloat($("#intHighNegVar").val());
            var rc = spread.GetRowCount();
            for (var i = 0; i < rc; i++) {
                var rowtype = spread.GetValue(i, 2);
                if (rowtype == "D") {
                    var value = parseFloat(spread.GetValue(i, 7).replace(',', ''));
                    if (value > posvaramt) {
                        if (highposvar == "checked") {

                            SetBackGroundColor(i, "Green")
                        }
                        else {
                            SetBackGroundColor(i, "Black")
                        }
                    }
                    else if (value < negvaramt) {
                        if (highnegvar == "checked") {
                            SetBackGroundColor(i, "Red")
                        }
                        else {
                            SetBackGroundColor(i, "Black")
                        }
                    }
                    else {

                        SetBackGroundColor(i, "Black")
                    }
                }
                else {
                    SetBackGroundColor(i, "Black")
                }
            }
        }


        $('#blnUseFilter').change(function () {
            if ($(this).is(':checked')) {
                $('#UseFilterControls').show()
                $('#HighlightVarControls').hide()
                $('#blnHighVar').removeAttr('checked')
                RemoveBackgroundColor()
                AddFilter()
            }
            else {
                $('#UseFilterControls').hide()
                RemoveFilter()
                //                AddBackgroundColor()
            }
        });

        $('#strFilterOn').change(function () {
            AddFilter()
        })

        $('#radioPosVar').change(function () {
            AddFilter()
        })

        $('#radioNegVar').change(function () {
            AddFilter()
        })

        $('#decFilterAmt').change(function () {
            AddFilter()
        })

        function AddFilter() {
            var spread = document.getElementById("spdAnalytics");
            var selindex = $('#strFilterOn').get(0).selectedIndex;
            var filteramt = $('#decFilterAmt').val();
            var selradio = $('#radioPosVar').attr('checked');
            var rc = spread.GetRowCount();
            var amount = parseFloat(filteramt);
            var showclass = false
            for (var i = 0; i < rc - 1; i++) {
                var rowtype = spread.GetValue(i, 2);
                if (rowtype == "D") {
                    if (showclass == false) {
                        spread.Rows(i).style.display = "none";
                    }
                    else {
                        var value = parseFloat(spread.GetValue(i, selindex + 5).replace(',', ''));
                        if (value < filteramt) {
                            if (selradio == "checked") {
                                spread.Rows(i).style.display = "none";
                            }
                            else {

                            }
                        }
                        else {
                            if (selradio == "checked") {
                                spread.Rows(i).style.display = "table-row";
                            }
                            else {
                                spread.Rows(i).style.display = "none";
                            }
                        }
                    }
                }
                else if (rowtype == "H") {
                    var cellvalue = spread.GetValue(i, 0);
                    if (cellvalue == "-") {
                        showclass = true
                    }
                    else {
                        showclass = false
                    }
                }
                else if (rowtype == "T") {
                    if (showclass == true) {
                        spread.Rows(i).style.display = "none";
                    }
                }
                else if (rowtype == "GP") {
                    spread.Rows(i).style.display = "none";
                }
                else if (rowtype == "IO") {
                    spread.Rows(i).style.display = "none";
                }
                else if (rowtype == "NI") {
                    spread.Rows(i).style.display = "none";
                }
                else {

                }
            }
        }

        function RemoveFilter() {
            var spread = document.getElementById("spdAnalytics");
            var rc = spread.GetRowCount();
            var showclass = false
            for (var i = 0; i < rc - 1; i++) {
                var rowtype = spread.GetValue(i, 2);
                if (rowtype == "D") {
                    if (showclass == false) {
                        spread.Rows(i).style.display = "none";
                    }
                    else {
                        spread.Rows(i).style.display = "table-row";
                    }
                }
                else if (rowtype == "H") {
                    var cellvalue = spread.GetValue(i, 0);
                    if (cellvalue == "-") {
                        showclass = true
                    }
                    else {
                        showclass = false
                    }
                }
                else {
                    spread.Rows(i).style.display = "table-row";
                }
            }
        }

        function AddBackgroundColor() {
            var spread = document.getElementById("spdAnalytics");
            var rc = spread.GetRowCount();
            var tf = false
            for (var i = 0; i < rc - 1; i++) {
                if (tf == false) {
                    SetBackGroundColor(i, "#fef5do")
                    spread.SetSelectedRange(i, 2, i, 7)
                    tf = true
                }
                else {
                    SetBackGroundColor(i, "White")
                    spread.SetSelectedRange(i, 2, i, 7)
                    tf = false
                }
            }
        }

        function RemoveBackgroundColor() {
            var spread = document.getElementById("spdAnalytics");
            var rc = spread.GetRowCount();
            var tf = false
            for (var i = 0; i < rc; i++) {
                SetBackGroundColor(i, "Black")
            }
        }

        function SetBackGroundColor(row, color) {
            var spread = document.getElementById("spdAnalytics");
            var colcount = spread.GetColCount();
            for (var i = 2; i < colcount; i++) {
                spread.Cells(row, i).style.color = color
            }
        }

        //        //charting functions
        $('#intPeriod').change(function () {
            $('#intChartType')[0].selectedIndex = 0;
            $('#causepost').trigger('click')
        })

        $('#intFormat').change(function () {
            $('#intChartType')[0].selectedIndex = 0;
            $('#causepost').trigger('click')
        })

        $('#blnShowasPer').change(function () {
            $('#intChartType')[0].selectedIndex = 0;
            $('#causepost').trigger('click')
        })

       $('#intChartType').change(function () {
            var selectedindex = $('#intChartType').get(0).selectedIndex;
            var spread = document.getElementById("spdAnalytics");
            var colcount = spread.GetColCount();
            if (selectedindex == 0) {
//                spread.SetColWidth(1, 0);
//                if (colcount > 8) {
//                    spread.SetColWidth(3, 172);
//                }
//                else {
//                    spread.SetColWidth(3, 272);
//                }
                $('#ShowTrend').hide();
            }
            else {
                spread.SetColWidth(1, 22);
                if (colcount > 8) {
                    spread.SetColWidth(3, 150);
                }
                else {
                    spread.SetColWidth(3, 250);
                }
                var rc = spread.GetRowCount();
                var count = 0;
                for (var i = 0; i < rc - 1; i++) {
                    var chkboxval = spread.GetValue(i, 1);
                    if (chkboxval == "true") {
                        count++;
                    }
                }
                if (count == 1) {
                    $('#UpdateChart').show();
                    if (selectedindex == 3) {
                        $('#ShowTrend').show();
                    }
                    else {
                        $('#ShowTrend').hide();
                    }
                }
                else if (count > 1) {
                    $('#UpdateChart').show();
                }
            }
        });

        //on load
        $(document).ready(function () {
            var selectedindex = $('#intChartType').get(0).selectedIndex;
            if (selectedindex > 0) { 
                $('#chart').show()
                $('#chart').css('height', '200px');
                $('#UpdateChart').show();
            }
            else {

            }
            $('#spread').show()
            
            if (selectedindex == 3) {
                $('#ShowTrend').show();
            }
            else {
                $('#ShowTrend').hide();
                $('#blnShowTrend').removeAttr('checked');
            }

            var selectedindex = $('#intFormat').get(0).selectedIndex;
            if (selectedindex > 4) {
                $('#optionsimage').hide()
                $('#chkHighlightVar').hide()
                $('#HighlightVarControls').hide()
                $('#CheckBoxFilter').hide()
            }
            else {
                $('#blnHighVar').removeAttr('checked')
                var selPercent = $('#blnShowasPer').attr('checked');
                if (selPercent == true) {
                    $('#CheckBoxFilter').hide()
                }
            }
            //adjust width
            var browsewidth = $(window).width();
            $('#intBrowserWidth').val(browsewidth)
            var spdWidth = $('#spdAnalytics').width();
            var calcwidth = spdWidth + 250;
            if (calcwidth > 960) {
                if (calcwidth < browsewidth) {
                    $('#main').css('padding-left', '20px');
                    $('body').css('width', calcwidth);
                }
                else {
                    $('body').css('width', browsewidth);
                }
            }
            //hide detail rows  
            var spread = document.getElementById("spdAnalytics");
            var rc = spread.GetRowCount();
            var showclass = false
            for (var i = 0; i < rc; i++) {
                var cellval = spread.GetValue(i, 2);
                if (cellval == "D") {
                    if (showclass == false) {
                        spread.Rows(i).style.display = "none";
                    }
                }
                if (cellval == "H") {
                    var cellvalue = spread.GetValue(i, 0);
                    if (cellvalue == "-") {
                        showclass = true
                        spread.Rows(i).style.display = "table-row";
                    }
                    else {
                        showclass = false
                        spread.Rows(i).style.display = "none";
                    }
                }
            }

        });

        window.onload = function () {
            var spread = document.getElementById("spdAnalytics");
            if (document.all) {
                if (spread.addEventListener) {
                    spread.addEventListener("ActiveCellChanged", cellChanged, false);
                }
                else {
                    spread.onActiveCellChanged = cellChanged;
                }
            }
            else {
                spread.addEventListener("ActiveCellChanged", cellChanged, false);
            }
        }

        function cellChanged(event) {
            if (event.col == 0) {
                var spread = document.getElementById("spdAnalytics");
                var cellval = spread.GetValue(event.row, 0);
                var rc = spread.GetRowCount();
                if (cellval == "-") {
                    for (var i = event.row; i < rc; i++) {
                        var rowtype = spread.GetValue(i, 2);
                        if (rowtype == "T") {
                            cell = document.getElementById("spdAnalytics").GetCellByRowCol(i, 0);
                            cell.removeAttribute("FpCellType");
                            spread.SetValue(i, 0, "+", false);
                            cell.setAttribute("FpCellType", "readonly");
                            spread.Rows(i).style.display = "table-row";
                            break;
                        }
                        else if (rowtype == "H") {
                            cell = document.getElementById("spdAnalytics").GetCellByRowCol(i, 0);
                            cell.removeAttribute("FpCellType");
                            spread.SetValue(i, 0, "+", false);
                            cell.setAttribute("FpCellType", "readonly");
                            spread.Rows(i).style.display = "none";
                        }
                        else {

                            spread.Rows(i).style.display = "none";
                        }
                    }
                }
                else if (cellval == "+") {
                    //   var rc = spread.GetRowCount();
                    for (var i = event.row; i > -1; i--) {
                        var rowtype = spread.GetValue(i, 2);
                        if (rowtype == "T") {
                            cell = document.getElementById("spdAnalytics").GetCellByRowCol(i, 0);
                            cell.removeAttribute("FpCellType");
                            spread.SetValue(i, 0, "", false);
                            cell.setAttribute("FpCellType", "readonly");
                            if ($('#blnUseFilter').is(':checked')) {
                                spread.Rows(i).style.display = "none";
                            }
                        }
                        else if (rowtype == "H") {
                            cell = document.getElementById("spdAnalytics").GetCellByRowCol(i, 0);
                            cell.removeAttribute("FpCellType");
                            spread.SetValue(i, 0, "-", false);
                            cell.setAttribute("FpCellType", "readonly");
                            spread.Rows(i).style.display = "table-row";
                            break
                        }
                        else if ($('#blnUseFilter').is(':checked')) {
                            HideUnFilterRow(i)
                        }
                        else {
                            spread.Rows(i).style.display = "table-row";
                        }
                    }
                }
                spread.SetActiveCell(event.row, 1)
            }
        }

        function HideUnFilterRow(row) {
            var selindex = $('#strFilterOn').get(0).selectedIndex;
            var filteramt = $('#decFilterAmt').val();
            var amount = parseFloat(filteramt);
            var selradio = $('#radioPosVar').attr('checked');
            var spread = document.getElementById("spdAnalytics");
            var value = parseFloat(spread.GetValue(row, selindex + 5).replace(',', ''));
            if (value < filteramt) {
                if (selradio == "checked") {
                    spread.Rows(row).style.display = "none";
                }
                else {
                    spread.Rows(row).style.display = "table-row";
                }
            }
            else {
                if (selradio == "checked") {
                    spread.Rows(row).style.display = "table-row";

                }
                else {
                    spread.Rows(row).style.display = "none";
                }
            }
        }


    });
    </script>
     <div  style="align:left; background-color:Gray; color:White;  height: 32px; margin: 10px 0; ">
                <p style = "padding-top:5px; padding-left:10px;">
                Current company name goes here
                @Html.DropDownList("intDept", DirectCast(ViewData("Dept"), SelectList), New With {.Style = "width: 175px;font-size: 12px; padding-top:2px; border-radius: 5px; border: 1px solid #DDD; color: #2c8eb9; "}) 
                </p>   
         </div>
            
         <div id="menucontainer" style="position: relative; z-index:2; "  > 
           <ul id="menu">
                    <li>@Html.ActionLink("Dashboard", "Index", "Dashboard")</li>
                    <li>@Html.ActionLink("Revenue and Expenses", "Index", "RE")</li>
                    <li>@Html.ActionLink("Assets, Liabilities and Equity", "Index", "")</li>
                    <li>@Html.ActionLink("Cash Flow", "Index", "")</li>
                    <li>@Html.ActionLink("Financial Ratios", "Index", "")</li> 
                    <li>@Html.ActionLink("Other Metrics", "Index", "")</li>
             </ul>    
         </div>    

    
    @code
        
        
        Using Html.BeginForm
           
       
            
        @<div id="sidebar">
            <div>
                 <img src="/Content/images/period.png" alt="Period" />
            </div>
            <div >
                @Html.DropDownList("intPeriod", DirectCast(ViewData("Periods"), SelectList), New With {.Style = "width: 175px;font-size: 11px; padding-top:2px; border-radius: 5px; border: 1px solid #DDD; color: #2c8eb9; "})  
            </div>
            <br />
            <div >
                 <img src="/Content/images/format.png" alt="Format" />
            </div>
            <div>
                @Html.DropDownList("intFormat", DirectCast(ViewData("Formats"), SelectList), New With {.Style = "width: 175px;font-size: 11px; padding-top:2px; border-radius: 5px; border: 1px solid #DDD; color: #2c8eb9; "})  
            </div>
            <div id = "checkbox" class = "indent0" style="margin-top: 5px;"  >
                 @Html.CheckBox("blnShowasPer", False) Show as percent          
            </div>         
            <div  id = "chkShowVar" style="display: none;" class = "indent0" >
                @Html.CheckBox("blnShowVar") Show variance
            </div>            
            <br />
            <div id = "optionsimage">               
                <img src="/Content/images/options.png" alt="Options"  />
            </div>
            <div id = "chkHighlightVar" class = "indent0"  style="margin-top: 5px;">            
                @Html.CheckBox("blnHighVar", ViewData("blnHighVar")) Highlight variances:            
            </div>
            <div id = "HighlightVarControls" style="display: none;">
                <div id = "chkHighlightPosVar" class = "indent10">
                    @Html.CheckBox("blnHighPosVar", True) + Variances > than:                
                    @Html.TextBox("intHighPosVar", 10, New With {.Style = "width: 20px; font-size: 10px; border-radius: 5px; border: 1px solid #DDD;text-align: right; "}) %
                </div>
                <div id = "chkHighlightNegVar" class = "indent10">
                    @Html.CheckBox("blnHighNegVar", True ) - Variances < than: 
                    @Html.TextBox("intHighNegVar", 0, New With {.Style = "width: 20px; margin-left: 3px; font-size: 10px; border-radius: 5px; border: 1px solid #DDD; text-align: right;"}) %                   
                </div>
            </div>
            <div id = "CheckBoxFilter" class = "indent0">
                @Html.CheckBox("blnUseFilter") Apply filter:            
            </div>
            <div id = "UseFilterControls" style="display: none;">
                <div  class = "indent10">
                    @Html.DropDownList("strFilterOn", DirectCast(ViewData("FilterOn"), SelectList), New With {.Style = "width: 150px;font-size: 11px; padding-top:2px; border-radius: 5px; border: 1px solid #DDD; color: #2c8eb9; "})  
                </div>
                <div > 
                    <label class = "indent10">                  
                    @Html.RadioButton("blnFilterPosVariance", True, New With {.id = "radioPosVar", .checked = "checked", .style = "width: 20px;"}) Amounts > than:
                    </label>                    
                </div>
                <div >
                    <label class = "indent10">
                    @Html.RadioButton("blnFilterPosVariance", False, New With {.id = "radioNegVar", .style = "width: 20px;"}) Amounts < than:
                    </label>
                </div>
                 <div class = "indent30" >
                    @Html.TextBox("decFilterAmt", 1000, New With {.Style = "width: 100px; font-size: 10px;  border-radius: 5px; border: 1px solid #DDD; text-align: right;", .id = "decFilterAmt"})
                </div>
            </div>
            <br />
            <div>
                <img src="/Content/images/charts.png" alt="Charts" />
            </div>
            <div>
               @Html.DropDownList("intChartType", DirectCast(ViewData("ChartType"), SelectList), New With {.Style = "width: 175px;font-size: 11px; padding-top:2px; border-radius: 5px; border: 1px solid #DDD; color: #2c8eb9; "})  
            </div >
            <div id = "ShowTrend" class = "indent10padtop5">
                @Html.CheckBox("blnShowTrend", New With {.Style = "border: 1px solid Silver; background-color:Silver;"})Show trend
            </div>
            <div id = "UpdateChart" class = "button" style="display: none;" >
                <input id="causepost" type="submit"  value="Update Chart" />
            </div> 
           
          </div>
          @<div  id="main" >  
            <div id="chart" style="display: none;">
                  @Html.FpSpread("spdChart", Sub(x)
                                               x.RowHeader.Visible = False
                                               x.ColumnHeader.Visible = False
                                               x.Height = 200
                                               x.Width = 500
                                           
                                           End Sub)
            </div> 
            <div id="spread" style="display: none;">
                 @Html.FpSpread("spdAnalytics", Sub(x)
                                                   
                                               x.RowHeader.Visible = False 
                                               x.ActiveSheetView.PageSize = 1000
                                           End Sub)
             </div>    
         </div>  
        End Using
    End Code
</div>
 

