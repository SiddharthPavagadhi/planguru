﻿@ModelType onlineAnalytics.Analysis
@Imports System.Collections.Generic
@Imports MvcCheckBoxList.Model
@Code
    ViewData("Title") = "Create New Analysis"
    Layout = "~/Views/Shared/AnalyticsMaster.vbhtml"
End Code
<script src="@Url.Content("~/Scripts/jquery.validate.min.js")" type="text/javascript"></script>
<script src="@Url.Content("~/Scripts/jquery.validate.unobtrusive.min.js")" type="text/javascript"></script>
<h2>
    Analysis</h2>
<br />
<div style="float: right">
    @Html.ActionLink("View Analysis List", "Index")
</div>
<br />
@Using Html.BeginForm("Create", "Analysis")
    @Html.AntiForgeryToken()   
    @Html.ValidationSummary(True)    
     
    @<fieldset>
        <legend>Add New Analysis</legend>
        <div style="width: 100%;">
            <div style="float: left; width: 33%;">
                <ol class="round">
                    <li>
                        @Html.LabelFor(Function(m) m.AnalysisName)<br />
                        @Html.TextBoxFor(Function(m) m.AnalysisName)<br />
                        @Html.ValidationMessageFor(Function(m) m.AnalysisName)
                    </li>
                </ol>
            </div>
            <div style="float: left; width: 36%;">
                <ol class="round">
                    <li>
                        @Html.LabelFor(Function(m) m.CompanyId, "Company")<br />
                        @Html.DropDownList("CompanyId", DirectCast(ViewBag.SelectedCompany, SelectList), "Select Company", New With {.Class = "select", .Style = "width:250px;"})<br />
                        @Html.ValidationMessageFor(Function(m) m.CompanyId)
                    </li>
                </ol>
            </div>
            <div class="associate_user">
                @Code
                    Dim htmlListInfo = New HtmlListInfo(HtmlTag.table, 10, New With {.class = "styled_list"}, TextLayout.Default, TemplateIsUsed.No)
                                                        
                    Dim checkBoxAtt = New With {.class = "styled_checkbox"}
                End Code
                <ol class="round">
                    <li>
                        @Html.LabelFor(Function(m) m.PostedUsers.UserIds) analysis
                        @Html.CheckBoxListFor(Function(u) u.PostedUsers.UserIds, Function(u) u.AvailableUsers, Function(u) u.Id, Function(u) u.Name, Function(u) u.SelectedUsers, checkBoxAtt, htmlListInfo, Nothing, Function(u) u.Tag)
                    </li>
                </ol>
            </div>
            <div class="input-form">
                <input type="submit" value="Create Analysis" class="submit-button" />
            </div>
            <div style="float: left;">
                @If Not (DirectCast(TempData("Message"), String) Is Nothing) Then
                    @<label class="message">@TempData("Message").ToString()
                    </label>                         
                End If
                @If Not (DirectCast(TempData("ErrorMessage"), String) Is Nothing) Then
                    @<label class="error">@TempData("ErrorMessage").ToString()
                    </label>                         
                End If
            </div>
        </div>
    </fieldset>                                  
End Using
