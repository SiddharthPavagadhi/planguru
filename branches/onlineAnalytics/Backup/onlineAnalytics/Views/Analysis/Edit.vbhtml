﻿@ModelType onlineAnalytics.Analysis
@Imports MvcCheckBoxList.Model

    
@Code
    ViewData("Title") = "Edit Analysis"
    Layout = "~/Views/Shared/AnalyticsMaster.vbhtml"
End Code
<script src="@Url.Content("~/Scripts/jquery.validate.min.js")" type="text/javascript"></script>
<script src="@Url.Content("~/Scripts/jquery.validate.unobtrusive.min.js")" type="text/javascript"></script>
<h2>
    Analysis</h2>
<br />
<div style="float: right">
    @Html.ActionLink("View Analysis List", "Index")
</div>
<br />
@Using Html.BeginForm("Edit", "Analysis")
    @Html.AntiForgeryToken()   
    @Html.ValidationSummary(True)    
     
    @<fieldset>
        <legend>Edit Analysis</legend>
        <div style="width: 100;">
            <div style="float: left; width: 30%;" class="editor-label">
                @Html.LabelFor(Function(m) m.AnalysisName)<br />
                @Html.TextBoxFor(Function(m) m.AnalysisName)<br />
                @Html.ValidationMessageFor(Function(m) m.AnalysisName)
            </div>
            <div style="float: left; width: 36%;" class="editor-label">
                @Html.LabelFor(Function(m) m.CompanyId, "Company")<br />
                @Html.DropDownList("CompanyId", DirectCast(ViewBag.SelectedCompany, SelectList), "Select Company", New With {.Class = "select", .Style = "width:250px;"})
                <br />
                @Html.ValidationMessageFor(Function(m) m.CompanyId)
            </div>
             <div style="display:inline-block;float:left; ">
                
                @Code
                    Dim htmlListInfo = New HtmlListInfo(HtmlTag.table, 10, New With {.class = "styled_list"}, TextLayout.Default, TemplateIsUsed.No)
                                                        
                    Dim checkBoxAtt = New With {.class = "styled_checkbox"}
                End Code
                <ol class="round">
                    <li>
                          @Html.LabelFor(Function(m) m.PostedUsers.UserIds)
                          @Html.CheckBoxListFor(Function(u) u.PostedUsers.UserIds, Function(u) u.AvailableUsers, Function(u) u.Id, Function(u) u.Name, Function(u) u.SelectedUsers, checkBoxAtt, htmlListInfo, Nothing, Function(u) u.Tag)
                          @Html.ValidationMessageFor(Function(m) m.PostedUsers.UserIds)
                  </li>
                  </ol>
            </div>
            <div style="float: left; width: 33%; margin: 1em 0 0 0;">
                <input type="submit" value="Update analysis details" class="submit-button" /><input id="Cancel" type="button" value="Cancel" class="submit-button button" data=@Url.Action("Index") />
            </div>
            <div style="float: left;margin-top:10px;">
                @If Not (DirectCast(TempData("Message"), String) Is Nothing) Then
                    @<label class="message">@TempData("Message").ToString()
                    </label>                         
                End If
                @If Not (DirectCast(TempData("ErrorMessage"), String) Is Nothing) Then
                    @<label class="error">@TempData("ErrorMessage").ToString()
                    </label>                         
                End If
            </div>
            @Html.HiddenFor(Function(m) m.AnalysisId)
        </div>
    </fieldset>                                  
End Using
<script type="text/javascript" language="javascript">

    $(document).ready(function () {
        $("#Cancel").click(function (event) {
            //event.preventDefault();
            var url = $(this).attr('data');
            window.location = url;
        });


    });  


</script>
