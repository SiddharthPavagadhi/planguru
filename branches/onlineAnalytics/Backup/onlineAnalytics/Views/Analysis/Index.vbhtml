﻿@ModelType  IEnumerable(Of onlineAnalytics.Analysis)
@Code
    ViewData("Title") = "View Analyses"
    Layout = "~/Views/Shared/AnalyticsMaster.vbhtml"
End Code
<h2>
    Analysis</h2>
<br />
<div style="float: right">
    @Html.ActionLink("Add New Analysis", "Create")
</div>
<br />
<fieldset>
    <legend>View Analysis List</legend>
    <br />
    <table class="table-layout">
        <tr>
            <th class="left" style="width: 45%">
                Analysis Name
            </th>
            <th class="left" style="width: 45%">
                Company Name
            </th>
            <th class="center" style="width:5%">
                Edit
            </th>
            <th class="center" style="width:5%">
                Delete
            </th>
        </tr>
        @For Each item In Model
            Dim currentItem = item
            @<tr>
                <td class="left">
                    @Html.DisplayFor(Function(modelItem) currentItem.AnalysisName)
                </td>
                <td class="left">
                    @Html.DisplayFor(Function(modelItem) currentItem.Company.CompanyName)
                </td>
                <td class="center">
                    @Html.ActionLink("Edit", "Edit", New With {.id = currentItem.AnalysisId})
                </td>
                <td class="center">                  
                    <a id=@currentItem.AnalysisId class="Delete" href="#" data='@Url.Action("Delete", "Analysis", New With {.id = currentItem.AnalysisId})' >
                        Delete</a>
                </td>
            </tr>
        Next
    </table>
    @If Not (DirectCast(TempData("Message"), String) Is Nothing) Then
        @<label class="success">@TempData("Message").ToString()
        </label>                         
    End If
    @If Not (DirectCast(TempData("ErrorMessage"), String) Is Nothing) Then
        @<label class="error">@TempData("ErrorMessage").ToString()
        </label>                         
    End If
</fieldset>
<script type="text/javascript" language="javascript">

    $(".Delete").click(function (event) {
        event.preventDefault();
        var url = $(this).attr('data');

        $.msgBox({
            title: "Confirm",
            content: "Are you sure you want to delete this analysis?",
            type: "confirm",
            buttons: [{ value: "Yes" }, { value: "No"}],
            success: function (result) {
                if (result == "Yes") {
                    window.location = url;
                }
            }
        });

    }); 
</script>