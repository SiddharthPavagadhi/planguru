﻿Imports System.Collections.Generic
Imports System.Linq
Imports System.Web
'Interface for Company, CompanyRepository.vb implementes all methods of this Interface.
Public Interface ICompanyRepository
    Inherits IDisposable
    Function GetCompanies() As IEnumerable(Of Company)
    Function GetCompanyById(id As Integer) As Company
    Sub InsertCompany(company As Company)
    Sub DeleteCompany(companyId As Integer)
    Sub UpdateCompany(company As Company)
    Sub Save()
End Interface

'Interface for Analysis, AnalysisRepository.vb implementes all methods of this Interface.
Public Interface IAnalysisRepository
    Inherits IDisposable

    Function GetAnalyses() As IEnumerable(Of Analysis)
    Function GetAnalysisById(id As Integer) As Analysis
    Function ValidateAnalysis(Analysis As String, CompanyId As Integer) As IQueryable(Of Analysis)
    Sub InsertAnalysis(analysis As Analysis)
    Sub DeleteAnalysis(analysisId As Integer)
    Sub UpdateAnalysis(analysis As Analysis)
    Sub Save()
End Interface

'Interface for Customer, CustomerRepository.vb implementes all methods of this Interface.
Public Interface ICustomerRepository
    Inherits IDisposable

    Function GetCustomers() As IEnumerable(Of Customer)
    Function GetCustomerById(id As String) As Customer
    Sub InsertCustomer(customer As Customer)
    Sub DeleteCustomer(customerId As String)
    Sub UpdateCustomer(customer As Customer)
    Function SearchCustomers(customerId As String, customerName As String, sauName As String, telephonenumber As String) As IEnumerable(Of Customer)
    Sub Save()
End Interface


'Interface for CountryCode, CountryStateRepository.vb implementes all methods of this Interface.
Public Interface ICountryCodeRepository
    Inherits IDisposable
    Function GetCountryCodes() As IEnumerable(Of CountryCode)
End Interface
'Interface for State, StateRepository.vb implementes all methods of this Interface.
Public Interface IStateCodeRepository
    Inherits IDisposable
    Function GetStateCodes() As IEnumerable(Of StateCode)
End Interface

'Interface for State, StateRepository.vb implementes all methods of this Interface.
Public Interface IUserRoleRepository
    Inherits IDisposable
    Function GetUserRoles() As IEnumerable(Of UserRole)
End Interface

Public Interface IUserRolePermissionRepository
    Inherits IDisposable
    Function GetUserRolePermission() As IEnumerable(Of UserRolePermission)
End Interface


Public Interface IUserRolePermissionMappingRepository
    Inherits IDisposable
    Function GetUserRolePermissionMapping() As IEnumerable(Of UserRolePermissionMapping)
End Interface

'Interface for State, StateRepository.vb implementes all methods of this Interface.
Public Interface IUserRepository
    Inherits IDisposable

    Function ValidateUserAccount(UserName As String, Password As String) As User
    Function GetUsers() As IEnumerable(Of User)
    Function GetUserById(id As String) As User
    Sub UpdateUser(user As User)
    Sub DeleteUser(userId As String)
    Sub Save()
End Interface

Public Interface IUserCompanyMappingRepository
    Inherits IDisposable

    Function GetUserCompanyMappingByCompanyId(CompanyId As Integer) As IEnumerable(Of UserCompanyMapping)
    Function GetUserCompanyMapping() As IEnumerable(Of UserCompanyMapping)
    Function GetUserCompanyMappingById(UserCompanyMappingId As Integer) As UserCompanyMapping
    Sub UpdateUserCompanyMapping(UserCompanyDetails As UserCompanyMapping)
    Sub DeleteUserCompanyMapping(UserCompanyMappingId As Integer)
    Function DeleteUserCompanyMappingByUserIdAndCompanyId(CompanyId As Integer, UserId As String) As Integer
    Sub Save()
End Interface

Public Interface IUserAnalysisMappingRepository
    Inherits IDisposable

    Function GetUserAnalysisMappingByAnalysisId(AnalysisId As Integer) As IEnumerable(Of UserAnalysisMapping)
    Function GetUserAnalysisMapping() As IEnumerable(Of UserAnalysisMapping)
    Function GetUserAnalysisMappingById(UserAnalysisMappingId As Integer) As UserAnalysisMapping
    Sub UpdateUserAnalysisMapping(UserAnalysisDetails As UserAnalysisMapping)
    Sub DeleteUserAnalysisMapping(UserAnalysisMappingId As Integer)
    Function DeleteUserAnalysisMappingByUserIdAndAnalysisId(AnalysisId As Integer, UserId As String) As Integer
    Sub Save()
End Interface
'Interface for EmailInfo, EmailRepository.vb implementes all methods of this Interface.
Public Interface IEmailRepository
    Inherits IDisposable

    'Function ValidateUserAccount(UserName As String, Password As String) As EmailInfo
    Function GetEmailInfo() As IEnumerable(Of EmailInfo)
    Function GetEmailInfoById(id As Integer) As EmailInfo
    'Sub UpdateEmailInfo(email As EmailInfo)
    'Sub DeleteEmailInfo(emailId As Integer)
    Sub Save()
End Interface
