﻿Imports System.Data.Entity

Public Class HomeController
    Inherits System.Web.Mvc.Controller

    Function Index() As ActionResult

        Database.SetInitializer(New SampleData())
        TestforData()
        GetAllRecords()
        ViewData("width") = 1000

        Return View()
    End Function

    Function About() As ActionResult
        Return View()
    End Function

    Function Contact() As ActionResult
        ViewData("Message") = "Your contact page."

        Return View()
    End Function

    Private Sub TestforData()
        Logger.Log.Info(String.Format("Inside HomeController TestforData() Started"))
        Try
            Using context = New DataAccess()
                For Each account As Account In context.Accounts
                    'ViewData("Message") = account.AcctType.ClassDesc
                Next
            End Using
            Logger.Log.Info(String.Format("Inside HomeController TestforData() Ended"))
        Catch ex As Exception
            Logger.Log.Error(String.Format("Exception occurred inside HomeController for TestforData() with Message- {0} - {1} ", ex.Message, ex.StackTrace))
        Finally
            Logger.Log.Info(String.Format("Inside HomeController TestforData() Finally Ended"))
        End Try
    End Sub

    Private Sub GetAllRecords()
        Logger.Log.Info(String.Format("Inside HomeController GetAllRecords() Started"))
        Try
            Using context As New DataAccess
                context.AcctTypes.Load()
                context.Accounts.Load()
                context.Balances.Load()
                Dim selectedAccounts = (From a In context.Accounts.Local
                                            Where a.AcctType.TypeDesc = "Revenue" Or a.AcctType.TypeDesc = "Expense" _
                                            Order By a.SortSequence _
                                            Select a)
                For Each account As Account In selectedAccounts
                    Debug.WriteLine(account.Description)
                Next

            End Using
            Logger.Log.Info(String.Format("Inside HomeController GetAllRecords() Ended"))
        Catch ex As Exception
            Logger.Log.Error(String.Format("Exception occurred inside HomeController for GetAllRecords() with Message- {0} - {1} ", ex.Message, ex.StackTrace))
        Finally
            Logger.Log.Info(String.Format("Inside HomeController GetAllRecords() Finally Ended"))
        End Try
    End Sub
End Class
