﻿Imports System.Dynamic
Imports System.Data.Entity
Imports System.Web.Mvc
Imports PagedList
Imports onlineAnalytics
Imports System.Collections.Generic 
Imports CheckBoxListInfo
Imports MvcCheckBoxList.Model


Namespace onlineAnalytics
    Public Class CompanyController
        Inherits System.Web.Mvc.Controller

        Private utility As New Utility()
        Private companyRepository As ICompanyRepository
        Private userRepository As IUserRepository
        Private usercompanyMapping As IUserCompanyMappingRepository

        Public Sub New()
            Me.companyRepository = New CompanyRepository(New DataAccess())
        End Sub

        Public Sub New(companyRepository As ICompanyRepository)
            Me.companyRepository = companyRepository
        End Sub

        Function Index() As ViewResult
            Try
                Dim companies = From c In companyRepository.GetCompanies()
                For Each item In companies
                    item.FiscalMonthName = MonthName(item.FiscalMonthStart)
                Next
                Logger.Log.Info(String.Format("Company Index Execution Done"))
                Return View(companies.ToPagedList(1, 20))
            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Unable to Index() company-", ex.Message)
                Logger.Log.Error(String.Format("\n Unable to Index() with Message- {0} " + Environment.NewLine + "Stack Trace: {1} ", ex.Message, ex.StackTrace))
                Return View()
            Finally
                Logger.Log.Info(String.Format("Company Index() Execution Ended"))
            End Try
           
        End Function

        Function Create() As ViewResult

            Dim companyDetail As New Company()
            Try
                PopulateMonths(0)
                PopulateUserList(companyDetail)
                Logger.Log.Info(String.Format("Company Create Execution Done"))
            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Unable to Create company-", ex.Message)
                Logger.Log.Error(String.Format("\n Unable to Edit with Message- {0} " + Environment.NewLine + "Stack Trace: {1} ", ex.Message, ex.StackTrace))
                Return View()
            Finally
                Logger.Log.Info(String.Format("Company Create Execution Ended"))
            End Try
            Return View(companyDetail)
        End Function

        <HttpPost()> _
       <ValidateAntiForgeryToken()> _
        Function Create(CompanyDetail As Company) As ActionResult
            Logger.Log.Info(String.Format("Company Creation started {0}", CompanyDetail.CompanyId))
            Try
                If ModelState.IsValid Then

                    CompanyDetail.CustomerId = 1
                    CompanyDetail.CreatedBy = 1
                    CompanyDetail.UpdatedBy = 1
                    companyRepository.InsertCompany(CompanyDetail)
                    companyRepository.Save()
                    Logger.Log.Info(String.Format("Company created successfully with Company ID : {0} , Company Name {1} ", CompanyDetail.CompanyId, CompanyDetail.CompanyName))

                    If Not (CompanyDetail.PostedUsers Is Nothing) Then

                        Dim usercompany As New UserCompanyMapping()
                        For Each selecteduser As String In CompanyDetail.PostedUsers.UserIds
                            usercompany.CompanyId = CompanyDetail.CompanyId
                            usercompany.UserId = selecteduser.ToString()
                            usercompany.CratedBy = 1
                            usercompany.UpdatedBy = 1
                            utility.UserCompanyMappingRepository.Insert(usercompany)
                            utility.UserCompanyMappingRepository.Save()
                            Logger.Log.Info(String.Format("User is successfully mapped with company, Company ID : {0} , UserId {1} ", CompanyDetail.CompanyId, usercompany.UserId))
                        Next

                    End If
                    TempData("Message") = "Company created successfully."
                    Return RedirectToAction("Index")

                End If
                PopulateMonths(CompanyDetail.FiscalMonthStart)
                PopulateUserList(CompanyDetail)
            Catch dataEx As DataException
                TempData("ErrorMessage") = String.Concat("Unable to create company-", dataEx.Message)
                Logger.Log.Error(String.Format(Environment.NewLine + " Unable to create company id- {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2} ", CompanyDetail.CompanyId, dataEx.Message, dataEx.StackTrace))
            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Unable to create company-", ex.Message)
                Logger.Log.Error(String.Format("\n Unable to create company id- {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2} ", CompanyDetail.CompanyId, ex.Message, ex.StackTrace))
            Finally
                Logger.Log.Info(String.Format("Create Company Execution Ended"))
            End Try

            
            Return View(CompanyDetail)
        End Function

        <HttpPost()>
        Function Edit(CompanyDetail As Company) As ActionResult
            Logger.Log.Info(String.Format("\n Company Update started {0}", CompanyDetail.CompanyId))
            Try
                If ModelState.IsValid Then

                    CompanyDetail.CustomerId = 1
                    CompanyDetail.CreatedBy = 1
                    CompanyDetail.UpdatedBy = 1
                    companyRepository.UpdateCompany(CompanyDetail)
                    companyRepository.Save()
                    'Get list of User mapped with the selected company
                    Dim selectMappedUserWithComp As Dictionary(Of String, Integer) = utility.UserCompanyMappingRepository.GetUserCompanyMappingByCompanyId(CompanyDetail.CompanyId).ToDictionary(Function(a) a.UserId, Function(a) a.UserCompanyMappingId)
                    Dim usercompany As New UserCompanyMapping()

                    If (Not CompanyDetail.PostedUsers Is Nothing) Then
                        For Each selecteduser As String In CompanyDetail.PostedUsers.UserIds
                            'If New
                            If selectMappedUserWithComp.ContainsKey(selecteduser) = False Then
                                usercompany.CompanyId = CompanyDetail.CompanyId
                                usercompany.UserId = selecteduser.ToString()
                                usercompany.CratedBy = 1
                                usercompany.UpdatedBy = 1
                                utility.UserCompanyMappingRepository.Insert(usercompany)
                                utility.UserCompanyMappingRepository.Save()
                                Logger.Log.Info(String.Format("User is successfully mapped with company, Company ID : {0} , UserId {1} ", CompanyDetail.CompanyId, usercompany.UserId))
                            End If
                            selectMappedUserWithComp.Remove(selecteduser)
                        Next
                    End If

                    'Delete the users, which are not found within redefine selected list.
                    If (selectMappedUserWithComp.Count > 0) Then
                        Logger.Log.Info(String.Format("Total {0} users found to be delete of Company ID {1} from UserCompanyMapping ", selectMappedUserWithComp.Count, CompanyDetail.CompanyId))
                        Dim getSelectMappedUser = String.Concat("'", String.Join("','", selectMappedUserWithComp.Select(Function(a) a.Key.ToString())), "'")
                        Dim result = utility.UserCompanyMappingRepository.DeleteUserCompanyMappingByUserIdAndCompanyId(CompanyDetail.CompanyId, getSelectMappedUser)
                        If (result = selectMappedUserWithComp.Count) Then
                            Logger.Log.Info(String.Format("Users delete successfully, which is not found within redefine selected list with company, Company ID : {0} , Users {1} ", CompanyDetail.CompanyId, getSelectMappedUser))
                        Else
                            Logger.Log.Info(String.Format("Delete users list mismatch --> FoundUsersToDelete {0} : , ActualUsersDeleted : {1} "))
                            Logger.Log.Info(String.Format("Users delete successfully, which is not found within redefine selected list with company, Company ID : {0} , Users {1} ", CompanyDetail.CompanyId, getSelectMappedUser))
                        End If
                    End If
                    TempData("Message") = "Company updated successfully."
                    Logger.Log.Info(String.Format("Company Updated successfully with id {0}", CompanyDetail.CompanyId))
                    Return RedirectToAction("Index")
                End If

                PopulateMonths(CompanyDetail.FiscalMonthStart)
                PopulateUserList(CompanyDetail)
            Catch dataEx As DataException
                TempData("ErrorMessage") = String.Concat("Unable to update company-", dataEx.Message)
                Logger.Log.Error(String.Format("\n Unable to Update company id- {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2} ", CompanyDetail.CompanyId, dataEx.Message, dataEx.StackTrace))
            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Unable to update company-", ex.Message)
                Logger.Log.Error(String.Format("\n Unable to Update company id- {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2} ", CompanyDetail.CompanyId, ex.Message, ex.StackTrace))
            Finally
                Logger.Log.Info(String.Format("Execution Ended"))
            End Try
            Return View(CompanyDetail)
        End Function

        Public Function Edit(id As Integer) As ActionResult
            Try
                Dim company = companyRepository.GetCompanyById(id)
                PopulateMonths(company.FiscalMonthStart)
                PopulateUserList(company)
                Logger.Log.Info(String.Format("Company Edit Execution Successfully Done"))
                Return View(company)
            Catch dataEx As DataException
                TempData("ErrorMessage") = String.Concat("Unable to Edit company-", dataEx.Message)
                Logger.Log.Error(String.Format("\n Unable to Edit company id- {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2} ", id, dataEx.Message, dataEx.StackTrace))
            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Unable to Edit company-", ex.Message)
                Logger.Log.Error(String.Format("\n Unable to Edit company id- {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2} ", id, ex.Message, ex.StackTrace))
            Finally
                Logger.Log.Info(String.Format("Company Edit Execution Ended"))
            End Try
            Return Nothing
        End Function

        Public Function Delete(Id As Integer) As ActionResult

            Logger.Log.Info(String.Format("Company Deletion Started {0}", Id))

            Try
                companyRepository.DeleteCompany(Id)
                companyRepository.Save()
                TempData("Message") = "Company deleted successfully."
                Logger.Log.Info(String.Format("Company Deleted successfully with id {0}", Id))
            Catch dataEx As DataException
                'Log the error
                TempData("ErrorMessage") = String.Concat("Unable to delete company - ", dataEx.Message)
                Logger.Log.Error(String.Format(Environment.NewLine + " Unable to Delete company id- {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2}", Id, dataEx.Message, dataEx.StackTrace))
                Return RedirectToAction("Index", New System.Web.Routing.RouteValueDictionary() _
                                        From {{"id", Id}, {"deleteChangesError", True}})
            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Unable to delete company-", ex.Message)
                Logger.Log.Error(String.Format(" Unable to Delete company id- {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2} ", Id, ex.Message, ex.StackTrace))
            Finally
                Logger.Log.Info(String.Format("Execution Ended"))
            End Try
            Return RedirectToAction("Index")
        End Function

        Private Sub PopulateMonths(Optional SelectedMonth As Object = Nothing)
            Try
                ViewBag.Months = utility.GetMonth(SelectedMonth)
                Logger.Log.Info(String.Format("PopulateMonths Sub Successfully Executed"))
            Catch dataEx As DataException
                TempData("ErrorMessage") = String.Concat("Unable to Populate Months-", dataEx.Message)
                Logger.Log.Error(String.Format(" Unable to Populate Months with Message-{0} {1} ", dataEx.Message, dataEx.StackTrace))
                Throw
            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Unable to Populate Months-", ex.Message)
                Logger.Log.Error(String.Format(" Unable to Populate Months with Message-{0} {1} ", ex.Message, ex.StackTrace))
                Throw
            Finally
                Logger.Log.Info(String.Format("PopulateMonths Execution Ended"))
            End Try
        End Sub

        Private Sub PopulateUserList(companyDetail As Company)
            Try
                Dim userComp = utility.UserCompanyMappingRepository.GetUserCompanyMappingByCompanyId(companyDetail.CompanyId)
                Dim users = utility.UserRepository.GetUsers()
                Dim userList As List(Of User_) = New List(Of User_)
                'if an array of posted user ids exists and is not empty,
                'save selected ids
                For Each user As User In users
                    userList.Add(New User_(user.UserId, user.FirstName, 1, False))
                Next
                companyDetail.AvailableUsers = userList
                If Not (userComp Is Nothing) Then
                    Dim InsertedUserList As List(Of User_) = New List(Of User_)
                    Dim InsertedUsers = users.Where(Function(b) userComp.Any(Function(a) a.UserId = b.UserId))
                    For Each user As User In InsertedUsers
                        InsertedUserList.Add(New User_(user.UserId, user.FirstName, 1, False))
                    Next
                    companyDetail.SelectedUsers = InsertedUserList
                End If
                ' if a view model array of posted user ids exists and is not empty,
                ' save selected ids
                If Not (companyDetail.PostedUsers Is Nothing) Then
                    Dim selectedUserList As List(Of User_) = New List(Of User_)
                    Dim selectedUsers = utility.UserRepository.GetUsers.Where(Function(u) companyDetail.PostedUsers.UserIds.Any(Function(s) u.UserId.ToString().Equals(s))).ToList()
                    For Each user As User In selectedUsers
                        selectedUserList.Add(New User_(user.UserId, user.FirstName, 1, False))
                    Next
                    companyDetail.SelectedUsers = selectedUserList
                End If
                Logger.Log.Info(String.Format("PopulateUserList Function Successfully Executed"))
            Catch dataEx As DataException
                TempData("ErrorMessage") = String.Concat("Populate User List-->", dataEx.Message)
                Logger.Log.Error(String.Format(" Unable to Populate UserListfor  company id- {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2} ", companyDetail.CompanyId, dataEx.Message, dataEx.StackTrace))
                Throw
            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Populate User List-->", ex.Message)
                Logger.Log.Error(String.Format(" Unable to Populate User List for companyid- {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2} ", companyDetail.CompanyId, ex.Message, ex.StackTrace))
                Throw
            Finally
                Logger.Log.Info(String.Format("PopulateUserList Function Ended"))
            End Try
        End Sub

        Protected Overrides Sub Dispose(disposing As Boolean)
            companyRepository.Dispose()
            MyBase.Dispose(disposing)
        End Sub

    End Class
End Namespace
