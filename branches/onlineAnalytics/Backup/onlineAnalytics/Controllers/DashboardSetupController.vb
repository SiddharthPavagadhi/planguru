﻿Imports FarPoint.Mvc.Spread
Imports System.Data.Entity
Namespace onlineAnalytics
    Public Class DashboardSetupController
        Inherits System.Web.Mvc.Controller

        '
        ' GET: /DashboardSetup

        Function Index(<MvcSpread("spdRE")> spdRE As FpSpread, <MvcSpread("spdBS")> spdBS As FpSpread, <MvcSpread("spdCF")> spdCF As FpSpread) As ActionResult

            Dim Formats As List(Of SelectListItem) = New List(Of SelectListItem)
            For intCtr = 0 To 8
                Formats.Add(New SelectListItem With {.Text = CommonProcedures.GetFormat(intCtr, "RE"), .Value = intCtr + 1, .Selected = False})
            Next
            ViewData("Formats") = New SelectList(Formats, "value", "text")
            Dim ChartType As List(Of SelectListItem) = New List(Of SelectListItem)
            For intCtr = 1 To 5
                ChartType.Add(New SelectListItem With {.Text = CommonProcedures.GetChartDesc(intCtr), .Value = intCtr + 1, .Selected = False})
            Next
            ViewData("ChartType") = New SelectList(ChartType, "value", "text", "selected")
            ViewData("intChartType") = 1
            ViewData("intFormat") = 1
            ViewData("blnShowTrend") = False
            ViewData("blnSmoothed") = False
            Return View()
        End Function



        <MvcSpreadEvent("Load", "spdRE", DirectCast(Nothing, String()))> _
        Private Sub FpRESpread_Load(sender As Object, e As EventArgs)
            Dim spread As FpSpread = DirectCast(sender, FpSpread)
            If Not spread.Page.IsPostBack Then
                LoadSpread(spread, "RE")

            End If

        End Sub

        <MvcSpreadEvent("Load", "spdBS", DirectCast(Nothing, String()))> _
        Private Sub FpBSSpread_Load(sender As Object, e As EventArgs)
            Dim spread As FpSpread = DirectCast(sender, FpSpread)
            If Not spread.Page.IsPostBack Then
                LoadSpread(spread, "BS")

            End If

        End Sub
        Private Sub LoadSpread(ByRef spread As FpSpread, strType As String)
            

            spread.Sheets(0).DefaultStyle.Font.Size = FontSize.Large
            spread.CommandBar.Visible = False
            '  spread.Sheets(0).FrozenColumnCount = 4
            spread.Sheets(0).ColumnCount = 4
            spread.Sheets(0).RowCount = 0
            spread.Sheets(0).Rows(-1).Height = 18
            spread.Sheets(0).Columns(0, 1).Width = 20
            spread.Sheets(0).Columns(2).Width = 0
            spread.Sheets(0).Columns(3).Width = 290
            spread.Sheets(0).Rows(-1).VerticalAlign = VerticalAlign.Middle
            'set column headers 
            spread.Sheets(0).ColumnHeader.Rows(0).Font.Size = FontSize.Large 
            spread.Sheets(0).ColumnHeader.Rows(0).Height = 20
            spread.Sheets(0).ColumnHeader.Cells(0, 0).Text = " "
            spread.Sheets(0).ColumnHeader.Cells(0, 1).Text = " "
            spread.Sheets(0).ColumnHeader.Cells(0, 2).Text = " "
            spread.Sheets(0).ColumnHeader.Cells(0, 3).Text = "Item to Display in Dashboard "
            spread.Sheets(0).SelectionPolicy = FarPoint.Web.Spread.Model.SelectionPolicy.Single
            spread.Sheets(0).SelectionBackColorStyle = FarPoint.Web.Spread.SelectionBackColorStyles.None
            spread.VerticalScrollBarPolicy = FarPoint.Web.Spread.ScrollBarPolicy.Always
            Select Case strType
                Case "RE"
                    LoadspdRE(spread)
                Case "BS"
                    spread.Sheets(0).SetValue(0, 0, "Assets")
            End Select
            spread.ActiveSheetView.PageSize = spread.Sheets(0).RowCount

        End Sub

        Public Sub LoadspdRE(spread As FpSpread)
            Using context As New DataAccess
                context.AcctTypes.Load()
                context.Accounts.Load()
                context.Balances.Load()
                Dim intRowCtr As Integer = -1 
                Dim strAcctType As String
                Dim intSpanRows(1) As Integer
                Dim chkbox As New FarPoint.Web.Spread.CheckBoxCellType
                Dim blnTotalFlag As Boolean
                Dim dropdown As New FarPoint.Web.Spread.ComboBoxCellType
                Dim cbstr As String()
                cbstr = New String() {"Bar", "Line"}


                dropdown.Items = cbstr
                chkbox.OnClientClick = DSChkBoxClicked()

                Dim selectedAccounts = (From a In context.Accounts.Local
                                            Where a.AcctType.TypeDesc = "Revenue" Or a.AcctType.TypeDesc = "Expense" _
                                            Order By a.SortSequence _
                                            Select a)
                For Each account As Account In selectedAccounts
                    intRowCtr += 1
                    spread.Sheets(0).AddRows(intRowCtr, 1)
                    ' spread.Sheets(0).Rows(intRowCtr).Height = 18
                    spread.Sheets(0).SetValue(intRowCtr, 3, account.Description)
                    spread.Sheets(0).SetValue(intRowCtr, 2, account.AcctDescriptor)
                    strAcctType = account.AcctType.TypeDesc
                    Select Case account.AcctDescriptor
                        Case "H"
                            CommonProcedures.FormatHeaderRow(spread, intRowCtr, intSpanRows, blnTotalFlag)
                        Case "T"
                            CommonProcedures.FormatTotalRow(spread, intRowCtr, intSpanRows, blnTotalFlag, 0, 0, account.AcctType.ClassDesc)
                            spread.Sheets(0).Cells(intRowCtr, 1).CellType = chkbox
                        Case "D"
                            spread.Sheets(0).Cells(intRowCtr, 0).Border.BorderColorBottom = Drawing.Color.White
                            spread.Sheets(0).Cells(intRowCtr, 1).CellType = chkbox
                    End Select

                Next
            End Using


        End Sub
        'Private Function RECheckBoxClicked() As String
        '    'RECheckBoxClicked = "var spread = document.getElementById('spdRE');" _
        '    '            & "var row = spread.ActiveRow;" _
        '    '            & "alert(row);"

        'End Function


    End Class

   

   
End Namespace