﻿Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel
Imports System.Collections.Generic
Imports System.Data.Entity

Public Class UserAccess

    Public Property SubscriptionManagement() As String = "none"
    Public Property UserManagement() As String = "none"
    Public Property SearchCustomer() As String = "none"
    Public Property ViewSubscription() As String = "none"
    Public Property AddSubscription() As String = "none"
    Public Property UpdateSubscription() As String = "none"
    Public Property CancelSubscription() As String = "none"
    Public Property AddUser() As String = "none"
    Public Property DeleteUser() As String = "none"
    Public Property ViewUser() As String = "none"
    Public Property UpdateUser() As String = "none"
    Public Property AddCompany() As String = "none"
    Public Property DeleteCompany() As String = "none"
    Public Property ViewCompanies() As String = "none"
    Public Property UpdateCompany() As String = "none"
    Public Property UserCompanyMapping() As String = "none"
    Public Property AddAnalysis() As String = "none"
    Public Property DeleteAnalysis() As String = "none"
    Public Property ViewAnalyses() As String = "none"
    Public Property UpdateAnalysis() As String = "none"
    Public Property UserAnalysisMapping() As String = "none"
    Public Property ViewAnalysisInfo() As String = "none"
    Public Property PrintAnalysisInfo() As String = "none"
    Public Property ModifyDashboard() As String = "none"


End Class
