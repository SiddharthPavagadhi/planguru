﻿Imports System.ComponentModel.DataAnnotations
Imports System.Data.Entity


<Table("UserCompanyMapping")> _
Public Class UserCompanyMapping
    <Key()> _
   <DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)> _
    Public Property UserCompanyMappingId As Integer

    Public Property CompanyId As Integer

    Public Property UserId As String

    Public Property CratedBy As String

    Public Property CratedOn As DateTime = DateTime.Now()

    Public Property UpdatedBy As String

    Public Property UpdatedOn As DateTime = DateTime.Now()
    'Public Overridable Property Company() As Company
    'Public Overridable Property User() As User
End Class

<Table("UserRolePermissionMapping")> _
Public Class UserRolePermissionMapping
    <Key()> _
   <DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)> _
    Public Property UserRolePermissionMappingId As Integer

    Public Property UserRoleId As Integer

    Public Property UserRolePermissionId As Integer

    Public Property CratedBy As String

    Public Property CratedOn As DateTime = DateTime.Now()

    Public Property UpdatedBy As String

    Public Property UpdatedOn As DateTime = DateTime.Now()

    Public Overridable Property UserRole() As UserRole
    Public Overridable Property UserRolePermission() As UserRolePermission
End Class


<Table("UserAnalysisMapping")> _
Public Class UserAnalysisMapping
    <Key()> _
   <DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)> _
    Public Property UserAnalysisMappingId As Integer

    Public Property UserId As String

    Public Property AnalysisId As Integer

    Public Property CratedBy As String

    Public Property CratedOn As DateTime = DateTime.Now()

    Public Property UpdatedBy As String

    Public Property UpdatedOn As DateTime = DateTime.Now()

    Public Overridable Property User() As User

    Public Overridable Property Analysis() As Analysis

End Class

<Table("CustomerResponseMapping")> _
Public Class CustomerResponseMapping
    <Key()> _
   <DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)> _
    Public Property CustomerResponseMappingId As Integer

    Public Property RequestId As Integer

    Public Property CustomerId As Integer

    Public Property CratedBy As String

    Public Property CratedOn As DateTime = DateTime.Now()

    Public Property UpdatedBy As String

    Public Property UpdatedOn As DateTime = DateTime.Now()

    Public Overridable Property CustomerRequestType() As CustomerRequestType

    Public Overridable Property Customer() As Customer

End Class

