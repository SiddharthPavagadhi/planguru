﻿Imports System.ComponentModel.DataAnnotations

Public Class AcctType

    Public Property AcctTypeId As Integer
    <Required()>
    <StringLength(50)>
    Public Property TypeDesc As String
    <StringLength(50)>
    Public Property ClassDesc As String
    <StringLength(50)>
    Public Property SubclassDesc As String
    Public Overridable Property Accounts() As ICollection(Of Account)

End Class
