﻿Imports System.ComponentModel.DataAnnotations

Public Class Account
    Public Property AccountId As Integer
    <Required()>
    <StringLength(1)>
    Public Property AcctDescriptor As String
    <Required()>
    Public Property SortSequence As Integer
    <Required()>
    <StringLength(50)>
    Public Property Description As String
    <StringLength(50)>
    Public Property Subgrouping As String
    Public Property AcctTypeId As Integer
    Public Overridable Property AcctType() As AcctType
    Public Overridable Property Balances() As ICollection(Of Balance)

End Class
