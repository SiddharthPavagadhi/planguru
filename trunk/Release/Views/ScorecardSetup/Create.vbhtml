﻿@ModelType onlineAnalytics.Scorecard 
@Imports Farpoint.Web.Spread
@Imports Farpoint.Mvc.Spread
@Imports onlineAnalytics

@Code
    ViewData("Title") = "PlanGuru Analytics | Add Scorecard Item"
    If Not Session("action") Is Nothing Then
        
        If (Session("action") = "SetDefaultAnalysis" or Session("action") = "GetAnalyses") Then
            Layout = Nothing
            Session.Remove("action")
        End If
    Else
        Layout = "~/Views/Shared/AnalyticsMaster.vbhtml"
    End If
    
    Dim ua As New UserAccess
    If Not (Session("UserAccess") Is Nothing) Then
        ua = DirectCast(Session("UserAccess"), UserAccess)
    End If
End Code
<script src="@Url.Content("~/Scripts/jquery.validate.min.js")" type="text/javascript"></script>
<script src="@Url.Content("~/Scripts/jquery.validate.unobtrusive.min.js")" type="text/javascript"></script>
<div class="headerTable" style="float: right">
    <table>
        <tr>
            <td align="left" width="70%">
                <div class="breadscrum"><a href='@Url.Action("Index", "Scorecard")'>Scorecard</a><span> > </span><a href='@Url.Action("Index", "ScorecardSetup")'>Scorecard Item List</a><span> > </span>Add Scorecard Item</div>                
            </td>                        
        </tr>
    </table>
</div>
<div style="margin-bottom: 20px;">    
    @If Not (DirectCast(TempData("Message"), String) Is Nothing) Then
        @<label class="success">@TempData("Message").ToString()
        </label>                         
    End If
    @If Not (DirectCast(TempData("ErrorMessage"), String) Is Nothing) Then
        @<label class="error">@TempData("ErrorMessage").ToString()
        </label>                         
    End If    
    <div style="font-size: 13px; display: -moz-box; width: 99.9%; border: 1px solid #AAAAAA;">
        @Using Html.BeginForm("Create", "ScorecardSetup", "", FormMethod.Post, New With {.style = "width:1082px"})
            @Html.AntiForgeryToken()   
            @Html.ValidationSummary(True)       
            @<div>
                @Html.ValidationMessageFor(Function(m) m.AccountId, Nothing, New With {.style = "margin-left:20px;font-family:Arial;font-size: 0.85em;"})
                 @Html.HiddenFor(Function(m) m.AccountId)
                 @Html.HiddenFor(Function(m) m.AcctTypeId)
                 
            </div>
            @<div id="tabs" style="float: left; font-size:11px;min-height: 450px; height: auto;">
                <ul>
                    <li><a href="#tabs-1">Revenue and expenses</a></li>
                    <li><a href="#tabs-2">Balance Sheet</a></li>
                    <li><a href="#tabs-3">Cash flow</a></li>
                    <li><a href="#tabs-4">Financial ratios</a></li>
                    <li><a href="#tabs-5">Non-financial data</a></li>
                </ul>
                <div id="tabs-1">
                    <div id="REspread">
                        @Html.FpSpread("spdRE", Sub(x)
                                                    x.RowHeader.Visible = False
                                                    x.Height = 380
                                                    x.Width = 800
                                              
                                              
                                                End Sub)
                    </div>
                </div>
                <div id="tabs-2">
                    <div id="BSspread">
                        @Html.FpSpread("spdBS", Sub(x)
                                                    x.RowHeader.Visible = False
                                                    x.Height = 380
                                                    x.Width = 800
                                              
                                                End Sub)
                    </div>
                </div>
                <div id="tabs-3">
                    <div id="CFspread">
                        @Html.FpSpread("spdCF", Sub(x)
                                                    x.RowHeader.Visible = False
                                                    x.Height = 380
                                                    x.Width = 800                                              
                                                End Sub)
                    </div>
                </div>
                <div id="tabs-4">
                    <div id="FRspread">                    
                        @Html.FpSpread("spdFR", Sub(x)
                                                         x.RowHeader.Visible = False
                                                         x.Height = 380
                                                         x.Width = 800
                                                     End Sub)
                    </div>
                </div>
                <div id="tabs-5">
                    <div id="OMspread">                    
                        @Html.FpSpread("spdOM", Sub(x)
                                                    x.RowHeader.Visible = False
                                                    x.Height = 380
                                                    x.Width = 800                                              
                                                End Sub)
                    </div>
                </div>
            </div>
            @<div style="float:right;width:22%;display:inline-block;">
                <fieldset style="min-height: 441px; height: auto; border: none;">
                    <div style="float:left;">
                        <ol class="round">
                            <li>
                                @Html.LabelFor(Function(m) m.Missed)<br />
                                @Html.Label("-")
                                @Html.TextBoxFor(Function(m) m.Missed,New With {.class = "scorecard_text"} )<br />
                                @Html.ValidationMessageFor(Function(m) m.Missed)
                            </li>
                            <li>
                                @Html.LabelFor(Function(m) m.Outperform)<br />
                                @Html.Label("+")
                                @Html.TextBoxFor(Function(m) m.Outperform,New With {.class = "scorecard_text"})<br />   
                                @Html.ValidationMessageFor(Function(m) m.Outperform)
                            </li>                           
                            <li id="ShowPercent">
                                @Html.LabelFor(Function(m) m.ShowasPercent, New With {.Id = "lblShowPercent"})
                                @Html.CheckBoxFor(Function(m) m.ShowasPercent, New With {.class = "styled_checkbox", .Id = "ShowPercentChk"})<br />                                
                            </li>
                            <li id="IsFavorable">
                                @Html.LabelFor(Function(m) m.VarIsFavorable, New With {.Id = "lblIsFavorable"})
                                @Html.DropDownList("VarIsFavorable", DirectCast(ViewBag.PositiveVariance, SelectList), New With {.Class = "select", .style = "width:200px;"})<br />                               
                                @Html.ValidationMessageFor(Function(m) m.VarIsFavorable)
                            </li>
                            <li>
                                <input id="causepost" type="submit" value="Add Scorecard Item" class="secondbutton_example" />
                            </li>
                        </ol>
                    </div>
                </fieldset>              
            </div>            
        End Using        
    </div>
</div>
<script type="text/javascript">   

    var selectedElement = "";
    var selectedElementDescription = "";

    $(".success").attr("style", "margin:0;padding: 10px 20px 10px 50px;");
    $(".error").attr("style", "margin:0;padding: 10px 20px 10px 50px;");

    $("#Missed").val(10);
    $("#Outperform").val(10);

    $("#IsFavorable").hide();

    function DSChkBoxClicked(checkedElement, selectedAccount, selectedAcctType, Description) {
        var chkElement = "#" + checkedElement

        if ($(chkElement).is(':checked')) {
            Checked(chkElement, selectedAccount, selectedAcctType, Description);
        }
        else {
            selectedElement = ""; $("#AccountId").val(''); $("#AcctTypeId").val('');
        }
    }

    function Checked(chkElement, selectedAccount, selectedAcctType, Description) {
        if ($("#AccountId").val() == "" || $("#AccountId").val() == 0) {

            selectedElement = chkElement;
            selectedElementDescription = Description;
            $("#AccountId").val(selectedAccount);
            $("#AcctTypeId").val(selectedAcctType);
        }
        else {

            var confirmationMessage = "''" + selectedElementDescription + "'' has already been selected for this scorecard chart. A scorecard chart can only include one item. Do you want to deselect the previously selected item?";

            $.msgBox({
                title: "Confirm",
                content: confirmationMessage,
                type: "Info",
                buttons: [{ type: "submit", value: "Yes" },
                          { type: "submit", value: "No"}],
                afterShow: function () { $('[name=No]').focus(); $(".msgBox").css("height", "230px"); $(".msgBoxButtons").css("padding-top", "45px"); },
                success: function (result) {
                    if (result == "Yes") {
                        $(selectedElement).attr('checked', false);
                        $(chkElement).attr('checked', true);
                        $("#AccountId").val(''); selectedElement = ''; selectedElementDescription = '';
                        Checked(chkElement, selectedAccount, selectedAcctType, Description);
                    }
                    else if (result == "No") {
                        $(chkElement).attr('checked', false);
                    }
                }
            });

        }
    }

    $('#ShowPercentChk').change(function () {
        if ($(this).is(':checked')) { 
            var activeTab = $("#tabs").tabs("option", "active");
            if (activeTab == 0 || activeTab == 1) {
                $("#IsFavorable").show();  
            }
        }
        else {
            var activeTab = $("#tabs").tabs("option", "active");
            if (activeTab == 0 || activeTab == 1) {
                $("#IsFavorable").hide();
            }
        }
    });

    $(function () {

        $("#tabs").tabs();
        $("#tabs").attr("class", "ui-tabs ui-widget");

        $(document).ready(function () {

            CallShowPercent();

            $('#tabs').tabs({
                activate: function (event, ui) {
                    CallShowPercent();
                }
            });

            function CallShowPercent() {
                var activeTab = $("#tabs").tabs("option", "active");
                if (activeTab == 0 || activeTab == 1) {
                    $("#ShowPercent").show();
                    if ($("#ShowPercentChk").is(':checked')) {
                        $("#IsFavorable").show();
                    }
                    else {
                        $("#IsFavorable").hide();
                    }                    
                    (activeTab == 0) ? $("#lblShowPercent").text("Show as % of Sales") : $("#lblShowPercent").text("Show as % of Assets");
                }
                else {
                    $("#ShowPercent").hide();
                    $("#IsFavorable").show();
                }
            }

            $("div > input:checkbox").each(function (index) {
                $("<label>").attr("for", this.id = "checkbox" + index)
                .attr("class", "styled_checkbox dashboard")
                .insertAfter(this);
            });

            $("li > input:checkbox").each(function (index) {
                $("<label>").attr("for", this.id)
                .attr("class", "float-left chk-dashboard")
                .insertAfter(this);
            });

            $("#tabs").removeClass("ui-corner-all");
            $("#footerpanel").removeClass("plangurufooter").addClass("plangurufooterfordashboard");
            IsSessionAlive();
            //hide detail rows  
            var spread = document.getElementById("spdRE");
            setRow(spread);

            var spread = document.getElementById("spdBS");
            setRow(spread);

            var spread = document.getElementById("spdCF");
            setRow(spread);

            //            var spread = document.getElementById("spdFR");
            //            setRow(spread);

            var spread = document.getElementById("spdOM");
            setRow(spread);

        });

        function setRow(spread) {

            var rc = spread.GetRowCount();
            var showclass = false
            for (var i = 0; i < rc; i++) {
                var cellval = spread.GetValue(i, 2);
                if (cellval == "Detail" || cellval == "SDetail" || cellval == "SHeading" || cellval == "STotal") {

                    if (spread.id == "spdOM") {
                        showclass = true
                        spread.Rows(i).style.display = "table-row";
                    }
                    else if (showclass == false) {
                        spread.Rows(i).style.display = "none";
                    }
                }
                if (cellval == "OpCash") {
                    if (showclass == false) {
                        spread.Rows(i).style.display = "none";
                    }
                }
                if (cellval == "Heading") {
                    var cellvalue = spread.GetValue(i, 0);
                    if (cellvalue == "-") {
                        showclass = true
                        spread.Rows(i).style.display = "table-row";
                    }
                    else {
                        showclass = false
                        spread.Rows(i).style.display = "none";
                    }
                }
            }
        }

        window.onload = function () {
            var spreadRE = document.getElementById("spdRE");
            var spreadBS = document.getElementById("spdBS");
            var spreadCF = document.getElementById("spdCF");
            var spreadFR = document.getElementById("spdFR");
            var spreadOM = document.getElementById("spdOM");

            tabEventListener(spreadRE);
            tabEventListener(spreadBS);
            tabEventListener(spreadCF);
            tabEventListener(spreadFR);
            tabEventListener(spreadOM);
        }

        function tabEventListener(spread) {
            if (document.all) {
                if (spread.addEventListener) {
                    spread.addEventListener("ActiveCellChanged", cellChanged, false);
                }
                else {
                    spread.onActiveCellChanged = cellChanged;
                }
            }
            else {
                spread.addEventListener("ActiveCellChanged", cellChanged, false);
            }
        }

        function cellChanged(event) {
            //IsSessionAlive();
            if (event.col == 0) {

                var spread = document.getElementById(event.target.id);  //document.getElementById("spdRE");
                var cellval = spread.GetValue(event.row, 0);
                var rc = spread.GetRowCount();

                if (cellval == "-") {
                    for (var i = event.row; i < rc; i++) {
                        var rowtype = spread.GetValue(i, 2);
                        if (rowtype == "Total") {
                            cell = document.getElementById(event.target.id).GetCellByRowCol(i, 0);
                            cell.removeAttribute("FpCellType");
                            spread.SetValue(i, 0, "+", false);
                            cell.setAttribute("FpCellType", "readonly");
                            spread.Rows(i).style.display = "table-row";
                            break;
                        }
                        else if (rowtype == "Heading") {
                            cell = document.getElementById(event.target.id).GetCellByRowCol(i, 0);
                            cell.removeAttribute("FpCellType");
                            spread.SetValue(i, 0, "+", false);
                            cell.setAttribute("FpCellType", "readonly");
                            spread.Rows(i).style.display = "none";
                        }
                        else {

                            spread.Rows(i).style.display = "none";
                        }
                    }
                }
                else if (cellval == "+") {
                    for (var i = event.row; i > -1; i--) {
                        var rowtype = spread.GetValue(i, 2);
                        if (rowtype == "Total") {
                            cell = document.getElementById(event.target.id).GetCellByRowCol(i, 0);
                            cell.removeAttribute("FpCellType");
                            spread.SetValue(i, 0, "", false);
                            cell.setAttribute("FpCellType", "readonly");
                            if ($('#blnUseFilter').is(':checked')) {
                                spread.Rows(i).style.display = "none";
                            }
                        }
                        else if (rowtype == "Heading") {
                            cell = document.getElementById(event.target.id).GetCellByRowCol(i, 0);
                            cell.removeAttribute("FpCellType");
                            spread.SetValue(i, 0, "-", false);
                            cell.setAttribute("FpCellType", "readonly");
                            spread.Rows(i).style.display = "table-row";
                            break
                        }
                        else if (rowtype == "SHeading") {
                            spread.Rows(i).style.display = "none";
                        }
                        else {
                            spread.Rows(i).style.display = "table-row";
                        }
                    }
                }
                spread.SetActiveCell(event.row, 1)
            }
        }
    });  
    $(window).load(function () {
        $(".wrapper").show();
    })
</script>

