﻿@ModelType onlineAnalytics.Analysis
@Imports System.Collections.Generic
@Imports MvcCheckBoxList.Model
@Imports  onlineAnalytics
@Code
    ViewData("Title") = "PlanGuru Analytics | Create New Analysis"
    If Not Session("action") Is Nothing Then
        
        If (Session("action") = "SetDefaultAnalysis" Or Session("action") = "GetAnalyses") Then
            Layout = Nothing
            Session.Remove("action")
        End If
    Else
        Layout = "~/Views/Shared/AnalyticsMaster.vbhtml"
    End If
    
    
    Dim selectedCompanyFromDropdown As Integer
    Dim ua As New UserAccess
    If Not (Session("UserAccess") Is Nothing) Then
        ua = DirectCast(Session("UserAccess"), UserAccess)
    End If
    If Not (Session("SelectedCompanyFromDropdown") Is Nothing) Then
        selectedCompanyFromDropdown = Session("SelectedCompanyFromDropdown")
    End If
    
End Code
<script src="@Url.Content("~/Scripts/jquery.validate.min.js")" type="text/javascript"></script>
<script src="@Url.Content("~/Scripts/jquery.validate.unobtrusive.min.js")" type="text/javascript"></script>
<div class="headerTable" style="float: right">
    <table>
        <tr>
            <td align="left" width="70%">
                <div class="breadscrum">
                    <a href='@Url.Action("Index", "Analysis")'>Analysis List</a><span> > </span>Create
                    Analysis</div>
            </td>
        </tr>
    </table>
</div>
<br />
<br />
<div class="center-panel">
    @Using Html.BeginForm("Create", "Analysis")
        @Html.AntiForgeryToken()   
        @Html.ValidationSummary(True)    
     
        If (Not IsNothing(Model) AndAlso selectedCompanyFromDropdown > 0) Then
        @<fieldset>
            <div style="width: 100%;">
                <div style="float: left; width: 33%;">
                    <ol class="round">
                        <li>
                            @Html.LabelFor(Function(m) m.AnalysisName)<br />
                            @Html.TextBoxFor(Function(m) m.AnalysisName)<br />
                            @Html.ValidationMessageFor(Function(m) m.AnalysisName)
                        </li>
                    </ol>
                </div>
                <div style="float: left; width: 36%;">
                    <ol class="round">
                        <li>
                            @Html.LabelFor(Function(m) m.CompanyId, "Company")<br />
                            @Html.EditorFor(Function(m) m.CompanyName)<br />
                            @Html.HiddenFor(Function(m) m.CompanyId)
                            @*@Html.DropDownList("CompanyId", DirectCast(ViewBag.SelectedCompany, SelectList), "Select Company", New With {.id = "analysis_CompanyId", .Class = "select", .Style = "width:250px;"})<br />*@
                        </li>
                    </ol>
                </div>
                <div style="float: left; width: 92%;">
                    <ol class="round">
                        <li>
                            @Html.LabelFor(Function(m) m.Option1, "Show shared analysis dashboard")<br />
                            @Html.CheckBoxFor(Function(m) m.Option1, New With {.class = "analysis_checkbox"})
                        </li>
                    </ol>
                </div>
                @If (ua.UserAnalysisMapping) Then
                    @<div class="associate_user">
                        @Code
                Dim htmlListInfo = New HtmlListInfo(HtmlTag.table, 5, New With {.class = "styled_list"}, TextLayout.Default, TemplateIsUsed.No)
                                                        
                Dim checkBoxAtt = New With {.class = "styled_checkbox user"}
                        End Code
                        <ol class="round">
                            <li>
                                @Html.LabelFor(Function(m) m.PostedUsers.UserIds) analysis
                                @Html.CheckBoxListFor(Function(u) u.PostedUsers.UserIds, Function(u) u.AvailableUsers, Function(u) u.Id, Function(u) u.Name, Function(u) u.SelectedUsers, checkBoxAtt, htmlListInfo, Nothing, Function(u) u.Tag)
                            </li>
                        </ol>
                    </div>
                End If
                <div class="input-form">
                    <input type="submit" value="Create Analysis" class="secondbutton_example" />
                    <input id="cancel-button" type="button" value="Cancel" class="cancelbutton" />
                </div>              
            </div>
        </fieldset>  
         End If
        @<div>
             @If Not (DirectCast(TempData("InfoMessage"), String) Is Nothing) Then
                 @<label class="info">@TempData("InfoMessage").ToString()</label>
             End If           
        </div>    
        @<div style="float: left;">
            @If Not (DirectCast(TempData("Message"), String) Is Nothing) Then
                @<label class="success">@TempData("Message").ToString()
                </label>                         
            End If
            @If Not (DirectCast(TempData("ErrorMessage"), String) Is Nothing) Then
                @<label class="error">@TempData("ErrorMessage").ToString()
                </label>                         
            End If
        </div>       
    End Using
</div>
<script type="text/javascript" language="javascript">

    $(document).ready(function () {

        $("input:checkbox").each(function (index) {
            $("<label>").attr("for", this.id)
                .attr("class", "analysis")
                .insertAfter(this);
        });

        $('[user="SAU"]').attr("class", "userlist_checkbox user SAU");
        $('[user="CAU"]').attr("class", "userlist_checkbox user CAU");
        $('[user="CRU"]').attr("class", "userlist_checkbox user CRU");

        $('[user="SAU"]').parent().prepend('<img src="@Url.Content("~/Content/Images/SAU.png")" style="height:25px;margin:0 0 0 2px;"/><br/>');
        $('[user="CAU"]').parent().prepend('<img src="@Url.Content("~/Content/Images/CAU.png")" style="height:25px;margin:0 0 0 2px;"/><br/>');
        $('[user="CRU"]').parent().prepend('<img src="@Url.Content("~/Content/Images/CRU.png")" style="height:25px;margin:0 0 0 2px;"/><br/>');

        $("#CompanyName").attr("disabled", true);

    });
    $("#cancel-button").click(function () {
        window.location = '@Url.Action("Index", "Analysis")';
    });

    $(window).load(function () {
        $(".wrapper").show();
    })
</script>
