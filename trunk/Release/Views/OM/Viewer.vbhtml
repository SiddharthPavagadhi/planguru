﻿@ModelType  onlineAnalytics.ReportHighChart
@Imports Farpoint.Web.Spread
@Imports Farpoint.Mvc.Spread
@Imports  onlineAnalytics
@Code    
    ViewData("Title") = "PlanGuru Analytics | Non-financial Data Saved Report"
    If Not Session("action") Is Nothing Then
        
        If (Session("action") = "SetDefaultAnalysis" Or Session("action") = "GetAnalyses") Then
            Layout = Nothing
            Session.Remove("action")
        End If
    Else
        Layout = "~/Views/Shared/AnalyticsMaster.vbhtml"
    End If
    
    Dim UserInfo As User = Nothing
    Dim ua As New UserAccess
    
    Dim objSetup As New SetupCount
    
    If Not (Session("UserInfo") Is Nothing) Then
        UserInfo = DirectCast(Session("UserInfo"), User)
    End If
    
    If (Not ViewBag.Setup Is Nothing) Then
        objSetup = DirectCast(ViewBag.Setup, SetupCount)
    End If
End Code
<script src="@Url.Content("~/Scripts/Highcharts.js")" type="text/javascript"></script>
<script src="@Url.Content("~/Scripts/Exporting.js")" type="text/javascript"></script>
<script src="@Url.Content("~/Scripts/regression.js")" type="text/javascript"></script>
<script src="@Url.Content("~/Scripts/Highslide-full.min.js")" type="text/javascript"></script>
<script src="@Url.Content("~/Scripts/Highslide.config.js")" type="text/javascript"></script>
<link href="@Url.Content("~/Content/css/highslide.css")" rel="stylesheet" type="text/css" />
<script type="text/javascript">
    var donutData = new Array();
    var counter = 0;
    var donutChartTitle = "";
    var donutChartIndex;
    var numberOfColumns;
    var showAsPercentageForDonut = false;
    $(function () {
        window.ViewerControlId = "#OMView"; 
        var model = @Html.Raw(Json.Encode(Model));

        hs.Expander.prototype.onAfterExpand = addChart;

        function addChart() {
            var finalData = new Array();
            var chart = $("#hc-test" + counter).highcharts();
            if (chart) {
                chart.destroy();
            }
            for(var k = 0 ; k < donutData.length ; k++)
            {
                if((k == donutChartIndex) || ((k % numberOfColumns) == donutChartIndex))
                {
                    if(donutData[k].y >= 0)
                    {
                        finalData.push(donutData[k]);
                    }
                }

            }

            $("#hc-test" + counter).highcharts({

                chart: {
                    type: 'pie'
                },
                title: {
                    text: donutChartTitle,
                    style: {
                        color: '#000000',
                        fontWeight: 'bold',
                        backgroundColor: '#d3d3d3',
                        fontSize: '16px'
                    }
                },
                yAxis: {

                },
                plotOptions: {
                    pie: {
                        shadow: false
                    }
                },
                tooltip: {



                    formatter: function() {
                        if(showAsPercentageForDonut == false)
                        {
                            return this.point.name +': <b>' + formatcurrency(this.y,0) + ' : '+ Math.round(this.percentage*100)/100 + ' % </b>';
                        }
                        else
                        {
                            return this.point.name +':<b>' + formatcurrency(this.y,2) + '%</b>';
                        }
                    }
                },
                credits: {
                    enabled : false
                },
                exporting: {
                    enabled: false
                },
                series: [{
                    data: finalData,
                    size: '100%',
                    innerSize: '40%',
                    showInLegend:true,
                    dataLabels: {
                        enabled: false
                    }
                }]
            });


        }

        function formatcurrency(value, decPlaces) {
            if (decPlaces != 0) {
                return value.toFixed(decPlaces).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString();
            }
            else {
                return parseInt(value).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
            }
        }

        function GetTrendComment(YaxisData, percentLabel, intFormat,isThousand, decPlaces) {
            var comment;
            if (YaxisData.length > 1) {
                var differenceValue = (fitData(YaxisData).data[1][1] - fitData(YaxisData).data[0][1]);
                if (percentLabel == false) {
                    if (differenceValue > 0) {
                        comment = "Trend - Increasing at rate of " + differenceValue.toFixed(decPlaces);
                    }
                    else if (differenceValue < 0) {
                        comment = "Trend - Decreasing at rate of " + (-differenceValue).toFixed(decPlaces);
                    }
                    else if (differenceValue == 0) {
                        comment = "Trend - Increasing at rate of " + 0;
                    }
                }
                else {
                    if (differenceValue > 0) {
                        comment = "Trend - Increasing at rate of " + (differenceValue).toFixed(decPlaces);
                    }
                    else if (differenceValue < 0) {
                        comment = "Trend - Decreasing at rate of " + ((-differenceValue)).toFixed(decPlaces);
                    }
                    else if (differenceValue == 0) {
                        comment = "Trend - Increasing at rate of " + 0;
                    }
                }

                if (percentLabel == true) {
                    comment = comment + "%"
                }

                if(isThousand == true)
                {
                    comment = comment + " thousand"
                }

                if (intFormat == 0 || intFormat == 2 || intFormat == 3 || intFormat == 5 || intFormat == 6 || intFormat == 9) {
                    comment = comment + " per month";
                }
                else {
                    comment = comment + " per year";
                }
            }
            else if (percentLabel == true) {
                comment = "Trend - Increasing at rate of " + 0 + "% per month";
            }
            else {
                comment = "Trend - Increasing at rate of " + 0 + " per month";
            }
            return comment;
        }

        function GetSeriesColor(SeriesIndex)
        {
            var colorCode;
            switch (SeriesIndex) {
                case 0:
                    colorCode = '#1083a3';
                    break;
                case 1:
                    colorCode = '#f19632';
                    break;
                case 2:
                    colorCode = '#66b09d';
                    break;
                case 3:
                    colorCode = '#dd5822';
                    break;
                case 4:
                    colorCode = '#0e6684';
                    break;
                case 5:
                    colorCode = '#eecf4d';
                    break;
                case 6:
                    colorCode = '#90ac47';
                    break;
                case 7:
                    colorCode = '#c7341f'
                    break;
                case 8:
                    colorCode = '#139fc2'
                    break;
                case 9:
                    colorCode = '#f27723'
                    break;
                case 10:
                    colorCode = '#1083a3';
                    break;
                case 11:
                    colorCode = '#f19632';
                    break;
                case 12:
                    colorCode = '#66b09d';
                    break;
                case 13:
                    colorCode = '#dd5822';
                    break;
                case 14:
                    colorCode = '#0e6684';
                    break;
                case 15:
                    colorCode = '#eecf4d';
                    break;
                case 16:
                    colorCode = '#90ac47';
                    break;
                case 17:
                    colorCode = '#c7341f'
                    break;
                case 18:
                    colorCode = '#139fc2'
                    break;
                case 19:
                    colorCode = '#f27723'
                    break;
                case 20:
                    colorCode = '#1083a3';
                    break;
                case 21:
                    colorCode = '#f19632';
                    break;
                case 22:
                    colorCode = '#66b09d';
                    break;
                case 23:
                    colorCode = '#dd5822';
                    break;
                case 24:
                    colorCode = '#0e6684';
                    break;
                case 25:
                    colorCode = '#eecf4d';
                    break;
                case 26:
                    colorCode = '#90ac47';
                    break;
                case 27:
                    colorCode = '#c7341f'
                    break;
                case 28:
                    colorCode = '#139fc2'
                    break;
                case 29:
                    colorCode = '#f27723'
                    break;
                default:
                    colorCode = '#1083a3';
            }
            return colorCode;
        }
        function getCursorValue(selectedRow)
        {
            if(selectedRow > 1)
            {
                return 'pointer';
            }
            else
            {
                return null;
            }
        }

        function getCrossHairValue(selectedRow)
        {
            if(selectedRow > 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        function getLegendWidth(Format)
        {
            if(Format == 7 || Format == 12)
            {
                return 151;
            }
            else
            {
                return 141;
            }
        }


        if(model != null)
        {
            if(model.isHttpPost == false)
            {
                $("#chart").hide();
            }
            else
            {
                $("#chart").show();
                /* Chart width related Logic */
                if(model.Format == 0 || model.Format == 3)
                {
                    $("#chart").width(parseInt(876)).height(200);
                }
                if(model.Format == 2)
                {
                    $("#chart").width(parseInt(586)).height(200);
                }
                if(model.Format == 5)
                {
                    if(model.NumbofCols == 3 || model.NumbofCols == 7)
                    {
                        $("#chart").width((parseInt(model.chartWidth.replace('px','') - 69))).height(200);
                    }
                    else
                    {
                        $("#chart").width((parseInt(model.chartWidth.replace('px','') - 18))).height(200);
                    }
                }
                if(model.Format == 6 || model.Format == 7 || model.Format == 8 || model.Format == 9 || model.Format == 12)
                {
                    $("#chart").width((parseInt(model.chartWidth.replace('px','') - 18))).height(200);
                }
                if(model.Format == 10 || model.Format == 11)
                {
                    $("#chart").width((parseInt(model.chartWidth.replace('px','') - 75))).height(200);
                }
                /* End Chart width related Logic */
                var xAxisData = new Array();
                var series1yAxisData = new Object();
                var series2yAxisData = new Object();
                var trendComment;

                if(model.Format == 0 || model.Format == 2 || model.Format == 3)
                {
                    if(model.SeriesData != "" && model.SeriesData != null)
                    {
                        for (var k = 0 ; k < model.SeriesData[0].SeriesData.length; k++) {
                            xAxisData.push(model.SeriesData[0].SeriesData[k]);
                        }
                    }

                    if(model.Format == 0 || model.Format == 3)
                    {
                        if(model.SelectedRows.length == 1)
                        {
                            series1yAxisData.Name = model.SeriesData[0].SeriesTitle;
                            series1yAxisData.Data = new Array();
                            for(var j = 0 ; j < 2 ; j++)
                            {
                                series1yAxisData.Data.push(model.OptionalYAxisData[j]);
                            }

                            series2yAxisData.Name = model.SeriesData[1].SeriesTitle;
                            series2yAxisData.Data = new Array();
                            for(var j = 2 ; j < 4 ; j++)
                            {
                                series2yAxisData.Data.push(model.OptionalYAxisData[j]);
                            }

                        }

                        else if(model.SelectedRows.length == 2)
                        {
                            series1yAxisData.Name = model.SeriesData[0].SeriesTitle;
                            series1yAxisData.Data = new Array();
                            for(var j = 0 ; j < 4 ; j++)
                            {
                                series1yAxisData.Data.push(model.OptionalYAxisData[j]);
                            }

                            series2yAxisData.Name = model.SeriesData[1].SeriesTitle;
                            series2yAxisData.Data = new Array();
                            for(var j = 4 ; j < 8 ; j++)
                            {
                                series2yAxisData.Data.push(model.OptionalYAxisData[j]);
                            }

                        }
                        else if(model.SelectedRows.length > 2)
                        {
                            series1yAxisData.Name = model.SeriesData[0].SeriesTitle;
                            series1yAxisData.Data = new Array();
                            for(var j = 0 ; j < 6 ; j++)
                            {
                                series1yAxisData.Data.push(model.OptionalYAxisData[j]);
                            }

                            series2yAxisData.Name = model.SeriesData[1].SeriesTitle;
                            series2yAxisData.Data = new Array();
                            for(var j = 6 ; j < 12 ; j++)
                            {
                                series2yAxisData.Data.push(model.OptionalYAxisData[j]);
                            }
                        }

                    }

                    if(model.Format == 2)
                    {
                        if(model.SelectedRows.length == 1)
                        {
                            series1yAxisData.Name = model.SeriesData[0].SeriesTitle;
                            series1yAxisData.Data = new Array();
                            for(var j = 0 ; j < 2 ; j++)
                            {
                                series1yAxisData.Data.push(model.OptionalYAxisData[j]);
                            }

                            series2yAxisData.Name = model.SeriesData[1].SeriesTitle;
                            series2yAxisData.Data = new Array();
                            for(var j = 2 ; j < 4 ; j++)
                            {
                                series2yAxisData.Data.push(model.OptionalYAxisData[j]);
                            }

                        }

                        else if(model.SelectedRows.length == 2)
                        {
                            series1yAxisData.Name = model.SeriesData[0].SeriesTitle;
                            series1yAxisData.Data = new Array();
                            for(var j = 0 ; j < 2 ; j++)
                            {
                                series1yAxisData.Data.push(model.OptionalYAxisData[j]);
                            }

                            series2yAxisData.Name = model.SeriesData[1].SeriesTitle;
                            series2yAxisData.Data = new Array();
                            for(var j = 2 ; j < 4 ; j++)
                            {
                                series2yAxisData.Data.push(model.OptionalYAxisData[j]);
                            }

                        }
                        else if(model.SelectedRows.length > 2)
                        {
                            series1yAxisData.Name = model.SeriesData[0].SeriesTitle;
                            series1yAxisData.Data = new Array();
                            for(var j = 0 ; j < 3 ; j++)
                            {
                                series1yAxisData.Data.push(model.OptionalYAxisData[j]);
                            }

                            series2yAxisData.Name = model.SeriesData[1].SeriesTitle;
                            series2yAxisData.Data = new Array();
                            for(var j = 3 ; j < 6 ; j++)
                            {
                                series2yAxisData.Data.push(model.OptionalYAxisData[j]);
                            }
                        }
                    }




                    $("#chart").highcharts({

                        chart: {
                            backgroundColor: "#f7f6f4",
                            type: model.HighChartType,
                        },
                        title: {
                            text: model.ChartDesc,
                            style: {
                                color: '#276FA6',
                                fontWeight: 'bold',
                                backgroundColor: '#d3d3d3',
                                fontSize: '14px'
                            }
                        },
                        xAxis: {
                            categories: xAxisData,
                            lineColor: '#000000',
                            tickLength: 8,
                            tickWidth: 1,
                            tickColor: "#000000"
                        },

                        yAxis: {
                            lineWidth: 1,
                            lineColor: '#000000',
                            gridLineColor: '#000000',
                            gridLineWidth: 1,
                            tickLength: 8,
                            tickWidth: 1,
                            tickColor: "#000000",
                            labels: {
                                formatter: function () {
                                    if(model.ShowasPerc == false)
                                    {
                                        return formatcurrency(this.value, 0);
                                    }
                                    else
                                    {
                                        return (Math.round(this.value * 100) + '%');
                                    }
                                }
                            },
                            title: {
                                text: ''
                            }

                        },

                        tooltip: {
                            formatter: function () {
                                if(model.ShowasPerc == false)
                                {
                                    return '<b>' + this.series.name + '</b><br/>' + this.key + ' :<b> ' + formatcurrency(this.y, 0) + '</b>';
                                }
                                else
                                {
                                    return '<b>' + this.series.name + '</b><br/>' + this.key + ' :<b> ' + Highcharts.numberFormat((this.y * 100), 2, '.') + '%</b>';
                                }
                            }
                        },
                        credits: {
                            enabled: false
                        },
                        exporting:
                            {
                                enabled: false
                            },
                        series: [{
                            data: series1yAxisData.Data,
                            name: series1yAxisData.Name,
                            color: '#1083a3'
                        },
                        {
                            data: series2yAxisData.Data,
                            name: series2yAxisData.Name,
                            color: '#f19632'
                        }],
                        legend: {
                            align:'left',
                            verticalAlign: 'top',
                            floating: false,
                            layout:'vertical'
                        }
                    });
                    if(model.Format == 0 || model.Format == 3)
                    {
                        $("#chart").highcharts().renderer.text(model.SubLabels[0].toString(), 280, 195)
                        .css({
                            color: '#000000',
                            fontSize: '12px',
                            fontWeight: 600
                        }).add();

                        $("#chart").highcharts().renderer.text(model.SubLabels[1].toString(), 660, 195)
                        .css({
                            color: '#000000',
                            fontSize: '12px',
                            fontWeight: 600
                        }).add();
                    }
                }
                else
                {

                    var cursorValue = getCursorValue(model.SelectedRows.length);
                    var crossHairValue = getCrossHairValue(model.SelectedRows.length);
                    var legendWidth = getLegendWidth(model.Format);

                    if(model.SeriesData != "" && model.SeriesData != null)
                    {
                        for (var k = 0 ; k < model.SeriesData[0].SeriesData.length; k++) {
                            xAxisData.push(model.SeriesData[0].SeriesData[k]);
                        }
                    }

                    if(model.ChartType == 3)
                    {

                        $("#chart").highcharts({

                            chart: {
                                backgroundColor: "#f7f6f4",
                                type: model.HighChartType,
                                spacingRight: 65
                            },
                            title: {
                                text: model.ChartDesc,
                                style: {
                                    color: '#276FA6',
                                    fontWeight: 'bold',
                                    backgroundColor: '#d3d3d3',
                                    fontSize: '14px'
                                }
                            },
                            xAxis: {
                                categories: xAxisData,
                                lineColor: '#000000',
                                tickLength: 8,
                                tickWidth: 1,
                                tickColor: "#000000"
                            },

                            yAxis: {
                                lineWidth: 1,
                                lineColor: '#000000',
                                gridLineColor: '#000000',
                                gridLineWidth: 1,
                                tickLength: 8,
                                tickWidth: 1,
                                tickColor: "#000000",
                                labels: {
                                    formatter: function () {
                                        if(model.ShowasPerc == false)
                                        {
                                            return formatcurrency(this.value, 0);
                                        }
                                        else
                                        {
                                            return this.value + '%';
                                        }
                                    }
                                },
                                title: {
                                    text: ''
                                }

                            },
                            tooltip: {
                                formatter: function () {
                                    if(model.ShowasPerc == false)
                                    {
                                        return '<b>' + this.series.name + '</b><br/>' + this.key + ' :<b> ' + formatcurrency(this.y, 0) + '</b>';
                                    }
                                    else
                                    {
                                        return '<b>' + this.series.name + '</b><br/>' + this.key + ' :<b> ' + formatcurrency(this.y, 2) + '%</b>';
                                    }
                                },
                                crosshairs: crossHairValue
                            },
                            plotOptions: {
                                column: {
                                    stacking: 'normal',
                                    dataLabels: {
                                        enabled: false,
                                        color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
                                        style: {
                                            textShadow: '0 0 3px black'
                                        }
                                    }
                                },
                                series:{
                                    cursor:cursorValue

                                }
                            },

                            credits: {
                                enabled: false
                            },
                            exporting:
                                {
                                    enabled: false
                                },
                            series: [

                            ],
                            legend: {
                                align:'left',
                                verticalAlign: 'top',
                                floating: false,
                                layout:'vertical',
                                width:legendWidth,
                                labelFormatter: function() {
                                    var words = this.name.split(/[\s]+/);
                                    var numWordsPerLine = 2;
                                    var str = [];

                                    for (var word in words) {
                                        if (word > 0 && word % numWordsPerLine == 0)
                                            str.push('<br>');

                                        str.push(words[word]);
                                    }

                                    return str.join(' ');
                                }
                            }
                        });
                    }
                    else if(model.showTrend == true && model.ChartType == 4)
                    {
                        var singleSeriesData = new Array();
                        for(var j = 0; j < model.NumbofCols; j++)
                        {
                            singleSeriesData.push(model.ChartValues[j]);
                        }
                        trendComment = GetTrendComment(singleSeriesData,model.ShowasPerc,model.Format,model.isThousand,2)
                        $("#chart").highcharts({

                            chart: {
                                backgroundColor: "#f7f6f4",
                                type: model.HighChartType,
                                spacingRight: 65
                            },
                            title: {
                                text: model.ChartDesc,
                                style: {
                                    color: '#276FA6',
                                    fontWeight: 'bold',
                                    backgroundColor: '#d3d3d3',
                                    fontSize: '14px'
                                }
                            },
                            xAxis: {
                                categories: xAxisData,
                                lineColor: '#000000',
                                tickLength: 8,
                                tickWidth: 1,
                                tickColor: "#000000"
                            },

                            yAxis: {
                                lineWidth: 1,
                                lineColor: '#000000',
                                gridLineColor: '#000000',
                                gridLineWidth: 1,
                                tickLength: 8,
                                tickWidth: 1,
                                tickColor: "#000000",
                                labels: {
                                    formatter: function () {
                                        if(model.ShowasPerc == false)
                                        {
                                            return formatcurrency(this.value, 0);
                                        }
                                        else
                                        {
                                            return this.value + '%';
                                        }
                                    
                                    }
                                },
                                title: {
                                    text: ''
                                }

                            },
                            tooltip: {
                                formatter: function () {
                                    if(model.ShowasPerc == false)
                                    {
                                        return '<b>' + this.series.name + '</b><br/>' + this.key + ' :<b> ' + formatcurrency(this.y, 0) + '</b>';
                                    }
                                    else
                                    {
                                        return '<b>' + this.series.name + '</b><br/>' + this.key + ' :<b> ' + formatcurrency(this.y, 2) + '%</b>';
                                    }
                                }
                            },

                            credits: {
                                enabled: true,
                                text: trendComment,
                                position: {
                                    align: 'center'
                                },
                                style: {
                                    color: 'black',
                                    fontSize: '12px',
                                    marginTop: '5px',
                                    marginBottom: '5px'
                                },
                                href: '#'
                            },
                            exporting:
                                {
                                    enabled: false
                                },
                            series: [

                            ],
                            legend: {
                                align:'left',
                                verticalAlign: 'top',
                                floating: false,
                                layout:'vertical',
                                width:legendWidth,
                                labelFormatter: function() {
                                    var words = this.name.split(/[\s]+/);
                                    var numWordsPerLine = 2;
                                    var str = [];

                                    for (var word in words) {
                                        if (word > 0 && word % numWordsPerLine == 0)
                                            str.push('<br>');

                                        str.push(words[word]);
                                    }

                                    return str.join(' ');
                                }
                            }
                        });
                    }
                    else
                    {
                        $("#chart").highcharts({

                            chart: {
                                backgroundColor: "#f7f6f4",
                                type: model.HighChartType,
                                spacingRight: 65
                            },
                            title: {
                                text: model.ChartDesc,
                                style: {
                                    color: '#276FA6',
                                    fontWeight: 'bold',
                                    backgroundColor: '#d3d3d3',
                                    fontSize: '14px'
                                }
                            },
                            xAxis: {
                                categories: xAxisData,
                                lineColor: '#000000',
                                tickLength: 8,
                                tickWidth: 1,
                                tickColor: "#000000"
                            },

                            yAxis: {
                                lineWidth: 1,
                                lineColor: '#000000',
                                gridLineColor: '#000000',
                                gridLineWidth: 1,
                                tickLength: 8,
                                tickWidth: 1,
                                tickColor: "#000000",
                                labels: {
                                    formatter: function () {
                                        if(model.ShowasPerc == false)
                                        {
                                            return formatcurrency(this.value, 0);
                                        }
                                        else
                                        {
                                            return this.value + '%';
                                        }
                                    
                                    }
                                },
                                title: {
                                    text: ''
                                }

                            },
                            plotOptions: {
                                series:{
                                    cursor:cursorValue
                                }
                            },

                            tooltip: {
                                formatter: function () {
                                    if(model.ShowasPerc == false)
                                    {
                                        return '<b>' + this.series.name + '</b><br/>' + this.key + ' :<b> ' + formatcurrency(this.y, 0) + '</b>';
                                    }
                                    else
                                    {
                                        return '<b>' + this.series.name + '</b><br/>' + this.key + ' :<b> ' + formatcurrency(this.y, 2) + '%</b>';
                                    }
                                },
                                crosshairs: crossHairValue

                            },

                            credits: {
                                enabled: false
                            },
                            exporting:
                                {
                                    enabled: false
                                },
                            series: [

                            ],
                            legend: {
                                align:'left',
                                verticalAlign: 'top',
                                floating: false,
                                layout:'vertical',
                                width:legendWidth,
                                labelFormatter: function() {
                                    var words = this.name.split(/[\s]+/);
                                    var numWordsPerLine = 2;
                                    var str = [];

                                    for (var word in words) {
                                        if (word > 0 && word % numWordsPerLine == 0)
                                            str.push('<br>');

                                        str.push(words[word]);
                                    }

                                    return str.join(' ');
                                }
                            }
                        });
                    }

                    if(model.showTrend == false)
                    {

                        if(model.Format == 5 || model.Format == 6 || model.Format == 9 || model.Format == 7 || model.Format == 8 || model.Format == 12)
                        {

                            var i = model.NumbofCols;
                            var l = 1;
                            if(l <= i)
                            {
                                for(var k = 0 ; k < model.SelectedRows.length ; k++)
                                {


                                    var singleSeriesData = new Array();
                                    var seriesColor = GetSeriesColor(k);
                                    var name = model.SeriesData[k].SeriesTitle.replace(/\n/g, " ");
                                    if(l == 1)
                                    {
                                        for(var j = ((i * l) - i) ; j < (i * l) ; j++)
                                        {
                                            singleSeriesData.push(model.ChartValues[j]);
                                            donutData.push({name:name,y:model.ChartValues[j], color: seriesColor });
                                        }
                                    }
                                    else if(l > 1)
                                    {
                                        for(var j = (((i * l) - i) + (l - 1)) ; j < ((i * l) + (l - 1)) ; j++)
                                        {
                                            singleSeriesData.push(model.ChartValues[j]);
                                            donutData.push({name:name,y:model.ChartValues[j], color: seriesColor });
                                        }
                                    }
                                    var data = singleSeriesData;
                                    if(model.SelectedRows.length > 1)
                                    {
                                        $("#chart").highcharts().addSeries({name : name , data : data, color: seriesColor,
                                            events: {
                                                click: function (event)
                                                {
                                                    counter++;
                                                    donutChartIndex = event.point.index;
                                                    if(model.isThousand == true)
                                                    {
                                                        donutChartTitle = event.point.category +  " (in thousands)";
                                                    }
                                                    else
                                                    {
                                                        donutChartTitle = event.point.category;
                                                    }
                                                    numberOfColumns = model.NumbofCols;
                                                    showAsPercentageForDonut = model.ShowasPerc;
                                                    hs.htmlExpand(null, {
                                                        pageOrigin: {
                                                            x: (event.screenX),
                                                            y: (event.screenY)
                                                        },
                                                        maincontentText: '<div id="hc-test' + counter + '"></div>',
                                                        width: 500,
                                                        height: 453
                                                    });
                                                }
                                            }
                                        });
                                    }
                                    else
                                    {
                                        $("#chart").highcharts().addSeries({name : name , data : data, color: seriesColor});
                                    }
                                    l++;
                                }
                            }
                        }

                        else
                        {
                            var i = model.NumbofCols;
                            var l = 1;
                            if(l <= i)
                            {
                                for(var k = 0 ; k < model.SelectedRows.length ; k++)
                                {


                                    var singleSeriesData = new Array();
                                    var seriesColor = GetSeriesColor(k);
                                    var name = model.SeriesData[k].SeriesTitle.replace(/\n/g, " ");

                                    for(var j = ((i * l) - i) ; j < (i * l) ; j++)
                                    {
                                        singleSeriesData.push(model.ChartValues[j]);
                                        donutData.push({name:name,y:model.ChartValues[j],color: seriesColor });
                                    }

                                    var data = singleSeriesData;
                                    if(model.SelectedRows.length > 1)
                                    {
                                        $("#chart").highcharts().addSeries({name : name , data : data, color: seriesColor,
                                            events: {
                                                click: function (event)
                                                {
                                                    counter++;
                                                    donutChartIndex = event.point.index;
                                                    if(model.isThousand == true)
                                                    {
                                                        donutChartTitle = event.point.category +  " (in thousands)";
                                                    }
                                                    else
                                                    {
                                                        donutChartTitle = event.point.category;
                                                    }
                                                    numberOfColumns = model.NumbofCols;
                                                    showAsPercentageForDonut = model.ShowasPerc;
                                                    hs.htmlExpand(null, {
                                                        pageOrigin: {
                                                            x: (event.screenX),
                                                            y: (event.screenY)
                                                        },
                                                        maincontentText: '<div id="hc-test' + counter + '"></div>',
                                                        width: 530,
                                                        height: 473
                                                    });
                                                }
                                            }
                                        });
                                    }
                                    else
                                    {
                                        $("#chart").highcharts().addSeries({name : name , data : data, color: seriesColor})
                                    }
                                    l++;
                                }
                            }
                        }
                    }

                    if(model.showTrend == true && model.ChartType == 4)
                    {
                        var singleSeriesData = new Array();
                        var name = model.SeriesData[0].SeriesTitle.replace(/\n/g, " ");

                        for(var j = 0; j < model.NumbofCols; j++)
                        {
                            singleSeriesData.push(model.ChartValues[j]);
                        }

                        var data = singleSeriesData;

                        $("#chart").highcharts().addSeries({name : name , data : data,color: '#1083a3'})
                        $("#chart").highcharts().addSeries({
                            type: 'line',
                            name: 'Trend',
                            color: '#f37823',
                            marker: {
                                enabled: false
                            },
                            data: (function () {
                                return fitData(singleSeriesData).data;
                            })()
                        })
                    }

                }

                

                

            }
            
        }
        else
        {
            $("#chart").hide();
        }
    })

    $(window).load(function () {
        $(".wrapper").show();
    })
</script>
<div style="display: inline-block; width: 100%;">
    @If Not (DirectCast(TempData("Message"), String) Is Nothing) Then
        @<label class="success">@TempData("Message").ToString()
        </label>                         
    End If
    @If Not (DirectCast(TempData("ErrorMessage"), String) Is Nothing) Then
        @<label class="error" style="margin-top: 0px;">@TempData("ErrorMessage").ToString()
        </label>                         
    End If
</div>
<div id="outerwrapper" style="display:none">
    @code   
        @Html.Hidden("countOfDataUploaded", objSetup.countDataUploadedOfSelectedAnalysis)        
        @<label id="validationMessage" class="info" style="display: none;">
            @If ((Not IsNothing(ViewBag.Setup)) AndAlso objSetup.AanlysisCount > 0 And objSetup.selectedAnalysis = 0) Then
                @MvcHtmlString.Create("Please select company & one of the respective analysis from top-menu selection, to get the dashboard view.")
            ElseIf ((Not IsNothing(ViewBag.Setup)) AndAlso objSetup.selectedAnalysis > 0 And objSetup.countDataUploadedOfSelectedAnalysis = 0) Then
                @MvcHtmlString.Create("Data has not been uploaded for selected analysis.")
            End If
        </label> 
        
        If (Not UserInfo Is Nothing AndAlso Not IsNothing(ViewBag.Setup)) Then
                   
            If (UserInfo.UserRoleId <= UserRoles.SAU) Then
                    
                If (objSetup.CompanyCount = 0 Or objSetup.AanlysisCount = 0) Then
        @<label class="info">You have to create below listed action item, to get the dashboard
            view.</label> 
                End If
                If (objSetup.CompanyCount = 0) Then
        @<div class="secondbutton_example" style='margin: 20px; width: 200px;'>
            <a href='@Url.Action("Create", "Company")' style="text-decoration:none;color: #FFFFFF;">
                Add Company</a>
        </div>  
                End If
                If (objSetup.CompanyCount > 0 And objSetup.AanlysisCount = 0) Then
        @<div class="secondbutton_example" style='margin: 20px; width: 200px;'>
            <a href='@Url.Action("Create", "Analysis")'  style="text-decoration:none;color: #FFFFFF;">
                Add Analysis</a>
        </div>  
                End If
                If (objSetup.CompanyCount = 0 Or objSetup.AanlysisCount = 0) Then
        @<div class="secondbutton_example" style='margin: 20px; width: 200px;'>
            <a href='@Url.Action("CreateUser", "User")'  style="text-decoration:none;color: #FFFFFF;">
                Add User</a>
        </div>  
                End If
            ElseIf (UserInfo.UserRoleId > UserRoles.SAU) Then
                
                If (objSetup.CompanyCount = 0) Then
        @<label class="info">You have not been given access to any company. Please contact your
            administrator. / analysis.</label>
                ElseIf (objSetup.AanlysisCount = 0) Then
        @<label class="info">You have not been given access to any analysis files for the selected
            company. Please contact your administrator.</label> 
                End If
                
            End If
        End If
        
        If (objSetup.CompanyCount > 0 And objSetup.AanlysisCount > 0 And objSetup.selectedAnalysis > 0 And objSetup.countDataUploadedOfSelectedAnalysis > 0) Then
            Using Html.BeginForm
        @<div id="OMView" style="display: none; width: 100%;">
            <div id="sidebar">
                <div>
                    <img src='@Url.Content("~/Content/images/period.png")' alt="Period" />
                </div>
                <div>
                    @Html.DropDownList("intPeriod", DirectCast(ViewData("Periods"), SelectList), New With {.class = "REselect"})
                </div>
                <div id="UpdateChart" style="display: none; margin: 15px 0 0 0; float: left;">
                    <input id="causepost" type="submit" value="View Chart" class="secondbutton_example" />
                </div> 
                <div>
                    <h2 class="savedViewsListHeading">Saved Reports</h2>
                    <ul id="SavedReportsList" class="savedViewsList">
                    </ul>
                </div>                
            </div>
            <div id="main" style="display: inline-block; width: 79%;">
                <div id="chart" style="display: none;">
                    @Html.FpSpread("spdChart", Sub(x)
                                                   x.RowHeader.Visible = False
                                                   x.ColumnHeader.Visible = False
                                                   x.Height = 200
                                                   x.Width = 500
                                           
                                               End Sub)
                </div>
                <div id="spread" style="display: none;">
                    @Html.FpSpread("spdAnalytics", Sub(x)
                                                   
                                                       x.RowHeader.Visible = False
                                                       x.ActiveSheetView.PageSize = 1000
                                                   End Sub)
                </div>
            </div>
        </div>   
            End Using
        End If
       
    End Code
</div>
<script type="text/javascript"> 
        function setSelectedPageNav() {
            var pathName = document.location.pathname;            
            if ($("#SavedReportsList li a") != null) {                
                var currentLink = $("#SavedReportsList li a[href='" + pathName + "']");                
                currentLink.addClass("activelink");
            }
        }             
        $('#intPeriod').change(function () {            
            $('#causepost').trigger('click')
        });

        //on load
        $(document).ready(function () {                            
            IsSessionAlive();
            getSavedReportsList();

            $("input:checkbox").each(function (index) {
                $("<label>").attr("for", this.id)
                .attr("class", "styled_checkbox dashboard")
                .insertAfter(this);
            });

            var selectedindex = @ViewData("intChartType");    
            var accID = '@ViewData("AccountID")';
            var splitID = accID.split(" ");
            
            var postback = '@ViewData("Postback")';
            if(postback == "True") $('#outerwrapper').show();
            if(postback == "True" && accID != "" && selectedindex > 0)
            {                
                $('#chart').show()
                $('#chart').css('height', '200px');
            }

            if (selectedindex > 1) {                               
                $('#sidebar').css('height', '800px');                
                var spread = document.getElementById("spdAnalytics");
                var colcount = spread.GetColCount();
                spread.SetColWidth(1, 22);
                if (colcount < 9) {
                    spread.SetColWidth(3, 250);
                }
                else if (colcount > 12) {
                    spread.SetColWidth(3, 150);
                }
                else {
                    spread.SetColWidth(3, 200);
                }
            }
            else {
                $('#sidebar').css('height', '600px')
            }
            $('#spread').show()


            //adjust width
            var browsewidth = $(window).width();
            $('#intBrowserWidth').val(browsewidth)
            var spdWidth = $('#spdAnalytics').width();
            var calcwidth = spdWidth + 250;
            if (calcwidth > 1100) {
                if (calcwidth < browsewidth) {                    
                     $(".header").width('1275');
                    $(".nav-panel").width('1275');
                    $(".main-container").width('1275px');
                }
                else {
                }
            }
            //hide detail rows  
            if (document.getElementById("spdAnalytics")) {
                var spread = document.getElementById("spdAnalytics");
                var rc = spread.GetRowCount();
                var showlastrow = false
                for (var i = 0; i < rc; i++) {
                // pre-select the account id's in the viewer  
                    for(var j=0; j < splitID.length; j++){
                        if(splitID[j] == spread.GetValue(i, 4)) {
                            
                            spread.SetValue(i, 1, true, false);
                            //alert(splitID[j]);
                        }
                    }
                    var cellval = spread.GetValue(i, 2);
                    if (cellval == "HHeading") {
                        spread.Rows(i).style.display = "none";
                        showlastrow = true
                    }
                    else if (cellval == "VHeading") {
                        showlastrow = false

                        //                        spread.Rows(i).style.display = "table-row";
                    }
                    else if (cellval == "SubEnd") {
                        if (showlastrow == false) {
                            spread.Rows(i).style.display = "none";
                        }
                        else {
                            //                            spread.Rows(i).style.display = "table-row";
                            showlastrow = false
                        }
                    }
                    else if (cellval == "SubDetail") {
                        spread.Rows(i).style.display = "none";
                    }
                }
            } 
            if(postback == "False")
            { 
                $('#spread').hide();                
                $('#causepost').trigger('click');
            }            
        });

        window.onload = function () {
            if (document.getElementById("spdAnalytics")) {
                var spread = document.getElementById("spdAnalytics");
                if (document.all) {
                    if (spread.addEventListener) {
                        spread.addEventListener("ActiveCellChanged", cellChanged, false);
                    }
                    else {
                        spread.onActiveCellChanged = cellChanged;
                    }
                }
                else {
                    spread.addEventListener("ActiveCellChanged", cellChanged, false);
                }
            }                        
        }

        function cellChanged(event) {
            if (event.col == 0 && document.getElementById("spdAnalytics")) {
                var spread = document.getElementById("spdAnalytics");
                var cellval = spread.GetValue(event.row, 0);
                var rc = spread.GetRowCount();
                if (cellval == "-") {
                    var rowheadertype = spread.GetValue(event.row, 2);
                    //                    if (rowheadertype = "VHeading") {
                    //                                   
                    //                    }
                    for (var i = event.row; i < rc; i++) {
                        var rowtype = spread.GetValue(i, 2);
                        if (rowtype == "SubEnd") {
                            cell = document.getElementById("spdAnalytics").GetCellByRowCol(i, 0);
                            cell.removeAttribute("FpCellType");
                            if (rowheadertype == "HHeading") {
                                spread.SetValue(i, 0, "+", false);
                                cell.setAttribute("FpCellType", "readonly");
                            }
                            if (rowheadertype == "VHeading") {
                                spread.Rows(i).style.display = "none";

                            }
                            break;
                        }
                        else if (rowtype == "VHeading") {
                            cell = document.getElementById("spdAnalytics").GetCellByRowCol(i, 0);
                            cell.removeAttribute("FpCellType");
                            cell = document.getElementById("spdAnalytics").GetCellByRowCol(event.row, 0);
                            cell.style.borderColor =  "#D3D3D3"; 
                            spread.SetValue(i, 0, "+", false);
                            cell.setAttribute("FpCellType", "readonly");
                        }
                        else {
                            spread.Rows(i).style.display = "none";
                        }
                    }
                }
                else if (cellval == "+") {
                    var rowtype = spread.GetValue(event.row, 2);
                    if (rowtype == "SubEnd") {
                        for (var i = event.row; i > -1; i--) {
                            var rowtype = spread.GetValue(i, 2);
                            if (rowtype == "SubEnd") {
                                cell = document.getElementById("spdAnalytics").GetCellByRowCol(i, 0);
                                cell.removeAttribute("FpCellType");
                                spread.SetValue(i, 0, "", false);
                                cell.setAttribute("FpCellType", "readonly");
                                if ($('#blnUseFilter').is(':checked')) {
                                    spread.Rows(i).style.display = "none";
                                }
                            }
                            else if (rowtype == "HHeading") {
                                spread.Rows(i).style.display = "table-row";
                                break
                            }
                            else if ($('#blnUseFilter').is(':checked')) {
                                HideUnFilterRow(i)
                            }
                            else {
                                spread.Rows(i).style.display = "table-row";
                            }
                        }
                    }
                    if (rowtype == "VHeading") {
                        cell = document.getElementById("spdAnalytics").GetCellByRowCol(event.row, 0);
                        cell.removeAttribute("FpCellType");
                        spread.SetValue(event.row, 0, "-", false);
                        cell.setAttribute("FpCellType", "readonly");
                        cell.style.borderStyleBottom = "solid";
                        cell.style.borderColor = "White";
                        for (var i = event.row; i < rc; i++) {
                            var rowtype = spread.GetValue(i, 2);
                            if (rowtype == "SubEnd") {
                                cell = document.getElementById("spdAnalytics").GetCellByRowCol(i, 0);
                                cell.removeAttribute("FpCellType");
                                spread.SetValue(i, 0, "", false);
                                cell.setAttribute("FpCellType", "readonly");
                                spread.Rows(i).style.display = "table-row";

                                break;
                            }
                            else {
                                spread.Rows(i).style.display = "table-row";
                            }
                        }
                    }
                }
                spread.SetActiveCell(event.row, 1)
            }
        }

        function getSavedReportsList() {

        if (isSessionAliveCalled == 'false') { IsSessionAlive(); }

        $.ajax({
            type: "GET",
            url: '@Url.Action("GetSavedViewsList", "SV")',
            global: false,
            cache: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (msg) {
                var _selectedCompany
                if (msg !== undefined && (msg != "")) {

                    for (var savedreport_index = 0; savedreport_index < msg.length; savedreport_index++) {
                        var idcontrolVal = msg[savedreport_index].Value;
                        var id_controller = idcontrolVal.split("_");
                        var controller = id_controller[0]
                        var url;
                        if (controller.trim() == "RE")
                            url = '@Url.Action("Viewer", "RE", New With {.Id = "-1"})'
                        else if (controller.trim() == "ALC")
                            url = '@Url.Action("Viewer", "ALC", New With {.Id = "-1"})'
                        else if (controller.trim() == "CF")
                            url = '@Url.Action("Viewer", "CF", New With {.Id = "-1"})'
                        else if (controller.trim() == "RA")
                            url = '@Url.Action("Viewer", "RA", New With {.Id = "-1"})'
                        else if (controller.trim() == "OM")
                            url = '@Url.Action("Viewer", "OM", New With {.Id = "-1"})'

                        url = url.replace("-1", id_controller[1]);
                        $("#SavedReportsList").append("<li><a  class='savedviews' href='" + url + "' data='" + msg[savedreport_index].Value + "' >" + msg[savedreport_index].Text + "</a></li>");
                    }
                }
                else {
                    $("#SavedReportsList").html("No Saved Reports found ! ");
                }
                setSelectedPageNav();
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
            }
        });
    }
</script>
