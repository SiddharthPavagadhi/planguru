﻿@ModelType  onlineAnalytics.ReportHighChart
@Imports Farpoint.Web.Spread
@Imports Farpoint.Mvc.Spread
@Imports  onlineAnalytics
@Code
    ViewData("Title") = "PlanGuru Analytics | Ratios"
    If Not Session("action") Is Nothing Then
        
        If (Session("action") = "SetDefaultAnalysis" Or Session("action") = "GetAnalyses") Then
            Layout = Nothing
            Session.Remove("action")
        End If
    Else
        Layout = "~/Views/Shared/AnalyticsMaster.vbhtml"
    End If
    
    Dim UserInfo As User = Nothing
    Dim ua As New UserAccess
    
    Dim objSetup As New SetupCount
    
    If Not (Session("UserInfo") Is Nothing) Then
        UserInfo = DirectCast(Session("UserInfo"), User)
    End If
    
    If (Not ViewBag.Setup Is Nothing) Then
        objSetup = DirectCast(ViewBag.Setup, SetupCount)
    End If
End Code
<script src="@Url.Content("~/Scripts/Highcharts.js")" type="text/javascript"></script>
<script src="@Url.Content("~/Scripts/Exporting.js")" type="text/javascript"></script>
<script src="@Url.Content("~/Scripts/regression.js")" type="text/javascript"></script>
<script src="@Url.Content("~/Scripts/Highslide-full.min.js")" type="text/javascript"></script>
<script src="@Url.Content("~/Scripts/Highslide.config.js")" type="text/javascript"></script>
<link href="Content/css/highslide.css" rel="stylesheet" />
<script type="text/javascript">
    var donutData = new Array();
    var counter = 0;
    var donutChartTitle = "";
    var donutChartIndex;
    var numberOfColumns;
    var showAsPercentageForDonut = false;
    $(function () {
        window.ViewerControlId = "#OMView";
        var model = @Html.Raw(Json.Encode(Model));

        hs.Expander.prototype.onAfterExpand = addChart;

        function addChart() {
            var finalData = new Array();
            var chart = $("#hc-test" + counter).highcharts();
            if (chart) {
                chart.destroy();
            }
            for(var k = 0 ; k < donutData.length ; k++)
            {
                if((k == donutChartIndex) || ((k % numberOfColumns) == donutChartIndex))
                {
                    if(donutData[k].y >= 0)
                    {
                        finalData.push(donutData[k]);
                    }
                }

            }

            $("#hc-test" + counter).highcharts({

                chart: {
                    type: 'pie'
                },
                title: {
                    text: donutChartTitle,
                    style: {
                        color: '#000000',
                        fontWeight: 'bold',
                        backgroundColor: '#d3d3d3',
                        fontSize: '16px'
                    }
                },
                yAxis: {

                },
                plotOptions: {
                    pie: {
                        shadow: false
                    }
                },
                tooltip: {



                    formatter: function() {
                        if(showAsPercentageForDonut == false)
                        {
                            return this.point.name +': <b>' + formatcurrency(this.y,0) + ' : '+ Math.round(this.percentage*100)/100 + ' % </b>';
                        }
                        else
                        {
                            return this.point.name +':<b>' + formatcurrency(this.y,2) + '%</b>';
                        }
                    }
                },
                credits: {
                    enabled : false
                },
                exporting: {
                    enabled: false
                },
                series: [{
                    data: finalData,
                    size: '100%',
                    innerSize: '40%',
                    showInLegend:true,
                    dataLabels: {
                        enabled: false
                    }
                }]
            });


        }

        function formatcurrency(value, decPlaces) {
            if (decPlaces != 0) {
                return value.toFixed(decPlaces).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString();
            }
            else {
                return parseInt(value).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
            }
        }

        function GetTrendComment(YaxisData, percentLabel, intFormat,isThousand, decPlaces) {
            var comment;
            if (YaxisData.length > 1) {
                var differenceValue = (fitData(YaxisData).data[1][1] - fitData(YaxisData).data[0][1]);
                if (percentLabel == false) {
                    if (differenceValue > 0) {
                        comment = "Trend - Increasing at rate of " + differenceValue.toFixed(decPlaces);
                    }
                    else if (differenceValue < 0) {
                        comment = "Trend - Decreasing at rate of " + (-differenceValue).toFixed(decPlaces);
                    }
                    else if (differenceValue == 0) {
                        comment = "Trend - Increasing at rate of " + 0;
                    }
                }
                else {
                    if (differenceValue > 0) {
                        comment = "Trend - Increasing at rate of " + (differenceValue).toFixed(decPlaces);
                    }
                    else if (differenceValue < 0) {
                        comment = "Trend - Decreasing at rate of " + ((-differenceValue)).toFixed(decPlaces);
                    }
                    else if (differenceValue == 0) {
                        comment = "Trend - Increasing at rate of " + 0;
                    }
                }

                if (percentLabel == true) {
                    comment = comment + "%"
                }

                if(isThousand == true)
                {
                    comment = comment + " thousand"
                }

                if (intFormat == 0 || intFormat == 2 || intFormat == 3 || intFormat == 5 || intFormat == 6 || intFormat == 9) {
                    comment = comment + " per month";
                }
                else {
                    comment = comment + " per year";
                }
            }
            else if (percentLabel == true) {
                comment = "Trend - Increasing at rate of " + 0 + "% per month";
            }
            else {
                comment = "Trend - Increasing at rate of " + 0 + " per month";
            }
            return comment;
        }

        function GetSeriesColor(SeriesIndex)
        {
            var colorCode;
            switch (SeriesIndex) {
                case 0:
                    colorCode = '#1083a3';
                    break;
                case 1:
                    colorCode = '#f19632';
                    break;
                case 2:
                    colorCode = '#66b09d';
                    break;
                case 3:
                    colorCode = '#dd5822';
                    break;
                case 4:
                    colorCode = '#0e6684';
                    break;
                case 5:
                    colorCode = '#eecf4d';
                    break;
                case 6:
                    colorCode = '#90ac47';
                    break;
                case 7:
                    colorCode = '#c7341f'
                    break;
                case 8:
                    colorCode = '#139fc2'
                    break;
                case 9:
                    colorCode = '#f27723'
                    break;
                case 10:
                    colorCode = '#1083a3';
                    break;
                case 11:
                    colorCode = '#f19632';
                    break;
                case 12:
                    colorCode = '#66b09d';
                    break;
                case 13:
                    colorCode = '#dd5822';
                    break;
                case 14:
                    colorCode = '#0e6684';
                    break;
                case 15:
                    colorCode = '#eecf4d';
                    break;
                case 16:
                    colorCode = '#90ac47';
                    break;
                case 17:
                    colorCode = '#c7341f'
                    break;
                case 18:
                    colorCode = '#139fc2'
                    break;
                case 19:
                    colorCode = '#f27723'
                    break;
                case 20:
                    colorCode = '#1083a3';
                    break;
                case 21:
                    colorCode = '#f19632';
                    break;
                case 22:
                    colorCode = '#66b09d';
                    break;
                case 23:
                    colorCode = '#dd5822';
                    break;
                case 24:
                    colorCode = '#0e6684';
                    break;
                case 25:
                    colorCode = '#eecf4d';
                    break;
                case 26:
                    colorCode = '#90ac47';
                    break;
                case 27:
                    colorCode = '#c7341f'
                    break;
                case 28:
                    colorCode = '#139fc2'
                    break;
                case 29:
                    colorCode = '#f27723'
                    break;
                default:
                    colorCode = '#1083a3';
            }
            return colorCode;
        }

        function getCursorValue(selectedRow)
        {
            if(selectedRow > 1)
            {
                return 'pointer';
            }
            else
            {
                return null;
            }
        }

        function getCrossHairValue(selectedRow)
        {
            if(selectedRow > 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        if(model != null)
        {
            if(model.isHttpPost == false)
            {
                $("#chart").hide();
            }
            else
            {
                $("#chart").show();
                /* Chart width related Logic */
                
                if(model.Format == 2 || model.Format == 0 || model.Format == 3)
                {
                    $("#chart").width(parseInt(586)).height(200);
                }
                if(model.Format == 5)
                {
                    if(model.NumbofCols == 4 || model.NumbofCols == 8)
                    {
                        $("#chart").width((parseInt(model.chartWidth.replace('px','') - 65))).height(200);
                    }
                    else
                    {
                        $("#chart").width((parseInt(model.chartWidth.replace('px','') - 18))).height(200);
                    }
                }
                if(model.Format == 6 || model.Format == 7 || model.Format == 8 || model.Format == 9 || model.Format == 12)
                {
                    $("#chart").width((parseInt(model.chartWidth.replace('px','') - 18))).height(200);
                }
                if(model.Format == 10 || model.Format == 11)
                {
                    $("#chart").width((parseInt(model.chartWidth.replace('px','') - 75))).height(200);
                }
                /* End Chart width related Logic */
                var xAxisData = new Array();
                var series1yAxisData = new Object();
                var series2yAxisData = new Object();
                var trendComment;

                if(model.Format == 0 || model.Format == 2 || model.Format == 3)
                {
                    if(model.SeriesData != "" && model.SeriesData != null)
                    {
                        for (var k = 0 ; k < model.SeriesData[0].SeriesData.length; k++) {
                            xAxisData.push(model.SeriesData[0].SeriesData[k]);
                        }
                    }

                    if(model.Format == 0 || model.Format == 3)
                    {
                        if(model.SelectedRows.length == 1)
                        {
                            series1yAxisData.Name = model.SeriesData[0].SeriesTitle;
                            series1yAxisData.Data = new Array();
                            for(var j = 0 ; j < 1 ; j++)
                            {
                                series1yAxisData.Data.push(model.OptionalYAxisData[j]);
                            }

                            series2yAxisData.Name = model.SeriesData[1].SeriesTitle;
                            series2yAxisData.Data = new Array();
                            for(var j = 1 ; j < 2 ; j++)
                            {
                                series2yAxisData.Data.push(model.OptionalYAxisData[j]);
                            }

                        }


                        else if(model.SelectedRows.length == 2)
                        {
                            series1yAxisData.Name = model.SeriesData[0].SeriesTitle;
                            series1yAxisData.Data = new Array();
                            for(var j = 0 ; j < 2 ; j++)
                            {
                                series1yAxisData.Data.push(model.OptionalYAxisData[j]);
                            }

                            series2yAxisData.Name = model.SeriesData[1].SeriesTitle;
                            series2yAxisData.Data = new Array();
                            for(var j = 2 ; j < 4 ; j++)
                            {
                                series2yAxisData.Data.push(model.OptionalYAxisData[j]);
                            }

                        }
                        else if(model.SelectedRows.length > 2)
                        {
                            series1yAxisData.Name = model.SeriesData[0].SeriesTitle;
                            series1yAxisData.Data = new Array();
                            for(var j = 0 ; j < 3 ; j++)
                            {
                                series1yAxisData.Data.push(model.OptionalYAxisData[j]);
                            }

                            series2yAxisData.Name = model.SeriesData[1].SeriesTitle;
                            series2yAxisData.Data = new Array();
                            for(var j = 3 ; j < 6 ; j++)
                            {
                                series2yAxisData.Data.push(model.OptionalYAxisData[j]);
                            }
                        }
                    }

                    if(model.Format == 2)
                    {
                        if(model.SelectedRows.length == 1)
                        {
                            series1yAxisData.Name = model.SeriesData[0].SeriesTitle;
                            series1yAxisData.Data = new Array();
                            for(var j = 0 ; j < 1 ; j++)
                            {
                                series1yAxisData.Data.push(model.OptionalYAxisData[j]);
                            }

                            series2yAxisData.Name = model.SeriesData[1].SeriesTitle;
                            series2yAxisData.Data = new Array();
                            for(var j = 1 ; j < 2 ; j++)
                            {
                                series2yAxisData.Data.push(model.OptionalYAxisData[j]);
                            }

                        }

                        else if(model.SelectedRows.length == 2)
                        {
                            series1yAxisData.Name = model.SeriesData[0].SeriesTitle;
                            series1yAxisData.Data = new Array();
                            for(var j = 0 ; j < 2 ; j++)
                            {
                                series1yAxisData.Data.push(model.OptionalYAxisData[j]);
                            }

                            series2yAxisData.Name = model.SeriesData[1].SeriesTitle;
                            series2yAxisData.Data = new Array();
                            for(var j = 2 ; j < 4 ; j++)
                            {
                                series2yAxisData.Data.push(model.OptionalYAxisData[j]);
                            }

                        }
                        else if(model.SelectedRows.length > 2)
                        {
                            series1yAxisData.Name = model.SeriesData[0].SeriesTitle;
                            series1yAxisData.Data = new Array();
                            for(var j = 0 ; j < 3 ; j++)
                            {
                                series1yAxisData.Data.push(model.OptionalYAxisData[j]);
                            }

                            series2yAxisData.Name = model.SeriesData[1].SeriesTitle;
                            series2yAxisData.Data = new Array();
                            for(var j = 3 ; j < 6 ; j++)
                            {
                                series2yAxisData.Data.push(model.OptionalYAxisData[j]);
                            }
                        }
                    }




                    $("#chart").highcharts({

                        chart: {
                            backgroundColor: "#f7f6f4",
                            type: model.HighChartType,
                        },
                        title: {
                            text: model.ChartDesc,
                            style: {
                                color: '#276FA6',
                                fontWeight: 'bold',
                                backgroundColor: '#d3d3d3',
                                fontSize: '14px'
                            }
                        },
                        xAxis: {
                            categories: xAxisData,
                            lineColor: '#000000',
                            tickLength: 8,
                            tickWidth: 1,
                            tickColor: "#000000"
                        },

                        yAxis: {
                            lineWidth: 1,
                            lineColor: '#000000',
                            gridLineColor: '#000000',
                            gridLineWidth: 1,
                            tickLength: 8,
                            tickWidth: 1,
                            tickColor: "#000000",
                            labels: {
                                formatter: function () {
                                    if(model.ShowasPerc == false)
                                    {
                                        return formatcurrency(this.value, 2);
                                    }
                                    else
                                    {
                                        return (Math.round(this.value * 100) + '%');
                                    }
                                }
                            },
                            title: {
                                text: ''
                            }

                        },

                        tooltip: {
                            formatter: function () {
                                if(model.ShowasPerc == false)
                                {
                                    return '<b>' + this.series.name + '</b><br/>' + this.key + ' :<b> ' + formatcurrency(this.y, 2) + '</b>';
                                }
                                else
                                {
                                    return '<b>' + this.series.name + '</b><br/>' + this.key + ' :<b> ' + Highcharts.numberFormat((this.y * 100), 2, '.') + '%</b>';
                                }
                            }
                        },
                        credits: {
                            enabled: false
                        },
                        exporting:
                            {
                                enabled: false
                            },
                        series: [{
                            data: series1yAxisData.Data,
                            name: series1yAxisData.Name,
                            color: '#1083a3'
                        },
                        {
                            data: series2yAxisData.Data,
                            name: series2yAxisData.Name,
                            color: '#f19632'
                        }],
                        legend: {
                            align:'left',
                            verticalAlign: 'top',
                            floating: false,
                            layout:'vertical'
                        }
                    });
                }
                else
                {
                    var cursorValue = getCursorValue(model.SelectedRows.length);
                    var crossHairValue = getCrossHairValue(model.SelectedRows.length);

                    if(model.SeriesData != "" && model.SeriesData != null)
                    {
                        for (var k = 0 ; k < model.SeriesData[0].SeriesData.length; k++) {
                            xAxisData.push(model.SeriesData[0].SeriesData[k]);
                        }
                    }

                    if(model.ChartType == 3)
                    {

                        $("#chart").highcharts({

                            chart: {
                                backgroundColor: "#f7f6f4",
                                type: model.HighChartType,
                            },
                            title: {
                                text: model.ChartDesc,
                                style: {
                                    color: '#276FA6',
                                    fontWeight: 'bold',
                                    backgroundColor: '#d3d3d3',
                                    fontSize: '14px'
                                }
                            },
                            xAxis: {
                                categories: xAxisData,
                                lineColor: '#000000',
                                tickLength: 8,
                                tickWidth: 1,
                                tickColor: "#000000"
                            },

                            yAxis: {
                                lineWidth: 1,
                                lineColor: '#000000',
                                gridLineColor: '#000000',
                                gridLineWidth: 1,
                                tickLength: 8,
                                tickWidth: 1,
                                tickColor: "#000000",
                                labels: {
                                    formatter: function () {
                                        if(model.ShowasPerc == false)
                                        {
                                            return formatcurrency(this.value, 2);
                                        }
                                        else
                                        {
                                            return this.value + '%';
                                        }
                                    }
                                },
                                title: {
                                    text: ''
                                }

                            },
                            tooltip: {
                                formatter: function () {
                                    if(model.ShowasPerc == false)
                                    {
                                        return '<b>' + this.series.name + '</b><br/>' + this.key + ' :<b> ' + formatcurrency(this.y, 2) + '</b>';
                                    }
                                    else
                                    {
                                        return '<b>' + this.series.name + '</b><br/>' + this.key + ' :<b> ' + formatcurrency(this.y, 2) + '%</b>';
                                    }
                                },
                                crosshairs: crossHairValue
                            },
                            plotOptions: {
                                column: {
                                    stacking: 'normal',
                                    dataLabels: {
                                        enabled: false,
                                        color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
                                        style: {
                                            textShadow: '0 0 3px black'
                                        }
                                    }
                                },
                                series:{
                                    cursor:cursorValue

                                }
                            },

                            credits: {
                                enabled: false
                            },
                            exporting:
                                {
                                    enabled: false
                                },
                            series: [

                            ],
                            legend: {
                                align:'left',
                                verticalAlign: 'top',
                                floating: false,
                                layout:'vertical',
                                width:130,
                                labelFormatter: function() {
                                    var words = this.name.split(/[\s]+/);
                                    var numWordsPerLine = 2;
                                    var str = [];

                                    for (var word in words) {
                                        if (word > 0 && word % numWordsPerLine == 0)
                                            str.push('<br>');

                                        str.push(words[word]);
                                    }

                                    return str.join(' ');
                                }
                            }
                        });
                    }
                    else if(model.showTrend == true && model.ChartType == 4)
                    {
                        var singleSeriesData = new Array();
                        for(var j = 0; j < model.NumbofCols; j++)
                        {
                            singleSeriesData.push(model.ChartValues[j]);
                        }
                        trendComment = GetTrendComment(singleSeriesData,model.ShowasPerc,model.Format,model.isThousand,2)
                        $("#chart").highcharts({

                            chart: {
                                backgroundColor: "#f7f6f4",
                                type: model.HighChartType,
                            },
                            title: {
                                text: model.ChartDesc,
                                style: {
                                    color: '#276FA6',
                                    fontWeight: 'bold',
                                    backgroundColor: '#d3d3d3',
                                    fontSize: '14px'
                                }
                            },
                            xAxis: {
                                categories: xAxisData,
                                lineColor: '#000000',
                                tickLength: 8,
                                tickWidth: 1,
                                tickColor: "#000000"
                            },

                            yAxis: {
                                lineWidth: 1,
                                lineColor: '#000000',
                                gridLineColor: '#000000',
                                gridLineWidth: 1,
                                tickLength: 8,
                                tickWidth: 1,
                                tickColor: "#000000",
                                labels: {
                                    formatter: function () {
                                        if(model.ShowasPerc == false)
                                        {
                                            return formatcurrency(this.value, 2);
                                        }
                                        else
                                        {
                                            return this.value + '%';
                                        }

                                    }
                                },
                                title: {
                                    text: ''
                                }

                            },
                            tooltip: {
                                formatter: function () {
                                    if(model.ShowasPerc == false)
                                    {
                                        return '<b>' + this.series.name + '</b><br/>' + this.key + ' :<b> ' + formatcurrency(this.y, 2) + '</b>';
                                    }
                                    else
                                    {
                                        return '<b>' + this.series.name + '</b><br/>' + this.key + ' :<b> ' + formatcurrency(this.y, 2) + '%</b>';
                                    }
                                }
                            },

                            credits: {
                                enabled: true,
                                text: trendComment,
                                position: {
                                    align: 'center'
                                },
                                style: {
                                    color: 'black',
                                    fontSize: '12px',
                                    marginTop: '5px',
                                    marginBottom: '5px'
                                },
                                href: '#'
                            },
                            exporting:
                                {
                                    enabled: false
                                },
                            series: [

                            ],
                            legend: {
                                align:'left',
                                verticalAlign: 'top',
                                floating: false,
                                layout:'vertical',
                                width:130,
                                labelFormatter: function() {
                                    var words = this.name.split(/[\s]+/);
                                    var numWordsPerLine = 2;
                                    var str = [];

                                    for (var word in words) {
                                        if (word > 0 && word % numWordsPerLine == 0)
                                            str.push('<br>');

                                        str.push(words[word]);
                                    }

                                    return str.join(' ');
                                }
                            }
                        });
                    }
                    else
                    {
                        $("#chart").highcharts({

                            chart: {
                                backgroundColor: "#f7f6f4",
                                type: model.HighChartType,
                            },
                            title: {
                                text: model.ChartDesc,
                                style: {
                                    color: '#276FA6',
                                    fontWeight: 'bold',
                                    backgroundColor: '#d3d3d3',
                                    fontSize: '14px'
                                }
                            },
                            xAxis: {
                                categories: xAxisData,
                                lineColor: '#000000',
                                tickLength: 8,
                                tickWidth: 1,
                                tickColor: "#000000"
                            },

                            yAxis: {
                                lineWidth: 1,
                                lineColor: '#000000',
                                gridLineColor: '#000000',
                                gridLineWidth: 1,
                                tickLength: 8,
                                tickWidth: 1,
                                tickColor: "#000000",
                                labels: {
                                    formatter: function () {
                                        if(model.ShowasPerc == false)
                                        {
                                            return formatcurrency(this.value, 2);
                                        }
                                        else
                                        {
                                            return this.value + '%';
                                        }

                                    }
                                },
                                title: {
                                    text: ''
                                }

                            },
                            plotOptions: {
                                series:{
                                    cursor:cursorValue
                                }
                            },
                            tooltip: {
                                formatter: function () {
                                    if(model.ShowasPerc == false)
                                    {
                                        return '<b>' + this.series.name + '</b><br/>' + this.key + ' :<b> ' + formatcurrency(this.y, 2) + '</b>';
                                    }
                                    else
                                    {
                                        return '<b>' + this.series.name + '</b><br/>' + this.key + ' :<b> ' + formatcurrency(this.y, 2) + '%</b>';
                                    }
                                },
                                crosshairs: crossHairValue
                            },

                            credits: {
                                enabled: false
                            },
                            exporting:
                                {
                                    enabled: false
                                },
                            series: [

                            ],
                            legend: {
                                align:'left',
                                verticalAlign: 'top',
                                floating: false,
                                layout:'vertical',
                                width:130,
                                labelFormatter: function() {
                                    var words = this.name.split(/[\s]+/);
                                    var numWordsPerLine = 2;
                                    var str = [];

                                    for (var word in words) {
                                        if (word > 0 && word % numWordsPerLine == 0)
                                            str.push('<br>');

                                        str.push(words[word]);
                                    }

                                    return str.join(' ');
                                }
                            }
                        });
                    }

                    if(model.showTrend == false)
                    {
                        var i = model.NumbofCols;
                        var l = 1;
                        if(l <= i)
                        {
                            for(var k = 0 ; k < model.SelectedRows.length ; k++)
                            {


                                var singleSeriesData = new Array();
                                var seriesColor = GetSeriesColor(k);
                                var name = model.SeriesData[k].SeriesTitle.replace(/\n/g, " ");

                                for(var j = ((i * l) - i) ; j < (i * l) ; j++)
                                {
                                    singleSeriesData.push(model.ChartValues[j]);
                                    donutData.push({name:name,y:model.ChartValues[j], color: seriesColor });
                                }

                                var data = singleSeriesData;
                                if(model.SelectedRows.length > 1)
                                {
                                    $("#chart").highcharts().addSeries({name : name , data : data, color: seriesColor,
                                        events: {
                                            click: function (event)
                                            {
                                                counter++;
                                                donutChartIndex = event.point.index;
                                                if(model.isThousand == true)
                                                {
                                                    donutChartTitle = event.point.category +  " (in thousands)";
                                                }
                                                else
                                                {
                                                    donutChartTitle = event.point.category;
                                                }
                                                numberOfColumns = model.NumbofCols;
                                                showAsPercentageForDonut = model.ShowasPerc;
                                                hs.htmlExpand(null, {
                                                    pageOrigin: {
                                                        x: (event.screenX),
                                                        y: (event.screenY)
                                                    },
                                                    maincontentText: '<div id="hc-test' + counter + '"></div>',
                                                    width: 530,
                                                    height: 473
                                                });
                                            }
                                        }
                                    });
                                }
                                else
                                {
                                    $("#chart").highcharts().addSeries({name : name , data : data, color: seriesColor});
                                }
                                l++;
                            }
                        }

                    }

                    if(model.showTrend == true && model.ChartType == 4)
                    {
                        var singleSeriesData = new Array();
                        var name = model.SeriesData[0].SeriesTitle.replace(/\n/g, " ");

                        for(var j = 0; j < model.NumbofCols; j++)
                        {
                            singleSeriesData.push(model.ChartValues[j]);
                        }

                        var data = singleSeriesData;

                        $("#chart").highcharts().addSeries({name : name , data : data,color: '#1083a3'})
                        $("#chart").highcharts().addSeries({
                            type: 'line',
                            name: 'Trend',
                            color: '#f37823',
                            marker: {
                                enabled: false
                            },
                            data: (function () {
                                return fitData(singleSeriesData).data;
                            })()
                        })
                    }

                }

               



            }

        }
        else
        {
            $("#chart").hide();
        }
    });
    $(window).load(function () {
        $(".wrapper").show();
    })
</script>
<div style="display: inline-block; width: 100%;">
    @If Not (DirectCast(TempData("Message"), String) Is Nothing) Then
        @<label class="success">@TempData("Message").ToString()
        </label>                         
    End If
    @If Not (DirectCast(TempData("ErrorMessage"), String) Is Nothing) Then
        @<label class="error">@TempData("ErrorMessage").ToString()
        </label>                         
    End If
</div>
<div>
    @code                
        
        @Html.Hidden("countOfDataUploaded", objSetup.countDataUploadedOfSelectedAnalysis)        
        @<label id="validationMessage" class="info" style="display: none;">
            @If ((Not IsNothing(ViewBag.Setup)) AndAlso objSetup.AanlysisCount > 0 And objSetup.selectedAnalysis = 0) Then
                @MvcHtmlString.Create("Please select company & one of the respective analysis from top-menu selection, to get the dashboard view.")
            ElseIf ((Not IsNothing(ViewBag.Setup)) AndAlso objSetup.selectedAnalysis > 0 And objSetup.countDataUploadedOfSelectedAnalysis = 0) Then
                @MvcHtmlString.Create("Data has not been uploaded for selected analysis.")
            End If
        </label> 
        
        If (Not UserInfo Is Nothing AndAlso Not IsNothing(ViewBag.Setup)) Then
                   
            If (UserInfo.UserRoleId <= UserRoles.SAU) Then
                    
                If (objSetup.CompanyCount = 0 Or objSetup.AanlysisCount = 0) Then
        @<label class="info">You have to create below listed action item, to get the dashboard
            view.</label> 
                End If
                If (objSetup.CompanyCount = 0) Then
        @<div class="secondbutton_example" style='margin: 20px; width: 200px;'>
            <a href='@Url.Action("Create", "Company")' style="text-decoration:none;color: #FFFFFF;">
                Add Company</a>
        </div>  
                End If
                If (objSetup.CompanyCount > 0 And objSetup.AanlysisCount = 0) Then
        @<div class="secondbutton_example" style='margin: 20px; width: 200px;'>
            <a href='@Url.Action("Create", "Analysis")'  style="text-decoration:none;color: #FFFFFF;">
                Add Analysis</a>
        </div>  
                End If
                If (objSetup.CompanyCount = 0 Or objSetup.AanlysisCount = 0) Then
        @<div class="secondbutton_example" style='margin: 20px; width: 200px;'>
            <a href='@Url.Action("CreateUser", "User")'  style="text-decoration:none;color: #FFFFFF;">
                Add User</a>
        </div>  
                End If
            ElseIf (UserInfo.UserRoleId > UserRoles.SAU) Then
                
                If (objSetup.CompanyCount = 0) Then
        @<label class="info">You have not been given access to any company. Please contact your administrator.</label>
                ElseIf (objSetup.AanlysisCount = 0) Then
        @<label class="info">You have not been given access to any analysis files for the selected company. Please contact your administrator.</label> 
                End If
                
            End If
        End If
        
        If (objSetup.CompanyCount > 0 And objSetup.AanlysisCount > 0 And objSetup.selectedAnalysis > 0 And objSetup.countDataUploadedOfSelectedAnalysis > 0) Then
            Using Html.BeginForm
        @<div id="OMView" style="display: none; width: 100%;">
            <div id="sidebar">
                <div>
                    <img src='@Url.Content("~/Content/images/period.png")' alt="Period" />
                </div>
                <div>
                    @Html.DropDownList("intPeriod", DirectCast(ViewData("Periods"), SelectList), New With {.class = "REselect"})
                </div>
                <br />
                <div>
                    <img src='@Url.Content("~/Content/images/format.png")' alt="Format" />
                </div>
                <div>
                    @Html.DropDownList("intFormat", DirectCast(ViewData("Formats"), SelectList), New With {.class = "REselect"})
                </div>
               
                 <div id="hperioddd" class="indent10" style="margin-top: 5px; display: none;" >
                    @Html.DropDownList("intHPeriod", DirectCast(ViewData("HPeriods"), SelectList), New With {.class = "REselect", .Style = "width: 80px; font-size: 10px; "})
                </div>

                <div id="chkShowVar" style="display: none;" class="indent0">
                    @Html.CheckBox("blnShowVar") Show variance
                </div>
                <br />
                <div id="optionsimage">
                    <img src='@Url.Content("~/Content/images/options.png")' alt="Options" />
                </div>
                <div id="chkHighlightVar" class="indent0" style="margin-top: 5px;">
                    @Html.CheckBox("blnHighVar", ViewData("blnHighVar")) Highlight variances:
                </div>
                <div id="HighlightVarControls" style="display: none;">
                    <div id="chkHighlightPosVar" class="indent10">
                        @Html.CheckBox("blnHighPosVar", True) + Variances > than:
                        @Html.TextBox("intHighPosVar", 10, New With {.Style = "width: 12px; margin-left: 5px; font-size: 10px; border: 1px solid #DDD;text-align: right; "})%
                        <span id="valintHighPosVar" style="color: red; text-align: right; float: right; width: 165px;
                            margin-right: 10px; display: none;">Numeric characters only.</span>
                    </div>
                    <div id="chkHighlightNegVar" class="indent10">
                        @Html.CheckBox("blnHighNegVar", True) - Variances < than: -
                        @Html.TextBox("intHighNegVar", 0, New With {.Style = "width: 12px;  font-size: 10px; border: 1px solid #DDD; text-align: right;"})%
                        <span id="valintHighNegVar" style="color: red; text-align: right; float: right; width: 165px;
                            margin-right: 10px; display: none;">Numeric characters only.</span>
                    </div>
                </div>
                <br />
                <div>
                    <img src='@Url.Content("~/Content/images/charts.png")' alt="Charts" />
                </div>
                <div>
                    @Html.DropDownList("intChartType", DirectCast(ViewData("ChartType"), SelectList), New With {.class = "REselect"})
                </div>
                <div id="ShowTrend" class="indent10" style="margin-top: 5px;">
                    @Html.CheckBox("blnShowTrend")Show trend
                </div>
                <div id="UpdateChart" style="display: none; margin: 15px 0 0 0; float: left;">
                    <input id="causepost" type="submit" value="Update Selections" class="cancelbutton" />
                </div>
                <div class="link">
                    @Code
                                If Not (UserInfo Is Nothing) Then
                                If (UserInfo.UserRoleId <> UserRoles.CRU And Utility.GetViewCount(UserInfo.AnalysisId) >= Convert.ToInt32(ConfigurationManager.AppSettings("ViewCount"))) Then
                    End Code
                    <a style="cursor: default;" onclick="return false;" class="addsavedviewsdisabled"> <img class="plusIcon" src='@Url.Content("~/Content/Images/plus.gif")'> Add to Saved Reports</a>
                    @Code   
                                ElseIf (UserInfo.UserRoleId <> UserRoles.CRU) Then
                    End Code                    
                               <a href="#" id="lnkAddSavedReports" class="addsavedviews"> <img class="plusIcon" src='@Url.Content("~/Content/Images/plus.gif")'> Add to Saved Reports</a>                    
                    @Code                                               
                                    End If
                                End If
                    End Code
                </div>
            </div>
             <div id="main" style="display: inline-block; width: 79%;">
                 <div id="chart" style="display: none;">
                     @*@Html.FpSpread("spdChart", Sub(x)
                    x.RowHeader.Visible = False
                    x.ColumnHeader.Visible = False
                    x.Height = 200
                    x.Width = 500

                End Sub)*@
                 </div>
                 <div id="spread" style="display: none;">
                     @Html.FpSpread("spdAnalytics", Sub(x)

                                                       x.RowHeader.Visible = False
                                                       x.ActiveSheetView.PageSize = 1000
                                                   End Sub)
                 </div>
             </div>
        </div>
            End Using
        End If
        
    End Code
</div>
<script type="text/javascript">
    $("#lnkAddSavedReports").click(function () {
        if (document.getElementById("spdAnalytics")) {
            var accountIDs = new Array();
            var spread = document.getElementById("spdAnalytics");
            var rc = spread.GetRowCount();
            var count = 0;
            for (var i = 0; i < rc; i++) {
                var chkboxval = spread.GetValue(i, 1);
                if (chkboxval == "true") {
                    if (count < 10)
                        accountIDs[count] = spread.GetValue(i, 4);
                    count++;
                }
            }
            if (count > 10) {

                $.msgBox({
                    title: "Save View",
                    content: "Only top 10 selected accounts will be saved !",
                    type: "info",
                    buttons: [{ value: "Ok"}],
                    success: function (result) {
                        showView(true, accountIDs);
                    },
                    afterShow: function () { $('[name=Ok]').focus(); }
                });
            }
            else {
                showView(true, accountIDs);
            }

        }

    });


    function showView(validation, accountIDs) {

        $.msgBox({
            title: "Save View",
            content: "<span class='viewsavereport'>View Name </span><span ><input type='text' class='viewsavetext' name='reportname'  maxlength='50'/><label id='viewvalidation' class='viewvalidation'></label></span>",
            type: "info",
            buttons: [{ value: "Ok" }, { value: "Cancel"}],
            success: function (result) {

                if (result == "Ok") {                    
                        var reportname = $('[name=reportname]').val();
                        document.forms[0].action = '@Url.Action("AddViews", "RA")' + "?viewname=" + encodeURIComponent(reportname) + "&accountid=" + accountIDs;
                        document.forms[0].submit();                    
                }
            },
            afterShow: function () { if (validation == false) { $('#viewvalidation').text('Please provide the view name.'); } $('[name=Cancel]').focus(); }
        });

    }

    $(function () {

        $('#blnHighVar').change(function () {
            if ($(this).is(':checked')) {
                $('#HighlightVarControls').show()
                AddHighlight()
            }
            else {
                $('#HighlightVarControls').hide()
                RemoveBackgroundColor()

            }
        });

        $('#blnHighPosVar').change(function () {
            AddHighlight()
        })

        $('#blnHighNegVar').change(function () {
            AddHighlight()
        })

        $('#intHighPosVar').change(function () {
            AddHighlight()
        })

        $('#intHighNegVar').change(function () {
            AddHighlight()
        })

        function AddHighlight() {
            if (document.getElementById("spdAnalytics")) {
                var spread = document.getElementById("spdAnalytics");
                if ($("#blnHighPosVar").is(':checked')) {
                    var highposvar = "checked"
                };
                if ($("#blnHighNegVar").is(':checked')) {
                    var highnegvar = "checked"
                };
                var posvaramt = parseFloat($("#intHighPosVar").val());
                var negvaramt = parseFloat($("#intHighNegVar").val());
                negvaramt = -negvaramt
                var rc = spread.GetRowCount();
                for (var i = 0; i < rc; i++) {
                    var rowtype = spread.GetValue(i, 2);
                    if (rowtype == "Detail") {
                        var value = parseFloat(spread.GetValue(i, 8).replace(',', ''));
                        if (value > posvaramt) {
                            if (highposvar == "checked") {
                                SetBackGroundColor(i, "Green")
                            }
                            else {
                                SetBackGroundColor(i, "Black")
                            }
                        }
                        else if (value < negvaramt) {
                            if (highnegvar == "checked") {
                                SetBackGroundColor(i, "Red")
                            }
                            else {
                                SetBackGroundColor(i, "Black")
                            }
                        }
                        else {

                            SetBackGroundColor(i, "Black")
                        }
                    }
                    else {
                        SetBackGroundColor(i, "Black")
                    }
                }
            }
        }






        function AddBackgroundColor() {
            if (document.getElementById("spdAnalytics")) {
                var spread = document.getElementById("spdAnalytics");
                var rc = spread.GetRowCount();
                var tf = false
                for (var i = 0; i < rc; i++) {
                    if (tf == false) {
                        SetBackGroundColor(i, "#fef5do")
                        spread.SetSelectedRange(i, 2, i, 7)
                        tf = true
                    }
                    else {
                        SetBackGroundColor(i, "White")
                        spread.SetSelectedRange(i, 2, i, 7)
                        tf = false
                    }
                }
            }
        }

        function RemoveBackgroundColor() {
            if (document.getElementById("spdAnalytics")) {
                var spread = document.getElementById("spdAnalytics");
                var rc = spread.GetRowCount();
                var tf = false
                for (var i = 0; i < rc; i++) {
                    SetBackGroundColor(i, "Black")
                }
            }
        }

        function SetBackGroundColor(row, color) {

            if (document.getElementById("spdAnalytics")) {
                var spread = document.getElementById("spdAnalytics");
                var colcount = spread.GetColCount();
                for (var i = 2; i < colcount; i++) {
                    spread.Cells(row, i).style.color = color
                }
            }
        }

        //        //charting functions
        $('#intPeriod').change(function () {
            CheckforSelectedRow()
            //            $('#causepost').trigger('click')
            $('#UpdateChart').show();
        })

        $('#intFormat').change(function () {
            if (Prevalidate()) {
                CheckforSelectedRow()
                var numberOfOptions = $('select#intChartType option').length
                $("#UpdateChart").show();
                var selectedValue = $(this).val();
                if (selectedValue < 5) {
                    if (intSaveFormat == selectedValue - 2) {
                        $('#optionsimage').show()
                        $('#chkHighlightVar').show()
                        if ($('#blnHighVar').is(':checked')) {
                            $('#HighlightVarControls').show()
                        }
                        $('#CheckBoxFilter').show();
                        $("#hperioddd").hide();
                    }
                    else {
                        $('#optionsimage').hide()
                        $('#chkHighlightVar').hide()
                        $('#HighlightVarControls').hide()
                        $('#CheckBoxFilter').hide()
                    }
                    if (numberOfOptions > 2) {
                        $("#intChartType").empty();
                        var opt = document.createElement("option");
                        document.getElementById("intChartType").options.add(opt);
                        opt.text = "None";
                        opt.value = 1;
                        var opt1 = document.createElement("option");
                        document.getElementById("intChartType").options.add(opt1);
                        opt1.text = "Bar";
                        opt1.value = 2;
                    }
                }
                else {
                    if (selectedValue == 13) {
                        $("#hperioddd").show();
                    }
                    else {
                        $("#hperioddd").hide();
                    }
                    $('#optionsimage').hide()
                    $('#chkHighlightVar').hide()
                    $('#HighlightVarControls').hide()
                    $('#CheckBoxFilter').hide()
                    if (numberOfOptions == 2) {
                        var opt = document.createElement("option");
                        document.getElementById("intChartType").options.add(opt);
                        opt.text = "Stacked bar";
                        opt.value = 3;
                        var opt1 = document.createElement("option");
                        document.getElementById("intChartType").options.add(opt1);
                        opt1.text = "Line";
                        opt1.value = 4;
                        var opt2 = document.createElement("option");
                        document.getElementById("intChartType").options.add(opt2);
                        opt2.text = "Area";
                        opt2.value = 5;
                    }
                }
            }
            else {
                $.msgBox({
                    title: "Alert",
                    content: "You can not select more than 3 factors for this chart format.",
                    type: "Error",
                    buttons: [{ value: "Ok" }],
                    afterShow: function () {
                        $('[name=Ok]').focus();
                        $('#spdAnalytics').find('input:checkbox').attr('checked', false);
                    }
                });
            }
        })

        $('#blnShowasPer').change(function () {
            CheckforSelectedRow()
            //            $('#causepost').trigger('click')
            $('#UpdateChart').show();
        })

        $('#blnShowBudget').change(function () {
            CheckforSelectedRow()
            //            $('#causepost').trigger('click')
            $('#UpdateChart').show();
        })

        $('#intHPeriod').change(function () {
            $("#UpdateChart").show();
        })

        function CheckforSelectedRow() {
            var spread = document.getElementById("spdAnalytics");
            var count = 0
            var rc = spread.GetRowCount();
            var count = 0;
            for (var i = 0; i < rc; i++) {
                var chkboxval = spread.GetValue(i, 1);
                if (chkboxval == "true") {
                    count++;
                    break
                }
            }
            if (count == 0) {
                $('#intChartType')[0].selectedIndex = 0;
            }
        }

        function Prevalidate() {
            var spread = document.getElementById("spdAnalytics");
            var rc = spread.GetRowCount();
            var count = 0;
            for (var i = 0; i < rc; i++) {
                var chkboxval = spread.GetValue(i, 2);
                if (chkboxval == "true") {
                    count++;
                }
            }

            var selectedValue = $('#intFormat').val();
            if ((selectedValue === "1" || selectedValue === "4" || selectedValue === "3") && count > 3) {
                return false;
            }
            else {
                return true;
            }
        }

        $('#intChartType').change(function () {

            if ($('#intChartType').get(0)) {
                var selectedindex = $('#intChartType').get(0).selectedIndex;
                var spread = document.getElementById("spdAnalytics");
                var colcount = spread.GetColCount();
                if (selectedindex == 0) {
                    $('#ShowTrend').hide();
                    //                    $('#UpdateChart').hide();
                    if ($('#spdChart').is(':visible')) {
                        $('#causepost').trigger('click')
                    }
                }
                else {
                    spread.SetColWidth(1, 22);
                    if (colcount < 9) {
                        spread.SetColWidth(3, 250);
                    }
                    else if (colcount > 12) {
                        spread.SetColWidth(3, 150);
                    }
                    else {
                        spread.SetColWidth(3, 200);
                    }
                    var rc = spread.GetRowCount();
                    var count = 0;
                    for (var i = 0; i < rc; i++) {
                        var chkboxval = spread.GetValue(i, 1);
                        if (chkboxval == "true") {
                            count++;
                        }
                    }
                    if (count == 1) {
                        $('#UpdateChart').show();
                        if (selectedindex == 3) {
                            $('#ShowTrend').show();
                        }
                        else {
                            $('#ShowTrend').hide();
                        }
                    }
                    else if (count > 1) {
                        $('#UpdateChart').show();
                    }
                }
            }
        });

        $('#ShowTrend').change(function () {
            $('#UpdateChart').show();
        });

        $("#intHighPosVar").keyup(function () {
            $("#valintHighPosVar").hide();
            var inputVal = $(this).val();
            var numericReg = /^\d*[0-9](|.\d*[0-9]|,\d*[0-9])?$/;
            if (!numericReg.test(inputVal)) {
                $("#intHighPosVar").val('');
                $("#valintHighPosVar").show();
            }
        });

        $("#intHighNegVar").keyup(function () {
            $("#valintHighNegVar").hide();
            var inputVal = $(this).val();
            var numericReg = /^\d*[0-9](|.\d*[0-9]|,\d*[0-9])?$/;
            if (!numericReg.test(inputVal)) {
                $("#intHighNegVar").val('');
                $("#valintHighNegVar").show();
            }
        });



        //on load
        $(document).ready(function () {
            IsSessionAlive();

            $("input:checkbox").each(function (index) {
                $("<label>").attr("for", this.id)
                .attr("class", "styled_checkbox dashboard")
                .insertAfter(this);
            });

            var selectedindex = 0;
            if ($('#intChartType').get(0) != undefined) {
                selectedindex = $('#intChartType').get(0).selectedIndex;
            }
            if (selectedindex > 0) {
                $('#chart').show()
                $('#chart').css('height', '200px');
                $('#sidebar').css('height', '800px')
                $('#UpdateChart').show();
                var spread = document.getElementById("spdAnalytics");
                var colcount = spread.GetColCount();
                spread.SetColWidth(1, 22);
                if (colcount < 9) {
                    spread.SetColWidth(3, 250);
                }
                else if (colcount > 12) {
                    spread.SetColWidth(3, 150);
                }
                else {
                    spread.SetColWidth(3, 200);
                }
            }
            else {
                $('#sidebar').css('height', '600px')
            }
            $('#spread').show()

            if (selectedindex == 3) {
                $('#ShowTrend').show();
            }
            else {
                $('#ShowTrend').hide();
                $('#blnShowTrend').removeAttr('checked');
            }

            var selectedindex = 0;
            if ($('#intFormat').get(0) != undefined) {
                selectedindex = $('#intFormat').get(0).selectedIndex; 
            }
            if (selectedindex == 0) {
                intSaveFormat = selectedindex - 1
            }
            else {
                intSaveFormat = selectedindex
            }
            if (selectedindex > 2) {
                $('#optionsimage').hide()
                $('#chkHighlightVar').hide()
                $('#HighlightVarControls').hide()
                $('#CheckBoxFilter').hide() 
                if (selectedindex == 9) {
                    $('#hperioddd').show()
                }
              
            }
            else {
                $('#blnHighVar').removeAttr('checked')
                var selPercent = $('#blnShowasPer').attr('checked');
                if (selPercent == true) {
                    $('#CheckBoxFilter').hide()
                }
            }
            //adjust width
            var browsewidth = $(window).width();
            $('#intBrowserWidth').val(browsewidth)
            var spdWidth = $('#spdAnalytics').width();
            var calcwidth = spdWidth + 250;
            if (calcwidth > 1100) {
                if (calcwidth < browsewidth) {
                    //$('#main').css('padding-left', '20px');
                    $(".header").width('1265');
                    $(".nav-panel").width('1265');
                    $(".main-container").width('1265px');
                }
                else {
                }
            }

        });



    });
</script>
