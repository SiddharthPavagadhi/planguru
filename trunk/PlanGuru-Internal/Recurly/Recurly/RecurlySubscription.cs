﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.IO;
using System.Net;
using System.Text;
using System.Xml;

namespace Recurly
{
    public class RecurlySubscription
    {
        /// <summary>
        /// Account in Recurly
        /// </summary>
        public RecurlyAccount Account { get; set; }
        public List<RecurlyAddOn> AddOns { get; set; }
        public RecurlyAddOn AddOn { get; set; }
        public int? Quantity { get; set; }
        public string PlanCode { get; set; }
        public string CouponCode { get; set; }
        public string State { get; private set; }
        public string Currency { get; set; }

        // Additional information
        /// <summary>
        /// Date the subscription started.
        /// </summary>
        public DateTime? ActivatedAt { get; private set; }
        /// <summary>
        /// If set, the date the subscriber canceled their subscription.
        /// </summary>
        public DateTime? CanceledAt { get; private set; }
        /// <summary>
        /// If set, the subscription will expire/terminate on this date.
        /// </summary>
        public DateTime? ExpiresAt { get; private set; }
        /// <summary>
        /// Date the current invoice period started.
        /// </summary>
        public DateTime? CurrentPeriodStartedAt { get; private set; }
        /// <summary>
        /// The subscription is paid until this date. Next invoice date.
        /// </summary>
        public DateTime? CurrentPeriodEndsAt { get; private set; }
        /// <summary>
        /// Date the trial started, if the subscription has a trial.
        /// </summary>
        public DateTime? TrialPeriodStartedAt { get; private set; }

        public string uuid { get; set; }

        /// <summary>
        /// Date the trial ends, if the subscription has/had a trial.
        /// 
        /// This may optionally be set on new subscriptions to specify an exact time for the 
        /// subscription to commence.  The subscription will be active and in "trial" until
        /// this date.
        /// </summary>
        public DateTime? TrialPeriodEndsAt
        {
            get { return this.trialPeriodEndsAt; }
            set
            {
                if (this.ActivatedAt.HasValue)
                    throw new InvalidOperationException("Cannot set TrialPeriodEndsAt on existing subscriptions.");
                if (value.HasValue && (value < DateTime.UtcNow))
                    throw new ArgumentException("TrialPeriodEndsAt must occur in the future.");

                this.trialPeriodEndsAt = value;
            }
        }
        private DateTime? trialPeriodEndsAt;

        // TODO: Read pending subscription information

        public enum ChangeTimeframe
        {
            Now,
            Renewal
        }

        public enum RefundType
        {
            Full,
            Partial,
            None
        }

        /// <summary>
        /// Unit amount per quantity.  Leave null to keep as is. Set to override plan's default amount.
        /// </summary>
        public int? UnitAmountInCents { get; set; }

        private const string UrlPrefix = "/accounts/";
        private const string UrlPostfix = "/subscriptions";

        public RecurlySubscription(RecurlyAccount account)
        {
            this.Account = account;
            this.Quantity = 1;
        }

        private static string SubscriptionUrl(string accountCode)
        {
            return UrlPrefix + System.Web.HttpUtility.UrlEncode(accountCode) + UrlPostfix;
        }

        private static string CreateSubscriptionUrl()
        {
            return UrlPostfix;
        }

        public static RecurlySubscription Get(string accountCode)
        {
            return Get(new RecurlyAccount(accountCode));
        }

        public static RecurlySubscription Get(RecurlyAccount account)
        {
            RecurlySubscription sub = new RecurlySubscription(account);

            HttpStatusCode statusCode = RecurlyClient.PerformRequest(RecurlyClient.HttpRequestMethod.Get,
                SubscriptionUrl(account.AccountCode),
                new RecurlyClient.ReadXmlDelegate(sub.ReadXml));

            if (statusCode == HttpStatusCode.NotFound)
                return null;

            return sub;
        }


        public void Create()
        {
            HttpStatusCode statusCode = RecurlyClient.PerformRequest(RecurlyClient.HttpRequestMethod.Post,
                SubscriptionUrl(this.Account.AccountCode),
                new RecurlyClient.WriteXmlDelegate(WriteSubscriptionXml),
                new RecurlyClient.ReadXmlDelegate(this.ReadXml));

        }

        public void CreateSubscription()
        {
            HttpStatusCode statusCode = RecurlyClient.PerformRequest(RecurlyClient.HttpRequestMethod.Post,
                CreateSubscriptionUrl(),
                new RecurlyClient.WriteXmlDelegate(WriteCreateSubscriptionXml),
                new RecurlyClient.ReadXmlDelegate(this.ReadXml));
        }


        public void ChangeSubscription(ChangeTimeframe timeframe)
        {
            RecurlyClient.WriteXmlDelegate writeXmlDelegate;

            if (timeframe == ChangeTimeframe.Now)
                writeXmlDelegate = new RecurlyClient.WriteXmlDelegate(WriteChangeSubscriptionNowXml);
            else
                writeXmlDelegate = new RecurlyClient.WriteXmlDelegate(WriteChangeSubscriptionAtRenewalXml);

            HttpStatusCode statusCode = RecurlyClient.PerformRequest(RecurlyClient.HttpRequestMethod.Put,
                UpdateSubscriptionUrl(this.uuid),
                writeXmlDelegate,
                new RecurlyClient.ReadXmlDelegate(this.ReadXml));
        }

        /// <summary>
        /// Cancel an active subscription.  The subscription will not renew, but will continue to be active
        /// through the remainder of the current term.
        /// </summary>
        /// <param name="accountCode">Subscriber's Account Code</param>
        public static void CancelSubscription(string accountCode)
        {
            RecurlyClient.PerformRequest(RecurlyClient.HttpRequestMethod.Delete, SubscriptionUrl(accountCode));
        }

        /// <summary>
        /// Immediately terminate the subscription and issue a refund.  The refund can be for the full amount
        /// or prorated until its paid-thru date.  If you need to refund a specific amount, please issue a
        /// refund against the individual transaction instead.
        /// </summary>
        /// <param name="accountCode">Subscriber's Account Code</param>
        /// <param name="refundType"></param>
        public static void RefundSubscription(string accountCode, RefundType refundType)
        {
            string refundTypeParameter = refundType.ToString().ToLower();

            string refundUrl = String.Format("{0}?refund={1}",
                SubscriptionUrl(accountCode),
                refundTypeParameter);

            RecurlyClient.PerformRequest(RecurlyClient.HttpRequestMethod.Delete, refundUrl);
        }

        private static string UpdateSubscriptionUrl(string uuid)
        {
            return UrlPostfix + "/" + uuid;
        }

        /// <summary>
        /// Terminate the subscription immediately and do not issue a refund.
        /// </summary>
        /// <param name="accountCode"></param>
        public static void TerminateSubscription(string accountCode)
        {
            RefundSubscription(accountCode, RefundType.None);
        }

        public static bool ValidateSubscription(string accountcode)
        {
            bool flag = false;
            try
            {
                if (RecurlyAccount.IsExist(accountcode))
                {
                    if (Get(accountcode).CurrentPeriodEndsAt.Value.Date < DateTime.Now.Date)
                    {
                        flag = false;
                    }
                    else
                    {
                        flag = true;
                    }
                }
                return flag;
            }
            catch (RecurlyException exception)
            {
                if (exception.Errors[0].Message != "Subscription not found")
                {
                    throw exception;
                }
                return false;
            }
        }

        public static void ReactivateSubscription(string accountCode)
        {
            RecurlyClient.PerformRequest(RecurlyClient.HttpRequestMethod.Post, SubscriptionUrl(accountCode) + "/reactivate");
        }

        #region Read and Write XML documents

        internal void ReadXml(XmlTextReader reader)
        {
            //XmlDocument doc = new XmlDocument();
            //doc.Load(reader);
            while (reader.Read())
            {
                // End of subscription element, get out of here
                if (reader.Name == "subscription" && reader.NodeType == XmlNodeType.EndElement)
                    break;

                if (reader.NodeType == XmlNodeType.Element)
                {
                    DateTime dateVal;
                    switch (reader.Name)
                    {
                        case "account":
                            this.Account = RecurlyAccount.GetAccountDetailWithCompleteURL(reader.GetAttribute("href").ToString());
                            break;

                        case "subscription_add_ons":
                            string subscriptionResponse = reader.ReadInnerXml().Trim();

                            //this.AddOn = new RecurlyAddOn(reader); 
                            if (subscriptionResponse == "")
                            {
                                RecurlyAddOn addon = new RecurlyAddOn();
                                addon.Addons = new List<RecurlyAddOn>();
                                this.AddOn = addon;
                            }
                            else
                            {
                                subscriptionResponse = "<xml>\n" + subscriptionResponse + "\n</xml>";
                                using (Stream responseStream = GenerateStreamFromString(subscriptionResponse))
                                {
                                    using (XmlTextReader xmlReader = new XmlTextReader(responseStream))
                                    {
                                        this.AddOn = new RecurlyAddOn(xmlReader);
                                    }

                                }
                                //RecurlyAddOn addon = new RecurlyAddOn();
                                //addon.Addons = new List<RecurlyAddOn>();
                                //this.AddOn = addon;
                            }
                            break;

                        case "uuid":
                            this.uuid = reader.ReadElementContentAsString();
                            break;

                        case "plan_code":
                            this.PlanCode = reader.ReadElementContentAsString();
                            break;

                        case "state":
                            this.State = reader.ReadElementContentAsString();
                            break;

                        case "quantity":
                            this.Quantity = reader.ReadElementContentAsInt();
                            break;

                        case "unit_amount_in_cents":
                            this.UnitAmountInCents = reader.ReadElementContentAsInt();
                            break;

                        case "activated_at":
                            if (DateTime.TryParse(reader.ReadElementContentAsString(), out dateVal))
                                this.ActivatedAt = dateVal;
                            break;

                        case "canceled_at":
                            if (DateTime.TryParse(reader.ReadElementContentAsString(), out dateVal))
                                this.CanceledAt = dateVal;
                            break;

                        case "expires_at":
                            if (DateTime.TryParse(reader.ReadElementContentAsString(), out dateVal))
                                this.ExpiresAt = dateVal;
                            break;

                        case "current_period_started_at":
                            if (DateTime.TryParse(reader.ReadElementContentAsString(), out dateVal))
                                this.CurrentPeriodStartedAt = dateVal;
                            break;

                        case "current_period_ends_at":
                            if (DateTime.TryParse(reader.ReadElementContentAsString(), out dateVal))
                                this.CurrentPeriodEndsAt = dateVal;
                            break;

                        case "trial_started_at":
                            if (DateTime.TryParse(reader.ReadElementContentAsString(), out dateVal))
                                this.TrialPeriodStartedAt = dateVal;
                            break;

                        case "trial_ends_at":
                            if (DateTime.TryParse(reader.ReadElementContentAsString(), out dateVal))
                                this.trialPeriodEndsAt = dateVal;
                            break;

                        case "pending_subscription":
                            // TODO: Parse pending subscription
                            break;
                    }
                }
            }
        }

        protected void WriteSubscriptionXml(XmlTextWriter xmlWriter)
        {
            xmlWriter.WriteStartElement("subscription"); // Start: subscription

            xmlWriter.WriteElementString("plan_code", this.PlanCode);

            if (!String.IsNullOrEmpty(this.CouponCode))
                xmlWriter.WriteElementString("coupon_code", this.CouponCode);

            if (this.Quantity.HasValue)
                xmlWriter.WriteElementString("quantity", this.Quantity.Value.ToString());

            if (this.UnitAmountInCents.HasValue)
                xmlWriter.WriteElementString("unit_amount_in_cents", this.UnitAmountInCents.Value.ToString());

            if (this.TrialPeriodEndsAt.HasValue)
                xmlWriter.WriteElementString("trial_ends_at", this.TrialPeriodEndsAt.Value.ToString("s"));

            this.Account.WriteXml(xmlWriter);

            xmlWriter.WriteEndElement(); // End: subscription
        }

        protected void WriteCreateSubscriptionXml(XmlTextWriter xmlWriter)
        {
            xmlWriter.WriteStartElement("subscription");
            xmlWriter.WriteElementString("plan_code", this.PlanCode);
            xmlWriter.WriteElementString("currency", "USD");
            xmlWriter.WriteElementString("coupon_code", this.CouponCode);
            xmlWriter.WriteStartElement("account");
            xmlWriter.WriteElementString("account_code", this.Account.AccountCode);
            xmlWriter.WriteElementString("email", this.Account.Email);
            xmlWriter.WriteElementString("first_name", this.Account.FirstName);
            xmlWriter.WriteElementString("last_name", this.Account.LastName);
            xmlWriter.WriteEndElement();
            if (this.AddOn != null)
            {
                if (this.AddOn.quantity > 0 && this.AddOn.add_on_code != null && this.AddOn.add_on_code != "")
                {
                    xmlWriter.WriteStartElement("subscription_add_ons");
                    xmlWriter.WriteStartElement("subscription_add_on");
                    xmlWriter.WriteElementString("add_on_code", this.AddOn.add_on_code);
                    xmlWriter.WriteElementString("quantity", this.AddOn.quantity.ToString());
                    xmlWriter.WriteElementString("unit_amount_in_cents", this.AddOn.unit_amount_in_cents.ToString());
                    xmlWriter.WriteEndElement();
                    xmlWriter.WriteEndElement();
                }
            }
            xmlWriter.WriteEndElement();
        }

        protected void WriteChangeSubscriptionNowXml(XmlTextWriter xmlWriter)
        {
            WriteChangeSubscriptionXml(xmlWriter, ChangeTimeframe.Now);
        }

        protected void WriteChangeSubscriptionAtRenewalXml(XmlTextWriter xmlWriter)
        {
            WriteChangeSubscriptionXml(xmlWriter, ChangeTimeframe.Renewal);
        }

        protected void WriteChangeSubscriptionXml(XmlTextWriter xmlWriter, ChangeTimeframe timeframe)
        {


            //xmlWriter = new XmlTextWriter("D:\\Desktop Beckup\\myXmFile.xml", null);
            //xmlWriter.WriteStartDocument();
            xmlWriter.WriteStartElement("subscription"); // Start: subscription

            xmlWriter.WriteElementString("timeframe", (timeframe == ChangeTimeframe.Now ? "now" : "renewal"));

            if (this.Quantity.HasValue)
                xmlWriter.WriteElementString("quantity", this.Quantity.Value.ToString());

            if (!String.IsNullOrEmpty(this.PlanCode))
                xmlWriter.WriteElementString("plan_code", this.PlanCode);

            if (this.AddOns.Count > 0)
            {
                xmlWriter.WriteRaw("<subscription_add_ons type=\"array\">");

                for (var i = 0; i < this.AddOns.Count; i++)
                {
                    xmlWriter.WriteRaw("<subscription_add_on>");
                    xmlWriter.WriteRaw("<add_on_code>" + this.AddOns[i].add_on_code + "</add_on_code>");
                    xmlWriter.WriteRaw("<quantity>" + this.AddOns[i].quantity.ToString() + "</quantity>");
                    xmlWriter.WriteRaw("<unit_amount_in_cents>" + this.AddOns[i].unit_amount_in_cents.ToString() + "</unit_amount_in_cents>");
                    xmlWriter.WriteRaw("</subscription_add_on>");
                }
                xmlWriter.WriteRaw("</subscription_add_ons>");
            }
            else
            {
                xmlWriter.WriteRaw("<subscription_add_ons/>");
            }


            //xmlWriter.WriteStartElement("subscription_add_ons");
            //xmlWriter.WriteStartElement("subscription_add_on");
            //xmlWriter.WriteElementString("add_on_type", "fixed");
            //xmlWriter.WriteElementString("add_on_code", this.AddOn.add_on_code);
            //xmlWriter.WriteElementString("unit_amount_in_cents", this.AddOn.unit_amount_in_cents.ToString());
            //xmlWriter.WriteElementString("quantity", this.AddOn.quantity.ToString());
            //xmlWriter.WriteEndElement();
            //xmlWriter.WriteEndElement();



            //   xmlWriter.WriteIfCollectionHasAny("subscription_add_ons", AddOns);

            //if (this.AddOns != null)
            //{
            //    xmlWriter.WriteStartElement("add_ons");
            //    //this.AddOns.WriteXml(xmlWriter);

            //    foreach (var AddOn in AddOns)
            //    {
            //        AddOn.WriteXml(xmlWriter);
            //    }
            //    xmlWriter.WriteEndElement();
            //}
            //   xmlWriter.WriteElementString("subscription_add_ons", AddOns);


            xmlWriter.WriteEndElement();

            //xmlWriter.WriteEndDocument();
            //// close writer
            //xmlWriter.Close();

        }

        public static Stream GenerateStreamFromString(string s)
        {
            MemoryStream stream = new MemoryStream();
            StreamWriter writer = new StreamWriter(stream);
            writer.Write(s);
            writer.Flush();
            stream.Position = 0;
            return stream;
        }

        #endregion
    }
}