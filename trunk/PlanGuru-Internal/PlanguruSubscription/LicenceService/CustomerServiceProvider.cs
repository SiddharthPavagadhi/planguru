﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LicenceService.CustomerService;
using System.Configuration;
//using Recurly;
using System.Globalization;
using System.Net;
using System.Xml.Linq;
using RecurlyNew;

namespace LicenceService
{
    public class CustomerServiceProvider
    {
        public int CreateCustomer(RecurlyNew.Account  accountInfo,string Password,RecurlyNew.BillingInfo billinginfo)
        {
            try
            {
                int customerLicenseId = 0;
                string CountryName = GetCountryCode(billinginfo.Country);
                CustomerServerSoapClient client = new CustomerServerSoapClient();
                customerLicenseId = client.CreateCustomer(Convert.ToInt32(ConfigurationManager.AppSettings["AuthorId"]), ConfigurationManager.AppSettings["UserId"], ConfigurationManager.AppSettings["Password"], accountInfo.CompanyName, accountInfo.FirstName, accountInfo.LastName, billinginfo.Address1, billinginfo.Address2, billinginfo.City, billinginfo.State, billinginfo.PostalCode, CountryName, accountInfo.Email, Password, billinginfo.PhoneNumber, "", "", true, true);
                return customerLicenseId;
            }
            catch(Exception ex)
            {
                Logger.Logger.Log.Error("Problem with creating Customer, Error is" + ex.Message);
                return -3;
            }
        }

        private string GetCountryCode(string twoLetterCountryCode)
        {
            var region = (from c in CultureInfo.GetCultures(CultureTypes.SpecificCultures)
                          where (new RegionInfo(c.LCID)).EnglishName != ""
                          || (new RegionInfo(c.LCID)).EnglishName != null
                          orderby (new RegionInfo(c.LCID)).EnglishName ascending
                          select new
                          {
                              Code = (new RegionInfo(c.LCID)).TwoLetterISORegionName,
                              Name = (new RegionInfo(c.LCID)).EnglishName
                          }).Distinct();

            var code = region.Where(t => t.Code.ToLower() == twoLetterCountryCode.ToLower()).FirstOrDefault();

            return (code == null) ? twoLetterCountryCode : code.Name.ToString();

        }

        
    }
}
