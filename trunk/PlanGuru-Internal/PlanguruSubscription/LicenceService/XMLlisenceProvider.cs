﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LicenceService.UserLicenseService;
using System.Xml;

namespace LicenceService
{
    public class XMLlisenceProvider
    {
        UserLicenseService.XmlLicenseServiceSoapClient client = new UserLicenseService.XmlLicenseServiceSoapClient();
        public XmlNode AddLicenseUser(int LicenseCustomerId, int ProductOptionId, int SeatQuantity, decimal UnitPrice)
        {
            string sLicenseAdd = "<LicenseAdd xmlns=''>" +
                          "<AuthorID>" + Convert.ToInt32(ConfigurationManager.AppSettings["AuthorId"]) + "</AuthorID>" +
                          "<UserID>" + ConfigurationManager.AppSettings["UserId"] + "</UserID>" +
                          "<UserPassword>" + ConfigurationManager.AppSettings["Password"] + "</UserPassword>" +
                          "<ProdOptionID>" + ProductOptionId + "</ProdOptionID>" +
                          "<Quantity>" + SeatQuantity + "</Quantity>" +
                          "<UnitPrice>" + UnitPrice + "</UnitPrice>" +
                          "<Expiration>12/31/2099</Expiration>" +
                          "<ActivationCount>" + SeatQuantity + "</ActivationCount>" +
                          "<DeactivationCount>-1</DeactivationCount>" +
                          "<LicenseCounter></LicenseCounter>" +
                          "<Version></Version>" +
                          "<LicenseUpdate></LicenseUpdate>" +
                          "<Notes></Notes>" +
                          "<CustomerID>" + LicenseCustomerId + "</CustomerID>" +
                          "<DistributorID></DistributorID>" +
                          "<LicenseeEmail></LicenseeEmail>" +
                          "<LicenseeName></LicenseeName>" +
                          "</LicenseAdd>";

            XmlNode newNode = client.AddS(sLicenseAdd);
            return newNode;
        }

        public XmlNode UpdateUserDefinedFields(int LicenseId, string UserDefinedChar1, string UserDefinedChar2,bool isAccountantAdvisorPlan)
        {
            if (isAccountantAdvisorPlan == false)
            {
                string sUpdateUserDefinedFields = "<UpdateUserDefinedFields xmlns=''>" +
                              "<AuthorID>" + Convert.ToInt32(ConfigurationManager.AppSettings["AuthorId"]) + "</AuthorID>" +
                              "<UserID>" + ConfigurationManager.AppSettings["UserId"] + "</UserID>" +
                              "<UserPassword>" + ConfigurationManager.AppSettings["Password"] + "</UserPassword>" +
                              "<LicenseID>" + LicenseId + "</LicenseID>" +
                              "<UDefChar1>" + UserDefinedChar1 + "</UDefChar1>" +
                              "<UDefChar2>" + UserDefinedChar2 + "</UDefChar2>" +
                              "</UpdateUserDefinedFields>";
                XmlNode ResponseFromUpdateFunction = client.UpdateUserDefinedFieldsS(sUpdateUserDefinedFields);
                return ResponseFromUpdateFunction;
            }
            else
            {
                string sUpdateUserDefinedFields = "<UpdateUserDefinedFields xmlns=''>" +
                             "<AuthorID>" + Convert.ToInt32(ConfigurationManager.AppSettings["AuthorId"]) + "</AuthorID>" +
                             "<UserID>" + ConfigurationManager.AppSettings["UserId"] + "</UserID>" +
                             "<UserPassword>" + ConfigurationManager.AppSettings["Password"] + "</UserPassword>" +
                             "<LicenseID>" + LicenseId + "</LicenseID>" +
                             "<UDefChar1>" + UserDefinedChar1 + "</UDefChar1>" +
                             "<UDefChar2>" + UserDefinedChar2 + "</UDefChar2>" +
                             "<UDefChar3>" + 2 + "</UDefChar3>" +
                             "</UpdateUserDefinedFields>";
                XmlNode ResponseFromUpdateFunction = client.UpdateUserDefinedFieldsS(sUpdateUserDefinedFields);
                return ResponseFromUpdateFunction;
            }
            
        }

        public XmlNode UpdateActivationFields(int LicenseId,int ActivationIncrementCount)
        {
            string sUpdateActivationFields = "<UpdateActivationFields xmlns=''>" +
                                             "<AuthorID>" + Convert.ToInt32(ConfigurationManager.AppSettings["AuthorId"]) + "</AuthorID>" +
                                             "<UserID>" + ConfigurationManager.AppSettings["UserId"] + "</UserID>" +
                                             "<UserPassword>" + ConfigurationManager.AppSettings["Password"] + "</UserPassword>" +
                                             "<LicenseID>" + LicenseId + "</LicenseID>" +
                                             "<ActivationsIncrement>" + ActivationIncrementCount + "</ActivationsIncrement> " +
                                             "</UpdateActivationFields>";

            XmlNode ResponseFromUpdateActivationField = client.UpdateActivationFields(sUpdateActivationFields);
            return ResponseFromUpdateActivationField;
        }

        public XmlNode UpdateUserDefineCharacterCount(int LicenseId,string UserDefinedChar1,string UserDefinedChar2,string Password,int NumberOfAddOnUser)
        {
            string sInfoCheckFields =
                "<LicenseInfoCheck xmlns=''>" +
                       "<LicenseID>" + LicenseId + "</LicenseID>" +
                       "<Password>" + Password + "</Password>" +
                       "<ProductID></ProductID>" +
                "</LicenseInfoCheck>";
            XmlNode ResponseFromInfoCheck = client.InfoCheckS(sInfoCheckFields);
            int numberOfAddIn = Convert.ToInt32(ResponseFromInfoCheck.LastChild.InnerText);
            string sUpdateUserDefinedFields = "<UpdateUserDefinedFields xmlns=''>" +
                             "<AuthorID>" + Convert.ToInt32(ConfigurationManager.AppSettings["AuthorId"]) + "</AuthorID>" +
                             "<UserID>" + ConfigurationManager.AppSettings["UserId"] + "</UserID>" +
                             "<UserPassword>" + ConfigurationManager.AppSettings["Password"] + "</UserPassword>" +
                             "<LicenseID>" + LicenseId + "</LicenseID>" +
                             "<UDefChar1>" + UserDefinedChar1 + "</UDefChar1>" +
                             "<UDefChar2>" + UserDefinedChar2 + "</UDefChar2>" +
                             "<UDefChar3>" + (numberOfAddIn + NumberOfAddOnUser) + "</UDefChar3>" +
                             "</UpdateUserDefinedFields>";
            XmlNode ResponseFromUpdateFunction = client.UpdateUserDefinedFieldsS(sUpdateUserDefinedFields);
            return ResponseFromUpdateFunction;
        }
    }
}
