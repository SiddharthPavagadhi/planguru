﻿using log4net;
using log4net.Config;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LicenceService.Logger
{
    public class Logger
    {
        private static ILog _log;
        public static ILog Log
        {
            get
            {

                if ((_log == null))
                {
                    _log = LogManager.GetLogger(typeof(Logger));
                }

                return _log;
            }
        }

        public static void InitLogger()
        {
            XmlConfigurator.Configure();
        }

        public static void SetProperty(string name, object value)
        {
            log4net.GlobalContext.Properties["name"] = value;

        }
    }
}
