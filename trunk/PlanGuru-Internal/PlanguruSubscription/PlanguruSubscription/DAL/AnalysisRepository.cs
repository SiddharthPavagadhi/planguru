﻿using PlanguruSubscription.Models;
using PlanguruSubscription.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PlanguruSubscription.DAL
{
    public class AnalysisRepository : GenericRepository<Analysis>, IAnalysisRepository, IDisposable
    {

        private bool disposed = false;

        public AnalysisRepository(DataAccess context)
            : base(context)
        {
        }

        public IEnumerable<Analysis> GetAnalyses()
        {
            return context.Analyses.OrderBy(a => a.AnalysisName).ToList();
        }


        public Analysis GetAnalysisById(int analysisId)
        {
            return context.Analyses.Where(a => a.AnalysisId == analysisId).FirstOrDefault();
        }

        public int DeleteAnalysisByCompany(int companyId)
        {
            Utility utility = new Utility();
            Logger.Log.Info(("Start Query : Get the list of Analyses by CompanyId: " + companyId));
            var analyses = (from d in context.Analyses.Where(a => a.CompanyId == companyId) select d);
            if ((analyses != null) & analyses.Count() > 0)
            {
                Logger.Log.Info(("Analyses items count : " + analyses.Count()));
                Logger.Log.Info("Start deleting : Analyses items");

                analyses.ToList().ForEach(item => utility.AccountTypeRepository.DeleteAccountTypeByAnalysis(item.AnalysisId));
                analyses.ToList().ForEach(item => utility.AccountRepository.DeleteAccountByAnalysis(item.AnalysisId));
                analyses.ToList().ForEach(item => utility.BalanceRepository.DeleteBalanceByAnalysis(item.AnalysisId));
                analyses.ToList().ForEach(item => utility.DashboardRepository.DeleteDashboardItemsByAnalysis(item.AnalysisId));
                analyses.ToList().ForEach(item => utility.ScorecardRepository.DeleteScorecardItemsByAnalysis(item.AnalysisId));
                analyses.ToList().ForEach(item => utility.SaveViewRepository.DeleteSavedViewItemsByAnalysis(item.AnalysisId));
                analyses.ToList().ForEach(item => utility.UserAnalysisMappingRepository.DeleteUserAnalysisMappingByAnalysis(item.AnalysisId));
                analyses.ToList().ForEach(item => context.Analyses.Remove(item));
                context.SaveChanges();
                Logger.Log.Info(("End deleting : Analyses items"));
            }
            return analyses.Count();
        }



        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }


    }
}