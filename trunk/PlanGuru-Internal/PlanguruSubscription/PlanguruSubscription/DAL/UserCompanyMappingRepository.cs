﻿using PlanguruSubscription.Models;
using PlanguruSubscription.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PlanguruSubscription.DAL
{
    public class UserCompanyMappingRepository : GenericRepository<UserCompanyMapping>, IUserCompanyMappingRepository, IDisposable
    {
        private bool disposed = false;

        public UserCompanyMappingRepository(DataAccess context)
            : base(context)
        {
        }

        public List<int> GetDistinctCompanies(List<int> deactivatedUsers)
        {
            return (from ucm in context.UserCompanyMappings.Where(ucm => deactivatedUsers.Contains(ucm.UserId)) select ucm.CompanyId).Distinct().ToList();

        }

        public object DeleteUserCompanyMappingByCompany(int companyId)
        {
            Logger.Log.Info(("Start Query : Get the list of Users by CompanyId: " + companyId));
            var companies = context.UserCompanyMappings.Where(a => a.CompanyId == companyId).ToList();

            if ((companies != null) & companies.Count > 0)
            {
                Logger.Log.Info(("UserCompanyMapping  count : " + companies.Count));
                Logger.Log.Info("Start deleting : UserCompanyMapping By CompanyId");
                companies.ToList().ForEach(item => context.UserCompanyMappings.Remove(item));
                Logger.Log.Info(("End deleting : UserCompanyMapping By CompanyId"));
            }
            return companies.Count;
        }

        public object DeleteUserCompanyMappingByUserId(int userId)
        {
            Logger.Log.Info(("Start Query : Get the list of User Mapping by UserId: " + userId));
            var userMapping = (from d in context.UserCompanyMappings.Where(a => a.UserId == userId) select d);
            if ((userMapping != null))
            {
                Logger.Log.Info(("User Mapping items count : " + userMapping.Count()));
                Logger.Log.Info("Start deleting : User Mapping items");
                userMapping.ToList().ForEach(item => context.UserCompanyMappings.Remove(item));
                Logger.Log.Info(("End Deleting : User Mapping items"));
            }
            return userMapping.Count();

        }


        public void Save()
        {
            context.SaveChanges();
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}