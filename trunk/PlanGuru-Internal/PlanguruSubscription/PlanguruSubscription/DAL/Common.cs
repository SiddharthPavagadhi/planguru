﻿using PlanguruSubscription.Models;
using PlanguruSubscription.Modules;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace PlanguruSubscription.DAL
{
    public class Common
    {
        public static string HostName(System.Web.HttpRequestBase Request)
        {

            string host = null;
            if ((!string.IsNullOrEmpty(System.Configuration.ConfigurationManager.AppSettings["Host"])))
            {
                host = System.Configuration.ConfigurationManager.AppSettings["Host"].ToString();
            }
            else
            {
                host = Request.Url.Host;
            }
            return host;

        }

        public static string Encrypt(string plainText)
        {

            string passPhrase = "yourPassPhrase";
            string saltValue = "mySaltValue";
            string hashAlgorithm = "SHA1";

            int passwordIterations = 2;
            string initVector = "@1B2c3D4e5F6g7H8";
            int keySize = 256;

            byte[] initVectorBytes = Encoding.ASCII.GetBytes(initVector);
            byte[] saltValueBytes = Encoding.ASCII.GetBytes(saltValue);

            byte[] plainTextBytes = Encoding.UTF8.GetBytes(plainText);


            PasswordDeriveBytes password = new PasswordDeriveBytes(passPhrase, saltValueBytes, hashAlgorithm, passwordIterations);

            byte[] keyBytes = password.GetBytes(keySize / 8);

            RijndaelManaged symmetricKey = new RijndaelManaged();

            symmetricKey.Mode = CipherMode.CBC;

            ICryptoTransform encryptor = symmetricKey.CreateEncryptor(keyBytes, initVectorBytes);

            MemoryStream memoryStream = new MemoryStream();
            CryptoStream cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write);

            cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
            cryptoStream.FlushFinalBlock();
            byte[] cipherTextBytes = memoryStream.ToArray();
            memoryStream.Close();
            cryptoStream.Close();
            string cipherText = Convert.ToBase64String(cipherTextBytes);
            return cipherText;
        }

        public static string MergeCustomerEmailBody(Customer customer, EmailInfo emailinfo)
        {

            string mergeBody = string.Empty;

            if (emailinfo.Id == (int)EmailType.NewSubscriptionSignup)
            {
                mergeBody = emailinfo.EmailBody.Replace("##CUSTOMERID##", customer.CustomerId.ToString());
                mergeBody = mergeBody.Replace("##AMOUNT##", customer.SubscriptionAmount.ToString());
                mergeBody = mergeBody.Replace("##NewCustomerName##", customer.CustomerCompanyName);
                mergeBody = mergeBody.Replace("##NewCustomerStreet##", customer.CustomerAddress1 + " " + customer.CustomerAddress2);
                mergeBody = mergeBody.Replace("##NewCustomerCityStateEtc##", customer.City + ", " + customer.State);
                mergeBody = mergeBody.Replace("##SUBSCRIPTIONSCHEDULE##", string.Concat(customer.SubscriptionStartAt, " - ", customer.SubscriptionEndAt));
                mergeBody = mergeBody.Replace("##NewCustomerEmail##", customer.CustomerEmail);
                mergeBody = mergeBody.Replace("##DayoftheMonth##", GetDayoftheMonth());

            }
            else if (emailinfo.Id == (int)EmailType.SubcriptionCancellation)
            {
                mergeBody = emailinfo.EmailBody.Replace("##CUSTOMERID##", customer.CustomerId.ToString());
                //mergeBody = mergeBody.Replace("##AMOUNT##", customer)
                mergeBody = mergeBody.Replace("##TelephoneNumber##", System.Configuration.ConfigurationManager.AppSettings["PLANGURUSUPPORTNO"]);
                //mergeBody = mergeBody.Replace("##PAYMENTCYCLE##", customer.PaymentCycle)
                mergeBody = mergeBody.Replace("##SUBSCRIPTIONSTARTDATE##", customer.SubscriptionStartAt);

            }
            return mergeBody;

        }
        private static string GetDayoftheMonth()
        {
            string dayofMonth = string.Empty;
            int currentday = DateTime.Now.Day;

            dynamic ones = currentday % 10;
            dynamic tens = Math.Floor(currentday / 10f) % 10;
            if (tens == 1)
            {
                dayofMonth = currentday + "th";
                return dayofMonth;
            }
            switch (currentday % 10)
            {
                case 1:
                    dayofMonth = currentday + "st";
                    break;
                case 2:
                    dayofMonth = currentday + "nd";
                    break;
                case 3:
                    dayofMonth = currentday + "rd";
                    break;
                default:
                    dayofMonth = currentday + "th";
                    break;
            }

            return dayofMonth;
        }
        public static void SendCustomerEmail(Customer customer, EmailInfo emailinfo)
        {
            Logger.Log.Info(string.Format("SendEmail started "));
            string smtp = System.Configuration.ConfigurationManager.AppSettings["Email_SMTP"];
            int port = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["Email_PORT"]);
            string EMAILFROM = System.Configuration.ConfigurationManager.AppSettings["Email_FROM"];
            string EMAILPASS = System.Configuration.ConfigurationManager.AppSettings["Email_PASS"];
            bool Email_EnableSsl = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["Email_EnableSsl"]);

            //Dim myEnum As StatusE
            //User.Password = Common.Decrypt(User.Password)
            //myEnum = CType(User.Status, StatusE)
            MailMessage mailMsg = new MailMessage();
            mailMsg.To.Add(new MailAddress(customer.CustomerEmail));
            mailMsg.From = new MailAddress(EMAILFROM);
            mailMsg.Subject = emailinfo.EmailSubject;
            mailMsg.IsBodyHtml = true;
            mailMsg.Body = emailinfo.EmailBody;
            SmtpClient emailClient = new SmtpClient();
            emailClient.UseDefaultCredentials = false;
            emailClient.Port = port;
            emailClient.EnableSsl = Email_EnableSsl;
            emailClient.Host = smtp;
            emailClient.Timeout = 3600000;
            emailClient.Credentials = new System.Net.NetworkCredential(EMAILFROM, EMAILPASS);
            emailClient.Send(mailMsg);
            Logger.Log.Info(string.Format("SendEmail   Ended "));
        }

        public static string Generate(int minLength, int maxLength, bool IsAlphaNumeric)
        {
            string functionReturnValue = null;
            // Define supported password characters divided into groups.
            // You can add (or remove) characters to (from) these groups.
            string PASSWORD_CHARS_LCASE = "abcdefghijklmnopqrstuvwxyz";
            string PASSWORD_CHARS_UCASE = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            string PASSWORD_CHARS_NUMERIC = "0123456789";
            string PASSWORD_CHARS_SPECIAL = "*$-+?_&=!%{}/";
            // Make sure that input parameters are valid.
            if ((minLength <= 0 | maxLength <= 0 | minLength > maxLength))
            {
                functionReturnValue = null;
            }

            // Create a local array containing supported password characters
            // grouped by types. You can remove character groups from this
            // array, but doing so will weaken the password strength.
            char[][] charGroups;

            if (IsAlphaNumeric == true)
            {
                charGroups = new char[][] {
		            PASSWORD_CHARS_LCASE.ToCharArray(),
		            PASSWORD_CHARS_UCASE.ToCharArray(),
		            PASSWORD_CHARS_NUMERIC.ToCharArray()
	            };
            }
            else
            {
                charGroups = new char[][] {
		            PASSWORD_CHARS_LCASE.ToCharArray(),
		            PASSWORD_CHARS_UCASE.ToCharArray(),
		            PASSWORD_CHARS_NUMERIC.ToCharArray(),
		            PASSWORD_CHARS_SPECIAL.ToCharArray()
	            };
            }

            // Use this array to track the number of unused characters in each
            // character group.
            int[] charsLeftInGroup = new int[charGroups.Length];

            // Initially, all characters in each group are not used.
            int I = 0;
            for (I = 0; I <= charsLeftInGroup.Length - 1; I++)
            {
                charsLeftInGroup[I] = charGroups[I].Length;
            }

            // Use this array to track (iterate through) unused character groups.
            int[] leftGroupsOrder = new int[charGroups.Length];

            // Initially, all character groups are not used.
            for (I = 0; I <= leftGroupsOrder.Length - 1; I++)
            {
                leftGroupsOrder[I] = I;
            }

            // Because we cannot use the default randomizer, which is based on the
            // current time (it will produce the same "random" number within a
            // second), we will use a random number generator to seed the
            // randomizer.

            // Use a 4-byte array to fill it with random bytes and convert it then
            // to an integer value.
            byte[] randomBytes = new byte[4];

            // Generate 4 random bytes.
            RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();

            rng.GetBytes(randomBytes);

            // Convert 4 bytes into a 32-bit integer value.
            int seed = ((randomBytes[0] & 0x7f) << 24 | randomBytes[1] << 16 | randomBytes[2] << 8 | randomBytes[3]);

            // Now, this is real randomization.
            Random random = new Random(seed);

            // This array will hold password characters.
            char[] password = null;

            // Allocate appropriate memory for the password.
            if ((minLength < maxLength))
            {
                password = new char[random.Next(minLength - 1, maxLength) + 1];
            }
            else
            {
                password = new char[minLength];
            }

            // Index of the next character to be added to password.
            int nextCharIdx = 0;

            // Index of the next character group to be processed.
            int nextGroupIdx = 0;

            // Index which will be used to track not processed character groups.
            int nextLeftGroupsOrderIdx = 0;

            // Index of the last non-processed character in a group.
            int lastCharIdx = 0;

            // Index of the last non-processed group.
            int lastLeftGroupsOrderIdx = leftGroupsOrder.Length - 1;

            // Generate password characters one at a time.

            for (I = 0; I <= password.Length - 1; I++)
            {
                // If only one character group remained unprocessed, process it;
                // otherwise, pick a random character group from the unprocessed
                // group list. To allow a special character to appear in the
                // first position, increment the second parameter of the Next
                // function call by one, i.e. lastLeftGroupsOrderIdx + 1.
                if ((lastLeftGroupsOrderIdx == 0))
                {
                    nextLeftGroupsOrderIdx = 0;
                }
                else
                {
                    nextLeftGroupsOrderIdx = random.Next(0, lastLeftGroupsOrderIdx);
                }

                // Get the actual index of the character group, from which we will
                // pick the next character.
                nextGroupIdx = leftGroupsOrder[nextLeftGroupsOrderIdx];

                // Get the index of the last unprocessed characters in this group.
                lastCharIdx = charsLeftInGroup[nextGroupIdx] - 1;

                // If only one unprocessed character is left, pick it; otherwise,
                // get a random character from the unused character list.
                if ((lastCharIdx == 0))
                {
                    nextCharIdx = 0;
                }
                else
                {
                    nextCharIdx = random.Next(0, lastCharIdx + 1);
                }

                // Add this character to the password.
                password[I] = charGroups[nextGroupIdx][nextCharIdx];

                // If we processed the last character in this group, start over.
                if ((lastCharIdx == 0))
                {
                    charsLeftInGroup[nextGroupIdx] = charGroups[nextGroupIdx].Length;
                    // There are more unprocessed characters left.
                }
                else
                {
                    // Swap processed character with the last unprocessed character
                    // so that we don't pick it until we process all characters in
                    // this group.
                    if ((lastCharIdx != nextCharIdx))
                    {
                        char temp = charGroups[nextGroupIdx][lastCharIdx];
                        charGroups[nextGroupIdx][lastCharIdx] = charGroups[nextGroupIdx][nextCharIdx];
                        charGroups[nextGroupIdx][nextCharIdx] = temp;
                    }

                    // Decrement the number of unprocessed characters in
                    // this group.
                    charsLeftInGroup[nextGroupIdx] = charsLeftInGroup[nextGroupIdx] - 1;
                }

                // If we processed the last group, start all over.
                if ((lastLeftGroupsOrderIdx == 0))
                {
                    lastLeftGroupsOrderIdx = leftGroupsOrder.Length - 1;
                    // There are more unprocessed groups left.
                }
                else
                {
                    // Swap processed group with the last unprocessed group
                    // so that we don't pick it until we process all groups.
                    if ((lastLeftGroupsOrderIdx != nextLeftGroupsOrderIdx))
                    {
                        int temp = leftGroupsOrder[lastLeftGroupsOrderIdx];
                        leftGroupsOrder[lastLeftGroupsOrderIdx] = leftGroupsOrder[nextLeftGroupsOrderIdx];
                        leftGroupsOrder[nextLeftGroupsOrderIdx] = temp;
                    }

                    // Decrement the number of unprocessed groups.
                    lastLeftGroupsOrderIdx = lastLeftGroupsOrderIdx - 1;
                }
            }

            // Convert password characters into a string and return the result.
            functionReturnValue = new string(password);
            return functionReturnValue;
        }

        public static string Decrypt(string cipherText)
        {

            string plainText = string.Empty;

            if ((string.IsNullOrEmpty(cipherText)))
            {
                plainText = cipherText;
            }
            else
            {
                string passPhrase = "yourPassPhrase";
                string saltValue = "mySaltValue";
                string hashAlgorithm = "SHA1";

                int passwordIterations = 2;
                string initVector = "@1B2c3D4e5F6g7H8";
                int keySize = 256;
                // Convert strings defining encryption key characteristics into byte
                // arrays. Let us assume that strings only contain ASCII codes.
                // If strings include Unicode characters, use Unicode, UTF7, or UTF8
                // encoding.
                byte[] initVectorBytes = Encoding.ASCII.GetBytes(initVector);
                byte[] saltValueBytes = Encoding.ASCII.GetBytes(saltValue);

                // Convert our ciphertext into a byte array.
                byte[] cipherTextBytes = Convert.FromBase64String(cipherText);

                // First, we must create a password, from which the key will be 
                // derived. This password will be generated from the specified 
                // passphrase and salt value. The password will be created using
                // the specified hash algorithm. Password creation can be done in
                // several iterations.
                PasswordDeriveBytes password = new PasswordDeriveBytes(passPhrase, saltValueBytes, hashAlgorithm, passwordIterations);

                // Use the password to generate pseudo-random bytes for the encryption
                // key. Specify the size of the key in bytes (instead of bits).
                byte[] keyBytes = password.GetBytes(keySize / 8);

                // Create uninitialized Rijndael encryption object.
                RijndaelManaged symmetricKey = new RijndaelManaged();

                // It is reasonable to set encryption mode to Cipher Block Chaining
                // (CBC). Use default options for other symmetric key parameters.
                symmetricKey.Mode = CipherMode.CBC;

                // Generate decryptor from the existing key bytes and initialization 
                // vector. Key size will be defined based on the number of the key 
                // bytes.
                ICryptoTransform decryptor = symmetricKey.CreateDecryptor(keyBytes, initVectorBytes);

                // Define memory stream which will be used to hold encrypted data.
                MemoryStream memoryStream = new MemoryStream(cipherTextBytes);

                // Define cryptographic stream (always use Read mode for encryption).
                CryptoStream cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read);

                // Since at this point we don't know what the size of decrypted data
                // will be, allocate the buffer long enough to hold ciphertext;
                // plaintext is never longer than ciphertext.
                byte[] plainTextBytes = new byte[cipherTextBytes.Length];

                // Start decrypting.
                int decryptedByteCount = cryptoStream.Read(plainTextBytes, 0, plainTextBytes.Length);

                // Close both streams.
                memoryStream.Close();
                cryptoStream.Close();

                // Convert decrypted data into a string. 
                // Let us assume that the original plaintext string was UTF8-encoded.
                plainText = Encoding.UTF8.GetString(plainTextBytes, 0, decryptedByteCount);

                // Return decrypted string.   

            }

            return plainText;
        }

        public static string MergeUserEmailBody(User User, EmailInfo emailinfo, string resetUrl, int? AddOnUserCount, Customer customerInfo)
        {
            string MergeContent = string.Empty;
            string mergeBody = string.Empty;
            if (emailinfo.Id == (int)EmailType.NewUserAdded)
            {
                mergeBody = emailinfo.EmailBody.Replace("##USERID##", User.UserName.ToString());
                mergeBody = mergeBody.Replace("##EMAIL##", User.UserEmail);
                mergeBody = mergeBody.Replace("##USERFULLNAME##", User.FirstName + " " + User.LastName);
                mergeBody = mergeBody.Replace("##TEMPPASSWORD##", Decrypt(User.Password));
                mergeBody = mergeBody.Replace("##PASSWORDRESETLINK##", resetUrl + "User/Verification?id=" + User.UserId.ToString() + "&securitykey=" + User.SecurityKey);


            }
            else if (emailinfo.Id == (int)EmailType.PasswordReset)
            {
                mergeBody = emailinfo.EmailBody.Replace("##USERID##", User.UserName.ToString());
                mergeBody = mergeBody.Replace("##EMAIL##", User.UserEmail);
                mergeBody = mergeBody.Replace("##USERFULLNAME##", User.FirstName + " " + User.LastName);
                mergeBody = mergeBody.Replace("##TEMPPASSWORD##", Decrypt(User.Password));
                mergeBody = mergeBody.Replace("##PASSWORDRESETLINK##", resetUrl + "User/Verification?id=" + User.UserId.ToString() + "&securitykey=" + User.SecurityKey);
                mergeBody = mergeBody.Replace("##PLANGURUTELEPHONENUMBER##", System.Configuration.ConfigurationManager.AppSettings["PLANGURUSUPPORTNO"]);

            }
            else if (emailinfo.Id == (int)EmailType.NewUserSignUp)
            {
                mergeBody = emailinfo.EmailBody.Replace("##USERID##", User.UserName.ToString());
                mergeBody = mergeBody.Replace("##EMAIL##", User.UserEmail);
                mergeBody = mergeBody.Replace("##USERFULLNAME##", User.FirstName + " " + User.LastName);
                mergeBody = mergeBody.Replace("##AddedDate##", DateTime.Now.ToString("MMM dd, yyyy"));
                mergeBody = mergeBody.Replace("##DayoftheMonth##", GetDayoftheMonth());
                mergeBody = mergeBody.Replace("##TEMPPASSWORD##", Decrypt(User.Password));
                mergeBody = mergeBody.Replace("##PASSWORDRESETLINK##", resetUrl + "User/Verification?id=" + User.UserId.ToString() + "&securitykey=" + User.SecurityKey);

            }
            else if (emailinfo.Id == (int)EmailType.UserDeleted)
            {
                mergeBody = emailinfo.EmailBody.Replace("##USERID##", User.UserName.ToString());
                mergeBody = mergeBody.Replace("##EMAIL##", User.UserEmail);
                mergeBody = mergeBody.Replace("##USERFULLNAME##", User.FirstName + " " + User.LastName);
                mergeBody = mergeBody.Replace("##TODAYDATE##", DateTime.Now.ToString("MMM dd, yyyy"));

            }
            else if (emailinfo.Id == (int)EmailType.ForgotUsername)
            {
                mergeBody = emailinfo.EmailBody.Replace("##USERNAME##", User.UserName.ToString());
                mergeBody = mergeBody.Replace("##USERNAME##", User.UserName);
            }
            else if (emailinfo.Id == (int)EmailType.NewUserAddedForBusiness)
            {
                mergeBody = emailinfo.EmailBody.Replace("##USERFULLNAME##", User.FirstName + " " + User.LastName);
                mergeBody = mergeBody.Replace("##PASSWORDRESETLINK##", resetUrl + "User/Verification?id=" + User.UserId.ToString() + "&securitykey=" + User.SecurityKey);
                mergeBody = mergeBody.Replace("##USERID##", User.UserName.ToString());
                mergeBody = mergeBody.Replace("##TEMPPASSWORD##", Decrypt(User.Password));
                mergeBody = mergeBody.Replace("##PLANGURUURL##", resetUrl);


                var path = System.Web.HttpContext.Current.Server.MapPath("~/MailInfo/DynamicContent.html");
                if (AddOnUserCount > 0)
                {

                    string DynamicContent = string.Empty;
                    using (var reader = new StreamReader(path))
                    {
                        DynamicContent = reader.ReadToEnd();
                    }
                    DynamicContent = DynamicContent.Replace("##UserCount##", "&nbsp;&ndash;&nbsp;" + (AddOnUserCount + 1).ToString());
                    DynamicContent = DynamicContent.Replace("##LICENSEID##", customerInfo.LicenseId.ToString());
                    DynamicContent = DynamicContent.Replace("##LICENSEPASSWORD##", customerInfo.ActivationPassword);
                    MergeContent = MergeContent + DynamicContent;

                }
                else
                {
                    string DynamicContent = string.Empty;
                    using (var reader = new StreamReader(path))
                    {
                        DynamicContent = reader.ReadToEnd();
                    }
                    DynamicContent = DynamicContent.Replace("##UserCount##", "");
                    DynamicContent = DynamicContent.Replace("##LICENSEID##", customerInfo.LicenseId.ToString());
                    DynamicContent = DynamicContent.Replace("##LICENSEPASSWORD##", customerInfo.ActivationPassword);
                    MergeContent = MergeContent + DynamicContent;

                }

                mergeBody = mergeBody.Replace("[DynamicContent]", MergeContent);
                mergeBody = mergeBody.Replace("#HostName#", System.Configuration.ConfigurationManager.AppSettings["LiveHostName"]);

            }
            else if (emailinfo.Id == (int)EmailType.AdditionalUserAdded)
            {
                mergeBody = emailinfo.EmailBody.Replace("#HostName#", System.Configuration.ConfigurationManager.AppSettings["LiveHostName"]);
                mergeBody = mergeBody.Replace("##NumberOfAddOnUser##", AddOnUserCount.ToString());
                mergeBody = mergeBody.Replace("##PLANGURUURL##", resetUrl);
            }
            return mergeBody;
        }

        public static void SendUserEmail(User User, EmailInfo emailinfo)
        {
            Logger.Log.Info(string.Format("SendEmail started "));
            string smtp = System.Configuration.ConfigurationManager.AppSettings["Email_SMTP"];
            int port = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["Email_PORT"]);
            string EMAILFROM = System.Configuration.ConfigurationManager.AppSettings["Email_FROM"];
            string From_DisplayName = System.Configuration.ConfigurationManager.AppSettings["From_DisplayName"];
            string EMAILPASS = System.Configuration.ConfigurationManager.AppSettings["Email_PASS"];
            string EMAILBCC = System.Configuration.ConfigurationManager.AppSettings["Email_BCC"];
            bool Email_EnableSsl = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["Email_EnableSsl"]);

            StatusE myEnum = default(StatusE);
            User.Password = Decrypt(User.Password);
            myEnum = (StatusE)User.Status;
            MailMessage mailMsg = new MailMessage();
            mailMsg.To.Add(new MailAddress(User.Customer.CustomerEmail));
            if(emailinfo.Id == (int)EmailType.NewUserAddedForBusiness)
            {
                mailMsg.Bcc.Add(new MailAddress(EMAILBCC));
            }
            mailMsg.From = new MailAddress(EMAILFROM, From_DisplayName);
            mailMsg.Subject = emailinfo.EmailSubject;
            mailMsg.IsBodyHtml = true;
            mailMsg.Body = emailinfo.EmailBody;
            SmtpClient emailClient = new SmtpClient();
            emailClient.UseDefaultCredentials = false;
            emailClient.Port = port;
            emailClient.EnableSsl = Email_EnableSsl;
            emailClient.Host = smtp;
            emailClient.Timeout = 3600000;
            emailClient.Credentials = new System.Net.NetworkCredential(EMAILFROM, EMAILPASS);
            emailClient.Send(mailMsg);
            Logger.Log.Info(string.Format("SendEmail   Ended "));
        }








    }
}