﻿using PlanguruSubscription.Models;
using PlanguruSubscription.Modules;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace PlanguruSubscription.DAL
{
    public class UserRepository : GenericRepository<User>, IUserRepository, IDisposable
    {
        private bool disposed = false;

        public UserRepository(DataAccess context)
            : base(context)
        {
        }

        public User ValidateUserAccount(string UserName, string Password)
        {
            return (from user in context.Users.Where(u => u.UserName.Equals(UserName) & u.Password.Equals(Password)) select user).SingleOrDefault();
        }

        public IEnumerable<User> GetUsers()
        {
            return context.Users.ToList();
        }
        public IEnumerable<User> GetUsers(int customerId, int userId)
        {
            dynamic getUserList = null;
            if ((userId == null))
            {
                getUserList = (context.Users.Where(a => a.CustomerId == customerId & (a.Status == (int)StatusE.Active | a.Status == (int)StatusE.Pending)));
            }
            else
            {
                getUserList = (context.Users.Where(a => a.CustomerId == customerId & a.UserId != userId & (a.Status == (int)StatusE.Active | a.Status == (int)StatusE.Pending)));
            }
            return (getUserList);
        }


        public User GetUserByID(int userId)
        {
            return context.Users.Find(userId);
        }
        public IQueryable<User> UsernameAvailibility(string UserName)
        {
            return (from user in context.Users.Where(u => u.UserName.ToLower().Equals(UserName.ToLower()) & u.Status <= 2) select user);
        }
        public IQueryable<User> EmailAddressAvailibility(string EmailAddress)
        {
            return (from user in context.Users.Where(u => u.UserEmail.ToLower().Equals(EmailAddress.ToLower()) & u.Status <= 2) select user);
        }
        public void UpdateUser(User user)
        {
            context.Entry(user).State = EntityState.Modified;
        }
        public void DeleteNonSAUUser(int userId)
        {
            Logger.Log.Info(("Start Execution of DeleteNonSAUUser"));
            Utility utility = new Utility();
            Logger.Log.Info(("Start Query :Delete the UserAnalysisMapping For: " + userId));
            utility.UserAnalysisMappingRepository.DeleteUserAnalysisMappingByUserId(userId);
            utility.UserAnalysisMappingRepository.Save();
            Logger.Log.Info(("Start Query :Delete the UserCompanyMapping For: " + userId));
            utility.UserCompanyMappingRepository.DeleteUserCompanyMappingByUserId(userId);
            utility.UserCompanyMappingRepository.Save();
            Logger.Log.Info(("Start Query :Delete the Dashboard Items For: " + userId));
            utility.DashboardRepository.DeleteDashboardItemsByUserId(userId);
            Logger.Log.Info("End Execution of DeleteNonSAUUser");
        }

        public User GetSAUUser(int customerId)
        {
            User userInfo = (from user in context.Users.Where(u => u.Customer.CustomerId.Equals(customerId) && u.UserRoleId == (int)UserRoles.SAU) select user).SingleOrDefault();
            userInfo.Customer.PlanType = context.SubscriptionPlans.Where(i => i.PlanTypeId == userInfo.Customer.PlanTypeId).Select(k => k.PlanType).FirstOrDefault();
            return userInfo;
        }

        public int GetNumberOfAddOn(int customerId)
        {
            var numberOfAddOn = context.Customers.Where(i => i.CustomerId == customerId).Select(k => k.NumberOfAddOns).FirstOrDefault();
            return Convert.ToInt32(numberOfAddOn);
        }


        public void Save()
        {
            context.SaveChanges();
        }




        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}