﻿using PlanguruSubscription.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PlanguruSubscription.DAL
{
    public class UserRolePermissionRepository : GenericRepository<UserRolePermission>, IUserRolePermissionRepository, IDisposable
    {
        private bool disposed = false;

        public UserRolePermissionRepository(DataAccess context)
            : base(context)
        {
        }

        public IEnumerable<UserRolePermission> GetUserRolePermission()
        {
            return context.UserRolePermissions.ToList();
        }


        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}