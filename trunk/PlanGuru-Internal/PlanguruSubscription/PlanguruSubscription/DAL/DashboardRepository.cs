﻿using PlanguruSubscription.Models;
using PlanguruSubscription.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PlanguruSubscription.DAL
{
    public class DashboardRepository : GenericRepository<Dashboard>, IDashboardRepository, IDisposable
    {
        private bool disposed = false;

        public DashboardRepository(DataAccess context)
            : base(context)
        {
        }

        public object DeleteDashboardItemsByAnalysis(int analysisId)
        {
            Logger.Log.Info(("Start Query : Get the list of Accounts by AnalysisId: " + analysisId));
            var dashboard = (from d in context.Dashboards.Where(a => a.AnalysisId == analysisId) select d);
            if (!(dashboard == null & dashboard.Count() > 0))
            {
                Logger.Log.Info(("Dashboard items count : " + dashboard.Count()));
                Logger.Log.Info("Start deleting : Dashboard items");
                dashboard.ToList().ForEach(item => context.Dashboards.Remove(item));
                context.SaveChanges();
                Logger.Log.Info(("End deleting : Dashboard items"));
            }
            return dashboard.Count();

        }
        public object DeleteDashboardItemsByUserId(int userId)
        {
            Logger.Log.Info(("Start Query : Get the list of Dashboard items by UserId: " + userId));
            var dashboardItems = (from d in context.Dashboards.Where(a => a.UserId == userId) select d);
            if ((dashboardItems != null) & dashboardItems.Count() > 0)
            {
                Logger.Log.Info(("Dashboard items count : " + dashboardItems.Count()));
                Logger.Log.Info("Start deleting : Dashboard items");
                dashboardItems.ToList().ForEach(item => context.Dashboards.Remove(item));
                Logger.Log.Info(("End Deleting : Dashboard items"));
            }
            return dashboardItems.Count();

        }
        public void Save()
        {
            context.SaveChanges();
        }



        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

    }
}