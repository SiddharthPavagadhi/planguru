﻿using PlanguruSubscription.Models;
using PlanguruSubscription.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PlanguruSubscription.DAL
{
    public class SaveViewRepository : GenericRepository<SaveView>, ISaveViewRepository, IDisposable
    {
        private bool disposed = false;

        public SaveViewRepository(DataAccess context)
            : base(context)
        {
        }

        public object DeleteSavedViewItemsByAnalysis(int analysisId)
        {
            Logger.Log.Info(("Start Query : Get the list of saved view by AnalysisId: " + analysisId));
            var savedView = (from d in context.SaveView.Where(a => a.analysisID == analysisId) select d);
            if (!(savedView == null & savedView.Count() > 0))
            {
                Logger.Log.Info(("SavedView items count : " + savedView.Count()));
                Logger.Log.Info("Start deleting : SavedView items");
                savedView.ToList().ForEach(item => context.SaveView.Remove(item));
                context.SaveChanges();
                Logger.Log.Info(("End deleting : SavedView items"));
            }
            return savedView.Count();

        }


        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}