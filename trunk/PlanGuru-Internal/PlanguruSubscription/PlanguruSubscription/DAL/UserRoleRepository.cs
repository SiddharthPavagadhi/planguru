﻿using PlanguruSubscription.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PlanguruSubscription.DAL
{
    public class UserRoleRepository : GenericRepository<UserRole>, IUserRoleRepository, IDisposable
    {
        private bool disposed = false;

        public UserRoleRepository(DataAccess context)
            : base(context)
        {
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}