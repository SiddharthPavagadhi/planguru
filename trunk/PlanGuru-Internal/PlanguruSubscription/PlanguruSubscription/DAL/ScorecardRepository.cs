﻿using PlanguruSubscription.Models;
using PlanguruSubscription.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PlanguruSubscription.DAL
{
    public class ScorecardRepository : GenericRepository<Scorecard>, IScorecardRepository, IDisposable
    {
        private bool disposed = false;

        public ScorecardRepository(DataAccess context)
            : base(context)
        {
        }

        public object DeleteScorecardItemsByAnalysis(int analysisId)
        {
            Logger.Log.Info(("Start Query : Get the list of Accounts by AnalysisId: " + analysisId));
            var scorecard = (from d in context.Scorecards.Where(a => a.AnalysisId == analysisId) select d);
            if (!(scorecard == null & scorecard.Count() > 0))
            {
                Logger.Log.Info(("Scorecard items count : " + scorecard.Count()));
                Logger.Log.Info("Start deleting : Scorecard items");
                scorecard.ToList().ForEach(item => context.Scorecards.Remove(item));
                context.SaveChanges();
                Logger.Log.Info(("End deleting : Scorecard items"));
            }
            return scorecard.Count();

        }

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

    }
}