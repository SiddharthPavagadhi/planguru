﻿using PlanguruSubscription.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PlanguruSubscription.DAL
{
    public class EmailRepository : GenericRepository<EmailInfo>, IEmailRepository, IDisposable
    {
        private bool disposed = false;

        public EmailRepository(DataAccess context)
            : base(context)
        {
        }

        public EmailInfo GetEmailInfoById(int id)
        {
            return context.EmailInfos.Find(id);
        }


        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}