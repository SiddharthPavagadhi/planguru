﻿using PlanguruSubscription.Models;
using PlanguruSubscription.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PlanguruSubscription.DAL
{
    public class AccountTypeRepository : GenericRepository<AcctType>, IAccountTypeRepository, IDisposable
    {
        private bool disposed = false;

        public AccountTypeRepository(DataAccess context)
            : base(context)
        {
        }

        public object DeleteAccountTypeByAnalysis(int analysisId)
        {
            Logger.Log.Info(("Start Query : Get the list of Account Type by AnalysisId: " + analysisId));
            var accountTypes = (from d in context.AcctTypes.Where(a => a.AnalysisId == analysisId) select d);
            if ((accountTypes != null) & accountTypes.Count() > 0)
            {
                Logger.Log.Info(("Account Type items count : " + accountTypes.Count()));
                Logger.Log.Info("Start deleting : Account Type items");
                accountTypes.ToList().ForEach(item => context.AcctTypes.Remove(item));
                context.SaveChanges();
                Logger.Log.Info(("End deleting : Account Type items"));
            }
            return accountTypes.Count();

        }

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}