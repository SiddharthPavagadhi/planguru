﻿using PlanguruSubscription.Models;
using PlanguruSubscription.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PlanguruSubscription.DAL
{
    public class CompanyRepository : GenericRepository<Company>,ICompanyRepository, IDisposable
    {
        private bool disposed = false;

        public CompanyRepository(DataAccess context)
            : base(context)
        {
        }



        public Company GetCompanyById(int companyId)
        {
            return context.Companies.Where(c => c.CompanyId == companyId).FirstOrDefault();
        }

        public void DeleteCompany(int companyID)
        {
            Logger.Log.Info(("Start Execution of DeleteCompany"));
            Utility utility = new Utility();
            Logger.Log.Info(("Start Query :Find the company by CompanyId: " + companyID));
            Company company = context.Companies.Find(companyID);

            Logger.Log.Info(("Start Query :To Delete the Analyses For " + companyID));
            utility.AnalysisRepository.DeleteAnalysisByCompany(companyID);
            Logger.Log.Info("Execution Ended for DeleteAnalysisByCompany");

            Logger.Log.Info(("Start Query :To Delete the UserCompany Mapping for " + companyID));
            utility.UserCompanyMappingRepository.DeleteUserCompanyMappingByCompany(companyID);
            utility.UserCompanyMappingRepository.Save();
            Logger.Log.Info("Execution Ended for DeleteUserCompanyMappingByCompany");

            Logger.Log.Info("Execution Ended for DeleteCompany");
            if ((company != null))
            {
                context.Companies.Remove(company);
                context.SaveChanges();
            }
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}