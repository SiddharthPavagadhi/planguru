﻿using PlanguruSubscription.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PlanguruSubscription.DAL;
using System.Data.Entity;


namespace PlanguruSubscription.DAL
{
    public class CustomerRepository : GenericRepository<Customer>, ICustomerRepository, IDisposable
    {
        private bool disposed = false;

        public CustomerRepository(DataAccess context)
            : base(context)
        {
        }


        public IEnumerable<Customer> GetCustomer()
        {
            return context.Customers.ToList();
        }

        public Customer GetCustomerById(int customerId)
        {
            return context.Customers.Find(customerId);
        }

        public bool IsFirmNameUnique(string vanityFirmName, int customerId)
        {
            return (from c in context.Customers.Where(c => c.VanityFirmName == vanityFirmName & c.CustomerId != customerId) select c).Any();
        }


        public void UpdateCustomer(Customer customer)
        {
            context.Entry(customer).State = EntityState.Modified;
        }

        public void Save()
        {
            context.SaveChanges();
        }
        public void InsertCustomer(Customer customer)
        {
            context.Customers.Add(customer);
        }
        public void IncrementAddOn(int customerId, int NumberOfAddOnUser)
        {
            Customer customerData = context.Customers.Find(customerId);
            customerData.NumberOfAddOns += NumberOfAddOnUser;
            customerData.NumberOfAnalyticsAddOn += NumberOfAddOnUser;
            context.Entry(customerData).State = EntityState.Modified;
            Save();
        }



        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}