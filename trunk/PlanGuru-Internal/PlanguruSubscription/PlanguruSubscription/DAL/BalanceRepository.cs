﻿using PlanguruSubscription.Models;
using PlanguruSubscription.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PlanguruSubscription.DAL
{
    public class BalanceRepository : GenericRepository<Balance>, IBalanceRepository, IDisposable
    {
        private bool disposed = false;

        public BalanceRepository(DataAccess context)
            : base(context)
        {
        }

        public object DeleteBalanceByAnalysis(int analysisId)
        {

            Logger.Log.Info(("Start Query : Get the list of Balance Itmes by AnalysisId: " + analysisId));
            var balance = (from d in context.Balances.Where(a => a.AnalysisId == analysisId) select d);
            if ((balance != null) & balance.Count() > 0)
            {
                Logger.Log.Info(("Balance items count : " + balance.Count()));
                Logger.Log.Info("Start deleting : Balance items");
                balance.ToList().ForEach(item => context.Balances.Remove(item));
                context.SaveChanges();
                Logger.Log.Info(("End deleting : Balance items"));
            }
            return balance.Count();

        }

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }



    }
}