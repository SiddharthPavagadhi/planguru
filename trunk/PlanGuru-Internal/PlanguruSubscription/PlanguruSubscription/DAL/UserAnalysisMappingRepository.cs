﻿using PlanguruSubscription.Models;
using PlanguruSubscription.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PlanguruSubscription.DAL
{
    public class UserAnalysisMappingRepository : GenericRepository<UserAnalysisMapping>, IUserAnalysisMappingRepository, IDisposable
    {

        private bool disposed = false;

        public UserAnalysisMappingRepository(DataAccess context)
            : base(context)
        {
        }

        public object DeleteUserAnalysisMappingByAnalysis(int analysisId)
        {
            Logger.Log.Info(("Start Query : Get the list of User Mapping by AnalysisId: " + analysisId));
            var userMapping = (from d in context.UserAnalysisMappings.Where(a => a.AnalysisId == analysisId) select d);
            if (!(userMapping == null & userMapping.Count() > 0))
            {
                Logger.Log.Info(("User Mapping items count : " + userMapping.Count()));
                Logger.Log.Info("Start deleting : User Mapping items");
                userMapping.ToList().ForEach(
                    item => context.UserAnalysisMappings.Remove(item));
                context.SaveChanges();
                Logger.Log.Info(("End deleting : User Mapping items"));
            }
            return userMapping.Count();

        }

        public object DeleteUserAnalysisMappingByUserId(int userId)
        {
            Logger.Log.Info(("Start Query : Get the list of User Mapping by UserId: " + userId));
            var userMapping = (from d in context.UserAnalysisMappings.Where(a => a.UserId == userId) select d);
            if ((userMapping != null))
            {
                Logger.Log.Info(("User Mapping items count : " + userMapping.Count()));
                Logger.Log.Info("Start deleting : User Mapping items");
                userMapping.ToList().ForEach(item => context.UserAnalysisMappings.Remove(item));
                Logger.Log.Info(("End Deleting : User Mapping items"));
            }
            return userMapping.Count();


        }
        public void Save()
        {
            context.SaveChanges();
        }



        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

    }
}