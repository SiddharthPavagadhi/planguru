﻿using PlanguruSubscription.Models;
using PlanguruSubscription.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PlanguruSubscription.DAL
{
    public class AccountRepository : GenericRepository<Account>, IAccountRepository, IDisposable
    {
        private bool disposed = false;

        public AccountRepository(DataAccess context)
            : base(context)
        {
        }

        public object DeleteAccountByAnalysis(int analysisId)
        {
            Logger.Log.Info(("Start Query : Get the list of Accounts by AnalysisId: " + analysisId));
            var account = (from d in context.Accounts.Where(a => a.AnalysisId == analysisId) select d);
            if ((account != null) & account.Count() > 0)
            {
                Logger.Log.Info(("Account items count : " + account.Count()));
                Logger.Log.Info("Start deleting : Account items");
                account.ToList().ForEach(item => context.Accounts.Remove(item));
                context.SaveChanges();
                Logger.Log.Info(("End deleting : Account items"));
            }
            return account.Count();

        }
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}