﻿using PlanguruSubscription.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PlanguruSubscription.DAL
{
    public class SubscriptionPlanRepositoy : GenericRepository<SubscriptionPlan>, ISubscriptionPlanRepository, IDisposable
    {

        private bool disposed = false;

        public SubscriptionPlanRepositoy(DataAccess context)
            : base(context)
        {
        }

        public SubscriptionPlan GetSubscriptionPlan(string plan)
        {
            return (from subscriptionPlan in context.SubscriptionPlans where subscriptionPlan.Plan == plan select subscriptionPlan).SingleOrDefault();
        }

        public SubscriptionPlan GetSubscriptionPlan(int planCode)
        {
            return (from subscriptionPlan in context.SubscriptionPlans where subscriptionPlan.PlanTypeId == planCode select subscriptionPlan).SingleOrDefault();
        }
       





        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}