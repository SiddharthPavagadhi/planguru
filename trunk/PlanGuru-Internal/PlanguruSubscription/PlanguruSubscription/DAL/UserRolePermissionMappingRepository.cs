﻿using PlanguruSubscription.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PlanguruSubscription.DAL
{
    public class UserRolePermissionMappingRepository : GenericRepository<UserRolePermissionMapping>, IUserRolePermissionMappingRepository, IDisposable
    {
        private bool disposed = false;

        public UserRolePermissionMappingRepository(DataAccess context)
            : base(context)
        {
        }

        public IEnumerable<UserRolePermissionMapping> GetUserRolePermissionMapping()
        {
            return context.UserRolePermissionMappings.ToList();
        }


        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}