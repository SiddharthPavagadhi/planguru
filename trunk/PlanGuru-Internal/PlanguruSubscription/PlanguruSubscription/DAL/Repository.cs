﻿using PlanguruSubscription.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PlanguruSubscription.DAL
{

    public interface ICustomerRepository : IDisposable
    {

        IEnumerable<Customer> GetCustomer();
        Customer GetCustomerById(int customerId);
        bool IsFirmNameUnique(string vanityFirmName, int customerId);
        void UpdateCustomer(Customer customer);
        void Save();
        void InsertCustomer(Customer customer);
    }

    public interface ISubscriptionPlanRepository : IDisposable
    {
        SubscriptionPlan GetSubscriptionPlan(string plan);
        SubscriptionPlan GetSubscriptionPlan(int planCode);
    }

    public interface IUserRepository : IDisposable
    {
        IEnumerable<User> GetUsers();
        User ValidateUserAccount(string UserName, string Password);
        User GetUserByID(int userId);
        IQueryable<User> UsernameAvailibility(string UserName);
        IQueryable<User> EmailAddressAvailibility(string EmailAddress);
        IEnumerable<User> GetUsers(int customerId, int userId);
        void UpdateUser(User user);
        void DeleteNonSAUUser(int userId);
        int GetNumberOfAddOn(int customerId);
        void Save();
    }

    public interface IAnalysisRepository : IDisposable
    {
        Analysis GetAnalysisById(int analysisId);
        IEnumerable<Analysis> GetAnalyses();
        int DeleteAnalysisByCompany(int companyId);
    }

    public interface IUserRoleRepository : IDisposable
    {

    }

    public interface IUserRolePermissionRepository : IDisposable
    {

    }

    public interface IUserRolePermissionMappingRepository : IDisposable
    {
        IEnumerable<UserRolePermissionMapping> GetUserRolePermissionMapping();
    }

    public interface ICompanyRepository : IDisposable
    {
        Company GetCompanyById(int companyId);
        void DeleteCompany(int companyID);
    }
    public interface IUserCompanyMappingRepository : IDisposable
    {
        List<int> GetDistinctCompanies(List<int> deactivatedUsers);
        void Save();
    }
    public interface IEmailRepository : IDisposable
    {
        EmailInfo GetEmailInfoById(int id);
    }

    public interface IUserAnalysisMappingRepository : IDisposable
    {
        object DeleteUserAnalysisMappingByAnalysis(int analysisId);
        object DeleteUserAnalysisMappingByUserId(int userId);
        void Save();
    }

    public interface ISaveViewRepository : IDisposable
    {
        object DeleteSavedViewItemsByAnalysis(int analysisId);
    }

    public interface IScorecardRepository : IDisposable
    {
        object DeleteScorecardItemsByAnalysis(int analysisId);
    }
    public interface IDashboardRepository : IDisposable
    {
        object DeleteDashboardItemsByAnalysis(int analysisId);
        object DeleteDashboardItemsByUserId(int userId);
        void Save();
    }
    public interface IBalanceRepository : IDisposable
    {
        object DeleteBalanceByAnalysis(int analysisId);
    }

    public interface IAccountRepository : IDisposable
    {
        object DeleteAccountByAnalysis(int analysisId);
    }

    public interface IAccountTypeRepository : IDisposable
    {
        object DeleteAccountTypeByAnalysis(int analysisId);
    }


}