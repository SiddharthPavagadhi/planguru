﻿using PlanguruSubscription.Models;
using PlanguruSubscription.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
//using Recurly;
using RecurlyNew;


namespace PlanguruSubscription.DAL
{
    public class Utility : IDisposable
    {
        private bool disposed = false;
        private DataAccess context = new DataAccess();
        private string plan = string.Empty;
        private CustomerRepository _customerRepository;
        private SubscriptionPlanRepositoy _subscriptionPlanRepository;
        private UserRepository _userRepository;
        private AnalysisRepository _analysisRepository;
        private UserRoleRepository _userRoleRepository;
        private UserRolePermissionRepository _userRolePermissionRepository;
        private UserRolePermissionMappingRepository _userRolePermissionMappingRepository;
        private CompanyRepository _companiesRepository;
        private UserCompanyMappingRepository _userCompanyMappingRepository;
        private EmailRepository _emailRepository;
        private UserAnalysisMappingRepository _userAnalysisMappingRepository;
        private SaveViewRepository _saveviewRepository;
        private ScorecardRepository _scorecardRepository;
        private DashboardRepository _dashboardRepository;
        private BalanceRepository _balanceRespository;
        private AccountRepository _accountRepository;
        private AccountTypeRepository _accountTypeRepository;
        private static string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

        public CustomerRepository CustomerRepository
        {

            get
            {
                if (this._customerRepository == null)
                {
                    this._customerRepository = new CustomerRepository(context);
                }
                return _customerRepository;

            }
        }

        public SubscriptionPlanRepositoy SubscriptionPlanRepository
        {
            get
            {
                if(this._subscriptionPlanRepository == null)
                {
                    this._subscriptionPlanRepository = new SubscriptionPlanRepositoy(context);
                }
                return _subscriptionPlanRepository;
            }
        }

        public UserRepository UserRepository
        {

            get
            {
                if (this._userRepository == null)
                {
                    this._userRepository = new UserRepository(context);
                }
                return _userRepository;
            }
        }

        public AnalysisRepository AnalysisRepository
        {

            get
            {
                if (this._analysisRepository == null)
                {
                    this._analysisRepository = new AnalysisRepository(context);
                }
                return _analysisRepository;
            }
        }

        public UserRoleRepository UserRoleRepository
        {

            get
            {
                if (this._userRoleRepository == null)
                {
                    this._userRoleRepository = new UserRoleRepository(context);
                }
                return _userRoleRepository;
            }
        }

        public UserRolePermissionRepository UserRolePermissionRepository
        {

            get
            {
                if (this._userRolePermissionRepository == null)
                {
                    this._userRolePermissionRepository = new UserRolePermissionRepository(context);
                }
                return _userRolePermissionRepository;
            }
        }

        public UserRolePermissionMappingRepository UserRolePermissionMappingRepository
        {

            get
            {
                if (this._userRolePermissionMappingRepository == null)
                {
                    this._userRolePermissionMappingRepository = new UserRolePermissionMappingRepository(context);
                }
                return _userRolePermissionMappingRepository;
            }
        }
        public CompanyRepository CompaniesRepository
        {

            get
            {
                if (this._companiesRepository == null)
                {
                    this._companiesRepository = new CompanyRepository(context);
                }
                return _companiesRepository;

            }
        }
        public UserCompanyMappingRepository UserCompanyMappingRepository
        {

            get
            {
                if (this._userCompanyMappingRepository == null)
                {
                    this._userCompanyMappingRepository = new UserCompanyMappingRepository(context);
                }
                return _userCompanyMappingRepository;
            }
        }
        public EmailRepository EmailRepository
        {

            get
            {
                if (this._emailRepository == null)
                {
                    this._emailRepository = new EmailRepository(context);
                }
                return _emailRepository;
            }
        }

        public AccountRepository AccountRepository
        {

            get
            {
                if (this._accountRepository == null)
                {
                    this._accountRepository = new AccountRepository(context);
                }
                return _accountRepository;
            }
        }

        public AccountTypeRepository AccountTypeRepository
        {

            get
            {
                if (this._accountTypeRepository == null)
                {
                    this._accountTypeRepository = new AccountTypeRepository(context);
                }
                return _accountTypeRepository;
            }
        }
        public BalanceRepository BalanceRepository
        {

            get
            {
                if (this._balanceRespository == null)
                {
                    this._balanceRespository = new BalanceRepository(context);
                }
                return _balanceRespository;
            }
        }
        public DashboardRepository DashboardRepository
        {

            get
            {
                if (this._dashboardRepository == null)
                {
                    this._dashboardRepository = new DashboardRepository(context);
                }
                return _dashboardRepository;
            }
        }
        public ScorecardRepository ScorecardRepository
        {

            get
            {
                if (this._scorecardRepository == null)
                {
                    this._scorecardRepository = new ScorecardRepository(context);
                }
                return _scorecardRepository;
            }
        }
        public SaveViewRepository SaveViewRepository
        {
            get
            {
                if (this._saveviewRepository == null)
                {
                    this._saveviewRepository = new SaveViewRepository(context);
                }
                return _saveviewRepository;
            }
        }
        public UserAnalysisMappingRepository UserAnalysisMappingRepository
        {

            get
            {
                if (this._userAnalysisMappingRepository == null)
                {
                    this._userAnalysisMappingRepository = new UserAnalysisMappingRepository(context);
                }
                return _userAnalysisMappingRepository;
            }
        }







        public static string SignWithParameters(params string[] parameters)
        {
            try
            {
                dynamic nonce = string.Format("{0}={1}", "nonce", Guid.NewGuid().ToString());
                dynamic timestamp = string.Format("{0}={1}", "timestamp", GetUnixTimeStamp(DateTime.UtcNow));

                List<string> signatureParameters = new List<string>();
                signatureParameters.Add(nonce);
                signatureParameters.AddRange(parameters);
                signatureParameters.Add(timestamp);

                dynamic protectedString = String.Join("&", signatureParameters);
                Logger.Log.Info(string.Format("SignWithParameters Executed"));
                return GenerateHMAC(protectedString) + "|" + Convert.ToString(protectedString);
            }
            catch (Exception ex)
            {
                Logger.Log.Error(string.Format("Unable to SignWithParameters with Message- {0} " + Environment.NewLine + "Stack Trace: {1} ", ex.Message, ex.StackTrace));
                return string.Empty;
            }
            finally
            {
                Logger.Log.Info(string.Format("SignWithParameters Execution Ended"));
            }
        }

        private static int GetUnixTimeStamp(DateTime timestamp)
        {
            try
            {
                dynamic referenceDate = new DateTime(1970, 1, 1);
                dynamic ts = new TimeSpan(timestamp.Ticks - referenceDate.Ticks);
                Logger.Log.Info(string.Format("GetUnixTimeStamp Executed"));
                return (Convert.ToInt32(ts.TotalSeconds));
            }
            catch (Exception ex)
            {
                Logger.Log.Error(string.Format("Unable to GetUnixTimeStamp, with Message- {0} " + Environment.NewLine + "Stack Trace: {1} ", ex.Message, ex.StackTrace));
                return 0;
            }
            finally
            {
                Logger.Log.Info(string.Format("GetUnixTimeStamp Execution Ended"));
            }

        }

        private static string GenerateHMAC(string stringToHash)
        {
            try
            {
                dynamic privatekey = "ae389bb1e1b94e6a84fb57c940694a17";
                dynamic hasher = new HMACSHA1(UTF8Encoding.UTF8.GetBytes(privatekey));
                dynamic hashBytes = hasher.ComputeHash(UTF8Encoding.UTF8.GetBytes(stringToHash));
                dynamic hexDigest = BitConverter.ToString(hashBytes).Replace("-", "").ToLower();
                Logger.Log.Info(string.Format("GenerateHMAC Executed"));
                return hexDigest;
            }
            catch (Exception ex)
            {
                Logger.Log.Error(string.Format("Unable to GenerateHMAC with Message- {0} " + Environment.NewLine + "Stack Trace: {1} ", ex.Message, ex.StackTrace));
                return string.Empty;
            }
            finally
            {
                Logger.Log.Info(string.Format("GenerateHMAC Execution Ended"));
            }

        }

        public static int CheckAccountCodeOnRecurly(int newUserAccountCode)
        {
            if (RecurlyNew.Account.IsExist(newUserAccountCode.ToString()))
            {
                newUserAccountCode = (newUserAccountCode + 1);
                newUserAccountCode = CheckAccountCodeOnRecurly(newUserAccountCode);
            }

            return newUserAccountCode;
        }

        public static string UpdateLogoImageFile(int customerId)
        {

            string logoImageFile = null;
            dynamic utility = new Utility();

            logoImageFile = utility.CustomerRepository.GetCustomerById(customerId).LogoImageFile;
            if ((string.IsNullOrEmpty(logoImageFile)))
            {
                logoImageFile = string.Empty;
            }

            return logoImageFile;
        }

        public static string AutoGenerateUserName()
        {

            Random random = new Random();
            string userName = new string(Enumerable.Repeat(chars, 5).Select(s => s[random.Next(s.Length)]).ToArray()) + new Random().Next(10000,99999).ToString();
            return userName;
        }

       





        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}