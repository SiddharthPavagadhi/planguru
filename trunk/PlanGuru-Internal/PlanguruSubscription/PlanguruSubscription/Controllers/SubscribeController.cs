﻿using PlanguruSubscription.DAL;
using PlanguruSubscription.Models;
using PlanguruSubscription.Modules;
//using Recurly;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RecurlyNew;

namespace PlanguruSubscription.Controllers
{
    public class SubscribeController : Controller
    {
        private Utility utility = new Utility();
        private Common common = new Common();
        private ICustomerRepository customerRepository;
        private IUserRepository userRepository;
        private UserRole UserType;
        private User LoginUserInfo;
        private User userInfo;
        private string CustomerId = null;
        private int pageSize;

        public SubscribeController()
        {
            this.userRepository = new UserRepository(new DataAccess());
            this.customerRepository = new CustomerRepository(new DataAccess());
        }

        public SubscribeController(IUserRepository userRepository)
        {
            this.userRepository = userRepository;
        }
        public SubscribeController(ICustomerRepository customerRepository)
        {
            this.customerRepository = customerRepository;
        }
        //
        // GET: /Subscribe/

        public ActionResult Index()
        {
            return View();
        }



        #region CancelSubscription Part

        [HttpGet()]
        [CustomActionFilter()]
        public bool CancelSubscription(string AccountCode)
        {
            Logger.Log.Info(string.Format("Subscription cancellation started {0}", AccountCode));
            IEnumerable<User> deactivateUsers = null;

            try
            {
                if (((Session["UserType"] != null)) & ((Session["UserInfo"] != null)))
                {
                    UserType = (UserRole)Session["UserType"];
                    LoginUserInfo = (User)Session["UserInfo"];
                    userInfo = utility.UserRepository.GetUsers().Where(u => u.UserId == LoginUserInfo.UserId).SingleOrDefault();
                    AccountCode = UserType.UserRoleId == (int)UserRoles.SAU ? LoginUserInfo.CustomerId.ToString() : AccountCode;
                }

                RecurlyNew.Account acc = RecurlyNew.Accounts.Get(AccountCode);
                var customer = utility.CustomerRepository.GetCustomerById(Convert.ToInt32(AccountCode));
                var plan = utility.SubscriptionPlanRepository.GetSubscriptionPlan(customer.PlanTypeId);
                var RecurlySubscriptionPlan = RecurlyNew.Plans.Get(plan.Plan);

                if ((RecurlyNew.Account.IsExist(AccountCode)))
                {
                    RecurlyNew.Accounts.Close(AccountCode);
                    Logger.Log.Info(string.Format("Account deactivated successfully in PlanGuru of CustomerId- {0}", AccountCode));
                    if ((acc.BillingInfo != null))
                    {
                        var subscription = new RecurlyNew.Subscription(acc, RecurlySubscriptionPlan, "USD");
                        subscription.Terminate(RecurlyNew.Subscription.RefundType.None);
                        Logger.Log.Info(string.Format("Account's subscription cancelled successfully in PlanGuru of CustomerId- {0}", AccountCode));
                        SendEmail(customer, (int)EmailType.SubcriptionCancellation);
                    }
                }

                if ((userInfo.UserRoleId == (int)UserRoles.PGAAdmin | userInfo.UserRoleId == (int)UserRoles.PGASupport))
                {
                    deactivateUsers = from user in utility.UserRepository.GetUsers().Where(x => x.CustomerId == Convert.ToInt32(AccountCode)) select user;
                }
                else
                {
                    deactivateUsers = (from user in utility.UserRepository.GetUsers(Convert.ToInt32(AccountCode), userInfo.UserId) select user).ToList();
                }

                List<int> users = (from du in deactivateUsers select du.UserId).ToList();

                dynamic companyIds = utility.UserCompanyMappingRepository.GetDistinctCompanies(users);

                if ((companyIds != null))
                {
                    foreach (int companyId in companyIds)
                    {
                        utility.CompaniesRepository.DeleteCompany(companyId);
                    }
                    foreach (User User in deactivateUsers)
                    {

                        utility.UserRepository.Delete(User);
                        Logger.Log.Info(string.Format("Account of user:{0} has been deleted, UserId : {1} , UserName : {2} , CustomerId : {3}", User.FirstName, User.UserId, User.UserName, User.CustomerId));
                    }
                }
                utility.UserRepository.Save();

                User subscriberInfo = utility.UserRepository.GetUsers().Where(u => u.CustomerId == Convert.ToInt32(AccountCode) & u.UserRoleId == (int)UserRoles.SAU).FirstOrDefault();

                if ((subscriberInfo != null))
                {
                    utility.UserRepository.Delete(subscriberInfo);
                    utility.UserRepository.Save();
                    Logger.Log.Info(string.Format("Account of user:{0} has been deactivated, UserId : {1} , UserName : {2} , CustomerId : {3}", userInfo.FirstName, userInfo.UserId, userInfo.UserName, userInfo.CustomerId));
                }





                utility.CustomerRepository.Delete(customer);
                utility.CustomerRepository.Save();

                TempData["Message"] = "Subscription cancelled successfully.";
                Logger.Log.Info(string.Format("Subscription deleted successfully in PLANGURU database of ACCOUNT-CODE  {0}", AccountCode));


                if ((userInfo.UserRoleId == (int)UserRoles.SAU | userInfo.UserRoleId == (int)UserRoles.SSU))
                {
                    Response.Cookies["ASP.NET_SessionId"].Value = string.Empty;
                    Response.Cookies["ASP.NET_SessionId"].Expires = DateTime.Now.AddMonths(-20);

                    HttpContext.Session.Clear();
                    HttpContext.Session.Abandon();
                    return true;

                }
                else if ((userInfo.UserRoleId < (int)UserRoles.SAU))
                {
                    return true;
                }

                return true;
            }
            catch (DataException dataEx)
            {
                TempData["ErrorMessage"] = string.Concat("Unable to cancel Subscription - ", dataEx.Message);
                Logger.Log.Error(string.Format("Unable to cancel Subscription, AccountCode- {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2}", AccountCode, dataEx.Message, dataEx.StackTrace));
                return false;
            }
            catch (Exception ex)
            {
                TempData["ErrorMessage"] = string.Concat("Unable to cancel Subscription-", ex.Message);
                Logger.Log.Error(string.Format("Unable to cancel Subscription, AccountCode- {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2}", AccountCode, ex.Message, ex.StackTrace));
                return false;
            }
            finally
            {
                Logger.Log.Info(string.Format("Execution Ended"));
            }
        }


        //[CustomActionFilter()]
        //public ActionResult CancelSubscription(string AccountCode)
        //{
        //    Logger.Log.Info(string.Format("Subscription cancellation started {0}", AccountCode));
        //    IEnumerable<User> deactivateUsers = null;

        //    try
        //    {
        //        if (((Session["UserType"] != null)) & ((Session["UserInfo"] != null)))
        //        {
        //            UserType = (UserRole)Session["UserType"];
        //            LoginUserInfo = (User)Session["UserInfo"];
        //            userInfo = utility.UserRepository.GetUsers().Where(u => u.UserId == LoginUserInfo.UserId).SingleOrDefault();
        //            AccountCode = UserType.UserRoleId == (int)UserRoles.SAU ? LoginUserInfo.CustomerId.ToString() : AccountCode;
        //        }


        //        RecurlyAccount acc = RecurlyAccount.Get(AccountCode);
        //        var customer = utility.CustomerRepository.GetCustomerById(Convert.ToInt32(AccountCode));

        //        if ((RecurlyAccount.IsExist(AccountCode)))
        //        {
        //            RecurlyAccount.CloseAccount(AccountCode);
        //            Logger.Log.Info(string.Format("Account deactivated successfully in PlanGuru of CustomerId- {0}", AccountCode));
        //            if ((acc.BillingInfo != null))
        //            {
        //                RecurlySubscription.TerminateSubscription(AccountCode);
        //                Logger.Log.Info(string.Format("Account's subscription cancelled successfully in PlanGuru of CustomerId- {0}", AccountCode));
        //                SendEmail(customer, (int)EmailType.SubcriptionCancellation);
        //            }
        //        }

        //        if ((userInfo.UserRoleId == (int)UserRoles.PGAAdmin | userInfo.UserRoleId == (int)UserRoles.PGASupport))
        //        {
        //            deactivateUsers = from user in utility.UserRepository.GetUsers().Where(x => x.CustomerId == Convert.ToInt32(AccountCode)) select user;
        //        }
        //        else
        //        {
        //            deactivateUsers = (from user in utility.UserRepository.GetUsers(Convert.ToInt32(AccountCode), userInfo.UserId) select user).ToList();
        //        }

        //        List<int> users = (from du in deactivateUsers select du.UserId).ToList();

        //        dynamic companyIds = utility.UserCompanyMappingRepository.GetDistinctCompanies(users);

        //        if ((companyIds != null))
        //        {
        //            foreach (int companyId in companyIds)
        //            {
        //                utility.CompaniesRepository.DeleteCompany(companyId);
        //            }
        //            foreach (User User in deactivateUsers)
        //            {

        //                utility.UserRepository.Delete(User);
        //                Logger.Log.Info(string.Format("Account of user:{0} has been deleted, UserId : {1} , UserName : {2} , CustomerId : {3}", User.FirstName, User.UserId, User.UserName, User.CustomerId));
        //            }
        //        }
        //        utility.UserRepository.Save();

        //        User subscriberInfo = utility.UserRepository.GetUsers().Where(u => u.CustomerId == Convert.ToInt32(AccountCode) & u.UserRoleId == (int)UserRoles.SAU).FirstOrDefault();

        //        if ((subscriberInfo != null))
        //        {
        //            utility.UserRepository.Delete(subscriberInfo);
        //            utility.UserRepository.Save();
        //            Logger.Log.Info(string.Format("Account of user:{0} has been deactivated, UserId : {1} , UserName : {2} , CustomerId : {3}", userInfo.FirstName, userInfo.UserId, userInfo.UserName, userInfo.CustomerId));
        //        }





        //        utility.CustomerRepository.Delete(customer);
        //        utility.CustomerRepository.Save();

        //        TempData["Message"] = "Subscription cancelled successfully.";
        //        Logger.Log.Info(string.Format("Subscription deleted successfully in PLANGURU database of ACCOUNT-CODE  {0}", AccountCode));


        //        if ((userInfo.UserRoleId == (int)UserRoles.SAU | userInfo.UserRoleId == (int)UserRoles.SSU))
        //        {
        //            Response.Cookies["ASP.NET_SessionId"].Value = string.Empty;
        //            Response.Cookies["ASP.NET_SessionId"].Expires = DateTime.Now.AddMonths(-20);

        //            HttpContext.Session.Clear();
        //            HttpContext.Session.Abandon();
        //            return RedirectToAction("Index", "Home");

        //        }
        //        else if ((userInfo.UserRoleId < (int)UserRoles.SAU))
        //        {
        //            return RedirectToAction("Index");
        //        }

        //        return RedirectToAction("Index");
        //    }
        //    catch (DataException dataEx)
        //    {
        //        TempData["ErrorMessage"] = string.Concat("Unable to cancel Subscription - ", dataEx.Message);
        //        Logger.Log.Error(string.Format("Unable to cancel Subscription, AccountCode- {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2}", AccountCode, dataEx.Message, dataEx.StackTrace));
        //        return RedirectToAction("Index", new System.Web.Routing.RouteValueDictionary {{"AccountCode",AccountCode},{"CancelChangesError",true}});
        //    }
        //    catch (Exception ex)
        //    {
        //        TempData["ErrorMessage"] = string.Concat("Unable to cancel Subscription-", ex.Message);
        //        Logger.Log.Error(string.Format("Unable to cancel Subscription, AccountCode- {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2}", AccountCode, ex.Message, ex.StackTrace));
        //        return RedirectToAction("Index");
        //    }
        //    finally
        //    {
        //        Logger.Log.Info(string.Format("Execution Ended"));
        //    }
        //}

        private void SendEmail(Customer customer, int emailId)
        {
            try
            {
                dynamic objEmailInfo = utility.EmailRepository.GetEmailInfoById(emailId);
                objEmailInfo.EmailBody = Common.MergeCustomerEmailBody(customer, objEmailInfo);
                Common.SendCustomerEmail(customer, objEmailInfo);
                Logger.Log.Info(string.Format("SendEmail Executed"));
            }
            catch (Exception ex)
            {
                TempData["ErrorMessage"] = string.Concat("Unable to Send Email -", ex.Message);
                Logger.Log.Error(string.Format("Unable to Send Email to CustomerID - {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2} ", customer.CustomerId, ex.Message, ex.StackTrace));
            }
            finally
            {
                Logger.Log.Info(string.Format("SendEmail Execution Ended"));
            }
        }
        #endregion
        #region ChangeSubscriptionDetailInfo
        [CustomActionFilter()]
        public RedirectResult EditSubscription(string AccountCode)
        {
            try
            {
                string subscription = string.Format("{0}={1}", "subscription%5Bplan_code%5D", "planguru-analytics");
                ViewBag.Signature = PlanguruSubscription.DAL.Utility.SignWithParameters(subscription);

                if (((Session["UserType"] != null)) & ((Session["UserInfo"] != null)))
                {
                    UserType = (UserRole)Session["UserType"];
                    LoginUserInfo = (User)Session["UserInfo"];

                    AccountCode = LoginUserInfo.UserRoleId == (int)UserRoles.SAU | LoginUserInfo.UserRoleId == (int)UserRoles.SSU ? LoginUserInfo.CustomerId.ToString() : AccountCode;

                }
                Logger.Log.Info(string.Format("AccountCode:- {0}", AccountCode));
                RecurlyNew.Account accountInfo = RecurlyNew.Accounts.Get(AccountCode);
                return Redirect("https://new-horizon-software-technologies-inc.recurly.com/account/billing_info/edit?ht=" + accountInfo.HostedLoginToken);
                //commented because latest Recurly API
                //dynamic customerDetails = customerRepository.GetCustomerById(Convert.ToInt32(AccountCode));

                //Session["AccountCodeToEditSubscription"] = AccountCode;

                //return View(customerDetails);

            }
            catch (DataException dataEx)
            {
                TempData["ErrorMessage"] = string.Concat("Unable to Edit Subscription-", dataEx.Message);
                Logger.Log.Error(string.Format("\\n Unable to Edit Subscription, AccountCode - {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2} ", AccountCode, dataEx.Message, dataEx.StackTrace));

            }
            catch (Exception ex)
            {
                TempData["ErrorMessage"] = string.Concat("Unable to Edit Subscription-", ex.Message);
                Logger.Log.Error(string.Format("\\n Unable to Edit Subscription, AccountCode - {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2} ", AccountCode, ex.Message, ex.StackTrace));

            }
            finally
            {
                Logger.Log.Info(string.Format("Execution Ended"));
            }

            return null;

        }

        [CustomActionFilter()]
        public ActionResult UpdateSubscription()
        {
            string AccountCodeToEditSubscription = string.Empty;

            try
            {
                if (((Session["UserType"] != null)) & ((Session["UserInfo"] != null)))
                {
                    UserType = (UserRole)Session["UserType"];
                    LoginUserInfo = (User)Session["UserInfo"];
                    AccountCodeToEditSubscription = Session["AccountCodeToEditSubscription"].ToString();
                }

                RecurlyNew.Account acc = RecurlyNew.Accounts.Get(AccountCodeToEditSubscription);
                var customer = customerRepository.GetCustomerById(Convert.ToInt32(AccountCodeToEditSubscription));
                var plan = utility.SubscriptionPlanRepository.GetSubscriptionPlan(customer.PlanTypeId);
                var RecurlySubscriptionPlan = RecurlyNew.Plans.Get(plan.Plan);
                if ((RecurlyNew.Account.IsExist(AccountCodeToEditSubscription)))
                {
                    var subscription = new RecurlyNew.Subscription(acc, RecurlySubscriptionPlan, "USD");
                    if ((subscription != null))
                    {
                        RecurlyNew.BillingInfo billingInfo = RecurlyNew.BillingInfo.Get(AccountCodeToEditSubscription.ToString());

                        //Credit Card Information
                        customer.FirstNameOnCard = string.Empty;
                        //billingInfo.FirstName
                        customer.LastNameOnCard = string.Empty;
                        //billingInfo.LastName
                        customer.CreditCardNumber = string.Empty;
                        //String.Concat("XXXX-XXXX-XXXX-", billingInfo.CreditCard.LastFour)
                        customer.CVV = string.Empty;
                        //billingInfo.VerificationValue
                        customer.ExpirationMonth = 0;
                        //billingInfo.CreditCard.ExpirationMonth
                        customer.ExpirationYear = 0;
                        //billingInfo.CreditCard.ExpirationYear

                        customer.CustomerAddress1 = string.Empty;
                        //billingInfo.Address1
                        customer.CustomerAddress2 = string.Empty;
                        //billingInfo.Address2
                        customer.CustomerPostalCode = string.Empty;
                        //billingInfo.PostalCode
                        customer.ContactTelephone = billingInfo.PhoneNumber;
                        customer.Country = billingInfo.Country;
                        customer.State = string.Empty;
                        //billingInfo.State
                        customer.City = string.Empty;
                        //billingInfo.City

                        customer.CreatedBy = customer.CreatedBy;
                        customer.UpdatedBy = LoginUserInfo.UserId.ToString();
                        utility.CustomerRepository.UpdateCustomer(customer);
                        utility.CustomerRepository.Save();


                        ViewBag.SubscriptionInfo = customer;
                        ViewBag.CreditCardLastFourDigit = billingInfo.LastFour.ToString();
                        ViewBag.CreditCardType = billingInfo.CardType.ToString();
                    }
                    else
                    {
                        //To do show error message
                        // Transaction unsuccessfull
                    }
                }
                Logger.Log.Info(string.Format("Subscribe Update Subscription Function Executed"));
                return View(customer);
            }
            catch (Exception ex)
            {
                TempData["ErrorMessage"] = string.Concat("Unable to Update Subscription -", ex.Message);
                Logger.Log.Error(string.Format("Unable to Update Subscription, with Message- {0} " + Environment.NewLine + "Stack Trace: {1} ", ex.Message, ex.StackTrace));
                return View();
            }
            finally
            {
                Logger.Log.Info(string.Format("Subscribe Update Subscription Function Execution Ended"));
            }
        }


        #endregion


    }
}
