﻿using PlanguruSubscription.DAL;
using PlanguruSubscription.Models;
using PlanguruSubscription.Modules;
//using Recurly;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;
using System.Data.Entity;
using System.Data.Entity.Validation;
using PagedList;
using PlanguruSubscription.DAL;
using System.Data;
using System.Xml;
using LicenceService;
using RecurlyNew;

namespace PlanguruSubscription.Controllers
{
    public class UserController : Controller
    {
        private Utility utility = new Utility();
        private Common common = new Common();
        private UserRole UserType;
        private User UserInfo;
        private UserAccess ua;
        private Customer customerInfo;
        private int? CustomerId;
        private IUserRepository userRepository;
        private int pageSize;

        public UserController()
        {
            this.userRepository = new UserRepository(new DataAccess());
        }

        public UserController(IUserRepository userRepository)
        {
            this.userRepository = userRepository;
        }

        public int Page_Size
        {

            get
            {
                if ((!string.IsNullOrEmpty(System.Configuration.ConfigurationManager.AppSettings["PageSize"])))
                {
                    pageSize = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["PageSize"]);
                }
                else
                {
                    pageSize = 10;
                }
                return this.pageSize;
            }
        }



        //
        // GET: /User/

        [CustomActionFilter]
        public ViewResult Index()
        {


            try
            {

                if (((Session["UserType"] != null)) & ((Session["UserInfo"] != null)) & ((Session["UserAccess"] != null)))
                {
                    UserType = (UserRole)Session["UserType"];
                    UserInfo = (User)Session["UserInfo"];
                    ua = (UserAccess)Session["UserAccess"];
                    CustomerId = UserType.UserRoleId == (int)UserRoles.SAU | UserType.UserRoleId == (int)UserRoles.SSU ? UserInfo.CustomerId : (int?)null;

                }
                //RecurlyBillingInfo billingInfo = RecurlyBillingInfo.Get(UserInfo.Customer.CustomerId.ToString());
                RecurlyNew.BillingInfo billingInfo = RecurlyNew.BillingInfo.Get(UserInfo.Customer.CustomerId.ToString());
                UserInfo.Customer.CustomerAddress1 = (billingInfo.Address2 != null && billingInfo.Address2 != "" ? (billingInfo.Address1 + "," + billingInfo.Address2 + ",") : billingInfo.Address1);
                UserInfo.Customer.State = billingInfo.State != null && billingInfo.State != "" ? ("," + billingInfo.State) + " " : "";
                UserInfo.Customer.City = billingInfo.City;
                UserInfo.Customer.CustomerPostalCode = billingInfo.PostalCode;
                UserInfo.Customer.Country = billingInfo.Country;


                return View(UserInfo);

            }
            catch (Exception ex)
            {
                TempData["ErrorMessage"] = string.Concat("Unable to show Landing Page - ", ex.Message);
                Logger.Log.Error(string.Format("Unable to show Landing Page -  Message- {0} " + Environment.NewLine + "Stack Trace: {1} ", ex.Message, ex.StackTrace));
                return View();
            }
            finally
            {
                Logger.Log.Info(string.Format("User Landing Page Loading Execution Ended"));
            }
        }

        [CustomActionFilter]
        public ViewResult UserList(System.Nullable<int> page)
        {


            try
            {
                IEnumerable<User> users = null;
                if (((Session["UserType"] != null)) & ((Session["UserInfo"] != null)) & ((Session["UserAccess"] != null)))
                {
                    UserType = (UserRole)Session["UserType"];
                    UserInfo = (User)Session["UserInfo"];
                    ua = (UserAccess)Session["UserAccess"];
                    CustomerId = UserType.UserRoleId == (int)UserRoles.SAU | UserType.UserRoleId == (int)UserRoles.SSU ? UserInfo.CustomerId : (int?)null;

                }

                if ((UserType.UserRoleId == (int)UserRoles.PGAAdmin | UserType.UserRoleId == (int)UserRoles.PGASupport))
                {
                    users = userRepository.GetUsers().Where(u => (u.Status == (int)StatusE.Active | u.Status == (int)StatusE.Pending) & (u.UserRoleId != (int)UserRoles.PGAAdmin & u.UserRoleId != (int)UserRoles.PGASupport & u.UserRoleId != (int)UserRoles.SAU)).ToList();

                }
                else
                {
                    users = (from usr in userRepository.GetUsers().Where(u => (u.Status == (int)StatusE.Active | u.Status == (int)StatusE.Pending) & (u.UserRoleId != (int)UserRoles.SAU) & u.CustomerId == CustomerId) select usr).ToList();


                }

                Logger.Log.Info(string.Format("User Loaded Successfully"));

                int pageNumber = (page ?? 1);
                if ((users == null))
                {
                    return View(users);
                }
                else
                {
                    return View(users.ToPagedList(pageNumber, Page_Size));
                }
            }
            catch (Exception ex)
            {
                TempData["ErrorMessage"] = string.Concat("Unable to show User List - ", ex.Message);
                Logger.Log.Error(string.Format("Unable to show User List -  Message- {0} " + Environment.NewLine + "Stack Trace: {1} ", ex.Message, ex.StackTrace));
                return View();
            }
            finally
            {
                Logger.Log.Info(string.Format("User Index Function Execution Ended"));
            }
        }


        [CustomActionFilter()]
        public ActionResult CreateUser()
        {
            Hashtable data = new Hashtable();
            User userDetail = new User();
            userDetail.Customer = new Customer();
            if (((Session["UserType"] != null)) & ((Session["UserInfo"] != null)) & ((Session["UserAccess"] != null)))
            {
                UserType = (UserRole)Session["UserType"];
                UserInfo = (User)Session["UserInfo"];
                ua = (UserAccess)Session["UserAccess"];
                CustomerId = UserType.UserRoleId == (int)UserRoles.SAU | UserType.UserRoleId == (int)UserRoles.SSU ? UserInfo.CustomerId : (int?)null;
            }

            if ((UserInfo.UserRoleId == (int)UserRoles.PGAAdmin | UserInfo.UserRoleId == (int)UserRoles.PGASupport))
            {
                PopulateCustomerList();

            }
            else if ((CustomerId > 0))
            {
                data = GetSubscriptionCost(CustomerId.ToString(), UserInfo, data);
                userDetail.CustomerId = UserInfo.CustomerId;
                userDetail.Customer.CustomerCompanyName = UserInfo.Customer.CustomerCompanyName;
                ViewBag.AddOnAmountInCents = data["AddOnAmountInCents"];
                ViewBag.SubsciptionUnitAmountInCents = data["Subsciption_UnitAmountInCents"];
            }

            PopulateUserRoleList();
            return View(userDetail);

        }

        [HttpPost()]
        [CustomActionFilter()]
        public ActionResult CreateUser(User User)
        {
            int newUserAccountCode = 0;
            Customer customerInfo = default(Customer);
            Hashtable data = new Hashtable();
            SubscriptionPlan SubscribedPlan = default(SubscriptionPlan);
            int userCount = 0;

            try
            {
                if (((Session["UserType"] != null)) & ((Session["UserInfo"] != null)) & ((Session["UserAccess"] != null)))
                {
                    UserType = (UserRole)Session["UserType"];
                    UserInfo = (User)Session["UserInfo"];
                    ua = (UserAccess)Session["UserAccess"];
                    CustomerId = UserType.UserRoleId == (int)UserRoles.SAU ? UserInfo.CustomerId : (int?)null;
                }


                if (ModelState.IsValid)
                {
                    var usernameAvailibility = utility.UserRepository.UsernameAvailibility(User.UserName);
                    var IsEmailAddressExists = utility.UserRepository.EmailAddressAvailibility(User.UserEmail);

                    newUserAccountCode = PlanguruSubscription.DAL.Utility.CheckAccountCodeOnRecurly(utility.UserRepository.GetUsers().Max(c => c.UserId) + 1);
                    RecurlyNew.Account acc = RecurlyNew.Accounts.Get(User.CustomerId.ToString());
                    RecurlyNew.Subscription subscriptionNew = acc.GetSubscriptions(RecurlyNew.Subscription.SubscriptionState.Active).FirstOrDefault();

                    // dynamic subscription = RecurlySubscription.Get(User.CustomerId.ToString());

                    SubscribedPlan = utility.SubscriptionPlanRepository.GetSubscriptionPlan(subscriptionNew.PlanCode);

                    userCount = utility.UserRepository.GetUsers(CustomerId.Value, 0).Count();

                    if ((usernameAvailibility.Count() > 0))
                    {
                        ModelState.AddModelError("Username", "Someone already has that username. Try another.");
                    }
                    else if ((IsEmailAddressExists.Count() > 0))
                    {
                        ModelState.AddModelError("UserEmail", "Someone already has that EmailAddress. Try another.");

                    }
                    else if ((subscriptionNew != null))
                    {

                        if ((userCount >= SubscribedPlan.DefaultAddOnUsers))
                        {
                            XMLlisenceProvider lisenceProvider = new XMLlisenceProvider();
                            RecurlyNew.Account newRecurlyAccount = RecurlyNew.Accounts.Get(newUserAccountCode.ToString());
                            // dynamic newRecurlyAccount = new RecurlyAccount(newUserAccountCode.ToString());
                            RecurlyNew.AddOn AddonUpdate = (RecurlyNew.AddOn)Session["NewUserAddonUpdate"];
                            newRecurlyAccount.Username = User.UserName;
                            newRecurlyAccount.FirstName = User.FirstName;
                            newRecurlyAccount.LastName = User.LastName;
                            newRecurlyAccount.Email = User.UserEmail;
                            newRecurlyAccount.Create();

                            if ((RecurlyNew.Account.IsExist(newUserAccountCode.ToString())))
                            {
                                if (subscriptionNew.AddOns.Count == 0)
                                {
                                    subscriptionNew.AddOns.Add(AddonUpdate);
                                }
                                else
                                {
                                    subscriptionNew.AddOns.All[0].Quantity += 1;
                                }


                                //subscription.AddOns = new List<RecurlyAddOn>();
                                //if ((subscription.AddOn.Addons.Count > 0 & (Addon != null)))
                                //{
                                //    Addon.quantity += 1;
                                //    subscription.AddOns.Add(Addon);
                                //}

                                subscriptionNew.ChangeSubscription(RecurlyNew.Subscription.ChangeTimeframe.Now);

                                customerInfo = utility.CustomerRepository.GetCustomerById(User.CustomerId);
                                utility.CustomerRepository.UpdateCustomer(customerInfo);
                                utility.CustomerRepository.Save();
                            }
                            XmlNode node = lisenceProvider.UpdateActivationFields(customerInfo.LicenseId.Value, (userCount + 1));

                        }

                        User.UserId = newUserAccountCode;
                        User.CreatedBy = UserInfo.UserId.ToString();
                        User.UpdatedBy = UserInfo.UserId.ToString();
                        User.CreatedOn = DateTime.Now;
                        User.UpdatedOn = DateTime.Now;
                        User.Status = (int)StatusE.Pending;
                        //Generate random password while creating new account
                        //and send mail to the user to reset it.
                        string password = Common.Generate(8, 2, false);
                        User.Password = Common.Encrypt(password);

                        User.SecurityKey = Guid.NewGuid().ToString();

                        utility.UserRepository.Insert(User);
                        utility.UserRepository.Save();
                        TempData["Message"] = "User created successfully.";
                        Logger.Log.Info(string.Format("CreateUser Successfully Executed"));

                        SendEmail(User, (int)EmailType.NewUserAdded, null);

                        return RedirectToAction("UserList");
                    }
                }

                if ((UserInfo.UserRoleId == (int)UserRoles.PGAAdmin | UserInfo.UserRoleId == (int)UserRoles.PGASupport))
                {
                    PopulateCustomerList();
                }
                else if ((CustomerId > 0))
                {
                    data = GetSubscriptionCost(CustomerId.ToString(), UserInfo, data);
                    User.CustomerId = UserInfo.CustomerId;
                    User.Customer = utility.CustomerRepository.GetCustomerById(User.CustomerId);
                    User.Customer.CustomerCompanyName = UserInfo.Customer.CustomerCompanyName;
                    ViewBag.AddOnAmountInCents = data["AddOnAmountInCents"];
                    ViewBag.SubsciptionUnitAmountInCents = data["Subsciption_UnitAmountInCents"];
                }
                Logger.Log.Info(string.Format("CreateUser Successfully Executed"));
            }
            catch (DataException dataEx)
            {
                TempData["ErrorMessage"] = string.Concat("Unable to create user account -", dataEx.Message);
                Logger.Log.Error(string.Format("Unable to create user account, UserId - {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2} ", newUserAccountCode, dataEx.Message, dataEx.StackTrace));
            }
            catch (Exception ex)
            {
                TempData["ErrorMessage"] = string.Concat("Unable to create user account -", ex.Message);
                Logger.Log.Error(string.Format("Unable to create user account, UserId - {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2} ", newUserAccountCode, ex.Message, ex.StackTrace));
            }
            finally
            {
                Logger.Log.Info(string.Format("CreateUser Execution Ended"));
            }
            PopulateUserRoleList(User.UserRoleId);
            return View(User);
        }



        public JsonResult Authenticate(string userName, string password, string returnUrl)
        {
            Hashtable data = new Hashtable();
            try
            {

                if ((AuthenticateUser(userName, password)))
                {
                    Logger.Log.Info(string.Format("Successfully Login"));

                    data.Add("Success", "Authenticated");
                    data.Add("Url", Url.Action("Index", "User").ToString());

                }
                else
                {
                    data.Add("Error", TempData["ErrorMessage"].ToString());
                }
            }
            catch (Exception ex)
            {
                if ((ex.Message.Equals("The remote name could not be resolved: 'api.recurly.com'")))
                {
                    Logger.Log.Error(string.Format("Your internet connection is down, - {0} " + Environment.NewLine + "Stack Trace: {1} ", ex.Message, ex.StackTrace));
                    data.Add("Error", "Please check your internet connection, it's required.");
                }
                else
                {
                    Logger.Log.Error(string.Format("Unable to Login with Message- {0} " + Environment.NewLine + "Stack Trace: {1} ", ex.Message, ex.StackTrace));
                    data.Add("Error", ex.Message.ToString());
                }
            }
            finally
            {
                Logger.Log.Info(string.Format("Login Function Execution Ended"));
            }

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        private bool AuthenticateUser(string userName, string password)
        {

            bool flag = false;

            dynamic result = utility.UserRepository.ValidateUserAccount(userName, PlanguruSubscription.DAL.Common.Encrypt(password));

            if (result != null)
            {
                result.Customer.PlanType = string.Empty;


                if ((result.Status == (int)StatusE.Pending))
                {
                    TempData["ErrorMessage"] = "Your account has not been activated. <br /> Please click on the link, send to your email-id. <br /> Or Contact Administrator.";
                    return flag;
                }
                else if ((result.Status == (int)StatusE.Deactivated))
                {
                    TempData["ErrorMessage"] = "This user account has been closed by the subscription administrator.";
                    return flag;
                }

                Logger.Log.Info(string.Format("Login Function : User status {0}: ", result.Status));

                Analysis analysisDetail = utility.AnalysisRepository.GetAnalysisById(result.AnalysisId);
                if (analysisDetail == null)
                {
                    result.isSharedAnalysis = false;
                }
                else
                {
                    result.isSharedAnalysis = Convert.ToBoolean(analysisDetail.Option1);
                    result.FirstYear = Convert.ToInt32(analysisDetail.FirstYear);
                }

                Logger.Log.Info(string.Format("Login Function : User selected analysis {0}: ", result.isSharedAnalysis));

                dynamic userRole = utility.UserRoleRepository.GetByID(result.UserRoleId);
                string state = Enum.GetName(typeof(StatusE), StatusE.Active);
                //If (result.UserName.Equals("Administrator", StringComparison.OrdinalIgnoreCase) Or result.UserName.Equals("SupportAdmin", StringComparison.OrdinalIgnoreCase)) Then
                if ((result.UserRoleId == 1 | result.UserRoleId == 2))
                {
                    Session["UserInfo"] = result;
                    Session["CustomerInfo"] = result.Customer;
                    Session["UserType"] = userRole;
                    GetUserRolePermission(userRole);
                    Logger.Log.Info(string.Format("Login Function : set administrative user's role permissions"));
                    flag = true;
                    return flag;
                }
                RecurlyNew.Account account = null;
                RecurlyNew.Account customerAccount = null;

                //Dim account As RecurlyAccount = If(userRole.UserRoleId = UserRoles.SAU, RecurlyAccount.Get(result.CustomerId), RecurlyAccount.Get(result.UserId))
                Logger.Log.Info(string.Format("Login Function : User Type : {0} ", userRole.UserRoleId));

                if ((userRole.UserRoleId == (int)UserRoles.SAU | userRole.UserRoleId == (int)UserRoles.SSU))
                {
                    if (RecurlyNew.Account.IsExist(result.CustomerId.ToString()))
                    {
                        account = RecurlyNew.Accounts.Get(result.CustomerId.ToString());
                        customerAccount = account;
                        Logger.Log.Info(string.Format("Login Function : User's CustomerId : {0} ", result.CustomerId));
                    }
                    result.Customer.PlanType = utility.SubscriptionPlanRepository.GetSubscriptionPlan(result.Customer.PlanTypeId).PlanType.ToString();
                    Logger.Log.Info(string.Format("Login Function : User's PlanType : {0} ", result.Customer.PlanType));

                }
                else
                {
                    if (RecurlyNew.Account.IsExist(result.CustomerId.ToString()))
                    {
                        account = RecurlyNew.Accounts.Get(result.UserId.ToString());
                        customerAccount = RecurlyNew.Accounts.Get(result.CustomerId.ToString());
                        Logger.Log.Info(string.Format("Login Function : User Id : {0} ", result.UserId));
                    }
                }

                if (account.HasActiveSubscription == false)
                {
                    TempData["ErrorMessage"] = "Access denied.  Your PlanGuru Analytics subscription has expired because the credit card information associated with the subscription is no longer valid.  Please contact support at 888.822.6300.";
                    return flag;
                }
                if (account.HasCanceledSubscription == true)
                {
                    TempData["ErrorMessage"] = "Access denied. Your PlanGuru Analytics subscription was cancelled by the subscription Administrator.  Please contact support at 888.822.6300.";
                    return flag;
                }

                if ((result.Customer.PlanTypeId >= (int)PlanType.BP) & (customerAccount.HasActiveSubscription == true) & (customerAccount.HasCanceledSubscription == false))
                {
                    GetUserDetails(result);
                    Logger.Log.Info(string.Format("Login Function : Get User's Deatils."));

                    if ((result.UserRoleId == 4 | result.UserRoleId == 5))
                    {
                        result.Customer.LogoImageFile = PlanguruSubscription.DAL.Utility.UpdateLogoImageFile(result.CustomerId);
                    }

                    Session["UserInfo"] = result;
                    Session["UserType"] = userRole;

                    GetUserRolePermission(userRole, true);
                    Logger.Log.Info(string.Format("Login Function : Get User's role permission."));

                    flag = true;


                }
                else if ((result.Customer.PlanTypeId == (int)PlanType.PPU) & (customerAccount.HasActiveSubscription == true) & (customerAccount.HasCanceledSubscription == false))
                {

                    if ((userRole.UserRoleId == (int)UserRoles.SAU | userRole.UserRoleId == (int)UserRoles.SSU))
                    {
                        GetUserDetails(result);
                        Logger.Log.Info(string.Format("Login Function : Get User's Deatils."));
                    }

                    Session["UserInfo"] = result;
                    Session["UserType"] = userRole;

                    GetUserRolePermission(userRole);
                    Logger.Log.Info(string.Format("Login Function : Get User's role permission."));
                    flag = true;

                }
                else
                {
                    TempData["ErrorMessage"] = "This account has been " + account.State + " by the Subscriber or Administrator.";
                }
            }
            else
            {
                TempData["ErrorMessage"] = "The Username or Password you entered is incorrect.";
            }

            return flag;
        }

        private void GetUserRolePermission(UserRole userRole, bool BP = false)
        {
            var userRolePermission = (from urp in utility.UserRolePermissionRepository.GetUserRolePermission()
                                      join urpm in utility.UserRolePermissionMappingRepository.GetUserRolePermissionMapping()
                                      on urp.UserRolePermissionId equals urpm.UserRolePermissionId
                                      where urpm.UserRoleId == userRole.UserRoleId
                                      orderby urp.UserRolePermissionId
                                      select urp).ToList();

            UserAccess UserAccess = new UserAccess();

            foreach (var itemPermission in userRolePermission)
            {
                switch (itemPermission.Description.ToString())
                {
                    case "Subscription Management":
                        UserAccess.SubscriptionManagement = true;
                        break;
                    case "User Management":
                        UserAccess.UserManagement = true;
                        break;
                    case "Search Customer":
                        UserAccess.SearchCustomer = true;
                        break;
                    case "View Subscription":
                        UserAccess.ViewSubscription = true;
                        break;
                    case "Add Subscription":
                        UserAccess.AddSubscription = true;
                        break;
                    case "Update Subscription":
                        UserAccess.UpdateSubscription = true;
                        break;
                    case "Cancel Subscription":
                        UserAccess.CancelSubscription = true;
                        break;
                    case "Add User":
                        UserAccess.AddUser = true;
                        break;
                    case "Delete User":
                        UserAccess.DeleteUser = true;
                        break;
                    case "View User":
                        UserAccess.ViewUser = true;
                        break;
                    case "Update User":
                        UserAccess.UpdateUser = true;
                        break;
                    case "Add Company":
                        UserAccess.AddCompany = true;
                        break;
                    case "Delete Company":
                        UserAccess.DeleteCompany = true;
                        break;
                    case "View Companies":
                        UserAccess.ViewCompanies = true;
                        break;
                    case "Update Company":
                        UserAccess.UpdateCompany = true;
                        break;
                    case "User Company Mapping":
                        UserAccess.UserCompanyMapping = true;
                        break;
                    case "Add Analysis":
                        UserAccess.AddAnalysis = true;
                        break;
                    case "Delete Analysis":
                        UserAccess.DeleteAnalysis = true;
                        break;
                    case "View Analyses":
                        UserAccess.ViewAnalyses = true;
                        break;
                    case "Update Analysis":
                        UserAccess.UpdateAnalysis = true;
                        break;
                    case "User Analysis Mapping":
                        UserAccess.UserAnalysisMapping = true;
                        break;
                    case "View Analysis Info":
                        UserAccess.ViewAnalysisInfo = true;
                        break;
                    case "Print Analysis Info":
                        UserAccess.PrintAnalysisInfo = true;
                        break;
                    case "Modify Dashboard":
                        UserAccess.ModifyDashboard = true;
                        break;
                    case "Update Saved View":
                        UserAccess.UpdateSavedViews = true;
                        break;
                    case "Delete Saved View":
                        UserAccess.DeleteSavedViews = true;
                        break;
                    case "View Saved View":
                        UserAccess.ViewSavedViews = true;
                        break;
                    case "Branding":
                        UserAccess.Branding = true;
                        break;
                }
            }

            if ((BP == true))
            {
                UserAccess.Branding = true;
            }

            Session["UserAccess"] = UserAccess;

        }

        private void GetUserDetails(User result)
        {
            dynamic customerInfo = utility.CustomerRepository.GetCustomerById(result.CustomerId);
            if (((customerInfo != null)))
            {
                Session["CustomerInfo"] = customerInfo;
            }

            if ((result.AnalysisId != 0))
            {
                result.selectedCompany = utility.AnalysisRepository.GetAnalyses().Where(b => b.AnalysisId == result.AnalysisId).Select(i => i.CompanyId).FirstOrDefault();
                if ((result.selectedCompany != 0))
                {
                    result.fiscalMonthOfSelectedCompany = utility.CompaniesRepository.GetCompanyById(result.selectedCompany).FiscalMonthStart;
                }
                else
                {
                    result.AnalysisId = 0;
                }

            }

        }

        [CustomActionFilter()]
        public ActionResult EditProfile()
        {
            dynamic UserId = string.Empty;
            ResetPassword User = new ResetPassword();
            try
            {

                if ((Session["UserInfo"] != null))
                {
                    UserInfo = (User)Session["UserInfo"];
                    UserInfo = utility.UserRepository.GetUserByID(UserInfo.UserId);

                    UserId = (User.UserId == UserInfo.UserId.ToString());
                    User.UserName = UserInfo.UserName;
                    User.FirstName = UserInfo.FirstName;
                    User.LastName = UserInfo.LastName;
                    User.CurrentPassword = UserInfo.Password;
                    User.UserId = UserInfo.UserId.ToString();
                    User.UserRoleId = UserInfo.UserRoleId;
                    User.UserEmail = UserInfo.UserEmail;

                    if ((UserInfo.UserRoleId == (int)UserRoles.SAU | UserInfo.UserRoleId == (int)UserRoles.SSU))
                    {
                        customerInfo = utility.CustomerRepository.GetCustomerById(UserInfo.CustomerId);
                        User.SubscriberName = customerInfo.CustomerCompanyName;
                    }
                    else
                    {
                        User.SubscriberName = "None";
                    }

                }
                else
                {
                    //To do : Session timeout 
                }

                return View(User);
            }
            catch (DataException dataEx)
            {
                TempData["ErrorMessage"] = string.Concat("Error occured while to view user information to edit - ", dataEx.Message);
                Logger.Log.Error(string.Format("\\n Error occured while to view user information to edit, UserID - {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2} ", UserId, dataEx.Message, dataEx.StackTrace));
            }
            catch (Exception ex)
            {
                TempData["ErrorMessage"] = string.Concat("Error occured while to view user information to edit - ", ex.Message);
                Logger.Log.Error(string.Format("\\n Error occured while to view user information to edit, UserID - {0} with Message- {1}" + Environment.NewLine + "Stack Trace: {2} ", UserId, ex.Message, ex.StackTrace));
            }
            finally
            {
                Logger.Log.Info(string.Format("Edit User Execution Ended"));
            }
            return null;
        }

        [HttpPost()]
        [CustomActionFilter()]
        public ActionResult EditProfile(ResetPassword User)
        {

            Logger.Log.Info(string.Format("\\n User Profile update started {0}", User.UserId));

            try
            {

                if ((ModelState.IsValid))
                {
                    if ((ValidateChangePassword(User) == false))
                    {
                        return View(User);
                    }

                    string errorMessage = string.Empty;
                    var usr = userRepository.GetUserByID(Convert.ToInt32(User.UserId));

                    var usernameAvailibility = utility.UserRepository.UsernameAvailibility(User.UserName).Where(u => u.UserId.ToString() != User.UserId);

                    var emailAddressAvailibility = utility.UserRepository.EmailAddressAvailibility(User.UserEmail).Where(u => u.UserId.ToString() != User.UserId);


                    if ((User.ChangePassword == true) & !(User.CurrentPassword == null & User.NewPassword == null & User.ConfirmNewPassword == null))
                    {
                        if (usr.Password == DAL.Common.Encrypt(User.CurrentPassword))
                        {
                            if ((User.NewPassword.Equals(User.ConfirmNewPassword)))
                            {
                                usr.Password = DAL.Common.Encrypt(User.NewPassword);
                            }
                            else
                            {
                                TempData["ErrorMessage"] = string.Concat("New & Confirm Password does not match.");
                                return View(User);
                            }
                        }
                        else
                        {
                            TempData["ErrorMessage"] = string.Concat("Current password does not match.");
                            return View(User);
                        }
                    }

                    if (usernameAvailibility.Count() > 0)
                    {
                        ModelState.AddModelError("Username", "Someone already has that username. Try another.");
                    }
                    else if (emailAddressAvailibility.Count() > 0)
                    {
                        ModelState.AddModelError("UserEmail", "Someone already has that Email Address. Try another.");

                    }
                    else
                    {

                        if ((User.UserRoleId == (int)UserRoles.SAU))
                        {
                            RecurlyNew.Account accountInfo = RecurlyNew.Accounts.Get(usr.CustomerId.ToString());
                            //RecurlyAccount accountInfo = RecurlyAccount.Get(usr.CustomerId.ToString());
                            //RecurlyBillingInfo billingInfo = RecurlyBillingInfo.Get(usr.CustomerId.ToString());

                            accountInfo.FirstName = User.FirstName;
                            accountInfo.LastName = User.LastName;
                            accountInfo.Email = User.UserEmail;
                            accountInfo.Username = User.UserName;
                            accountInfo.Update();
                            Logger.Log.Info(string.Format("Update user's profile on recurly successfully, CustomerId : {0} , Username : {1} ", usr.CustomerId, User.UserName));

                            //billingInfo.FirstName = User.FirstName;
                            //billingInfo.LastName = User.LastName;
                            //billingInfo.Update();
                            //Logger.Log.Info(string.Format("Update user's billing information on recurly successfully, CustomerId : {0} , Username : {1} ", usr.CustomerId, User.UserName));

                            dynamic subscriberInfo = utility.CustomerRepository.GetCustomerById(usr.CustomerId);
                            subscriberInfo.CustomerFirstName = User.FirstName;
                            subscriberInfo.CustomerLastName = User.LastName;
                            subscriberInfo.CustomerEmail = User.UserEmail;
                            subscriberInfo.CustomerCompanyName = User.SubscriberName;

                            utility.CustomerRepository.UpdateCustomer(subscriberInfo);
                            utility.CustomerRepository.Save();
                            Logger.Log.Info(string.Format("Update customer's information successfully on local database, CustomerId : {0} , Username : {1} ", usr.CustomerId, User.UserName));

                        }

                        usr.UserName = User.UserName;
                        usr.FirstName = User.FirstName;
                        usr.LastName = User.LastName;
                        usr.UserEmail = User.UserEmail;

                        usr.UpdatedBy = User.UserId;
                        userRepository.UpdateUser(usr);
                        userRepository.Save();
                        Logger.Log.Info(string.Format("Update user's information successfully on local database, UserId : {0} , Username : {1} ", User.UserId, User.UserName));

                        Session["UserInfo"] = usr;

                        TempData["Message"] = "Profile updated Successfully.";
                        return RedirectToAction("Index");

                    }
                }

                return View(User);
            }
            catch (DataException dataEx)
            {
                TempData["ErrorMessage"] = string.Concat("Error occured while updating user detail - ", dataEx.Message);
                Logger.Log.Error(string.Format("\\n Error occured while updating user detail of User id - {0} with Message - {1} " + Environment.NewLine + "Stack Trace: {2} ", User.UserId, dataEx.Message, dataEx.StackTrace));
            }
            catch (Exception ex)
            {
                TempData["ErrorMessage"] = string.Concat("Error occured while updating user detail of - ", ex.Message);
                Logger.Log.Error(string.Format("\\n Error occured while updating user detail of User id- {0} with Message - {1} " + Environment.NewLine + "Stack Trace: {2} ", User.UserId, ex.Message, ex.StackTrace));
            }
            finally
            {
                Logger.Log.Info(string.Format("Execution Ended"));
            }

            return View(User);
        }

        private bool ValidateChangePassword(ResetPassword User)
        {

            dynamic Flag = true;
            if ((User.ChangePassword == true) & (User.CurrentPassword == null))
            {
                ModelState.AddModelError("CurrentPassword", "Current Password is required.");
                Flag = false;
            }
            else if ((User.ChangePassword == true) & (User.NewPassword == null))
            {
                ModelState.AddModelError("NewPassword", "New Password is required.");
                Flag = false;
            }
            else if ((User.ChangePassword == true) & (User.ConfirmNewPassword == null))
            {
                ModelState.AddModelError("ConfirmNewPassword", "Confirm Password is required.");
                Flag = false;

            }
            else if ((User.ChangePassword == true) & (!(User.NewPassword == null & User.ConfirmNewPassword == null)))
            {
                if ((!User.NewPassword.Equals(User.ConfirmNewPassword)))
                {
                    Flag = false;
                }

            }

            return Flag;
        }

        public ActionResult Verification(string id, string securitykey)
        {
            User user = default(User);
            UserVerification ResetPassword = new UserVerification();
            try
            {
                ViewBag.Status = 0;
                ViewBag.UserRoleId = 0;

                user = userRepository.GetUserByID(Convert.ToInt32(id));
                string str = securitykey;

                if ((user == null))
                {
                    TempData["ErrorMessage"] = string.Concat("It seems this user does not exist, Contact Administrator.");
                    return View();
                }

                if ((user.SecurityKey == securitykey))
                {
                    ResetPassword.User_Id = user.UserId;
                    ResetPassword.UserRoleId = user.UserRoleId;
                    ResetPassword.Status = user.Status;

                    if (((user.UserRoleId > (int)UserRoles.SAU) | (user.UserRoleId == (int)UserRoles.SAU & user.Status == (int)StatusE.Active)))
                    {
                        ResetPassword.UserName = user.UserName;
                    }

                    ViewBag.Status = user.Status;
                    ViewBag.UserRoleId = user.UserRoleId;

                    return View(ResetPassword);
                }
                else
                {
                    TempData["ErrorMessage"] = string.Concat("SecurityToken does not match, Contact Administrator.");
                    return View();
                }

            }
            catch (Exception ex)
            {
                TempData["ErrorMessage"] = string.Concat("Error occured while Reset Password -", ex.Message);
                Logger.Log.Error(string.Format("Error occured while Reset Password of User Id - {0} with Message - {1} " + Environment.NewLine + "Stack Trace: {1}", id, ex.Message, ex.StackTrace));
                return View();
            }
            finally
            {
                Logger.Log.Info(string.Format("Verification Function Execution Ended"));
            }

        }

        [HttpPost()]
        public ActionResult Verification(UserVerification User)
        {

            User usr = null;
            bool isAccExists = false;
            RecurlyNew.Account usrOnRecurly = null;
            SubscriptionPlan SubscribedPlan = default(SubscriptionPlan);

            try
            {

                if (ModelState.IsValid)
                {
                    RecurlyNew.Account acc = RecurlyNew.Accounts.Get(utility.UserRepository.GetUserByID(User.User_Id).CustomerId.ToString());
                    RecurlyNew.Subscription subscription = acc.GetSubscriptions(RecurlyNew.Subscription.SubscriptionState.Active).FirstOrDefault();
                    //dynamic subscription = RecurlySubscription.Get(utility.UserRepository.GetUserByID(User.User_Id).CustomerId.ToString());

                    SubscribedPlan = utility.SubscriptionPlanRepository.GetSubscriptionPlan(subscription.PlanCode);

                    var usernameAvailibility = utility.UserRepository.UsernameAvailibility(User.UserName).Where(u => u.UserId != User.User_Id);

                    usr = utility.UserRepository.GetUserByID(User.User_Id);

                    if ((usr.UserRoleId == (int)UserRoles.SAU))
                    {
                        isAccExists = RecurlyNew.Account.IsExist(usr.CustomerId.ToString());
                    }
                    else if ((SubscribedPlan.PlanTypeId == (int)PlanType.PPU))
                    {
                        isAccExists = RecurlyNew.Account.IsExist(usr.UserId.ToString());
                    }
                    else
                    {
                        isAccExists = true;
                    }


                    if ((isAccExists == false))
                    {
                        TempData["ErrorMessage"] = string.Concat("This account doesn't exists!");


                    }
                    else
                    {
                        if ((usr.UserRoleId == (int)UserRoles.SAU))
                        {
                            usrOnRecurly = RecurlyNew.Accounts.Get(usr.CustomerId.ToString());
                        }
                        else if ((SubscribedPlan.PlanTypeId == (int)PlanType.PPU))
                        {
                            usrOnRecurly = RecurlyNew.Accounts.Get(usr.UserId.ToString());
                        }
                        else
                        {
                            if ((RecurlyNew.Account.IsExist(usr.UserId.ToString())))
                            {
                                usrOnRecurly = RecurlyNew.Accounts.Get(usr.UserId.ToString());
                            }
                        }

                        //usrOnRecurly = IIf(usr.UserRoleId = UserRoles.SAU, RecurlyAccount.Get(usr.CustomerId.ToString()), RecurlyAccount.Get(usr.UserId.ToString()))

                        if ((usernameAvailibility.Count() > 0))
                        {
                            ModelState.AddModelError("Username", "Someone already has that username. Try another.");

                        }
                        else if ((usr.Password == Common.Encrypt(User.CurrentPassword)))
                        {

                            if ((User.NewPassword.Equals(User.ConfirmNewPassword)))
                            {
                                if ((usrOnRecurly != null))
                                {
                                    usrOnRecurly.Username = User.UserName;
                                    usrOnRecurly.Update();
                                }

                                Logger.Log.Info(string.Format("User Role : {0} ", Enum.GetName(typeof(UserRoles), usr.UserRoleId)));
                                Logger.Log.Info(string.Format("Update username on recurly of UserId : {0} , Username : {1} ", usr.UserId, usr.UserName));

                                usr.UserName = User.UserName;
                                usr.Password = Common.Encrypt(User.NewPassword);
                                usr.Status = Convert.ToInt32(StatusE.Active);
                                utility.UserRepository.UpdateUser(usr);
                                utility.UserRepository.Save();
                                //userRepository.UpdateUser(usr)
                                //userRepository.Save()

                                TempData["Message"] = "Password Reset Successfully.";
                                Logger.Log.Info(string.Format("Password Reset Successfully of User : {0} , UserId : {1}", usr.UserName, usr.UserId));

                                return RedirectToAction("Index", "Home");
                            }
                            else
                            {
                                TempData["ErrorMessage"] = string.Concat("New & Confirm Password does not match.");
                                return View(User);
                            }
                        }
                        else
                        {
                            TempData["ErrorMessage"] = string.Concat("Temporary password does not match.");
                            return View(User);
                        }

                    }
                }

                return View(User);
            }
            catch (Exception ex)
            {
                TempData["ErrorMessage"] = "Error occured while Reset Password.";
                Logger.Log.Error(string.Format("Error occured while Reset Password of User Id - {0} with Message - {1} " + Environment.NewLine + "Stack Trace: {1}", User.User_Id, ex.Message, ex.StackTrace));
                return View();
            }
            finally
            {
                Logger.Log.Info(string.Format("Verification Function Ended"));
            }
        }

        [CustomActionFilter()]
        private void PopulateCustomerList()
        {
            Logger.Log.Info(string.Format("Populate Company List  started "));
            try
            {
                if (((Session["UserType"] != null)) & ((Session["UserInfo"] != null)))
                {
                    UserType = (UserRole)Session["UserType"];
                    UserInfo = (User)Session["UserInfo"];
                    CustomerId = UserType.UserRoleId == (int)UserRoles.SAU | UserType.UserRoleId == (int)UserRoles.SSU ? UserInfo.CustomerId : (int?)null;
                }
                dynamic customers = null;
                if ((UserType.UserRoleId == (int)UserRoles.PGAAdmin | UserType.UserRoleId == (int)UserRoles.PGASupport))
                {
                    customers = utility.CustomerRepository.Get(filter: q => (q.Quantity > 0), orderBy: q => q.OrderBy(d => d.CustomerLastName));
                }
                else
                {
                    customers = utility.CustomerRepository.Get(filter: q => (q.Quantity > 0 & q.CustomerId == CustomerId), orderBy: q => q.OrderBy(d => d.CustomerLastName));
                }

                ViewBag.SelectedCustomer = new SelectList(customers, "CustomerId", "CustomerFullName", null);
                Logger.Log.Info(string.Format("Populate Company List successfully Loaded"));
            }
            catch (DataException dataEx)
            {
                TempData["ErrorMessage"] = string.Concat("Unable to Populate Company List -", dataEx.Message);
                Logger.Log.Error(string.Format("Unable to Populate Company List with Message- {0} " + Environment.NewLine + "Stack Trace: {1} ", dataEx.Message, dataEx.StackTrace));
                throw;
            }
            catch (Exception ex)
            {
                TempData["ErrorMessage"] = string.Concat("Unable to Populate Company List-", ex.Message);
                Logger.Log.Error(string.Format("Unable to populate company list with Message- {0} " + Environment.NewLine + "Stack Trace: {1}", ex.Message, ex.StackTrace));
                throw;
            }
            finally
            {
                Logger.Log.Info(string.Format("Execution Ended"));
            }
        }

        private Hashtable GetSubscriptionCost(string CustomerId, User UserInfo, Hashtable data)
        {


            if ((!string.IsNullOrEmpty(CustomerId)))
            {
                SubscriptionPlan subscriptionPlan = utility.SubscriptionPlanRepository.GetSubscriptionPlan(UserInfo.Customer.PlanTypeId);
                RecurlyNew.Account acc = RecurlyNew.Accounts.Get(CustomerId);
                RecurlyNew.Plan RecurlySubscriptionPlan = RecurlyNew.Plans.Get(subscriptionPlan.Plan);
                RecurlyNew.Subscription subscriptionNew = acc.GetSubscriptions(RecurlyNew.Subscription.SubscriptionState.Active).FirstOrDefault();
                //dynamic subscription = RecurlySubscription.Get(CustomerId);
                int Quantity = 0;
                double AmountInCents = 0;
                double TotalAmountInCents = 0;
                string planCode = null;
                //SubscriptionPlan SubscribedPlan = default(SubscriptionPlan);
                int userCount = 0;

                //SubscribedPlan = utility.SubscriptionPlanRepository.GetSubscriptionPlan(subscription.PlanCode);

                userCount = utility.UserRepository.GetUsers(Convert.ToInt32(CustomerId), 0).Count();


                customerInfo = utility.CustomerRepository.GetCustomerById(Convert.ToInt32(CustomerId));
                //planCode = utility.RecurlyPlan(CustomerId)


                if ((!string.IsNullOrEmpty(subscriptionNew.PlanCode)))
                {
                    // dynamic plan = RecurlyPlan.Get(subscription.PlanCode);


                    if (((int)PlanType.PPU == subscriptionPlan.PlanTypeId))
                    {

                        if ((RecurlySubscriptionPlan.AddOns.Count > 0 & userCount == (subscriptionPlan.DefaultAddOnUsers + 1)))
                        {
                            Quantity = RecurlySubscriptionPlan.AddOns[0].DefaultQuantity;
                            AmountInCents = RecurlySubscriptionPlan.AddOns[0].UnitAmountInCents.FirstOrDefault().Value / 100;
                            TotalAmountInCents = (Quantity * AmountInCents);

                            data.Add("AddOnAmountInCents", AmountInCents.ToString("N2"));
                            Session["NewUserAddonUpdate"] = (RecurlyNew.AddOn)RecurlySubscriptionPlan.AddOns[0];


                        }
                        else if ((RecurlySubscriptionPlan.AddOns.Count > 0 & userCount > subscriptionPlan.DefaultAddOnUsers))
                        {
                            AmountInCents = RecurlySubscriptionPlan.AddOns[0].UnitAmountInCents.FirstOrDefault().Value / 100;
                            Quantity = subscriptionNew.AddOns[0].Quantity + 1;
                            TotalAmountInCents = (Quantity * AmountInCents);

                            data.Add("AddOnAmountInCents", AmountInCents.ToString("N2"));
                            Session["NewUserAddonUpdate"] = (RecurlyNew.AddOn)RecurlySubscriptionPlan.AddOns[0];

                        }


                    }
                    else if (((int)PlanType.BP == subscriptionPlan.PlanTypeId))
                    {

                        if ((RecurlySubscriptionPlan.AddOns.Count > 0 & (subscriptionPlan.DefaultAddOnUsers > userCount) || (UserInfo.Customer.NumberOfAddOns > userCount)))
                        {
                            data.Add("AddOnAmountInCents", AmountInCents.ToString("N2"));


                        }
                        else if ((RecurlySubscriptionPlan.AddOns.Count > 0 & subscriptionPlan.DefaultAddOnUsers <= userCount))
                        {
                            if (((subscriptionNew.AddOns.Count == 0) | (subscriptionPlan.DefaultAddOnUsers == userCount)))
                            {
                                Quantity = RecurlySubscriptionPlan.AddOns[0].DefaultQuantity;
                                AmountInCents = RecurlySubscriptionPlan.AddOns[0].UnitAmountInCents.FirstOrDefault().Value / 100;
                                TotalAmountInCents = (Quantity * AmountInCents);
                                data.Add("AddOnAmountInCents", AmountInCents.ToString("N2"));
                                Session["NewUserAddon"] = (RecurlyNew.AddOn)RecurlySubscriptionPlan.AddOns[0];
                            }
                            else if ((subscriptionNew.AddOns.Count >= 1))
                            {
                                Quantity = (userCount - subscriptionPlan.DefaultAddOnUsers) + 1;
                                AmountInCents = subscriptionNew.AddOns[0].UnitAmountInCents / 100;
                                TotalAmountInCents = (Quantity * AmountInCents);
                                data.Add("AddOnAmountInCents", AmountInCents.ToString("N2"));
                                Session["NewUserAddon"] = (RecurlyNew.AddOn)RecurlySubscriptionPlan.AddOns[0];
                            }
                        }

                    }
                    else if ((int)PlanType.BP < subscriptionPlan.PlanTypeId)
                    {
                        data.Add("AddOnAmountInCents", subscriptionPlan.Cost_of_Additional_Users.ToString("N2"));
                        if ((subscriptionPlan.PlanTypeId == (int)PlanType.AAM || subscriptionPlan.PlanTypeId == (int)PlanType.AAY))
                        {
                            double additionalAmount = 0d;
                            if ((customerInfo.NumberOfAddOns + 2) >= customerInfo.NumberOfAnalyticsAddOn)
                            {
                                additionalAmount = Convert.ToDouble(subscriptionPlan.Cost_of_Subscription + (subscriptionPlan.Cost_of_Additional_Users * (customerInfo.NumberOfAddOns + 1)));
                            }
                            else
                            {
                                additionalAmount = Convert.ToDouble(subscriptionPlan.Cost_of_Subscription + (subscriptionPlan.Cost_of_Additional_Users * (customerInfo.NumberOfAddOns + 1)) + (subscriptionPlan.Cost_of_Additional_Analytics_Users * (customerInfo.NumberOfAnalyticsAddOn - (customerInfo.NumberOfAddOns + 2))));
                            }
                            data.Add("Subsciption_UnitAmountInCents", additionalAmount.ToString());
                        }
                        else
                        {
                            double additionalAmount = 0d;
                            if (customerInfo.NumberOfAddOns >= customerInfo.NumberOfAnalyticsAddOn)
                            {
                                additionalAmount = Convert.ToDouble(subscriptionPlan.Cost_of_Subscription + (subscriptionPlan.Cost_of_Additional_Users * (customerInfo.NumberOfAddOns + 1)));
                            }
                            else
                            {
                                additionalAmount = Convert.ToDouble(subscriptionPlan.Cost_of_Subscription + (subscriptionPlan.Cost_of_Additional_Users * (customerInfo.NumberOfAddOns + 1)) + (subscriptionPlan.Cost_of_Additional_Analytics_Users * (customerInfo.NumberOfAnalyticsAddOn - customerInfo.NumberOfAddOns)));
                            }
                            data.Add("Subsciption_UnitAmountInCents", additionalAmount.ToString());
                        }
                        if (subscriptionNew.AddOns.Count == 0)
                        {
                            //List<RecurlyAddOn> listRecurly = new List<RecurlyAddOn>();
                            // listRecurly.Add(GetRecurlyAddonModel(subscription.PlanCode));
                            Session["NewUserAddonUpdate"] = (RecurlyNew.AddOn)RecurlySubscriptionPlan.AddOns[0];
                        }
                        else
                        {
                            Session["NewUserAddonUpdate"] = (RecurlyNew.AddOn)RecurlySubscriptionPlan.AddOns[0];
                        }


                    }
                }

                if (!data.ContainsKey("Subsciption_UnitAmountInCents"))
                {
                    data.Add("Subsciption_UnitAmountInCents", TotalAmountInCents + (subscriptionNew.UnitAmountInCents / 100));
                }

                ViewBag.Subsciption_UnitAmountInCents = TotalAmountInCents + (subscriptionNew.UnitAmountInCents / 100);
            }

            return data;
        }

        [CustomActionFilter()]
        private void PopulateUserRoleList(object SelectedUserRole = null)
        {
            Logger.Log.Info(string.Format("Populate User List  started "));
            try
            {
                dynamic user_roles = utility.UserRoleRepository.Get(filter: q => (q.UserRoleId > (int)UserRoles.SAU), orderBy: q => q.OrderBy(d => d.RoleName));
                ViewBag.SelectedRoles = new SelectList(user_roles, "UserRoleId", "RoleName", SelectedUserRole);
                Logger.Log.Info(string.Format("Populate User List successfully Loaded"));
            }
            catch (DataException dataEx)
            {
                TempData["ErrorMessage"] = string.Concat("Unable to Populate Company List -", dataEx.Message);
                Logger.Log.Error(string.Format("Unable to Populate User List with Message- {0} " + Environment.NewLine + "Stack Trace: {1} ", dataEx.Message, dataEx.StackTrace));
                throw;
            }
            catch (Exception ex)
            {
                TempData["ErrorMessage"] = string.Concat("Unable to Populate Company List-", ex.Message);
                //Logger.Log.Error(String.Format("Unable to populate User list with Message- {0} " + Environment.NewLine + "Stack Trace: {1}", ex.Message, ex.StackTrace))
                throw;
            }
            finally
            {
                // Logger.Log.Info(String.Format("Populate User Role List Execution Ended"))
            }
        }

        private void SendEmail(User User, int emailId, int? numberOfAddOn)
        {
            string planGuruUrl = string.Empty;
            try
            {
                if (Request.Url.Port != 80)
                {
                    planGuruUrl = "http://" + Common.HostName(Request) + "/";
                }
                else { planGuruUrl = "http://" + Common.HostName(Request) + (Request.Url.IsDefaultPort ? Request.ApplicationPath + "/" : ":" + Request.Url.Port.ToString() + "/"); }
                dynamic objEmailInfo = utility.EmailRepository.GetEmailInfoById(emailId);
                if (emailId == (int)EmailType.AdditionalUserAdded)
                {
                    objEmailInfo.EmailBody = Common.MergeUserEmailBody(User, objEmailInfo, planGuruUrl, numberOfAddOn, null);
                }
                else
                {
                    objEmailInfo.EmailBody = Common.MergeUserEmailBody(User, objEmailInfo, planGuruUrl, null, null);
                }
                Common.SendUserEmail(User, objEmailInfo);
                Logger.Log.Info(string.Format("Email Sent Successfully to User: {0} for EmailId: {1}", User.UserId, emailId));
            }
            catch (Exception ex)
            {
                TempData["ErrorMessage"] = string.Concat("Unable to Send Email -", ex.Message);
                Logger.Log.Error(string.Format("Unable to Send Email, UserId - {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2} ", User.UserId, ex.Message, ex.StackTrace));
            }
            finally
            {
                Logger.Log.Info(string.Format("SendEmail Execution Ended"));
            }
        }

        [CustomActionFilter()]
        public ActionResult ResetPassword(int userId, string pathToRedirect)
        {
            Logger.Log.Info(string.Format("ResetPassword Execution Started"));
            User userDetails = new User();
            try
            {
                userDetails = userRepository.GetUserByID(userId);
                string password = Common.Generate(8, 2, false);
                userDetails.Password = Common.Encrypt(password);
                userDetails.SecurityKey = Guid.NewGuid().ToString();
                userDetails.UpdatedBy = userId.ToString();
                userDetails.UpdatedOn = DateTime.UtcNow;
                userRepository.UpdateUser(userDetails);
                userRepository.Save();
                SendEmail(userDetails, (int)EmailType.PasswordReset, null);
                TempData["Message"] = "Reset Password link has been sent to your Email Id Successfully.";


            }
            catch (DbEntityValidationException dbEntityEx)
            {
                foreach (var sError in dbEntityEx.EntityValidationErrors)
                {

                    Logger.Log.Error(string.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:", sError.Entry.Entity, sError.Entry.State));
                    foreach (var ve in sError.ValidationErrors)
                    {
                        Logger.Log.Error(string.Format("-Property: {0}, Error: {1} ", ve.PropertyName, ve.ErrorMessage));
                    }
                }
            }
            catch (Exception ex)
            {
                TempData["ErrorMessage"] = "Error occured, during reset password request - " + ex.Message.ToString();
                Logger.Log.Error(string.Format("Unable to Reset the password for {0} - " + Environment.NewLine + "{1}, Stack Trace: {2} ", userDetails.UserName, ex.Message, ex.StackTrace));
            }
            finally
            {
                Logger.Log.Info(string.Format("ResetPassword Execution Ended"));
            }
            if ((pathToRedirect == "user"))
            {
                return RedirectToAction("Index");
            }
            else
            {
                return RedirectToAction("Index", "Subscribe");
            }
        }

        [CustomActionFilter()]
        public ActionResult Edit(string userAccountCode)
        {

            try
            {
                dynamic userInfo = userRepository.GetUserByID(Convert.ToInt32(userAccountCode));
                PopulateCustomerList();
                PopulateUserRoleList();
                return View(userInfo);
            }
            catch (DataException dataEx)
            {
                TempData["ErrorMessage"] = string.Concat("Unable to view user details to Edit it - ", dataEx.Message);
                Logger.Log.Error(string.Format("\\n Unable to view user details to Edit it, UserID - {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2} ", userAccountCode, dataEx.Message, dataEx.StackTrace));
            }
            catch (Exception ex)
            {
                TempData["ErrorMessage"] = string.Concat("Unable to view user details to Edit User Information - ", ex.Message);
                Logger.Log.Error(string.Format("\\n Unable to view user details to Edit it, UserID - {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2} ", userAccountCode, ex.Message, ex.StackTrace));
            }
            finally
            {
                Logger.Log.Info(string.Format("Edit User Execution Ended"));
            }
            return View();
        }


        [HttpPost()]
        [CustomActionFilter()]
        public ActionResult Edit(User UserDetails)
        {
            Logger.Log.Info(string.Format("\\n User Update started {0}", UserDetails.UserId));
            User storedUserInfo = default(User);

            try
            {
                if (((Session["UserType"] != null)) & ((Session["UserInfo"] != null)) & ((Session["UserAccess"] != null)))
                {
                    UserType = (UserRole)Session["UserType"];
                    UserInfo = (User)Session["UserInfo"];
                    ua = (UserAccess)Session["UserAccess"];

                }

                if (ModelState.IsValid)
                {
                    //Dim accountInfo = RecurlyAccount.Get(UserDetails.UserId)

                    var usernameAvailibility = utility.UserRepository.UsernameAvailibility(UserDetails.UserName).Where(u => u.UserId != UserDetails.UserId);
                    var emailAddressAvailibility = utility.UserRepository.EmailAddressAvailibility(UserDetails.UserEmail).Where(u => u.UserId != UserDetails.UserId);
                    if ((usernameAvailibility.Count() > 0))
                    {
                        ModelState.AddModelError("Username", "Someone already has that username. Try another.");
                    }
                    else if ((emailAddressAvailibility.Count() > 0))
                    {
                        ModelState.AddModelError("UserEmail", "Someone already has that Email Address. Try another.");
                    }
                    else
                    {
                        //accountInfo.Username = UserDetails.UserName
                        //accountInfo.FirstName = UserDetails.FirstName
                        //accountInfo.LastName = UserDetails.LastName
                        //accountInfo.Email = UserDetails.UserEmail
                        //accountInfo.Update()

                        storedUserInfo = userRepository.GetUserByID(UserDetails.UserId);

                        storedUserInfo.UserName = UserDetails.UserName;
                        storedUserInfo.FirstName = UserDetails.FirstName;
                        storedUserInfo.LastName = UserDetails.LastName;
                        storedUserInfo.UserEmail = UserDetails.UserEmail;
                        storedUserInfo.UserRole = UserDetails.UserRole;
                        storedUserInfo.UserRoleId = UserDetails.UserRoleId;
                        storedUserInfo.CustomerId = UserDetails.CustomerId;
                        storedUserInfo.UpdatedBy = UserInfo.UserId.ToString();

                        userRepository.UpdateUser(storedUserInfo);
                        userRepository.Save();

                        TempData["Message"] = "User updated successfully.";
                        Logger.Log.Info(string.Format("User Updated successfully with id {0}", UserDetails.UserId));
                        return RedirectToAction("Index");
                    }
                }
            }
            catch (DataException dataEx)
            {
                TempData["ErrorMessage"] = string.Concat("Error occured while updating user details - ", dataEx.Message);
                Logger.Log.Error(string.Format("\\n Error occured while updating user details of User id- {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2} ", UserDetails.UserId, dataEx.Message, dataEx.StackTrace));
            }
            catch (Exception ex)
            {
                TempData["ErrorMessage"] = string.Concat("Error occured while updating user details", ex.Message);
                Logger.Log.Error(string.Format("\\n Error occured while updating user details of User id- {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2} ", UserDetails.UserId, ex.Message, ex.StackTrace));
            }
            finally
            {
                Logger.Log.Info(string.Format(" User Update Execution Ended"));
            }

            PopulateCustomerList();
            PopulateUserRoleList(UserDetails.UserRoleId);
            return View(UserDetails);
        }

        [CustomActionFilter()]
        public ActionResult Delete(string userAccountCode)
        {
            Logger.Log.Info(string.Format("User Account Deletion Started {0}", userAccountCode));
            SubscriptionPlan SubscribedPlan = default(SubscriptionPlan);
            int userCount = 0;

            try
            {
                string state = Enum.GetName(typeof(StatusE), StatusE.Active);
                //Get Subscription AccountCode from UserInfo table from local database.
                var localDBUserInfo = utility.UserRepository.GetUserByID(Convert.ToInt32(userAccountCode));
                //Get Recurly Subscription Information using Subscription AccountCode.

                SubscribedPlan = utility.SubscriptionPlanRepository.GetSubscriptionPlan(localDBUserInfo.Customer.PlanTypeId);
                userCount = utility.UserRepository.GetUsers(localDBUserInfo.CustomerId, 0).Where(c => c.UserRoleId != (int)UserRoles.SAU).Count();


                RecurlyNew.Account acc = RecurlyNew.Accounts.Get(localDBUserInfo.CustomerId.ToString());
                RecurlyNew.Subscription recurlySubscriptionInfo = acc.GetSubscriptions(RecurlyNew.Subscription.SubscriptionState.Active).FirstOrDefault();
                //dynamic recurlySubscriptionInfo = RecurlySubscription.Get(localDBUserInfo.CustomerId.ToString());

                //Get Customer Information from Local Database.
                if (((recurlySubscriptionInfo.State == RecurlyNew.Subscription.SubscriptionState.Active)))
                {
                    //To do Add-On functionality
                    //Decrease quantity on recurly subscription

                    if ((recurlySubscriptionInfo.AddOns.Count > 0))
                    {
                        if (recurlySubscriptionInfo.AddOns[0].Quantity > 1)
                        {
                            recurlySubscriptionInfo.AddOns[0].Quantity -= 1;
                            recurlySubscriptionInfo.ChangeSubscription(RecurlyNew.Subscription.ChangeTimeframe.Now);
                        }
                        else if (recurlySubscriptionInfo.AddOns[0].Quantity == 1)
                        {
                            recurlySubscriptionInfo.AddOns.RemoveAt(0);
                            recurlySubscriptionInfo.ChangeSubscription(RecurlyNew.Subscription.ChangeTimeframe.Now);
                        }

                        //RecurlyAddOn addOn = new RecurlyAddOn();
                        //addOn = recurlySubscriptionInfo.AddOn.Addons(0);

                        ////Check if quantity , if quantity is equal to 1 then Addon update is not required.
                        ////It will automatically remove the last addon.
                        //if ((addOn.quantity > 1))
                        //{
                        //    recurlySubscriptionInfo.AddOns = new List<RecurlyAddOn>();
                        //    addOn.quantity -= 1;
                        //    recurlySubscriptionInfo.AddOns.Add(addOn);
                        //    recurlySubscriptionInfo.ChangeSubscription(RecurlySubscription.ChangeTimeframe.Now);

                        //}

                    }

                    //utility.CustomerRepository.UpdateCustomer(customerInfo)
                    //utility.Save()

                    Logger.Log.Info(string.Format("Update Quantity successfully of Customer in local database using UserId - {0}", userAccountCode));

                    //Closing account on recurly
                    if ((RecurlyNew.Account.IsExist(userAccountCode.ToString())))
                    {
                        RecurlyNew.Accounts.Close(userAccountCode);
                    }

                    Logger.Log.Info(string.Format("Close Account successfully on RECURLY using UserId - {0}", userAccountCode));
                }

                //utility.UserRepository.DeleteUser(userAccountCode)
                localDBUserInfo.Status = (int)StatusE.Deactivated;
                localDBUserInfo.Password = Common.Encrypt(localDBUserInfo.Password);
                utility.UserRepository.UpdateUser(localDBUserInfo);
                utility.UserRepository.Save();
                if ((localDBUserInfo.UserRoleId != (int)UserRoles.SAU))
                {
                    utility.UserRepository.DeleteNonSAUUser(Convert.ToInt32(userAccountCode));
                    //utility.UserRepository.Save()
                }
                SendEmail(localDBUserInfo, (int)EmailType.UserDeleted, null);

                utility.UserAnalysisMappingRepository.DeleteUserAnalysisMappingByUserId(Convert.ToInt32(userAccountCode));
                utility.UserAnalysisMappingRepository.Save();

                utility.UserCompanyMappingRepository.DeleteUserCompanyMappingByUserId(Convert.ToInt32(userAccountCode));
                utility.UserCompanyMappingRepository.Save();

                utility.DashboardRepository.DeleteDashboardItemsByUserId(Convert.ToInt32(userAccountCode));
                utility.DashboardRepository.Save();

                Logger.Log.Info(string.Format("Deactivate Account successfully on local database using UserId - {0}", userAccountCode));

                TempData["Message"] = "User account deleted successfully.";
                Logger.Log.Info(string.Format("User account deleted successfully using  {0}", userAccountCode));
            }
            catch (DataException dataEx)
            {
                TempData["ErrorMessage"] = string.Concat("Error occured while deleting selected User - ", dataEx.Message);
                Logger.Log.Error(string.Format(Environment.NewLine + " Error occured while deleting selected User, UserId - {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2}", userAccountCode, dataEx.Message, dataEx.StackTrace));
                return RedirectToAction("Index", new System.Web.Routing.RouteValueDictionary {
			{
				"id",
				userAccountCode
			},
			{
				"deleteChangesError",
				true
			}
		});
            }
            catch (Exception ex)
            {
                TempData["ErrorMessage"] = string.Concat("Error occured while deleting selected User - ", ex.Message);
                Logger.Log.Error(string.Format(" Error occured while deleting selected User, UserId - {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2} ", userAccountCode, ex.Message, ex.StackTrace));
            }
            finally
            {
                Logger.Log.Info(string.Format("Execution Ended"));
            }
            return RedirectToAction("Index");
        }

        [CustomActionFilter()]
        public ActionResult SeatIndex()
        {
            try
            {

                if (Session["UserInfo"] != null)
                {
                    UserInfo = (User)Session["UserInfo"];
                }
                return View(UserInfo);
            }
            catch (Exception ex)
            {
                TempData["ErrorMessage"] = string.Concat("Unable to show User List - ", ex.Message);
                Logger.Log.Error(string.Format("Unable to show User List -  Message- {0} " + Environment.NewLine + "Stack Trace: {1} ", ex.Message, ex.StackTrace));
                return View();
            }
            finally
            {
                Logger.Log.Info(string.Format("User Index Function Execution Ended"));
            }

        }


        [CustomActionFilter()]
        public ActionResult AddSeat()
        {
            Hashtable data = new Hashtable();
            User userDetail = new User();
            userDetail.Customer = new Customer();
            if (((Session["UserType"] != null)) & ((Session["UserInfo"] != null)) & ((Session["UserAccess"] != null)))
            {
                UserType = (UserRole)Session["UserType"];
                UserInfo = (User)Session["UserInfo"];
                ua = (UserAccess)Session["UserAccess"];
                CustomerId = UserType.UserRoleId == (int)UserRoles.SAU | UserType.UserRoleId == (int)UserRoles.SSU ? UserInfo.CustomerId : (int?)null;
            }
            if (CustomerId > 0)
            {
                data = GetSubscriptionCost(CustomerId.ToString(), UserInfo, data);
                userDetail.CustomerId = UserInfo.CustomerId;
                userDetail.Customer.CustomerCompanyName = UserInfo.Customer.CustomerCompanyName;
                ViewBag.AddOnAmountInCents = data["AddOnAmountInCents"];
                ViewBag.SubsciptionUnitAmountInCents = data["Subsciption_UnitAmountInCents"];

            }
            return View(userDetail);
        }

        [HttpGet()]
        [CustomActionFilter()]
        public bool AddNewSeat(int CustomerId, int NumberOfAddOnUser)
        {
            try
            {

                XMLlisenceProvider lisenceProvider = new XMLlisenceProvider();
                customerInfo = utility.CustomerRepository.GetCustomerById(CustomerId);
                var AddonUpdate = (RecurlyNew.AddOn)Session["NewUserAddonUpdate"];

                RecurlyNew.Account acc = RecurlyNew.Accounts.Get(CustomerId.ToString());
                //var subscription = RecurlySubscription.Get(CustomerId.ToString());
                var subscription = acc.GetSubscriptions(RecurlyNew.Subscription.SubscriptionState.Active).FirstOrDefault();
                if  (RecurlyNew.Account.IsExist(CustomerId.ToString()))
                {

                    //subscription.AddOns = new List<RecurlyAddOn>();
                    if (AddonUpdate != null)
                    {
                        if ((subscription.AddOns.Count == 0))
                        {
                            //Addon[0].quantity = NumberOfAddOnUser;
                            subscription.AddOns.Add(AddonUpdate);
                        }
                        else
                        {
                            String addOnCode = AddonUpdate.AddOnCode;
                            //int count = subscription.Addons.Where(y => y.add_on_code == GetRecurlyAddonModel(subscription.PlanCode).add_on_code).Count(); 
                            int countNew = subscription.AddOns.Where(z => z.AddOnCode == addOnCode).Count();
                            if (countNew > 0)
                            {
                                foreach (RecurlyNew.SubscriptionAddOn item in subscription.AddOns)
                                {
                                    if ((item.AddOnCode == addOnCode))
                                    {
                                        item.Quantity += NumberOfAddOnUser;
                                    }
                                    //subscription.AddOns.Add(item);
                                }
                            }
                            else
                            {
                                //    foreach (RecurlyAddOn item in Addon)
                                //    {
                                //        subscription.AddOns.Add(item);
                                //    }
                                //    var numberAddOn = GetRecurlyAddonModel(subscription.PlanCode);
                                //   numberAddOn.quantity = NumberOfAddOnUser;
                                // RecurlyNew.SubscriptionAddOn addOn = new RecurlyNew.SubscriptionAddOn(AddonUpdate.AddOnCode,AddonUpdate.UnitAmountInCents.FirstOrDefault().Value,NumberOfAddOnUser);
                                subscription.AddOns.Add(AddonUpdate.AddOnCode, NumberOfAddOnUser, AddonUpdate.UnitAmountInCents.FirstOrDefault().Value);
                            }

                        }
                        subscription.ChangeSubscription(RecurlyNew.Subscription.ChangeTimeframe.Now);
                    }


                }
                XmlNode node = lisenceProvider.UpdateActivationFields(customerInfo.LicenseId.Value, NumberOfAddOnUser);
                if (customerInfo.PlanTypeId == (int)PlanType.AAM || customerInfo.PlanTypeId == (int)PlanType.AAY)
                {
                    XmlNode updateUserDefinedFieldResponse = lisenceProvider.UpdateUserDefineCharacterCount(customerInfo.LicenseId.Value, customerInfo.CustomerId.ToString(), customerInfo.PlanTypeId.ToString(), customerInfo.Password, NumberOfAddOnUser);
                }
                utility.CustomerRepository.IncrementAddOn(CustomerId, NumberOfAddOnUser);
                dynamic userDetail = utility.UserRepository.GetSAUUser(customerInfo.CustomerId);
                Session["UserInfo"] = userDetail;
                SendEmail(userDetail, (int)EmailType.AdditionalUserAdded, NumberOfAddOnUser);
                return true;
            }
            catch (DataException dataEx)
            {
                Logger.Log.Error(string.Format("\\n Error occured while updating user details of User id- {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2} ", CustomerId, dataEx.Message, dataEx.StackTrace));
                return false;
            }
            catch (Exception ex)
            {
                Logger.Log.Error(string.Format("\\n Error occured while updating user details of User id- {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2} ", CustomerId, ex.Message, ex.StackTrace));
                return false;
            }
            finally
            {
                Logger.Log.Info(string.Format(" User Update Execution Ended"));
            }
        }

        //[CustomActionFilter()]
        //public ActionResult AddNewSeat(int CustomerId)
        //{
        //    try
        //    {

        //        XMLlisenceProvider lisenceProvider = new XMLlisenceProvider();
        //        customerInfo = utility.CustomerRepository.GetCustomerById(CustomerId);
        //        dynamic Addon = (RecurlyAddOn)Session["NewUserAddon"];
        //        var subscription = RecurlySubscription.Get(CustomerId.ToString());

        //        if (RecurlyAccount.IsExist(CustomerId.ToString()))
        //        {

        //            subscription.AddOns = new List<RecurlyAddOn>();
        //            if (Addon != null)
        //            {
        //                Addon.quantity += 1;
        //                subscription.AddOns.Add(Addon);
        //            }

        //            subscription.ChangeSubscription(RecurlySubscription.ChangeTimeframe.Now);
        //        }
        //        XmlNode node = lisenceProvider.UpdateActivationFields(customerInfo.LicenseId.Value, 1);
        //        utility.CustomerRepository.IncrementAddOnByOne(CustomerId);
        //        UserInfo = utility.UserRepository.GetSAUUser(customerInfo.CustomerId);
        //        Session["UserInfo"] = UserInfo;
        //        TempData["Message"] = "User added successfully.";
        //        SendEmail(UserInfo, (int)EmailType.AdditionalUserAdded);
        //        return RedirectToAction("Index");
        //    }
        //    catch (DataException dataEx)
        //    {
        //        TempData["ErrorMessage"] = string.Concat("Error occured while updating user details - ", dataEx.Message);
        //        Logger.Log.Error(string.Format("\\n Error occured while updating user details of User id- {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2} ", CustomerId, dataEx.Message, dataEx.StackTrace));
        //    }
        //    catch (Exception ex)
        //    {
        //        TempData["ErrorMessage"] = string.Concat("Error occured while updating user details", ex.Message);
        //        Logger.Log.Error(string.Format("\\n Error occured while updating user details of User id- {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2} ", CustomerId, ex.Message, ex.StackTrace));
        //    }
        //    finally
        //    {
        //        Logger.Log.Info(string.Format(" User Update Execution Ended"));
        //    }
        //    return RedirectToAction("Index");
        //}

        //private RecurlyAddOn GetRecurlyAddonModel(string planCode)
        //{

        //    RecurlyAddOn addon = new RecurlyAddOn();
        //    SubscriptionPlan plan = utility.SubscriptionPlanRepository.GetSubscriptionPlan(planCode);
        //    if (plan != null)
        //    {
        //        addon.AccountCode = null;
        //        addon.add_on_code = planCode + "01";
        //        addon.Addon = null;
        //        addon.Addons = null;
        //        addon.name = null;
        //        addon.quantity = 1;
        //        addon.unit_amount_in_cents = Convert.ToInt32(plan.Cost_of_Additional_Users * 100);
        //    }

        //    return addon;
        //}


        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult IsSessionAlive()
        {

            bool data = false;
            if ((Session.IsNewSession))
            {
                data = false;
            }
            else
            {
                data = true;
            }

            return Json(data, JsonRequestBehavior.AllowGet);
        }



        public ActionResult Logout()
        {
            Session.Clear();
            Session.Abandon();
            Response.Cookies["ASP.NET_SessionId"].Value = string.Empty;
            Response.Cookies["ASP.NET_SessionId"].Expires = DateTime.Now.AddMonths(-20);
            return RedirectToAction("Index", "Home");
        }


    }
}
