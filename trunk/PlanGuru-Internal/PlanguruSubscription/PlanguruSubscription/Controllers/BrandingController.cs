﻿using PlanguruSubscription.DAL;
using PlanguruSubscription.Models;
using PlanguruSubscription.Modules;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace PlanguruSubscription.Controllers
{
    public class BrandingController : Controller
    {
        private Utility utility = new Utility();
        private ICustomerRepository customerRepository;

        private IUserRepository userRepository;
        private User UserInfo;

        public BrandingController()
        {
            this.customerRepository = new CustomerRepository(new DataAccess());
            this.userRepository = new UserRepository(new DataAccess());
        }

        public BrandingController(IUserRepository userRepository)
        {
            this.userRepository = userRepository;
        }

        public BrandingController(ICustomerRepository customerRepository)
        {
            this.customerRepository = customerRepository;
        }

        public ActionResult Index()
        {

            Customer customer = null;
            Branding BrandingOptions = new Branding();
            try
            {
                if ((Session["UserInfo"] != null))
                {
                    UserInfo = (User)Session["UserInfo"];
                }
                customer = utility.CustomerRepository.GetCustomerById(UserInfo.CustomerId);

                if ((customer.PrimaryColor == null | customer.SecondaryColor == null))
                {
                    BrandingOptions.PrimaryColor = "#276FA6";
                    BrandingOptions.SecondaryColor = "#1B3E70";
                }
                else
                {
                    BrandingOptions.PrimaryColor = customer.PrimaryColor;
                    BrandingOptions.SecondaryColor = customer.SecondaryColor;
                }
                BrandingOptions.VanityFirmName = customer.VanityFirmName;
                BrandingOptions.Footnote = customer.FootNote;

            }
            catch (Exception ex)
            {
                TempData["ErrorMessage"] = string.Concat("Error occured while showing branding - ", ex.Message);
                Logger.Log.Error(string.Format("\\n Error occured while showing branding - with Message- {0} " + Environment.NewLine + "Stack Trace: {1} ", ex.Message, ex.StackTrace));
            }
            return View(BrandingOptions);
        }

        [HttpPost()]
        [CustomActionFilter()]
        public ActionResult Index(Branding BrandingOptions)
        {

            Customer customer = null;
            bool uniqueFirmName = false;

            try
            {
                if ((Session["UserInfo"] != null))
                {
                    UserInfo = (User)Session["UserInfo"];
                }

                uniqueFirmName = utility.CustomerRepository.IsFirmNameUnique(BrandingOptions.VanityFirmName, UserInfo.CustomerId);

                if ((uniqueFirmName == true))
                {
                    ModelState.AddModelError("VanityFirmName", "Someone already has that Vanity Firm Name. Try another.");
                }


                if (ModelState.IsValid)
                {
                    customer = utility.CustomerRepository.GetCustomerById(UserInfo.CustomerId);

                    //Update New filepath with old filepath
                    if ((BrandingOptions.PrimaryColor == null | BrandingOptions.SecondaryColor == null))
                    {
                        customer.PrimaryColor = "#276FA6";
                        customer.SecondaryColor = "#1B3E70";
                    }
                    else
                    {
                        customer.PrimaryColor = BrandingOptions.PrimaryColor;
                        customer.SecondaryColor = BrandingOptions.SecondaryColor;
                    }
                    customer.FootNote = (BrandingOptions.Footnote == null ? string.Empty : BrandingOptions.Footnote);
                    customer.VanityFirmName = (BrandingOptions.VanityFirmName == null ? string.Empty : BrandingOptions.VanityFirmName.Trim());

                    customer.UpdatedBy = UserInfo.UserId.ToString();
                    utility.CustomerRepository.UpdateCustomer(customer);
                    utility.CustomerRepository.Save();

                    UserInfo.Customer.PrimaryColor = BrandingOptions.PrimaryColor;
                    UserInfo.Customer.SecondaryColor = BrandingOptions.SecondaryColor;
                    UserInfo.Customer.FootNote = BrandingOptions.Footnote;

                    Session["UserInfo"] = UserInfo;
                    TempData["Message"] = "Branding Options updated successfully.";
                }

            }
            catch (Exception ex)
            {
                TempData["ErrorMessage"] = string.Concat("Error occured while updating branding - ", ex.Message);
                Logger.Log.Error(string.Format("\\n Customer Id:{0}  ,VanityFirmName: {1}", customer.CustomerId, BrandingOptions.VanityFirmName));
                Logger.Log.Error(string.Format("\\n Error occured while updating branding - with Message- {0} " + Environment.NewLine + "Stack Trace: {1} ", ex.Message, ex.StackTrace));
                Logger.Log.Error(string.Format("\\n Innerexception : {0}", ex.InnerException.ToString()));

            }
            return View(BrandingOptions);
        }


        [HttpPost()]
        [CustomActionFilter()]
        public JsonResult UploadFile()
        {

            Customer customer = null;
            string OldFilePath = null;
            dynamic data = null;
            HttpPostedFileBase hpf = null;
            string savedFileName = string.Empty;
            string filename = string.Empty;
            string specialCharacterRegEx = "[^a-zA-Z0-9.]+";

            try
            {
                if ((Session["UserInfo"] != null))
                {
                    UserInfo = (User)Session["UserInfo"];
                }


                foreach (string File in Request.Files)
                {
                    customer = utility.CustomerRepository.GetCustomerById(UserInfo.CustomerId);
                    OldFilePath = customer.LogoImageFile;


                    hpf = Request.Files[File]  as HttpPostedFileBase;
                    filename = Path.GetFileNameWithoutExtension(hpf.FileName);
                    filename = Regex.Replace(filename, specialCharacterRegEx, "");
                    filename = string.Concat(filename, "_", DateTime.UtcNow.ToString("dd-MM-yyyy_HHmmss").ToString(), Path.GetExtension(hpf.FileName));

                    savedFileName = Path.Combine(Server.MapPath("~/Branding_Logos"), filename);

                    hpf.SaveAs(savedFileName);

                    //Update New filepath with old filepath
                    customer.LogoImageFile = filename;
                    customer.UpdatedBy = UserInfo.UserId.ToString();
                    utility.CustomerRepository.UpdateCustomer(customer);
                    utility.CustomerRepository.Save();

                    UserInfo.Customer.LogoImageFile = filename;

                    Session["UserInfo"] = UserInfo;

                    //Delete old saved file
                    if ((System.IO.File.Exists(OldFilePath)))
                    {
                        System.IO.File.Delete(OldFilePath);
                    }


                }

                data = new
                {
                    status = "success",
                    src = string.Concat("././Branding_Logos/", filename)
                };

                return Json(data, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                data = new
                {
                    status = "error",
                    error = ex.Message.ToString()
                };
                return Json(data, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost()]
        [CustomActionFilter()]
        public JsonResult ResetMenuColors()
        {
            dynamic data = null;
            Customer customer = null;
            try
            {
                if ((Session["UserInfo"] != null))
                {
                    UserInfo = (User)Session["UserInfo"];
                }

                customer = utility.CustomerRepository.GetCustomerById(UserInfo.CustomerId);

                customer.PrimaryColor = "#276FA6";
                customer.SecondaryColor = "#1B3E70";
                customer.UpdatedBy = UserInfo.UserId.ToString();
                utility.CustomerRepository.UpdateCustomer(customer);
                utility.CustomerRepository.Save();
                Session["UserInfo"] = UserInfo;

                UserInfo.Customer.PrimaryColor = "#276FA6";
                UserInfo.Customer.SecondaryColor = "#1B3E70";

                data = new { status = "success" };
                return Json(data, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                data = new
                {
                    status = "error",
                    error = ex.Message.ToString()
                };
                return Json(data, JsonRequestBehavior.AllowGet);
            }
        }




    }
}
