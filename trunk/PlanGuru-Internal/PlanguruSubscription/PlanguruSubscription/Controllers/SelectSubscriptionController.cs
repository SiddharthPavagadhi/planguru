﻿using PlanguruSubscription.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PlanguruSubscription.Models;
using PlanguruSubscription.Modules;
using System.Data;
//using Recurly;
using RecurlyNew;
using System.Data.Entity.Validation;
using LicenceService;
using System.Xml;

namespace PlanguruSubscription.Controllers
{
    public class SelectSubscriptionController : Controller
    {
        private Utility utility = new Utility();
        private Common common = new Common();
        private CustomerServiceProvider customerserviceprovider;
        private XMLlisenceProvider licenseprovider;
        private ICustomerRepository customerRepository;
        private ISubscriptionPlanRepository subscriptionPlanRepository;

        public SelectSubscriptionController()
        {
            this.customerRepository = new CustomerRepository(new DataAccess());
            this.subscriptionPlanRepository = new SubscriptionPlanRepositoy(new DataAccess());
        }

        public SelectSubscriptionController(ISubscriptionPlanRepository subscriptionPlanRepository)
        {
            this.subscriptionPlanRepository = subscriptionPlanRepository;
        }

        public SelectSubscriptionController(ICustomerRepository customerRepository)
        {
            this.customerRepository = customerRepository;
        }

        //
        // GET: /SelectSubscription/

        public ActionResult ChooseSubscription()
        {
            return View();
        }

        public ActionResult BusinessSignup()
        {
            return View();
        }

        public ActionResult AdvisorSignup()
        {
            return View();
        }

        public ActionResult Subscribe(int selectedPlan)
        {

            try
            {
                SubscribeForm subscribeModel = new SubscribeForm();
                var subscriptionPlan = utility.SubscriptionPlanRepository.GetSubscriptionPlan(selectedPlan);
                var RecurlySubscriptionPlan = RecurlyNew.Plans.Get(subscriptionPlan.Plan);

                subscribeModel.PlanCode = RecurlySubscriptionPlan.PlanCode;
                subscribeModel.PlanName = RecurlySubscriptionPlan.Name;
                subscribeModel.PlanRate = subscriptionPlan.Cost_of_Subscription.ToString();
                subscribeModel.PlanTypeId = subscriptionPlan.PlanTypeId;
                if (RecurlySubscriptionPlan.AddOns.All.Count > 0)
                {
                    subscribeModel.AddOnName = RecurlySubscriptionPlan.AddOns.All[0].Name.ToString();
                    subscribeModel.AddOnUnitAmount = RecurlySubscriptionPlan.AddOns.All[0].UnitAmountInCents.FirstOrDefault().Value / 100;
                    subscribeModel.AddOnCode = RecurlySubscriptionPlan.AddOns.All[0].AddOnCode.ToString();
                }
                if (RecurlySubscriptionPlan.PlanIntervalLength == 1)
                {
                    subscribeModel.PlanFrequency = "month";
                }
                if (RecurlySubscriptionPlan.PlanIntervalLength == 12)
                {
                    subscribeModel.PlanFrequency = "12 months";
                }
               // subscribeModel.CoupenList = RecurlyNew.Coupons.List(Coupon.CouponState.Redeemable).All().ToList();
                subscribeModel.CoupenList = RecurlyNew.Coupons.List(Coupon.CouponState.Redeemable).All.ToList();
                string subscription = string.Format("{0}={1}", "subscription%5Bplan_code%5D", subscriptionPlan.Plan);



                ViewBag.Signature = PlanguruSubscription.DAL.Utility.SignWithParameters(subscription);
                ViewBag.AccountCode = PlanguruSubscription.DAL.Utility.CheckAccountCodeOnRecurly((utility.CustomerRepository.GetCustomer().Max(c => c.CustomerId)) + 1);
                ViewBag.Plan = subscriptionPlan.Plan.ToString();

                TempData["AccountCode"] = ViewBag.AccountCode.ToString();
                Logger.Log.Info(string.Format("Subscribe Execution Done"));
                return View(subscribeModel);
            }
            catch (DataException dataEx)
            {
                TempData["ErrorMessage"] = string.Concat("Error occured during loading subscription plan -", dataEx.Message);
                Logger.Log.Error(string.Format("Error occured during loading subscription plan - {0} " + Environment.NewLine + "Stack Trace: {1} ", dataEx.Message, dataEx.StackTrace));
                return View();
            }
            catch (Exception ex)
            {
                TempData["ErrorMessage"] = string.Concat("Unable to Subscribe()-", ex.Message);
                Logger.Log.Error(string.Format("\\n Unable to Subscribe() with Message- {0} " + Environment.NewLine + "Stack Trace: {1} ", ex.Message, ex.StackTrace));
                return View();
            }
            finally
            {
                Logger.Log.Info(string.Format("Subscribe() Execution Ended"));
            }

        }

        [HttpPost]
        public ActionResult Subscribe(SubscribeForm subscribeForm)
        {
            Customer customer = null;
            int AccountCode = 0;
            int newUserAccountCode = 0;
            User userInfo = null;
            string username = string.Empty;
            int licensecustomerid = 0;
            SubscriptionPlan plan = null;
            XmlNode node = null;
            try
            {
                Logger.Log.Info(string.Format("Home - Subscription Execution Started"));

                if (((TempData["AccountCode"] != null)))
                {
                    AccountCode = Convert.ToInt32(TempData["AccountCode"]);

                }

                if (((TempData["AccountCode"] != null)))
                {

                    //RecurlyAccount account = new Recurly.RecurlyAccount(AccountCode.ToString());
                    RecurlyNew.Account account = new RecurlyNew.Account(AccountCode.ToString());
                    account.CompanyName = subscribeForm.Subscriber_Name;
                    account.Email = subscribeForm.Email;
                    account.FirstName = subscribeForm.First_Name;
                    account.LastName = subscribeForm.Last_Name;
                    account.AcceptLanguage = subscribeForm.PreferedLanguage;
                    account.Create();


                    //RecurlyBillingInfo BillingInfo = new Recurly.RecurlyBillingInfo(account);
                    RecurlyNew.BillingInfo BillingInfo = new RecurlyNew.BillingInfo(account);
                    BillingInfo.TokenId = subscribeForm.RecurlyToken;
                    BillingInfo.Create();

                    // RecurlyAccount accountInfoFromRecurly = RecurlyAccount.Get(AccountCode.ToString());
                    RecurlyNew.Account accountInfoFromRecurly = RecurlyNew.Accounts.Get(AccountCode.ToString());
                    Logger.Log.Info("Account Information is successfully submitted in Recurly for Account Id =" + accountInfoFromRecurly.AccountCode + " and Status is = " + accountInfoFromRecurly.State);
                    // RecurlyBillingInfo billingInfoFromRecurly = RecurlyBillingInfo.Get(AccountCode.ToString());
                    RecurlyNew.BillingInfo billingInfoFromRecurly = RecurlyNew.BillingInfo.Get(AccountCode.ToString());
                    Logger.Log.Info("Billing Information is successfully submitted in Recurly for Account Id =" + billingInfoFromRecurly.AccountCode);
                    RecurlyNew.Plan RecurlySubscriptionPlan = RecurlyNew.Plans.Get(subscribeForm.PlanCode);

                    //RecurlySubscription subscription = new RecurlySubscription(account);
                    //subscription.PlanCode = subscribeForm.PlanCode;
                    string CouponCode = subscribeForm.Coupen_Code != null ? subscribeForm.Coupen_Code.ToLower() : null;
                    //subscription.Account = accountInfoFromRecurly;
                    //if (subscribeForm.Number_Of_Addon > 0)
                    //{
                    //    subscription.AddOn = new RecurlyAddOn();
                    //    subscription.AddOn.add_on_code = subscribeForm.AddOnCode;
                    //    subscription.AddOn.quantity = subscribeForm.Number_Of_Addon;
                    //    subscription.AddOn.unit_amount_in_cents = Convert.ToInt32(subscribeForm.AddOnUnitAmount * 100);
                    //}

                    //subscription.CreateSubscription();
                    RecurlyNew.Subscription subscription = new RecurlyNew.Subscription(accountInfoFromRecurly, RecurlySubscriptionPlan, "USD", CouponCode);
                    if (subscribeForm.Number_Of_Addon > 0)
                    {
                        subscription.AddOns.Add(subscribeForm.AddOnCode, subscribeForm.Number_Of_Addon, Convert.ToInt32(subscribeForm.AddOnUnitAmount * 100));
                    }
                    subscription.Create();
                    RecurlyNew.Subscription subscriptionInfoFromRecurly = RecurlyNew.Subscriptions.Get(subscription.Uuid);


                    // RecurlySubscription subscriptionInfoFromRecurly = RecurlySubscription.Get(AccountCode.ToString());
                    Logger.Log.Info("Successfully get Recurly Account Code from Recurly API,Account Id is =" + subscriptionInfoFromRecurly.Account.AccountCode + "Status is = " + subscriptionInfoFromRecurly.State);
                    int numberOfAddOn = subscriptionInfoFromRecurly.AddOns.All.Count == 0 ? 0 : subscriptionInfoFromRecurly.AddOns.All.FirstOrDefault().Quantity;

                    if ((subscriptionInfoFromRecurly != null))
                    {
                        //RecurlyAccount accountInfo = RecurlyAccount.Get(AccountCode.ToString());
                        //Logger.Log.Info("Account Information is successfully submitted in Recurly for Account Id =" + accountInfo.AccountCode + " and Status is = " + accountInfo.State);
                        //RecurlyBillingInfo billingInfo = RecurlyBillingInfo.Get(AccountCode.ToString());
                        //Logger.Log.Info("Billing Information is successfully submitted in Recurly for Account Id =" + billingInfo.AccountCode);
                        plan = utility.SubscriptionPlanRepository.GetSubscriptionPlan(subscriptionInfoFromRecurly.PlanCode);

                        if (plan.PlanTypeId != (int)PlanType.BP && plan.PlanTypeId != (int)PlanType.PPU)
                        {

                            if (accountInfoFromRecurly != null && billingInfoFromRecurly != null)
                            {
                                customerserviceprovider = new CustomerServiceProvider();
                                licenseprovider = new XMLlisenceProvider();
                                Logger.Log.Info(String.Format("Customer License registration is started for account code=" + accountInfoFromRecurly.AccountCode));
                                licensecustomerid = customerserviceprovider.CreateCustomer(accountInfoFromRecurly, Common.Generate(8, 15, true), billingInfoFromRecurly);
                                if (licensecustomerid < 0)
                                {
                                    RecurlyNew.Accounts.Close(AccountCode.ToString());
                                    // RecurlyAccount.CloseAccount(AccountCode.ToString());
                                    subscriptionInfoFromRecurly.Terminate(RecurlyNew.Subscription.RefundType.None);
                                    //RecurlySubscription.TerminateSubscription(AccountCode.ToString());
                                    Logger.Log.Error(string.Format("can't get response from create customer method the response text is =" + licensecustomerid));
                                    TempData["ErrorMessage"] = "cant create customer"; //need to ask
                                }
                                Logger.Log.Info(string.Format("create customer method creates customer with licensecustomerid" + licensecustomerid));
                            }

                            if (licensecustomerid > 0)
                            {
                                bool isAccountantAdvisor = false;
                                Logger.Log.Info(string.Concat("xml add user service started"));
                                if (plan.PlanTypeId == (int)PlanType.AAM || plan.PlanTypeId == (int)PlanType.AAY)
                                {
                                    node = licenseprovider.AddLicenseUser(licensecustomerid, plan.ProductOptionId.Value, (numberOfAddOn + 5), plan.Cost_of_Subscription);
                                    isAccountantAdvisor = true;
                                }
                                else
                                {
                                    node = licenseprovider.AddLicenseUser(licensecustomerid, plan.ProductOptionId.Value, (numberOfAddOn + 1), plan.Cost_of_Subscription);
                                    isAccountantAdvisor = false;
                                }
                                Logger.Log.Info("xml add license user service invoked successfully for account id =" + accountInfoFromRecurly.AccountCode);

                                if (node != null)
                                {
                                    Logger.Log.Info("xml license add user method successfully invoked with response = " + node.InnerXml);
                                    XmlNode updateUserDefinedFieldResponse = licenseprovider.UpdateUserDefinedFields(Convert.ToInt32(node.ChildNodes[1].InnerText), AccountCode.ToString(), plan.PlanTypeId.ToString(), isAccountantAdvisor);
                                    Logger.Log.Info("xml updateuserdefinedfieldresponse successfully invoked with response = " + updateUserDefinedFieldResponse.InnerXml);
                                }
                            }
                        }


                        customer = new Customer();
                        customer.CustomerId = AccountCode;
                        customer.PlanTypeId = utility.SubscriptionPlanRepository.GetSubscriptionPlan(subscriptionInfoFromRecurly.PlanCode).PlanTypeId;
                        customer.Quantity = subscription.Quantity;
                        customer.CustomerFirstName = accountInfoFromRecurly.FirstName;
                        customer.CustomerLastName = accountInfoFromRecurly.LastName;
                        customer.CustomerEmail = accountInfoFromRecurly.Email;
                        customer.CustomerCompanyName = accountInfoFromRecurly.CompanyName;

                        //******Comment below code because of PCI compliance ***********
                        customer.ContactFirstName = string.Empty;
                        customer.ContactLastName = string.Empty;
                        //Credit Card Information                       
                        customer.FirstNameOnCard = string.Empty;
                        //billingInfo.FirstName
                        customer.LastNameOnCard = string.Empty;
                        //billingInfo.LastName
                        customer.CreditCardNumber = string.Empty;
                        //String.Concat("XXXX-XXXX-XXXX-", billingInfo.CreditCard.LastFour)
                        customer.CVV = string.Empty;
                        //billingInfo.VerificationValue
                        customer.ExpirationMonth = 0;
                        //billingInfo.CreditCard.ExpirationMonth
                        customer.ExpirationYear = 0;
                        //billingInfo.CreditCard.ExpirationYear
                        customer.CustomerAddress1 = string.Empty;
                        //billingInfo.Address1
                        customer.CustomerAddress2 = string.Empty;
                        //billingInfo.Address2

                        //******Comment below code because of PCI compliance ***********
                        //If (billingInfo.PostalCode.Length > 10) Then
                        //    customer.CustomerPostalCode = billingInfo.PostalCode.ToString().Substring(0, 10)
                        //Else
                        //    customer.CustomerPostalCode = billingInfo.PostalCode
                        //End If
                        customer.CustomerPostalCode = string.Empty;

                        if ((billingInfoFromRecurly.PhoneNumber.Length > 15))
                        {
                            customer.ContactTelephone = billingInfoFromRecurly.PhoneNumber.ToString().Substring(0, 15);
                        }
                        else
                        {
                            customer.ContactTelephone = billingInfoFromRecurly.PhoneNumber;
                        }

                        customer.Country = billingInfoFromRecurly.Country;
                        customer.State = string.Empty;
                        //billingInfo.State
                        customer.City = string.Empty;
                        //billingInfo.City

                        customer.SubscriptionStartAt = Convert.ToDateTime(subscriptionInfoFromRecurly.CurrentPeriodStartedAt).ToString("MMM dd, yyyy");
                        customer.SubscriptionEndAt = Convert.ToDateTime(subscriptionInfoFromRecurly.CurrentPeriodEndsAt).ToString("MMM dd, yyyy");
                        customer.SubscriptionAmount = (Convert.ToDouble(subscriptionInfoFromRecurly.UnitAmountInCents) / 100);
                        customer.CreatedOn = DateTime.Now;
                        customer.CreatedBy = customer.CustomerId.ToString();
                        customer.UpdatedOn = DateTime.Now;
                        customer.UpdatedBy = customer.CustomerId.ToString();
                        customer.LogoImageFile = "";
                        customer.PrimaryColor = "";
                        customer.SecondaryColor = "";
                        customer.FootNote = "";
                        customer.LicenseCustomerId = licensecustomerid;
                        if (node != null)
                        {
                            customer.ResultCode = Convert.ToInt32(node.ChildNodes[0].InnerText);
                            customer.LicenseId = Convert.ToInt32(node.ChildNodes[1].InnerText);
                            customer.Password = node.ChildNodes[2].InnerText;
                            customer.SerialNumber = node.ChildNodes[3].InnerText;
                            customer.ActivationPassword = node.ChildNodes[4].InnerText;
                            customer.AddUserResponse = node.InnerXml;
                            if (subscription.AddOns.All.Count == 0)
                            {
                                if (plan.PlanTypeId == (int)PlanType.AAM || plan.PlanTypeId == (int)PlanType.AAY)
                                {
                                    customer.NumberOfAnalyticsAddOn = 2;
                                }
                                else
                                {
                                    customer.NumberOfAnalyticsAddOn = 0;
                                }
                                customer.NumberOfAddOns = 0;

                            }
                            else
                            {
                                if (plan.PlanTypeId == (int)PlanType.AAM || plan.PlanTypeId == (int)PlanType.AAY)
                                {
                                    customer.NumberOfAnalyticsAddOn = 2 + (subscriptionInfoFromRecurly.AddOns.All.FirstOrDefault().Quantity);
                                }
                                else
                                {
                                    customer.NumberOfAnalyticsAddOn = subscriptionInfoFromRecurly.AddOns.All.FirstOrDefault().Quantity;
                                }
                                customer.NumberOfAddOns = subscriptionInfoFromRecurly.AddOns.All.FirstOrDefault().Quantity;

                            }
                        }

                        utility.CustomerRepository.InsertCustomer(customer);
                        utility.CustomerRepository.Save();
                        Logger.Log.Info(string.Format("Home - Customer account created successfully"));
                        TempData["Message"] = "Subscription created successfully.";

                        newUserAccountCode = (utility.UserRepository.GetUsers().Max(c => c.UserId) + 1);

                        //SAU user account creation

                        username = PlanguruSubscription.DAL.Utility.AutoGenerateUserName();
                        string password = Common.Generate(8, 2, false);

                        Logger.Log.Info(string.Format("Home - Username {0} , Password {1}", username, password));

                        userInfo = new User();
                        userInfo.UserId = newUserAccountCode;
                        userInfo.CustomerId = customer.CustomerId;
                        userInfo.UserName = username;
                        userInfo.UserRoleId = (int)UserRoles.SAU;
                        userInfo.FirstName = accountInfoFromRecurly.FirstName;
                        userInfo.LastName = accountInfoFromRecurly.LastName;
                        userInfo.UserEmail = accountInfoFromRecurly.Email;
                        userInfo.Status = Convert.ToInt32(StatusE.Pending);
                        userInfo.CreatedBy = customer.CustomerId.ToString();
                        userInfo.CreatedOn = DateTime.Now;
                        userInfo.UpdatedOn = DateTime.Now;
                        userInfo.AnalysisId = 0;
                        userInfo.UpdatedBy = customer.CustomerId.ToString();
                        userInfo.Password = Common.Encrypt(password);
                        userInfo.SecurityKey = Guid.NewGuid().ToString();

                        utility.UserRepository.Insert(userInfo);
                        utility.UserRepository.Save();
                        Logger.Log.Info(string.Format("Home - User account created successfully"));
                        if (plan.PlanTypeId <= (int)PlanType.BP)
                        {
                            SendEmail(customer, (int)EmailType.NewSubscriptionSignup);
                        }

                        if (plan.PlanTypeId <= (int)PlanType.BP)
                        {

                            SendEmail(userInfo, (int)EmailType.NewUserSignUp, null, null);
                        }
                        else
                        {
                            SendEmail(userInfo, (int)EmailType.NewUserAddedForBusiness, numberOfAddOn, customer);
                        }
                        ViewBag.SubscriptionInfo = customer;
                        if (!string.IsNullOrEmpty(billingInfoFromRecurly.LastFour))
                        {
                            ViewBag.CreditCardLastFourDigit = billingInfoFromRecurly.LastFour.ToString();
                            Logger.Log.Info("Last four digit is" + billingInfoFromRecurly.LastFour.ToString());
                        }
                        if (billingInfoFromRecurly.CardType != BillingInfo.CreditCardType.Unknown && billingInfoFromRecurly.CardType != BillingInfo.CreditCardType.Invalid)
                        {
                            ViewBag.CreditCardType = billingInfoFromRecurly.CardType.ToString();
                            Logger.Log.Info("Credit Card Type is" + billingInfoFromRecurly.CardType.ToString());
                        }
                    }
                    else
                    {
                        TempData["ErrorMessage"] = string.Concat("Account Code is missing! Please try again to do subscription.");
                        Logger.Log.Error(string.Format("Unable to Create Customer :- Account Code is missing."));
                    }

                }
                else
                {
                    TempData["ErrorMessage"] = string.Concat("Account Code is missing! Please try again to do subscription.");
                    Logger.Log.Error(string.Format("Unable to Create Customer :- Account Code is missing."));
                }
                Logger.Log.Info(string.Format("Subscribe Receipt Function Executied"));

            }
            catch (DbEntityValidationException dbEntityEx)
            {
                Logger.Log.Info(string.Format("Home - Username {0}", username));

                foreach (var sError in dbEntityEx.EntityValidationErrors)
                {

                    Logger.Log.Error(string.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:", sError.Entry.Entity, sError.Entry.State));
                    foreach (var ve in sError.ValidationErrors)
                    {
                        Logger.Log.Error(string.Format("-Property: {0}, Error: {1} ", ve.PropertyName, ve.ErrorMessage));
                    }
                }
            }
            catch (DataException dataEx)
            {
                TempData["ErrorMessage"] = string.Concat("Unable to create customer-", dataEx.Message);
                Logger.Log.Error(string.Format("Unable to Create Customer id- {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2} ", customer.CustomerId, dataEx.Message, dataEx.StackTrace));
            }
            catch (Exception ex)
            {
                TempData["ErrorMessage"] = string.Concat("Unable to create customer-", ex.Message);
                Logger.Log.Error(string.Format("Unable to Create Customer id- {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2} ", customer.CustomerId, ex.Message, ex.StackTrace));

            }
            finally
            {
                Logger.Log.Info(string.Format("Home - Subscription Execution Ended"));
            }
            return View("Receipt", customer);
        }

        #region oldcode
        //public ActionResult SubscribeOld(int selectedPlan)
        //{

        //    try
        //    {
        //        dynamic subscriptionPlan = utility.SubscriptionPlanRepository.GetSubscriptionPlan(selectedPlan);

        //        string subscription = string.Format("{0}={1}", "subscription%5Bplan_code%5D", subscriptionPlan.Plan);


        //        ViewBag.Signature = PlanguruSubscription.DAL.Utility.SignWithParameters(subscription);
        //        ViewBag.AccountCode = PlanguruSubscription.DAL.Utility.CheckAccountCodeOnRecurly((utility.CustomerRepository.GetCustomer().Max(c => c.CustomerId)) + 1);
        //        ViewBag.Plan = subscriptionPlan.Plan.ToString();

        //        TempData["AccountCode"] = ViewBag.AccountCode.ToString();
        //        Logger.Log.Info(string.Format("Subscribe Execution Done"));
        //        return View();
        //    }
        //    catch (DataException dataEx)
        //    {
        //        TempData["ErrorMessage"] = string.Concat("Error occured during loading subscription plan -", dataEx.Message);
        //        Logger.Log.Error(string.Format("Error occured during loading subscription plan - {0} " + Environment.NewLine + "Stack Trace: {1} ", dataEx.Message, dataEx.StackTrace));
        //        return View();
        //    }
        //    catch (Exception ex)
        //    {
        //        TempData["ErrorMessage"] = string.Concat("Unable to Subscribe()-", ex.Message);
        //        Logger.Log.Error(string.Format("\\n Unable to Subscribe() with Message- {0} " + Environment.NewLine + "Stack Trace: {1} ", ex.Message, ex.StackTrace));
        //        return View();
        //    }
        //    finally
        //    {
        //        Logger.Log.Info(string.Format("Subscribe() Execution Ended"));
        //    }

        //}


        //public ViewResult Receipt()
        //{
        //    Customer customer = null;
        //    int AccountCode = 0;
        //    int newUserAccountCode = 0;
        //    User userInfo = null;
        //    string username = string.Empty;
        //    int licensecustomerid = 0;
        //    SubscriptionPlan plan = null;
        //    XmlNode node = null;
        //    try
        //    {
        //        Logger.Log.Info(string.Format("Home - Subscription Execution Started"));

        //        if (((TempData["AccountCode"] != null)))
        //        {
        //            AccountCode = Convert.ToInt32(TempData["AccountCode"]);

        //        }

        //        if (RecurlyAccount.IsExist(AccountCode.ToString()) == true)
        //        {
        //            RecurlySubscription subscription = RecurlySubscription.Get(AccountCode.ToString());
        //            Logger.Log.Info("Successfully get Recurly Account Code from Recurly API,Account Id is =" + subscription.Account.AccountCode + "Status is = " + subscription.State);
        //            int numberOfAddOn = subscription.AddOn.Addon == null ? 0 : subscription.AddOn.Addon.quantity.Value;

        //            if ((subscription != null))
        //            {
        //                RecurlyAccount accountInfo = RecurlyAccount.Get(AccountCode.ToString());
        //                Logger.Log.Info("Account Information is successfully submitted in Recurly for Account Id =" + accountInfo.AccountCode + " and Status is = " + accountInfo.State);
        //                RecurlyBillingInfo billingInfo = RecurlyBillingInfo.Get(AccountCode.ToString());
        //                Logger.Log.Info("Billing Information is successfully submitted in Recurly for Account Id =" + billingInfo.AccountCode);
        //                plan = utility.SubscriptionPlanRepository.GetSubscriptionPlan(subscription.PlanCode);

        //                if (plan.PlanTypeId != (int)PlanType.BP && plan.PlanTypeId != (int)PlanType.PPU)
        //                {

        //                    if (accountInfo != null && billingInfo != null)
        //                    {
        //                        customerserviceprovider = new CustomerServiceProvider();
        //                        licenseprovider = new XMLlisenceProvider();
        //                        Logger.Log.Info(String.Format("Customer License registration is started for account code=" + accountInfo.AccountCode));
        //                        licensecustomerid = customerserviceprovider.CreateCustomer(accountInfo, Common.Generate(8, 15, true), billingInfo);
        //                        if (licensecustomerid < 0)
        //                        {
        //                            RecurlyAccount.CloseAccount(AccountCode.ToString());
        //                            RecurlySubscription.TerminateSubscription(AccountCode.ToString());
        //                            Logger.Log.Error(string.Format("can't get response from create customer method the response text is =" + licensecustomerid));
        //                            TempData["ErrorMessage"] = "cant create customer"; //need to ask
        //                        }
        //                        Logger.Log.Info(string.Format("create customer method creates customer with licensecustomerid" + licensecustomerid));
        //                    }

        //                    if (licensecustomerid > 0)
        //                    {
        //                        bool isAccountantAdvisor = false;
        //                        Logger.Log.Info(string.Concat("xml add user service started"));
        //                        if (plan.PlanTypeId == (int)PlanType.AAM || plan.PlanTypeId == (int)PlanType.AAY)
        //                        {
        //                            node = licenseprovider.AddLicenseUser(licensecustomerid, plan.ProductOptionId.Value, (numberOfAddOn + 5), plan.Cost_of_Subscription);
        //                            isAccountantAdvisor = true;
        //                        }
        //                        else
        //                        {
        //                            node = licenseprovider.AddLicenseUser(licensecustomerid, plan.ProductOptionId.Value, (numberOfAddOn + 1), plan.Cost_of_Subscription);
        //                            isAccountantAdvisor = false;
        //                        }
        //                        Logger.Log.Info("xml add license user service invoked successfully for account id =" + accountInfo.AccountCode);

        //                        if (node != null)
        //                        {
        //                            Logger.Log.Info("xml license add user method successfully invoked with response = " + node.InnerXml);
        //                            XmlNode updateUserDefinedFieldResponse = licenseprovider.UpdateUserDefinedFields(Convert.ToInt32(node.ChildNodes[1].InnerText), AccountCode.ToString(), plan.PlanTypeId.ToString(), isAccountantAdvisor);
        //                            Logger.Log.Info("xml updateuserdefinedfieldresponse successfully invoked with response = " + updateUserDefinedFieldResponse.InnerXml);
        //                        }
        //                    }
        //                }


        //                customer = new Customer();
        //                customer.CustomerId = AccountCode;
        //                customer.PlanTypeId = utility.SubscriptionPlanRepository.GetSubscriptionPlan(subscription.PlanCode).PlanTypeId;
        //                customer.Quantity = subscription.Quantity.Value;
        //                customer.CustomerFirstName = accountInfo.FirstName;
        //                customer.CustomerLastName = accountInfo.LastName;
        //                customer.CustomerEmail = accountInfo.Email;
        //                customer.CustomerCompanyName = accountInfo.CompanyName;

        //                //******Comment below code because of PCI compliance ***********
        //                customer.ContactFirstName = string.Empty;
        //                customer.ContactLastName = string.Empty;
        //                //Credit Card Information                       
        //                customer.FirstNameOnCard = string.Empty;
        //                //billingInfo.FirstName
        //                customer.LastNameOnCard = string.Empty;
        //                //billingInfo.LastName
        //                customer.CreditCardNumber = string.Empty;
        //                //String.Concat("XXXX-XXXX-XXXX-", billingInfo.CreditCard.LastFour)
        //                customer.CVV = string.Empty;
        //                //billingInfo.VerificationValue
        //                customer.ExpirationMonth = 0;
        //                //billingInfo.CreditCard.ExpirationMonth
        //                customer.ExpirationYear = 0;
        //                //billingInfo.CreditCard.ExpirationYear
        //                customer.CustomerAddress1 = string.Empty;
        //                //billingInfo.Address1
        //                customer.CustomerAddress2 = string.Empty;
        //                //billingInfo.Address2

        //                //******Comment below code because of PCI compliance ***********
        //                //If (billingInfo.PostalCode.Length > 10) Then
        //                //    customer.CustomerPostalCode = billingInfo.PostalCode.ToString().Substring(0, 10)
        //                //Else
        //                //    customer.CustomerPostalCode = billingInfo.PostalCode
        //                //End If
        //                customer.CustomerPostalCode = string.Empty;

        //                if ((billingInfo.PhoneNumber.Length > 15))
        //                {
        //                    customer.ContactTelephone = billingInfo.PhoneNumber.ToString().Substring(0, 15);
        //                }
        //                else
        //                {
        //                    customer.ContactTelephone = billingInfo.PhoneNumber;
        //                }

        //                customer.Country = billingInfo.Country;
        //                customer.State = string.Empty;
        //                //billingInfo.State
        //                customer.City = string.Empty;
        //                //billingInfo.City

        //                customer.SubscriptionStartAt = Convert.ToDateTime(subscription.CurrentPeriodStartedAt).ToString("MMM dd, yyyy");
        //                customer.SubscriptionEndAt = Convert.ToDateTime(subscription.CurrentPeriodEndsAt).ToString("MMM dd, yyyy");
        //                customer.SubscriptionAmount = (Convert.ToDouble(subscription.UnitAmountInCents) / 100);
        //                customer.CreatedOn = DateTime.Now;
        //                customer.CreatedBy = customer.CustomerId.ToString();
        //                customer.UpdatedOn = DateTime.Now;
        //                customer.UpdatedBy = customer.CustomerId.ToString();
        //                customer.LogoImageFile = "";
        //                customer.PrimaryColor = "";
        //                customer.SecondaryColor = "";
        //                customer.FootNote = "";
        //                customer.LicenseCustomerId = licensecustomerid;
        //                if (node != null)
        //                {
        //                    customer.ResultCode = Convert.ToInt32(node.ChildNodes[0].InnerText);
        //                    customer.LicenseId = Convert.ToInt32(node.ChildNodes[1].InnerText);
        //                    customer.Password = node.ChildNodes[2].InnerText;
        //                    customer.SerialNumber = node.ChildNodes[3].InnerText;
        //                    customer.ActivationPassword = node.ChildNodes[4].InnerText;
        //                    customer.AddUserResponse = node.InnerXml;
        //                    if (subscription.AddOn.Addon == null)
        //                    {
        //                        if (plan.PlanTypeId == (int)PlanType.AAM || plan.PlanTypeId == (int)PlanType.AAY)
        //                        {
        //                            customer.NumberOfAnalyticsAddOn = 2;
        //                        }
        //                        else
        //                        {
        //                            customer.NumberOfAnalyticsAddOn = 0;
        //                        }
        //                        customer.NumberOfAddOns = 0;

        //                    }
        //                    else
        //                    {
        //                        if (plan.PlanTypeId == (int)PlanType.AAM || plan.PlanTypeId == (int)PlanType.AAY)
        //                        {
        //                            customer.NumberOfAnalyticsAddOn = 2 + (subscription.AddOn.Addon.quantity.Value);
        //                        }
        //                        else
        //                        {
        //                            customer.NumberOfAnalyticsAddOn = subscription.AddOn.Addon.quantity.Value;
        //                        }
        //                        customer.NumberOfAddOns = subscription.AddOn.Addon.quantity.Value;

        //                    }
        //                }

        //                utility.CustomerRepository.InsertCustomer(customer);
        //                utility.CustomerRepository.Save();
        //                Logger.Log.Info(string.Format("Home - Customer account created successfully"));
        //                TempData["Message"] = "Subscription created successfully.";

        //                newUserAccountCode = (utility.UserRepository.GetUsers().Max(c => c.UserId) + 1);

        //                //SAU user account creation

        //                username = PlanguruSubscription.DAL.Utility.AutoGenerateUserName();
        //                string password = Common.Generate(8, 2, false);

        //                Logger.Log.Info(string.Format("Home - Username {0} , Password {1}", username, password));

        //                userInfo = new User();
        //                userInfo.UserId = newUserAccountCode;
        //                userInfo.CustomerId = customer.CustomerId;
        //                userInfo.UserName = username;
        //                userInfo.UserRoleId = (int)UserRoles.SAU;
        //                userInfo.FirstName = accountInfo.FirstName;
        //                userInfo.LastName = accountInfo.LastName;
        //                userInfo.UserEmail = accountInfo.Email;
        //                userInfo.Status = Convert.ToInt32(StatusE.Pending);
        //                userInfo.CreatedBy = customer.CustomerId.ToString();
        //                userInfo.CreatedOn = DateTime.Now;
        //                userInfo.UpdatedOn = DateTime.Now;
        //                userInfo.AnalysisId = 0;
        //                userInfo.UpdatedBy = customer.CustomerId.ToString();
        //                userInfo.Password = Common.Encrypt(password);
        //                userInfo.SecurityKey = Guid.NewGuid().ToString();

        //                utility.UserRepository.Insert(userInfo);
        //                utility.UserRepository.Save();
        //                Logger.Log.Info(string.Format("Home - User account created successfully"));
        //                if (plan.PlanTypeId <= (int)PlanType.BP)
        //                {
        //                    SendEmail(customer, (int)EmailType.NewSubscriptionSignup);
        //                }

        //                if (plan.PlanTypeId <= (int)PlanType.BP)
        //                {

        //                    SendEmail(userInfo, (int)EmailType.NewUserSignUp, null, null);
        //                }
        //                else
        //                {
        //                    SendEmail(userInfo, (int)EmailType.NewUserAddedForBusiness, numberOfAddOn, customer);
        //                }
        //                ViewBag.SubscriptionInfo = customer;
        //                ViewBag.CreditCardLastFourDigit = billingInfo.CreditCard.LastFour.ToString();
        //                ViewBag.CreditCardType = billingInfo.CreditCard.CreditCardType.ToString();
        //            }
        //            else
        //            {
        //                TempData["ErrorMessage"] = string.Concat("Account Code is missing !");
        //                Logger.Log.Error(string.Format("Unable to Create Customer :- Account Code is missing."));
        //            }

        //        }
        //        Logger.Log.Info(string.Format("Subscribe Receipt Function Executied"));

        //    }
        //    catch (DbEntityValidationException dbEntityEx)
        //    {
        //        Logger.Log.Info(string.Format("Home - Username {0}", username));

        //        foreach (var sError in dbEntityEx.EntityValidationErrors)
        //        {

        //            Logger.Log.Error(string.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:", sError.Entry.Entity, sError.Entry.State));
        //            foreach (var ve in sError.ValidationErrors)
        //            {
        //                Logger.Log.Error(string.Format("-Property: {0}, Error: {1} ", ve.PropertyName, ve.ErrorMessage));
        //            }
        //        }
        //    }
        //    catch (DataException dataEx)
        //    {
        //        TempData["ErrorMessage"] = string.Concat("Unable to create customer-", dataEx.Message);
        //        Logger.Log.Error(string.Format("Unable to Create Customer id- {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2} ", AccountCode, dataEx.Message, dataEx.StackTrace));
        //    }
        //    catch (Exception ex)
        //    {
        //        TempData["ErrorMessage"] = string.Concat("Unable to create customer-", ex.Message);
        //        Logger.Log.Error(string.Format("Unable to Create Customer id- {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2} ", AccountCode, ex.Message, ex.StackTrace));

        //    }
        //    finally
        //    {
        //        Logger.Log.Info(string.Format("Home - Subscription Execution Ended"));
        //    }
        //    return View(customer);

        //}
        #endregion
        private void SendEmail(Customer customer, int emailId)
        {
            Logger.Log.Info(string.Format("Customer SendEmail Execution Started"));
            try
            {
                Logger.Log.Info(string.Format("Trial sign-up confirmation - SendEmail Executed"));
                var objEmailInfo = utility.EmailRepository.GetEmailInfoById(emailId);
                objEmailInfo.EmailBody = Common.MergeCustomerEmailBody(customer, objEmailInfo);
                Common.SendCustomerEmail(customer, objEmailInfo);
                Logger.Log.Info(string.Format("Trial sign-up confirmation - SendEmail Ended"));
            }
            catch (Exception ex)
            {
                TempData["ErrorMessage"] = string.Concat("Unable to Send Email -", ex.Message);
                Logger.Log.Error(string.Format("Unable to Send Email to CustomerID - {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2} ", customer.CustomerId, ex.Message, ex.StackTrace));
            }
            finally
            {
                Logger.Log.Info(string.Format("Customer SendEmail Execution Ended"));
            }
        }

        private void SendEmail(User User, int emailId, int? AddOnUserCount, Customer customerInfo)
        {
            Logger.Log.Info(string.Format("User SendEmail Execution Started"));
            string planGuruUrl = string.Empty;
            try
            {
                Logger.Log.Info("You have been added as a user - SendEmail Started");

                if (System.Configuration.ConfigurationManager.AppSettings["IsProduction"] == "true")
                {
                    planGuruUrl = System.Configuration.ConfigurationManager.AppSettings["PlanGuruProductionUrl"];
                }
                else
                {
                    if (Request.Url.Port != 80)
                    {
                        planGuruUrl = "http://" + Common.HostName(Request) + "/";
                    }
                    else { planGuruUrl = "http://" + Common.HostName(Request) + (Request.Url.IsDefaultPort ? Request.ApplicationPath + "/" : ":" + Request.Url.Port.ToString() + "/"); }
                }
                dynamic objEmailInfo = utility.EmailRepository.GetEmailInfoById(emailId);
                objEmailInfo.EmailBody = Common.MergeUserEmailBody(User, objEmailInfo, planGuruUrl, AddOnUserCount, customerInfo);
                Common.SendUserEmail(User, objEmailInfo);
                Logger.Log.Info("You have been added as a user - SendEmail Ended");
                Logger.Log.Info(string.Format("Email Sent Successfully to User: {0} for EmailId: {1}", User.UserId, emailId));
            }
            catch (Exception ex)
            {
                TempData["ErrorMessage"] = string.Concat("Unable to Send Email -", ex.Message);
                Logger.Log.Error(string.Format("Unable to Send Email, UserId - {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2} ", User.UserId, ex.Message, ex.StackTrace));
            }
            finally
            {
                Logger.Log.Info(string.Format("User SendEmail Execution Ended"));
            }
        }

    }
}
