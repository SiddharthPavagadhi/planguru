﻿using PlanguruSubscription.DAL;
using PlanguruSubscription.Models;
using PlanguruSubscription.Modules;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
//using Recurly;
using System.Globalization;

namespace PlanguruSubscription.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/
        private Utility utility = new Utility();

       

        public ActionResult Index()
        {
            Logger.Log.Info("Index Function Execution Started");
            Customer result = null;
            string subdomain = null;
            string[] UrlParams = null;
            try
            {
                TempData.Clear();
                Logger.Log.Info(string.Format("Request.Url: {0}", Request.Url.ToString().ToLower()));
                UrlParams = Request.Url.ToString().Replace("https://", "").Split('.');

                Logger.Log.Info(string.Format("Url Param(0):{0}, Param(1):{1}", UrlParams[0], UrlParams[1]));

                if (!(UrlParams[0].Equals("www") & UrlParams[0].Equals("planguruanalytics")))
                {
                    subdomain = GetSubDomain(Request.Url);
                    Logger.Log.Info(string.Format("subdomain:{0}", subdomain));
                }

                result = (from c in utility.CustomerRepository.GetCustomer() where c.PlanTypeId == (int)PlanType.BP & c.VanityFirmName == subdomain select c).SingleOrDefault();


                if (result != null)
                {
                    ViewBag.Logo = result.LogoImageFile;

                    Logger.Log.Info(string.Format("CustomerId:{0}, VanityFirmName:{1}, Logo:{2}", result.CustomerId, result.VanityFirmName, result.LogoImageFile));


                }
                else
                {
                    Logger.Log.Info(string.Format("Doesn't find such vanity firm name requested: {0} ", Request.Url.ToString()));
                    ViewBag.Logo = string.Empty;

                }

                if (result != null)
                {
                    return View("Login");
                }
                else
                {
                    return View();
                }


            }
            catch (Exception indexException)
            {
                if (result != null)
                {
                    Logger.Log.Error(string.Format("Error occured while match subdomain name : {0} - " + Environment.NewLine + "{1}, Stack Trace: {2} ", Request.Url.ToString(), indexException.Message, indexException.StackTrace));
                }
                else
                {
                    Logger.Log.Error(string.Format("Error occured while match subdomain name : {0} Stack Trace: {1} ", indexException.Message, indexException.StackTrace));
                }
                return View();
            }
            finally
            {
                Logger.Log.Info(string.Format("Index Function Execution Ended"));
            }

        }

        #region "ForgotUsername Methods"
        [HttpGet()]
        public ActionResult ForgotUsername()
        {
            try
            {
                TempData.Clear();
            }
            catch (Exception ex)
            {
                TempData["ErrorMessage"] = "Error occured on forgot username form -" + ex.Message.ToString();
                Logger.Log.Error(string.Format("Unable to Reset the username for " + Environment.NewLine + "{0}, Stack Trace: {1} ", ex.Message, ex.StackTrace));
            }
            return View();
        }

        [HttpPost()]
        public ActionResult ForgotUsername(ForgotUsername model)
        {
            Logger.Log.Info(string.Format("ForgotUsername Function Execution Started"));
            User result = null;
            try
            {
                TempData.Clear();
                if (ModelState.IsValid)
                {
                    result = utility.UserRepository.GetUsers().Where(u => u.UserEmail == model.emailAddress).SingleOrDefault();

                    if (((result != null)))
                    {
                        if ((SendMail_ForgotUserName(result, (int)EmailType.ForgotUsername)))
                        {
                            TempData["Message"] = "Email has been sent Successfully.";
                            Logger.Log.Info(string.Format("ForgotUsername Reset Successfully of UserId : {0}", result.UserId));
                        }
                    }
                    else
                    {
                        TempData["InfoMessage"] = "No user with the Email address entered was found.";
                    }
                }
            }
            catch (Exception ex)
            {
                TempData["ErrorMessage"] = string.Concat("There were no matching Planguru accounts found with the information you provided.-", ex.Message);
                if (((result != null)))
                {
                    Logger.Log.Error(string.Format("Error occured while Reset username for User : {0} - " + Environment.NewLine + "{1}, Stack Trace: {2} ", result.UserName, ex.Message, ex.StackTrace));
                }
                else
                {
                    Logger.Log.Error(string.Format("Error occured while Reset username - {0} Stack Trace: {1} ", ex.Message, ex.StackTrace));
                }
            }
            finally
            {
                Logger.Log.Info(string.Format("ForgotUsername Function Execution Ended"));
            }

            return View();
        }

        private bool SendMail_ForgotUserName(User User, int emailId)
        {
            Logger.Log.Info(string.Format("User SendEmail Execution Started"));
            bool IsMailTrigger = true;
            try
            {
                Logger.Log.Info("You have been added as a user - SendMail_ForgotUserName Started");

                dynamic objEmailInfo = utility.EmailRepository.GetEmailInfoById(emailId);
                objEmailInfo.EmailBody = Common.MergeUserEmailBody(User, objEmailInfo, null,null,null);
                Common.SendUserEmail(User, objEmailInfo);
                Logger.Log.Info("You have been added as a user - SendEmail Ended");
                Logger.Log.Info(string.Format("Email Sent Successfully to User: {0} for EmailId: {1}", User.UserId, emailId));
            }
            catch (Exception ex)
            {
                IsMailTrigger = false;
                TempData["ErrorMessage"] = string.Concat("Unable to Send Email -", ex.Message);
                Logger.Log.Error(string.Format("Unable to Send Email, UserId - {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2} ", User.UserId, ex.Message, ex.StackTrace));
            }
            finally
            {
                Logger.Log.Info(string.Format("SendMail_ForgotUserName Execution Ended"));
            }
            return IsMailTrigger;
        }
        #endregion

        #region "ForgotPassword Methods"

        [HttpGet()]
        public ActionResult ForgotPassword()
        {
            try
            {
                TempData.Clear();
            }
            catch (Exception ex)
            {
                TempData["ErrorMessage"] = "Error occured on forgot password form - " + ex.Message.ToString();
                Logger.Log.Error(string.Format("Unable to Reset the password for " + Environment.NewLine + "{0}, Stack Trace: {1} ", ex.Message, ex.StackTrace));
            }
            return View();
        }

        public ActionResult ForgotPassword(ForgotPassword userInput)
        {
            Logger.Log.Info(string.Format("ForgotPassword Execution Started"));
            User userDetails = null;
            try
            {
                TempData.Clear();

                if (ModelState.IsValid)
                {
                    userDetails = utility.UserRepository.GetUsers().Where(u => u.UserName == userInput.userName & u.UserEmail == userInput.emailAddress).SingleOrDefault();

                    if (((userDetails != null)))
                    {
                        //userDetails = New User()
                        string password = Common.Generate(8, 2,false);
                        userDetails.Password = Common.Encrypt(password);
                        userDetails.SecurityKey = Guid.NewGuid().ToString();
                        userDetails.UpdatedBy = userDetails.UserId.ToString();
                        userDetails.UpdatedOn = DateTime.UtcNow;
                        utility.UserRepository.UpdateUser(userDetails);
                        utility.UserRepository.Save();

                        SendEmail(userDetails, (int)EmailType.PasswordReset);
                        if ((TempData["ErrorMessage"] == null))
                        {
                            TempData["Message"] = "Reset Password link has been sent to your Email Id Successfully.";
                        }
                    }
                    else
                    {
                        TempData["InfoMessage"] = "No user with the Username and Email address entered was found.";
                    }
                }
            }
            catch (DbEntityValidationException dbEntityEx)
            {
                TempData["ErrorMessage"] = "Error occured, during reset password request - " + dbEntityEx.Message.ToString();
                foreach (var sError in dbEntityEx.EntityValidationErrors)
                {

                    Logger.Log.Error(string.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:", sError.Entry.Entity, sError.Entry.State));
                    foreach (var ve in sError.ValidationErrors)
                    {
                        Logger.Log.Error(string.Format("-Property: {0}, Error: {1} ", ve.PropertyName, ve.ErrorMessage));
                    }
                }
            }
            catch (Exception ex)
            {
                TempData["ErrorMessage"] = "Error occured, while reset password - " + ex.Message.ToString();
                if (((userDetails != null)))
                {
                    Logger.Log.Error(string.Format("Unable to Reset password for User : {0} - " + Environment.NewLine + "{1}, Stack Trace: {2} ", userDetails.UserName, ex.Message, ex.StackTrace));
                }
                else
                {
                    Logger.Log.Error(string.Format("Unable to Reset password - {0} Stack Trace: {1} ", ex.Message, ex.StackTrace));
                }

            }
            finally
            {
                Logger.Log.Info(string.Format("ForgotPassword Execution Ended"));
            }
            return View();
        }
        #endregion
        private void SendEmail(User User, int emailId)
        {
            Logger.Log.Info(string.Format("User SendEmail Execution Started"));
            try
            {
                Logger.Log.Info("You have been added as a user - SendEmail Started");
                string PlanGuruUrl = "http://" + Common.HostName(Request) + (Request.Url.IsDefaultPort ? Request.ApplicationPath + "/" : ":" + Request.Url.Port.ToString() + "/");
                dynamic objEmailInfo = utility.EmailRepository.GetEmailInfoById(emailId);
                objEmailInfo.EmailBody = Common.MergeUserEmailBody(User, objEmailInfo, PlanGuruUrl,null,null);
                Common.SendUserEmail(User, objEmailInfo); 
                Logger.Log.Info("You have been added as a user - SendEmail Ended");
                Logger.Log.Info(string.Format("Email Sent Successfully to User: {0} for EmailId: {1}", User.UserId, emailId));
            }
            catch (Exception ex)
            {
                TempData["ErrorMessage"] = string.Concat("Unable to Send Email -", ex.Message);
                Logger.Log.Error(string.Format("Unable to Send Email, UserId - {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2} ", User.UserId, ex.Message, ex.StackTrace));
            }
            finally
            {
                Logger.Log.Info(string.Format("User SendEmail Execution Ended"));
            }
        }




        private static string GetSubDomain(Uri url)
        {

            if (url.HostNameType == UriHostNameType.Dns)
            {
                string host = url.Host;
                if (host.Split('.').Length > 2)
                {
                    int lastIndex = host.LastIndexOf(".");
                    int index = host.LastIndexOf(".", lastIndex - 1);
                    return host.Substring(0, index);
                }
            }

            return null;
        }

    }
}
