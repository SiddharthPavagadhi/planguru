﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace PlanguruSubscription.Models
{
    [Table("UserRole")]
    public class UserRole
    {
        [Key()]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int UserRoleId { get; set; }
        [StringLength(50)]
        public string RoleName { get; set; }
    }


    [Table("UserRolePermission")]
    public class UserRolePermission
    {
        [Key()]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int UserRolePermissionId { get; set; }
        [StringLength(200)]
        public string Description { get; set; }
    }

    [Table("UserRolePermissionMapping")]
    public class UserRolePermissionMapping
    {
        [Key()]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int UserRolePermissionMappingId { get; set; }

        public int UserRoleId { get; set; }

        public int UserRolePermissionId { get; set; }

        public string CreatedBy { get; set; }

        public DateTime CreatedOn { get; set; }

        public string UpdatedBy { get; set; }

        public DateTime UpdatedOn { get; set; }

        public virtual UserRole UserRole { get; set; }
        public virtual UserRolePermission UserRolePermission { get; set; }
    }

    [Table("EmailInfo")]
    public class EmailInfo
    {

        public int Id { get; set; }
        public string EmailType { get; set; }
        public string EmailSubject { get; set; }
        public string EmailBody { get; set; }
        public DateTime CreatedDateTime { get; set; }
    }

    [Table("ChartFormat")]
    public class ChartFormat
    {
        [Key()]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int ChartFormatId { get; set; }
        public string Format { get; set; }
    }

    [Table("ChartType")]
    public class ChartType
    {
        [Key()]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int ChartTypeId { get; set; }
        public string Type { get; set; }
    }


    public enum UserRoles : int
    {
        PGAAdmin = 1,
        PGASupport = 2,
        SAU = 3,
        CAU = 4,
        CRU = 5,
        SSU = 6
    }



}