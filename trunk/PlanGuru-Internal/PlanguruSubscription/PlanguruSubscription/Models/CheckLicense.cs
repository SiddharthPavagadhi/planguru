﻿using LicenceService;
using PlanguruSubscription.DAL;
using PlanguruSubscription.Modules;
//using Recurly;
using RecurlyNew;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Xml;
using RecurlyNew;

namespace PlanguruSubscription.Models
{
    public class CheckLicense
    {
        public XmlNode TestLicenseProvider(int AccountCode)
        {
            int licensecustomerid = 0;
            SubscriptionPlan plan = new SubscriptionPlan();
            Utility utility = new Utility();
            CustomerServiceProvider customerserviceprovider = new CustomerServiceProvider();
            XMLlisenceProvider licenseprovider = new XMLlisenceProvider();
            XmlNode updateUserDefinedFieldResponse = null;
            if (RecurlyNew.Account.IsExist(AccountCode.ToString()) == true)
            {
                //RecurlySubscription subscription = RecurlySubscription.Get(AccountCode.ToString());
                RecurlyNew.Account acc = RecurlyNew.Accounts.Get(AccountCode.ToString());
                var subscription = acc.GetSubscriptions(RecurlyNew.Subscription.SubscriptionState.Active).FirstOrDefault();

                if ((subscription != null))
                {
                    //RecurlyAccount accountInfo = RecurlyAccount.Get(AccountCode.ToString());
                    //RecurlyBillingInfo billingInfo = RecurlyBillingInfo.Get(AccountCode.ToString());
                    RecurlyNew.Account accountInfo = RecurlyNew.Accounts.Get(AccountCode.ToString());
                    RecurlyNew.BillingInfo billinginfo = RecurlyNew.BillingInfo.Get(AccountCode.ToString());
                    if (accountInfo != null)
                    {
                        licensecustomerid = customerserviceprovider.CreateCustomer(accountInfo, Common.Generate(8, 15, true), billinginfo);
                        if (licensecustomerid < 0)
                        {
                            RecurlyNew.Accounts.Close(AccountCode.ToString());
                            subscription.Terminate(RecurlyNew.Subscription.RefundType.None);

                        }

                    }

                    if (licensecustomerid > 0)
                    {
                        plan = utility.SubscriptionPlanRepository.GetSubscriptionPlan(subscription.PlanCode);
                        XmlNode node = licenseprovider.AddLicenseUser(licensecustomerid, 1250, 1, 99);
                        if (node != null)
                        {
                            Logger.Log.Info("xml license add user method successfully invoked with response = " + node.ToString());
                            updateUserDefinedFieldResponse = licenseprovider.UpdateUserDefinedFields(Convert.ToInt32(node.ChildNodes[1].InnerText), AccountCode.ToString(), plan.PlanTypeId.ToString(), false);

                        }

                    }
                }
            }
            return updateUserDefinedFieldResponse;
        }

        public string CheckMailBody()
        {
            string mergeContent = string.Empty;
            var path = System.Web.HttpContext.Current.Server.MapPath("../MailInfo/DynamicContent.html");
            for (var i = 1; i <= 5; i++)
            {
                string DynamicContent = string.Empty;
                using (var reader = new StreamReader(path))
                {
                    DynamicContent = reader.ReadToEnd();
                }
                DynamicContent.Replace("##UserCount##", i.ToString());
                DynamicContent.Replace("##LICENSEID##", "123456");
                DynamicContent.Replace("##LICENSEPASSWORD##", "123456df");
                mergeContent = mergeContent + DynamicContent;
            }
            return mergeContent;
        }
    }
}