﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace PlanguruSubscription.Models
{
    public class Analysis
    {
        public int AnalysisId { get; set; }

        [Required(ErrorMessage = "Analysis Name is required.")]
        [MaxLength(50)]
        [Display(Name = "Analysis Name")]
        public string AnalysisName { get; set; }

        [Required(ErrorMessage = "Select Company")]
        [Display(Name = "Company")]
        public int CompanyId { get; set; }

        [NotMapped()]
        public string CompanyName { get; set; }

        [NotMapped()]
        public IList<User_> AvailableUsers { get; set; }

        [NotMapped()]
        public IList<User_> SelectedUsers { get; set; }

        [NotMapped()]
        public PostedUsers PostedUsers { get; set; }

        public bool ShowBS { get; set; }

        public bool ShowRatios { get; set; }

        public bool Option1 { get; set; }

        public int? FirstYear { get; set; }

        public int? NumberofPeriods { get; set; }

        public string CreatedBy { get; set; }

        public DateTime CreatedOn { get; set; }

        public string UpdatedBy { get; set; }

        public DateTime UpdatedOn { get; set; }

        public virtual Company Company { get; set; }

    }
}