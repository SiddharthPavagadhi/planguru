﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace PlanguruSubscription.Models
{
    public class Dashboard
    {
        public int DashboardId { get; set; }

        public int Order { get; set; }

        [StringLength(50)]
        public string DashDescription { get; set; }

        [Required(ErrorMessage = "Select Chart.")]
        [Display(Name = "Format")]
        public int ChartFormatId { get; set; }

        [Required(ErrorMessage = "Select Chart Type.")]
        [Display(Name = "Chart Type")]
        public int ChartTypeId { get; set; }

        [Display(Name = "Show As Percent")]
        public bool ShowAsPercent { get; set; }

        [Display(Name = "Show Trend")]
        public bool ShowTrendline { get; set; }

        [Display(Name = "Show Goal")]
        public bool ShowGoal { get; set; }

        [Display(Name = "Show Budget")]
        public bool ShowBudget { get; set; }

        //<RegularExpression("^-?[0-9]+(\.[0-9]{1,5})?$")>
        [Display(Name = "Goal")]
        public decimal Period1Goal { get; set; }

        //<RegularExpression("^-?[0-9]+(\.[0-9]{1,5})?$")>
        [Display(Name = "Goal Growth Rate")]
        public decimal GoalGrowthRate { get; set; }

        [RegularExpression("^[1-9]+[0-9]*$", ErrorMessage = "Please select the item to be listed in the dashboard.")]
        [Required(ErrorMessage = "Please select the item to be listed in the dashboard.")]
        public int AccountId { get; set; }

        [NotMapped()]
        public int AcctTypeId { get; set; }
        public int UserId { get; set; }
        public int AnalysisId { get; set; }



        public int Option1 { get; set; }

        public int Option2 { get; set; }

        public int Option3 { get; set; }

        public bool IsValid { get; set; }

        public string CreatedBy { get; set; }

        public DateTime CreatedOn { get; set; }

        public string UpdatedBy { get; set; }

        public DateTime UpdatedOn { get; set; }

        [NotMapped()]
        public string selectedCheckboxId { get; set; }
        [NotMapped()]
        public string selectedCheckboxDescription { get; set; }
        [NotMapped()]
        public string selectedTabId { get; set; }


        public virtual ChartType ChartType { get; set; }
        public virtual ChartFormat ChartFormat { get; set; }

    }

    public class SaveView
    {
        public int ID { get; set; }
        public int analysisID { get; set; }
        public int Order { get; set; }
        [Required(ErrorMessage = "View Name is required.")]
        [MaxLength(50)]
        [Display(Name = "View Name")]
        public string viewname { get; set; }
        [StringLength(3)]
        public string controller { get; set; }
        public int format { get; set; }
        public bool showaspercent { get; set; }
        public bool showbudget { get; set; }
        public bool highlightva { get; set; }
        public Nullable<bool> posvar { get; set; }
        public Nullable<decimal> posvaramt { get; set; }
        public Nullable<bool> negvar { get; set; }
        public Nullable<decimal> negvaramt { get; set; }
        public bool filter { get; set; }
        public Nullable<int> filteron { get; set; }
        public Nullable<decimal> filteramt { get; set; }
        public int charttype { get; set; }
        public Nullable<bool> trend { get; set; }
        public Nullable<int> account1 { get; set; }
        public Nullable<int> account2 { get; set; }
        public Nullable<int> account3 { get; set; }
        public Nullable<int> account4 { get; set; }
        public Nullable<int> account5 { get; set; }
        public Nullable<int> account6 { get; set; }
        public Nullable<int> account7 { get; set; }
        public Nullable<int> account8 { get; set; }
        public Nullable<int> account9 { get; set; }
        public Nullable<int> account10 { get; set; }
    }
}