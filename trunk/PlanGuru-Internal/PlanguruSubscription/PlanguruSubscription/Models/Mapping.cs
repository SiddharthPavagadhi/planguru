﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace PlanguruSubscription.Models
{
    [Table("UserCompanyMapping")]
    public class UserCompanyMapping
    {
        [Key()]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int UserCompanyMappingId { get; set; }

        public int CompanyId { get; set; }

        public int UserId { get; set; }

        public string CreatedBy { get; set; }

        public DateTime CreatedOn { get; set; }

        public string UpdatedBy { get; set; }

        public DateTime UpdatedOn { get; set; }
        //Public Overridable Property Company() As Company
        //Public Overridable Property User() As User
    }


    [Table("UserAnalysisMapping")]
    public class UserAnalysisMapping
    {
        [Key()]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int UserAnalysisMappingId { get; set; }

        public int UserId { get; set; }

        public int AnalysisId { get; set; }

        public string CreatedBy { get; set; }

        public DateTime CreatedOn { get; set; }

        public string UpdatedBy { get; set; }

        public DateTime UpdatedOn { get; set; }

        public virtual User User { get; set; }

        public virtual Analysis Analysis { get; set; }

    }


}