﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace PlanguruSubscription.Models
{
    public class Account
    {
        [Key(), Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int AccountId { get; set; }

        [Key(), Column(Order = 1)]
        public int AcctTypeId { get; set; }

        [Key(), Column(Order = 2)]
        public int AnalysisId { get; set; }

        [Required()]
        [StringLength(50)]
        public string AcctDescriptor { get; set; }
        [Required()]
        public int SortSequence { get; set; }
        [Required()]
        [StringLength(50)]
        public string Description { get; set; }
        [StringLength(50)]
        public string Subgrouping { get; set; }

        public int NumberFormat { get; set; }

        public int TotalType { get; set; }

        public int Option1 { get; set; }

        [NotMapped()]
        public string TypeDesc { get; set; }

        [NotMapped()]
        public string ClassDesc { get; set; }

    }
}