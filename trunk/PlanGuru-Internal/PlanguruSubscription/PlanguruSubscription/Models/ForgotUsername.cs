﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PlanguruSubscription.Models
{
    public class ForgotUsername
    {
        [Required(ErrorMessage = "Email address is required.")]
        [StringLength(50)]
        [RegularExpression("^([a-zA-Z0-9_\\-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)$", ErrorMessage = "Not a valid email adress.")]
        [Display(Name = "Email address")]
        public string emailAddress { get; set; }
    }

    public class ForgotPassword
    {
        [Required(ErrorMessage = "Username is required.")]
        [StringLength(15)]
        [Display(Name = "Username")]
        public string userName { get; set; }

        [Required(ErrorMessage = "Email address is required.")]
        [StringLength(50)]
        [RegularExpression("^([a-zA-Z0-9_\\-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)$", ErrorMessage = "Not a valid email adress.")]
        [Display(Name = "Email address")]
        public string emailAddress { get; set; }

    }
}