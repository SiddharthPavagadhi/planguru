﻿using PlanguruSubscription.Modules;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;

namespace PlanguruSubscription.Models
{
    public class DataAccess : DbContext
    {
        public DbSet<Customer> Customers { get; set; }
        public DbSet<SubscriptionPlan> SubscriptionPlans { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Analysis> Analyses { get; set; }
        public DbSet<UserRole> UserRoles { get; set; }
        public DbSet<UserRolePermission> UserRolePermissions { get; set; }
        public DbSet<UserRolePermissionMapping> UserRolePermissionMappings { get; set; }
        public DbSet<Company> Companies { get; set; }
        public DbSet<UserCompanyMapping> UserCompanyMappings { get; set; }
        public DbSet<EmailInfo> EmailInfos { get; set; }
        public DbSet<UserAnalysisMapping> UserAnalysisMappings { get; set; }
        public DbSet<SaveView> SaveView { get; set; }
        public DbSet<Scorecard> Scorecards { get; set; }
        public DbSet<Dashboard> Dashboards { get; set; }
        public DbSet<Balance> Balances { get; set; }
        public DbSet<Account> Accounts { get; set; }
        public DbSet<AcctType> AcctTypes { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            Logger.Log.Info(string.Format("DataBase Creation started "));
            try
            {
                modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

                //modelBuilder.Entity(Of Customer).ToTable("Customer")
                //modelBuilder.Entity(Of User).ToTable("User")
                //modelBuilder.Entity(Of Company).ToTable("Company")

                base.OnModelCreating(modelBuilder);
                modelBuilder.Conventions.Remove<IncludeMetadataConvention>();

                Logger.Log.Info(string.Format("DataBase Creation ended "));
            }
            catch (Exception ex)
            {
                Logger.Log.Error(string.Format("Unable to create Database with Message- {0} " + Environment.NewLine + "Stack Trace: {1} ", ex.Message, ex.StackTrace));
            }
            finally
            {
                Logger.Log.Info(string.Format("Execution Ended"));
            }
        }
    }
}