﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace PlanguruSubscription.Models
{
    public class Balance
    {
        [Key(), Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int BalanceId { get; set; }

        [Key(), Column(Order = 1)]
        public int AccountId { get; set; }

        [Key(), Column(Order = 2)]
        public int AnalysisId { get; set; }

        [RegularExpression("^-?[0-9]+(\\.[0-9]{1,5})?$")]
        public decimal H101 { get; set; }
        [RegularExpression("^-?[0-9]+(\\.[0-9]{1,5})?$")]
        public decimal H102 { get; set; }
        [RegularExpression("^-?[0-9]+(\\.[0-9]{1,5})?$")]
        public decimal H103 { get; set; }
        [RegularExpression("^-?[0-9]+(\\.[0-9]{1,5})?$")]
        public decimal H104 { get; set; }
        [RegularExpression("^-?[0-9]+(\\.[0-9]{1,5})?$")]
        public decimal H105 { get; set; }
        [RegularExpression("^-?[0-9]+(\\.[0-9]{1,5})?$")]
        public decimal H106 { get; set; }
        [RegularExpression("^-?[0-9]+(\\.[0-9]{1,5})?$")]
        public decimal H107 { get; set; }
        [RegularExpression("^-?[0-9]+(\\.[0-9]{1,5})?$")]
        public decimal H108 { get; set; }
        [RegularExpression("^-?[0-9]+(\\.[0-9]{1,5})?$")]
        public decimal H109 { get; set; }
        [RegularExpression("^-?[0-9]+(\\.[0-9]{1,5})?$")]
        public decimal H110 { get; set; }
        [RegularExpression("^-?[0-9]+(\\.[0-9]{1,5})?$")]
        public decimal H111 { get; set; }
        [RegularExpression("^-?[0-9]+(\\.[0-9]{1,5})?$")]
        public decimal H112 { get; set; }

        [RegularExpression("^-?[0-9]+(\\.[0-9]{1,5})?$")]
        public decimal H201 { get; set; }
        [RegularExpression("^-?[0-9]+(\\.[0-9]{1,5})?$")]
        public decimal H202 { get; set; }
        [RegularExpression("^-?[0-9]+(\\.[0-9]{1,5})?$")]
        public decimal H203 { get; set; }
        [RegularExpression("^-?[0-9]+(\\.[0-9]{1,5})?$")]
        public decimal H204 { get; set; }
        [RegularExpression("^-?[0-9]+(\\.[0-9]{1,5})?$")]
        public decimal H205 { get; set; }
        [RegularExpression("^-?[0-9]+(\\.[0-9]{1,5})?$")]
        public decimal H206 { get; set; }
        [RegularExpression("^-?[0-9]+(\\.[0-9]{1,5})?$")]
        public decimal H207 { get; set; }
        [RegularExpression("^-?[0-9]+(\\.[0-9]{1,5})?$")]
        public decimal H208 { get; set; }
        [RegularExpression("^-?[0-9]+(\\.[0-9]{1,5})?$")]
        public decimal H209 { get; set; }
        [RegularExpression("^-?[0-9]+(\\.[0-9]{1,5})?$")]
        public decimal H210 { get; set; }
        [RegularExpression("^-?[0-9]+(\\.[0-9]{1,5})?$")]
        public decimal H211 { get; set; }
        [RegularExpression("^-?[0-9]+(\\.[0-9]{1,5})?$")]
        public decimal H212 { get; set; }


        [RegularExpression("^-?[0-9]+(\\.[0-9]{1,5})?$")]
        public decimal H301 { get; set; }
        [RegularExpression("^-?[0-9]+(\\.[0-9]{1,5})?$")]
        public decimal H302 { get; set; }
        [RegularExpression("^-?[0-9]+(\\.[0-9]{1,5})?$")]
        public decimal H303 { get; set; }
        [RegularExpression("^-?[0-9]+(\\.[0-9]{1,5})?$")]
        public decimal H304 { get; set; }
        [RegularExpression("^-?[0-9]+(\\.[0-9]{1,5})?$")]
        public decimal H305 { get; set; }
        [RegularExpression("^-?[0-9]+(\\.[0-9]{1,5})?$")]
        public decimal H306 { get; set; }
        [RegularExpression("^-?[0-9]+(\\.[0-9]{1,5})?$")]
        public decimal H307 { get; set; }
        [RegularExpression("^-?[0-9]+(\\.[0-9]{1,5})?$")]
        public decimal H308 { get; set; }
        [RegularExpression("^-?[0-9]+(\\.[0-9]{1,5})?$")]
        public decimal H309 { get; set; }
        [RegularExpression("^-?[0-9]+(\\.[0-9]{1,5})?$")]
        public decimal H310 { get; set; }
        [RegularExpression("^-?[0-9]+(\\.[0-9]{1,5})?$")]
        public decimal H311 { get; set; }
        [RegularExpression("^-?[0-9]+(\\.[0-9]{1,5})?$")]
        public decimal H312 { get; set; }

        [RegularExpression("^-?[0-9]+(\\.[0-9]{1,5})?$")]
        public decimal H401 { get; set; }
        [RegularExpression("^-?[0-9]+(\\.[0-9]{1,5})?$")]
        public decimal H402 { get; set; }
        [RegularExpression("^-?[0-9]+(\\.[0-9]{1,5})?$")]
        public decimal H403 { get; set; }
        [RegularExpression("^-?[0-9]+(\\.[0-9]{1,5})?$")]
        public decimal H404 { get; set; }
        [RegularExpression("^-?[0-9]+(\\.[0-9]{1,5})?$")]
        public decimal H405 { get; set; }
        [RegularExpression("^-?[0-9]+(\\.[0-9]{1,5})?$")]
        public decimal H406 { get; set; }
        [RegularExpression("^-?[0-9]+(\\.[0-9]{1,5})?$")]
        public decimal H407 { get; set; }
        [RegularExpression("^-?[0-9]+(\\.[0-9]{1,5})?$")]
        public decimal H408 { get; set; }
        [RegularExpression("^-?[0-9]+(\\.[0-9]{1,5})?$")]
        public decimal H409 { get; set; }
        [RegularExpression("^-?[0-9]+(\\.[0-9]{1,5})?$")]
        public decimal H410 { get; set; }
        [RegularExpression("^-?[0-9]+(\\.[0-9]{1,5})?$")]
        public decimal H411 { get; set; }
        [RegularExpression("^-?[0-9]+(\\.[0-9]{1,5})?$")]
        public decimal H412 { get; set; }

        [RegularExpression("^-?[0-9]+(\\.[0-9]{1,5})?$")]
        public decimal H501 { get; set; }
        [RegularExpression("^-?[0-9]+(\\.[0-9]{1,5})?$")]
        public decimal H502 { get; set; }
        [RegularExpression("^-?[0-9]+(\\.[0-9]{1,5})?$")]
        public decimal H503 { get; set; }
        [RegularExpression("^-?[0-9]+(\\.[0-9]{1,5})?$")]
        public decimal H504 { get; set; }
        [RegularExpression("^-?[0-9]+(\\.[0-9]{1,5})?$")]
        public decimal H505 { get; set; }
        [RegularExpression("^-?[0-9]+(\\.[0-9]{1,5})?$")]
        public decimal H506 { get; set; }
        [RegularExpression("^-?[0-9]+(\\.[0-9]{1,5})?$")]
        public decimal H507 { get; set; }
        [RegularExpression("^-?[0-9]+(\\.[0-9]{1,5})?$")]
        public decimal H508 { get; set; }
        [RegularExpression("^-?[0-9]+(\\.[0-9]{1,5})?$")]
        public decimal H509 { get; set; }
        [RegularExpression("^-?[0-9]+(\\.[0-9]{1,5})?$")]
        public decimal H510 { get; set; }
        [RegularExpression("^-?[0-9]+(\\.[0-9]{1,5})?$")]
        public decimal H511 { get; set; }
        [RegularExpression("^-?[0-9]+(\\.[0-9]{1,5})?$")]
        public decimal H512 { get; set; }

        [RegularExpression("^-?[0-9]+(\\.[0-9]{1,5})?$")]
        public decimal B101 { get; set; }
        [RegularExpression("^-?[0-9]+(\\.[0-9]{1,5})?$")]
        public decimal B102 { get; set; }
        [RegularExpression("^-?[0-9]+(\\.[0-9]{1,5})?$")]
        public decimal B103 { get; set; }
        [RegularExpression("^-?[0-9]+(\\.[0-9]{1,5})?$")]
        public decimal B104 { get; set; }
        [RegularExpression("^-?[0-9]+(\\.[0-9]{1,5})?$")]
        public decimal B105 { get; set; }
        [RegularExpression("^-?[0-9]+(\\.[0-9]{1,5})?$")]
        public decimal B106 { get; set; }
        [RegularExpression("^-?[0-9]+(\\.[0-9]{1,5})?$")]
        public decimal B107 { get; set; }
        [RegularExpression("^-?[0-9]+(\\.[0-9]{1,5})?$")]
        public decimal B108 { get; set; }
        [RegularExpression("^-?[0-9]+(\\.[0-9]{1,5})?$")]
        public decimal B109 { get; set; }
        [RegularExpression("^-?[0-9]+(\\.[0-9]{1,5})?$")]
        public decimal B110 { get; set; }
        [RegularExpression("^-?[0-9]+(\\.[0-9]{1,5})?$")]
        public decimal B111 { get; set; }
        [RegularExpression("^-?[0-9]+(\\.[0-9]{1,5})?$")]
        public decimal B112 { get; set; }

        [RegularExpression("^-?[0-9]+(\\.[0-9]{1,5})?$")]
        public decimal A101 { get; set; }
        [RegularExpression("^-?[0-9]+(\\.[0-9]{1,5})?$")]
        public decimal A102 { get; set; }
        [RegularExpression("^-?[0-9]+(\\.[0-9]{1,5})?$")]
        public decimal A103 { get; set; }
        [RegularExpression("^-?[0-9]+(\\.[0-9]{1,5})?$")]
        public decimal A104 { get; set; }
        [RegularExpression("^-?[0-9]+(\\.[0-9]{1,5})?$")]
        public decimal A105 { get; set; }
        [RegularExpression("^-?[0-9]+(\\.[0-9]{1,5})?$")]
        public decimal A106 { get; set; }
        [RegularExpression("^-?[0-9]+(\\.[0-9]{1,5})?$")]
        public decimal A107 { get; set; }
        [RegularExpression("^-?[0-9]+(\\.[0-9]{1,5})?$")]
        public decimal A108 { get; set; }
        [RegularExpression("^-?[0-9]+(\\.[0-9]{1,5})?$")]
        public decimal A109 { get; set; }
        [RegularExpression("^-?[0-9]+(\\.[0-9]{1,5})?$")]
        public decimal A110 { get; set; }
        [RegularExpression("^-?[0-9]+(\\.[0-9]{1,5})?$")]
        public decimal A111 { get; set; }
        [RegularExpression("^-?[0-9]+(\\.[0-9]{1,5})?$")]
        public decimal A112 { get; set; }

        [RegularExpression("^-?[0-9]+(\\.[0-9]{1,5})?$")]
        public decimal B201 { get; set; }
        [RegularExpression("^-?[0-9]+(\\.[0-9]{1,5})?$")]
        public decimal B202 { get; set; }
        [RegularExpression("^-?[0-9]+(\\.[0-9]{1,5})?$")]
        public decimal B203 { get; set; }
        [RegularExpression("^-?[0-9]+(\\.[0-9]{1,5})?$")]
        public decimal B204 { get; set; }
        [RegularExpression("^-?[0-9]+(\\.[0-9]{1,5})?$")]
        public decimal B205 { get; set; }
        [RegularExpression("^-?[0-9]+(\\.[0-9]{1,5})?$")]
        public decimal B206 { get; set; }
        [RegularExpression("^-?[0-9]+(\\.[0-9]{1,5})?$")]
        public decimal B207 { get; set; }
        [RegularExpression("^-?[0-9]+(\\.[0-9]{1,5})?$")]
        public decimal B208 { get; set; }
        [RegularExpression("^-?[0-9]+(\\.[0-9]{1,5})?$")]
        public decimal B209 { get; set; }
        [RegularExpression("^-?[0-9]+(\\.[0-9]{1,5})?$")]
        public decimal B210 { get; set; }
        [RegularExpression("^-?[0-9]+(\\.[0-9]{1,5})?$")]
        public decimal B211 { get; set; }
        [RegularExpression("^-?[0-9]+(\\.[0-9]{1,5})?$")]
        public decimal B212 { get; set; }

        [RegularExpression("^-?[0-9]+(\\.[0-9]{1,5})?$")]
        public decimal B301 { get; set; }
        [RegularExpression("^-?[0-9]+(\\.[0-9]{1,5})?$")]
        public decimal B302 { get; set; }
        [RegularExpression("^-?[0-9]+(\\.[0-9]{1,5})?$")]
        public decimal B303 { get; set; }
        [RegularExpression("^-?[0-9]+(\\.[0-9]{1,5})?$")]
        public decimal B304 { get; set; }
        [RegularExpression("^-?[0-9]+(\\.[0-9]{1,5})?$")]
        public decimal B305 { get; set; }
        [RegularExpression("^-?[0-9]+(\\.[0-9]{1,5})?$")]
        public decimal B306 { get; set; }
        [RegularExpression("^-?[0-9]+(\\.[0-9]{1,5})?$")]
        public decimal B307 { get; set; }
        [RegularExpression("^-?[0-9]+(\\.[0-9]{1,5})?$")]
        public decimal B308 { get; set; }
        [RegularExpression("^-?[0-9]+(\\.[0-9]{1,5})?$")]
        public decimal B309 { get; set; }
        [RegularExpression("^-?[0-9]+(\\.[0-9]{1,5})?$")]
        public decimal B310 { get; set; }
        [RegularExpression("^-?[0-9]+(\\.[0-9]{1,5})?$")]
        public decimal B311 { get; set; }
        [RegularExpression("^-?[0-9]+(\\.[0-9]{1,5})?$")]
        public decimal B312 { get; set; }

    }
}