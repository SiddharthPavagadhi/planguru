﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PlanguruSubscription.Models
{
    public class SubscribeForm
    {
        public string Coupen_Code { get; set; }
        public string First_Name { get; set; }
        public string Last_Name { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Subscriber_Name { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }
        public string Country { get; set; }
        public string RecurlyToken { get; set; }
        public string PreferedLanguage { get; set; }
        //subscribe plan related thing
        public string PlanCode { get; set; }
        public string PlanName { get; set; }
        public string PlanRate { get; set; }
        public string PlanFrequency { get; set; }
        public int PlanTypeId { get; set; }
        //addon related detail
        public string AddOnCode { get; set; }
        public string AddOnName { get; set; }
        public double AddOnUnitAmount { get; set; }
        public int Number_Of_Addon { get; set; }
        //coupon code list
        //public List<Recurly.RecurlyAccountCoupon> CoupenList { get; set; }
        public List<RecurlyNew.Coupon> CoupenList { get; set; }
    }
}