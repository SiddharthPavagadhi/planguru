﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace PlanguruSubscription.Models
{
    public class Scorecard
    {

        public int ScorecardId { get; set; }
        public int Order { get; set; }

        [RegularExpression("^[1-9]+[0-9]*$", ErrorMessage = "Please select the item to be listed in the scorecard.")]
        [Required(ErrorMessage = "Please select the item to be listed in the scorecard.")]
        public int AccountId { get; set; }
        public int AnalysisId { get; set; }
        public string ScorecardDesc { get; set; }

        [Required(ErrorMessage = "Select goal missed threshold")]
        [RegularExpression("(([0-9]\\d?)|100)$", ErrorMessage = "It will accept between (0 to 100) numberic values.")]
        [Display(Name = "Goal Missed threshold %")]
        public int Missed { get; set; }

        [Required(ErrorMessage = "Select exceeded threshold")]
        [RegularExpression("(([0-9]\\d?)|100)$", ErrorMessage = "It will accept between (0 to 100) numberic values.")]
        [Display(Name = "Goal Exceeded threshold %")]
        public int Outperform { get; set; }

        [Display(Name = "Show as % of ")]
        public bool ShowasPercent { get; set; }

        [Display(Name = "If actual exceeds budget the variance is ")]
        public bool VarIsFavorable { get; set; }

        public int Option1 { get; set; }
        public int Option2 { get; set; }
        public int Option3 { get; set; }
        public bool IsValid { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime UpdatedOn { get; set; }

        [NotMapped()]
        public string selectedCheckboxId { get; set; }
        [NotMapped()]
        public string selectedCheckboxDescription { get; set; }
        [NotMapped()]
        public string selectedTabId { get; set; }

        [NotMapped()]
        public int AcctTypeId { get; set; }
        [NotMapped()]
        public int UserId { get; set; }
    }
}