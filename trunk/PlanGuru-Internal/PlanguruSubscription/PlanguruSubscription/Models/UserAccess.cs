﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace PlanguruSubscription.Models
{
    using Microsoft.VisualBasic;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Data;
    using System.Diagnostics;
    using System.Web.Mvc;
    public class CustomActionFilter : System.Web.Mvc.ActionFilterAttribute
    {



        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            HttpContext ctx = HttpContext.Current;

            // check if session is supported

            if (ctx.Session != null)
            {
                // check if a new session id was generated

                if (ctx.Session.IsNewSession)
                {
                    // If it says it is a new session, but an existing cookie exists, then it must
                    // have timed out
                    string sessionCookie = ctx.Request.Headers["Cookie"];
                    if ((sessionCookie != null) && (sessionCookie.IndexOf("ASP.NET_SessionId") >= 0))
                    {
                        ActionResult result = null;
                        filterContext.HttpContext.Session.Clear();
                        filterContext.HttpContext.Session.Abandon();
                        //Dim TempData = New System.Web.Mvc.TempDataDictionary()                    
                        //TempData("ErrorMessage") = String.Concat("Session Expired, Please login again.")

                        if ((filterContext.HttpContext.Request.IsAjaxRequest()))
                        {
                            result = new JsonResult
                            {
                                Data = false,
                                JsonRequestBehavior = JsonRequestBehavior.AllowGet
                            };
                        }
                        else
                        {
                            result = new RedirectResult("~/Home/Index?session=-1");
                            //New RedirectToRouteResult(New RouteValueDictionary(New With {.controller = "Home", .action = "Index?session=-1"}))
                        }

                        filterContext.Result = result;


                    }
                    else if ((sessionCookie != null) && (sessionCookie.IndexOf("ASP.NET_SessionId") == -1))
                    {
                        ctx.Response.Redirect("~/Home/Index");

                    }
                }
            }

            base.OnActionExecuting(filterContext);

        }


    }
 


    




    public class UserAccess
    {
        public bool SubscriptionManagement { get; set; }
        public bool UserManagement { get; set; }
        public bool SearchCustomer { get; set; }
        public bool ViewSubscription { get; set; }
        public bool AddSubscription { get; set; }
        public bool UpdateSubscription { get; set; }
        public bool CancelSubscription { get; set; }
        public bool AddUser { get; set; }
        public bool DeleteUser { get; set; }
        public bool ViewUser { get; set; }
        public bool UpdateUser { get; set; }
        public bool AddCompany { get; set; }
        public bool DeleteCompany { get; set; }
        public bool ViewCompanies { get; set; }
        public bool UpdateCompany { get; set; }
        public bool UserCompanyMapping { get; set; }
        public bool AddAnalysis { get; set; }
        public bool DeleteAnalysis { get; set; }
        public bool ViewAnalyses { get; set; }
        public bool UpdateAnalysis { get; set; }
        public bool UserAnalysisMapping { get; set; }
        public bool ViewAnalysisInfo { get; set; }
        public bool PrintAnalysisInfo { get; set; }
        public bool ModifyDashboard { get; set; }
        public bool DeleteSavedViews { get; set; }
        public bool UpdateSavedViews { get; set; }
        public bool ViewSavedViews { get; set; }
        public bool Branding { get; set; }

    }
}