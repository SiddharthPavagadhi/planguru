﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace PlanguruSubscription.Models
{
    public class AcctType
    {
        [Key(), Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int AcctTypeId { get; set; }

        [Key(), Column(Order = 1)]
        public int AnalysisId { get; set; }

        [Required()]
        [StringLength(50)]
        public string TypeDesc { get; set; }
        [StringLength(50)]
        public string ClassDesc { get; set; }
        [StringLength(50)]
        public string SubclassDesc { get; set; }

    }
}