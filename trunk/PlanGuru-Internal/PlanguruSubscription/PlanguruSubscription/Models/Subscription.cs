﻿//using Recurly;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace PlanguruSubscription.Models
{
    public class Customer
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int CustomerId { get; set; }

        [Display(Name = "Subscription plan")]
        public int PlanTypeId { get; set; }

        [Required(ErrorMessage = "First Name is required.")]
        [StringLength(50)]
        [Display(Name = "Customer First Name")]
        public string CustomerFirstName { get; set; }

        [Required(ErrorMessage = "Last Name is required.")]
        [StringLength(50)]
        [Display(Name = "Customer Last Name")]
        public string CustomerLastName { get; set; }

        [StringLength(150)]
        [Display(Name = "Customer's Company Name")]
        [Required(ErrorMessage = "Subscriber name is required.")]
        public string CustomerCompanyName { get; set; }

        [Display(Name = "Quantity")]
        public int Quantity { get; set; }

        [StringLength(50)]
        [Display(Name = "Contact First Name")]
        public string ContactFirstName { get; set; }

        [StringLength(50)]
        [Display(Name = "Contact Last Name")]
        public string ContactLastName { get; set; }

        [StringLength(100)]
        [Display(Name = "Customer Addresss1")]
        public string CustomerAddress1 { get; set; }

        [Display(Name = "Customer Address2")]
        [StringLength(100)]
        public string CustomerAddress2 { get; set; }

        [StringLength(50)]
        [Display(Name = "Country")]
        public string Country { get; set; }

        [StringLength(50)]
        [Display(Name = "State")]
        public string State { get; set; }

        [StringLength(50)]
        [Display(Name = "City")]
        public string City { get; set; }


        [StringLength(10)]
        [Display(Name = "Postal Code")]
        public string CustomerPostalCode { get; set; }


        [StringLength(15)]
        [Display(Name = "Telephone No")]
        [DataType(DataType.PhoneNumber)]
        public string ContactTelephone { get; set; }


        [StringLength(200)]
        [Display(Name = "Email address")]
        [Required(ErrorMessage = "Email address is required.")]
        [DataType(DataType.EmailAddress)]
        public string CustomerEmail { get; set; }


        [Display(Name = "First Name On Card")]
        public string FirstNameOnCard { get; set; }


        [Display(Name = "Last Name On Card")]
        public string LastNameOnCard { get; set; }


        [Display(Name = "Credit Card Number")]
        public string CreditCardNumber { get; set; }

        [Display(Name = "CVV")]
        public string CVV { get; set; }

        public int ExpirationYear { get; set; }

        public int ExpirationMonth { get; set; }

        public string CreatedBy { get; set; }

        public DateTime CreatedOn { get; set; }

        public string UpdatedBy { get; set; }

        public DateTime UpdatedOn { get; set; }

        public string LogoImageFile { get; set; }

        public string PrimaryColor { get; set; }

        public string SecondaryColor { get; set; }

        public string FootNote { get; set; }

        public string VanityFirmName { get; set; }

        [NotMapped()]
        public int UserId { get; set; }

        [NotMapped()]
        public string UserName { get; set; }

        [NotMapped()]
        public int Status { get; set; }

        [NotMapped()]
        public string SubscriptionStartAt { get; set; }

        [NotMapped()]
        public string SubscriptionEndAt { get; set; }

        [NotMapped()]
        public double SubscriptionAmount { get; set; }

        [Display(Name = "SAU Name")]
        [NotMapped()]
        public string SAUName { get; set; }

        [Display(Name = "Customer No")]
        [NotMapped()]
        public string SearchCustId { get; set; }

        [Display(Name = "Customer Name")]
        [NotMapped()]
        public string SearchCustName { get; set; }

        [Display(Name = "Telephone Number")]
        [NotMapped()]
        public string SearchCustTelephone { get; set; }

        [NotMapped()]
        public string PlanType { get; set; }



        public string CustomerFullName
        {
            get { return string.Concat(this.CustomerLastName, "  ", this.CustomerFirstName); }
        }

        public string ContactPersonFullName
        {
            get { return string.Concat(this.ContactLastName, "  ", this.ContactFirstName); }
        }



        public Nullable<int> ResultCode { get; set; }

        public Nullable<int> LicenseCustomerId { get; set; }

        public Nullable<int> LicenseId { get; set; }

        public string Password { get; set; }

        public string SerialNumber { get; set; }

        public string ActivationPassword { get; set; }

        public string AddUserResponse { get; set; }

        public Nullable<int> NumberOfAddOns { get; set; }

        public int NumberOfAnalyticsAddOn { get; set; }

    }

    [NotMapped()]
    public class LoginModel
    {

        [Required(ErrorMessage = "Username is required.")]
        [StringLength(50)]
        [Display(Name = "Username")]
        public string Username { get; set; }

        [Required(ErrorMessage = "Password is required.")]
        [Display(Name = "Password")]
        public string Password { get; set; }

    }


    public class SubscriptionPlan
    {

        [Key()]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int PlanTypeId { get; set; }
        public string PlanType { get; set; }
        public string Plan { get; set; }
        public decimal Cost_of_Subscription { get; set; }
        public int DefaultAddOnUsers { get; set; }
        public decimal Cost_of_Additional_Users { get; set; }
        public Nullable<int> ProductOptionId { get; set; }
        public Nullable<int> AllowableSeats { get; set; }
        public Nullable<decimal> Cost_of_Additional_Analytics_Users { get; set; }

    }

    public class User
    {

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int UserId { get; set; }

        [Required(ErrorMessage = "Username is required.")]
        [StringLength(50)]
        [RegularExpression("^[a-zA-Z0-9_]{5,15}$", ErrorMessage = "Username accepts min 5 and max 15 characters, it accept alphabets, numbers & underscore.")]
        [Display(Name = "Username")]
        public string UserName { get; set; }

        //<Required(ErrorMessage:="Password is required.")> _
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Required(ErrorMessage = "User First Name is required.")]
        [StringLength(50)]
        [Display(Name = "User First Name")]
        public string FirstName { get; set; }
        [Required(ErrorMessage = "User Last Name is required.")]
        [StringLength(50)]
        [Display(Name = "User Last Name")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "User Email is required.")]
        [StringLength(50)]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "User's Email")]
        public string UserEmail { get; set; }

        public int Status { get; set; }

        public string SecurityKey { get; set; }

        [Required(ErrorMessage = "Select User Role.")]
        [Display(Name = "User Role")]
        public int UserRoleId { get; set; }

        [Required(ErrorMessage = "Select Subscriber.")]
        [Display(Name = "Subscriber Name")]
        public int CustomerId { get; set; }

        [NotMapped()]
        public RecurlyNew.AddOn AddOn { get; set; }

        public int AnalysisId { get; set; }

        [NotMapped()]
        public int selectedCompany { get; set; }

        [NotMapped()]
        public string fiscalMonthOfSelectedCompany { get; set; }

        [NotMapped()]
        public bool isSharedAnalysis { get; set; }

        [NotMapped()]
        public int currentTabIndex { get; set; }

        [NotMapped()]
        public int FirstYear { get; set; }

        public string CreatedBy { get; set; }

        public DateTime CreatedOn { get; set; }

        public string UpdatedBy { get; set; }

        public DateTime UpdatedOn { get; set; }

        public virtual UserRole UserRole { get; set; }
        public virtual Customer Customer { get; set; }

    }

    public class PostedUsers
    {

        [Display(Name = "Associate User with")]
        public string[] UserIds { get; set; }
    }

    public class Branding
    {

        [Required(ErrorMessage = "Vanity Firm Name is required.")]
        [Display(Name = "Vanity Firm Name")]
        public string VanityFirmName { get; set; }

        [Display(Name = "Logo Image")]
        public string LogoImageFile { get; set; }

        [Display(Name = "Menu background color")]
        public string PrimaryColor { get; set; }

        [Display(Name = "Selected item color")]
        public string SecondaryColor { get; set; }

        [MaxLength(1000, ErrorMessage = "Maximum 1000 characters allowed for footnote.")]
        public string Footnote { get; set; }

    }

    public class ResetPassword
    {

        public string UserId { get; set; }
        public int UserRoleId { get; set; }

        [Required(ErrorMessage = "Username is required.")]
        [StringLength(50)]
        [RegularExpression("^[a-zA-Z0-9_]{5,15}$", ErrorMessage = "Username accepts min 5 and max 15 characters, it accept alphabets, numbers & underscore.")]
        [Display(Name = "Username")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "User First Name is required.")]
        [StringLength(50)]
        [Display(Name = "User First Name")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "User Last Name is required.")]
        [StringLength(50)]
        [Display(Name = "User Last Name")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "User Email is required.")]
        [StringLength(50)]
        [RegularExpression("^([a-zA-Z0-9_\\-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,99}|[0-9]{1,3})(\\]?)$", ErrorMessage = "Not a valid email adress.")]
        [Display(Name = "User's Email")]
        public string UserEmail { get; set; }

        [Required(ErrorMessage = "Subscriber Name is required.")]
        [Display(Name = "Subscriber Name")]
        public string SubscriberName { get; set; }

        public bool ChangePassword { get; set; }

        [Display(Name = "Current Password")]
        [DataType(DataType.Password)]
        public string CurrentPassword { get; set; }

        [Display(Name = "New Password")]
        [DataType(DataType.Password)]
        [RegularExpression("(?=^.{8,15}$)(?=.*\\d)(?=.*[a-z | A-Z])(?!.*\\s).*$", ErrorMessage = "Password should be minimum 8 characters, at least one number and one letter.")]
        public string NewPassword { get; set; }

        [Compare("NewPassword", ErrorMessage = "Confirm New Password and New Password do not match.")]
        [DataType(DataType.Password)]
        [Display(Name = "Confirm New Password")]
        public string ConfirmNewPassword { get; set; }

    }

    public class UserVerification
    {

        public int User_Id { get; set; }

        public int UserRoleId { get; set; }
        public int Status { get; set; }

        [Required(ErrorMessage = "New Username is required.")]
        [StringLength(50)]
        [RegularExpression("^[a-zA-Z0-9_]{5,15}$", ErrorMessage = "New Username accepts min 5 and max 15 characters, it accept alphabets, numbers & underscore.")]
        [Display(Name = "New Username")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "Temporary Password is required.")]
        [Display(Name = "Temporary Password")]
        [NotMapped()]
        public string CurrentPassword { get; set; }

        [Required(ErrorMessage = "New Password is required.")]
        [Display(Name = "New Password")]
        [RegularExpression("(?=^.{8,15}$)(?=.*\\d)(?=.*[a-z | A-Z])(?!.*\\s).*$", ErrorMessage = "Password should be minimum 8 characters, at least one number and one letter.")]
        [NotMapped()]
        public string NewPassword { get; set; }

        [Compare("NewPassword", ErrorMessage = "The New Password and Confirmation New Password do not match.")]
        [Required(ErrorMessage = "Confirm Password is required.")]
        [Display(Name = "Confirm New Password")]
        [NotMapped()]
        public string ConfirmNewPassword { get; set; }

    }






    public class User_
    {

        public string Id;
        public string Name;
        public object Tag;

        public string IsSelected;

        public User_()
        {
        }

        public User_(string Id, string Name, object Tag, bool IsSelected)
        {
            this.Id = Id;
            this.Name = Name;
            this.Tag = Tag;
            this.IsSelected = IsSelected.ToString();
        }


    }

    public class Company : User_
    {

        public int CompanyId { get; set; }

        [Required(ErrorMessage = "Company Name is required.")]
        [Display(Name = "Company Name")]
        public string CompanyName { get; set; }

        [Required(ErrorMessage = "Fiscal Month Start is required.")]
        [Display(Name = "Fiscal Month Start")]
        public string FiscalMonthStart { get; set; }

        public string FiscalMonthName { get; set; }

        [Required(ErrorMessage = "Contact First Name is required.")]
        [Display(Name = "Contact First Name")]
        public string ContactFirstName { get; set; }

        [Required(ErrorMessage = "Contact Last Name is required.")]
        [Display(Name = "Contact Last Name")]
        public string ContactLastName { get; set; }

        [Required(ErrorMessage = "Contact Email is required.")]
        [Display(Name = "Contact Email")]
        public string ContactEmail { get; set; }


        [Required(ErrorMessage = "Contact Telephone is required.")]
        [Display(Name = "Contact Telephone")]
        [StringLength(50, ErrorMessage = "Contact Telephone accepts maximum 15 characters.")]
        public string ContactTelephone { get; set; }

        public int CustomerId { get; set; }

        public int IndustryCode { get; set; }

        [NotMapped()]
        public IList<User_> AvailableUsers { get; set; }

        [NotMapped()]
        public IList<User_> SelectedUsers { get; set; }

        [NotMapped()]
        public PostedUsers PostedUsers { get; set; }

        public string CreatedBy { get; set; }

        public DateTime CreatedOn { get; set; }

        public string UpdatedBy { get; set; }

        public DateTime UpdatedOn { get; set; }

    }

    public enum EmailType
    {
        NewSubscriptionSignup = 1,
        // Subscription
        NewUserAdded = 2,
        // CRU 
        NewUserSignUp = 3,
        // SAU
        PasswordReset = 4,
        // 
        SubcriptionCancellation = 5,
        // 
        UserDeleted = 6,
        //
        ForgotUsername = 7,

        NewUserAddedForBusiness = 9,

        AdditionalUserAdded = 10
    }



    public enum PlanType
    {
        PPU = 1,
        BP = 2,
        BNPM = 3,
        BNPY = 4,
        AAM = 5,
        AAY = 6,
        BNPMUPG = 7,
        BNPYUPG = 8
    }

    public enum StatusE
    {
        Pending = 1,
        Active = 2,
        Suspended = 3,
        Deactivated = 4
    }

}