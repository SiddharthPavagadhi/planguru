﻿Imports System.Web.UI.WebControls.Expressions
Imports FarPoint.Mvc.Spread
Imports System.Data.Entity
Imports PagedList

Namespace onlineAnalytics
    <CustAuthFilter()>
    Public Class SVController
        Inherits System.Web.Mvc.Controller
        
        Private utility As New Utility()
        Private savedviewrepository As ISaveViewRepository
        Private UserType As UserRole
        Private UserInfo As User
        Private pageSize As Integer

        Public ReadOnly Property Page_Size As Integer

            Get
                If (System.Configuration.ConfigurationManager.AppSettings("PageSize") <> "") Then
                    pageSize = System.Configuration.ConfigurationManager.AppSettings("PageSize").ToString()
                Else
                    pageSize = 10
                End If
                Return Me.pageSize
            End Get
        End Property

        Public Sub New()
            Me.savedviewrepository = New SaveViewRepository(New DataAccess())
        End Sub

        Public Sub New(saveviewrepository As ISaveViewRepository)
            Me.savedviewrepository = saveviewrepository
        End Sub

        <CustomActionFilter()>
        Function Index() As ActionResult
            Return View()
        End Function

        <CustomActionFilter()>
        Function UpdateOrder(Id As Integer, fromposition As Integer, toPosition As Integer, direction As String) As JsonResult
            Dim saveviewDetail
            Try
                If (Not Session("UserType") Is Nothing) And (Not Session("UserInfo") Is Nothing) Then
                    UserInfo = DirectCast(Session("UserInfo"), User)
                    If (UserInfo.AnalysisId = 0) Then
                        'Return RedirectToAction("Index", "Dashboard")
                    End If
                End If

                If (direction.ToLower() = "back") Then

                    If UserInfo.currentTabIndex = 1 Then
                        saveviewDetail = utility.SaveViewRepository.GetSavedViewDetail().Where(Function(d) d.analysisID = UserInfo.AnalysisId _
                                                                                                   And toPosition <= d.Order And d.Order <= fromposition).ToList()

                    Else
                        saveviewDetail = utility.SaveViewRepository.GetSavedViewDetail().Where(Function(d) d.AnalysisId = UserInfo.AnalysisId _
                                                                                                 And toPosition <= d.Order And d.Order <= fromposition).ToList()
                    End If

                    For Each item In saveviewDetail
                        item.Order += 1
                    Next

                Else
                    If UserInfo.currentTabIndex = 1 Then
                        saveviewDetail = utility.SaveViewRepository.GetSavedViewDetail().Where(Function(d) d.analysisID = UserInfo.AnalysisId _
                                                                                                And fromposition <= d.Order And d.Order <= toPosition).ToList()
                    Else
                        saveviewDetail = utility.SaveViewRepository.GetSavedViewDetail().Where(Function(d) d.analysisID = UserInfo.AnalysisId _
                                                                                                And fromposition <= d.Order And d.Order <= toPosition).ToList()
                    End If

                    For Each item In saveviewDetail
                        item.Order -= 1
                    Next
                End If

                utility.SaveViewRepository.GetSavedViewDetail().First(Function(d) d.ID = Id).Order = toPosition
                utility.SaveViewRepository.Save()

                Return Json("success", JsonRequestBehavior.AllowGet)
            Catch ex As Exception
                Return Json("success", JsonRequestBehavior.AllowGet)
            Finally

            End Try

        End Function

        <CustomActionFilter()>
        Function GetSavedViewsList() As JsonResult
            Dim selectedAnalysisId As Integer
            If (Not Session("UserType") Is Nothing) And (Not Session("UserInfo") Is Nothing) Then
                UserType = DirectCast(Session("UserType"), UserRole)
                UserInfo = DirectCast(Session("UserInfo"), User)
                'When user logged in, it will show the company list based on analysisId
                If (Not Session("SelectedAnalysisFromDropdown") Is Nothing) Then
                    selectedAnalysisId = Session("SelectedAnalysisFromDropdown").ToString()
                End If
            End If
            Dim data As SelectList = Utility.PouplateSavedReports(selectedAnalysisId)
            Return Json(data, JsonRequestBehavior.AllowGet)
        End Function

        <CustomActionFilter()>
        Function Views(page As System.Nullable(Of Integer)) As ViewResult
            Dim order = 1
            Try
                Logger.Log.Info(String.Format("Saved Reports - Views Index Execution Started"))
                Dim selectedAnalysisId As Integer
                If (Not Session("UserType") Is Nothing) And (Not Session("UserInfo") Is Nothing) Then
                    UserType = DirectCast(Session("UserType"), UserRole)
                    UserInfo = DirectCast(Session("UserInfo"), User)
                    If (Not Session("SelectedAnalysisFromDropdown") Is Nothing) Then
                        selectedAnalysisId = Session("SelectedAnalysisFromDropdown").ToString()
                    End If
                End If

                Dim savedViews As IEnumerable(Of SaveView) = Enumerable.Empty(Of SaveView)()

                savedViews = utility.SaveViewRepository.GetSavedViews(selectedAnalysisId)

                For Each item As SaveView In savedViews
                    item.Order = order
                    order += 1
                Next
                utility.SaveViewRepository.Save()

                Dim pageNumber As Integer = (If(page, 1))
                If (savedViews Is Nothing) Then
                    Return View(savedViews)
                Else
                    Return View(savedViews.ToPagedList(pageNumber, Page_Size))
                End If
            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Unable to Index() Views in SV-", ex.Message)
                Logger.Log.Error(String.Format("\n Unable to Index() with Message- {0} " + Environment.NewLine + "Stack Trace: {1} ", ex.Message, ex.StackTrace))
                Return View()
            Finally
                Logger.Log.Info(String.Format("SV - Views Index() Execution Ended"))
            End Try
        End Function

        <CustomActionFilter()>
        Public Function Delete(savedViewID As Integer) As ActionResult

            Logger.Log.Info(String.Format("Saved Reports Deletion Started {0}", savedViewID))

            Try
                savedviewrepository.DeleteSavedView(savedViewID)
                savedviewrepository.Save()
                TempData("Message") = "Saved Report deleted successfully."
                Logger.Log.Info(String.Format("Saved Report Deleted successfully with id {0}", savedViewID))
            Catch dataEx As DataException
                'Log the error
                TempData("ErrorMessage") = String.Concat("Unable to delete Saved Report - ", dataEx.Message)
                Logger.Log.Error(String.Format(Environment.NewLine + " Unable to Delete Saved Report id- {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2}", savedViewID, dataEx.Message, dataEx.StackTrace))
                Return RedirectToAction("Views", New System.Web.Routing.RouteValueDictionary() _
                                        From {{"id", savedViewID}, {"deleteChangesError", True}})
            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Unable to delete Saved Report-", ex.Message)
                Logger.Log.Error(String.Format(" Unable to Delete Saved Report id- {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2} ", savedViewID, ex.Message, ex.StackTrace))
            Finally
                Logger.Log.Info(String.Format("Execution Ended"))
            End Try
            Return RedirectToAction("Views")
        End Function

        <CustomActionFilter()>
        Function Edit(Id As Integer) As ViewResult
            Try
                Dim savedView = savedviewrepository.GetSavedViewDetail(Id)
                Logger.Log.Info(String.Format("Saved Report  updated successfully id- {0} ", Id))
                Return View(savedView)

            Catch dataEx As DataException
                TempData("ErrorMessage") = String.Concat("Unable to Edit Saved Report-", dataEx.Message)
                Logger.Log.Error(String.Format("Unable to Edit Saved Report id- {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2} ", Id, dataEx.Message, dataEx.StackTrace))
                ModelState.AddModelError("", "Unable to update analysis-." + dataEx.Message)
            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Unable to Edit Saved Report-", ex.Message)
                Logger.Log.Error(String.Format("Unable to Edit Saved Report id- {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2} ", Id, ex.Message, ex.StackTrace))
                ModelState.AddModelError("", "Unable to update Saved Report." + ex.Message)
            Finally
                Logger.Log.Info(String.Format("Execution Ended"))
            End Try
            Return Nothing
        End Function

        <HttpPost()>
        <CustomActionFilter()>
        Function Edit(SavedViewDetail As SaveView) As ActionResult
            Try
                If (Not Session("UserType") Is Nothing) And (Not Session("UserInfo") Is Nothing) Then
                    UserType = DirectCast(Session("UserType"), UserRole)
                    UserInfo = DirectCast(Session("UserInfo"), User)
                End If

                If ModelState.IsValid Then
                    savedviewrepository.UpdateSavedViewName(SavedViewDetail)
                    TempData("Message") = "Saved Report updated successfully."
                    Logger.Log.Info(String.Format("Saved Report Updated Successfully id- {0}", SavedViewDetail.ID))
                    Return RedirectToAction("Views")
                End If
            Catch dataEx As DataException
                TempData("ErrorMessage") = String.Concat("Unable to update Saved Report-", dataEx.Message)
                Logger.Log.Error(String.Format("Unable to Update Saved Report id- {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2}", SavedViewDetail.ID, dataEx.Message, dataEx.StackTrace))
               
            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Unable to update Saved Report-", ex.Message)
                Logger.Log.Error(String.Format("Unable to Update Saved Report id- {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2}", SavedViewDetail.ID, ex.Message, ex.StackTrace))

            Finally
                Logger.Log.Info(String.Format("Execution Ended"))
            End Try
            Return View(SavedViewDetail)
        End Function
        Protected Overrides Sub Dispose(disposing As Boolean)
            savedviewrepository.Dispose()
            utility.Dispose()
            MyBase.Dispose(disposing)
        End Sub
    End Class
End Namespace