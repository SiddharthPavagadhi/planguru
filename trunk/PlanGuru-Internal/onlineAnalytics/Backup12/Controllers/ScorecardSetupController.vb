﻿Imports FarPoint.CalcEngine
Imports FarPoint.Mvc.Spread
Imports System.Data.Entity
Imports PagedList

Namespace onlineAnalytics

    <CustAuthFilter()>
    Public Class ScorecardSetupController
        Inherits System.Web.Mvc.Controller
        Private utility As New Utility()
        'Define parameter 
        Private chartFormatRepository As IChartFormatRepository
        Private chartTypeRepository As IChartTypeRepository
        Private balanceRepository As IBalanceRepository
        Private scorecardRepository As IScorecardRepository
        Property accountRepository As IAccountRepository
        Property accountTypeRepository As IAccountTypeRepository

        Private chkIndex As Integer
        Private UserInfo As User
        Private CustomerId As String = Nothing
        Private pageSize As Integer
        Private selectedAccount As Integer
        Private intAnalysisID As Integer
        Private scorecardItemObj As Scorecard

        Public ReadOnly Property ScoredcardObj As Scorecard
            Get
                Return Me.scorecardItemObj
            End Get
        End Property

        'Get configured page_size from web.config
        Public ReadOnly Property Page_Size As Integer

            Get
                If (System.Configuration.ConfigurationManager.AppSettings("PageSize") <> "") Then
                    pageSize = 15 'System.Configuration.ConfigurationManager.AppSettings("PageSize").ToString()
                Else
                    pageSize = 10
                End If
                Return Me.pageSize
            End Get
        End Property
        '
        ' GET: /DashboardSetup
        Public Sub New()
            Me.chartFormatRepository = New ChartFormatRepository(New DataAccess())
            Me.chartTypeRepository = New ChartTypeRepository(New DataAccess())
            Me.scorecardRepository = New ScorecardRepository(New DataAccess())
            Me.accountRepository = New AccountRepository(New DataAccess())
            Me.accountTypeRepository = New AccountTypeRepository(New DataAccess())
        End Sub

        Public Sub New(chartFormatRepository As IChartFormatRepository)
            Me.chartFormatRepository = chartFormatRepository
        End Sub

        Public Sub New(chartTypeRepository As IChartTypeRepository)
            Me.chartTypeRepository = chartTypeRepository
        End Sub
        Public Sub New(scorecardRepository As IScorecardRepository)
            Me.scorecardRepository = scorecardRepository
        End Sub

        Public Sub New(accountRepository As IAccountRepository)
            Me.accountRepository = accountRepository
        End Sub

        Public Sub New(accountTypeRepository As IAccountTypeRepository)
            Me.accountTypeRepository = accountTypeRepository
        End Sub

        <CustomActionFilter()>
        Function Index(page As System.Nullable(Of Integer)) As ActionResult

            Logger.Log.Info(String.Format("Scorecard Item List Execution Started"))

            Try
                If (Not Session("UserType") Is Nothing) And (Not Session("UserInfo") Is Nothing) Then
                    UserInfo = DirectCast(Session("UserInfo"), User)
                    If (UserInfo.AnalysisId = 0) Then
                        Return RedirectToAction("Index", "Scorecard")
                    End If
                End If

                Dim strUserIDs As String = String.Empty
                If (UserInfo.UserRoleId = UserRoles.CRU) Then
                    strUserIDs = UserInfo.UserId
                Else
                        strUserIDs = UserInfo.UserId
                End If

                Dim scorecardDetail = From a In utility.ScorecardRepository.GetScorecardDetail(UserInfo.AnalysisId, True).OrderBy(Function(da) da.Order)

                If (scorecardDetail.Count > 0) Then
                    ViewBag.IsValid = scorecardDetail.FirstOrDefault().IsValid
                    If (ViewBag.IsValid = False) Then
                        TempData("InfoMessage") = "Scorecard items are no longer valid now, delete invalid items & add new scorecard items."
                    End If

                End If

                Dim pageNumber As Integer = (If(page, 1))
                If (scorecardDetail Is Nothing) Then
                    Return View(scorecardDetail)
                Else
                    Return View(scorecardDetail.ToPagedList(pageNumber, Page_Size))
                End If
            Catch ex As Exception
                Return View()
            Finally
                Logger.Log.Info(String.Format("Scorecard Item List Execution Ended"))
            End Try
        End Function

        <CustomActionFilter()>
        Function UpdateOrder(Id As Integer, fromposition As Integer, toPosition As Integer, direction As String) As JsonResult
            Dim scorecardDetail
            Try
                If (Not Session("UserType") Is Nothing) And (Not Session("UserInfo") Is Nothing) Then
                    UserInfo = DirectCast(Session("UserInfo"), User)
                    If (UserInfo.AnalysisId = 0) Then
                        'Return RedirectToAction("Index", "Dashboard")
                    End If
                End If

                If (direction.ToLower() = "back") Then

                    If UserInfo.currentTabIndex = 1 Then
                        scorecardDetail = utility.ScorecardRepository.GetScorecardDetail().Where(Function(d) d.AnalysisId = UserInfo.AnalysisId _
                                                                                                 And toPosition <= d.Order And d.Order <= fromposition).ToList()

                    Else
                        scorecardDetail = utility.ScorecardRepository.GetScorecardDetail().Where(Function(d) d.AnalysisId = UserInfo.AnalysisId _
                                                                                                 And toPosition <= d.Order And d.Order <= fromposition).ToList()

                    End If

                    For Each item In scorecardDetail
                        item.Order += 1
                    Next

                Else
                    If UserInfo.currentTabIndex = 1 Then
                        scorecardDetail = utility.ScorecardRepository.GetScorecardDetail().Where(Function(d) d.AnalysisId = UserInfo.AnalysisId _
                                                                                                And fromposition <= d.Order And d.Order <= toPosition).ToList()
                    Else
                        scorecardDetail = utility.ScorecardRepository.GetScorecardDetail().Where(Function(d) d.AnalysisId = UserInfo.AnalysisId _
                                                                                                And fromposition <= d.Order And d.Order <= toPosition).ToList()
                    End If

                    For Each item In scorecardDetail
                        item.Order -= 1
                    Next
                End If

                utility.ScorecardRepository.GetScorecardDetail().First(Function(d) d.ScorecardId = Id).Order = toPosition
                utility.ScorecardRepository.Save()

                Return Json("success", JsonRequestBehavior.AllowGet)
            Catch ex As Exception
                Return Json("success", JsonRequestBehavior.AllowGet)
            Finally

            End Try

        End Function

        <CustomActionFilter()>
        Function Create(<MvcSpread("spdRE")> spdRE As FpSpread, <MvcSpread("spdBS")> spdBS As FpSpread, <MvcSpread("spdCF")> spdCF As FpSpread, <MvcSpread("spdFR")> spdFR As FpSpread, <MvcSpread("spdOM")> spdOM As FpSpread) As ActionResult

            Logger.Log.Info(String.Format("Scorecard Setup Create View Execution Started"))
            Dim scorecardDetail As New Scorecard()
            Try

                If (Not Session("UserType") Is Nothing) And (Not Session("UserInfo") Is Nothing) Then
                    UserInfo = DirectCast(Session("UserInfo"), User)
                    If (UserInfo.AnalysisId = 0) Then
                        Return RedirectToAction("Index", "Scorecard")
                    Else
                        intAnalysisID = UserInfo.AnalysisId
                    End If
                End If

                scorecardDetail.VarIsFavorable = True
                PopulatePositiveVariance(scorecardDetail.VarIsFavorable)

                Return View(scorecardDetail)

            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Error occured during to View ScorecardSetup with Message- {0}" + Environment.NewLine + "Stack Trace: {1} ", ex.Message, ex.StackTrace)
                Logger.Log.Error(String.Format(Environment.NewLine + "Error occured during to View ScorecardSetup with with Message- {0} " + Environment.NewLine + "Stack Trace: {1} ", ex.Message, ex.StackTrace))
                Return View(scorecardDetail)
            Finally
                Logger.Log.Info(String.Format("Scorecard Setup Create View Execution Ended"))
            End Try
        End Function

        <HttpPost()> _
      <ValidateAntiForgeryToken()> _
      <CustomActionFilter()>
        Function Create(scorecardDetail As Scorecard, <MvcSpread("spdRE")> spdRE As FpSpread, <MvcSpread("spdBS")> spdBS As FpSpread, <MvcSpread("spdCF")> spdCF As FpSpread, <MvcSpread("spdFR")> spdFR As FpSpread, <MvcSpread("spdOM")> spdOM As FpSpread) As ActionResult

            Logger.Log.Info(String.Format("Scorecard setup create submission execution started"))
            Logger.Log.Info(String.Format("Scorecard creation started of scorecardId {0}", scorecardDetail.ScorecardId))

            Try
                If (Not Session("UserType") Is Nothing) And (Not Session("UserInfo") Is Nothing) Then
                    UserInfo = DirectCast(Session("UserInfo"), User)
                    CustomerId = UserInfo.CustomerId

                End If

                If ModelState.IsValid Then

                    Dim scorecardQuery = utility.ScorecardRepository.GetScorecardDetail(UserInfo.AnalysisId, True)

                    'Validate Account, if already exists show the message. 
                    'If (dashboardQuery.Where(Function(a) a.AccountId = dashboardDetail.AccountId).Count > 0) Then
                    '    TempData("ErrorMessage") = String.Concat("You already added this item into the list. Please select another.")
                    'Else

                    Dim orderCount As Integer

                  
                    orderCount = scorecardQuery.Select(Function(a) a.Order).Count()


                    Dim accountDetail = utility.AccountRepository.GetAccounts(scorecardDetail.AccountId, scorecardDetail.AcctTypeId, UserInfo.AnalysisId)

                    If (orderCount = 15) Then
                        TempData("ErrorMessage") = "You can add maximum 15 item from dashboard item list. Delete item, then pick any other item to add from the dashboard item list."
                        Return RedirectToAction("Index")
                    End If

                    scorecardDetail.Order = orderCount + 1
                    scorecardDetail.AnalysisId = UserInfo.AnalysisId
                  
                    scorecardDetail.UserId = UserInfo.UserId
                    scorecardDetail.ScorecardDesc = accountDetail.Description

                    scorecardDetail.Option1 = 0
                    scorecardDetail.Option2 = 0
                    scorecardDetail.Option3 = 0
                    scorecardDetail.IsValid = True

                    scorecardDetail.CreatedBy = UserInfo.UserId
                    scorecardDetail.UpdatedBy = UserInfo.UserId
                    utility.ScorecardRepository.Insert(scorecardDetail)
                    utility.ScorecardRepository.Save()

                    TempData("Message") = "Scorecard item created successfully."
                    Return RedirectToAction("Index")

                    'End If

                End If

            Catch dataEx As DataException
                TempData("ErrorMessage") = String.Concat("Unable to create dashboard item of DashboardId - {0} with Message- {1}", scorecardDetail.ScorecardId, dataEx.Message)
                Logger.Log.Error(String.Format(Environment.NewLine + " Unable to create dashboard item of DashboardId - {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2} ", scorecardDetail.ScorecardId, dataEx.Message, dataEx.StackTrace))

            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Unable to create dashboard item of DashboardId - {0} with Message- {1}", scorecardDetail.ScorecardId, ex.Message)
                Logger.Log.Error(String.Format(Environment.NewLine + " Unable to create dashboard item of DashboardId - {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2} ", scorecardDetail.ScorecardId, ex.Message, ex.StackTrace))

            Finally
                Logger.Log.Info(String.Format("Scorecard setup create submission execution ended"))
            End Try
            PopulatePositiveVariance(scorecardDetail.VarIsFavorable)

            Return View(scorecardDetail)
        End Function

        <CustomActionFilter()>
        Function Edit(scorecardId As Integer, <MvcSpread("spdRE")> spdRE As FpSpread, <MvcSpread("spdBS")> spdBS As FpSpread, <MvcSpread("spdCF")> spdCF As FpSpread, <MvcSpread("spdFR")> spdFR As FpSpread, <MvcSpread("spdOM")> spdOM As FpSpread) As ActionResult

            Logger.Log.Info(String.Format("To Edit Scorecard items started, it display item of scorecardId {0}", scorecardId))
            Dim scorecardItem As New Scorecard()
            Try
                If (Not Session("UserType") Is Nothing) And (Not Session("UserInfo") Is Nothing) Then
                    UserInfo = DirectCast(Session("UserInfo"), User)
                    CustomerId = UserInfo.CustomerId
                    If (UserInfo.AnalysisId = 0) Then
                        Return RedirectToAction("Index", "Dashboard")
                    Else
                        intAnalysisID = UserInfo.AnalysisId
                    End If
                End If

                scorecardItem = utility.ScorecardRepository.GetScorecardDetailById(scorecardId)

                If (Not scorecardItem Is Nothing) Then

                    Me.scorecardItemObj = scorecardItem

                    Dim accountTypeId As Integer = utility.AccountRepository.GetAccounts(scorecardItem.AccountId, scorecardItem.AnalysisId).AcctTypeId
                    scorecardItem.AcctTypeId = accountTypeId

                    If (scorecardItem.AnalysisId <> UserInfo.AnalysisId) Then
                        scorecardItem.AnalysisId = UserInfo.AnalysisId
                    End If

                    PopulatePositiveVariance(scorecardItem.VarIsFavorable)

                    Me.selectedAccount = scorecardItem.AccountId
                Else
                    Return RedirectToAction("Index", "Scorecard")
                End If

                Return View(scorecardItem)
            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Error occured during to display data to edit scorecard item of scorecardId - {0} with Message- {1}", scorecardId, ex.Message)
                Logger.Log.Error(String.Format(Environment.NewLine + "Error occured during to display data to edit scorecard item of scorecardId - {0} with Message- {1}" + Environment.NewLine + "Stack Trace: {2} ", scorecardId, ex.Message, ex.StackTrace))
                Return View()
            Finally
                Logger.Log.Info(String.Format("To Edit Scorecard items ended, it display item of scorecardId {0}", scorecardId))
            End Try
        End Function

        <HttpPost()> _
        <ValidateAntiForgeryToken()> _
        <CustomActionFilter()>
        Function Edit(scorecardDetail As Scorecard, <MvcSpread("spdRE")> spdRE As FpSpread, <MvcSpread("spdBS")> spdBS As FpSpread, <MvcSpread("spdCF")> spdCF As FpSpread, <MvcSpread("spdFR")> spdFR As FpSpread, <MvcSpread("spdOM")> spdOM As FpSpread) As ActionResult
            Logger.Log.Info(String.Format("Edited Scorecard item started of scorecardId {0}", scorecardDetail.ScorecardId))

            Try
                If (Not Session("UserType") Is Nothing) And (Not Session("UserInfo") Is Nothing) Then
                    UserInfo = DirectCast(Session("UserInfo"), User)
                    CustomerId = UserInfo.CustomerId

                End If

                If ModelState.IsValid Then

                    Dim accountDetail = utility.AccountRepository.GetAccounts.Where(Function(a) a.AccountId = scorecardDetail.AccountId And a.AcctTypeId = scorecardDetail.AcctTypeId And a.AnalysisId = UserInfo.AnalysisId).FirstOrDefault()

                    'Dim dashboardSelectedItem = userDashboardItemList.Where(Function(d) d.DashboardId = dashboardDetail.DashboardId).First()
                    Dim scorecardSelectedItem = utility.ScorecardRepository.GetScorecardDetailById(scorecardDetail.ScorecardId)

                    scorecardSelectedItem.Missed = scorecardDetail.Missed
                    scorecardSelectedItem.Outperform = scorecardDetail.Outperform
                    scorecardSelectedItem.ShowasPercent = scorecardDetail.ShowasPercent
                    scorecardSelectedItem.VarIsFavorable = scorecardDetail.VarIsFavorable
                    scorecardSelectedItem.ScorecardDesc = accountDetail.Description
                    scorecardSelectedItem.AccountId = scorecardDetail.AccountId
                    scorecardSelectedItem.AnalysisId = scorecardDetail.AnalysisId

                    scorecardSelectedItem.Option1 = 0
                    scorecardSelectedItem.Option2 = 0
                    scorecardSelectedItem.Option3 = 0

                    scorecardSelectedItem.UpdatedBy = UserInfo.UserId
                    utility.ScorecardRepository.UpdateScorecardDetail(scorecardSelectedItem)
                    utility.ScorecardRepository.Save()

                    TempData("Message") = "Scorecard item updated successfully."
                    Return RedirectToAction("Index")

                    'End If

                End If

                PopulatePositiveVariance(scorecardDetail.VarIsFavorable)

                Return View(scorecardDetail)
            Catch dataEx As DataException
                TempData("ErrorMessage") = String.Concat("Error occured to edited scorecard item of scorecardId - {0} with Message- {1}", scorecardDetail.ScorecardId, dataEx.Message)
                Logger.Log.Error(String.Format(Environment.NewLine + "Error occured to edit scorecard item of scorecardId - {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2} ", scorecardDetail.ScorecardId, dataEx.Message, dataEx.StackTrace))
                Return View(scorecardDetail)
            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Error occured to edited scorecard item of scorecardId - {0} with Message- {1}", scorecardDetail.ScorecardId, ex.Message)
                Logger.Log.Error(String.Format(Environment.NewLine + "Error occured to edit scorecard item of scorecardId - {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2} ", scorecardDetail.ScorecardId, ex.Message, ex.StackTrace))
                Return View(scorecardDetail)
            Finally
                Logger.Log.Info(String.Format("Edited Scorecard item ended of scorecardId {0}", scorecardDetail.ScorecardId))
            End Try
        End Function


        <CustomActionFilter()>
        Function Delete(scorecardId As Integer, orderId As Integer) As ActionResult
            Logger.Log.Info(String.Format("Scorecard item deleting started {0}", scorecardId))
            Dim deleteFlag = False
            Try
                If (Not Session("UserType") Is Nothing) And (Not Session("UserInfo") Is Nothing) Then
                    UserInfo = DirectCast(Session("UserInfo"), User)
                End If

                utility.ScorecardRepository.DeleteScorecardDetail(scorecardId)
                utility.ScorecardRepository.Save()
                TempData("Message") = "Scorecard item deleted successfully."
                Dim strUserIDs As String = String.Empty               
                strUserIDs = UserInfo.UserId


                deleteFlag = True
                Dim orderScorecardItem = From a In utility.ScorecardRepository.GetScorecardDetail().Where(Function(d) d.AnalysisId = UserInfo.AnalysisId And d.Order > orderId).OrderBy(Function(da) da.Order)
                For Each item In orderScorecardItem
                    item.Order -= 1
                Next

                utility.ScorecardRepository.Save()

                Logger.Log.Info(String.Format("Scorecard item deleted successfully id- {0}", scorecardId))
                Return RedirectToAction("Index")
            Catch ex As Exception
                If (deleteFlag = False) Then
                    TempData("ErrorMessage") = String.Format("Unable to delete scorecard item, ScorecardId - {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2}", scorecardId, ex.Message, ex.StackTrace)
                    Logger.Log.Error(String.Format("Unable to delete scorecard item, ScorecardId - {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2}", scorecardId, ex.Message, ex.StackTrace))
                ElseIf (deleteFlag = True) Then
                    TempData("ErrorMessage") = String.Format("Error occured during re-ordering scorecard items, after deleting item id - {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2}", scorecardId, ex.Message, ex.StackTrace)
                    Logger.Log.Error(String.Format("Error occured during re-ordering scorecard items, after deleting item id - {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2}", scorecardId, ex.Message, ex.StackTrace))
                End If
                Return RedirectToAction("Index")
            Finally
                Logger.Log.Info(String.Format("Scorecard item deleting ended {0}", scorecardId))
            End Try
        End Function

        <MvcSpreadEvent("Load", "spdRE", DirectCast(Nothing, String()))> _
        Private Sub FpRESpread_Load(sender As Object, e As EventArgs)
            Dim spread As FpSpread = DirectCast(sender, FpSpread)

            If Not spread.Page.IsPostBack Then
                LoadSpread(spread, "RE")

            End If

        End Sub

        <MvcSpreadEvent("Load", "spdBS", DirectCast(Nothing, String()))> _
        Private Sub FpBSSpread_Load(sender As Object, e As EventArgs)
            Dim spread As FpSpread = DirectCast(sender, FpSpread)
            If Not spread.Page.IsPostBack Then
                LoadSpread(spread, "BS")

            End If

        End Sub

        <MvcSpreadEvent("Load", "spdCF", DirectCast(Nothing, String()))> _
        Private Sub FpCFSpread_Load(sender As Object, e As EventArgs)
            Dim spread As FpSpread = DirectCast(sender, FpSpread)
            If Not spread.Page.IsPostBack Then
                LoadSpread(spread, "CF")

            End If

        End Sub

        <MvcSpreadEvent("Load", "spdFR", DirectCast(Nothing, String()))> _
        Private Sub FpFRSpread_Load(sender As Object, e As EventArgs)
            Dim spread As FpSpread = DirectCast(sender, FpSpread)
            If Not spread.Page.IsPostBack Then
                LoadSpread(spread, "FR")

            End If

        End Sub

        <MvcSpreadEvent("Load", "spdOM", DirectCast(Nothing, String()))> _
        Private Sub FpOMSpread_Load(sender As Object, e As EventArgs)
            Dim spread As FpSpread = DirectCast(sender, FpSpread)
            If Not spread.Page.IsPostBack Then
                LoadSpread(spread, "OM")

            End If

        End Sub

        Private Sub LoadSpread(ByRef spread As FpSpread, strType As String)


            spread.Sheets(0).DefaultStyle.Font.Size = FontSize.Large
            spread.HorizontalScrollBarPolicy = FarPoint.Web.Spread.ScrollBarPolicy.Never
            spread.CommandBar.Visible = False
            '  spread.Sheets(0).FrozenColumnCount = 4
            spread.Sheets(0).ColumnCount = 4
            spread.Sheets(0).RowCount = 0
            spread.Sheets(0).Rows(-1).Height = 18
            spread.Sheets(0).Columns(0, 1).Width = 20
            spread.Sheets(0).Columns(2).Width = 0
            spread.Sheets(0).Columns(3).Width = 740
            spread.Sheets(0).Columns(3).Locked = True
            spread.Sheets(0).Rows(-1).VerticalAlign = VerticalAlign.Middle
            'set column headers 
            spread.Sheets(0).ColumnHeader.Rows(0).Font.Size = FontSize.Large
            spread.Sheets(0).ColumnHeader.Rows(0).Height = 20
            spread.Sheets(0).ColumnHeader.Cells(0, 0).Text = " "
            spread.Sheets(0).ColumnHeader.Cells(0, 1).Text = " "
            spread.Sheets(0).ColumnHeader.Cells(0, 2).Text = " "
            spread.Sheets(0).ColumnHeader.Cells(0, 3).Text = "Item to Display in Scorecard "
            spread.Sheets(0).SelectionPolicy = FarPoint.Web.Spread.Model.SelectionPolicy.Single
            spread.Sheets(0).SelectionBackColorStyle = FarPoint.Web.Spread.SelectionBackColorStyles.None
            spread.VerticalScrollBarPolicy = FarPoint.Web.Spread.ScrollBarPolicy.Always
            Select Case strType
                Case "RE"
                    LoadspdItems(spread)
                Case "BS"
                    LoadspdItems(spread)
                Case "CF"
                    LoadspdItems(spread)
                Case "FR"
                    LoadspdItems(spread)
                Case "OM"
                    LoadspdItems(spread)
            End Select
            spread.ActiveSheetView.PageSize = spread.Sheets(0).RowCount

        End Sub

        Public Sub LoadspdItems(spread As FpSpread)
            Using context As New DataAccess
                Dim intRowCtr As Integer = -1
                Dim intSpanRows(1) As Integer
                Dim chkbox As FarPoint.Web.Spread.CheckBoxCellType

                Dim blnTotalFlag As Boolean = False
                Dim dropdown As New FarPoint.Web.Spread.ComboBoxCellType
                Dim cbstr As String()
                cbstr = New String() {"Bar", "Line"}


                dropdown.Items = cbstr

                Dim selectedAccounts = Nothing

                Select Case spread.ClientID
                    Case "spdRE"

                        selectedAccounts = (From a In utility.AccountRepository.GetAccountRecordsByAnalysisId(intAnalysisID)
                                            Join ac In utility.AccountTypeRepository.GetAccountTypeRecordsByAnalysisId(intAnalysisID, New String() {"revenue", "expense"})
                                            On a.AcctTypeId Equals ac.AcctTypeId Where a.AnalysisId = ac.AnalysisId
                                            Order By a.SortSequence
                                            Select a).ToList()

                    Case "spdBS"

                        selectedAccounts = (From a In utility.AccountRepository.GetAccountRecordsByAnalysisId(intAnalysisID)
                                            Join ac In utility.AccountTypeRepository.GetAccountTypeRecordsByAnalysisId(intAnalysisID, New String() {"assets", "liabilities and equity"})
                                            On a.AcctTypeId Equals ac.AcctTypeId Where a.AnalysisId = ac.AnalysisId
                                            Order By a.SortSequence
                                            Select a).ToList()
                    Case "spdCF"

                        selectedAccounts = (From a In utility.AccountRepository.GetAccountRecordsByAnalysisId(intAnalysisID)
                                            Join ac In utility.AccountTypeRepository.GetAccountTypeRecordsByAnalysisId(intAnalysisID, New String() {"cashflow"})
                                            On a.AcctTypeId Equals ac.AcctTypeId Where a.AnalysisId = ac.AnalysisId
                                            Order By a.SortSequence
                                           Select a).ToList()

                    Case "spdFR"
                        selectedAccounts = (From a In utility.AccountRepository.GetAccountRecordsByAnalysisId(intAnalysisID)
                                       Join ac In utility.AccountTypeRepository.GetAccountTypeRecordsByAnalysisId(intAnalysisID, New String() {"ratio"})
                                       On a.AcctTypeId Equals ac.AcctTypeId Where a.AnalysisId = ac.AnalysisId
                                       Order By a.SortSequence
                                       Select a).ToList()


                    Case "spdOM"

                        selectedAccounts = (From a In utility.AccountRepository.GetAccountRecordsByAnalysisId(intAnalysisID)
                                          Join ac In utility.AccountTypeRepository.GetAccountTypeRecordsByAnalysisId(intAnalysisID, New String() {"nonfinancial"})
                                          On a.AcctTypeId Equals ac.AcctTypeId Where a.AnalysisId = ac.AnalysisId
                                          Order By a.SortSequence
                                          Select a).ToList()
                End Select

                If (Not selectedAccounts Is Nothing) Then
                    For Each account As Account In selectedAccounts
                        If account.AcctDescriptor <> "SHeading" Then
                            intRowCtr += 1
                            spread.Sheets(0).AddRows(intRowCtr, 1)
                            ' spread.Sheets(0).Rows(intRowCtr).Height = 18
                            spread.Sheets(0).SetValue(intRowCtr, 3, account.Description)
                            spread.Sheets(0).SetValue(intRowCtr, 2, account.AcctDescriptor)

                            Select Case account.AcctDescriptor
                                Case "Heading"
                                    If spread.ID <> "spdFR" Then
                                        CommonProcedures.FormatHeaderRow(spread, intRowCtr, intSpanRows, blnTotalFlag, 3)
                                    Else
                                        CommonProcedures.FormatHeaderRatioRow(spread, intRowCtr, intSpanRows, False, 3)
                                    End If
                                Case "Total"
                                    CommonProcedures.FormatTotalRow(spread, intRowCtr, intSpanRows, blnTotalFlag, 0, 0, account.ClassDesc, "", 3)
                                    chkbox = New FarPoint.Web.Spread.CheckBoxCellType
                                    chkbox.OnClientClick = "DSChkBoxClicked('checkbox" + String.Concat(chkIndex) + "'," + account.AccountId.ToString() + "," + account.AcctTypeId.ToString() + ",'" + account.Description + "');"
                                    spread.Sheets(0).Cells(intRowCtr, 1).CellType = chkbox
                                    If (account.AccountId = Me.selectedAccount) Then
                                        spread.Sheets(0).DataModel.SetValue(intRowCtr, 1, True)
                                        Me.scorecardItemObj.selectedTabId = spread.ClientID
                                        Me.scorecardItemObj.selectedCheckboxId = String.Concat("#checkbox", chkIndex)
                                        Me.scorecardItemObj.selectedCheckboxDescription = account.Description

                                    End If
                                    chkIndex += 1
                                Case "Detail", "SubDetail", "SubEnd", "OpCash", "NetIncrease", "BegCash", "EndCash", "SDetail", "STotal"
                                    spread.Sheets(0).Cells(intRowCtr, 0).Border.BorderColorBottom = Drawing.Color.White
                                    chkbox = New FarPoint.Web.Spread.CheckBoxCellType
                                    chkbox.OnClientClick = "DSChkBoxClicked('checkbox" + String.Concat(chkIndex) + "'," + account.AccountId.ToString() + "," + account.AcctTypeId.ToString() + ",'" + account.Description + "');"
                                    spread.Sheets(0).Cells(intRowCtr, 1).CellType = chkbox
                                    If (account.AccountId = Me.selectedAccount) Then
                                        spread.Sheets(0).DataModel.SetValue(intRowCtr, 1, True)
                                        Me.scorecardItemObj.selectedTabId = spread.ClientID
                                        Me.scorecardItemObj.selectedCheckboxId = String.Concat("#checkbox", chkIndex)
                                        Me.scorecardItemObj.selectedCheckboxDescription = account.Description
                                    End If
                                    chkIndex += 1
                                Case "ATotal", "LTotal", "LCTotal", "GPTotal", "IOTotal", "IBTTotal", "NITotal"
                                    CommonProcedures.FormatGrandTotalRow(spread, intRowCtr, blnTotalFlag, 0, 0, account.ClassDesc, "", 3)
                                    chkbox = New FarPoint.Web.Spread.CheckBoxCellType
                                    chkbox.OnClientClick = "DSChkBoxClicked('checkbox" + String.Concat(chkIndex) + "'," + account.AccountId.ToString() + "," + account.AcctTypeId.ToString() + ",'" + account.Description + "');"
                                    spread.Sheets(0).Cells(intRowCtr, 1).CellType = chkbox
                                    If (account.AccountId = Me.selectedAccount) Then
                                        spread.Sheets(0).DataModel.SetValue(intRowCtr, 1, True)
                                        Me.scorecardItemObj.selectedTabId = spread.ClientID
                                        Me.scorecardItemObj.selectedCheckboxId = String.Concat("#checkbox", chkIndex)
                                        Me.scorecardItemObj.selectedCheckboxDescription = account.Description
                                    End If
                                    chkIndex += 1
                            End Select
                        End If
                    Next
                End If
            End Using
        End Sub

        Private Sub PopulatePositiveVariance(Optional selectedVariance As Boolean = True)

            Logger.Log.Info(String.Format("PopulatePositiveVariance Execution Started"))
            Dim variance As List(Of SelectListItem) = New List(Of SelectListItem)
            Try

                variance.Add(New SelectListItem With {.Text = [Enum].GetName(GetType(PositiveVariance), PositiveVariance.Favorable), .Value = Convert.ToBoolean(PositiveVariance.Favorable), .Selected = True})
                variance.Add(New SelectListItem With {.Text = [Enum].GetName(GetType(PositiveVariance), PositiveVariance.Unfavorable), .Value = Convert.ToBoolean(PositiveVariance.Unfavorable)})

                ViewBag.PositiveVariance = New SelectList(variance, "value", "text", selectedVariance)

            Catch ex As Exception
                Logger.Log.Info(String.Format("PopulatePositiveVariance Execution Ended"))
            End Try

        End Sub

    End Class




End Namespace