﻿Imports FarPoint.CalcEngine
Imports FarPoint.Mvc.Spread
Imports System.Data.Entity
Imports PagedList

Namespace onlineAnalytics

    <CustAuthFilter()>
    Public Class DashboardSetupController
        Inherits System.Web.Mvc.Controller
        Private utility As New Utility()
        'Define parameter 
        Private chartFormatRepository As IChartFormatRepository
        Private chartTypeRepository As IChartTypeRepository
        Private balanceRepository As IBalanceRepository
        Private dashboardRepository As IDashboardRepository
        Property accountRepository As IAccountRepository
        Property accountTypeRepository As IAccountTypeRepository

        Private chkIndex As Integer
        Private UserInfo As User
        Private CustomerId As String = Nothing
        Private pageSize As Integer
        Private selectedAccount As Integer
        Private intAnalysisID As Integer
        Private dashboardItemObj As Dashboard

        Public ReadOnly Property DashboardObj As Dashboard
            Get
                Return Me.dashboardItemObj
            End Get
        End Property

        'Get configured page_size from web.config
        Public ReadOnly Property Page_Size As Integer

            Get
                If (System.Configuration.ConfigurationManager.AppSettings("PageSize") <> "") Then
                    pageSize = 15 'System.Configuration.ConfigurationManager.AppSettings("PageSize").ToString()
                Else
                    pageSize = 10
                End If
                Return Me.pageSize
            End Get
        End Property
        '
        ' GET: /DashboardSetup
        Public Sub New()
            Me.chartFormatRepository = New ChartFormatRepository(New DataAccess())
            Me.chartTypeRepository = New ChartTypeRepository(New DataAccess())
            Me.dashboardRepository = New DashboardRepository(New DataAccess())
            Me.accountRepository = New AccountRepository(New DataAccess())
            Me.accountTypeRepository = New AccountTypeRepository(New DataAccess())
        End Sub

        Public Sub New(chartFormatRepository As IChartFormatRepository)
            Me.chartFormatRepository = chartFormatRepository
        End Sub

        Public Sub New(chartTypeRepository As IChartTypeRepository)
            Me.chartTypeRepository = chartTypeRepository
        End Sub
        Public Sub New(dashboardRepository As IDashboardRepository)
            Me.dashboardRepository = dashboardRepository
        End Sub

        Public Sub New(accountRepository As IAccountRepository)
            Me.accountRepository = accountRepository
        End Sub

        Public Sub New(accountTypeRepository As IAccountTypeRepository)
            Me.accountTypeRepository = accountTypeRepository
        End Sub

        <CustomActionFilter()>
        Function Index(page As System.Nullable(Of Integer)) As ActionResult

            Logger.Log.Info(String.Format("Dashboard Item List Execution Started"))

            Try
                If (Not Session("UserType") Is Nothing) And (Not Session("UserInfo") Is Nothing) Then
                    UserInfo = DirectCast(Session("UserInfo"), User)
                    If (UserInfo.AnalysisId = 0) Then
                        Return RedirectToAction("Index", "Dashboard")
                    End If
                End If

                Dim strUserIDs As String = String.Empty
                If (UserInfo.UserRoleId = UserRoles.CRU) Then
                    strUserIDs = UserInfo.UserId
                Else
                    If UserInfo.currentTabIndex = 1 Then
                        strUserIDs = "-999"
                    Else
                        strUserIDs = UserInfo.UserId
                    End If
                End If

                Dim dashboardDetail = From a In utility.DashboardRepository.GetDashboardDetail().Where(Function(d) (strUserIDs.Contains(d.UserId)) And d.AnalysisId = UserInfo.AnalysisId).OrderBy(Function(da) da.Order)

                If (dashboardDetail.Count > 0) Then
                    Dim intCtr As Integer
                    For intCtr = 0 To dashboardDetail.Count - 1
                        Select Case dashboardDetail(intCtr).ChartFormatId
                            Case 6
                                If dashboardDetail(intCtr).ShowBudget = True Then
                                    dashboardDetail(intCtr).ChartFormatId = 7
                                End If
                            Case 7
                                dashboardDetail(intCtr).ChartFormatId = 10
                            Case 8
                                dashboardDetail(intCtr).ChartFormatId = 11
                            Case 9
                                dashboardDetail(intCtr).ChartFormatId = 12
                            Case 10
                                dashboardDetail(intCtr).ChartFormatId = 9
                            Case 13
                                dashboardDetail(intCtr).ChartFormatId = 8

                        End Select
                    Next
                    ViewBag.IsValid = dashboardDetail.FirstOrDefault().IsValid
                    If (ViewBag.IsValid = False) Then
                        TempData("InfoMessage") = "Dashboard items are no longer valid now, delete invalid items & add new dashboard items."
                    End If

                End If

                Dim pageNumber As Integer = (If(page, 1))
                If (dashboardDetail Is Nothing) Then
                    Return View(dashboardDetail)
                Else
                    Return View(dashboardDetail.ToPagedList(pageNumber, Page_Size))
                End If
            Catch ex As Exception
                Return View()
            Finally
                Logger.Log.Info(String.Format("Dashboard Item List Execution Ended"))
            End Try
        End Function

        <CustomActionFilter()>
        Function UpdateOrder(Id As Integer, fromposition As Integer, toPosition As Integer, direction As String) As JsonResult
            Dim dashboardDetail
            Try
                If (Not Session("UserType") Is Nothing) And (Not Session("UserInfo") Is Nothing) Then
                    UserInfo = DirectCast(Session("UserInfo"), User)
                    If (UserInfo.AnalysisId = 0) Then
                        'Return RedirectToAction("Index", "Dashboard")
                    End If
                End If

                If (direction.ToLower() = "back") Then

                    If UserInfo.currentTabIndex = 1 Then
                        dashboardDetail = utility.DashboardRepository.GetDashboardDetail().Where(Function(d) (d.UserId = "-999") And d.AnalysisId = UserInfo.AnalysisId _
                                                                                                 And toPosition <= d.Order And d.Order <= fromposition).ToList()

                    Else
                        dashboardDetail = utility.DashboardRepository.GetDashboardDetail().Where(Function(d) d.UserId = UserInfo.UserId And d.AnalysisId = UserInfo.AnalysisId _
                                                                                                 And toPosition <= d.Order And d.Order <= fromposition).ToList()

                    End If
                    
                    For Each item In dashboardDetail
                        item.Order += 1
                    Next

                Else
                    If UserInfo.currentTabIndex = 1 Then
                        dashboardDetail = utility.DashboardRepository.GetDashboardDetail().Where(Function(d) (d.UserId = "-999") And d.AnalysisId = UserInfo.AnalysisId _
                                                                                                And fromposition <= d.Order And d.Order <= toPosition).ToList()
                    Else
                        dashboardDetail = utility.DashboardRepository.GetDashboardDetail().Where(Function(d) d.UserId = UserInfo.UserId And d.AnalysisId = UserInfo.AnalysisId _
                                                                                                And fromposition <= d.Order And d.Order <= toPosition).ToList()
                    End If

                    For Each item In dashboardDetail
                        item.Order -= 1
                    Next
                End If

                utility.DashboardRepository.GetDashboardDetail().First(Function(d) d.DashboardId = Id).Order = toPosition

                utility.DashboardRepository.Save()
                Return Json("success", JsonRequestBehavior.AllowGet)
            Catch ex As Exception
                Return Json("success", JsonRequestBehavior.AllowGet)
            Finally

            End Try

        End Function

        <CustomActionFilter()>
        Function Create(<MvcSpread("spdRE")> spdRE As FpSpread, <MvcSpread("spdBS")> spdBS As FpSpread, <MvcSpread("spdCF")> spdCF As FpSpread, <MvcSpread("spdFR")> spdFR As FpSpread, <MvcSpread("spdOM")> spdOM As FpSpread) As ActionResult

            Logger.Log.Info(String.Format("Dashboard Setup Create View Execution Started"))
            Dim dashboardDetail As New Dashboard()
            Try

                If (Not Session("UserType") Is Nothing) And (Not Session("UserInfo") Is Nothing) Then
                    UserInfo = DirectCast(Session("UserInfo"), User)
                    If (UserInfo.AnalysisId = 0) Then
                        Return RedirectToAction("Index", "Dashboard")
                    Else
                        intAnalysisID = UserInfo.AnalysisId
                    End If
                End If

                PopulateChartFormat(0)
                PopulateChartType(1)

                Return View(dashboardDetail)

            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Error occured during to View DashboardSetup with Message- {0}" + Environment.NewLine + "Stack Trace: {1} ", ex.Message, ex.StackTrace)
                Logger.Log.Error(String.Format(Environment.NewLine + "Error occured during to View DashboardSetup with with Message- {0} " + Environment.NewLine + "Stack Trace: {1} ", ex.Message, ex.StackTrace))
                Return View(dashboardDetail)
            Finally
                Logger.Log.Info(String.Format("Dashboard Setup Create View Execution Ended"))
            End Try
        End Function

        <HttpPost()> _
      <ValidateAntiForgeryToken()> _
      <CustomActionFilter()>
        Function Create(dashboardDetail As Dashboard, <MvcSpread("spdRE")> spdRE As FpSpread, <MvcSpread("spdBS")> spdBS As FpSpread, <MvcSpread("spdCF")> spdCF As FpSpread, <MvcSpread("spdFR")> spdFR As FpSpread, <MvcSpread("spdOM")> spdOM As FpSpread) As ActionResult


            Logger.Log.Info(String.Format("Dashboard Setup Create Submission Execution Started"))
            Logger.Log.Info(String.Format("Dashboard Creation started of DashboardId {0}", dashboardDetail.DashboardId))

            Try
                If (Not Session("UserType") Is Nothing) And (Not Session("UserInfo") Is Nothing) Then
                    UserInfo = DirectCast(Session("UserInfo"), User)
                    CustomerId = UserInfo.CustomerId

                End If

                If ModelState.IsValid Then

                    Dim dashboardQuery = utility.DashboardRepository.GetDashboardDetail(UserInfo.UserId, UserInfo.AnalysisId, True)
                    Dim sharedDashboardQuery = utility.DashboardRepository.GetDashboardDetail("-999", UserInfo.AnalysisId, True)

                    'Validate Account, if already exists show the message. 
                    'If (dashboardQuery.Where(Function(a) a.AccountId = dashboardDetail.AccountId).Count > 0) Then
                    '    TempData("ErrorMessage") = String.Concat("You already added this item into the list. Please select another.")
                    'Else

                    Dim orderCount As Integer

                    If UserInfo.currentTabIndex = 1 Then
                        orderCount = sharedDashboardQuery.Select(Function(a) a.Order).Count()
                    Else
                        orderCount = dashboardQuery.Select(Function(a) a.Order).Count()
                    End If

                    Dim accountDetail = utility.AccountRepository.GetAccounts(dashboardDetail.AccountId, dashboardDetail.AcctTypeId, UserInfo.AnalysisId)

                    If (orderCount = 15) Then
                        TempData("ErrorMessage") = "You can add maximum 15 item from dashboard item list. Delete item, then pick any other item to add from the dashboard item list."
                        Return RedirectToAction("Index")
                    End If

                    dashboardDetail.Order = orderCount + 1
                    dashboardDetail.AnalysisId = UserInfo.AnalysisId

                    'Check if Shared Analysis option is set, For shared analysis we have UserID as -999
                    If UserInfo.currentTabIndex = 1 Then
                        dashboardDetail.UserId = "-999"
                    Else
                        dashboardDetail.UserId = UserInfo.UserId
                    End If

                    dashboardDetail.DashDescription = accountDetail.Description

                    dashboardDetail.ChartType = utility.ChartTypeRepository().GetChartTypes().Where(Function(ch) ch.ChartTypeId = dashboardDetail.ChartTypeId).FirstOrDefault()

                    If (dashboardDetail.ChartType.Type.ToLower() <> "line") Then
                        dashboardDetail.ShowTrendline = False
                    End If

                    dashboardDetail.Period1Goal = 0
                    dashboardDetail.GoalGrowthRate = 0
                    dashboardDetail.ShowGoal = False
                    'ew 2/15 chgs for new formats
                    Select Case dashboardDetail.ChartFormatId
                        Case 6 'current year actuals
                            dashboardDetail.ShowBudget = False
                        Case 7 'current year forecast
                            dashboardDetail.ChartFormatId = 6
                            dashboardDetail.ShowBudget = True
                        Case 8  'current year budget
                            dashboardDetail.ChartFormatId = 13
                        Case 9  '12 month rolling forecast
                            dashboardDetail.ChartFormatId = 10
                        Case 10  'last 12 months
                            dashboardDetail.ChartFormatId = 7
                        Case 11  'multi year trend
                            dashboardDetail.ChartFormatId = 8
                        Case 12  'multi year trend ytd
                            dashboardDetail.ChartFormatId = 9
                    End Select
                    'If (dashboardDetail.ChartFormatId = 6) Then
                    '    dashboardDetail.ShowBudget = dashboardDetail.ShowBudget
                    'Else
                    '    dashboardDetail.ShowBudget = True
                    'End If
                    dashboardDetail.IsValid = True
                    dashboardDetail.CreatedBy = UserInfo.UserId
                    dashboardDetail.UpdatedBy = UserInfo.UserId
                    utility.DashboardRepository.Insert(dashboardDetail)
                    utility.DashboardRepository.Save()

                    TempData("Message") = "Dashboard item created successfully."
                    Return RedirectToAction("Index")

                    'End If

                End If

            Catch dataEx As DataException
                TempData("ErrorMessage") = String.Concat("Unable to create dashboard item of DashboardId - {0} with Message- {1}", dashboardDetail.DashboardId, dataEx.Message)
                Logger.Log.Error(String.Format(Environment.NewLine + " Unable to create dashboard item of DashboardId - {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2} ", dashboardDetail.DashboardId, dataEx.Message, dataEx.StackTrace))

            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Unable to create dashboard item of DashboardId - {0} with Message- {1}", dashboardDetail.DashboardId, ex.Message)
                Logger.Log.Error(String.Format(Environment.NewLine + " Unable to create dashboard item of DashboardId - {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2} ", dashboardDetail.DashboardId, ex.Message, ex.StackTrace))

            Finally
                Logger.Log.Info(String.Format("Dashboard Setup Create Submission Execution Ended"))
            End Try
            PopulateChartFormat(0)
            PopulateChartType(1)

            Return View(dashboardDetail)
        End Function

        <CustomActionFilter()>
        Function Edit(dashboardId As Integer, <MvcSpread("spdRE")> spdRE As FpSpread, <MvcSpread("spdBS")> spdBS As FpSpread, <MvcSpread("spdCF")> spdCF As FpSpread, <MvcSpread("spdFR")> spdFR As FpSpread, <MvcSpread("spdOM")> spdOM As FpSpread) As ActionResult

            Logger.Log.Info(String.Format("To Edit Dashboard items started, it display item of DashboardId {0}", dashboardId))
            Dim dashboardItem As New Dashboard()
            Try
                If (Not Session("UserType") Is Nothing) And (Not Session("UserInfo") Is Nothing) Then
                    UserInfo = DirectCast(Session("UserInfo"), User)
                    CustomerId = UserInfo.CustomerId
                    If (UserInfo.AnalysisId = 0) Then
                        Return RedirectToAction("Index", "Dashboard")
                    Else
                        intAnalysisID = UserInfo.AnalysisId
                    End If
                End If

                dashboardItem = utility.DashboardRepository.GetDashboardDetail().Where(Function(d) d.DashboardId = dashboardId).FirstOrDefault()
                Select Case dashboardItem.ChartFormatId
                    Case 6
                        If dashboardItem.ShowBudget = True Then
                            dashboardItem.ChartFormatId = 7
                        End If
                    Case 7
                        dashboardItem.ChartFormatId = 10
                    Case 8
                        dashboardItem.ChartFormatId = 11
                    Case 9
                        dashboardItem.ChartFormatId = 12
                    Case 10
                        dashboardItem.ChartFormatId = 9
                    Case 13
                        dashboardItem.ChartFormatId = 8
                End Select
                If dashboardItem.ChartFormatId = 6 Then
                    
                End If
                If (Not dashboardItem Is Nothing) Then

                    Me.dashboardItemObj = dashboardItem

                    Dim accountTypeId As Integer = utility.AccountRepository.GetAccounts(dashboardItem.AccountId, dashboardItem.AnalysisId).AcctTypeId
                    dashboardItem.AcctTypeId = accountTypeId

                    If (dashboardItem.AnalysisId <> UserInfo.AnalysisId) Then
                        dashboardItem.AnalysisId = UserInfo.AnalysisId
                    End If
                    PopulateChartFormat(dashboardItem.ChartFormatId)
                    PopulateChartType(dashboardItem.ChartTypeId)
                    Me.selectedAccount = dashboardItem.AccountId
                Else
                    Return RedirectToAction("Index", "Dashboard")
                End If

                Return View(dashboardItem)
            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Error occured during to display data to edit dashboard item of DashboardId - {0} with Message- {1}", dashboardId, ex.Message)
                Logger.Log.Error(String.Format(Environment.NewLine + "Error occured during to display data to edit dashboard item of DashboardId - {0} with Message- {1}" + Environment.NewLine + "Stack Trace: {2} ", dashboardId, ex.Message, ex.StackTrace))
                Return View()
            Finally
                Logger.Log.Info(String.Format("To Edit Dashboard items ended, it display item of DashboardId {0}", dashboardId))
            End Try
        End Function

        <HttpPost()> _
        <ValidateAntiForgeryToken()> _
        <CustomActionFilter()>
        Function Edit(dashboardDetail As Dashboard, <MvcSpread("spdRE")> spdRE As FpSpread, <MvcSpread("spdBS")> spdBS As FpSpread, <MvcSpread("spdCF")> spdCF As FpSpread, <MvcSpread("spdFR")> spdFR As FpSpread, <MvcSpread("spdOM")> spdOM As FpSpread) As ActionResult
            Logger.Log.Info(String.Format("Edited Dashboard item started of DashboardId {0}", dashboardDetail.DashboardId))

            Try
                If (Not Session("UserType") Is Nothing) And (Not Session("UserInfo") Is Nothing) Then
                    UserInfo = DirectCast(Session("UserInfo"), User)
                    CustomerId = UserInfo.CustomerId

                End If

                If ModelState.IsValid Then

                    Dim userDashboardItemList = utility.DashboardRepository.GetDashboardDetail().Where(Function(d) d.UserId = UserInfo.UserId)

                    'Validate Account, if already exists show the message. 
                    'If (userDashboardItemList.Where(Function(a) a.AccountId = dashboardDetail.AccountId And a.DashboardId <> dashboardDetail.DashboardId).Count > 0) Then
                    '    TempData("ErrorMessage") = String.Concat("You already added this item into the list. Please select another.")
                    'Else

                    Dim accountDetail = utility.AccountRepository.GetAccounts.Where(Function(a) a.AccountId = dashboardDetail.AccountId And a.AcctTypeId = dashboardDetail.AcctTypeId And a.AnalysisId = UserInfo.AnalysisId).FirstOrDefault()

                    'Dim dashboardSelectedItem = userDashboardItemList.Where(Function(d) d.DashboardId = dashboardDetail.DashboardId).First()
                    Dim dashboardSelectedItem = utility.DashboardRepository.GetDashboardDetail().Where(Function(d) d.DashboardId = dashboardDetail.DashboardId).First()

                    dashboardSelectedItem.ChartFormatId = dashboardDetail.ChartFormatId
                    dashboardSelectedItem.ChartTypeId = dashboardDetail.ChartTypeId
                    dashboardSelectedItem.AccountId = dashboardDetail.AccountId
                    dashboardSelectedItem.AnalysisId = dashboardDetail.AnalysisId


                    dashboardDetail.ChartType = utility.ChartTypeRepository().GetChartTypes().Where(Function(ch) ch.ChartTypeId = dashboardDetail.ChartTypeId).FirstOrDefault()

                    If (dashboardDetail.ChartType.Type.ToLower() <> "line") Then
                        dashboardDetail.ShowTrendline = False
                    End If

                    dashboardSelectedItem.Period1Goal = 0
                    dashboardSelectedItem.GoalGrowthRate = 0
                    dashboardSelectedItem.ShowGoal = False

                    dashboardSelectedItem.ShowAsPercent = dashboardDetail.ShowAsPercent
                    dashboardSelectedItem.ShowTrendline = dashboardDetail.ShowTrendline

                    'ew 2/15 chgs for new formats
                    Select Case dashboardDetail.ChartFormatId
                        Case 6 'current year actuals
                            dashboardSelectedItem.ShowBudget = False
                        Case 7 'current year forecast
                            dashboardSelectedItem.ChartFormatId = 6
                            dashboardSelectedItem.ShowBudget = True
                        Case 8  'current year budget
                            dashboardSelectedItem.ChartFormatId = 13
                        Case 9  '12 month rolling forecast
                            dashboardSelectedItem.ChartFormatId = 10
                        Case 10  'last 12 months
                            dashboardSelectedItem.ChartFormatId = 7
                        Case 11  'multi year trend
                            dashboardSelectedItem.ChartFormatId = 8
                        Case 12  'multi year trend ytd
                            dashboardSelectedItem.ChartFormatId = 9
                    End Select
                    'If (dashboardDetail.ChartFormatId = 6) Then
                    '    dashboardSelectedItem.ShowBudget = dashboardDetail.ShowBudget
                    'Else
                    '    dashboardSelectedItem.ShowBudget = True
                    'End If

                    dashboardSelectedItem.DashDescription = accountDetail.Description
                    dashboardDetail.UpdatedBy = UserInfo.UserId
                    utility.DashboardRepository.UpdateDashboardDetail(dashboardSelectedItem)
                    utility.DashboardRepository.Save()

                    TempData("Message") = "Dashboard item updated successfully."
                    Return RedirectToAction("Index")

                    'End If

                End If
                PopulateChartFormat(dashboardDetail.ChartFormatId)
                PopulateChartType(dashboardDetail.ChartTypeId)
                Return View(dashboardDetail)
            Catch dataEx As DataException
                TempData("ErrorMessage") = String.Concat("Error occured to edited dashboard item of DashboardId - {0} with Message- {1}", dashboardDetail.DashboardId, dataEx.Message)
                Logger.Log.Error(String.Format(Environment.NewLine + "Error occured to edit dashboard item of DashboardId - {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2} ", dashboardDetail.DashboardId, dataEx.Message, dataEx.StackTrace))
                Return View(dashboardDetail)
            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Error occured to edited dashboard item of DashboardId - {0} with Message- {1}", dashboardDetail.DashboardId, ex.Message)
                Logger.Log.Error(String.Format(Environment.NewLine + "Error occured to edit dashboard item of DashboardId - {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2} ", dashboardDetail.DashboardId, ex.Message, ex.StackTrace))
                Return View(dashboardDetail)
            Finally
                Logger.Log.Info(String.Format("Edited Dashboard item ended of DashboardId {0}", dashboardDetail.DashboardId))
            End Try
        End Function


        <CustomActionFilter()>
        Function Delete(dashboardId As Integer, orderId As Integer) As ActionResult
            Logger.Log.Info(String.Format("Dash Item Delete started {0}", dashboardId))
            Dim deleteFlag = False
            Try
                If (Not Session("UserType") Is Nothing) And (Not Session("UserInfo") Is Nothing) Then
                    UserInfo = DirectCast(Session("UserInfo"), User)
                End If

                utility.DashboardRepository.DeleteDashboardDetail(dashboardId)
                utility.DashboardRepository.Save()
                TempData("Message") = "Dashboard item deleted successfully."
                Dim strUserIDs As String = String.Empty
                If UserInfo.currentTabIndex = 1 Then
                    strUserIDs = "-999"
                Else
                    strUserIDs = UserInfo.UserId
                End If

                deleteFlag = True
                Dim orderDashboardItem = From a In utility.DashboardRepository.GetDashboardDetail().Where(Function(d) (strUserIDs.Contains(d.UserId)) And d.AnalysisId = UserInfo.AnalysisId And d.Order > orderId).OrderBy(Function(da) da.Order)
                For Each item In orderDashboardItem
                    item.Order -= 1
                Next

                utility.DashboardRepository.Save()

                Logger.Log.Info(String.Format("Dashboard  item deleted successfully id- {0}", dashboardId))
                Return RedirectToAction("Index")
            Catch ex As Exception
                If (deleteFlag = False) Then
                    TempData("ErrorMessage") = String.Format("Unable to Delete Dashboard item, DashboardId - {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2}", dashboardId, ex.Message, ex.StackTrace)
                    Logger.Log.Error(String.Format("Unable to Delete Dashboard item, DashboardId - {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2}", dashboardId, ex.Message, ex.StackTrace))
                ElseIf (deleteFlag = True) Then
                    TempData("ErrorMessage") = String.Format("Error occured during re-ordering dashboard items, after deleting item id - {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2}", dashboardId, ex.Message, ex.StackTrace)
                    Logger.Log.Error(String.Format("Error occured during re-ordering dashboard items, after deleting item id - {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2}", dashboardId, ex.Message, ex.StackTrace))
                End If
                Return RedirectToAction("Index")
            Finally
                Logger.Log.Info(String.Format("Dash Item Delete ended {0}", dashboardId))
            End Try
        End Function

        <MvcSpreadEvent("Load", "spdRE", DirectCast(Nothing, String()))> _
        Private Sub FpRESpread_Load(sender As Object, e As EventArgs)
            Dim spread As FpSpread = DirectCast(sender, FpSpread)

            If Not spread.Page.IsPostBack Then
                LoadSpread(spread, "RE")

            End If

        End Sub

        <MvcSpreadEvent("Load", "spdBS", DirectCast(Nothing, String()))> _
        Private Sub FpBSSpread_Load(sender As Object, e As EventArgs)
            Dim spread As FpSpread = DirectCast(sender, FpSpread)
            If Not spread.Page.IsPostBack Then
                LoadSpread(spread, "BS")

            End If

        End Sub

        <MvcSpreadEvent("Load", "spdCF", DirectCast(Nothing, String()))> _
        Private Sub FpCFSpread_Load(sender As Object, e As EventArgs)
            Dim spread As FpSpread = DirectCast(sender, FpSpread)
            If Not spread.Page.IsPostBack Then
                LoadSpread(spread, "CF")

            End If

        End Sub

        <MvcSpreadEvent("Load", "spdFR", DirectCast(Nothing, String()))> _
        Private Sub FpFRSpread_Load(sender As Object, e As EventArgs)
            Dim spread As FpSpread = DirectCast(sender, FpSpread)
            If Not spread.Page.IsPostBack Then
                LoadSpread(spread, "FR")

            End If

        End Sub

        <MvcSpreadEvent("Load", "spdOM", DirectCast(Nothing, String()))> _
        Private Sub FpOMSpread_Load(sender As Object, e As EventArgs)
            Dim spread As FpSpread = DirectCast(sender, FpSpread)
            If Not spread.Page.IsPostBack Then
                LoadSpread(spread, "OM")

            End If

        End Sub

        Private Sub LoadSpread(ByRef spread As FpSpread, strType As String)


            spread.Sheets(0).DefaultStyle.Font.Size = FontSize.Large
            spread.HorizontalScrollBarPolicy = FarPoint.Web.Spread.ScrollBarPolicy.Never
            spread.CommandBar.Visible = False
            '  spread.Sheets(0).FrozenColumnCount = 4
            spread.Sheets(0).ColumnCount = 4
            spread.Sheets(0).RowCount = 0
            spread.Sheets(0).Rows(-1).Height = 18
            spread.Sheets(0).Columns(0, 1).Width = 20
            spread.Sheets(0).Columns(2).Width = 0
            spread.Sheets(0).Columns(3).Width = 740
            spread.Sheets(0).Columns(3).Locked = True
            spread.Sheets(0).Rows(-1).VerticalAlign = VerticalAlign.Middle
            'set column headers 
            spread.Sheets(0).ColumnHeader.Rows(0).Font.Size = FontSize.Large
            spread.Sheets(0).ColumnHeader.Rows(0).Height = 20
            spread.Sheets(0).ColumnHeader.Cells(0, 0).Text = " "
            spread.Sheets(0).ColumnHeader.Cells(0, 1).Text = " "
            spread.Sheets(0).ColumnHeader.Cells(0, 2).Text = " "
            spread.Sheets(0).ColumnHeader.Cells(0, 3).Text = "Item to Display in Dashboard "
            spread.Sheets(0).SelectionPolicy = FarPoint.Web.Spread.Model.SelectionPolicy.Single
            spread.Sheets(0).SelectionBackColorStyle = FarPoint.Web.Spread.SelectionBackColorStyles.None
            spread.VerticalScrollBarPolicy = FarPoint.Web.Spread.ScrollBarPolicy.Always
            Select Case strType
                Case "RE"
                    LoadspdItems(spread)
                Case "BS"
                    LoadspdItems(spread)
                Case "CF"
                    LoadspdItems(spread)
                Case "FR"
                    LoadspdItems(spread)
                Case "OM"
                    LoadspdItems(spread)
            End Select
            spread.ActiveSheetView.PageSize = spread.Sheets(0).RowCount

        End Sub

        Public Sub LoadspdItems(spread As FpSpread)
            Using context As New DataAccess
                Dim intRowCtr As Integer = -1
                Dim intSpanRows(1) As Integer
                Dim chkbox As FarPoint.Web.Spread.CheckBoxCellType

                Dim blnTotalFlag As Boolean = False
                Dim dropdown As New FarPoint.Web.Spread.ComboBoxCellType
                Dim cbstr As String()
                cbstr = New String() {"Bar", "Line"}


                dropdown.Items = cbstr

                Dim selectedAccounts = Nothing

                Select Case spread.ClientID
                    Case "spdRE"

                        selectedAccounts = (From a In utility.AccountRepository.GetAccountRecordsByAnalysisId(intAnalysisID)
                                            Join ac In utility.AccountTypeRepository.GetAccountTypeRecordsByAnalysisId(intAnalysisID, New String() {"revenue", "expense"})
                                            On a.AcctTypeId Equals ac.AcctTypeId Where a.AnalysisId = ac.AnalysisId
                                            Order By a.SortSequence
                                            Select a).ToList()

                    Case "spdBS"

                        selectedAccounts = (From a In utility.AccountRepository.GetAccountRecordsByAnalysisId(intAnalysisID)
                                            Join ac In utility.AccountTypeRepository.GetAccountTypeRecordsByAnalysisId(intAnalysisID, New String() {"assets", "liabilities and equity"})
                                            On a.AcctTypeId Equals ac.AcctTypeId Where a.AnalysisId = ac.AnalysisId
                                            Order By a.SortSequence
                                            Select a).ToList()
                    Case "spdCF"

                        selectedAccounts = (From a In utility.AccountRepository.GetAccountRecordsByAnalysisId(intAnalysisID)
                                            Join ac In utility.AccountTypeRepository.GetAccountTypeRecordsByAnalysisId(intAnalysisID, New String() {"cashflow"})
                                            On a.AcctTypeId Equals ac.AcctTypeId Where a.AnalysisId = ac.AnalysisId
                                            Order By a.SortSequence
                                           Select a).ToList()

                    Case "spdFR"
                        selectedAccounts = (From a In utility.AccountRepository.GetAccountRecordsByAnalysisId(intAnalysisID)
                                       Join ac In utility.AccountTypeRepository.GetAccountTypeRecordsByAnalysisId(intAnalysisID, New String() {"ratio"})
                                       On a.AcctTypeId Equals ac.AcctTypeId Where a.AnalysisId = ac.AnalysisId
                                       Order By a.SortSequence
                                       Select a).ToList()


                    Case "spdOM"

                        selectedAccounts = (From a In utility.AccountRepository.GetAccountRecordsByAnalysisId(intAnalysisID)
                                          Join ac In utility.AccountTypeRepository.GetAccountTypeRecordsByAnalysisId(intAnalysisID, New String() {"nonfinancial"})
                                          On a.AcctTypeId Equals ac.AcctTypeId Where a.AnalysisId = ac.AnalysisId
                                          Order By a.SortSequence
                                          Select a).ToList()
                End Select

                If (Not selectedAccounts Is Nothing) Then
                    For Each account As Account In selectedAccounts
                        If account.AcctDescriptor <> "SHeading" Then
                            intRowCtr += 1
                            spread.Sheets(0).AddRows(intRowCtr, 1)
                            ' spread.Sheets(0).Rows(intRowCtr).Height = 18
                            spread.Sheets(0).SetValue(intRowCtr, 3, account.Description)
                            spread.Sheets(0).SetValue(intRowCtr, 2, account.AcctDescriptor)

                            Select Case account.AcctDescriptor
                                Case "Heading"
                                    If spread.ID <> "spdFR" Then
                                        CommonProcedures.FormatHeaderRow(spread, intRowCtr, intSpanRows, blnTotalFlag, 3)
                                    Else
                                        CommonProcedures.FormatHeaderRatioRow(spread, intRowCtr, intSpanRows, False, 3)
                                    End If
                                Case "Total"
                                    CommonProcedures.FormatTotalRow(spread, intRowCtr, intSpanRows, blnTotalFlag, 0, 0, account.ClassDesc, "", 3)
                                    chkbox = New FarPoint.Web.Spread.CheckBoxCellType
                                    chkbox.OnClientClick = "DSChkBoxClicked('checkbox" + String.Concat(chkIndex) + "'," + account.AccountId.ToString() + "," + account.AcctTypeId.ToString() + ",'" + account.Description + "');"
                                    spread.Sheets(0).Cells(intRowCtr, 1).CellType = chkbox
                                    If (account.AccountId = Me.selectedAccount) Then
                                        spread.Sheets(0).DataModel.SetValue(intRowCtr, 1, True)
                                        Me.dashboardItemObj.selectedTabId = spread.ClientID
                                        Me.dashboardItemObj.selectedCheckboxId = String.Concat("#checkbox", chkIndex)
                                        Me.dashboardItemObj.selectedCheckboxDescription = account.Description

                                    End If
                                    chkIndex += 1
                                Case "Detail", "SubDetail", "SubEnd", "OpCash", "NetIncrease", "BegCash", "EndCash", "SDetail", "STotal"
                                    spread.Sheets(0).Cells(intRowCtr, 0).Border.BorderColorBottom = Drawing.Color.White
                                    chkbox = New FarPoint.Web.Spread.CheckBoxCellType
                                    chkbox.OnClientClick = "DSChkBoxClicked('checkbox" + String.Concat(chkIndex) + "'," + account.AccountId.ToString() + "," + account.AcctTypeId.ToString() + ",'" + account.Description + "');"
                                    spread.Sheets(0).Cells(intRowCtr, 1).CellType = chkbox
                                    If (account.AccountId = Me.selectedAccount) Then
                                        spread.Sheets(0).DataModel.SetValue(intRowCtr, 1, True)
                                        Me.dashboardItemObj.selectedTabId = spread.ClientID
                                        Me.dashboardItemObj.selectedCheckboxId = String.Concat("#checkbox", chkIndex)
                                        Me.dashboardItemObj.selectedCheckboxDescription = account.Description
                                    End If
                                    chkIndex += 1
                                Case "ATotal", "LTotal", "LCTotal", "GPTotal", "IOTotal", "IBTTotal", "NITotal"
                                    CommonProcedures.FormatGrandTotalRow(spread, intRowCtr, blnTotalFlag, 0, 0, account.ClassDesc, "", 3)
                                    chkbox = New FarPoint.Web.Spread.CheckBoxCellType
                                    chkbox.OnClientClick = "DSChkBoxClicked('checkbox" + String.Concat(chkIndex) + "'," + account.AccountId.ToString() + "," + account.AcctTypeId.ToString() + ",'" + account.Description + "');"
                                    spread.Sheets(0).Cells(intRowCtr, 1).CellType = chkbox
                                    If (account.AccountId = Me.selectedAccount) Then
                                        spread.Sheets(0).DataModel.SetValue(intRowCtr, 1, True)
                                        Me.dashboardItemObj.selectedTabId = spread.ClientID
                                        Me.dashboardItemObj.selectedCheckboxId = String.Concat("#checkbox", chkIndex)
                                        Me.dashboardItemObj.selectedCheckboxDescription = account.Description
                                    End If
                                    chkIndex += 1
                            End Select
                        End If
                    Next
                End If
            End Using
        End Sub

        Private Sub PopulateChartFormat(Optional selectedFormat As Object = Nothing)

            Logger.Log.Info(String.Format("PopulateChartFormat Execution Started"))
            Dim formats As List(Of SelectListItem) = New List(Of SelectListItem)
            Try
                For Each chartFormat As ChartFormat In chartFormatRepository.GetChartFormats()
                    Select Case chartFormat.ChartFormatId
                        Case 13

                        Case Else
                            formats.Add(New SelectListItem With {.Text = chartFormat.Format, .Value = chartFormat.ChartFormatId, .Selected = False})
                    End Select

                Next
                ViewBag.Formats = New SelectList(formats, "value", "text", selectedFormat)
            Catch ex As Exception
                Logger.Log.Info(String.Format("PopulateChartFormat Execution Ended"))
            End Try

        End Sub

        Private Sub PopulateChartType(Optional selectedChart As Object = Nothing)
            Logger.Log.Info(String.Format("PopulateChartType Execution Started"))
            Dim formats As List(Of SelectListItem) = New List(Of SelectListItem)
            Try
                Dim chartTypes As List(Of SelectListItem) = New List(Of SelectListItem)
                For Each chartType As ChartType In chartTypeRepository.GetChartTypes().Where(Function(ch) Not ch.Type.Equals("None", StringComparison.OrdinalIgnoreCase))
                    Select Case chartType.ChartTypeId
                        Case 1, 2, 4, 5
                            chartTypes.Add(New SelectListItem With {.Text = chartType.Type, .Value = chartType.ChartTypeId, .Selected = False})
                    End Select
                Next
                ViewBag.ChartType = New SelectList(chartTypes, "value", "text", selectedChart)

            Catch ex As Exception
                Logger.Log.Info(String.Format("PopulateChartType Execution Ended"))
            End Try
        End Sub



    End Class




End Namespace