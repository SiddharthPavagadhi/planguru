﻿Imports System.Collections.Generic
Imports System.Linq
Imports System.Data

Public Class AccountTypeRepository
    Inherits GenericRepository(Of AcctType)
    Implements IAccountTypeRepository

    'Implements IDisposable
    Private disposed As Boolean = False

    Public Sub New(context As DataAccess)
        'Me.context = context
        MyBase.New(context)
    End Sub

    Public Function GetAccountTypes() As IEnumerable(Of AcctType) Implements IAccountTypeRepository.GetAccountTypes
        Return context.AcctTypes.ToList()
    End Function

    Public Function GetAccountTypeRecordsByAnalysisId(analysisId As Integer, Optional typeDesc() As String = Nothing) As IEnumerable(Of AcctType) Implements IAccountTypeRepository.GetAccountTypeRecordsByAnalysisId

        If (typeDesc Is Nothing) Then
            Return context.AcctTypes.Where(Function(a) a.AnalysisId = analysisId).ToList()
        Else
            Return context.AcctTypes.Where(Function(at) at.AnalysisId = analysisId And typeDesc.Contains(at.TypeDesc.ToLower())).ToList()
        End If

    End Function

    Public Function GeAccountTypeById(accountId As Integer) As AcctType Implements IAccountTypeRepository.GeAccountTypeById
        Return context.AcctTypes.Find(accountId)
    End Function

    Public Sub Save() Implements IAccountTypeRepository.Save
        context.SaveChanges()
    End Sub

    Protected Overridable Sub Dispose(disposing As Boolean)
        If Not Me.disposed Then
            If disposing Then
                context.Dispose()
            End If
        End If
        Me.disposed = True
    End Sub

    Public Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Public Function DeleteAccountTypeByAnalysis(analysisId As Integer) Implements IAccountTypeRepository.DeleteAccountTypeByAnalysis

        Logger.Log.Info(("Start Query : Get the list of Account Type by AnalysisId: " & analysisId))
        Dim accountTypes = (From d In context.AcctTypes.Where(Function(a) a.AnalysisId = analysisId)
                                       Select d)
        If Not (accountTypes Is Nothing) And accountTypes.Count > 0 Then
            Logger.Log.Info(("Account Type items count : " & accountTypes.Count))
            Logger.Log.Info("Start deleting : Account Type items")
            accountTypes.ToList().ForEach(Sub(item) context.AcctTypes.Remove(item))
            context.SaveChanges()
            Logger.Log.Info(("End deleting : Account Type items"))
        End If
        Return accountTypes.Count
    End Function
End Class
