﻿Imports System.Collections.Generic
Imports System.Linq
Imports System.Data
Imports System.Data.Entity

Public Class AccountRepository
    Inherits GenericRepository(Of Account)
    Implements IAccountRepository

    'Implements IDisposable
    Private disposed As Boolean = False

    Public Sub New(context As DataAccess)
        'Me.context = context
        MyBase.New(context)
    End Sub

    Public Function GetAccounts() As IEnumerable(Of Account) Implements IAccountRepository.GetAccounts
        Return context.Accounts.ToList()
    End Function

    Public Function GetSelectedAccounts(intAnalysisId As Integer, Optional typeDescription() As String = Nothing) As IEnumerable(Of SelectedAccount) Implements IAccountRepository.GetSelectedAccounts

        Dim selectedAccounts = (From a In context.Accounts
                                                  Join ac In context.AcctTypes On a.AcctTypeId Equals ac.AcctTypeId And ac.AnalysisId Equals a.AnalysisId
                                                  Where a.AnalysisId = intAnalysisId And ac.AnalysisId = intAnalysisId And typeDescription.Contains(ac.TypeDesc.ToLower())
                                                  Order By a.SortSequence
                                                  Select New SelectedAccount With {.AccountId = a.AccountId, .AcctTypeId = a.AcctTypeId,
                                                                                    .AnalysisId = a.AnalysisId, .AcctDescriptor = a.AcctDescriptor,
                                                                                    .SortSequence = a.SortSequence, .Description = a.Description,
                                                                                     .Subgrouping = a.Subgrouping, .TypeDesc = ac.TypeDesc, .ClassDesc = ac.ClassDesc}
                                                ).ToList()

        Return selectedAccounts
    End Function

    Public Function GetAccounts(accountId As Integer, acctTypeId As Integer, analysisId As Integer) As Account Implements IAccountRepository.GetAccounts
        Return context.Accounts.Where(Function(a) a.AccountId = accountId And a.AcctTypeId = acctTypeId And a.AnalysisId = analysisId).First()
    End Function
    Public Function GetAccounts(accountId As Integer, analysisId As Integer) As Account Implements IAccountRepository.GetAccounts
        Return context.Accounts.Where(Function(a) a.AccountId = accountId And a.AnalysisId = analysisId).First()
    End Function
    Public Function GetAccountsAndAccTypes(analysisId As Integer, typeDescList As String) As IEnumerable(Of Account) Implements IAccountRepository.GetAccountsAndAccTypes
        Dim selectedAccountsQuery As String = String.Format("Select a.*,at.TypeDesc,at.ClassDesc from Account(nolock) a" & _
                                                             " Inner join AcctType(nolock) at On a.AcctTypeId = at.AcctTypeId" & _
                                                             " Where (at.TypeDesc in ({0})) and a.AnalysisId = at.AnalysisId" & _
                                                             " and a.AnalysisId = {1}", typeDescList, analysisId)

        Return context.Database.SqlQuery(Of Account)(selectedAccountsQuery)
    End Function

    Public Function GetAccountRecordsByAnalysisId(analysisId As Integer, Optional acctDescriptor() As String = Nothing) As IEnumerable(Of Account) Implements IAccountRepository.GetAccountRecordsByAnalysisId

        If (acctDescriptor Is Nothing) Then
            Return context.Accounts.Where(Function(a) a.AnalysisId = analysisId).ToList()
        Else
            Return context.Accounts.Where(Function(a) a.AnalysisId = analysisId And acctDescriptor.Contains(a.AcctDescriptor.ToLower())).ToList()
        End If

    End Function

    Public Function GetAccountById(accountId As Integer) As Account Implements IAccountRepository.GeAccountById
        Return context.Accounts.Find(accountId)
    End Function

    Public Function GetDataUploadedCount(analysisId As Integer) As Integer Implements IAccountRepository.GetDataUploadedCount
        Return context.Accounts.Where(Function(a) a.AnalysisId = analysisId).Count
    End Function
    Public Sub Save() Implements IAccountRepository.Save
        context.SaveChanges()
    End Sub

    Protected Overridable Sub Dispose(disposing As Boolean)
        If Not Me.disposed Then
            If disposing Then
                context.Dispose()
            End If
        End If
        Me.disposed = True
    End Sub

    Public Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Public Function DeleteAccountByAnalysis(analysisId As Integer) Implements IAccountRepository.DeleteAccountByAnalysis
        Logger.Log.Info(("Start Query : Get the list of Accounts by AnalysisId: " & analysisId))
        Dim account = (From d In context.Accounts.Where(Function(a) a.AnalysisId = analysisId)
                                       Select d)
        If Not (account Is Nothing) And account.Count > 0 Then
            Logger.Log.Info(("Account items count : " & account.Count))
            Logger.Log.Info("Start deleting : Account items")
            account.ToList().ForEach(Sub(item) context.Accounts.Remove(item))
            context.SaveChanges()
            Logger.Log.Info(("End deleting : Account items"))
        End If
        Return account.Count
    End Function
End Class
