﻿Imports net.openstack.Core
Imports net.openstack.Providers.Rackspace
Imports net.openstack.Core.Domain
Imports net.openstack.Core.Providers
Imports net.openstack.Core.Exceptions.Response
Imports net.openstack.Core.Collections
Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Collections.ObjectModel
Imports System.Threading
Imports System.Threading.Tasks
Imports System.Configuration
Imports net.openstack.Providers.Rackspace.Objects.Dns
Imports log4net
Imports System.Reflection

Public Class RackspaceDomain

    Private Shared username As String = String.Empty
    Private Shared apikey As String
    Private Shared domainid As String
    Private Shared domain As String
    Private Shared take As Integer
    Private Shared sleeptime As Integer

    Private Shared cloudIdentity As CloudIdentity = Nothing
    Private Shared cloudDNSProvider As CloudDnsProvider = Nothing
    Private Shared dbContext As onlineAnalyticsDataContext

    Public Shared Sub ProcessCnameAddAsync()
        Dim processtask As Task = Nothing
        Try

            Logger.Log.Info("Call ProcessDataAsync")
            If IsNothing(processtask) Then

                Dim getCustomers = GetPremiumGroupCustomer()

                Logger.Log.Info(String.Format("Customer record found: {0}", getCustomers.Count().ToString()))
                If (getCustomers.Count > 0) Then

                    RackspaceDomain(GetUserName(), GetApiKey())
                    cloudDNSProvider = Connect()

                    processtask = New Task(Sub() RegisterCnameOfPremiumGroupCustomer(getCustomers))
                    processtask.Start()
                    processtask.Wait()

                    Task.WaitAll(processtask)

                    If (processtask.IsFaulted) Then
                        Logger.Log.Info(String.Format("ProcessDataAsync : Task IsFaulted"))
                    End If

                    If (processtask.IsCanceled) Then
                        Logger.Log.Info(String.Format("ProcessDataAsync : Task IsCanceled"))
                    End If

                End If
            End If
        Catch processDataCanceledException As OperationCanceledException

            Logger.Log.Error(String.Format("Error in processDataCanceledException : ", processDataCanceledException.Message.ToString()))
            If (Not IsNothing(processDataCanceledException.InnerException)) Then
                Logger.Log.Error(String.Format("Error in processDataCanceledException : ", processDataCanceledException.InnerException.Message.ToString()))
            End If


        Catch processDataAsyncException As Exception

            Logger.Log.Error(String.Format("Error in processDataAsyncException : ", processDataAsyncException.Message.ToString()))
            If (Not IsNothing(processDataAsyncException.InnerException)) Then
                Logger.Log.Error(String.Format("Error in processDataAsyncException : ", processDataAsyncException.InnerException.Message.ToString()))
            End If
        End Try
    End Sub

    Shared Function GetPremiumGroupCustomer() As IEnumerable(Of Customer)
        Dim getCustomers As IEnumerable(Of Customer) = Nothing
        Logger.Log.Info(String.Format("Execution Started : GetPremiumGroupCustomer"))
        Try
            dbContext = New onlineAnalyticsDataContext()

            Logger.Log.Info("Call Premium Group Customer Linq query")
            getCustomers = (From c In dbContext.Customers.Where(Function(c) (c.PlanTypeId = PlanType.BP Or c.PlanTypeId = PlanType.AAM Or c.PlanTypeId = PlanType.AAY Or c.PlanTypeId = PlanType.BNPM Or c.PlanTypeId = PlanType.BNPMUPG Or c.PlanTypeId = PlanType.BNPY Or c.PlanTypeId = PlanType.BNPYUPG) And c.VanityFirmName <> Nothing And c.VanityFirmNameStatus = VanityFirmStatus.DefaultStatus)
                                                           Join u In dbContext.Users.Where(Function(u) u.UserRoleId = UserRoles.SAU And u.Status = StatusE.Active) On u.CustomerId Equals c.CustomerId
                                                           Select c).Take(TakeRecord()).ToList()

            Return getCustomers

            Logger.Log.Info(String.Format(" Customers Count: {0}", getCustomers.Count.ToString()))

        Catch getPremiumGroupCustomerException As Exception
            Logger.Log.Error(String.Format(" Error in ProcessDataAsync ", getPremiumGroupCustomerException.Message.ToString()))
            Return getCustomers
        End Try
        Logger.Log.Info(String.Format("Execution Completed : GetPremiumGroupCustomer"))
    End Function

    Shared Async Sub RegisterCnameOfPremiumGroupCustomer(getCustomers As IEnumerable(Of Customer))
        Dim DnsRecordTask As Task(Of DnsRecord()) = Nothing
        Dim dnsRecords As DnsRecord() = Nothing
        Logger.Log.Info(String.Format("Execution Started : RegisterCnameOfPremiumGroupCustomer"))
        Try

            If (getCustomers.Count > 0) Then


                Dim dnsrecordsconfigurations As List(Of DnsDomainRecordConfiguration) = CreateDnsDomainRecordsConfigurationList(getCustomers)

                DnsRecordTask = CreateZones(cloudDNSProvider, dnsrecordsconfigurations)

                Task.WaitAll(DnsRecordTask)

                dnsRecords = Await DnsRecordTask

                If (DnsRecordTask.Status = TaskStatus.RanToCompletion) Then

                    For Each dnsZone As DnsRecord In dnsRecords
                        Logger.Log.Info(String.Format("Zone record added on rackspace --> Id:{0} , Name:{1}, Type:{2}  ", dnsZone.Id, dnsZone.Name, dnsZone.Type))

                        Dim zonename = dnsZone.Name.Replace(String.Concat(".", GetDomain()), "")
                        Dim cust = (From c In getCustomers.ToList() Where c.VanityFirmName.Equals(zonename, StringComparison.OrdinalIgnoreCase) Select c).FirstOrDefault()
                        cust.VanityFirmNameStatus = VanityFirmStatus.CnameAdded
                        dbContext.SubmitChanges()
                        Logger.Log.Info(String.Format("Customer record Updated --> Vanity Firm Name Status : {0} ", cust.VanityFirmNameStatus))
                    Next

                End If

            End If
        Catch registertaskOperationCanceledException As OperationCanceledException

            Logger.Log.Error(String.Format("Error in regisgtertaskOperationCanceledException: ", registertaskOperationCanceledException.Message.ToString()))
            If (Not IsNothing(registertaskOperationCanceledException.InnerException)) Then
                Logger.Log.Error(String.Format("Error in regisgtertaskOperationCanceledException: ", registertaskOperationCanceledException.InnerException.Message.ToString()))
            End If


        Catch registercnameException As Exception

            Logger.Log.Error(String.Format("Error in registercnameException: ", registercnameException.Message.ToString()))
            If (Not IsNothing(registercnameException.InnerException)) Then
                Logger.Log.Error(String.Format("Error in registercnameException: ", registercnameException.InnerException.Message.ToString()))
            End If

        End Try

        If (DnsRecordTask.IsFaulted) Then
            Logger.Log.Info(String.Format("DnsRecordTask : Task IsFaulted"))
        End If

        If (DnsRecordTask.IsCanceled) Then
            Logger.Log.Info(String.Format("DnsRecordTask : Task IsCanceled"))
        End If

        Logger.Log.Info(String.Format("Execution Completed : RegisterCnameOfPremiumGroupCustomer"))
    End Sub

#Region "Configuration"

   

    Public Shared Function TakeRecord() As String
        If (ConfigurationManager.AppSettings("take") <> "") Then
            take = ConfigurationManager.AppSettings("take").ToString()
        End If
        Return take
    End Function

    Public Shared Function GetUserName() As String
        If (ConfigurationManager.AppSettings("username") <> "") Then
            username = ConfigurationManager.AppSettings("username").ToString()
        End If
        Return username
    End Function

    Private Shared Function GetDomain() As String
        If (ConfigurationManager.AppSettings("domain") <> "") Then
            domain = ConfigurationManager.AppSettings("domain").ToString()
        End If
        Return domain
    End Function

    Private Shared Function GetApiKey() As String
        If (ConfigurationManager.AppSettings("apikey") <> "") Then
            apikey = ConfigurationManager.AppSettings("apikey").ToString()
        End If
        Return apikey
    End Function

    Private Shared Function GetDomainId() As String
        If (ConfigurationManager.AppSettings("domainid") <> "") Then
            domainid = ConfigurationManager.AppSettings("domainid").ToString()
        End If
        Return domainid
    End Function

    Public Shared Sub RackspaceDomain(username As String, apikey As String)
        Logger.Log.Info(String.Format("Execution Started : RackspaceDomain"))
        cloudIdentity = New CloudIdentity()
        cloudIdentity.APIKey = GetApiKey()
        cloudIdentity.Username = GetUserName()
        Logger.Log.Info(String.Format("Execution Completed : RackspaceDomain"))
    End Sub

    Public Shared Function Connect() As CloudDnsProvider
        Logger.Log.Info(String.Format("Execution Started : Connect"))
        Dim cloudIdentityProvider As New CloudIdentityProvider(cloudIdentity)
        Dim userAccess As UserAccess = cloudIdentityProvider.Authenticate(cloudIdentity)
        Dim cloudDNSProvider As New CloudDnsProvider(cloudIdentity, Nothing, False, Nothing)
        Logger.Log.Info(String.Format("Execution Completed : Connect"))
        Return cloudDNSProvider
    End Function
#End Region

    Private Shared Async Function GetZones(cloudDNSProvider As CloudDnsProvider) As Task(Of Tuple(Of ReadOnlyCollectionPage(Of DnsRecord), System.Nullable(Of Integer)))
        Dim domainId As New DomainId(GetDomainId())

        Dim records As Tuple(Of ReadOnlyCollectionPage(Of DnsRecord), System.Nullable(Of Integer)) = Await cloudDNSProvider.ListRecordsAsync(domainId, DnsRecordType.Cname, Nothing, Nothing, Nothing, Nothing, CancellationToken.None)

        Return records

    End Function

    Private Shared Function CreateDnsDomainRecordsConfigurationList(getCustomers As IEnumerable(Of Customer)) As List(Of DnsDomainRecordConfiguration)

        Logger.Log.Info(String.Format("Execution Started : CreateDnsDomainRecordsConfigurationList"))
        Dim recordsConfigurations As List(Of DnsDomainRecordConfiguration) = New List(Of DnsDomainRecordConfiguration)()

        For Each Customer In getCustomers
            recordsConfigurations.Add(New DnsDomainRecordConfiguration(type:=DnsRecordType.Cname, name:=String.Format("{0}.{1}", Customer.VanityFirmName, GetDomain()), data:=GetDomain(), timeToLive:=New TimeSpan(0, 5, 0), comment:=String.Format("{0}.{1}", Customer.VanityFirmName, GetDomain()), priority:=Nothing))
            Logger.Log.Info(String.Format("Add customer record as a DnsDomainRecordConfiguration CustomerId : {0}, VanityFirmName : {1} ", Customer.CustomerId, Customer.VanityFirmName))
        Next

        Logger.Log.Info(String.Format("Execution Completed : CreateDnsDomainRecordsConfigurationList"))
        Return recordsConfigurations

    End Function

    Private Shared Async Function CreateZone(cloudDNSProvider As CloudDnsProvider, subdomainName As String, domainName As String) As Task(Of DnsRecord())
        Dim sleeptime As Integer = 0
        Dim records As DnsRecord() = Nothing
        Try

            Dim domainId As New DomainId(GetDomainId())

            Dim recordConfigurations As DnsDomainRecordConfiguration() = {New DnsDomainRecordConfiguration(type:=DnsRecordType.Cname, name:=String.Format("{0}.{1}", subdomainName, domainName), data:=domainName, timeToLive:=New TimeSpan(0, 5, 0), comment:=String.Format("{0}.{1}", subdomainName, domainName), priority:=Nothing)}

            Dim recordsResponse As DnsJob(Of DnsRecordsList) = Await cloudDNSProvider.AddRecordsAsync(domainId, recordConfigurations, AsyncCompletionOption.RequestCompleted, CancellationToken.None, Nothing)

            records = recordsResponse.Response.Records.ToArray()

            Return records

        Catch ex As Exception

            Return records
        End Try

    End Function

    Private Shared Async Function CreateZones(cloudDNSProvider As CloudDnsProvider, recordsConfigurations As IEnumerable(Of DnsDomainRecordConfiguration)) As Task(Of DnsRecord())

        Dim records As DnsRecord() = Nothing
        Try
            Logger.Log.Info(String.Format("Execution Started : CreateZones"))

            Dim domainId As New DomainId(GetDomainId())

            Logger.Log.Info(String.Format("Execution Started : cloudDNSProvider.AddRecordsAsync() "))
            Dim recordsResponse As DnsJob(Of DnsRecordsList) = Await cloudDNSProvider.AddRecordsAsync(domainId, recordsConfigurations.AsEnumerable(), AsyncCompletionOption.RequestCompleted, CancellationToken.None, Nothing)
            Logger.Log.Info(String.Format("Execution Completed : cloudDNSProvider.AddRecordsAsync() "))

            If (Not IsNothing(recordsResponse) And (recordsResponse.Response.Records.Count > 0)) Then
                Logger.Log.Info(String.Format("recordsResponse.Response.Records.Count : {0} ", recordsResponse.Response.Records.Count))
                records = recordsResponse.Response.Records.ToArray()
            End If

            Return records

        Catch createZonesoperationException As OperationCanceledException

            Logger.Log.Error(String.Format("Error in createZonesoperationException: ", createZonesoperationException.Message.ToString()))
            If (Not IsNothing(createZonesoperationException.InnerException)) Then
                Logger.Log.Error(String.Format("Error in createZonesoperationException: ", createZonesoperationException.InnerException.Message.ToString()))
            End If

            Return records
        Catch createZonesException As Exception

            Logger.Log.Error(String.Format("Error in createZonesException: ", createZonesException.Message.ToString()))
            If (Not IsNothing(createZonesException.InnerException)) Then
                Logger.Log.Error(String.Format("Error in createZonesException: ", createZonesException.InnerException.Message.ToString()))
            End If

            Return records

        End Try

        Logger.Log.Info(String.Format("Execution Completed : CreateZones"))
    End Function

    Private Shared Async Function GetSpecificZoneEntry(cloudDNSProvider As CloudDnsProvider, recordName As String) As Task(Of Tuple(Of ReadOnlyCollectionPage(Of DnsRecord), System.Nullable(Of Integer)))
        Dim domainId As New DomainId(GetDomainId())

        Dim records As Tuple(Of ReadOnlyCollectionPage(Of DnsRecord), System.Nullable(Of Integer)) = Await cloudDNSProvider.ListRecordsAsync(domainId, DnsRecordType.Cname, recordName, Nothing, Nothing, Nothing, _
            CancellationToken.None)

        Return records
    End Function

    Private Shared Async Function DeleteZone(cloudDNSProvider As CloudDnsProvider, recordIds As IEnumerable(Of RecordId)) As Task(Of DnsJob)
        Dim domainId As New DomainId(GetDomainId())

        Dim dnsJob As DnsJob = Await cloudDNSProvider.RemoveRecordsAsync(domainId, recordIds, AsyncCompletionOption.RequestCompleted, CancellationToken.None, Nothing)


        Return dnsJob

    End Function



End Class
