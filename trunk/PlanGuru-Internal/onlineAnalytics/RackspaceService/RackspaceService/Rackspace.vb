﻿Imports System.Configuration
Imports System.ServiceProcess
Imports net.openstack.Core
Imports net.openstack.Providers.Rackspace
Imports net.openstack.Core.Domain
Imports net.openstack.Core.Providers
Imports net.openstack.Core.Exceptions.Response
Imports net.openstack.Core.Collections
Imports net.openstack.Providers.Rackspace.Objects.Dns
Imports System.Threading
Imports System.Threading.Tasks
Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Collections.ObjectModel
Imports System.Timers

Public Class Rackspace
    Inherits System.ServiceProcess.ServiceBase

    Private Shared schedulerTimer As System.Timers.Timer = Nothing
    Private Shared interval As Integer = 0

    Public Sub New()
        MyBase.New()

        ' This call is required by the Component Designer.
        InitializeComponent()
        schedulerTimer = New System.Timers.Timer()
        ' Add any initialization after the InitializeComponent() call

    End Sub

    Public Shared Function TimeInterval() As Integer
        If (ConfigurationManager.AppSettings("interval") <> "") Then
            interval = ConfigurationManager.AppSettings("interval").ToString()
        End If
        Return interval
    End Function

    Protected Overrides Sub OnStart(ByVal args() As String)
        ' Add code here to start your service. This method should set things
        ' in motion so your service can do its work.
        Logger.Log.Info("RackspaceService Started")
        Logger.Log.Info(String.Format("RackspaceService Timer Interval : {0}", TimeInterval()))
        schedulerTimer.Interval = TimeInterval()
        AddHandler schedulerTimer.Elapsed, AddressOf RackspaceService_Elapsed
        schedulerTimer.AutoReset = True
        schedulerTimer.Enabled = True
        schedulerTimer.Start()
    End Sub

    Protected Overrides Sub OnPause()
        Try
            'Me.OnPause()
            'schedulerTimer.Stop()
            schedulerTimer.Enabled = False
            Dim Log As New EventLog("PlanGuruAnalytics - RackspaceService")
            Log.Source = "ServiceTimer"
            Log.WriteEntry("Service paused at " & DateTime.Now)
            Log.Close()

            Logger.Log.Info("PlanGuruAnalytics - RackspaceService : Paused")
        Catch onPauseException As Exception
            Logger.Log.Error(String.Format("PlanGuruAnalytics - RackspaceService OnPauseException : {0} ", onPauseException.Message.ToString()))
        End Try
    End Sub

    Protected Overrides Sub OnContinue()
        Try
            'Me.OnContinue()
            'schedulerTimer.Start()
            schedulerTimer.Enabled = True
            Dim Log As New EventLog("PlanGuruAnalytics - RackspaceService")
            Log.Source = "ServiceTimer"
            Log.WriteEntry("Service paused at " & DateTime.Now)
            Log.Close()

            Logger.Log.Info("PlanGuruAnalytics - RackspaceService : Continued")
        Catch onContinueException As Exception
            Logger.Log.Error(String.Format("PlanGuruAnalytics - RackspaceService OnContinueException : {0} ", onContinueException.Message.ToString()))
        End Try
    End Sub

    Protected Overrides Sub OnShutdown()
        Try
            'MyBase.OnShutdown()
            'schedulerTimer.Stop()
            schedulerTimer.Enabled = False
            Dim Log As New EventLog("PlanGuruAnalytics - RackspaceService")
            Log.Source = "ServiceTimer"
            Log.WriteEntry("Service shutdown at " & DateTime.Now)
            Log.Close()
            Logger.Log.Info("PlanGuruAnalytics - RackspaceService : Shutdown")
        Catch onShutdownException As Exception
            Logger.Log.Error(String.Format("PlanGuruAnalytics - RackspaceService OnShutdownException : {0} ", onShutdownException.Message.ToString()))
        End Try
    End Sub

    Protected Overrides Sub OnStop()
        ' Add code here to perform any tear-down necessary to stop your service.
        Try
            'Me.Stop()
            'schedulerTimer.Stop()
            schedulerTimer.Enabled = False
            Dim Log As New EventLog("PlanGuruAnalytics - RackspaceService")
            Log.Source = "ServiceTimer"
            Log.WriteEntry("Service stopped at " & DateTime.Now)
            Log.Close()

            Logger.Log.Info("PlanGuruAnalytics - RackspaceService : Stopped")

        Catch onStopException As Exception
            Logger.Log.Error(String.Format("PlanGuruAnalytics - RackspaceService OnStopException : {0} ", onStopException.Message.ToString()))
        End Try
    End Sub

    'Private Schedular As Timer
    Public Sub RackspaceService_Elapsed(ByVal source As Object, ByVal e As ElapsedEventArgs)

        Logger.Log.Info("RackspaceService_Elapsed : calling ProcessCnameAddAsync")
        RackspaceDomain.ProcessCnameAddAsync()

    End Sub


End Class
