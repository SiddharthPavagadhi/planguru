﻿Imports System.ServiceProcess
Imports System.IO


<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Rackspace
    Inherits System.ServiceProcess.ServiceBase

    'UserService overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    ' The main entry point for the process  
    <MTAThread()> _
    <System.Diagnostics.DebuggerNonUserCode()> _
    Shared Sub Main()
        Dim ServicesToRun() As System.ServiceProcess.ServiceBase
        '' More than one NT Service may run within the same process. To add
        '' another service to this process, change the following line to
        '' create a second service object. For example,  
        ServicesToRun = New System.ServiceProcess.ServiceBase() {New Rackspace}
        System.ServiceProcess.ServiceBase.Run(ServicesToRun)

        'To Debug Window Service
        'Dim rack As Rackspace = New Rackspace()
        'rack.OnStart(Nothing)
    End Sub

    'Required by the Component Designer
    Private components As System.ComponentModel.IContainer

    ' NOTE: The following procedure is required by the Component Designer
    ' It can be modified using the Component Designer.  
    ' Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        components = New System.ComponentModel.Container()
        Me.CanPauseAndContinue = True
        Me.CanShutdown = True
        Me.ServiceName = "PlanGuruAnalytics - RackspaceService"
    End Sub

End Class
