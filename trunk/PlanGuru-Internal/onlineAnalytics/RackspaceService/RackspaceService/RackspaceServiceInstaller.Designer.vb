﻿Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Configuration.Install
Imports System.Collections.Specialized
Imports System.ServiceProcess
Imports Microsoft.Win32

<System.ComponentModel.RunInstaller(True)>
Partial Class RackspaceServiceInstaller
    Inherits System.Configuration.Install.Installer

    'Installer overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Component Designer
    Private components As System.ComponentModel.IContainer
    Private Shared servicename As String = "PlanGuruAnalytics - RackspaceService"
    Private Shared servicedescription As String = "PlanGuruAnalytics - Rackspace service used to add vanity firm name provided by the customer, to add / remove as a Cname for subdomain using RackspaceApi. \\n Ex: consier 'Abc' as vanity firm name, then it would be Abc.planguruanalytics.com."
    'NOTE: The following procedure is required by the Component Designer
    'It can be modified using the Component Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.RackspaceApiServiceProcessInstaller = New System.ServiceProcess.ServiceProcessInstaller()
        Me.RackspaceApiServiceInstaller = New System.ServiceProcess.ServiceInstaller()
        '
        'RackspaceApiServiceProcessInstaller
        '
        Me.RackspaceApiServiceProcessInstaller.Account = System.ServiceProcess.ServiceAccount.LocalSystem
        Me.RackspaceApiServiceProcessInstaller.Password = Nothing
        Me.RackspaceApiServiceProcessInstaller.Username = Nothing
        '
        'RackspaceApiServiceInstaller
        '
        Me.RackspaceApiServiceInstaller.StartType = System.ServiceProcess.ServiceStartMode.Automatic
        Me.RackspaceApiServiceInstaller.ServiceName = servicename
        '
        'RackspaceServiceInstaller
        '
        Me.Installers.AddRange(New System.Configuration.Install.Installer() {Me.RackspaceApiServiceProcessInstaller, Me.RackspaceApiServiceInstaller})

    End Sub

    'Public Function GetContextParameter(key As String) As String
    '    Dim sValue As String = ""
    '    Try
    '        sValue = Me.Context.Parameters(key).ToString()
    '    Catch
    '        sValue = ""
    '    End Try

    '    Return sValue
    'End Function

    'Public Overrides Sub Install(stateSaver As IDictionary)

    '    Dim system, currentControlSet, services, service, config As RegistryKey

    '    MyBase.Install(stateSaver)

    '    'Define the registry keys
    '    'Navigate to services
    '    system = Registry.LocalMachine.OpenSubKey("System")
    '    currentControlSet = system.OpenSubKey("CurrentControlSet")
    '    services = currentControlSet.OpenSubKey("Services")

    '    'Add the service
    '    service = services.OpenSubKey(Me.RackspaceApiServiceInstaller.ServiceName, True)
    '    'Default service description
    '    service.SetValue("Description", servicedescription)
    '    config = service.CreateSubKey("Parameters")

    '    config.Close()
    '    service.Close()
    '    services.Close()
    '    currentControlSet.Close()
    '    system.Close()
    '    'If Context.Parameters.ContainsKey("username") Then
    '    '    RackspaceApiServiceProcessInstaller.Account = ServiceAccount.User
    '    'End If

    '    'Dim svcName As String = Context.Parameters(servicename)
    '    'If [String].IsNullOrEmpty(svcName) Then
    '    '    Throw New ArgumentException("Missing required parameter 'ServiceName'.")
    '    'End If
    '    'RackspaceApiServiceInstaller.ServiceName = svcName
    '    'RackspaceApiServiceInstaller.DisplayName = svcName
    '    'RackspaceApiServiceInstaller.Description = servicedescription
    '    'stateSaver.Add(servicename, svcName)

    '    ''run the install
    '    'MyBase.Install(stateSaver)
    'End Sub

    'Protected Overrides Sub OnAfterInstall(savedState As IDictionary)
    '    MyBase.OnAfterInstall(savedState)

    '    'The following code starts the services after it is installed.
    '    Using serviceController As New System.ServiceProcess.ServiceController(RackspaceApiServiceInstaller.ServiceName)
    '        serviceController.Start()
    '    End Using
    'End Sub

    'Protected Overrides Sub OnBeforeInstall(savedState As IDictionary)

    '    MyBase.OnBeforeInstall(savedState)

    '    Dim isUserAccount As Boolean = False

    '    'Decode the command line switches
    '    'Dim name As String = GetContextParameter("name")
    '    RackspaceApiServiceInstaller.ServiceName = servicename

    '    'What type of credentials to use to run the service
    '    'The default is User
    '    Dim acct As String = GetContextParameter("account")

    '    If (0 = acct.Length) Then
    '        acct = "user"
    '    End If

    '    Select Case acct
    '        Case "user"
    '            RackspaceApiServiceProcessInstaller.Account = System.ServiceProcess.ServiceAccount.User
    '            isUserAccount = True
    '            Exit Select
    '        Case "localservice"
    '            RackspaceApiServiceProcessInstaller.Account = System.ServiceProcess.ServiceAccount.LocalService
    '            Exit Select
    '        Case "localsystem"
    '            RackspaceApiServiceProcessInstaller.Account = System.ServiceProcess.ServiceAccount.LocalSystem
    '            Exit Select
    '        Case "networkservice"
    '            RackspaceApiServiceProcessInstaller.Account = System.ServiceProcess.ServiceAccount.NetworkService
    '            Exit Select
    '        Case Else
    '            RackspaceApiServiceProcessInstaller.Account = System.ServiceProcess.ServiceAccount.User
    '            isUserAccount = True
    '            Exit Select
    '    End Select

    '    ' User name and password
    '    Dim username As String = GetContextParameter("user")
    '    Dim password As String = GetContextParameter("password")

    '    ' Should I use a user account?
    '    If isUserAccount Then
    '        ' If we need to use a user account,
    '        ' set the user name and password
    '        RackspaceApiServiceProcessInstaller.Username = username
    '        RackspaceApiServiceProcessInstaller.Password = password
    '    End If

    'End Sub

    'Protected Overrides Sub OnBeforeUninstall(savedState As IDictionary)
    '    MyBase.OnBeforeUninstall(savedState)

    '    ' Set the service name based on the command line
    '    RackspaceApiServiceInstaller.ServiceName = servicename 'GetContextParameter("name")
    'End Sub

    'Public Overrides Sub Rollback(savedState As IDictionary)
    '    RackspaceApiServiceInstaller.ServiceName = servicename   'DirectCast(savedState(servicename), String)
    '    MyBase.Rollback(savedState)
    'End Sub

    'Public Overrides Sub Uninstall(savedState As IDictionary)

    '    Dim system, currentControlSet, services, service As RegistryKey

    '    RackspaceApiServiceInstaller.ServiceName = servicename 'DirectCast(savedState(servicename), String)

    '    MyBase.Uninstall(savedState)

    '    system = Registry.LocalMachine.OpenSubKey("System")
    '    currentControlSet = system.OpenSubKey("CurrentControlSet")
    '    services = currentControlSet.OpenSubKey("Services")
    '    service = services.OpenSubKey(RackspaceApiServiceInstaller.ServiceName, True)
    '    'Remove the parameters key
    '    service.DeleteSubKeyTree("Parameters")

    'End Sub

    'Public Overrides Sub Commit(savedState As IDictionary)
    '    MyBase.Commit(savedState)
    'End Sub


End Class
