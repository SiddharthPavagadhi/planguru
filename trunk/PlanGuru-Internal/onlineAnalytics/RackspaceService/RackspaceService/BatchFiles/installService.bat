﻿@echo off
set SERVICE_HOME=D:\PlanGuru-RackspaceService
set SERVICE_EXE=RackspaceService.exe
REM the following directory is for .NET V4.0.30319
set INSTALL_UTIL_HOME=C:\Windows\Microsoft.NET\Framework\v4.0.30319
REM Account credentials if the service uses a user account
set USER_NAME=PGAIIS
set PASSWORD=kD6&My9Z@r

set PATH=%PATH%;%INSTALL_UTIL_HOME%

cd %SERVICE_HOME%

echo Installing Service...
installutil /name=<service name> 
  /account=<account type> /user=%USER_NAME% /password=%PASSWORD% %SERVICE_EXE%

echo Done.