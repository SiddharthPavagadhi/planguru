﻿@echo off
set SERVICE_HOME=D:\PlanGuru-RackspaceService
set SERVICE_EXE=RackspaceService.exe
REM the following directory is for .NET V4.0.30319
set INSTALL_UTIL_HOME=C:\Windows\Microsoft.NET\Framework\v4.0.30319

set PATH=%PATH%;%INSTALL_UTIL_HOME%

cd %SERVICE_HOME%

echo Uninstalling Service...
installutil /u /name=<service name> %SERVICE_EXE%

echo Done.