﻿
Partial Public Class RackspaceDomain

    Public Enum PlanType
        PPU = 1 ' PPU
        BP = 2
        BNPM = 3
        BNPY = 4
        AAM = 5
        AAY = 6
        BNPMUPG = 7
        BNPYUPG = 8 ' BP     
    End Enum

    Public Enum VanityFirmStatus
        DefaultStatus = 0
        CnameAdded = 1
        CnameRemoved = 2
    End Enum

    Public Enum StatusE
        Pending = 1
        Active = 2
        Suspended = 3
        Deactivated = 4
    End Enum

    Public Enum UserRoles As Integer
        PGAAdmin = 1
        PGASupport = 2
        'Customer Administrative User (Super User) 
        SAU = 3
        'Company Administrative User
        CAU = 4
        'Company Regular User
        CRU = 5
        'Subscription Supervisor user
        SSU = 6
    End Enum

End Class
