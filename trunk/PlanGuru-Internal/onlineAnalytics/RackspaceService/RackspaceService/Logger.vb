﻿Imports log4net
Imports log4net.Config

Public Class Logger

    Private Shared _log As ILog

    Public Shared ReadOnly Property Log() As ILog
        Get
            If (_log Is Nothing) Then

                _log = LogManager.GetLogger(GetType(Rackspace))
            End If

            Return _log
        End Get

    End Property

    Public Shared Sub InitLogger()
        BasicConfigurator.Configure()
    End Sub

    Public Shared Sub SetProperty(name As String, value As Object)

        log4net.GlobalContext.Properties(name) = value

    End Sub

End Class
