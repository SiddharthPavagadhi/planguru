﻿Imports System.Collections.Generic
Imports System.Linq
Imports System.Web
Imports System.Data.Entity


Public Class SampleData
    'Inherits DropCreateDatabaseAlways(Of DataAccess)
    Inherits DropCreateDatabaseIfModelChanges(Of DataAccess)

    Private balanceIndex As Integer = 0

    Private Function IncrementBalanceIndex(ByRef value As Integer) As Integer
        balanceIndex = value + 1
        Return balanceIndex
    End Function

    Protected Overrides Sub Seed(context As DataAccess)

        'Master records of UserTables
        Dim userTables = New List(Of UserTables)() From { _
             New UserTables() With {.UserTableId = 1, .UserTableName = "AcctType"}, _
             New UserTables() With {.UserTableId = 1, .UserTableName = "Account"}, _
             New UserTables() With {.UserTableId = 1, .UserTableName = "Balance"}
        }
        userTables.ForEach(Function(u) context.UserTabless.Add(u))

        'Master records of UserRole
        Dim userRoles = New List(Of UserRole)() From { _
             New UserRole() With {.RoleName = "PGAU"}, _
             New UserRole() With {.RoleName = "PGSU"}, _
             New UserRole() With {.RoleName = "SAU"}, _
             New UserRole() With {.RoleName = "CAU"}, _
             New UserRole() With {.RoleName = "CRU"} _
        }
        userRoles.ForEach(Function(u) context.UserRoles.Add(u))

        'Master records of UserRolePermission
        Dim userRolePermissions = New List(Of UserRolePermission)() From { _
             New UserRolePermission() With {.Description = "Subscription Management"}, _
             New UserRolePermission() With {.Description = "User Management"}, _
             New UserRolePermission() With {.Description = "Search Customer"}, _
             New UserRolePermission() With {.Description = "View Subscription"}, _
             New UserRolePermission() With {.Description = "Add Subscription"}, _
             New UserRolePermission() With {.Description = "Update Subscription"}, _
             New UserRolePermission() With {.Description = "Cancel Subscription"}, _
             New UserRolePermission() With {.Description = "Add User"}, _
             New UserRolePermission() With {.Description = "Delete User"}, _
             New UserRolePermission() With {.Description = "View User"}, _
             New UserRolePermission() With {.Description = "Update User"}, _
             New UserRolePermission() With {.Description = "Add Company"}, _
             New UserRolePermission() With {.Description = "Delete Company"}, _
             New UserRolePermission() With {.Description = "View Companies"}, _
             New UserRolePermission() With {.Description = "Update Company"}, _
             New UserRolePermission() With {.Description = "User Company Mapping"}, _
             New UserRolePermission() With {.Description = "Add Analysis"}, _
             New UserRolePermission() With {.Description = "Delete Analysis"}, _
             New UserRolePermission() With {.Description = "View Analyses"}, _
             New UserRolePermission() With {.Description = "Update Analysis"}, _
             New UserRolePermission() With {.Description = "User Analysis Mapping"}, _
             New UserRolePermission() With {.Description = "View Analysis Info"}, _
             New UserRolePermission() With {.Description = "Print Analysis Info"}, _
             New UserRolePermission() With {.Description = "Modify Dashboard"}
        }
        userRolePermissions.ForEach(Function(u) context.UserRolePermissions.Add(u))

        'Make save changes to let insert identity key for UserRoleId. 
        'UserRoleId is used as reference key in User table.
        MyBase.Seed(context)
        context.SaveChanges()

        Dim status = New List(Of Status)() From { _
             New Status() With {.StatusName = "Pending"}, _
             New Status() With {.StatusName = "Active"}, _
             New Status() With {.StatusName = "Suspended"}, _
             New Status() With {.StatusName = "Deactivated"} _
        }
        status.ForEach(Function(u) context.Status.Add(u))

        Dim chartFormats = New List(Of ChartFormat)() From { _
             New ChartFormat() With {.Format = "Budget vs Actual"}, _
             New ChartFormat() With {.Format = "Budget vs Actual YTD"}, _
             New ChartFormat() With {.Format = "This period vs last period"}, _
             New ChartFormat() With {.Format = "This period vs same period prior year"}, _
             New ChartFormat() With {.Format = "This period vs same period prior year YTD"}, _
             New ChartFormat() With {.Format = "Current year trend"}, _
             New ChartFormat() With {.Format = "Trend for the last 12 months"}, _
             New ChartFormat() With {.Format = "Multi-year trend"}, _
             New ChartFormat() With {.Format = "Year to date multi-year trend"} _
        }
        chartFormats.ForEach(Function(ch) context.ChartFormats.Add(ch))

        Dim chartTypes = New List(Of ChartType)() From { _
             New ChartType() With {.Type = "None"}, _
             New ChartType() With {.Type = "Bar"}, _
             New ChartType() With {.Type = "Stacked bar"}, _
             New ChartType() With {.Type = "Line"}, _
             New ChartType() With {.Type = "Area"}, _
             New ChartType() With {.Type = "Pie"}
        }
        chartTypes.ForEach(Function(chty) context.ChartTypes.Add(chty))

        Dim customer = New Customer() With {.CustomerId = 111111, .CustomerFirstName = "Edward", .CustomerLastName = "Wielage",
                                 .CustomerCompanyName = "New Horizon Software Technologies, Inc.", .Quantity = 0,
                                 .ContactFirstName = "Sally", .ContactLastName = "Sprankle",
                                 .ContactTelephone = "", .CustomerAddress1 = "212 AP", .CustomerAddress2 = "Santa Fe,",
                                 .Country = "United States", .State = "New Maxico", .City = "Santa Fe", .CustomerPostalCode = "87505",
                                 .CustomerEmail = "ed@planguru.com", .FirstNameOnCard = "", .LastNameOnCard = "",
                                 .CreditCardNumber = "", .CVV = "", .ExpirationYear = 0, .ExpirationMonth = 0,
                                 .CreatedBy = "", .CreatedOn = Date.Now, .UpdatedBy = "", .UpdatedOn = Date.Now}
        context.Customers.Add(customer)

        Dim user = New User() With {.UserId = 100000, .UserName = "Administrator", .FirstName = "Edward", .LastName = "Wielage", .Password = "6wyBc4t+0oB7s6qruD965w==",
                           .UserEmail = "ed@planguru.com", .Status = 2, .SecurityKey = "", .UserRoleId = 1, .Customer = customer,
                           .CreatedBy = "", .CreatedOn = Date.Now, .UpdatedBy = "", .UpdatedOn = Date.Now}
        context.Users.Add(user)

        Dim customer1 = New Customer() With {.CustomerId = 111112, .CustomerFirstName = "Sally", .CustomerLastName = "Sprankle", _
                                 .CustomerCompanyName = "New Horizon Software Technologies, Inc.", .Quantity = 0,
                                 .ContactFirstName = "Edward", .ContactLastName = "Wielage",
                                 .ContactTelephone = "", .CustomerAddress1 = "212 AP", .CustomerAddress2 = "Santa Fe,",
                                 .Country = "United States", .State = "New Maxico", .City = "Santa Fe", .CustomerPostalCode = "87505",
                                 .CustomerEmail = "sally@planguru.com", .FirstNameOnCard = "", .LastNameOnCard = "",
                                 .CreditCardNumber = "", .CVV = "", .ExpirationYear = 0, .ExpirationMonth = 0,
                                 .CreatedBy = "", .CreatedOn = Date.Now, .UpdatedBy = "", .UpdatedOn = Date.Now}
        context.Customers.Add(customer1)

        Dim user1 = New User() With {.UserId = 100001, .UserName = "SupportAdmin", .FirstName = "Sally", .LastName = "Sprankle", .Password = " KQED+2NEha0MU4FogPEbtw==",
                           .UserEmail = "ed@planguru.com", .Status = 2, .SecurityKey = "", .UserRoleId = 2, .Customer = customer1,
                           .CreatedBy = "", .CreatedOn = Date.Now, .UpdatedBy = "", .UpdatedOn = Date.Now}

        context.Users.Add(user1)


        'Email Info table sample data
        Dim emailBodySubSignup As String = Common.GetEmailBodyfromFile("NewsubscriptionSignup")
        Dim emailBodyNewUserAdded As String = Common.GetEmailBodyfromFile("NewUserAdded")
        Dim emailBodyNewUserSignUp As String = Common.GetEmailBodyfromFile("NewUserSignUp")
        Dim emailBodyPasswordReset As String = Common.GetEmailBodyfromFile("PasswordReset")
        Dim emailBodySubcriptionCancellation As String = Common.GetEmailBodyfromFile("SubcriptionCancellation")
        Dim emailBodyUserDeleted As String = Common.GetEmailBodyfromFile("UserDeleted")

        Dim ei = New List(Of EmailInfo)() From { _
            New EmailInfo() With {.Id = CType(EmailType.NewSubscriptionSignup, Integer), .EmailType = EmailType.NewSubscriptionSignup.ToString(), .EmailSubject = "Trial sign-up confirmation", .EmailBody = emailBodySubSignup, .CreatedDateTime = DateTime.Now}, _
            New EmailInfo() With {.Id = CType(EmailType.NewUserAdded, Integer), .EmailType = EmailType.NewUserAdded.ToString(), .EmailSubject = "You have been added as a user", .EmailBody = emailBodyNewUserAdded, .CreatedDateTime = DateTime.Now}, _
            New EmailInfo() With {.Id = CType(EmailType.NewUserSignUp, Integer), .EmailType = EmailType.NewUserSignUp.ToString(), .EmailSubject = "A new user has been added to your subscription", .EmailBody = emailBodyNewUserSignUp, .CreatedDateTime = DateTime.Now}, _
            New EmailInfo() With {.Id = CType(EmailType.PasswordReset, Integer), .EmailType = EmailType.PasswordReset.ToString(), .EmailSubject = "Your password has been reset", .EmailBody = emailBodyPasswordReset, .CreatedDateTime = DateTime.Now}, _
            New EmailInfo() With {.Id = CType(EmailType.SubcriptionCancellation, Integer), .EmailType = EmailType.SubcriptionCancellation.ToString(), .EmailSubject = "Your subscription has been cancelled", .EmailBody = emailBodySubcriptionCancellation, .CreatedDateTime = DateTime.Now}, _
            New EmailInfo() With {.Id = CType(EmailType.UserDeleted, Integer), .EmailType = EmailType.UserDeleted.ToString(), .EmailSubject = "Confirmation of user deletion", .EmailBody = emailBodyUserDeleted, .CreatedDateTime = DateTime.Now} _
       }
        ei.ForEach(Function(u) context.EmailInfos.Add(u))


        'Assign all rights to UserRole of Administrator and Administrator Support.
        For index As Integer = 1 To 5
            Dim userRolePermissionsMappings = New List(Of UserRolePermissionMapping)

            For Each rolepermission As UserRolePermission In userRolePermissions

                If (index = 1 Or index = 2) Then
                    userRolePermissionsMappings.Add(New UserRolePermissionMapping() With {.UserRoleId = index, .UserRolePermissionId = rolepermission.UserRolePermissionId, .CreatedBy = "110e7aa0-5f05-48a6-adb5-affcc6df8811", .UpdatedBy = "110e7aa0-5f05-48a6-adb5-affcc6df8811", .CreatedOn = DateTime.Now(), .UpdatedOn = DateTime.Now()})
                ElseIf (index = 3 And rolepermission.UserRolePermissionId > 5) Then
                    userRolePermissionsMappings.Add(New UserRolePermissionMapping() With {.UserRoleId = index, .UserRolePermissionId = rolepermission.UserRolePermissionId, .CreatedBy = "110e7aa0-5f05-48a6-adb5-affcc6df8811", .UpdatedBy = "110e7aa0-5f05-48a6-adb5-affcc6df8811", .CreatedOn = DateTime.Now(), .UpdatedOn = DateTime.Now()})
                ElseIf (index = 4 And rolepermission.UserRolePermissionId > 17) Then
                    userRolePermissionsMappings.Add(New UserRolePermissionMapping() With {.UserRoleId = index, .UserRolePermissionId = rolepermission.UserRolePermissionId, .CreatedBy = "110e7aa0-5f05-48a6-adb5-affcc6df8811", .UpdatedBy = "110e7aa0-5f05-48a6-adb5-affcc6df8811", .CreatedOn = DateTime.Now(), .UpdatedOn = DateTime.Now()})
                ElseIf (index = 5 And (rolepermission.UserRolePermissionId = 14 Or rolepermission.UserRolePermissionId = 19 Or rolepermission.UserRolePermissionId > 22)) Then
                    userRolePermissionsMappings.Add(New UserRolePermissionMapping() With {.UserRoleId = index, .UserRolePermissionId = rolepermission.UserRolePermissionId, .CreatedBy = "110e7aa0-5f05-48a6-adb5-affcc6df8811", .UpdatedBy = "110e7aa0-5f05-48a6-adb5-affcc6df8811", .CreatedOn = DateTime.Now(), .UpdatedOn = DateTime.Now()})
                End If
            Next
            userRolePermissionsMappings.ForEach(Function(u) context.UserRolePermissionMappings.Add(u))
        Next

        Dim acctType = New AcctType() With {.AcctTypeId = 1, .TypeDesc = "Assets", .ClassDesc = "Current assets", .SubclassDesc = "Current assets", .AnalysisId = 1}
        context.AcctTypes.Add(acctType)

        Dim acctList As New List(Of Account)() From {
            New Account() With {.AccountId = 1, .AcctDescriptor = "Heading", .SortSequence = 1, .Description = "Current assets", .AcctTypeId = acctType.AcctTypeId, .AnalysisId = 1}, _
            New Account() With {.AccountId = 2, .AcctDescriptor = "Detail", .SortSequence = 2, .Description = "Cash", .AcctTypeId = acctType.AcctTypeId, .AnalysisId = 1}, _
            New Account() With {.AccountId = 3, .AcctDescriptor = "Detail", .SortSequence = 3, .Description = "Checking", .AcctTypeId = acctType.AcctTypeId, .AnalysisId = 1}, _
            New Account() With {.AccountId = 4, .AcctDescriptor = "Detail", .SortSequence = 4, .Description = "Cash and cash equivalents", .AcctTypeId = acctType.AcctTypeId, .AnalysisId = 1}, _
            New Account() With {.AccountId = 5, .AcctDescriptor = "Detail", .SortSequence = 5, .Description = "Accounts receivable", .AcctTypeId = acctType.AcctTypeId, .AnalysisId = 1}, _
            New Account() With {.AccountId = 6, .AcctDescriptor = "Detail", .SortSequence = 6, .Description = "Prepaid expense", .AcctTypeId = acctType.AcctTypeId, .AnalysisId = 1}, _
            New Account() With {.AccountId = 7, .AcctDescriptor = "Total", .SortSequence = 7, .Description = "Total Current Assets", .AcctTypeId = acctType.AcctTypeId, .AnalysisId = 1}
        }
        acctList.ForEach(Function(u) context.Accounts.Add(u))


        For Each acctItem As Account In acctList
            Select Case acctItem.Description
                Case "Cash"
                    Dim balList As New List(Of Balance)() From { _
                        New Balance() With {.BalanceId = IncrementBalanceIndex(balanceIndex), .H101 = 100, .H102 = 150, .H103 = 200, .H104 = 250, _
                    .H105 = 300, .H106 = 350, .H107 = 400, .H108 = 450, .H109 = 500, .H110 = 550, .H111 = 600, _
                    .H112 = 650, .AccountId = acctItem.AccountId, .AnalysisId = 1}, _
                        New Balance() With {.BalanceId = IncrementBalanceIndex(balanceIndex), .A101 = 100, .A102 = 150, .A103 = 200, .A104 = 250, _
                    .A105 = 300, .A106 = 350, .A107 = 400, .A108 = 450, .A109 = 500, .A110 = 550, .A111 = 600, _
                    .A112 = 650, .AccountId = acctItem.AccountId, .AnalysisId = 1}
                    }
                    balList.ForEach(Function(u) context.Balances.Add(u))
                Case "Checking"
                    Dim balList As New List(Of Balance)() From { _
                        New Balance() With {.BalanceId = IncrementBalanceIndex(balanceIndex), .H101 = 100, .H102 = 150, .H103 = 200, .H104 = 250, _
                    .H105 = 300, .H106 = 350, .H107 = 400, .H108 = 450, .H109 = 500, .H110 = 550, .H111 = 600, _
                    .H112 = 650, .AccountId = acctItem.AccountId, .AnalysisId = 1}
                    }
                    balList.ForEach(Function(u) context.Balances.Add(u))
            End Select
        Next

        acctType = New AcctType() With {.AcctTypeId = 2, .TypeDesc = "Revenue", .ClassDesc = "Revenue", .SubclassDesc = "Revenue", .AnalysisId = 1}
        context.AcctTypes.Add(acctType)

        acctList = New List(Of Account)() From { _
         New Account() With {.AccountId = 8, .AcctDescriptor = "Heading", .SortSequence = 3, .Description = "Revenue", .AcctTypeId = acctType.AcctTypeId, .AnalysisId = 1}, _
         New Account() With {.AccountId = 9, .AcctDescriptor = "Detail", .SortSequence = 4, .Description = "Total new sales", .AcctTypeId = acctType.AcctTypeId, .AnalysisId = 1}, _
         New Account() With {.AccountId = 10, .AcctDescriptor = "Detail", .SortSequence = 5, .Description = "Total upgrade sales", .AcctTypeId = acctType.AcctTypeId, .AnalysisId = 1}, _
         New Account() With {.AccountId = 11, .AcctDescriptor = "Detail", .SortSequence = 6, .Description = "Cloud sales", .AcctTypeId = acctType.AcctTypeId, .AnalysisId = 1}, _
         New Account() With {.AccountId = 12, .AcctDescriptor = "Detail", .SortSequence = 7, .Description = "Consulting revenue", .AcctTypeId = acctType.AcctTypeId, .AnalysisId = 1}, _
         New Account() With {.AccountId = 13, .AcctDescriptor = "Detail", .SortSequence = 8, .Description = "Shipping, returns & discounts", .AcctTypeId = acctType.AcctTypeId, .AnalysisId = 1}, _
         New Account() With {.AccountId = 14, .AcctDescriptor = "Total", .SortSequence = 9, .Description = "Total revenue", .AcctTypeId = acctType.AcctTypeId, .AnalysisId = 1} _
        }
        acctList.ForEach(Function(u) context.Accounts.Add(u))


        'Country Code table
        Dim countryCode As New List(Of CountryCode)() From { _
        New CountryCode() With {.Code = "AF", .CountryName = "Afghanistan"}, _
        New CountryCode() With {.Code = "AL", .CountryName = "Albania"}, _
        New CountryCode() With {.Code = "DZ", .CountryName = "Algeria"}, _
        New CountryCode() With {.Code = "AS", .CountryName = "American Samoa"}, _
        New CountryCode() With {.Code = "AD", .CountryName = "Andorra"}, _
        New CountryCode() With {.Code = "AO", .CountryName = "Angola"}, _
        New CountryCode() With {.Code = "AI", .CountryName = "Anguilla"}, _
        New CountryCode() With {.Code = "AQ", .CountryName = "Antarctica"}, _
        New CountryCode() With {.Code = "AG", .CountryName = "Antigua and Barbuda"}, _
        New CountryCode() With {.Code = "AR", .CountryName = "Argentina"}, _
        New CountryCode() With {.Code = "AM", .CountryName = "Armenia"}, _
        New CountryCode() With {.Code = "AW", .CountryName = "Aruba"}, _
        New CountryCode() With {.Code = "AU", .CountryName = "Australia"}, _
        New CountryCode() With {.Code = "AT", .CountryName = "Austria"}, _
        New CountryCode() With {.Code = "AZ", .CountryName = "Azerbaijan"}, _
        New CountryCode() With {.Code = "BS", .CountryName = "Bahamas"}, _
        New CountryCode() With {.Code = "BH", .CountryName = "Bahrain"}, _
        New CountryCode() With {.Code = "BD", .CountryName = "Bangladesh"}, _
        New CountryCode() With {.Code = "BB", .CountryName = "Barbados"}, _
        New CountryCode() With {.Code = "BY", .CountryName = "Belarus"}, _
        New CountryCode() With {.Code = "BE", .CountryName = "Belgium"}, _
        New CountryCode() With {.Code = "BZ", .CountryName = "Belize"}, _
        New CountryCode() With {.Code = "BJ", .CountryName = "Benin"}, _
        New CountryCode() With {.Code = "BM", .CountryName = "Bermuda"}, _
        New CountryCode() With {.Code = "BT", .CountryName = "Bhutan"}, _
        New CountryCode() With {.Code = "BO", .CountryName = "Bolivia"}, _
        New CountryCode() With {.Code = "BA", .CountryName = "Bosnia and Herzegovina"}, _
        New CountryCode() With {.Code = "BW", .CountryName = "Botswana"}, _
        New CountryCode() With {.Code = "BV", .CountryName = "Bouvet Island"}, _
        New CountryCode() With {.Code = "BR", .CountryName = "Brazil"}, _
        New CountryCode() With {.Code = "BQ", .CountryName = "British Antarctic Territory"}, _
        New CountryCode() With {.Code = "IO", .CountryName = "British Indian Ocean Territory"}, _
        New CountryCode() With {.Code = "VG", .CountryName = "British Virgin Islands"}, _
        New CountryCode() With {.Code = "BN", .CountryName = "Brunei"}, _
        New CountryCode() With {.Code = "BG", .CountryName = "Bulgaria"}, _
        New CountryCode() With {.Code = "BF", .CountryName = "Burkina Faso"}, _
        New CountryCode() With {.Code = "BI", .CountryName = "Burundi"}, _
        New CountryCode() With {.Code = "KH", .CountryName = "Cambodia"}, _
        New CountryCode() With {.Code = "CM", .CountryName = "Cameroon"}, _
        New CountryCode() With {.Code = "CA", .CountryName = "Canada"}, _
        New CountryCode() With {.Code = "CT", .CountryName = "Canton and Enderbury Islands"}, _
        New CountryCode() With {.Code = "CV", .CountryName = "Cape Verde"}, _
        New CountryCode() With {.Code = "KY", .CountryName = "Cayman Islands"}, _
        New CountryCode() With {.Code = "CF", .CountryName = "Central African Republic"}, _
        New CountryCode() With {.Code = "TD", .CountryName = "Chad"}, _
        New CountryCode() With {.Code = "CL", .CountryName = "Chile"}, _
        New CountryCode() With {.Code = "CN", .CountryName = "China"}, _
        New CountryCode() With {.Code = "CX", .CountryName = "Christmas Island"}, _
        New CountryCode() With {.Code = "CC", .CountryName = "Cocos [Keeling] Islands"}, _
        New CountryCode() With {.Code = "CO", .CountryName = "Colombia"}, _
        New CountryCode() With {.Code = "KM", .CountryName = "Comoros"}, _
        New CountryCode() With {.Code = "CG", .CountryName = "Congo - Brazzaville"}, _
        New CountryCode() With {.Code = "CD", .CountryName = "Congo - Kinshasa"}, _
        New CountryCode() With {.Code = "CK", .CountryName = "Cook Islands"}, _
        New CountryCode() With {.Code = "CR", .CountryName = "Costa Rica"}, _
        New CountryCode() With {.Code = "HR", .CountryName = "Croatia"}, _
        New CountryCode() With {.Code = "CU", .CountryName = "Cuba"}, _
        New CountryCode() With {.Code = "CY", .CountryName = "Cyprus"}, _
        New CountryCode() With {.Code = "CZ", .CountryName = "Czech Republic"}, _
        New CountryCode() With {.Code = "CI", .CountryName = "Côte d Ivoire"}, _
        New CountryCode() With {.Code = "DK", .CountryName = "Denmark"}, _
        New CountryCode() With {.Code = "DJ", .CountryName = "Djibouti"}, _
        New CountryCode() With {.Code = "DM", .CountryName = "Dominica"}, _
        New CountryCode() With {.Code = "DO", .CountryName = "Dominican Republic"}, _
        New CountryCode() With {.Code = "NQ", .CountryName = "Dronning Maud Land"}, _
        New CountryCode() With {.Code = "DD", .CountryName = "East Germany"}, _
        New CountryCode() With {.Code = "EC", .CountryName = "Ecuador"}, _
        New CountryCode() With {.Code = "EG", .CountryName = "Egypt"}, _
        New CountryCode() With {.Code = "SV", .CountryName = "El Salvador"}, _
        New CountryCode() With {.Code = "GQ", .CountryName = "Equatorial Guinea"}, _
        New CountryCode() With {.Code = "ER", .CountryName = "Eritrea"}, _
        New CountryCode() With {.Code = "EE", .CountryName = "Estonia"}, _
        New CountryCode() With {.Code = "ET", .CountryName = "Ethiopia"}, _
        New CountryCode() With {.Code = "FK", .CountryName = "Falkland Islands"}, _
        New CountryCode() With {.Code = "FO", .CountryName = "Faroe Islands"}, _
        New CountryCode() With {.Code = "FJ", .CountryName = "Fiji"}, _
        New CountryCode() With {.Code = "FI", .CountryName = "Finland"}, _
        New CountryCode() With {.Code = "FR", .CountryName = "France"}, _
        New CountryCode() With {.Code = "GF", .CountryName = "French Guiana"}, _
        New CountryCode() With {.Code = "PF", .CountryName = "French Polynesia"}, _
        New CountryCode() With {.Code = "TF", .CountryName = "French Southern Territories"}, _
        New CountryCode() With {.Code = "FQ", .CountryName = "French Southern and Antarctic Territories"}, _
        New CountryCode() With {.Code = "GA", .CountryName = "Gabon"}, _
        New CountryCode() With {.Code = "GM", .CountryName = "Gambia"}, _
        New CountryCode() With {.Code = "GE", .CountryName = "Georgia"}, _
        New CountryCode() With {.Code = "DE", .CountryName = "Germany"}, _
        New CountryCode() With {.Code = "GH", .CountryName = "Ghana"}, _
        New CountryCode() With {.Code = "GI", .CountryName = "Gibraltar"}, _
        New CountryCode() With {.Code = "GR", .CountryName = "Greece"}, _
        New CountryCode() With {.Code = "GL", .CountryName = "Greenland"}, _
        New CountryCode() With {.Code = "GD", .CountryName = "Grenada"}, _
        New CountryCode() With {.Code = "GP", .CountryName = "Guadeloupe"}, _
        New CountryCode() With {.Code = "GU", .CountryName = "Guam"}, _
        New CountryCode() With {.Code = "GT", .CountryName = "Guatemala"}, _
        New CountryCode() With {.Code = "GG", .CountryName = "Guernsey"}, _
        New CountryCode() With {.Code = "GN", .CountryName = "Guinea"}, _
        New CountryCode() With {.Code = "GW", .CountryName = "Guinea-Bissau"}, _
        New CountryCode() With {.Code = "GY", .CountryName = "Guyana"}, _
        New CountryCode() With {.Code = "HT", .CountryName = "Haiti"}, _
        New CountryCode() With {.Code = "HM", .CountryName = "Heard Island and McDonald Islands"}, _
        New CountryCode() With {.Code = "HN", .CountryName = "Honduras"}, _
        New CountryCode() With {.Code = "HK", .CountryName = "Hong Kong SAR China"}, _
        New CountryCode() With {.Code = "HU", .CountryName = "Hungary"}, _
        New CountryCode() With {.Code = "IS", .CountryName = "Iceland"}, _
        New CountryCode() With {.Code = "IN", .CountryName = "India"}, _
        New CountryCode() With {.Code = "ID", .CountryName = "Indonesia"}, _
        New CountryCode() With {.Code = "IR", .CountryName = "Iran"}, _
        New CountryCode() With {.Code = "IQ", .CountryName = "Iraq"}, _
        New CountryCode() With {.Code = "IE", .CountryName = "Ireland"}, _
        New CountryCode() With {.Code = "IM", .CountryName = "Isle of Man"}, _
        New CountryCode() With {.Code = "IL", .CountryName = "Israel"}, _
        New CountryCode() With {.Code = "IT", .CountryName = "Italy"}, _
        New CountryCode() With {.Code = "JM", .CountryName = "Jamaica"}, _
        New CountryCode() With {.Code = "JP", .CountryName = "Japan"}, _
        New CountryCode() With {.Code = "JE", .CountryName = "Jersey"}, _
        New CountryCode() With {.Code = "JT", .CountryName = "Johnston Island"}, _
        New CountryCode() With {.Code = "JO", .CountryName = "Jordan"}, _
        New CountryCode() With {.Code = "KZ", .CountryName = "Kazakhstan"}, _
        New CountryCode() With {.Code = "KE", .CountryName = "Kenya"}, _
        New CountryCode() With {.Code = "KI", .CountryName = "Kiribati"}, _
        New CountryCode() With {.Code = "KW", .CountryName = "Kuwait"}, _
        New CountryCode() With {.Code = "KG", .CountryName = "Kyrgyzstan"}, _
        New CountryCode() With {.Code = "LA", .CountryName = "Laos"}, _
        New CountryCode() With {.Code = "LV", .CountryName = "Latvia"}, _
        New CountryCode() With {.Code = "LB", .CountryName = "Lebanon"}, _
        New CountryCode() With {.Code = "LS", .CountryName = "Lesotho"}, _
        New CountryCode() With {.Code = "LR", .CountryName = "Liberia"}, _
        New CountryCode() With {.Code = "LY", .CountryName = "Libya"}, _
        New CountryCode() With {.Code = "LI", .CountryName = "Liechtenstein"}, _
        New CountryCode() With {.Code = "LT", .CountryName = "Lithuania"}, _
        New CountryCode() With {.Code = "LU", .CountryName = "Luxembourg"}, _
        New CountryCode() With {.Code = "MO", .CountryName = "Macau SAR China"}, _
        New CountryCode() With {.Code = "MK", .CountryName = "Macedonia"}, _
        New CountryCode() With {.Code = "MG", .CountryName = "Madagascar"}, _
        New CountryCode() With {.Code = "MW", .CountryName = "Malawi"}, _
        New CountryCode() With {.Code = "MY", .CountryName = "Malaysia"}, _
        New CountryCode() With {.Code = "MV", .CountryName = "Maldives"}, _
        New CountryCode() With {.Code = "ML", .CountryName = "Mali"}, _
        New CountryCode() With {.Code = "MT", .CountryName = "Malta"}, _
        New CountryCode() With {.Code = "MH", .CountryName = "Marshall Islands"}, _
        New CountryCode() With {.Code = "MQ", .CountryName = "Martinique"}, _
        New CountryCode() With {.Code = "MR", .CountryName = "Mauritania"}, _
        New CountryCode() With {.Code = "MU", .CountryName = "Mauritius"}, _
        New CountryCode() With {.Code = "YT", .CountryName = "Mayotte"}, _
        New CountryCode() With {.Code = "FX", .CountryName = "Metropolitan France"}, _
        New CountryCode() With {.Code = "MX", .CountryName = "Mexico"}, _
        New CountryCode() With {.Code = "FM", .CountryName = "Micronesia"}, _
        New CountryCode() With {.Code = "MI", .CountryName = "Midway Islands"}, _
        New CountryCode() With {.Code = "MD", .CountryName = "Moldova"}, _
        New CountryCode() With {.Code = "MC", .CountryName = "Monaco"}, _
        New CountryCode() With {.Code = "MN", .CountryName = "Mongolia"}, _
        New CountryCode() With {.Code = "ME", .CountryName = "Montenegro"}, _
        New CountryCode() With {.Code = "MS", .CountryName = "Montserrat"}, _
        New CountryCode() With {.Code = "MA", .CountryName = "Morocco"}, _
        New CountryCode() With {.Code = "MZ", .CountryName = "Mozambique"}, _
        New CountryCode() With {.Code = "MM", .CountryName = "Myanmar [Burma]"}, _
        New CountryCode() With {.Code = "NA", .CountryName = "Namibia"}, _
        New CountryCode() With {.Code = "NR", .CountryName = "Nauru"}, _
        New CountryCode() With {.Code = "NP", .CountryName = "Nepal"}, _
        New CountryCode() With {.Code = "NL", .CountryName = "Netherlands"}, _
        New CountryCode() With {.Code = "AN", .CountryName = "Netherlands Antilles"}, _
        New CountryCode() With {.Code = "NT", .CountryName = "Neutral Zone"}, _
        New CountryCode() With {.Code = "NC", .CountryName = "New Caledonia"}, _
        New CountryCode() With {.Code = "NZ", .CountryName = "New Zealand"}, _
        New CountryCode() With {.Code = "NI", .CountryName = "Nicaragua"}, _
        New CountryCode() With {.Code = "NE", .CountryName = "Niger"}, _
        New CountryCode() With {.Code = "NG", .CountryName = "Nigeria"}, _
        New CountryCode() With {.Code = "NU", .CountryName = "Niue"}, _
        New CountryCode() With {.Code = "NF", .CountryName = "Norfolk Island"}, _
        New CountryCode() With {.Code = "KP", .CountryName = "North Korea"}, _
        New CountryCode() With {.Code = "VD", .CountryName = "North Vietnam"}, _
        New CountryCode() With {.Code = "MP", .CountryName = "Northern Mariana Islands"}, _
        New CountryCode() With {.Code = "NO", .CountryName = "Norway"}, _
        New CountryCode() With {.Code = "OM", .CountryName = "Oman"}, _
        New CountryCode() With {.Code = "PC", .CountryName = "Pacific Islands Trust Territory"}, _
        New CountryCode() With {.Code = "PK", .CountryName = "Pakistan"}, _
        New CountryCode() With {.Code = "PW", .CountryName = "Palau"}, _
        New CountryCode() With {.Code = "PS", .CountryName = "Palestinian Territories"}, _
        New CountryCode() With {.Code = "PA", .CountryName = "Panama"}, _
        New CountryCode() With {.Code = "PZ", .CountryName = "Panama Canal Zone"}, _
        New CountryCode() With {.Code = "PG", .CountryName = "Papua New Guinea"}, _
        New CountryCode() With {.Code = "PY", .CountryName = "Paraguay"}, _
        New CountryCode() With {.Code = "YD", .CountryName = "People""s Democratic Republic of Yemen"}, _
        New CountryCode() With {.Code = "PE", .CountryName = "Peru"}, _
        New CountryCode() With {.Code = "PH", .CountryName = "Philippines"}, _
        New CountryCode() With {.Code = "PN", .CountryName = "Pitcairn Islands"}, _
        New CountryCode() With {.Code = "PL", .CountryName = "Poland"}, _
        New CountryCode() With {.Code = "PT", .CountryName = "Portugal"}, _
        New CountryCode() With {.Code = "PR", .CountryName = "Puerto Rico"}, _
        New CountryCode() With {.Code = "QA", .CountryName = "Qatar"}, _
        New CountryCode() With {.Code = "RO", .CountryName = "Romania"}, _
        New CountryCode() With {.Code = "RU", .CountryName = "Russia"}, _
        New CountryCode() With {.Code = "RW", .CountryName = "Rwanda"}, _
        New CountryCode() With {.Code = "RE", .CountryName = "Réunion"}, _
        New CountryCode() With {.Code = "BL", .CountryName = "Saint Barthélemy"}, _
        New CountryCode() With {.Code = "SH", .CountryName = "Saint Helena"}, _
        New CountryCode() With {.Code = "KN", .CountryName = "Saint Kitts and Nevis"}, _
        New CountryCode() With {.Code = "LC", .CountryName = "Saint Lucia"}, _
        New CountryCode() With {.Code = "MF", .CountryName = "Saint Martin"}, _
        New CountryCode() With {.Code = "PM", .CountryName = "Saint Pierre and Miquelon"}, _
        New CountryCode() With {.Code = "VC", .CountryName = "Saint Vincent and the Grenadines"}, _
        New CountryCode() With {.Code = "WS", .CountryName = "Samoa"}, _
        New CountryCode() With {.Code = "SM", .CountryName = "San Marino"}, _
        New CountryCode() With {.Code = "SA", .CountryName = "Saudi Arabia"}, _
        New CountryCode() With {.Code = "SN", .CountryName = "Senegal"}, _
        New CountryCode() With {.Code = "RS", .CountryName = "Serbia"}, _
        New CountryCode() With {.Code = "CS", .CountryName = "Serbia and Montenegro"}, _
        New CountryCode() With {.Code = "SC", .CountryName = "Seychelles"}, _
        New CountryCode() With {.Code = "SL", .CountryName = "Sierra Leone"}, _
        New CountryCode() With {.Code = "SG", .CountryName = "Singapore"}, _
        New CountryCode() With {.Code = "SK", .CountryName = "Slovakia"}, _
        New CountryCode() With {.Code = "SI", .CountryName = "Slovenia"}, _
        New CountryCode() With {.Code = "SB", .CountryName = "Solomon Islands"}, _
        New CountryCode() With {.Code = "SO", .CountryName = "Somalia"}, _
        New CountryCode() With {.Code = "ZA", .CountryName = "South Africa"}, _
        New CountryCode() With {.Code = "GS", .CountryName = "South Georgia and the South Sandwich Islands"}, _
        New CountryCode() With {.Code = "KR", .CountryName = "South Korea"}, _
        New CountryCode() With {.Code = "ES", .CountryName = "Spain"}, _
        New CountryCode() With {.Code = "LK", .CountryName = "Sri Lanka"}, _
        New CountryCode() With {.Code = "SD", .CountryName = "Sudan"}, _
        New CountryCode() With {.Code = "SR", .CountryName = "Suriname"}, _
        New CountryCode() With {.Code = "SJ", .CountryName = "Svalbard and Jan Mayen"}, _
        New CountryCode() With {.Code = "SZ", .CountryName = "Swaziland"}, _
        New CountryCode() With {.Code = "SE", .CountryName = "Sweden"}, _
        New CountryCode() With {.Code = "CH", .CountryName = "Switzerland"}, _
        New CountryCode() With {.Code = "SY", .CountryName = "Syria"}, _
        New CountryCode() With {.Code = "ST", .CountryName = "São Tomé and Príncipe"}, _
        New CountryCode() With {.Code = "TW", .CountryName = "Taiwan"}, _
        New CountryCode() With {.Code = "TJ", .CountryName = "Tajikistan"}, _
        New CountryCode() With {.Code = "TZ", .CountryName = "Tanzania"}, _
        New CountryCode() With {.Code = "TH", .CountryName = "Thailand"}, _
        New CountryCode() With {.Code = "TL", .CountryName = "Timor-Leste"}, _
        New CountryCode() With {.Code = "TG", .CountryName = "Togo"}, _
        New CountryCode() With {.Code = "TK", .CountryName = "Tokelau"}, _
        New CountryCode() With {.Code = "TO", .CountryName = "Tonga"}, _
        New CountryCode() With {.Code = "TT", .CountryName = "Trinidad and Tobago"}, _
        New CountryCode() With {.Code = "TN", .CountryName = "Tunisia"}, _
        New CountryCode() With {.Code = "TR", .CountryName = "Turkey"}, _
        New CountryCode() With {.Code = "TM", .CountryName = "Turkmenistan"}, _
        New CountryCode() With {.Code = "TC", .CountryName = "Turks and Caicos Islands"}, _
        New CountryCode() With {.Code = "TV", .CountryName = "Tuvalu"}, _
        New CountryCode() With {.Code = "UM", .CountryName = "U.S. Minor Outlying Islands"}, _
        New CountryCode() With {.Code = "PU", .CountryName = "U.S. Miscellaneous Pacific Islands"}, _
        New CountryCode() With {.Code = "VI", .CountryName = "U.S. Virgin Islands"}, _
        New CountryCode() With {.Code = "UG", .CountryName = "Uganda"}, _
        New CountryCode() With {.Code = "UA", .CountryName = "Ukraine"}, _
        New CountryCode() With {.Code = "SU", .CountryName = "Union of Soviet Socialist Republics"}, _
        New CountryCode() With {.Code = "AE", .CountryName = "United Arab Emirates"}, _
        New CountryCode() With {.Code = "GB", .CountryName = "United Kingdom"}, _
        New CountryCode() With {.Code = "US", .CountryName = "United States"}, _
        New CountryCode() With {.Code = "ZZ", .CountryName = "Unknown or Invalid Region"}, _
        New CountryCode() With {.Code = "UY", .CountryName = "Uruguay"}, _
        New CountryCode() With {.Code = "UZ", .CountryName = "Uzbekistan"}, _
        New CountryCode() With {.Code = "VU", .CountryName = "Vanuatu"}, _
        New CountryCode() With {.Code = "VA", .CountryName = "Vatican City"}, _
        New CountryCode() With {.Code = "VE", .CountryName = "Venezuela"}, _
        New CountryCode() With {.Code = "VN", .CountryName = "Vietnam"}, _
        New CountryCode() With {.Code = "WK", .CountryName = "Wake Island"}, _
        New CountryCode() With {.Code = "WF", .CountryName = "Wallis and Futuna"}, _
        New CountryCode() With {.Code = "EH", .CountryName = "Western Sahara"}, _
        New CountryCode() With {.Code = "YE", .CountryName = "Yemen"}, _
        New CountryCode() With {.Code = "ZM", .CountryName = "Zambia"}, _
        New CountryCode() With {.Code = "ZW", .CountryName = "Zimbabwe"}, _
        New CountryCode() With {.Code = "AX", .CountryName = "Åland Islands"} _
        }
        countryCode.ForEach(Function(u) context.CountryCodes.Add(u))

        'State Code table creation
        Dim stateCode As New List(Of StateCode)() From { _
       New StateCode() With {.StateName = "Alabama", .Code = "AL", .CountryId = 250}, _
        New StateCode() With {.StateName = "Alaska", .Code = "AK", .CountryId = 250}, _
        New StateCode() With {.StateName = "Arizona", .Code = "AZ", .CountryId = 250}, _
        New StateCode() With {.StateName = "Arkansas", .Code = "AR", .CountryId = 250}, _
        New StateCode() With {.StateName = "California", .Code = "CA", .CountryId = 250}, _
        New StateCode() With {.StateName = "Colorado", .Code = "CO", .CountryId = 250}, _
        New StateCode() With {.StateName = "Connecticut", .Code = "CT", .CountryId = 250}, _
        New StateCode() With {.StateName = "Delaware", .Code = "DE", .CountryId = 250}, _
        New StateCode() With {.StateName = "District of Columbia", .Code = "DC", .CountryId = 250}, _
        New StateCode() With {.StateName = "Florida", .Code = "FL", .CountryId = 250}, _
        New StateCode() With {.StateName = "Georgia", .Code = "GA", .CountryId = 250}, _
        New StateCode() With {.StateName = "Hawaii", .Code = "HI", .CountryId = 250}, _
        New StateCode() With {.StateName = "Idaho", .Code = "ID", .CountryId = 250}, _
        New StateCode() With {.StateName = "Illinois", .Code = "IL", .CountryId = 250}, _
        New StateCode() With {.StateName = "Indiana", .Code = "IN", .CountryId = 250}, _
        New StateCode() With {.StateName = "Iowa", .Code = "IA", .CountryId = 250}, _
        New StateCode() With {.StateName = "Kansas", .Code = "KS", .CountryId = 250}, _
        New StateCode() With {.StateName = "Kentucky", .Code = "KY", .CountryId = 250}, _
        New StateCode() With {.StateName = "Louisiana", .Code = "LA", .CountryId = 250}, _
        New StateCode() With {.StateName = "Maine", .Code = "ME", .CountryId = 250}, _
        New StateCode() With {.StateName = "Maryland", .Code = "MD", .CountryId = 250}, _
        New StateCode() With {.StateName = "Massachusetts", .Code = "MA", .CountryId = 250}, _
        New StateCode() With {.StateName = "Michigan", .Code = "MI", .CountryId = 250}, _
        New StateCode() With {.StateName = "Minnesota", .Code = "MN", .CountryId = 250}, _
        New StateCode() With {.StateName = "Mississippi", .Code = "MS", .CountryId = 250}, _
        New StateCode() With {.StateName = "Missouri", .Code = "MO", .CountryId = 250}, _
        New StateCode() With {.StateName = "Montana", .Code = "MT", .CountryId = 250}, _
        New StateCode() With {.StateName = "Nebraska", .Code = "NE", .CountryId = 250}, _
        New StateCode() With {.StateName = "Nevada", .Code = "NV", .CountryId = 250}, _
        New StateCode() With {.StateName = "New Hampshire", .Code = "NH", .CountryId = 250}, _
        New StateCode() With {.StateName = "New Jersey", .Code = "NJ", .CountryId = 250}, _
        New StateCode() With {.StateName = "New Mexico", .Code = "NM", .CountryId = 250}, _
        New StateCode() With {.StateName = "New York", .Code = "NY", .CountryId = 250}, _
        New StateCode() With {.StateName = "North Carolina", .Code = "NC", .CountryId = 250}, _
        New StateCode() With {.StateName = "North Dakota", .Code = "ND", .CountryId = 250}, _
        New StateCode() With {.StateName = "Ohio", .Code = "OH", .CountryId = 250}, _
        New StateCode() With {.StateName = "Oklahoma", .Code = "OK", .CountryId = 250}, _
        New StateCode() With {.StateName = "Oregon", .Code = "OR", .CountryId = 250}, _
        New StateCode() With {.StateName = "Pennsylvania", .Code = "PA", .CountryId = 250}, _
        New StateCode() With {.StateName = "Rhode Island", .Code = "RI", .CountryId = 250}, _
        New StateCode() With {.StateName = "South Carolina", .Code = "SC", .CountryId = 250}, _
        New StateCode() With {.StateName = "South Dakota", .Code = "SD", .CountryId = 250}, _
        New StateCode() With {.StateName = "Tennessee", .Code = "TN", .CountryId = 250}, _
        New StateCode() With {.StateName = "Texas", .Code = "TX", .CountryId = 250}, _
        New StateCode() With {.StateName = "Utah", .Code = "UT", .CountryId = 250}, _
        New StateCode() With {.StateName = "Vermont", .Code = "VT", .CountryId = 250}, _
        New StateCode() With {.StateName = "Virginia", .Code = "VA", .CountryId = 250}, _
        New StateCode() With {.StateName = "Washington", .Code = "WA", .CountryId = 250}, _
        New StateCode() With {.StateName = "West Virginia", .Code = "WV", .CountryId = 250}, _
        New StateCode() With {.StateName = "Wisconsin", .Code = "WI", .CountryId = 250}, _
        New StateCode() With {.StateName = "Wyoming", .Code = "WY", .CountryId = 250} _
       }
        stateCode.ForEach(Function(u) context.StateCodes.Add(u))

        For Each acctItem As Account In acctList
            Select Case acctItem.Description
                Case "Total new sales"
                    Dim balList As New List(Of Balance)() From { _
                        New Balance() With {.BalanceId = IncrementBalanceIndex(balanceIndex), .H201 = 6259, .H202 = 5678, .H203 = 6995, .H204 = 7344, _
                   .H205 = 24305, .H206 = 16770, .H207 = 27823, .H208 = 22134, .H209 = 18725, .H210 = 14995, .H211 = 14847, _
                   .H212 = 14847, .AccountId = acctItem.AccountId, .AnalysisId = 1}, _
                       New Balance() With {.BalanceId = IncrementBalanceIndex(balanceIndex), .H101 = 23842, .H102 = 14397, .H103 = 10557, .H104 = 16244, _
                   .H105 = 5045, .H106 = 21047, .H107 = 20202, .H108 = 34245, .H109 = 29846, .H110 = 24246, .H111 = 37945, _
                   .H112 = 27996, .AccountId = acctItem.AccountId, .AnalysisId = 1}, _
                       New Balance() With {.BalanceId = IncrementBalanceIndex(balanceIndex), .A101 = 27755, .A102 = 12647, .A103 = 16671, .A104 = 24840, _
                   .A105 = 27046, .A106 = 21246, .A107 = 18223, .A108 = 31795, .A109 = 31094, .A110 = 40751, .A111 = 38444, _
                   .A112 = 0, .AccountId = acctItem.AccountId, .AnalysisId = 1}, _
                       New Balance() With {.BalanceId = IncrementBalanceIndex(balanceIndex), .B101 = 35126, .B102 = 25000, .B103 = 15000, .B104 = 1200, _
                   .B105 = 20000, .B106 = 25000, .B107 = 30000, .B108 = 33000, .B109 = 36000, .B110 = 39000, .B111 = 42000, _
                   .B112 = 45000, .AccountId = acctItem.AccountId, .AnalysisId = 1}
                   }
                    balList.ForEach(Function(u) context.Balances.Add(u))
                Case "Total upgrade sales"
                    Dim balList As New List(Of Balance)() From { _
                        New Balance() With {.BalanceId = IncrementBalanceIndex(balanceIndex), .H201 = 0, .H202 = 0, .H203 = 0, .H204 = 0, _
                    .H205 = 0, .H206 = 0, .H207 = 0, .H208 = 0, .H209 = 0, .H210 = 0, .H211 = 0, _
                    .H212 = 0, .AccountId = acctItem.AccountId, .AnalysisId = 1}, _
                        New Balance() With {.BalanceId = IncrementBalanceIndex(balanceIndex), .H101 = 4882, .H102 = 2948, .H103 = 2162, .H104 = 3327, _
                    .H105 = 57388, .H106 = 9708, .H107 = 10678, .H108 = 3829, .H109 = 2709, .H110 = 4429, .H111 = 1890, _
                    .H112 = 4669, .AccountId = acctItem.AccountId, .AnalysisId = 1}, _
                        New Balance() With {.BalanceId = IncrementBalanceIndex(balanceIndex), .A101 = 1360, .A102 = 4366, .A103 = 720, .A104 = 32619, _
                    .A105 = 18246, .A106 = 9808, .A107 = 5678, .A108 = 10248, .A109 = 1199, .A110 = 5399, .A111 = 6528, _
                    .A112 = 0, .AccountId = acctItem.AccountId, .AnalysisId = 1}, _
                        New Balance() With {.BalanceId = IncrementBalanceIndex(balanceIndex), .B101 = 0, .B102 = 0, .B103 = 0, .B104 = 7500, _
                    .B105 = 3000, .B106 = 12500, .B107 = 5000, .B108 = 3000, .B109 = 30000, .B110 = 3000, .B111 = 3000, _
                    .B112 = 3000, .AccountId = acctItem.AccountId, .AnalysisId = 1}
                    }
                    balList.ForEach(Function(u) context.Balances.Add(u))
                Case "Cloud sales"
                    Dim balList As New List(Of Balance)() From { _
                        New Balance() With {.BalanceId = IncrementBalanceIndex(balanceIndex), .H201 = 0, .H202 = 0, .H203 = 0, .H204 = 0, _
                    .H205 = 0, .H206 = 0, .H207 = 0, .H208 = 0, .H209 = 0, .H210 = 0, .H211 = 0, _
                    .H212 = 0, .AccountId = acctItem.AccountId, .AnalysisId = 1}, _
                        New Balance() With {.BalanceId = IncrementBalanceIndex(balanceIndex), .H101 = 0, .H102 = 0, .H103 = 0, .H104 = 0, _
                    .H105 = 0, .H106 = 0, .H107 = 0, .H108 = 0, .H109 = 0, .H110 = 0, .H111 = 0, _
                    .H112 = 0, .AccountId = acctItem.AccountId, .AnalysisId = 1}, _
                        New Balance() With {.BalanceId = IncrementBalanceIndex(balanceIndex), .A101 = 1, .A102 = 0, .A103 = 0, .A104 = 0, _
                    .A105 = 0, .A106 = 0, .A107 = 0, .A108 = 0, .A109 = 0, .A110 = 430, .A111 = 0, _
                    .A112 = 0, .AccountId = acctItem.AccountId, .AnalysisId = 1}, _
                        New Balance() With {.BalanceId = IncrementBalanceIndex(balanceIndex), .B101 = 1, .B102 = 0, .B103 = 0, .B104 = 0, _
                    .B105 = 0, .B106 = 0, .B107 = 0, .B108 = 0, .B109 = 0, .B110 = 0, .B111 = 0, _
                    .B112 = 0, .AccountId = acctItem.AccountId, .AnalysisId = 1}
                    }
                    balList.ForEach(Function(u) context.Balances.Add(u))
                Case "Consulting revenue"
                    Dim balList As New List(Of Balance)() From { _
                        New Balance() With {.BalanceId = IncrementBalanceIndex(balanceIndex), .H201 = 0, .H202 = 0, .H203 = 0, .H204 = 0, _
                    .H205 = 0, .H206 = 0, .H207 = 0, .H208 = 0, .H209 = 0, .H210 = 0, .H211 = 0, _
                    .H212 = 0, .AccountId = acctItem.AccountId, .AnalysisId = 1}, _
                        New Balance() With {.BalanceId = IncrementBalanceIndex(balanceIndex), .B101 = 1, .B102 = 0, .B103 = 0, .B104 = 0, _
                    .B105 = 0, .B106 = 0, .B107 = 0, .B108 = 0, .B109 = 0, .B110 = 0, .B111 = 0, _
                    .B112 = 0, .AccountId = acctItem.AccountId, .AnalysisId = 1}, _
                        New Balance() With {.BalanceId = IncrementBalanceIndex(balanceIndex), .A101 = 1, .A102 = 0, .A103 = 0, .A104 = 0, _
                    .A105 = 0, .A106 = 0, .A107 = 80, .A108 = 160, .A109 = 400, .A110 = 640, .A111 = 0, _
                    .A112 = 0, .AccountId = acctItem.AccountId, .AnalysisId = 1}, _
                        New Balance() With {.BalanceId = IncrementBalanceIndex(balanceIndex), .B101 = 1, .B102 = 0, .B103 = 0, .B104 = 0, _
                    .B105 = 0, .B106 = 0, .B107 = 0, .B108 = 0, .B109 = 0, .B110 = 0, .B111 = 0, _
                    .B112 = 0, .AccountId = acctItem.AccountId, .AnalysisId = 1}
                    }
                    balList.ForEach(Function(u) context.Balances.Add(u))
                Case "Shipping, returns & discounts"
                    Dim balList As New List(Of Balance)() From { _
                        New Balance() With {.BalanceId = IncrementBalanceIndex(balanceIndex), .H201 = 0, .H202 = 0, .H203 = -414, .H204 = 0, _
                    .H205 = 0, .H206 = -560, .H207 = -255, .H208 = -178, .H209 = 0, .H210 = -250, .H211 = -250, _
                    .H212 = 0, .AccountId = acctItem.AccountId, .AnalysisId = 1}, _
                        New Balance() With {.BalanceId = IncrementBalanceIndex(balanceIndex), .H101 = -4317, .H102 = -2605, .H103 = -1910, .H104 = -2940, _
                    .H105 = -8829, .H106 = -8567, .H107 = -10825, .H108 = -4421, .H109 = -6400, .H110 = -6282, .H111 = -2445, _
                    .H112 = -6480, .AccountId = acctItem.AccountId, .AnalysisId = 1}, _
                        New Balance() With {.BalanceId = IncrementBalanceIndex(balanceIndex), .A101 = -5099, .A102 = -4046, .A103 = -2649, .A104 = -11904, _
                    .A105 = -8546, .A106 = -4004, .A107 = -2740, .A108 = -5389, .A109 = -4634, .A110 = -7588, .A111 = -7967, _
                    .A112 = 0, .AccountId = acctItem.AccountId, .AnalysisId = 1}, _
                        New Balance() With {.BalanceId = IncrementBalanceIndex(balanceIndex), .B101 = -1756, .B102 = -1250, .B103 = -750, .B104 = -600, _
                    .B105 = -1000, .B106 = -1250, .B107 = -1500, .B108 = -1650, .B109 = -1800, .B110 = -1950, .B111 = -2100, _
                    .B112 = -2250, .AccountId = acctItem.AccountId, .AnalysisId = 1}
                    }
                    balList.ForEach(Function(u) context.Balances.Add(u))
                Case "Total revenue"
                    Dim balList As New List(Of Balance)() From { _
                        New Balance() With {.BalanceId = IncrementBalanceIndex(balanceIndex), .H201 = 6259, .H202 = 5678, .H203 = 6561, .H204 = 7344, _
                    .H205 = 24305, .H206 = 16210, .H207 = 27568, .H208 = 21956, .H209 = 18725, .H210 = 14745, .H211 = 14597, _
                    .H212 = 14863, .AccountId = acctItem.AccountId, .AnalysisId = 1}, _
                        New Balance() With {.BalanceId = IncrementBalanceIndex(balanceIndex), .H101 = 24652, .H102 = 14888, .H103 = 10918, .H104 = 16799, _
                    .H105 = 53604, .H106 = 22188, .H107 = 20055, .H108 = 33653, .H109 = 26155, .H110 = 22393, .H111 = 37390, _
                    .H112 = 28185, .AccountId = acctItem.AccountId, .AnalysisId = 1}, _
                        New Balance() With {.BalanceId = IncrementBalanceIndex(balanceIndex), .A101 = 24016, .A102 = 12967, .A103 = 14742, .A104 = 45555, _
                    .A105 = 36746, .A106 = 27050, .A107 = 21241, .A108 = 36814, .A109 = 28056, .A110 = 39632, .A111 = 36696, _
                    .A112 = 0, .AccountId = acctItem.AccountId, .AnalysisId = 1}, _
                        New Balance() With {.BalanceId = IncrementBalanceIndex(balanceIndex), .B101 = 33370, .B102 = 23750, .B103 = 14250, .B104 = 18900, _
                    .B105 = 49000, .B106 = 36250, .B107 = 33500, .B108 = 34350, .B109 = 37200, .B110 = 40050, .B111 = 42900, _
                    .B112 = 45750, .AccountId = acctItem.AccountId, .AnalysisId = 1}
                    }
                    balList.ForEach(Function(u) context.Balances.Add(u))
                Case "Save"
                    Dim balList As New List(Of Balance)() From { _
                        New Balance() With {.BalanceId = IncrementBalanceIndex(balanceIndex), .H101 = 0, .H102 = 0, .H103 = 0, .H104 = 0, _
                    .H105 = 0, .H106 = 0, .H107 = 0, .H108 = 0, .H109 = 0, .H110 = 0, .H111 = 0, _
                    .H112 = 0, .AccountId = acctItem.AccountId, .AnalysisId = 1}, _
                        New Balance() With {.BalanceId = IncrementBalanceIndex(balanceIndex), .A101 = 0, .A102 = 0, .A103 = 0, .A104 = 0, _
                    .A105 = 0, .A106 = 0, .A107 = 0, .A108 = 0, .A109 = 0, .A110 = 0, .A111 = 0, _
                    .A112 = 0, .AccountId = acctItem.AccountId, .AnalysisId = 1}, _
                        New Balance() With {.BalanceId = IncrementBalanceIndex(balanceIndex), .B101 = 0, .B102 = 0, .B103 = 0, .B104 = 0, _
                    .B105 = 0, .B106 = 0, .B107 = 0, .B108 = 0, .B109 = 0, .B110 = 0, .B111 = 0, _
                    .B112 = 0, .AccountId = acctItem.AccountId, .AnalysisId = 1}
                    }
                    balList.ForEach(Function(u) context.Balances.Add(u))
            End Select
        Next

        acctType = New AcctType() With {.AcctTypeId = 3, .TypeDesc = "Expense", .ClassDesc = "Cost of sales", .SubclassDesc = "Cost of sales", .AnalysisId = 1}
        context.AcctTypes.Add(acctType)

        acctList = New List(Of Account)() From { _
         New Account() With {.AccountId = 15, .AcctDescriptor = "Heading", .SortSequence = 10, .Description = "Cost of sales", .AcctTypeId = acctType.AcctTypeId, .AnalysisId = 1}, _
         New Account() With {.AccountId = 16, .AcctDescriptor = "Detail", .SortSequence = 11, .Description = "Solo server license fees", .AcctTypeId = acctType.AcctTypeId, .AnalysisId = 1}, _
         New Account() With {.AccountId = 17, .AcctDescriptor = "Detail", .SortSequence = 12, .Description = "Credit card processing fees", .AcctTypeId = acctType.AcctTypeId, .AnalysisId = 1}, _
         New Account() With {.AccountId = 18, .AcctDescriptor = "Detail", .SortSequence = 13, .Description = "Packaging and shipping", .AcctTypeId = acctType.AcctTypeId, .AnalysisId = 1}, _
         New Account() With {.AccountId = 19, .AcctDescriptor = "Detail", .SortSequence = 14, .Description = "Commissions and referral fees", .AcctTypeId = acctType.AcctTypeId, .AnalysisId = 1}, _
         New Account() With {.AccountId = 20, .AcctDescriptor = "Total", .SortSequence = 15, .Description = "Total Cost of sales", .AcctTypeId = acctType.AcctTypeId, .AnalysisId = 1} _
        }
        acctList.ForEach(Function(u) context.Accounts.Add(u))

        For Each acctItem As Account In acctList
            Select Case acctItem.Description
                Case "Solo server license fees"
                    Dim balList As New List(Of Balance)() From { _
                        New Balance() With {.BalanceId = IncrementBalanceIndex(balanceIndex), .H201 = 0, .H202 = 0, .H203 = 0, .H204 = 0, _
                    .H205 = 0, .H206 = 0, .H207 = 0, .H208 = 0, .H209 = 0, .H210 = 0, .H211 = 0, _
                    .H212 = 0, .AccountId = acctItem.AccountId, .AnalysisId = 1}, _
                        New Balance() With {.BalanceId = IncrementBalanceIndex(balanceIndex), .H101 = 0, .H102 = 0, .H103 = 0, .H104 = 0, _
                    .H105 = 0, .H106 = 0, .H107 = 0, .H108 = 579, .H109 = 394, .H110 = 535, .H111 = 415, _
                    .H112 = 0, .AccountId = acctItem.AccountId, .AnalysisId = 1}, _
                        New Balance() With {.BalanceId = IncrementBalanceIndex(balanceIndex), .A101 = 648, .A102 = 623, .A103 = 617, .A104 = 614, _
                    .A105 = 688, .A106 = 667, .A107 = 593, .A108 = 703, .A109 = 677, .A110 = 642, .A111 = 641, _
                    .A112 = 0, .AccountId = acctItem.AccountId, .AnalysisId = 1}, _
                        New Balance() With {.BalanceId = IncrementBalanceIndex(balanceIndex), .B101 = 584, .B102 = 416, .B103 = 249, .B104 = 331, _
                    .B105 = 858, .B106 = 634, .B107 = 586, .B108 = 601, .B109 = 651, .B110 = 701, .B111 = 751, _
                    .B112 = 801, .AccountId = acctItem.AccountId, .AnalysisId = 1}
                    }
                    balList.ForEach(Function(u) context.Balances.Add(u))
                Case "Credit card processing fees"
                    Dim balList As New List(Of Balance)() From { _
                        New Balance() With {.BalanceId = IncrementBalanceIndex(balanceIndex), .H201 = 54, .H202 = 0, .H203 = 0, .H204 = 0, _
                    .H205 = 0, .H206 = 0, .H207 = 0, .H208 = 0, .H209 = 0, .H210 = 0, .H211 = 0, _
                    .H212 = 0, .AccountId = acctItem.AccountId, .AnalysisId = 1}, _
                        New Balance() With {.BalanceId = IncrementBalanceIndex(balanceIndex), .H101 = 725, .H102 = 432, .H103 = 322, .H104 = 560, _
                    .H105 = 1329, .H106 = 662, .H107 = 0, .H108 = 875, .H109 = 713, .H110 = 682, .H111 = 1022, _
                    .H112 = 0, .AccountId = acctItem.AccountId, .AnalysisId = 1}, _
                        New Balance() With {.BalanceId = IncrementBalanceIndex(balanceIndex), .A101 = 744, .A102 = 437, .A103 = 451, .A104 = 1346, _
                    .A105 = 1071, .A106 = 731, .A107 = 547, .A108 = 1089, .A109 = 731, .A110 = 1188, .A111 = 957, _
                    .A112 = 0, .AccountId = acctItem.AccountId, .AnalysisId = 1}, _
                        New Balance() With {.BalanceId = IncrementBalanceIndex(balanceIndex), .B101 = 918, .B102 = 653, .B103 = 392, .B104 = 420, _
                    .B105 = 1348, .B106 = 997, .B107 = 921, .B108 = 945, .B109 = 1023, .B110 = 1101, .B111 = 1180, _
                    .B112 = 1258, .AccountId = acctItem.AccountId, .AnalysisId = 1}
                    }
                    balList.ForEach(Function(u) context.Balances.Add(u))
                Case "Packaging and shipping"
                    Dim balList As New List(Of Balance)() From { _
                        New Balance() With {.BalanceId = IncrementBalanceIndex(balanceIndex), .H201 = 252, .H202 = 75, .H203 = 32, .H204 = 63, _
                    .H205 = 26, .H206 = 101, .H207 = 593, .H208 = 132, .H209 = 310, .H210 = 387, .H211 = 162, _
                    .H212 = 42, .AccountId = acctItem.AccountId, .AnalysisId = 1}, _
                        New Balance() With {.BalanceId = IncrementBalanceIndex(balanceIndex), .H101 = 47, .H102 = 26, .H103 = 78, .H104 = 59, _
                    .H105 = 10, .H106 = 77, .H107 = 0, .H108 = 669, .H109 = 82, .H110 = 26, .H111 = 91, _
                    .H112 = 0, .AccountId = acctItem.AccountId, .AnalysisId = 1}, _
                        New Balance() With {.BalanceId = IncrementBalanceIndex(balanceIndex), .A101 = 66, .A102 = 66, .A103 = 96, .A104 = 66, _
                    .A105 = 0, .A106 = 360, .A107 = 208, .A108 = 158, .A109 = 0, .A110 = 0, .A111 = 0, _
                    .A112 = 0, .AccountId = acctItem.AccountId, .AnalysisId = 1}, _
                        New Balance() With {.BalanceId = IncrementBalanceIndex(balanceIndex), .B101 = 167, .B102 = 119, .B103 = 71, .B104 = 95, _
                    .B105 = 245, .B106 = 181, .B107 = 168, .B108 = 172, .B109 = 186, .B110 = 200, .B111 = 215, _
                    .B112 = 229, .AccountId = acctItem.AccountId, .AnalysisId = 1}
                    }
                    balList.ForEach(Function(u) context.Balances.Add(u))
                Case "Commissions and referral fees"
                    Dim balList As New List(Of Balance)() From { _
                        New Balance() With {.BalanceId = IncrementBalanceIndex(balanceIndex), .H201 = 0, .H202 = 0, .H203 = 0, .H204 = 0, _
                    .H205 = 0, .H206 = 0, .H207 = 0, .H208 = 0, .H209 = 0, .H210 = 0, .H211 = 0, _
                    .H212 = 0, .AccountId = acctItem.AccountId, .AnalysisId = 1}, _
                        New Balance() With {.BalanceId = IncrementBalanceIndex(balanceIndex), .H101 = 0, .H102 = 0, .H103 = 0, .H104 = 0, _
                    .H105 = 0, .H106 = 0, .H107 = 0, .H108 = 0, .H109 = 0, .H110 = 0, .H111 = 0, _
                    .H112 = 0, .AccountId = acctItem.AccountId, .AnalysisId = 1}, _
                        New Balance() With {.BalanceId = IncrementBalanceIndex(balanceIndex), .A101 = 0, .A102 = 0, .A103 = 588, .A104 = 1639, _
                    .A105 = 1037, .A106 = 728, .A107 = 1189, .A108 = 2128, .A109 = 1339, .A110 = 3056, .A111 = 1132, _
                    .A112 = 0, .AccountId = acctItem.AccountId, .AnalysisId = 1}, _
                        New Balance() With {.BalanceId = IncrementBalanceIndex(balanceIndex), .B101 = 0, .B102 = 0, .B103 = 0, .B104 = 0, _
                    .B105 = 0, .B106 = 0, .B107 = 0, .B108 = 0, .B109 = 0, .B110 = 0, .B111 = 0, _
                    .B112 = 0, .AccountId = acctItem.AccountId, .AnalysisId = 1}
                    }
                    balList.ForEach(Function(u) context.Balances.Add(u))
                Case "Total Cost of sales"
                    Dim balList As New List(Of Balance)() From { _
                        New Balance() With {.BalanceId = IncrementBalanceIndex(balanceIndex), .H201 = 306, .H202 = 75, .H203 = 32, .H204 = 63, _
                    .H205 = 26, .H206 = 101, .H207 = 593, .H208 = 132, .H209 = 310, .H210 = 387, .H211 = 162, _
                    .H212 = 42, .AccountId = acctItem.AccountId, .AnalysisId = 1}, _
                        New Balance() With {.BalanceId = IncrementBalanceIndex(balanceIndex), .H101 = 772, .H102 = 458, .H103 = 400, .H104 = 619, _
                    .H105 = 1339, .H106 = 739, .H107 = 0, .H108 = 2123, .H109 = 1189, .H110 = 1243, .H111 = 1523, _
                    .H112 = 0, .AccountId = acctItem.AccountId, .AnalysisId = 1}, _
                        New Balance() With {.BalanceId = IncrementBalanceIndex(balanceIndex), .A101 = 1458, .A102 = 1126, .A103 = 1752, .A104 = 2796, _
                    .A105 = 2796, .A106 = 2486, .A107 = 2537, .A108 = 4078, .A109 = 2747, .A110 = 4886, .A111 = 3096, _
                    .A112 = 0, .AccountId = acctItem.AccountId, .AnalysisId = 1}, _
                        New Balance() With {.BalanceId = IncrementBalanceIndex(balanceIndex), .B101 = 1669, .B102 = 1188, .B103 = 712, .B104 = 946, _
                    .B105 = 2451, .B106 = 1812, .B107 = 1675, .B108 = 1718, .B109 = 1860, .B110 = 2002, .B111 = 2146, _
                    .B112 = 2288, .AccountId = acctItem.AccountId, .AnalysisId = 1}
                    }
                    balList.ForEach(Function(u) context.Balances.Add(u))

            End Select

        Next

        acctType = New AcctType() With {.AcctTypeId = 4, .TypeDesc = "Expense", .ClassDesc = "Sales and marketing expenses", .SubclassDesc = "Sales and marketing expenses", .AnalysisId = 1}
        context.AcctTypes.Add(acctType)

        acctList = New List(Of Account)() From { _
         New Account() With {.AccountId = 21, .AcctDescriptor = "Heading", .SortSequence = 16, .Description = "Sales and marketing expenses", .AcctTypeId = acctType.AcctTypeId, .AnalysisId = 1}, _
        New Account() With {.AccountId = 22, .AcctDescriptor = "Detail", .SortSequence = 17, .Description = "Online advertising", .AcctTypeId = acctType.AcctTypeId, .AnalysisId = 1}, _
        New Account() With {.AccountId = 23, .AcctDescriptor = "Detail", .SortSequence = 18, .Description = "Brafton content development", .AcctTypeId = acctType.AcctTypeId, .AnalysisId = 1}, _
        New Account() With {.AccountId = 24, .AcctDescriptor = "Detail", .SortSequence = 19, .Description = "Other sales and marketing", .AcctTypeId = acctType.AcctTypeId, .AnalysisId = 1}, _
        New Account() With {.AccountId = 25, .AcctDescriptor = "Detail", .SortSequence = 20, .Description = "Trade show expenses", .AcctTypeId = acctType.AcctTypeId, .AnalysisId = 1}, _
        New Account() With {.AccountId = 26, .AcctDescriptor = "Detail", .SortSequence = 21, .Description = "Direct mail", .AcctTypeId = acctType.AcctTypeId, .AnalysisId = 1}, _
        New Account() With {.AccountId = 27, .AcctDescriptor = "Total", .SortSequence = 22, .Description = "Total sales and marketing", .AcctTypeId = acctType.AcctTypeId, .AnalysisId = 1} _
       }
        acctList.ForEach(Function(u) context.Accounts.Add(u))

        For Each acctItem As Account In acctList
            Select Case acctItem.Description
                Case "Online advertising"
                    Dim balList As New List(Of Balance)() From { _
                        New Balance() With {.BalanceId = IncrementBalanceIndex(balanceIndex), .H201 = 507, .H202 = 567, .H203 = 1014, .H204 = 952, _
                    .H205 = 1043, .H206 = 1421, .H207 = 1147, .H208 = 3628, .H209 = 1454, .H210 = 1312, .H211 = 3307, _
                    .H212 = 2233, .AccountId = acctItem.AccountId, .AnalysisId = 1}, _
                        New Balance() With {.BalanceId = IncrementBalanceIndex(balanceIndex), .H101 = 2467, .H102 = 6822, .H103 = 3604, .H104 = 2981, _
                    .H105 = 3055, .H106 = 1976, .H107 = 2263, .H108 = 2296, .H109 = 2612, .H110 = 6590, .H111 = 2650, _
                    .H112 = 3500, .AccountId = acctItem.AccountId, .AnalysisId = 1}, _
                        New Balance() With {.BalanceId = IncrementBalanceIndex(balanceIndex), .A101 = 3224, .A102 = 3226, .A103 = 3225, .A104 = 3225, _
                    .A105 = 3224, .A106 = 3326, .A107 = 3325, .A108 = 4068, .A109 = 4018, .A110 = 4657, .A111 = 4426, _
                    .A112 = 0, .AccountId = acctItem.AccountId, .AnalysisId = 1}, _
                        New Balance() With {.BalanceId = IncrementBalanceIndex(balanceIndex), .B101 = 3000, .B102 = 3000, .B103 = 3000, .B104 = 3000, _
                    .B105 = 5000, .B106 = 5000, .B107 = 5000, .B108 = 5000, .B109 = 5000, .B110 = 5000, .B111 = 5000, _
                    .B112 = 0, .AccountId = acctItem.AccountId, .AnalysisId = 1}
                    }
                    balList.ForEach(Function(u) context.Balances.Add(u))
                Case "Brafton content development"
                    Dim balList As New List(Of Balance)() From { _
                        New Balance() With {.BalanceId = IncrementBalanceIndex(balanceIndex), .H201 = 0, .H202 = 0, .H203 = 0, .H204 = 0, _
                    .H205 = 0, .H206 = 0, .H207 = 0, .H208 = 0, .H209 = 0, .H210 = 0, .H211 = 0, _
                    .H212 = 0, .AccountId = acctItem.AccountId, .AnalysisId = 1}, _
                        New Balance() With {.BalanceId = IncrementBalanceIndex(balanceIndex), .H101 = 0, .H102 = 0, .H103 = 0, .H104 = 0, _
                    .H105 = 0, .H106 = 0, .H107 = 0, .H108 = 0, .H109 = 0, .H110 = 0, .H111 = 0, _
                    .H112 = 0, .AccountId = acctItem.AccountId, .AnalysisId = 1}, _
                        New Balance() With {.BalanceId = IncrementBalanceIndex(balanceIndex), .A101 = 0, .A102 = 0, .A103 = 0, .A104 = 0, _
                    .A105 = 1000, .A106 = 2200, .A107 = 2200, .A108 = 2200, .A109 = 2200, .A110 = 1469, .A111 = 1000, _
                    .A112 = 0, .AccountId = acctItem.AccountId, .AnalysisId = 1}, _
                        New Balance() With {.BalanceId = IncrementBalanceIndex(balanceIndex), .B101 = 0, .B102 = 0, .B103 = 0, .B104 = 0, _
                    .B105 = 0, .B106 = 0, .B107 = 0, .B108 = 0, .B109 = 0, .B110 = 0, .B111 = 0, _
                    .B112 = 0, .AccountId = acctItem.AccountId, .AnalysisId = 1}
                    }
                    balList.ForEach(Function(u) context.Balances.Add(u))
                Case "Other sales and marketing"
                    Dim balList As New List(Of Balance)() From { _
                        New Balance() With {.BalanceId = IncrementBalanceIndex(balanceIndex), .H201 = 0, .H202 = 0, .H203 = 0, .H204 = 0, _
                    .H205 = 0, .H206 = 283, .H207 = 2105, .H208 = 2947, .H209 = 4111, .H210 = 2680, .H211 = 1272, _
                    .H212 = 1318, .AccountId = acctItem.AccountId, .AnalysisId = 1}, _
                        New Balance() With {.BalanceId = IncrementBalanceIndex(balanceIndex), .H101 = 272, .H102 = 713, .H103 = 326, .H104 = 316, _
                    .H105 = 465, .H106 = 675, .H107 = 1249, .H108 = 490, .H109 = 533, .H110 = 804, .H111 = 575, _
                    .H112 = 0, .AccountId = acctItem.AccountId, .AnalysisId = 1}, _
                        New Balance() With {.BalanceId = IncrementBalanceIndex(balanceIndex), .A101 = 572, .A102 = 447, .A103 = 391, .A104 = 410, _
                    .A105 = 201, .A106 = 510, .A107 = 341, .A108 = 951, .A109 = 1241, .A110 = 2404, .A111 = 1550, _
                    .A112 = 0, .AccountId = acctItem.AccountId, .AnalysisId = 1}, _
                        New Balance() With {.BalanceId = IncrementBalanceIndex(balanceIndex), .B101 = 770, .B102 = 770, .B103 = 770, .B104 = 770, _
                    .B105 = 770, .B106 = 770, .B107 = 770, .B108 = 770, .B109 = 770, .B110 = 770, .B111 = 770, _
                    .B112 = 770, .AccountId = acctItem.AccountId, .AnalysisId = 1}
                    }
                    balList.ForEach(Function(u) context.Balances.Add(u))
                Case "Trade show expenses"
                    Dim balList As New List(Of Balance)() From { _
                        New Balance() With {.BalanceId = IncrementBalanceIndex(balanceIndex), .H201 = 0, .H202 = 0, .H203 = 0, .H204 = 0, _
                    .H205 = 0, .H206 = 0, .H207 = 0, .H208 = 0, .H209 = 0, .H210 = 0, .H211 = 0, _
                    .H212 = 0, .AccountId = acctItem.AccountId, .AnalysisId = 1}, _
                        New Balance() With {.BalanceId = IncrementBalanceIndex(balanceIndex), .H101 = 0, .H102 = 0, .H103 = 0, .H104 = 0, _
                    .H105 = 0, .H106 = 2761, .H107 = 2043, .H108 = 2065, .H109 = 321, .H110 = 5617, .H111 = 173, _
                    .H112 = 0, .AccountId = acctItem.AccountId, .AnalysisId = 1}, _
                        New Balance() With {.BalanceId = IncrementBalanceIndex(balanceIndex), .A101 = 3200, .A102 = 0, .A103 = 0, .A104 = 0, _
                    .A105 = 0, .A106 = 0, .A107 = 0, .A108 = 0, .A109 = 598, .A110 = 1853, .A111 = 867, _
                    .A112 = 0, .AccountId = acctItem.AccountId, .AnalysisId = 1}, _
                        New Balance() With {.BalanceId = IncrementBalanceIndex(balanceIndex), .B101 = 0, .B102 = 0, .B103 = 0, .B104 = 0, _
                    .B105 = 5000, .B106 = 0, .B107 = 0, .B108 = 0, .B109 = 5000, .B110 = 0, .B111 = 0, _
                    .B112 = 0, .AccountId = acctItem.AccountId, .AnalysisId = 1}
                    }
                    balList.ForEach(Function(u) context.Balances.Add(u))
                Case "Direct mail"
                    Dim balList As New List(Of Balance)() From { _
                        New Balance() With {.BalanceId = IncrementBalanceIndex(balanceIndex), .H201 = 0, .H202 = 0, .H203 = 801, .H204 = 0, _
                    .H205 = 1437, .H206 = 998, .H207 = 1957, .H208 = 1014, .H209 = 0, .H210 = 785, .H211 = 0, _
                    .H212 = 0, .AccountId = acctItem.AccountId, .AnalysisId = 1}, _
                        New Balance() With {.BalanceId = IncrementBalanceIndex(balanceIndex), .H101 = 0, .H102 = 0, .H103 = 0, .H104 = 0, _
                    .H105 = 4438, .H106 = 1373, .H107 = 1047, .H108 = 255, .H109 = 0, .H110 = 0, .H111 = 0, _
                    .H112 = 0, .AccountId = acctItem.AccountId, .AnalysisId = 1}, _
                        New Balance() With {.BalanceId = IncrementBalanceIndex(balanceIndex), .A101 = 0, .A102 = 0, .A103 = 0, .A104 = 0, _
                    .A105 = 4054, .A106 = -267, .A107 = -195, .A108 = 0, .A109 = 0, .A110 = 0, .A111 = 66, _
                    .A112 = 0, .AccountId = acctItem.AccountId, .AnalysisId = 1}, _
                        New Balance() With {.BalanceId = IncrementBalanceIndex(balanceIndex), .B101 = 0, .B102 = 0, .B103 = 0, .B104 = 2000, _
                    .B105 = 5000, .B106 = 0, .B107 = 0, .B108 = 0, .B109 = 0, .B110 = 0, .B111 = 0, _
                    .B112 = 0, .AccountId = acctItem.AccountId, .AnalysisId = 1}
                    }
                    balList.ForEach(Function(u) context.Balances.Add(u))
                Case "Total sales and marketing"
                    Dim balList As New List(Of Balance)() From { _
                        New Balance() With {.BalanceId = IncrementBalanceIndex(balanceIndex), .H201 = 507, .H202 = 567, .H203 = 1815, .H204 = 952, _
                    .H205 = 2480, .H206 = 2702, .H207 = 5209, .H208 = 7589, .H209 = 5565, .H210 = 4777, .H211 = 4579, _
                    .H212 = 3543, .AccountId = acctItem.AccountId, .AnalysisId = 1}, _
                        New Balance() With {.BalanceId = IncrementBalanceIndex(balanceIndex), .H101 = 2739, .H102 = 7535, .H103 = 3930, .H104 = 3297, _
                    .H105 = 7988, .H106 = 6785, .H107 = 6602, .H108 = 5107, .H109 = 3463, .H110 = 13011, .H111 = 3393, _
                    .H112 = 3503, .AccountId = acctItem.AccountId, .AnalysisId = 1}, _
                        New Balance() With {.BalanceId = IncrementBalanceIndex(balanceIndex), .A101 = 6996, .A102 = 3673, .A103 = 3616, .A104 = 3635, _
                    .A105 = 8479, .A106 = 5769, .A107 = 5671, .A108 = 7219, .A109 = 8055, .A110 = 10383, .A111 = 7909, _
                    .A112 = 0, .AccountId = acctItem.AccountId, .AnalysisId = 1}, _
                        New Balance() With {.BalanceId = IncrementBalanceIndex(balanceIndex), .B101 = 3770, .B102 = 3770, .B103 = 3770, .B104 = 5770, _
                    .B105 = 15770, .B106 = 5770, .B107 = 5770, .B108 = 5770, .B109 = 10770, .B110 = 5770, .B111 = 5770, _
                    .B112 = 5770, .AccountId = acctItem.AccountId, .AnalysisId = 1}
                    }
                    balList.ForEach(Function(u) context.Balances.Add(u))
            End Select

        Next

        acctType = New AcctType() With {.AcctTypeId = 5, .TypeDesc = "Expense", .ClassDesc = "Support expenses", .SubclassDesc = "Support expenses", .AnalysisId = 1}
        context.AcctTypes.Add(acctType)

        acctList = New List(Of Account)() From { _
         New Account() With {.AccountId = 28, .AcctDescriptor = "Heading", .SortSequence = 23, .Description = "Support expenses", .AcctTypeId = acctType.AcctTypeId, .AnalysisId = 1}, _
         New Account() With {.AccountId = 29, .AcctDescriptor = "Detail", .SortSequence = 24, .Description = "Zendesk", .AcctTypeId = acctType.AcctTypeId, .AnalysisId = 1}, _
         New Account() With {.AccountId = 30, .AcctDescriptor = "Total", .SortSequence = 25, .Description = "Total support expenses", .AcctTypeId = acctType.AcctTypeId, .AnalysisId = 1} _
       }
        acctList.ForEach(Function(u) context.Accounts.Add(u))

        For Each acctItem As Account In acctList
            Select Case acctItem.Description
                Case "Zendesk"
                    Dim balList As New List(Of Balance)() From { _
                        New Balance() With {.BalanceId = IncrementBalanceIndex(balanceIndex), .H201 = 0, .H202 = 0, .H203 = 0, .H204 = 0, _
                    .H205 = 0, .H206 = 0, .H207 = 0, .H208 = 0, .H209 = 0, .H210 = 0, .H211 = 0, _
                    .H212 = 0, .AccountId = acctItem.AccountId, .AnalysisId = 1}, _
                         New Balance() With {.BalanceId = IncrementBalanceIndex(balanceIndex), .H101 = 0, .H102 = 0, .H103 = 0, .H104 = 0, _
                    .H105 = 0, .H106 = 0, .H107 = 0, .H108 = 0, .H109 = 290, .H110 = 145, .H111 = 145, _
                    .H112 = 0, .AccountId = acctItem.AccountId, .AnalysisId = 1}, _
                        New Balance() With {.BalanceId = IncrementBalanceIndex(balanceIndex), .A101 = 0, .A102 = 145, .A103 = 145, .A104 = 131, _
                    .A105 = 116, .A106 = 255, .A107 = 145, .A108 = 220, .A109 = 220, .A110 = 220, .A111 = 0, _
                    .A112 = 0, .AccountId = acctItem.AccountId, .AnalysisId = 1}, _
                       New Balance() With {.BalanceId = IncrementBalanceIndex(balanceIndex), .B101 = 145, .B102 = 145, .B103 = 145, .B104 = 145, _
                    .B105 = 145, .B106 = 145, .B107 = 145, .B108 = 145, .B109 = 145, .B110 = 145, .B111 = 145, _
                    .B112 = 145, .AccountId = acctItem.AccountId, .AnalysisId = 1}
                    }
                    balList.ForEach(Function(u) context.Balances.Add(u))
                Case "Total support expenses"
                    Dim balList As New List(Of Balance)() From { _
                        New Balance() With {.BalanceId = IncrementBalanceIndex(balanceIndex), .H201 = 0, .H202 = 0, .H203 = 0, .H204 = 0, _
                    .H205 = 0, .H206 = 0, .H207 = 0, .H208 = 0, .H209 = 0, .H210 = 0, .H211 = 0, _
                    .H212 = 0, .AccountId = acctItem.AccountId, .AnalysisId = 1}, _
                          New Balance() With {.BalanceId = IncrementBalanceIndex(balanceIndex), .H101 = 0, .H102 = 0, .H103 = 0, .H104 = 0, _
                    .H105 = 0, .H106 = 0, .H107 = 0, .H108 = 0, .H109 = 290, .H110 = 145, .H111 = 145, _
                    .H112 = 0, .AccountId = acctItem.AccountId, .AnalysisId = 1}, _
                        New Balance() With {.BalanceId = IncrementBalanceIndex(balanceIndex), .A101 = 0, .A102 = 145, .A103 = 145, .A104 = 131, _
                    .A105 = 116, .A106 = 255, .A107 = 145, .A108 = 220, .A109 = 220, .A110 = 220, .A111 = 0, _
                    .A112 = 0, .AccountId = acctItem.AccountId, .AnalysisId = 1}, _
                        New Balance() With {.BalanceId = IncrementBalanceIndex(balanceIndex), .B101 = 145, .B102 = 145, .B103 = 145, .B104 = 145, _
                    .B105 = 145, .B106 = 145, .B107 = 145, .B108 = 145, .B109 = 145, .B110 = 145, .B111 = 145, _
                    .B112 = 145, .AccountId = acctItem.AccountId, .AnalysisId = 1}
                    }
                    balList.ForEach(Function(u) context.Balances.Add(u))
            End Select
        Next

        acctType = New AcctType() With {.AcctTypeId = 6, .TypeDesc = "Expense", .ClassDesc = "Operating expenses", .SubclassDesc = "Operating expenses", .AnalysisId = 1}
        context.AcctTypes.Add(acctType)

        acctList = New List(Of Account)() From { _
         New Account() With {.AccountId = 31, .AcctDescriptor = "Heading", .SortSequence = 26, .Description = "Operating expenses", .AcctTypeId = acctType.AcctTypeId, .AnalysisId = 1}, _
         New Account() With {.AccountId = 32, .AcctDescriptor = "Detail", .SortSequence = 27, .Description = "Employee compensation", .AcctTypeId = acctType.AcctTypeId, .AnalysisId = 1}, _
         New Account() With {.AccountId = 33, .AcctDescriptor = "Detail", .SortSequence = 28, .Description = "Home office expense", .AcctTypeId = acctType.AcctTypeId, .AnalysisId = 1}, _
         New Account() With {.AccountId = 34, .AcctDescriptor = "Detail", .SortSequence = 29, .Description = "Health insurance", .AcctTypeId = acctType.AcctTypeId, .AnalysisId = 1}, _
         New Account() With {.AccountId = 35, .AcctDescriptor = "Detail", .SortSequence = 30, .Description = "Life insurance", .AcctTypeId = acctType.AcctTypeId, .AnalysisId = 1}, _
         New Account() With {.AccountId = 36, .AcctDescriptor = "Detail", .SortSequence = 31, .Description = "Payroll taxes", .AcctTypeId = acctType.AcctTypeId, .AnalysisId = 1}, _
         New Account() With {.AccountId = 37, .AcctDescriptor = "Detail", .SortSequence = 32, .Description = "Professional services", .AcctTypeId = acctType.AcctTypeId, .AnalysisId = 1}, _
         New Account() With {.AccountId = 38, .AcctDescriptor = "Detail", .SortSequence = 33, .Description = "Internet services", .AcctTypeId = acctType.AcctTypeId, .AnalysisId = 1}, _
         New Account() With {.AccountId = 39, .AcctDescriptor = "Detail", .SortSequence = 34, .Description = "Other office expense", .AcctTypeId = acctType.AcctTypeId, .AnalysisId = 1}, _
         New Account() With {.AccountId = 40, .AcctDescriptor = "Detail", .SortSequence = 35, .Description = "Telephone", .AcctTypeId = acctType.AcctTypeId, .AnalysisId = 1}, _
         New Account() With {.AccountId = 41, .AcctDescriptor = "Detail", .SortSequence = 36, .Description = "Utilities", .AcctTypeId = acctType.AcctTypeId, .AnalysisId = 1}, _
         New Account() With {.AccountId = 42, .AcctDescriptor = "Detail", .SortSequence = 37, .Description = "Call center", .AcctTypeId = acctType.AcctTypeId, .AnalysisId = 1}, _
         New Account() With {.AccountId = 43, .AcctDescriptor = "Detail", .SortSequence = 38, .Description = "Corporate taxes", .AcctTypeId = acctType.AcctTypeId, .AnalysisId = 1}, _
         New Account() With {.AccountId = 44, .AcctDescriptor = "Detail", .SortSequence = 39, .Description = "Travel", .AcctTypeId = acctType.AcctTypeId, .AnalysisId = 1}, _
         New Account() With {.AccountId = 45, .AcctDescriptor = "Total", .SortSequence = 40, .Description = "Total operating expenses expenses", .AcctTypeId = acctType.AcctTypeId, .AnalysisId = 1} _
       }
        acctList.ForEach(Function(u) context.Accounts.Add(u))

        acctType = New AcctType() With {.AcctTypeId = 7, .TypeDesc = "Cash Flow", .ClassDesc = "Cash Flow activities", .SubclassDesc = "Cash Flow activities", .AnalysisId = 1}
        context.AcctTypes.Add(acctType)

        acctList = New List(Of Account)() From { _
         New Account() With {.AccountId = 46, .AcctDescriptor = "Heading", .SortSequence = 41, .Description = "Cash Flow Operations", .AcctTypeId = acctType.AcctTypeId, .AnalysisId = 1}, _
         New Account() With {.AccountId = 47, .AcctDescriptor = "Detail", .SortSequence = 42, .Description = "Sales", .AcctTypeId = acctType.AcctTypeId, .AnalysisId = 1}, _
         New Account() With {.AccountId = 48, .AcctDescriptor = "Detail", .SortSequence = 43, .Description = "Materials", .AcctTypeId = acctType.AcctTypeId, .AnalysisId = 1}, _
         New Account() With {.AccountId = 49, .AcctDescriptor = "Detail", .SortSequence = 44, .Description = "Labour", .AcctTypeId = acctType.AcctTypeId, .AnalysisId = 1}, _
         New Account() With {.AccountId = 50, .AcctDescriptor = "Detail", .SortSequence = 45, .Description = "Incoming Loan", .AcctTypeId = acctType.AcctTypeId, .AnalysisId = 1}, _
         New Account() With {.AccountId = 51, .AcctDescriptor = "Detail", .SortSequence = 46, .Description = "Loan Repayment", .AcctTypeId = acctType.AcctTypeId, .AnalysisId = 1}, _
         New Account() With {.AccountId = 52, .AcctDescriptor = "Detail", .SortSequence = 47, .Description = "Taxes", .AcctTypeId = acctType.AcctTypeId, .AnalysisId = 1}, _
         New Account() With {.AccountId = 53, .AcctDescriptor = "Detail", .SortSequence = 48, .Description = "Purchased Capital", .AcctTypeId = acctType.AcctTypeId, .AnalysisId = 1}, _
         New Account() With {.AccountId = 54, .AcctDescriptor = "Total", .SortSequence = 49, .Description = "Total Cash Flow Operations", .AcctTypeId = acctType.AcctTypeId, .AnalysisId = 1}
       }
        acctList.ForEach(Function(u) context.Accounts.Add(u))

        acctType = New AcctType() With {.AcctTypeId = 8, .TypeDesc = "Financial Ratio", .ClassDesc = "Liquidity Ratios", .SubclassDesc = "Liquidity Ratios", .AnalysisId = 1}
        context.AcctTypes.Add(acctType)

        acctList = New List(Of Account)() From { _
         New Account() With {.AccountId = 55, .AcctDescriptor = "Heading", .SortSequence = 50, .Description = "Liquidity Ratios", .AcctTypeId = acctType.AcctTypeId, .AnalysisId = 1}, _
         New Account() With {.AccountId = 56, .AcctDescriptor = "Detail", .SortSequence = 51, .Description = "Working Capital", .AcctTypeId = acctType.AcctTypeId, .AnalysisId = 1}, _
         New Account() With {.AccountId = 57, .AcctDescriptor = "Detail", .SortSequence = 52, .Description = "Current Ratio", .AcctTypeId = acctType.AcctTypeId, .AnalysisId = 1}, _
         New Account() With {.AccountId = 58, .AcctDescriptor = "Detail", .SortSequence = 53, .Description = "Quick Ratios", .AcctTypeId = acctType.AcctTypeId, .AnalysisId = 1}, _
         New Account() With {.AccountId = 59, .AcctDescriptor = "Detail", .SortSequence = 54, .Description = "Inventory Days", .AcctTypeId = acctType.AcctTypeId, .AnalysisId = 1}, _
         New Account() With {.AccountId = 60, .AcctDescriptor = "Detail", .SortSequence = 55, .Description = "Account Receivable Days", .AcctTypeId = acctType.AcctTypeId, .AnalysisId = 1}, _
         New Account() With {.AccountId = 61, .AcctDescriptor = "Detail", .SortSequence = 56, .Description = "Account Payable Days", .AcctTypeId = acctType.AcctTypeId, .AnalysisId = 1}, _
         New Account() With {.AccountId = 62, .AcctDescriptor = "Detail", .SortSequence = 57, .Description = "Incoming Loan", .AcctTypeId = acctType.AcctTypeId, .AnalysisId = 1}, _
         New Account() With {.AccountId = 63, .AcctDescriptor = "Detail", .SortSequence = 58, .Description = "Loan Repayment", .AcctTypeId = acctType.AcctTypeId, .AnalysisId = 1}, _
         New Account() With {.AccountId = 64, .AcctDescriptor = "Detail", .SortSequence = 59, .Description = "Taxes", .AcctTypeId = acctType.AcctTypeId, .AnalysisId = 1}, _
         New Account() With {.AccountId = 65, .AcctDescriptor = "Detail", .SortSequence = 60, .Description = "Purchased Capital", .AcctTypeId = acctType.AcctTypeId, .AnalysisId = 1}, _
         New Account() With {.AccountId = 66, .AcctDescriptor = "Total", .SortSequence = 61, .Description = "Total Liquidity Ratios", .AcctTypeId = acctType.AcctTypeId, .AnalysisId = 1}
       }
        acctList.ForEach(Function(u) context.Accounts.Add(u))

        acctType = New AcctType() With {.AcctTypeId = 9, .TypeDesc = "Financial Ratio", .ClassDesc = "Profit and Profit Margins", .SubclassDesc = "Profit and Profit Margins", .AnalysisId = 1}
        context.AcctTypes.Add(acctType)

        acctList = New List(Of Account)() From { _
         New Account() With {.AccountId = 67, .AcctDescriptor = "Heading", .SortSequence = 62, .Description = "Profit and Profit Margins", .AcctTypeId = acctType.AcctTypeId, .AnalysisId = 1}, _
         New Account() With {.AccountId = 68, .AcctDescriptor = "Detail", .SortSequence = 63, .Description = "Gross Profit Margin", .AcctTypeId = acctType.AcctTypeId, .AnalysisId = 1}, _
         New Account() With {.AccountId = 69, .AcctDescriptor = "Detail", .SortSequence = 64, .Description = "Net Profit Margin", .AcctTypeId = acctType.AcctTypeId, .AnalysisId = 1}, _
         New Account() With {.AccountId = 70, .AcctDescriptor = "Detail", .SortSequence = 65, .Description = "Advertising To Sales", .AcctTypeId = acctType.AcctTypeId, .AnalysisId = 1}, _
         New Account() With {.AccountId = 71, .AcctDescriptor = "Detail", .SortSequence = 66, .Description = "Rent To Sales", .AcctTypeId = acctType.AcctTypeId, .AnalysisId = 1}, _
         New Account() With {.AccountId = 72, .AcctDescriptor = "Detail", .SortSequence = 67, .Description = "Payroll To Sales", .AcctTypeId = acctType.AcctTypeId, .AnalysisId = 1}, _
         New Account() With {.AccountId = 73, .AcctDescriptor = "Total", .SortSequence = 68, .Description = "Total Profit and Profit Margins", .AcctTypeId = acctType.AcctTypeId, .AnalysisId = 1}
       }
        acctList.ForEach(Function(u) context.Accounts.Add(u))

        acctType = New AcctType() With {.AcctTypeId = 10, .TypeDesc = "Other Metrics", .ClassDesc = "Borrowing Ratios", .SubclassDesc = "Borrowing Ratios", .AnalysisId = 1}
        context.AcctTypes.Add(acctType)

        acctList = New List(Of Account)() From { _
         New Account() With {.AccountId = 74, .AcctDescriptor = "Heading", .SortSequence = 69, .Description = "Borrowing Ratios", .AcctTypeId = acctType.AcctTypeId, .AnalysisId = 1}, _
         New Account() With {.AccountId = 75, .AcctDescriptor = "Detail", .SortSequence = 70, .Description = "EBITDA", .AcctTypeId = acctType.AcctTypeId, .AnalysisId = 1}, _
         New Account() With {.AccountId = 76, .AcctDescriptor = "Detail", .SortSequence = 71, .Description = "Interest Coverage Ratio", .AcctTypeId = acctType.AcctTypeId, .AnalysisId = 1}, _
         New Account() With {.AccountId = 77, .AcctDescriptor = "Detail", .SortSequence = 72, .Description = "Debt-To-Equity Ratio", .AcctTypeId = acctType.AcctTypeId, .AnalysisId = 1}, _
         New Account() With {.AccountId = 78, .AcctDescriptor = "Detail", .SortSequence = 73, .Description = "Debt Leverage Ratio", .AcctTypeId = acctType.AcctTypeId, .AnalysisId = 1}, _
         New Account() With {.AccountId = 79, .AcctDescriptor = "Total", .SortSequence = 74, .Description = "Total Borrowing Ratios", .AcctTypeId = acctType.AcctTypeId, .AnalysisId = 1}
       }
        acctList.ForEach(Function(u) context.Accounts.Add(u))

        acctType = New AcctType() With {.AcctTypeId = 11, .TypeDesc = "Other Metrics", .ClassDesc = "Asset Ratios", .SubclassDesc = "Asset Ratios", .AnalysisId = 1}
        context.AcctTypes.Add(acctType)

        acctList = New List(Of Account)() From { _
         New Account() With {.AccountId = 80, .AcctDescriptor = "Heading", .SortSequence = 75, .Description = "Asset Ratios", .AcctTypeId = acctType.AcctTypeId, .AnalysisId = 1}, _
         New Account() With {.AccountId = 81, .AcctDescriptor = "Detail", .SortSequence = 76, .Description = "Return On Equity", .AcctTypeId = acctType.AcctTypeId, .AnalysisId = 1}, _
         New Account() With {.AccountId = 82, .AcctDescriptor = "Detail", .SortSequence = 77, .Description = "Return On Assets", .AcctTypeId = acctType.AcctTypeId, .AnalysisId = 1}, _
         New Account() With {.AccountId = 83, .AcctDescriptor = "Detail", .SortSequence = 78, .Description = "Fixed Asset Turnover", .AcctTypeId = acctType.AcctTypeId, .AnalysisId = 1}, _
         New Account() With {.AccountId = 84, .AcctDescriptor = "Total", .SortSequence = 79, .Description = "Total Asset Ratios", .AcctTypeId = acctType.AcctTypeId, .AnalysisId = 1}
       }
        acctList.ForEach(Function(u) context.Accounts.Add(u))

        For Each acctItem As Account In acctList
            Select Case acctItem.Description
                Case "Employee compensation"
                    Dim balList As New List(Of Balance)() From { _
                        New Balance() With {.BalanceId = IncrementBalanceIndex(balanceIndex), .H201 = 3400, .H202 = 3400, .H203 = 3400, .H204 = 3400, _
                    .H205 = 3400, .H206 = 3400, .H207 = 5100, .H208 = 6800, .H209 = 6800, .H210 = 6800, .H211 = 8900, _
                    .H212 = 8900, .AccountId = acctItem.AccountId, .AnalysisId = 1}, _
                         New Balance() With {.BalanceId = IncrementBalanceIndex(balanceIndex), .H101 = 12800, .H102 = 14900, .H103 = 8900, .H104 = 8900, _
                    .H105 = 19400, .H106 = 11900, .H107 = 13408, .H108 = 10600, .H109 = 10600, .H110 = 10600, .H111 = 10600, _
                    .H112 = 10600, .AccountId = acctItem.AccountId, .AnalysisId = 1}, _
                        New Balance() With {.BalanceId = IncrementBalanceIndex(balanceIndex), .A101 = 10600, .A102 = 8500, .A103 = 8500, .A104 = 8500, _
                    .A105 = 9500, .A106 = 12495, .A107 = 12495, .A108 = 12492, .A109 = 10492, .A110 = 10492, .A111 = 10492, _
                    .A112 = 0, .AccountId = acctItem.AccountId, .AnalysisId = 1}, _
                        New Balance() With {.BalanceId = IncrementBalanceIndex(balanceIndex), .B101 = 8500, .B102 = 8500, .B103 = 8500, .B104 = 5100, _
                    .B105 = 10500, .B106 = 10500, .B107 = 10500, .B108 = 10500, .B109 = 13500, .B110 = 13500, .B111 = 13500, _
                    .B112 = 13500, .AccountId = acctItem.AccountId, .AnalysisId = 1}
                    }
                    balList.ForEach(Function(u) context.Balances.Add(u))
                Case "Home office expense"
                    Dim balList As New List(Of Balance)() From { _
                        New Balance() With {.BalanceId = IncrementBalanceIndex(balanceIndex), .H201 = 0, .H202 = 0, .H203 = 0, .H204 = 0, _
                    .H205 = 0, .H206 = 0, .H207 = 0, .H208 = 0, .H209 = 0, .H210 = 0, .H211 = 0, _
                    .H212 = 0, .AccountId = acctItem.AccountId, .AnalysisId = 1}, _
                         New Balance() With {.BalanceId = IncrementBalanceIndex(balanceIndex), .H101 = 0, .H102 = 0, .H103 = 0, .H104 = 0, _
                    .H105 = 0, .H106 = 2000, .H107 = 1500, .H108 = 4500, .H109 = 0, .H110 = 3000, .H111 = 3000, _
                    .H112 = 0, .AccountId = acctItem.AccountId, .AnalysisId = 1}, _
                        New Balance() With {.BalanceId = IncrementBalanceIndex(balanceIndex), .A101 = 2500, .A102 = 2500, .A103 = 0, .A104 = 2500, _
                    .A105 = 2500, .A106 = 2500, .A107 = 2500, .A108 = 2500, .A109 = 2500, .A110 = 2500, .A111 = 2500, _
                    .A112 = 2500, .AccountId = acctItem.AccountId, .AnalysisId = 1}, _
                        New Balance() With {.BalanceId = IncrementBalanceIndex(balanceIndex), .B101 = 2500, .B102 = 2500, .B103 = 2500, .B104 = 2500, _
                    .B105 = 2500, .B106 = 2500, .B107 = 2500, .B108 = 2500, .B109 = 2500, .B110 = 2500, .B111 = 2500, _
                    .B112 = 2500, .AccountId = acctItem.AccountId, .AnalysisId = 1}
                    }
                    balList.ForEach(Function(u) context.Balances.Add(u))
                Case "Health insurance"
                    Dim balList As New List(Of Balance)() From { _
                        New Balance() With {.BalanceId = IncrementBalanceIndex(balanceIndex), .H201 = 533, .H202 = 533, .H203 = 533, .H204 = 533, _
                    .H205 = 533, .H206 = 533, .H207 = 533, .H208 = 533, .H209 = 533, .H210 = 533, .H211 = 585, _
                    .H212 = 644, .AccountId = acctItem.AccountId, .AnalysisId = 1}, _
                         New Balance() With {.BalanceId = IncrementBalanceIndex(balanceIndex), .H101 = 644, .H102 = 644, .H103 = 644, .H104 = 1094, _
                    .H105 = 644, .H106 = 982, .H107 = 2139, .H108 = 1290, .H109 = 1290, .H110 = 1290, .H111 = 1290, _
                    .H112 = 1290, .AccountId = acctItem.AccountId, .AnalysisId = 1}, _
                        New Balance() With {.BalanceId = IncrementBalanceIndex(balanceIndex), .A101 = 1275, .A102 = 854, .A103 = 1084, .A104 = 1065, _
                    .A105 = 1164, .A106 = 1164, .A107 = 1206, .A108 = 1775, .A109 = 1776, .A110 = 1783, .A111 = 756, _
                    .A112 = 0, .AccountId = acctItem.AccountId, .AnalysisId = 1}, _
                        New Balance() With {.BalanceId = IncrementBalanceIndex(balanceIndex), .B101 = 1100, .B102 = 1100, .B103 = 1100, .B104 = 1100, _
                    .B105 = 1100, .B106 = 1100, .B107 = 1100, .B108 = 1100, .B109 = 1100, .B110 = 1100, .B111 = 1100, _
                    .B112 = 1100, .AccountId = acctItem.AccountId, .AnalysisId = 1}
                    }
                    balList.ForEach(Function(u) context.Balances.Add(u))
                Case "Total support expenses"
                    Dim balList As New List(Of Balance)() From { _
                        New Balance() With {.BalanceId = IncrementBalanceIndex(balanceIndex), .H201 = 0, .H202 = 0, .H203 = 0, .H204 = 0, _
                    .H205 = 0, .H206 = 0, .H207 = 0, .H208 = 0, .H209 = 0, .H210 = 0, .H211 = 0, _
                    .H212 = 0, .AccountId = acctItem.AccountId, .AnalysisId = 1}, _
                         New Balance() With {.BalanceId = IncrementBalanceIndex(balanceIndex), .H101 = 0, .H102 = 0, .H103 = 0, .H104 = 0, _
                    .H105 = 0, .H106 = 0, .H107 = 0, .H108 = 0, .H109 = 0, .H110 = 0, .H111 = 0, _
                    .H112 = 0, .AccountId = acctItem.AccountId, .AnalysisId = 1}, _
                        New Balance() With {.BalanceId = IncrementBalanceIndex(balanceIndex), .A101 = 0, .A102 = 0, .A103 = 0, .A104 = 0, _
                    .A105 = 0, .A106 = 0, .A107 = 0, .A108 = 0, .A109 = 0, .A110 = 0, .A111 = 0, _
                    .A112 = 0, .AccountId = acctItem.AccountId, .AnalysisId = 1}, _
                        New Balance() With {.BalanceId = IncrementBalanceIndex(balanceIndex), .B101 = 0, .B102 = 0, .B103 = 0, .B104 = 0, _
                    .B105 = 0, .B106 = 0, .B107 = 0, .B108 = 0, .B109 = 0, .B110 = 0, .B111 = 0, _
                    .B112 = 0, .AccountId = acctItem.AccountId, .AnalysisId = 1}
                    }
                    balList.ForEach(Function(u) context.Balances.Add(u))
                Case "Life insurance"
                    Dim balList As New List(Of Balance)() From { _
                        New Balance() With {.BalanceId = IncrementBalanceIndex(balanceIndex), .H201 = 0, .H202 = 0, .H203 = 0, .H204 = 0, _
                    .H205 = 0, .H206 = 0, .H207 = 0, .H208 = 0, .H209 = 0, .H210 = 0, .H211 = 0, _
                    .H212 = 0, .AccountId = acctItem.AccountId, .AnalysisId = 1}, _
                         New Balance() With {.BalanceId = IncrementBalanceIndex(balanceIndex), .H101 = 0, .H102 = 0, .H103 = 0, .H104 = 0, _
                    .H105 = 0, .H106 = 0, .H107 = 0, .H108 = 0, .H109 = 0, .H110 = 0, .H111 = 0, _
                    .H112 = 0, .AccountId = acctItem.AccountId, .AnalysisId = 1}, _
                        New Balance() With {.BalanceId = IncrementBalanceIndex(balanceIndex), .A101 = 0, .A102 = 0, .A103 = 0, .A104 = 0, _
                    .A105 = 0, .A106 = 0, .A107 = 0, .A108 = 0, .A109 = 0, .A110 = 0, .A111 = 0, _
                    .A112 = 0, .AccountId = acctItem.AccountId, .AnalysisId = 1}, _
                        New Balance() With {.BalanceId = IncrementBalanceIndex(balanceIndex), .B101 = 0, .B102 = 0, .B103 = 0, .B104 = 0, _
                    .B105 = 0, .B106 = 0, .B107 = 0, .B108 = 0, .B109 = 0, .B110 = 0, .B111 = 0, _
                    .B112 = 0, .AccountId = acctItem.AccountId, .AnalysisId = 1}
                    }
                    balList.ForEach(Function(u) context.Balances.Add(u))

            End Select
        Next

        Dim acctTypeList As New List(Of AcctType)() From { _
        New AcctType() With {.AcctTypeId = 12, .TypeDesc = "Expense", .ClassDesc = "Other Income (Expense)", .SubclassDesc = "Other Income (Expense)", .AnalysisId = 1}, _
        New AcctType() With {.AcctTypeId = 13, .TypeDesc = "Expense", .ClassDesc = "Provision for Taxes", .SubclassDesc = "Provision for Taxes", .AnalysisId = 1}, _
        New AcctType() With {.AcctTypeId = 14, .TypeDesc = "Expense", .ClassDesc = "Property and Equipment", .SubclassDesc = "Property and Equipment", .AnalysisId = 1}, _
        New AcctType() With {.AcctTypeId = 15, .TypeDesc = "Expense", .ClassDesc = "Other Assets", .SubclassDesc = "Other Assets", .AnalysisId = 1}, _
        New AcctType() With {.AcctTypeId = 16, .TypeDesc = "Liabilities", .ClassDesc = "Current Liabilities", .SubclassDesc = "Current Liabilities", .AnalysisId = 1}, _
        New AcctType() With {.AcctTypeId = 17, .TypeDesc = "Liabilities", .ClassDesc = "Long Term Liabilities", .SubclassDesc = "Long Term Liabilities", .AnalysisId = 1}, _
        New AcctType() With {.AcctTypeId = 18, .TypeDesc = "Equity", .ClassDesc = "Stockholders Equity", .SubclassDesc = "Stockholders Equity", .AnalysisId = 1} _
       }
        acctTypeList.ForEach(Function(u) context.AcctTypes.Add(u))

        MyBase.Seed(context)


    End Sub
End Class
