﻿@ModelType onlineAnalytics.Customer
@Code
    ViewData("Title") = "PlanGuru Analytics | Signup Confirmation"
    Layout = "~/Views/Shared/_Layout.vbhtml"    
End Code

<header>    
    <link rel="stylesheet" href="@Url.Content("~/themes/default/recurly.css")" type="text/css" />
    <link rel="stylesheet" href="@Url.Content("~/themes/default/examples.css")" type="text/css" />
</header>

<div id="billing-info" class="section">
    <div id="recurly-subscribe" >
        <h2 class="heading">
            Signup Confirmation</h2>
        <div style="width:90%;margin:0px 40px;"><br />
            <p>Thank you for your PlanGuru Analytics signup.  Shortly you will receive a confirmation email.  In addition, you will receive an email with your temporary password.  Use the link in the email and the temporary password to activate your user account.</p><br />  
            <p>Once you have activated your account you can log into PlanGuru Analytics.  Your next steps will be to:</p><br />
            <ul>
             <li>Add any additional users you want to be able to access PlanGuru Analytics.  Once your trial period expires, you will be charged $@Model.SubscriptionAmount  per month for each user.</li>
             <li>Add companies and analyses.  You can add as many companies as you want and within each company you can have as many analyses (departments, divisions, etc) as you want at no additional charge. </li>
             <li>Once you’ve added at least one company and analysis you’ll be able to upload data from your PlanGuru to PlanGuru Analytics.  To upload data you must be using PlanGuru 2013 Version 3.0.0.4 or later.</li> 
            </ul>
            @*<p>So let’s get started:  @Html.ActionLink("Add a new company", "Create","Company")</p>*@<br />
        </div>       
    </div>
     
    <div style="float: left; margin-left: 20px;">
    @If Not (DirectCast(TempData("Message"), String) Is Nothing) Then
            @<label class="success">@TempData("Message").ToString()
            </label>                         
    End If
        @If Not (DirectCast(TempData("ErrorMessage"), String) Is Nothing) Then
            @<label class="error">@TempData("ErrorMessage").ToString()
            </label>                         
    End If
    </div>
</div>