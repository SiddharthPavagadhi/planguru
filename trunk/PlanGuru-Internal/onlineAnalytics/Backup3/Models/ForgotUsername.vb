﻿Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel
Public Class ForgotUsername

    <Required(ErrorMessage:="Email address is required.")>
    <StringLength(50)>
    <RegularExpression("^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$", ErrorMessage:="Not a valid email adress.")>
     <Display(Name:="Email address")>
    Public Property emailAddress() As String
End Class

Public Class ForgotPassword

    <Required(ErrorMessage:="Username is required.")> _
   <StringLength(15)> _  
   <Display(Name:="Username")> _
    Public Property userName As String

    <Required(ErrorMessage:="Email address is required.")>
    <StringLength(50)>
    <RegularExpression("^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$", ErrorMessage:="Not a valid email adress.")>
    <Display(Name:="Email address")>
    Public Property emailAddress() As String
End Class