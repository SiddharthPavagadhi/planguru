﻿Imports System.ComponentModel.DataAnnotations
Imports System.Data.Entity

<Table("SubscriptionTransactionRequestResponse")> _
Public Class SubscriptionTransactionRequestResponse
    <Key()> _
  <DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)> _
    Public Property SubscriptionRequestResponseId() As Integer
    Public Property Signature() As String
    Public Property PlanCode() As String
    Public Property AccountCode() As String
    Public Property TimeStamp() As String
    Public Property Nonce() As String
    Public Property UUID() As String
    Public Property State() As String
    Public Property Result() As String

    Public Property CratedBy() As String

    Public Property CratedOn As DateTime = DateTime.Now()

    Public Property UpdatedBy() As String

    Public Property UpdatedOn As DateTime = DateTime.Now()


End Class





