﻿@Imports Farpoint.Web.Spread
@Imports Farpoint.Mvc.Spread
@Imports  onlineAnalytics
@Code    
    ViewData("Title") = "PlanGuru Analytics | Revenue and Expenses Saved Report"
    If Not Session("action") Is Nothing Then
        
        If (Session("action") = "SetDefaultAnalysis" Or Session("action") = "GetAnalyses") Then
            Layout = Nothing
            Session.Remove("action")
        End If
    Else
        Layout = "~/Views/Shared/AnalyticsMaster.vbhtml"
    End If
    
    Dim UserInfo As User = Nothing
    Dim ua As New UserAccess
    
    Dim objSetup As New SetupCount
    
    If Not (Session("UserInfo") Is Nothing) Then
        UserInfo = DirectCast(Session("UserInfo"), User)
    End If
    
    If (Not ViewBag.Setup Is Nothing) Then
        objSetup = DirectCast(ViewBag.Setup, SetupCount)
    End If
End Code
<script type="text/javascript">
    window.ViewerControlId = "#REView";   
</script>
<div style="display: inline-block; width: 100%;">
    @If Not (DirectCast(TempData("Message"), String) Is Nothing) Then
        @<label class="success">@TempData("Message").ToString()
        </label>                         
    End If
    @If Not (DirectCast(TempData("ErrorMessage"), String) Is Nothing) Then
        @<label class="error" style="margin-top: 0px;">@TempData("ErrorMessage").ToString()
        </label>                         
    End If
</div>
<div id="outerwrapper" style="display:none">
    @code   
        @Html.Hidden("countOfDataUploaded", objSetup.countDataUploadedOfSelectedAnalysis)        
        @<label id="validationMessage" class="info" style="display: none;">
            @If ((Not IsNothing(ViewBag.Setup)) AndAlso objSetup.AanlysisCount > 0 And objSetup.selectedAnalysis = 0) Then
                @MvcHtmlString.Create("Please select company & one of the respective analysis from top-menu selection, to get the dashboard view.")
            ElseIf ((Not IsNothing(ViewBag.Setup)) AndAlso objSetup.selectedAnalysis > 0 And objSetup.countDataUploadedOfSelectedAnalysis = 0) Then
                @MvcHtmlString.Create("Data has not been uploaded for selected analysis.")
            End If
        </label> 
        
        If (Not UserInfo Is Nothing AndAlso Not IsNothing(ViewBag.Setup)) Then
                   
            If (UserInfo.UserRoleId <= UserRoles.SAU) Then
                    
                If (objSetup.CompanyCount = 0 Or objSetup.AanlysisCount = 0) Then
        @<label class="info">You have to create below listed action item, to get the dashboard view.</label> 
                End If
                If (objSetup.CompanyCount = 0) Then
        @<div class="secondbutton_example" style='margin: 20px; width: 200px;'>
            <a href='@Url.Action("Create", "Company")' style="text-decoration:none;color: #FFFFFF;">
                Add Company</a>
        </div>  
                End If
                If (objSetup.CompanyCount > 0 And objSetup.AanlysisCount = 0) Then
        @<div class="secondbutton_example" style='margin: 20px; width: 200px;'>
            <a href='@Url.Action("Create", "Analysis")'  style="text-decoration:none;color: #FFFFFF;">
                Add Analysis</a>
        </div>  
                End If
                If (objSetup.CompanyCount = 0 Or objSetup.AanlysisCount = 0) Then
        @<div class="secondbutton_example" style='margin: 20px; width: 200px;'>
            <a href='@Url.Action("CreateUser", "User")'  style="text-decoration:none;color: #FFFFFF;">
                Add User</a>
        </div>  
                End If
            ElseIf (UserInfo.UserRoleId > UserRoles.SAU) Then
                
                If (objSetup.CompanyCount = 0) Then
        @<label class="info">You have not been given access to any company. Please contact your
            administrator. / analysis.</label>
                ElseIf (objSetup.AanlysisCount = 0) Then
        @<label class="info">You have not been given access to any analysis files for the selected
            company. Please contact your administrator.</label> 
                End If
                
            End If
        End If
        
        If (objSetup.CompanyCount > 0 And objSetup.AanlysisCount > 0 And objSetup.selectedAnalysis > 0 And objSetup.countDataUploadedOfSelectedAnalysis > 0) Then
            Using Html.BeginForm
        @<div id="REView" style="display: none; width: 100%;">
            <div id="sidebar">
                <div>
                    <img src='@Url.Content("~/Content/images/period.png")' alt="Period" />
                </div>
                <div>
                    @Html.DropDownList("intPeriod", DirectCast(ViewData("Periods"), SelectList), New With {.class = "REselect"})
                </div>
                <br/>
                 <div id="optionsimage">
                    <img src='@Url.Content("~/Content/images/options.png")' alt="Options" />
                </div>                 
                <div id="chkHighlightVar" class="indent0" style="margin-top: 5px;">
                    @Html.CheckBox("blnHighVar", ViewData("blnHighVar")) Highlight variances
                </div>
                <div id="HighlightVarControls" style="display: none;">
                    <div id="chkHighlightPosVar" class="indent10">
                        @Html.CheckBox("blnHighPosVar", ViewData("blnPosVar")) + Variances > than:
                        @Html.TextBox("intHighPosVar", ViewData("intPosVarAmt"), New With {.Style = "width: 12px;  margin-left: 5px; font-size: 10px; border: 1px solid #DDD;text-align: right; "})%
                        <span id="valintHighPosVar" style="color: red; text-align: right; float: right; width: 165px;
                            margin-right: 10px; display: none;">Numeric characters only.</span>
                    </div>
                    <div id="chkHighlightNegVar" class="indent10">
                        @Html.CheckBox("blnHighNegVar", ViewData("blnNegVar")) - Variances > than: -
                        @Html.TextBox("intHighNegVar", ViewData("intNegVarAmt"), New With {.Style = "width: 12px; font-size: 10px; border: 1px solid #DDD; text-align: right;"})%
                        <span id="valintHighNegVar" style="color: red; text-align: right; float: right; width: 165px;
                            margin-right: 10px; display: none;">Numeric characters only.</span>
                    </div>
                </div>
                <div id="CheckBoxFilter" class="indent0">
                    @Html.CheckBox("blnUseFilter", ViewData("blnFilterOnSelected")) Apply Filter
                </div>
                <div id="UseFilterControls"  style="display: none;" >
                    <div class="indent10">
                        @Html.DropDownList("intFilterOn", DirectCast(ViewData("FilterOn"), SelectList), New With {.class = "REselect"})
                    </div>
                    <div>
                        <label class="indent10">
                            @Html.RadioButton("blnFilterPosVariance", ViewData("blnFilterPosVar"), New With {.id = "radioPosVar", .checked = "checked", .style = "width: 20px;"})
                            Amounts > than:
                        </label>
                    </div>
                    <div>
                        <label class="indent10">
                            @Html.RadioButton("blnFilterPosVariance", ViewData("blnFilterNegVar"), New With {.id = "radioNegVar", .style = "width: 20px;"})
                            Amounts < than:
                        </label>
                    </div>
                    <div class="indent30">
                        @Html.TextBox("decFilterAmt", ViewData("intFilterAmt"), New With {.Style = "width: 50px; margin-left: 5px; font-size: 10px; border: 1px solid #DDD;text-align: right; "})
                        <span id="valDecFilterAmt" style="color: red; text-align: right; float: right; width: 165px;
                            margin-right: 10px; display: none;">Numeric characters only.</span>
                    </div>
                </div>                
                <br />
                <div id="UpdateChart" style="display: none; margin: 15px 0 0 0; float: left;">
                    <input id="causepost" type="submit" value="View Chart" class="secondbutton_example" />
                </div> 
                <div>
                    <h2 class="savedViewsListHeading">Saved Reports</h2>
                    <ul id="SavedReportsList" class="savedViewsList">
                    </ul>
                </div>                
            </div>
            <div id="main" style="display: inline-block; width: 79%;">
                <div id="chart" style="display: none;">
                    @Html.FpSpread("spdChart", Sub(x)
                                                   x.RowHeader.Visible = False
                                                   x.ColumnHeader.Visible = False
                                                   x.Height = 200
                                                   x.Width = 500
                                           
                                               End Sub)
                </div>
                <div id="spread" style="display: none;">
                    @Html.FpSpread("spdAnalytics", Sub(x)
                                                   
                                                       x.RowHeader.Visible = False
                                                       x.ActiveSheetView.PageSize = 1000
                                                   End Sub)
                </div>
            </div>
        </div>   
            End Using
        End If
       
    End Code
</div>
<script type="text/javascript">
        function setSelectedPageNav() {
            var pathName = document.location.pathname;
            if ($("#SavedReportsList li a") != null) {
                var currentLink = $("#SavedReportsList li a[href='" + pathName + "']");
                currentLink.addClass("activelink");
            }
        }
           
//        $('#intPeriod').change(function () {            
//            $('#causepost').trigger('click')
//        });

        //on load
        $(document).ready(function () {                            
            IsSessionAlive();            
            getSavedReportsList();            
            $("input:checkbox").each(function (index) {
                $("<label>").attr("for", this.id)
                .attr("class", "styled_checkbox dashboard")
                .insertAfter(this);
            });

            var selectedindex = @ViewData("intChartType");   
            
            var accID = '@ViewData("AccountID")';
            var splitID = accID.split(" ");
            
            var postback = '@ViewData("Postback")';
            if(postback == "True") $('#outerwrapper').show();
            if(postback == "True" && accID != "" && selectedindex > 1)
            {                
                $('#chart').show()
                $('#chart').css('height', '200px');
            }

            if (selectedindex > 1) {                               
                $('#sidebar').css('height', '800px');                
                var spread = document.getElementById("spdAnalytics");
                var colcount = spread.GetColCount();
                spread.SetColWidth(2, 22);
                if (colcount < 10) {
                    spread.SetColWidth(4, 250);
                }
                else if (colcount > 13) {
                    spread.SetColWidth(4, 150);
                }
                else {
                    spread.SetColWidth(4, 200);
                }
            }
            else {
                $('#sidebar').css('height', '600px')
            }
            $('#spread').show()  

              //hide detail rows  
            if (document.getElementById("spdAnalytics")) {
                var spread = document.getElementById("spdAnalytics");
                var rc = spread.GetRowCount();
                var showclass = false
                var showsubtotal = false
                for (var i = 0; i < rc; i++) {
                    
                    // pre-select the account id's in the viewer  
                    for(var j=0; j < splitID.length; j++){
                        if(splitID[j] == spread.GetValue(i, 5)) {
                            
                            spread.SetValue(i, 2, true, false); 
                        }
                    }
                    

                    var cellval = spread.GetValue(i, 3);
                    if (cellval == "Detail") {
                        if (showclass == false) {
                            spread.Rows(i).style.display = "none";
                        }
                    }
                    if (cellval == "SDetail" || cellval == "SHeading" || cellval == "STotal") {
                        if (showclass == false) {
                            spread.Rows(i).style.display = "none";
                        }
                        else if (showclass == true) {
                            if (cellval == "SHeading") {
                                cellval = spread.GetValue(i, 1);
                                if (cellval == "-") {
                                    showsubtotal = true
                                }
                                else {
                                    showsubtotal = false
                                    spread.Rows(i).style.display = "none";
                                }
                            }
                            else if (cellval == "SDetail") {
                                if (showsubtotal == false) {
                                    spread.Rows(i).style.display = "none";
                                }
                            }
                        }
                    }
                    if (cellval == "Heading") {
                        var cellvalue = spread.GetValue(i, 0);
                        if (cellvalue == "-") {
                            showclass = true
                            spread.Rows(i).style.display = "table-row";
                        }
                        else {
                            showclass = false
                            spread.Rows(i).style.display = "none";
                        }
                    }
                }
            } 
            
//            ew 6/15 code for highlight and filter
            var selectedFormat = '@ViewData("intFormat")';   
            if (selectedFormat > 4) {
                $('#optionsimage').hide()
                $('#chkHighlightVar').hide()
                $('#HighlightVarControls').hide() 
                $('#CheckBoxFilter').hide()
                $('#UseFilterControls').hide()
            }
            else { 
                if ($("#blnHighVar").is(':checked')) { 
                    var HighPosVar = '@ViewData("blnPosVar")' 
                    if (HighPosVar == "True") { 
                        $('#blnHighPosVar').attr('checked', true);
                    }  
                    var HighNegVar = '@ViewData("blnNegVar")' 
                    if (HighNegVar == "True") { 
                        $('#blnHighNegVar').attr('checked', true);
                    }  
                    $('#HighlightVarControls').show() 
                    AddHighlight()
                };
                if ($("#blnUseFilter").is(':checked')) { 
                    $('#UseFilterControls').show()
                    var posvar = '@ViewData("blnFilterPosVar")' 
                    if (posvar == "False") {  
                        $('#radioNegVar').attr('checked', true);
                        $('#radioPosVar').removeAttr('checked');
                    }  
                    AddFilter() 
                }
            }
            
//            ew 6/15 end of code for highlight and filter
            //adjust width
            var browsewidth = $(window).width();
            $('#intBrowserWidth').val(browsewidth)
            var spdWidth = $('#spdAnalytics').width();
            var calcwidth = spdWidth + 250;
            if (calcwidth > 1100) {
                if (calcwidth < browsewidth) {                    
                     $(".header").width('1275');
                    $(".nav-panel").width('1275');
                    $(".main-container").width('1275px');
                }
                else {
                }
            }
          
            if(postback == "False")
            {
                $('#spread').hide();                
                $('#causepost').trigger('click');
            }            
        });

        window.onload = function () {
            if (document.getElementById("spdAnalytics")) {
                var spread = document.getElementById("spdAnalytics");
                if (document.all) {
                    if (spread.addEventListener) {
                        spread.addEventListener("ActiveCellChanged", cellChanged, false);
                    }
                    else {
                        spread.onActiveCellChanged = cellChanged;
                    }
                }
                else {
                    spread.addEventListener("ActiveCellChanged", cellChanged, false);
                }
            }                        
        }

        function cellChanged(event) {
            if (event.col == 0 && document.getElementById("spdAnalytics")) {
                var spread = document.getElementById("spdAnalytics");
                var cellval = spread.GetValue(event.row, 0);
                var showsubtotal = false
                var rc = spread.GetRowCount();
                if (cellval == "-") {
                    for (var i = event.row; i < rc; i++) {
                        var rowtype = spread.GetValue(i, 3);
                        if (rowtype == "Total") {
                            cell = document.getElementById("spdAnalytics").GetCellByRowCol(i, 0);
                            cell.removeAttribute("FpCellType");
                            spread.SetValue(i, 0, "+", false);
                            cell.setAttribute("FpCellType", "readonly");
                            spread.Rows(i).style.display = "table-row";
                            break;
                        }
                        else if (rowtype == "Heading") {
                            cell = document.getElementById("spdAnalytics").GetCellByRowCol(i, 0);
                            cell.removeAttribute("FpCellType");
                            spread.SetValue(i, 0, "+", false);
                            cell.setAttribute("FpCellType", "readonly");
                            spread.Rows(i).style.display = "none";
                        }
                        else {

                            spread.Rows(i).style.display = "none";
                        }
                    }
                }
                else if (cellval == "+") { 
                    for (var i = event.row; i > -1; i--) {
                        var rowtype = spread.GetValue(i, 3);
                        if (rowtype == "Total") {
                            cell = document.getElementById("spdAnalytics").GetCellByRowCol(i, 0);
                            cell.removeAttribute("FpCellType");
                            spread.SetValue(i, 0, "", false);
                            cell.setAttribute("FpCellType", "readonly");
                            if ($('#blnUseFilter').is(':checked')) {
                                spread.Rows(i).style.display = "none";
                            }
                        }
                        else if (rowtype == "Heading") {
                            cell = document.getElementById("spdAnalytics").GetCellByRowCol(i, 0);
                            cell.removeAttribute("FpCellType");
                            spread.SetValue(i, 0, "-", false);
                            cell.setAttribute("FpCellType", "readonly");
                            spread.Rows(i).style.display = "table-row";
                            break
                        }
                        else if (rowtype == "SHeading") {
                            if (showsubtotal == true) {
                                spread.Rows(i).style.display = "table-row";
                            }
                        }
                        else if (rowtype == "SDetail") {
                            if (showsubtotal == true) {
                                spread.Rows(i).style.display = "table-row";
                            }
                        }
                        else if (rowtype == "STotal") {
                            cellval = spread.GetValue(i, 1);
                            if (cellval == "") {
                                showsubtotal = true
                            }
                            if (cellval == "+") {
                                showsubtotal = false
                            }
                            spread.Rows(i).style.display = "table-row";
                        }
//                        else if ($('#blnUseFilter').is(':checked')) {
//                            HideUnFilterRow(i)
//                        }
                        else {
                            spread.Rows(i).style.display = "table-row";
                        }
                    }
                }
                spread.SetActiveCell(event.row, 2)
            }
            if (event.col == 1 && document.getElementById("spdAnalytics")) {
                var spread = document.getElementById("spdAnalytics");
                var cellval = spread.GetValue(event.row, 1);
                var rc = spread.GetRowCount();
                if (cellval == "-") {
                    for (var i = event.row; i < rc; i++) {
                        var rowtype = spread.GetValue(i, 3);
                        if (rowtype == "SHeading") {
                            cell = document.getElementById("spdAnalytics").GetCellByRowCol(i, 1);
                            cell.removeAttribute("FpCellType");
                            spread.SetValue(i, 1, "+", false);
                            cell.setAttribute("FpCellType", "readonly");
                            spread.Rows(i).style.display = "none";
                        }
                        if (rowtype == "STotal") {
                            cell = document.getElementById("spdAnalytics").GetCellByRowCol(i, 1);
                            cell.removeAttribute("FpCellType");
                            spread.SetValue(i, 1, "+", false);
                            cell.setAttribute("FpCellType", "readonly");
                            break
                        }
                        else {
                            spread.Rows(i).style.display = "none";
                        }
                    }
                }
                if (cellval == "+") {
                    for (var i = event.row; i > -1; i--) {
                        var rowtype = spread.GetValue(i, 3);
                        if (rowtype == "STotal") {
                            cell = document.getElementById("spdAnalytics").GetCellByRowCol(i, 1);
                            cell.removeAttribute("FpCellType");
                            spread.SetValue(i, 1, "", false);
                            cell.setAttribute("FpCellType", "readonly");
                        }
                        if (rowtype == "SHeading") {
                            cell = document.getElementById("spdAnalytics").GetCellByRowCol(i, 1);
                            cell.removeAttribute("FpCellType");
                            spread.SetValue(i, 1, "-", false);
                            cell.setAttribute("FpCellType", "readonly");
                            spread.Rows(i).style.display = "table-row";
                            break
                        }
//                        else if ($('#blnUseFilter').is(':checked')) {
//                            HideUnFilterRow(i)
//                        }
                        else {
                            spread.Rows(i).style.display = "table-row";
                        }
                    }
                }

            }
        }


//        function cellChanged(event) {
//            if (event.col == 0 && document.getElementById("spdAnalytics")) {
//                var spread = document.getElementById("spdAnalytics");
//                var cellval = spread.GetValue(event.row, 0);
//                var showsubtotal = false
//                var rc = spread.GetRowCount();
//                if (cellval == "-") {
//                    for (var i = event.row; i < rc; i++) {
//                        var rowtype = spread.GetValue(i, 3);
//                        if (rowtype == "Total") {
//                            cell = document.getElementById("spdAnalytics").GetCellByRowCol(i, 0);
//                            cell.removeAttribute("FpCellType");
//                            spread.SetValue(i, 0, "+", false);
//                            cell.setAttribute("FpCellType", "readonly");
//                            spread.Rows(i).style.display = "table-row";
//                            break;
//                        }
//                        else if (rowtype == "Heading") {
//                            cell = document.getElementById("spdAnalytics").GetCellByRowCol(i, 0);
//                            cell.removeAttribute("FpCellType");
//                            spread.SetValue(i, 0, "+", false);
//                            cell.setAttribute("FpCellType", "readonly");
//                            spread.Rows(i).style.display = "none";
//                        }
//                        else {

//                            spread.Rows(i).style.display = "none";
//                        }
//                    }
//                }
//                else if (cellval == "+") {
//                    for (var i = event.row; i > -1; i--) {
//                        var rowtype = spread.GetValue(i, 3);
//                        if (rowtype == "Total") {
//                            cell = document.getElementById("spdAnalytics").GetCellByRowCol(i, 0);
//                            cell.removeAttribute("FpCellType");
//                            spread.SetValue(i, 0, "", false);
//                            cell.setAttribute("FpCellType", "readonly");
//                            if ($('#blnUseFilter').is(':checked')) {
//                                spread.Rows(i).style.display = "none";
//                            }
//                        }
//                        else if (rowtype == "Heading") {
//                            cell = document.getElementById("spdAnalytics").GetCellByRowCol(i, 0);
//                            cell.removeAttribute("FpCellType");
//                            spread.SetValue(i, 0, "-", false);
//                            cell.setAttribute("FpCellType", "readonly");
//                            spread.Rows(i).style.display = "table-row";
//                            break
//                        }
//                        else if (rowtype == "SHeading") {
//                            if (showsubtotal == true) {
//                                spread.Rows(i).style.display = "table-row";
//                            }
//                        }
//                        else if (rowtype == "SDetail") {
//                            if (showsubtotal == true) {
//                                spread.Rows(i).style.display = "table-row";
//                            }
//                        }
//                        else if (rowtype == "STotal") {
//                            cellval = spread.GetValue(i, 1);
//                            if (cellval == "") {
//                                showsubtotal = true
//                            }
//                            if (cellval == "+") {
//                                showsubtotal = false
//                            }
//                            spread.Rows(i).style.display = "table-row";
//                        }
//                        else {
//                            spread.Rows(i).style.display = "table-row";
//                        }
//                    }
//                }
//                spread.SetActiveCell(event.row, 2)
//            }
//            if (event.col == 1 && document.getElementById("spdAnalytics")) {
//                var spread = document.getElementById("spdAnalytics");
//                var cellval = spread.GetValue(event.row, 1);
//                var rc = spread.GetRowCount();
//                if (cellval == "-") {
//                    for (var i = event.row; i < rc; i++) {
//                        var rowtype = spread.GetValue(i, 3);
//                        if (rowtype == "SHeading") {
//                            cell = document.getElementById("spdAnalytics").GetCellByRowCol(i, 1);
//                            cell.removeAttribute("FpCellType");
//                            spread.SetValue(i, 1, "+", false);
//                            cell.setAttribute("FpCellType", "readonly");
//                            spread.Rows(i).style.display = "none";
//                        }
//                        if (rowtype == "STotal") {
//                            cell = document.getElementById("spdAnalytics").GetCellByRowCol(i, 1);
//                            cell.removeAttribute("FpCellType");
//                            spread.SetValue(i, 1, "+", false);
//                            cell.setAttribute("FpCellType", "readonly");
//                            break
//                        }
//                        else {
//                            spread.Rows(i).style.display = "none";
//                        }
//                    }
//                }
//                if (cellval == "+") {
//                    for (var i = event.row; i > -1; i--) {
//                        var rowtype = spread.GetValue(i, 3);
//                        if (rowtype == "STotal") {
//                            cell = document.getElementById("spdAnalytics").GetCellByRowCol(i, 1);
//                            cell.removeAttribute("FpCellType");
//                            spread.SetValue(i, 1, "", false);
//                            cell.setAttribute("FpCellType", "readonly");
//                        }
//                        if (rowtype == "SHeading") {
//                            cell = document.getElementById("spdAnalytics").GetCellByRowCol(i, 1);
//                            cell.removeAttribute("FpCellType");
//                            spread.SetValue(i, 1, "-", false);
//                            cell.setAttribute("FpCellType", "readonly");
//                            spread.Rows(i).style.display = "table-row";
//                            break
//                        }
//                        else {
//                            spread.Rows(i).style.display = "table-row";
//                        }
//                    }
//                }

//            }
//        }
//        ew 6/16 code for highlight variance and filter
        $('#blnHighVar').change(function () {  
            if ($(this).is(':checked')) {
                $('#HighlightVarControls').show()
                 if ($("#blnUseFilter").is(':checked')) {
                    $('#UseFilterControls').hide()
                    $('#blnUseFilter').removeAttr('checked');
                    RemoveFilter()
                };    
                AddHighlight()
            }
            else {
                $('#HighlightVarControls').hide()
                RemoveBackgroundColor()
            }
        });

        $('#blnHighPosVar').change(function () {
            AddHighlight()
        })

        $('#blnHighNegVar').change(function () {
            AddHighlight()
        })

        $('#intHighPosVar').change(function () { 
            AddHighlight()
        })

        $('#intHighNegVar').change(function () {
            AddHighlight()
        })

        function AddHighlight() { 
            if (document.getElementById("spdAnalytics")) {
                var spread = document.getElementById("spdAnalytics");
                var selindex = '@ViewData("intFormat")'; 
                if ($("#blnHighPosVar").is(':checked')) {
                    var highposvar = "checked"
                };
                if ($("#blnHighNegVar").is(':checked')) {
                    var highnegvar = "checked"
                };
                var posvaramt = parseFloat($("#intHighPosVar").val());
                var negvaramt = parseFloat($("#intHighNegVar").val());
                negvaramt = -negvaramt;
                var rc = spread.GetRowCount();
                for (var i = 0; i < rc; i++) {
                    var rowtype = spread.GetValue(i, 3);
                    if (rowtype == "Detail" || rowtype == "SDetail" || rowtype == "STotal") {
                        var value = parseFloat(spread.GetValue(i, 9).replace(',', '')); 
                        if (value > posvaramt) {
                            if (highposvar == "checked") {
                                SetBackGroundColor(i, "Green","current")
                            }
                            else {
                                SetBackGroundColor(i, "Black", "current")
                            }
                        }
                        else if (value < negvaramt) {
                            if (highnegvar == "checked") {
                                SetBackGroundColor(i, "Red", "current")
                            }
                            else {
                                SetBackGroundColor(i, "Black", "current")
                            }
                        }
                        else {

                            SetBackGroundColor(i, "Black", "current")
                        }
                        if (selindex == 0 || selindex == 2) {
                            var ytdvalue = parseFloat(spread.GetValue(i, 13).replace(',', ''));
                            if (ytdvalue > posvaramt) {
                                if (highposvar == "checked") {
                                    SetBackGroundColor(i, "Green", "ytd")
                                }
                                else {
                                    SetBackGroundColor(i, "Black", "ytd")
                                }
                            }
                            else if (ytdvalue < negvaramt) {
                                if (highnegvar == "checked") {
                                    SetBackGroundColor(i, "Red", "ytd")
                                }
                                else {
                                    SetBackGroundColor(i, "Black", "ytd")
                                }
                            }
                            else {

                                SetBackGroundColor(i, "Black", "ytd")
                            }
                        } 
                    }
                }
            }
        }

        function AddBackgroundColor() {
            if (document.getElementById("spdAnalytics")) {
                var spread = document.getElementById("spdAnalytics");
                var rc = spread.GetRowCount();
                var tf = false
                for (var i = 0; i < rc; i++) {
                    if (tf == false) {
                        SetBackGroundColor(i, "#fef5do", "current")
                        spread.SetSelectedRange(i, 3, i, 8)
                        tf = true
                    }
                    else {
                        SetBackGroundColor(i, "White", "current")
                        spread.SetSelectedRange(i, 3, i, 8)
                        tf = false
                    }
                }
            }
        }

        function RemoveBackgroundColor() {
            if (document.getElementById("spdAnalytics")) {
                var spread = document.getElementById("spdAnalytics");
                var rc = spread.GetRowCount();
                var tf = false
                for (var i = 0; i < rc; i++) {
                    SetBackGroundColor(i, "Black", "current")
                    SetBackGroundColor(i, "Black", "ytd")
                }
            }
        }

        function SetBackGroundColor(row, color, period) {
            
            if (document.getElementById("spdAnalytics")) {
                if (period == "current") {
                    var startcol = 6
                    var endcol = 10
                }
                if (period == "ytd") {
                    var startcol = 10
                    var endcol = 14
                }
                var spread = document.getElementById("spdAnalytics");
                var colcount = spread.GetColCount();
                for (var i = startcol; i < endcol; i++) {                   
                    spread.Cells(row, i).style.color = color
                }
            }
        }

        $('#blnUseFilter').change(function () {
            if ($(this).is(':checked')) {
                $('#UseFilterControls').show()
                $('#HighlightVarControls').hide()
                $('#blnHighVar').removeAttr('checked')
                RemoveBackgroundColor()
                AddFilter()
            }
            else {
                $('#UseFilterControls').hide()
                RemoveFilter()
            }
        });

        $('#intFilterOn').change(function () {
            AddFilter()
        })

        $('#radioPosVar').change(function () {
            $('#radioPosVar').attr('checked', true);
            AddFilter()
        })

        $('#radioNegVar').change(function () {
            $('#radioPosVar').removeAttr('checked')
            AddFilter()
        })

        $('#decFilterAmt').change(function () {
            AddFilter()
        })

        function AddFilter() { 
            if (document.getElementById("spdAnalytics")) {
                var spread = document.getElementById("spdAnalytics");
                var selindex = $('#intFilterOn').get(0).selectedIndex;
                var filteramt = $('#decFilterAmt').val();
                 var posvar = "unchecked"
                if ($("#radioPosVar").is(':checked')) {
                    posvar = "checked"
                }; 
                var rc = spread.GetRowCount();
                var amount = parseFloat(filteramt);
                var showclass = false
                var showsubclass = false
                var coladj = selindex
                //                spread.SetColWidth(0, 0);
                if (selindex > 2) {
                    coladj = 1 + selindex
                }
                for (var i = 0; i < rc; i++) {
                    var rowtype = spread.GetValue(i, 3);

                    if (rowtype == "Detail" || rowtype == "SDetail") {
                        //                      
                        var value = parseInt(spread.GetValue(i, coladj + 7).replace(',', '', 'g'));
                        if (value < filteramt) {
                            if (posvar == "checked") {
                                spread.Rows(i).style.display = "none";
                            }
                            else {
                                spread.Rows(i).style.display = "table-row";
                            }
                        }
                        else if (value > filteramt) {
                            if (posvar == "checked") {
                                spread.Rows(i).style.display = "table-row";
                            }
                            else {
                                spread.Rows(i).style.display = "none";
                            }                            
                        }
                    }
                    else if (rowtype == "Heading") {                        
                        spread.Rows(i).style.display = "table-row";
                        cell = document.getElementById("spdAnalytics").GetCellByRowCol(i, 0);
                        cell.removeAttribute("FpCellType");
                        spread.Cells(i, 0).style.color = "white"
                        var cellvalue = spread.GetValue(i, 0); 
                        if (cellvalue == "-") {
                            spread.SetValue(i, 0, "o", false);
                        }
                        else {
                            spread.SetValue(i, 0, "x", false);

                        }
                        cell.setAttribute("FpCellType", "readonly");
                    }
                    else if (rowtype == "Total") {
                        spread.Rows(i).style.display = "none";
                    }
                    else {
                        spread.Rows(i).style.display = "none";
                    }
                } 
            }
        }

        function RemoveFilter() {
            if (document.getElementById("spdAnalytics")) {
                var spread = document.getElementById("spdAnalytics");
                var rc = spread.GetRowCount(); 
                var cellvalue = ""
                var headingvalue = ""
                var showclass = false
                var showsubtotal = false
                for (var i = 0; i < rc; i++) {
                    var rowtype = spread.GetValue(i, 3);
                    if (rowtype == "Detail" || rowtype == "SDetail" || rowtype == "STotal") {
                        if (showclass == false) {
                            spread.Rows(i).style.display = "none";
                        }
                        else {
                            if (rowtype == "SDetail") {
                                
                                if (showsubtotal == true) {
                                    spread.Rows(i).style.display = "table-row";
                                }
                                else {
                                    spread.Rows(i).style.display = "none";
                                }
                            }
                            else {
                                spread.Rows(i).style.display = "table-row";
                            }
                        }
                    }
                    else if (rowtype == "Heading") {                                                
                        cell = document.getElementById("spdAnalytics").GetCellByRowCol(i, 0);
                        cell.removeAttribute("FpCellType");
                        spread.Cells(i, 0).style.color = "black"
                        var cellvalue = spread.GetValue(i, 0);
                        var headingvalue = spread.GetValue(i, 4);
                                                
                        
                        if (cellvalue == "o") {
                            spread.SetValue(i, 0, "-", false); 
                            showclass = true
                        }
                        else {
                            spread.SetValue(i, 0, "+", false);
                            spread.Rows(i).style.display = "none";
                            showclass = false
                        }
                        cell.setAttribute("FpCellType", "readonly");
                      
                        
                    }
                    else if (rowtype == "SHeading") { 
                        if (showclass == false) {
                            spread.Rows(i).style.display = "none";
                        }
                        else {
                            cellvalue = spread.GetValue(i, 1);
                            if (cellvalue == "-") {
                                showsubtotal = true
                                spread.Rows(i).style.display = "table-row";
                            }
                            else {
                                showsubtotal = false
                                spread.Rows(i).style.display = "none";
                            }
                        }
                    }
                    else if (rowtype == "Total") {
                        if (showclass == false) {
                            cell = document.getElementById("spdAnalytics").GetCellByRowCol(i, 0);
                            cell.removeAttribute("FpCellType");
                            spread.SetValue(i, 0, "+", false);
                            cell.setAttribute("FpCellType", "readonly");
                        }
                        spread.Rows(i).style.display = "table-row";
                    }
                    else {                       
                       
                        spread.Rows(i).style.display = "table-row";
                    }
                }
            }
        }

//        function AddFilter() {
//            
//            if (document.getElementById("spdAnalytics")) {
//                var spread = document.getElementById("spdAnalytics");
//                var selindex = $('#intFilterOn').get(0).selectedIndex;
//                var filteramt = $('#decFilterAmt').val();
//                var selradio = $('#radioPosVar').attr('checked');
//                
//                var rc = spread.GetRowCount();
//                var amount = parseFloat(filteramt);
//                 
//                var showclass = false
//                var showsubclass = false
//                var coladj = selindex 
//                if (selindex >  2) {
//                  coladj = 1 + selindex 
//                }
//               
//                for (var i = 0; i < rc; i++) {
//                    var rowtype = spread.GetValue(i, 3); 
//                    if (rowtype == "Detail" || rowtype == "SDetail" || rowtype == "STotal") {
//                        if (showclass == false) {
//                            spread.Rows(i).style.display = "none";
//                        }
//                        else { 
//                            var value = parseInt(spread.GetValue(i, coladj + 7).replace(',', '', 'g')); 
//                            if (value < filteramt) { 
//                                if (selradio == "checked") {
//                                    spread.Rows(i).style.display = "none";
//                                }
//                                else {
//                                    spread.Rows(i).style.display = "table-row";
//                                }
//                            }
//                            else if (value > filteramt) { 
//                                if (selradio == "checked") {
//                                    if (rowtype == "SDetail") {
//                                        if (showsubtotal == true) {
//                                            spread.Rows(i).style.display = "table-row";
//                                        }
//                                        else {
//                                            spread.Rows(i).style.display = "none";
//                                        }
//                                    }
//                                    else {
//                                        spread.Rows(i).style.display = "table-row";
//                                    }
//                                }
//                                else {
//                                    spread.Rows(i).style.display = "none";
//                                }
//                            }
//                        }
//                    }
//                    else if (rowtype == "SHeading") {
//                        cellvalue = spread.GetValue(i, 1);
//                        if (cellvalue == "-") {
//                            showsubtotal = true
//                        }
//                        else {
//                            showsubtotal = false
//                        }
//                    }
//                    else if (rowtype == "Heading") {
//                        var cellvalue = spread.GetValue(i, 0);
//                        if (cellvalue == "-") {
//                            showclass = true
//                        }
//                        else {
//                            showclass = false
//                        }
//                    }
//                    else if (rowtype == "Total") {
//                        if (showclass == true) {
//                            spread.Rows(i).style.display = "none";
//                        }
//                    }
//                    else if (rowtype == "GPTotal") {
//                        spread.Rows(i).style.display = "none";
//                    }
//                    else if (rowtype == "IOTotal") {
//                        spread.Rows(i).style.display = "none";
//                    }
//                    else if (rowtype == "NITotal") {
//                        spread.Rows(i).style.display = "none";
//                    }
//                    else {

//                    }
//                }
//            }
//        }

//        function RemoveFilter() {
//            if (document.getElementById("spdAnalytics")) {
//                var spread = document.getElementById("spdAnalytics");
//                var rc = spread.GetRowCount();
//                var cellvalue = ""
//                var showclass = false
//                var showsubtotal = false
//                for (var i = 0; i < rc; i++) {
//                    var rowtype = spread.GetValue(i, 3);
//                    if (rowtype == "Detail" || rowtype == "SDetail" || rowtype == "STotal") {
//                        if (showclass == false) {
//                            spread.Rows(i).style.display = "none";
//                        }
//                        else {
//                            if (rowtype == "SDetail") {
//                                if (showsubtotal == true) {
//                                    spread.Rows(i).style.display = "table-row";
//                                }
//                                else {
//                                    spread.Rows(i).style.display = "none";
//                                }
//                            }
//                            else {
//                                spread.Rows(i).style.display = "table-row";
//                            }
//                        }
//                    }
//                    else if (rowtype == "Heading") {
//                        cellvalue = spread.GetValue(i, 0);
//                        if (cellvalue == "-") {
//                            showclass = true
//                        }
//                        else {
//                            showclass = false
//                        }
//                    }
//                    else if (rowtype == "SHeading") {
//                        if (showclass == false) {
//                            spread.Rows(i).style.display = "none";
//                        }
//                        else {
//                            cellvalue = spread.GetValue(i, 1);
//                            if (cellvalue == "-") {
//                                showsubtotal = true
//                                spread.Rows(i).style.display = "table-row";
//                            }
//                            else {
//                                showsubtotal = false
//                                spread.Rows(i).style.display = "none";
//                            }
//                        }
//                    }
//                    else {
//                        spread.Rows(i).style.display = "table-row";
//                    }
//                }
//            }
//        }

        function HideUnFilterRow(row) { 
            var selindex = $('#intFilterOn').get(0).selectedIndex;
            var filteramt = $('#decFilterAmt').val();
            var amount = parseFloat(filteramt); 
            var selradio = $('#radioPosVar').attr('checked'); 
            var spread = document.getElementById("spdAnalytics");
            var coladj = selindex 
            if (selindex >  2) {
                coladj = 1 + selindex 
            }
            var value = parseFloat(spread.GetValue(row, coladj  + 7).replace(',', '', 'g')); 
            if (value < filteramt) {
                if (selradio == "checked") {
                    spread.Rows(row).style.display = "none";
                }
                else {
                    spread.Rows(row).style.display = "table-row";
                }
            }
            else {
                if (selradio == "checked") {
                    spread.Rows(row).style.display = "table-row";

                }
                else {
                    spread.Rows(row).style.display = "none";
                }
            }
        }
//        ew end of code for highlight var and filter
        function getSavedReportsList() {

        if (isSessionAliveCalled == 'false') { IsSessionAlive(); }

        $.ajax({
            type: "GET",
            url: '@Url.Action("GetSavedViewsList", "SV")',
            global: false,
            cache: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (msg) {
                var _selectedCompany
                if (msg !== undefined && (msg != "")) {

                    for (var savedreport_index = 0; savedreport_index < msg.length; savedreport_index++) {
                        var idcontrolVal = msg[savedreport_index].Value;
                        var id_controller = idcontrolVal.split("_");
                        var controller = id_controller[0]
                        var url;
                        if (controller.trim() == "RE")
                            url = '@Url.Action("Viewer", "RE", New With {.Id = "-1"})'
                        else if (controller.trim() == "ALC")
                            url = '@Url.Action("Viewer", "ALC", New With {.Id = "-1"})'
                        else if (controller.trim() == "CF")
                            url = '@Url.Action("Viewer", "CF", New With {.Id = "-1"})'
                        else if (controller.trim() == "RA")
                            url = '@Url.Action("Viewer", "RA", New With {.Id = "-1"})'
                        else if (controller.trim() == "OM")
                            url = '@Url.Action("Viewer", "OM", New With {.Id = "-1"})'

                        url = url.replace("-1", id_controller[1]);
                        $("#SavedReportsList").append("<li><a  class='savedviews' href='" + url + "' data='" + msg[savedreport_index].Value + "' >" + msg[savedreport_index].Text + "</a></li>");
                    }                    
                }
                else {
                    $("#SavedReportsList").html("No Saved Reports found ! ");
                }
                setSelectedPageNav();
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
            }
        });
    }
</script>
