﻿@ModelType onlineAnalytics.SaveView 
@Imports  onlineAnalytics
@Code
    Dim utility As New Utility()
    ViewData("Title") = "PlanGuru Analytics | Edit Saved Reports"
    If Not Session("action") Is Nothing Then
        
        If (Session("action") = "SetDefaultAnalysis" or Session("action") = "GetAnalyses") Then
            Layout = Nothing
            Session.Remove("action")
        End If
    Else
        Layout = "~/Views/Shared/AnalyticsMaster.vbhtml"
    End If
    
    
    Dim ua As New UserAccess
    If Not (Session("UserAccess") Is Nothing) Then
        ua = DirectCast(Session("UserAccess"), UserAccess)
    End If
    Dim SelectedCompany As String = String.Empty
    Dim CompanyName As String = String.Empty 
    
    If (Not Session("SelectedCompanyFromDropdown") Is Nothing) Then
        SelectedCompany = Session("SelectedCompanyFromDropdown").ToString()
        CompanyName = Common.Decrypt(utility.CompaniesRepository.GetCompanyById(SelectedCompany).CompanyName)
    End If
End Code
<script src="@Url.Content("~/Scripts/jquery.validate.min.js")" type="text/javascript"></script>
<script src="@Url.Content("~/Scripts/jquery.validate.unobtrusive.min.js")" type="text/javascript"></script>

<div class="headerTable" style="float: right">
    <table>
        <tr>
            <td align="left" width="70%">
                Edit Saved Reports
            </td>            
            @Code
                If (ua.ViewSavedViews = True) Then
                @<td align="right" width="12%" class="add button_example" href='@Url.Action("Views", "SV")'>
                    <img src='@Url.Content("~/Content/Images/List.png")' class="analysisIcon" style="margin-right:5px;float:left;"/> Reports List
                </td>
                End If
            End Code
        </tr>
    </table>
</div>
<br />
<br />
<div class="center-panel">
    @Using Html.BeginForm("Edit", "SV")
        @Html.AntiForgeryToken()   
        @Html.ValidationSummary(True)        
        @<fieldset>
            <div style="width: 100;">
                <div style="float: left; width: 33%;">
                    <ol class="round">
                        <li>
                            @Html.LabelFor(Function(m) m.viewname)<br />
                            @Html.TextBoxFor(Function(m) m.viewname)<br />
                            @Html.ValidationMessageFor(Function(m) m.viewname)
                        </li>
                    </ol>
                </div>
                <div style="float: left; width: 36%;">
                    <ol class="round">
                        <li>
                            Company<br />
                            <input type="text" value='@CompanyName' id="CompanyName" disabled="disabled" /><br />                                                        
                        </li>
                    </ol>
                </div>
                
                <div class="input-form">
                    <input type="submit" value="Update saved report details" class="secondbutton_example" />
                    <input id="Cancel" type="button" value="Cancel" class="secondbutton_example" data='@Url.Action("Views")' />
                </div>
                <div style="float: left; margin-top: 10px;">
                    @If Not (DirectCast(TempData("Message"), String) Is Nothing) Then
                        @<label class="success">@TempData("Message").ToString()
                        </label>                         
                    End If
                    @If Not (DirectCast(TempData("ErrorMessage"), String) Is Nothing) Then
                        @<label class="error">@TempData("ErrorMessage").ToString()
                        </label>                         
                    End If
                </div>
                @Html.HiddenFor(Function(m) m.ID)
            </div>
        </fieldset>                                  
    End Using
</div>
<script type="text/javascript" language="javascript">

    $(document).ready(function () {
        $("#Cancel").click(function (event) {            
            var url = $(this).attr('data');
            window.location = url;
        });

        $("#CompanyName").attr("disabled", true);
    });  


</script>
