﻿Imports System.Collections.Generic
Imports System.Linq
Imports System.Data

Public Class ChartTypeRepository
    Inherits GenericRepository(Of ChartType)
    Implements IChartTypeRepository

    'Implements IDisposable
    Private disposed As Boolean = False
    Public Sub New(context As DataAccess)
        MyBase.New(context)
    End Sub

    Public Function GetChartTypes() As IEnumerable(Of ChartType) Implements IChartTypeRepository.GetChartTypes
        Return context.ChartTypes.ToList()
    End Function

    Public Function GetChartTypeById(ChartTypeId As Integer) As ChartType Implements IChartTypeRepository.GetChartTypeById
        Return context.ChartTypes.Find(ChartTypeId)
    End Function

    Public Sub Save() Implements IChartTypeRepository.Save
        context.SaveChanges()
    End Sub
    Protected Overridable Sub Dispose(disposing As Boolean)
        If Not Me.disposed Then
            If disposing Then
                context.Dispose()
            End If
        End If
        Me.disposed = True
    End Sub

    Public Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
End Class
