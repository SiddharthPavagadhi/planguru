﻿Imports System.ComponentModel.DataAnnotations

Public Class Balance

    'Primary key (BalanceId,AccountId,AnalysisId)
    <Key(), Column(Order:=0)>
    <DatabaseGenerated(DatabaseGeneratedOption.None)>
    Public Property BalanceId As Integer

    <Key(), Column(Order:=1)>
    Public Property AccountId As Integer

    <Key(), Column(Order:=2)>
    Public Property AnalysisId As Integer

    <RegularExpression("^-?[0-9]+(\.[0-9]{1,5})?$")>
    Public Property H101 As Decimal
    <RegularExpression("^-?[0-9]+(\.[0-9]{1,5})?$")>
    Public Property H102 As Decimal
    <RegularExpression("^-?[0-9]+(\.[0-9]{1,5})?$")>
    Public Property H103 As Decimal
    <RegularExpression("^-?[0-9]+(\.[0-9]{1,5})?$")>
    Public Property H104 As Decimal
    <RegularExpression("^-?[0-9]+(\.[0-9]{1,5})?$")>
    Public Property H105 As Decimal
    <RegularExpression("^-?[0-9]+(\.[0-9]{1,5})?$")>
    Public Property H106 As Decimal
    <RegularExpression("^-?[0-9]+(\.[0-9]{1,5})?$")>
    Public Property H107 As Decimal
    <RegularExpression("^-?[0-9]+(\.[0-9]{1,5})?$")>
    Public Property H108 As Decimal
    <RegularExpression("^-?[0-9]+(\.[0-9]{1,5})?$")>
    Public Property H109 As Decimal
    <RegularExpression("^-?[0-9]+(\.[0-9]{1,5})?$")>
    Public Property H110 As Decimal
    <RegularExpression("^-?[0-9]+(\.[0-9]{1,5})?$")>
    Public Property H111 As Decimal
    <RegularExpression("^-?[0-9]+(\.[0-9]{1,5})?$")>
    Public Property H112 As Decimal

    <RegularExpression("^-?[0-9]+(\.[0-9]{1,5})?$")>
    Public Property H201 As Decimal
    <RegularExpression("^-?[0-9]+(\.[0-9]{1,5})?$")>
    Public Property H202 As Decimal
    <RegularExpression("^-?[0-9]+(\.[0-9]{1,5})?$")>
    Public Property H203 As Decimal
    <RegularExpression("^-?[0-9]+(\.[0-9]{1,5})?$")>
    Public Property H204 As Decimal
    <RegularExpression("^-?[0-9]+(\.[0-9]{1,5})?$")>
    Public Property H205 As Decimal
    <RegularExpression("^-?[0-9]+(\.[0-9]{1,5})?$")>
    Public Property H206 As Decimal
    <RegularExpression("^-?[0-9]+(\.[0-9]{1,5})?$")>
    Public Property H207 As Decimal
    <RegularExpression("^-?[0-9]+(\.[0-9]{1,5})?$")>
    Public Property H208 As Decimal
    <RegularExpression("^-?[0-9]+(\.[0-9]{1,5})?$")>
    Public Property H209 As Decimal
    <RegularExpression("^-?[0-9]+(\.[0-9]{1,5})?$")>
    Public Property H210 As Decimal
    <RegularExpression("^-?[0-9]+(\.[0-9]{1,5})?$")>
    Public Property H211 As Decimal
    <RegularExpression("^-?[0-9]+(\.[0-9]{1,5})?$")>
    Public Property H212 As Decimal


    <RegularExpression("^-?[0-9]+(\.[0-9]{1,5})?$")>
    Public Property H301 As Decimal
    <RegularExpression("^-?[0-9]+(\.[0-9]{1,5})?$")>
    Public Property H302 As Decimal
    <RegularExpression("^-?[0-9]+(\.[0-9]{1,5})?$")>
    Public Property H303 As Decimal
    <RegularExpression("^-?[0-9]+(\.[0-9]{1,5})?$")>
    Public Property H304 As Decimal
    <RegularExpression("^-?[0-9]+(\.[0-9]{1,5})?$")>
    Public Property H305 As Decimal
    <RegularExpression("^-?[0-9]+(\.[0-9]{1,5})?$")>
    Public Property H306 As Decimal
    <RegularExpression("^-?[0-9]+(\.[0-9]{1,5})?$")>
    Public Property H307 As Decimal
    <RegularExpression("^-?[0-9]+(\.[0-9]{1,5})?$")>
    Public Property H308 As Decimal
    <RegularExpression("^-?[0-9]+(\.[0-9]{1,5})?$")>
    Public Property H309 As Decimal
    <RegularExpression("^-?[0-9]+(\.[0-9]{1,5})?$")>
    Public Property H310 As Decimal
    <RegularExpression("^-?[0-9]+(\.[0-9]{1,5})?$")>
    Public Property H311 As Decimal
    <RegularExpression("^-?[0-9]+(\.[0-9]{1,5})?$")>
    Public Property H312 As Decimal

    <RegularExpression("^-?[0-9]+(\.[0-9]{1,5})?$")>
    Public Property H401 As Decimal
    <RegularExpression("^-?[0-9]+(\.[0-9]{1,5})?$")>
    Public Property H402 As Decimal
    <RegularExpression("^-?[0-9]+(\.[0-9]{1,5})?$")>
    Public Property H403 As Decimal
    <RegularExpression("^-?[0-9]+(\.[0-9]{1,5})?$")>
    Public Property H404 As Decimal
    <RegularExpression("^-?[0-9]+(\.[0-9]{1,5})?$")>
    Public Property H405 As Decimal
    <RegularExpression("^-?[0-9]+(\.[0-9]{1,5})?$")>
    Public Property H406 As Decimal
    <RegularExpression("^-?[0-9]+(\.[0-9]{1,5})?$")>
    Public Property H407 As Decimal
    <RegularExpression("^-?[0-9]+(\.[0-9]{1,5})?$")>
    Public Property H408 As Decimal
    <RegularExpression("^-?[0-9]+(\.[0-9]{1,5})?$")>
    Public Property H409 As Decimal
    <RegularExpression("^-?[0-9]+(\.[0-9]{1,5})?$")>
    Public Property H410 As Decimal
    <RegularExpression("^-?[0-9]+(\.[0-9]{1,5})?$")>
    Public Property H411 As Decimal
    <RegularExpression("^-?[0-9]+(\.[0-9]{1,5})?$")>
    Public Property H412 As Decimal

    <RegularExpression("^-?[0-9]+(\.[0-9]{1,5})?$")>
    Public Property H501 As Decimal
    <RegularExpression("^-?[0-9]+(\.[0-9]{1,5})?$")>
    Public Property H502 As Decimal
    <RegularExpression("^-?[0-9]+(\.[0-9]{1,5})?$")>
    Public Property H503 As Decimal
    <RegularExpression("^-?[0-9]+(\.[0-9]{1,5})?$")>
    Public Property H504 As Decimal
    <RegularExpression("^-?[0-9]+(\.[0-9]{1,5})?$")>
    Public Property H505 As Decimal
    <RegularExpression("^-?[0-9]+(\.[0-9]{1,5})?$")>
    Public Property H506 As Decimal
    <RegularExpression("^-?[0-9]+(\.[0-9]{1,5})?$")>
    Public Property H507 As Decimal
    <RegularExpression("^-?[0-9]+(\.[0-9]{1,5})?$")>
    Public Property H508 As Decimal
    <RegularExpression("^-?[0-9]+(\.[0-9]{1,5})?$")>
    Public Property H509 As Decimal
    <RegularExpression("^-?[0-9]+(\.[0-9]{1,5})?$")>
    Public Property H510 As Decimal
    <RegularExpression("^-?[0-9]+(\.[0-9]{1,5})?$")>
    Public Property H511 As Decimal
    <RegularExpression("^-?[0-9]+(\.[0-9]{1,5})?$")>
    Public Property H512 As Decimal

    <RegularExpression("^-?[0-9]+(\.[0-9]{1,5})?$")>
    Public Property B101 As Decimal
    <RegularExpression("^-?[0-9]+(\.[0-9]{1,5})?$")>
    Public Property B102 As Decimal
    <RegularExpression("^-?[0-9]+(\.[0-9]{1,5})?$")>
    Public Property B103 As Decimal
    <RegularExpression("^-?[0-9]+(\.[0-9]{1,5})?$")>
    Public Property B104 As Decimal
    <RegularExpression("^-?[0-9]+(\.[0-9]{1,5})?$")>
    Public Property B105 As Decimal
    <RegularExpression("^-?[0-9]+(\.[0-9]{1,5})?$")>
    Public Property B106 As Decimal
    <RegularExpression("^-?[0-9]+(\.[0-9]{1,5})?$")>
    Public Property B107 As Decimal
    <RegularExpression("^-?[0-9]+(\.[0-9]{1,5})?$")>
    Public Property B108 As Decimal
    <RegularExpression("^-?[0-9]+(\.[0-9]{1,5})?$")>
    Public Property B109 As Decimal
    <RegularExpression("^-?[0-9]+(\.[0-9]{1,5})?$")>
    Public Property B110 As Decimal
    <RegularExpression("^-?[0-9]+(\.[0-9]{1,5})?$")>
    Public Property B111 As Decimal
    <RegularExpression("^-?[0-9]+(\.[0-9]{1,5})?$")>
    Public Property B112 As Decimal

    <RegularExpression("^-?[0-9]+(\.[0-9]{1,5})?$")>
    Public Property A101 As Decimal
    <RegularExpression("^-?[0-9]+(\.[0-9]{1,5})?$")>
    Public Property A102 As Decimal
    <RegularExpression("^-?[0-9]+(\.[0-9]{1,5})?$")>
    Public Property A103 As Decimal
    <RegularExpression("^-?[0-9]+(\.[0-9]{1,5})?$")>
    Public Property A104 As Decimal
    <RegularExpression("^-?[0-9]+(\.[0-9]{1,5})?$")>
    Public Property A105 As Decimal
    <RegularExpression("^-?[0-9]+(\.[0-9]{1,5})?$")>
    Public Property A106 As Decimal
    <RegularExpression("^-?[0-9]+(\.[0-9]{1,5})?$")>
    Public Property A107 As Decimal
    <RegularExpression("^-?[0-9]+(\.[0-9]{1,5})?$")>
    Public Property A108 As Decimal
    <RegularExpression("^-?[0-9]+(\.[0-9]{1,5})?$")>
    Public Property A109 As Decimal
    <RegularExpression("^-?[0-9]+(\.[0-9]{1,5})?$")>
    Public Property A110 As Decimal
    <RegularExpression("^-?[0-9]+(\.[0-9]{1,5})?$")>
    Public Property A111 As Decimal
    <RegularExpression("^-?[0-9]+(\.[0-9]{1,5})?$")>
    Public Property A112 As Decimal

    <RegularExpression("^-?[0-9]+(\.[0-9]{1,5})?$")>
    Public Property B201 As Decimal
    <RegularExpression("^-?[0-9]+(\.[0-9]{1,5})?$")>
    Public Property B202 As Decimal
    <RegularExpression("^-?[0-9]+(\.[0-9]{1,5})?$")>
    Public Property B203 As Decimal
    <RegularExpression("^-?[0-9]+(\.[0-9]{1,5})?$")>
    Public Property B204 As Decimal
    <RegularExpression("^-?[0-9]+(\.[0-9]{1,5})?$")>
    Public Property B205 As Decimal
    <RegularExpression("^-?[0-9]+(\.[0-9]{1,5})?$")>
    Public Property B206 As Decimal
    <RegularExpression("^-?[0-9]+(\.[0-9]{1,5})?$")>
    Public Property B207 As Decimal
    <RegularExpression("^-?[0-9]+(\.[0-9]{1,5})?$")>
    Public Property B208 As Decimal
    <RegularExpression("^-?[0-9]+(\.[0-9]{1,5})?$")>
    Public Property B209 As Decimal
    <RegularExpression("^-?[0-9]+(\.[0-9]{1,5})?$")>
    Public Property B210 As Decimal
    <RegularExpression("^-?[0-9]+(\.[0-9]{1,5})?$")>
    Public Property B211 As Decimal
    <RegularExpression("^-?[0-9]+(\.[0-9]{1,5})?$")>
    Public Property B212 As Decimal

    <RegularExpression("^-?[0-9]+(\.[0-9]{1,5})?$")>
    Public Property B301 As Decimal
    <RegularExpression("^-?[0-9]+(\.[0-9]{1,5})?$")>
    Public Property B302 As Decimal
    <RegularExpression("^-?[0-9]+(\.[0-9]{1,5})?$")>
    Public Property B303 As Decimal
    <RegularExpression("^-?[0-9]+(\.[0-9]{1,5})?$")>
    Public Property B304 As Decimal
    <RegularExpression("^-?[0-9]+(\.[0-9]{1,5})?$")>
    Public Property B305 As Decimal
    <RegularExpression("^-?[0-9]+(\.[0-9]{1,5})?$")>
    Public Property B306 As Decimal
    <RegularExpression("^-?[0-9]+(\.[0-9]{1,5})?$")>
    Public Property B307 As Decimal
    <RegularExpression("^-?[0-9]+(\.[0-9]{1,5})?$")>
    Public Property B308 As Decimal
    <RegularExpression("^-?[0-9]+(\.[0-9]{1,5})?$")>
    Public Property B309 As Decimal
    <RegularExpression("^-?[0-9]+(\.[0-9]{1,5})?$")>
    Public Property B310 As Decimal
    <RegularExpression("^-?[0-9]+(\.[0-9]{1,5})?$")>
    Public Property B311 As Decimal
    <RegularExpression("^-?[0-9]+(\.[0-9]{1,5})?$")>
    Public Property B312 As Decimal
    'Public Overridable Property Account() As Account

End Class
