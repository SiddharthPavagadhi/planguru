﻿<?xml version="1.0"?>

<configuration>
  <configSections>
    <sectionGroup name="system.web.webPages.razor" type="System.Web.WebPages.Razor.Configuration.RazorWebSectionGroup, System.Web.WebPages.Razor, Version=1.0.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35">
      <section name="host" type="System.Web.WebPages.Razor.Configuration.HostSection, System.Web.WebPages.Razor, Version=1.0.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35" requirePermission="false" />
      <section name="pages" type="System.Web.WebPages.Razor.Configuration.RazorPagesSection, System.Web.WebPages.Razor, Version=1.0.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35" requirePermission="false" />      
    </sectionGroup>
    <section name="log4net" type="log4net.Config.Log4NetConfigurationSectionHandler, log4net" />
  </configSections>
  <log4net>
    <appender name="DEBUGFileAppender" type="log4net.Appender.RollingFileAppender">
      <lockingmodel type="log4net.Appender.FileAppender+MinimalLock" />
      <file value="Logs\Debug\" />
      <appendtofile value="true" />
      <rollingstyle value="Date" />
      <datePattern value="dd-MM-yyyy'.log'" />
      <maxsizerollbackups value="10" />
      <maximumfilesize value="200MB" />
      <staticlogfilename value="false" />
      <filter type="log4net.Filter.LevelRangeFilter">
        <levelMin value="DEBUG" />
        <levelMax value="DEBUG" />
      </filter>
      <layout type="log4net.Layout.PatternLayout">
        <conversionPattern value="%-5p [%d] [%C.%M] %m%n" />
      </layout>
    </appender>
    <appender name="INFOFileAppender" type="log4net.Appender.RollingFileAppender">
      <lockingmodel type="log4net.Appender.FileAppender+MinimalLock" />
      <file value="Logs\Info\" />
      <appendtofile value="true" />
      <rollingstyle value="Date" />
      <datePattern value="dd-MM-yyyy'.log'" />
      <maxsizerollbackups value="10" />
      <maximumfilesize value="200MB" />
      <staticlogfilename value="false" />
      <filter type="log4net.Filter.LevelRangeFilter">
        <levelMin value="INFO" />
        <levelMax value="INFO" />
      </filter>
      <layout type="log4net.Layout.PatternLayout">
        <conversionPattern value="%-5p [%d] [%C.%M] %m%n" />
      </layout>
    </appender>
    <appender name="WARNFileAppender" type="log4net.Appender.RollingFileAppender">
      <lockingmodel type="log4net.Appender.FileAppender+MinimalLock" />
      <file value="Logs\Warn\" />
      <appendtofile value="true" />
      <rollingstyle value="Date" />
      <datePattern value="dd-MM-yyyy'.log'" />
      <maxsizerollbackups value="10" />
      <maximumfilesize value="200MB" />
      <staticlogfilename value="false" />
      <filter type="log4net.Filter.LevelRangeFilter">
        <levelMin value="WARN" />
        <levelMax value="WARN" />
      </filter>
      <layout type="log4net.Layout.PatternLayout">
        <conversionPattern value="%-5p [%d] [%C.%M] %m%n" />
      </layout>
    </appender>
    <appender name="ERRORFileAppender" type="log4net.Appender.RollingFileAppender">
      <lockingmodel type="log4net.Appender.FileAppender+MinimalLock" />
      <file value="Logs\Error\" />
      <appendtofile value="true" />
      <rollingstyle value="Date" />
      <datePattern value="dd-MM-yyyy'.log'" />
      <maxsizerollbackups value="10" />
      <maximumfilesize value="200MB" />
      <staticlogfilename value="false" />
      <filter type="log4net.Filter.LevelRangeFilter">
        <levelMin value="ERROR" />
        <levelMax value="FATAL" />
      </filter>
      <layout type="log4net.Layout.PatternLayout">
        <conversionPattern value="%-5p [%d] [%C.%M] %m%n" />
      </layout>
    </appender>
    <appender name="ElectronicDeliveryRollingFileAppender" type="log4net.Appender.RollingFileAppender">
      <file value="ElectronicDelivery.txt" />
      <appendToFile value="true" />
      <rollingStyle value="Size" />
      <maxSizeRollBackups value="3" />”
      <maximumFileSize value="500KB" /><staticLogFileName value="true" /><layout type="log4net.Layout.PatternLayout">
        <conversionPattern value="%date [%thread] %-5level %logger [%property{NDC}] – %message%newline" />
      </layout>
    </appender>
    <logger name="ElectronicDelivery">
      <level value="DEBUG" />
      <appender-ref ref="ElectronicDeliveryRollingFileAppender" />
    </logger>
    <root>
      <level value="ALL" />
      <appender-ref ref="DEBUGFileAppender" />
      <appender-ref ref="INFOFileAppender" />
      <appender-ref ref="WARNFileAppender" />
      <appender-ref ref="ERRORFileAppender" />
    </root>
  </log4net>
  <system.web.webPages.razor>
    <host factoryType="System.Web.Mvc.MvcWebRazorHostFactory, System.Web.Mvc, Version=3.0.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35" />
    <pages pageBaseType="System.Web.Mvc.WebViewPage">
      <namespaces>
        <add namespace="System.Web.Mvc" />
        <add namespace="System.Web.Mvc.Ajax" />
        <add namespace="System.Web.Mvc.Html" />
        <add namespace="System.Web.Routing" />
      </namespaces>
    </pages>
  </system.web.webPages.razor>

  <appSettings>
    <add key="webpages:Enabled" value="false" />
  </appSettings>

  <system.web>
    <httpHandlers>
      <add path="*" verb="*" type="System.Web.HttpNotFoundHandler"/>
    </httpHandlers>

    <!--
        Enabling request validation in view pages would cause validation to occur
        after the input has already been processed by the controller. By default
        MVC performs request validation before a controller processes the input.
        To change this behavior apply the ValidateInputAttribute to a
        controller or action.
    -->
    <pages
        validateRequest="false"
        pageParserFilterType="System.Web.Mvc.ViewTypeParserFilter, System.Web.Mvc, Version=3.0.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35"
        pageBaseType="System.Web.Mvc.ViewPage, System.Web.Mvc, Version=3.0.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35"
        userControlBaseType="System.Web.Mvc.ViewUserControl, System.Web.Mvc, Version=3.0.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35">
      <controls>
        <add assembly="System.Web.Mvc, Version=3.0.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35" namespace="System.Web.Mvc" tagPrefix="mvc" />
      </controls>
    </pages>
  </system.web>

  <system.webServer>
    <validation validateIntegratedModeConfiguration="false" />

    <handlers>
      <remove name="BlockViewHandler"/>
      <add name="BlockViewHandler" path="*" verb="*" preCondition="integratedMode" type="System.Web.HttpNotFoundHandler" />
    </handlers>
  </system.webServer>
</configuration>
