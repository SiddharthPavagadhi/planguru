﻿@ModelType onlineAnalytics.Customer
@Code
    ViewData("Title") = "PlanGuru Analytics | Signup Confirmation"
    If Not Session("action") Is Nothing Then
        
        If (Session("action") = "SetDefaultAnalysis" or Session("action") = "GetAnalyses") Then
            Layout = Nothing
            Session.Remove("action")
        End If
    Else
        Layout = "~/Views/Shared/AnalyticsMaster.vbhtml"
    End If
End Code
<header>    
<link rel="stylesheet" href="@Url.Content("~/themes/default/recurly.css")" type="text/css" />
    <link rel="stylesheet" href="@Url.Content("~/themes/default/examples.css")" type="text/css" />
    </header>
<h2>
    Subscription</h2>
<div id="billing-info" class="section">
    <div id="recurly-subscribe" >
        <h2 class="heading">
            Signup Confirmation</h2>
        <div style="width:90%;margin:0px 40px;"><br /><br /><br />
            <p>Thank you for your PlanGuru Analytics trial signup.  Shortly you’ll be receiving a signup confirmation email.  In addition, you receive your temporary password which will enable you to log into PlanGuru Analytics in the future.</p><br />  
            <p>The next step is to add a company.  Like the PlanGuru budgeting and forecasting software, you can add as many companies as you want at no additional cost.  Plus, within each company you can have as many analyses, department or divisions as you need.</p><br />
            <p>So let’s get started:  @Html.ActionLink("Add a new company", "Create","Company")</p><br /><br />
        </div>       
    </div>
     
    <div style="float: left; margin-left: 20px;">
        @If Not (DirectCast(TempData("Message"), String) Is Nothing) Then
            @<label class="success">@TempData("Message").ToString()
            </label>                         
    End If
        @If Not (DirectCast(TempData("ErrorMessage"), String) Is Nothing) Then
            @<label class="error">@TempData("ErrorMessage").ToString()
            </label>                         
    End If
</div>
</div>