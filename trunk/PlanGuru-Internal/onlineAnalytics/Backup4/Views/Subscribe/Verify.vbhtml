﻿@Code
    ViewData("Title") = "PlanGuru Analytics | Verify"
End Code

<h2>Verify</h2>

@If (Request.QueryString.Count <= 0) Then
    @<p>No params posted back</p>
Else
    @<p>The posted params are:</p>
    @<ul>
        @For Each key As String In Request.QueryString.Keys
                @<li>key=Request.QueryString[key]</li>
         Next
    </ul>
End If
<label class="success">User has subscribed successfully.</label>     