﻿Public Class FormValues
    Public Property intPeriod As Integer
    Public Property intFormat As Integer
    Public Property blnShowasPer As Boolean
    Public Property blnShowVar As Boolean
    Public Property blnHighVar As Boolean
    Public Property blnHighPosVar As Boolean
    Public Property intHighPosVar As Integer
    Public Property blnHighNegVar As Boolean
    Public Property intHighNegVar As Integer
    Public Property blnUseFilter As Boolean
    Public Property strFilterOn As String
    Public Property intFilterOn As Integer
    Public Property decFilterAmt As Decimal
    Public Property blnFilterPosVariance As Boolean
    Public Property blnUpdateChart As Boolean
    Public Property intChartType As Integer
    Public Property intBrowserWidth As Integer
    Public Property blnShowTrend As Boolean
    Public Property intDept As Integer
    Public Property blnShowBudget As Boolean
    Public Property strCheckboxCaption As String
    Public Property intHPeriod As Integer

End Class
