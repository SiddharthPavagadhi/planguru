﻿Imports System.Web.UI.WebControls.Expressions
Imports FarPoint.Mvc.Spread
Imports System.Data.Entity
Namespace onlineAnalytics

    <CustAuthFilter()>
    Public Class ScorecardController
        Inherits System.Web.Mvc.Controller

        '
        ' GET: /Scorecard
        Private utility As New Utility()
        Private userRepository As IUserRepository
        Private accountRepository As IAccountRepository
        Private accountTypeRepository As IAccountTypeRepository
        Private scorecardRepository As IScorecardRepository
        Private balanceRepository As IBalanceRepository
        Private context As DataAccess

        Private UserType As UserRole
        Private UserInfo As User
        Private scorecardItems As Scorecard
        Private intFirstFiscal As Integer
        Private CustomerId As String = Nothing
        Private intUserID As Integer
        Private intAnalysisID As Integer
        Private intSelPeriod As Integer
        Private intSaveAnalyisID As Integer
        Private orderIndex As Integer = 0
        Private sPeriod As Integer = Val(DateTime.Today.ToString("MM")) - 2
        Private sFormat As Integer = 1
        Private typeDescList() As String = {"revenue", "expense"}
        Private blnArchBudget As Boolean = False


        Public Sub New()
            Me.userRepository = New UserRepository(New DataAccess())
            Me.scorecardRepository = New ScorecardRepository(New DataAccess())
            Me.accountRepository = New AccountRepository(New DataAccess())
            Me.accountTypeRepository = New AccountTypeRepository(New DataAccess())
            Me.balanceRepository = New BalanceRepository(New DataAccess())
        End Sub

        Public Sub New(userRepository As IUserRepository)
            Me.userRepository = userRepository
        End Sub
        Public Sub New(scorecardRepository As IScorecardRepository)
            Me.scorecardRepository = scorecardRepository
        End Sub

        Public Sub New(accountRepository As IAccountRepository)
            Me.accountRepository = accountRepository
        End Sub

        Public Sub New(accountTypeRepository As IAccountTypeRepository)
            Me.accountTypeRepository = accountTypeRepository
        End Sub

        Public Sub New(balanceRepository As IBalanceRepository)
            Me.balanceRepository = balanceRepository
        End Sub

        Private Function scorecardOrderIndex(ByRef value As Integer) As Integer
            orderIndex = value + 1
            Return orderIndex
        End Function


        <AcceptVerbs(HttpVerbs.Get)>
        Function IsSessionAlive() As JsonResult

            Dim data As Boolean
            If (Session.IsNewSession) Then
                data = False
            Else
                data = True
            End If

            Return Json(data, JsonRequestBehavior.AllowGet)
        End Function

        <CustomActionFilter()>
        Function Index(<MvcSpread("spdScorecard")> spdScorecard As FpSpread) As ActionResult

            Try
                Logger.Log.Info(String.Format("ScorecardController Index (HttpGet) method execution starts"))

                If (Not Session("sPeriod") Is Nothing) Then
                    sPeriod = Session("sPeriod")
                    sFormat = Session("sFormat")
                Else
                    If sPeriod = -1 Then
                        sPeriod = 11
                    End If
                End If

                Me.IndexViewer(sPeriod, False)

                Return View()
            Catch dataEx As DataException
                TempData("ErrorMessage") = String.Concat("Error occured to view scorecard - ", dataEx.Message)
                Logger.Log.Error(String.Format("\n Error occured to view scorecard - {0} " + Environment.NewLine + "Stack Trace: {1} ", dataEx.Message, dataEx.StackTrace))
            Catch ex As Exception
                Me.IndexViewer(sPeriod, False)
                TempData("ErrorMessage") = String.Concat("Error occured during processing Scorecard index page -", ex.Message)
                Logger.Log.Error(String.Format("ScorecardController Index (HttpGet) - Error occured during processing Scorecard index page - with Message- {0} " + Environment.NewLine + "Stack Trace: {1} ", ex.Message, ex.StackTrace))
            Finally
                Logger.Log.Info(String.Format("ScorecardController Index (HttpGet) Execution Ended"))
            End Try
            Return View()
        End Function


        <HttpPost()> _
      <CustomActionFilter()>
        Function Index(<MvcSpread("spdScorecard")> spdScorecard As FpSpread, ByVal formValues As FormValues) As ActionResult
            Logger.Log.Info(String.Format("Scorecard postback Index Execution Started"))
            Try

                Me.IndexViewer(formValues.intPeriod - 1, True)

                Return View()                        
            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Error occured during processing postback Scorecard index page -", ex.Message)
                Logger.Log.Error(String.Format("Error occured during processing Scorecard postback index page - with Message- {0} " + Environment.NewLine + "Stack Trace: {1} ", ex.Message, ex.StackTrace))
            Finally
                Logger.Log.Info(String.Format("Scorecard postback Index Execution Ended"))
            End Try
            Return View()
        End Function

        Private Sub IndexViewer(Optional period As Integer = 0, Optional post As Boolean = False)

            Logger.Log.Info(String.Format("Scorecard IndexViewer Execution Started"))
            Dim setupCountObj As SetupCount
            Dim selectedAnalysis As Integer

            If (Not Session("UserType") Is Nothing) And (Not Session("UserInfo") Is Nothing) Then
                UserType = DirectCast(Session("UserType"), UserRole)
                UserInfo = DirectCast(Session("UserInfo"), User)
                intAnalysisID = UserInfo.AnalysisId
                intUserID = UserInfo.UserId
                intFirstFiscal = UserInfo.fiscalMonthOfSelectedCompany

            End If
            'If intSaveAnalyisID <> UserInfo.AnalysisId Then
            '    intSaveAnalyisID = UserInfo.AnalysisId
            '    period = 0
            'End If

            Logger.Log.Info(String.Format("Scorecard CommonProcedures.NumberofPeriods Execution Begin"))
            '   Dim intNumberofPeriods As Integer = CommonProcedures.NumberofPeriods(intCurrentMonth, intFirstFiscal)
            Logger.Log.Info(String.Format("Scorecard CommonProcedures.NumberofPeriods Execution End"))

            'If period = 0 Then
            '    intSelPeriod = intNumberofPeriods ' Val(DateTime.Today.ToString("MM")) - 2
            'Else
            intSelPeriod = period
            'End If


            If (UserInfo.AnalysisId > 0) Then
                selectedAnalysis = UserInfo.AnalysisId
            Else
                If (Not Session("SelectedAnalysisFromDropdown") Is Nothing) Then
                    selectedAnalysis = Session("SelectedAnalysisFromDropdown")
                Else
                    selectedAnalysis = -1
                End If
            End If

            Logger.Log.Info(String.Format("Scorecard CheckRequiredSetupCount Execution Begin"))
            setupCountObj = utility.CheckRequiredSetupCount(UserInfo, selectedAnalysis)
            ViewBag.Setup = setupCountObj
            Logger.Log.Info(String.Format("Scorecard CheckRequiredSetupCount Execution End"))

            If (setupCountObj.countDataUploadedOfSelectedAnalysis > 0 And setupCountObj.countOfScorecardItem = 0) Then

                Logger.Log.Info(String.Format("Scorecard selectedAccumAccounts Query Execution Begin"))
                Dim selectedAccumAccounts = (From a In utility.AccountRepository.GetAccountRecordsByAnalysisId(intAnalysisID, New String() {"total"})
                                             Join at In utility.AccountTypeRepository.GetAccountTypeRecordsByAnalysisId(intAnalysisID, New String() {"revenue"})
                                             On a.AcctTypeId Equals at.AcctTypeId And a.AnalysisId Equals at.AnalysisId
                                             Order By a.SortSequence
                                             Select a.AccountId, a.AcctTypeId, a.AnalysisId, a.AcctDescriptor, a.SortSequence, a.Description, a.Subgrouping, at.TypeDesc, at.ClassDesc
                                             ).ToList()
                Logger.Log.Info(String.Format("Scorecard selectedAccumAccounts Query Execution End"))

                If (selectedAccumAccounts.Count > 0) Then
                    context = New DataAccess()
                    For Each account In selectedAccumAccounts
                        Dim scorecardItemList As New List(Of Scorecard)() From {
                                   New Scorecard() With {.Order = scorecardOrderIndex(orderIndex), .ScorecardDesc = account.Description, .AccountId = account.AccountId, .AnalysisId = intAnalysisID, .Missed = 10, .Outperform = 10, .ShowasPercent = False, .VarIsFavorable = True, .Option1 = 0, .Option2 = 0, .Option3 = 0, .IsValid = True, .CreatedBy = UserInfo.UserId, .UpdatedBy = UserInfo.UserId}
                   }
                        scorecardItemList.ForEach(Function(u) context.Scorecards.Add(u))
                        context.SaveChanges()

                    Next
                End If

                Logger.Log.Info(String.Format("Scorecard selectedAccumAccounts1 Query Execution Begin"))
                Dim selectedAccumAccounts1 = (From a In utility.AccountRepository.GetAccountRecordsByAnalysisId(intAnalysisID, New String() {"nitotal"})
                                          Join at In utility.AccountTypeRepository.GetAccountTypeRecordsByAnalysisId(intAnalysisID)
                                          On a.AcctTypeId Equals at.AcctTypeId And a.AnalysisId Equals at.AnalysisId
                                          Order By a.SortSequence
                                          Select a.AccountId, a.AcctTypeId, a.AnalysisId, a.AcctDescriptor, a.SortSequence, a.Description, a.Subgrouping, at.TypeDesc, at.ClassDesc
                                          ).ToList()
                Logger.Log.Info(String.Format("Scorecard selectedAccumAccounts1 Query Execution End"))

                If (selectedAccumAccounts1.Count > 0) Then
                    context = New DataAccess()
                    For Each account In selectedAccumAccounts1
                        Dim scorecardItemList As New List(Of Scorecard)() From {
                                                New Scorecard() With {.Order = scorecardOrderIndex(orderIndex), .ScorecardDesc = account.Description, .AccountId = account.AccountId, .AnalysisId = intAnalysisID, .Missed = 10, .Outperform = 10, .ShowasPercent = False, .VarIsFavorable = True, .Option1 = 0, .Option2 = 0, .Option3 = 0, .IsValid = True, .CreatedBy = UserInfo.UserId, .UpdatedBy = UserInfo.UserId}
                        }
                        scorecardItemList.ForEach(Function(u) context.Scorecards.Add(u))
                        context.SaveChanges()

                    Next
                End If

            End If

            Dim Periods As List(Of SelectListItem) = New List(Of SelectListItem)
            For intCtr As Integer = 0 To 11
                Periods.Add(New SelectListItem With {.Text = CommonProcedures.GetMonth(intCtr, intFirstFiscal), .Value = intCtr + 1, .Selected = False})
            Next
            ViewData("Periods") = New SelectList(Periods, "value", "text", "May")
            ViewData("intPeriod") = intSelPeriod + 1
            ViewData("intDept") = 1
            Session("sPeriod") = intSelPeriod
            Logger.Log.Info(String.Format("Scorecard IndexViewer Execution Ended"))
        End Sub

        <AcceptVerbs(HttpVerbs.Get)> _
        <CustomActionFilter()>
        Public Function GetCompanies(Optional SelectedCompany As Integer = Nothing) As JsonResult

            If (Not Session("UserType") Is Nothing) And (Not Session("UserInfo") Is Nothing) Then
                UserType = DirectCast(Session("UserType"), UserRole)
                UserInfo = DirectCast(Session("UserInfo"), User)
                CustomerId = If(UserType.UserRoleId = UserRoles.SAU, UserInfo.CustomerId, Nothing)

                'When user logged in, it will show the company list based on analysisId
                If (SelectedCompany = 0) Then
                    SelectedCompany = Convert.ToInt64(utility.AnalysisRepository().GetAnalyses().Where(Function(b) b.AnalysisId = UserInfo.AnalysisId).Select(Function(a) a.CompanyId).FirstOrDefault())
                End If

                If (Not Session("SelectedCompanyFromDropdown") Is Nothing) Then
                    SelectedCompany = Session("SelectedCompanyFromDropdown").ToString()
                End If

            End If

            Dim data As SelectList = utility.PouplateCompanyList(UserType, UserInfo, CustomerId, SelectedCompany)

            Return Json(data, JsonRequestBehavior.AllowGet)

        End Function

        <CustomActionFilter()>
        <AcceptVerbs(HttpVerbs.Get)>
        Public Function GetAnalyses(Optional selectedCompany As Integer = Nothing, Optional source As String = Nothing) As JsonResult
            Dim msg = Nothing
            Dim selectedAnalysis As Integer
            Try
                If (Not Session("UserType") Is Nothing) And (Not Session("UserInfo") Is Nothing) Then
                    UserType = DirectCast(Session("UserType"), UserRole)
                    UserInfo = DirectCast(Session("UserInfo"), User)
                    CustomerId = If(UserType.UserRoleId = UserRoles.SAU, UserInfo.CustomerId, Nothing)

                    UserInfo.selectedCompany = selectedCompany
                    UserInfo.fiscalMonthOfSelectedCompany = utility.CompaniesRepository.GetCompanyByID(selectedCompany).FiscalMonthStart
                    Session("UserInfo") = UserInfo

                    'When user logged in, it will show the analysis list based on analysisId
                    'If (selectedAnalysis = 0) Then
                    selectedAnalysis = UserInfo.AnalysisId
                    'End If

                    If (selectedCompany > 0) Then
                        Session("SelectedCompanyFromDropdown") = selectedCompany
                    End If

                End If

                Dim data As SelectList = utility.PouplateAnalysisList(UserType, UserInfo, CustomerId, selectedCompany, selectedAnalysis)

                If (data.Count = 0) Then
                    Session("SelectedAnalysisFromDropdown") = -1
                Else

                    Dim isExists As Boolean = data.Where(Function(a) a.Value = UserInfo.AnalysisId.ToString()).Any()

                    Session("SelectedAnalysisFromDropdown") = IIf(isExists = True, UserInfo.AnalysisId, 0)

                End If

                If (source <> "load" And source <> "user") Then
                    Session("action") = "GetAnalyses"
                    msg = New With {.result = "success", .data = data, .urlReferrer = Request.UrlReferrer.ToString()}
                Else
                    msg = New With {.result = "success", .data = data}
                End If

            Catch ex As Exception
                msg = New With {.result = "error", .errorMsg = ex.Message.ToString()}
            Finally

            End Try
            Return Json(msg, JsonRequestBehavior.AllowGet)

        End Function

        <AcceptVerbs(HttpVerbs.Get)> _
        <CustomActionFilter()>
        Public Function SetDefaultAnalysis(Optional SelectedAnalysis As Integer = Nothing) As JsonResult
            Dim msg As New Hashtable()
            Try
                If (Not Session("UserType") Is Nothing) And (Not Session("UserInfo") Is Nothing) Then
                    UserType = DirectCast(Session("UserType"), UserRole)
                    UserInfo = DirectCast(Session("UserInfo"), User)

                    If (SelectedAnalysis > 0) And (SelectedAnalysis <> UserInfo.AnalysisId) Then

                        Dim user = utility.UserRepository.GetUserByID(UserInfo.UserId)
                        user.AnalysisId = SelectedAnalysis
                        utility.UserRepository.Update(user)
                        utility.UserRepository.Save()

                        UserInfo.AnalysisId = SelectedAnalysis
                        Session("UserInfo") = UserInfo
                        Session("action") = "SetDefaultAnalysis"

                        msg.Add("Result", "success")
                        msg.Add("UrlReferrer", Request.UrlReferrer)
                    End If
                End If
            Catch ex As Exception
                msg.Add("Result", "error")
                msg.Add("error", ex.Message.ToString())
            End Try

            Return Json(msg, JsonRequestBehavior.AllowGet)
        End Function

        <MvcSpreadEvent("Load", "spdScorecard", DirectCast(Nothing, String()))> _
        Private Sub FpSpread_Load(sender As Object, e As EventArgs)
            Dim spread As FpSpread = DirectCast(sender, FpSpread)
            Dim decChartAmts(0, 12) As Decimal
            Dim decSaveAmts(12) As Decimal
            Dim strXLabels(12) As String
            Dim intCurrentMonth As Integer
            '  Dim intPeriods As Integer
            Dim intRowCounter As Integer
            Dim intFirstNumberColumn As Integer = 6
            Dim intNumberColCount As Integer
            Dim percentcell As New FarPoint.Web.Spread.PercentCellType
            Dim deccell As New FarPoint.Web.Spread.DoubleCellType
            Dim intSign As Integer 

          
            blnArchBudget = DirectCast(Session("BudgetArch"), Boolean)
            'It will get the records which match with selected analysis & Scorecard items selected by Logged-in user.
            'Dim dashboardItems = utility.DashboardRepository.GetDashboardDetail.Where(Function(d) d.UserId = UserInfo.UserId And d.AnalysisId = UserInfo.AnalysisId And d.IsValid = True)
            Logger.Log.Info(String.Format("Scorecard FpSpread_Load- scorecardItems Query Execution Begin"))
            Dim scorecardItems = (From d In utility.ScorecardRepository.GetScorecardDetail(UserInfo.AnalysisId, True)
                                   Join a In utility.AccountRepository.GetAccountRecordsByAnalysisId(UserInfo.AnalysisId) On a.AccountId Equals d.AccountId And a.AnalysisId Equals d.AnalysisId
                                   Join at In utility.AccountTypeRepository.GetAccountTypeRecordsByAnalysisId(UserInfo.AnalysisId) On at.AcctTypeId Equals a.AcctTypeId And at.AnalysisId Equals a.AnalysisId
                                   Select New With {d.ScorecardId, d.Order, d.AccountId, d.AnalysisId, a.AcctDescriptor, d.ScorecardDesc, _
                                                    d.Missed, d.Outperform, d.ShowasPercent, d.VarIsFavorable, d.Option1, d.Option2, d.Option3, d.IsValid, at.TypeDesc, a.NumberFormat}
                                   ).OrderBy(Function(d) d.Order).ToList()

            Logger.Log.Info(String.Format("Scorecard FpSpread_Load - scorecardItems Query Execution End"))

            Logger.Log.Info(String.Format("Scorecard:FpSpread_Load --> Balance Query execution Starts"))
            Dim balanceScorecard = (From b In utility.BalanceRepository.GetBalanceRecordsByAnalysisId(intAnalysisID)
                            Join di In scorecardItems On b.AccountId Equals di.AccountId And di.AnalysisId Equals b.AnalysisId
                            Select b).ToList()

            Logger.Log.Info(String.Format("Scorecard:FpSpread_Load --> Balance Query execution Starts"))
            Dim intCount As Integer = scorecardItems.Count
            If (Not scorecardItems Is Nothing) Then
                intCurrentMonth = intSelPeriod
                SetScorecardSpreadProperties(spread, intCurrentMonth)
                intNumberColCount = spread.Sheets(0).ColumnCount - intFirstNumberColumn
                For Each account In scorecardItems
                    Dim accountId As Integer = account.AccountId
                    Dim analysisId As Integer = account.AnalysisId
                    Dim blnShowasPercent As Boolean = account.ShowasPercent
                    Dim blnPercentLabel As Boolean = account.ShowasPercent
                    Dim decPeriodAmts(11) As Decimal
                    Dim decTotalBalances(12, 1) As Decimal
                    Dim decThreshold(1) As Decimal
                    Dim strYearType(5) As String
                    Dim intLoopCtr As Integer
                    Dim blnShowTotalCol As Boolean
                    Dim intOrder As Integer = account.Order
                    Dim intBalCtr As Integer = -1
                    Dim strAppendLabel As String = ""
                    Dim strBrowser = Request.Browser.Browser

                    Dim strScorecardDesc As String = account.ScorecardDesc & " score"
                    Array.Clear(decChartAmts, 0, 12)


                    decThreshold(0) = account.Outperform / 100
                    decThreshold(1) = (account.Missed / 100) * -1
                    '     If blnShowasPercent = True Then
                    'get TypeDesc for the account used for this scorecard item
                    Select Case account.TypeDesc.ToLower()
                        Case "assets", "liabilities and equity"                        '      
                            'get balances for account with AcctDescriptor of "ATotal"

                            Logger.Log.Info(String.Format("Scorecard FpSpread_Load - Case : assets, liabilities and equity - balanceAccum Query Execution Begin"))
                            Dim balanceAccum = (From b In utility.BalanceRepository.GetBalanceRecordsByAnalysisId(analysisId)
                                                  Join a In utility.AccountRepository.GetAccountRecordsByAnalysisId(analysisId) On a.AccountId Equals b.AccountId And a.AnalysisId Equals b.AnalysisId
                                                  Select b).ToList()
                            Logger.Log.Info(String.Format("Scorecard FpSpread_Load - Case : assets, liabilities and equity - balanceAccum Query Execution Begin"))

                            Logger.Log.Info(String.Format("Scorecard FpSpread_Load - Case : assets, liabilities and equity - balanceAccum For Each Execution Begin"))
                            For Each balance As Balance In (From b In balanceAccum)
                                StoreTotalBal(balance, decTotalBalances, intSelPeriod)
                                Exit For
                            Next
                            Logger.Log.Info(String.Format("Scorecard FpSpread_Load - Case : assets, liabilities and equity - balanceAccum For Each Execution End"))
                        Case "revenue", "expense"

                            Logger.Log.Info(String.Format("Scorecard FpSpread_Load - Case : revenue, expense - balanceAccum Query Execution Begin"))
                            Dim balanceAccum = (From b In utility.BalanceRepository.GetBalanceRecordsByAnalysisId(analysisId)
                                                 Join a In utility.AccountRepository.GetAccountRecordsByAnalysisId(analysisId) On a.AccountId Equals b.AccountId And a.AnalysisId Equals b.AnalysisId
                                                 Join at In utility.AccountTypeRepository.GetAccountTypeRecordsByAnalysisId(analysisId, New String() {"revenue"}) On at.AcctTypeId Equals a.AcctTypeId And at.AnalysisId Equals a.AnalysisId
                                                 Select b).ToList()
                            Logger.Log.Info(String.Format("Scorecard FpSpread_Load - Case : revenue, expense - balanceAccum Query Execution Begin"))

                            Dim count As Integer = balanceAccum.Count
                            Logger.Log.Info(String.Format("Scorecard FpSpread_Load - Case : revenue, expense - balanceAccum For Each Execution Begin"))
                            For Each balance As Balance In (From b In balanceAccum)
                                StoreTotalBal(balance, decTotalBalances, intSelPeriod)

                            Next
                            Logger.Log.Info(String.Format("Scorecard FpSpread_Load - Case : revenue, expense - balanceAccum For Each Execution End"))
                        Case Else
                            blnShowasPercent = False
                    End Select
                    '    End If
                    spread.Sheets(0).SetValue(intRowCounter, 5, account.AccountId)
                    Dim decValue As Decimal = decTotalBalances(0, 0)
                    For Each balance As Balance In (From b In balanceScorecard.Where(Function(a) a.AccountId = accountId And a.AnalysisId = intAnalysisID) Select b)
                        Select Case account.TypeDesc.ToLower()
                            Case "revenue"
                                blnShowTotalCol = True
                                If account.ShowasPercent = True Then
                                    If account.VarIsFavorable = True Then
                                        intSign = 1
                                    Else
                                        intSign = -1
                                    End If
                                Else
                                    intSign = 1
                                End If
                                If account.ShowasPercent = True Then
                                    strScorecardDesc = strScorecardDesc & " (as a % of total revenue)"
                                End If
                            Case "expense"
                                blnShowTotalCol = True
                                Select Case account.AcctDescriptor
                                    Case "GPTotal", "IOTotal", "IBTTotal", "NITotal"
                                        If account.ShowasPercent = True Then
                                            If account.VarIsFavorable = True Then
                                                intSign = 1
                                            Else
                                                intSign = -1
                                            End If
                                        Else
                                            intSign = 1
                                        End If
                                        ' intSign = 1
                                    Case Else
                                        If account.ShowasPercent = True Then
                                            If account.VarIsFavorable = True Then
                                                intSign = 1
                                            Else
                                                intSign = -1
                                            End If
                                        Else
                                            intSign = -1
                                        End If
                                End Select
                                If account.ShowasPercent = True Then
                                    strScorecardDesc = strScorecardDesc & " (as a % of total revenue)"
                                End If
                            Case "assets"
                                blnShowTotalCol = False
                                If account.ShowasPercent = True Then
                                    If account.VarIsFavorable = True Then
                                        intSign = 1
                                    Else
                                        intSign = -1
                                    End If
                                Else
                                    intSign = 1
                                End If
                                If account.ShowasPercent = True Then
                                    strScorecardDesc = strScorecardDesc & " (as a % of total assets)"
                                End If
                            Case "liabilities and equity"
                                blnShowTotalCol = False
                                If account.ShowasPercent = True Then
                                    If account.VarIsFavorable = True Then
                                        intSign = 1
                                    Else
                                        intSign = -1
                                    End If
                                Else
                                    intSign = -1
                                End If
                                If account.ShowasPercent = True Then
                                    strScorecardDesc = strScorecardDesc & " (as a % of total assets)"
                                End If
                            Case Else
                                blnShowTotalCol = False
                                If account.VarIsFavorable = False Then
                                    intSign = -1
                                Else
                                    intSign = 1
                                End If

                        End Select
                        ''add 3 rows for each scorecard item
                        For intLoopCtr = 0 To 2
                            Select Case intLoopCtr
                                Case 2
                                    CommonProcedures.AddRow(spread, intRowCounter, strScorecardDesc, "Score", account.TypeDesc, strBrowser, 4)
                                    With spread.Sheets(0)
                                        .Cells(intRowCounter, 1).HorizontalAlign = HorizontalAlign.Center
                                    End With
                                    spread.Sheets(0).Cells(intRowCounter, intFirstNumberColumn, intRowCounter, intFirstNumberColumn + intNumberColCount - 1).CellType = percentcell
                                    spread.Sheets(0).SetValue(intRowCounter, 1, "+")
                                    'compute % variance
                                    For intColumnCounter = 0 To intNumberColCount - 1
                                        For intBalCtr = 0 To 1
                                            decPeriodAmts(0) = spread.Sheets(0).GetValue(intRowCounter - 2, intFirstNumberColumn + intColumnCounter)
                                            decPeriodAmts(1) = spread.Sheets(0).GetValue(intRowCounter - 1, intFirstNumberColumn + intColumnCounter)
                                        Next
                                        decPeriodAmts(2) = decPeriodAmts(1) - decPeriodAmts(0)
                                        Try
                                            If decPeriodAmts(0) < 0 Then
                                                decPeriodAmts(3) = (decPeriodAmts(2) / decPeriodAmts(0) * -1) * intSign
                                            Else
                                                decPeriodAmts(3) = (decPeriodAmts(2) / decPeriodAmts(0)) * intSign
                                            End If

                                        Catch ex As Exception
                                            decPeriodAmts(3) = 0
                                        End Try
                                        If intColumnCounter = intNumberColCount - 1 Then
                                            If blnShowTotalCol = True Then
                                                spread.Sheets(0).SetValue(intRowCounter, intFirstNumberColumn + intColumnCounter, decPeriodAmts(3))
                                                Select Case decPeriodAmts(3)
                                                    Case Is > decThreshold(0)
                                                        spread.Sheets(0).Cells(intRowCounter, intFirstNumberColumn + intColumnCounter).BackColor = Drawing.Color.LightGreen
                                                    Case Is < decThreshold(1)
                                                        spread.Sheets(0).Cells(intRowCounter, intFirstNumberColumn + intColumnCounter).BackColor = Drawing.Color.FromArgb(254, 154, 154)
                                                    Case Else
                                                        spread.Sheets(0).Cells(intRowCounter, intFirstNumberColumn + intColumnCounter).BackColor = Drawing.Color.LightBlue
                                                End Select
                                            Else
                                                spread.Sheets(0).Cells(intRowCounter, intFirstNumberColumn + intCurrentMonth + 1).BackColor = Drawing.Color.LightGray
                                            End If
                                        Else
                                            spread.Sheets(0).SetValue(intRowCounter, intFirstNumberColumn + intColumnCounter, decPeriodAmts(3))
                                            Select Case decPeriodAmts(3)
                                                Case Is > decThreshold(0)
                                                    spread.Sheets(0).Cells(intRowCounter, intFirstNumberColumn + intColumnCounter).BackColor = Drawing.Color.LightGreen
                                                Case Is < decThreshold(1)
                                                    spread.Sheets(0).Cells(intRowCounter, intFirstNumberColumn + intColumnCounter).BackColor = Drawing.Color.FromArgb(254, 154, 154)
                                                Case Else
                                                    spread.Sheets(0).Cells(intRowCounter, intFirstNumberColumn + intColumnCounter).BackColor = Drawing.Color.LightBlue
                                            End Select
                                        End If
                                    Next
                                Case Else

                                    Select Case intLoopCtr
                                        Case 0
                                            Array.Clear(decPeriodAmts, 0, 12)
                                            Array.Clear(decSaveAmts, 0, 13)
                                            CommonProcedures.AddRow(spread, intRowCounter, "Budget", "SDetail", account.TypeDesc, strBrowser, 4)
                                            If blnArchBudget = False Then
                                                GetBudgetAmts(balance, decPeriodAmts)
                                            Else
                                                GetArchivedBudgetAmts(balance, decPeriodAmts)
                                            End If

                                        Case 1
                                            Array.Clear(decPeriodAmts, 0, 12)
                                            Array.Clear(decSaveAmts, 0, 13)
                                            CommonProcedures.AddRow(spread, intRowCounter, "Actual", "SDetail", account.TypeDesc, strBrowser, 4)
                                            GetActualAmts(balance, decPeriodAmts)
                                    End Select
                                    Select Case account.TypeDesc
                                        Case "Ratio", "NonFinancial"
                                            Select Case account.NumberFormat
                                                Case 1 To 5
                                                    deccell.DecimalDigits = account.NumberFormat
                                                    spread.Sheets(0).Cells(intRowCounter, intFirstNumberColumn, intRowCounter, intFirstNumberColumn + intCurrentMonth).CellType = deccell
                                                Case 99
                                                    spread.Sheets(0).Cells(intRowCounter, intFirstNumberColumn, intRowCounter, intFirstNumberColumn + intCurrentMonth).CellType = percentcell
                                            End Select
                                    End Select
                                    If blnShowasPercent = False Then
                                        For intColumnCounter = 0 To intCurrentMonth
                                            spread.Sheets(0).SetValue(intRowCounter, intFirstNumberColumn + intColumnCounter, decPeriodAmts(intColumnCounter))
                                            decSaveAmts(intColumnCounter) = decPeriodAmts(intColumnCounter)
                                            decSaveAmts(intCurrentMonth + 1) += decPeriodAmts(intColumnCounter)
                                        Next
                                    Else
                                        spread.Sheets(0).Cells(intRowCounter, intFirstNumberColumn, intRowCounter, intFirstNumberColumn + intNumberColCount - 1).CellType = percentcell
                                        For intColumnCounter = 0 To intCurrentMonth
                                            If decTotalBalances(intColumnCounter, intLoopCtr) <> 0 Then
                                                spread.Sheets(0).SetValue(intRowCounter, intFirstNumberColumn + intColumnCounter, decPeriodAmts(intColumnCounter) / decTotalBalances(intColumnCounter, intLoopCtr))
                                                decSaveAmts(intColumnCounter) = decPeriodAmts(intColumnCounter) ' / decAccumTotals(intColumnCounter)
                                            Else
                                                spread.Sheets(0).SetValue(intRowCounter, intFirstNumberColumn + intColumnCounter, 0)
                                                decSaveAmts(intColumnCounter) = 0
                                            End If
                                            If intColumnCounter = 0 Then
                                                decTotalBalances(intCurrentMonth + 1, intLoopCtr) = decTotalBalances(intColumnCounter, intLoopCtr)
                                            Else
                                                decTotalBalances(intCurrentMonth + 1, intLoopCtr) += decTotalBalances(intColumnCounter, intLoopCtr)
                                            End If
                                            decSaveAmts(intCurrentMonth + 1) += decPeriodAmts(intColumnCounter)
                                        Next
                                    End If
                                    If blnShowTotalCol = True Then
                                        'save amounts for total column
                                        If blnShowasPercent = False Then
                                            spread.Sheets(0).SetValue(intRowCounter, intFirstNumberColumn + intCurrentMonth + 1, decSaveAmts(intCurrentMonth + 1))
                                        Else
                                            If decTotalBalances(intCurrentMonth + 1, intLoopCtr) <> 0 Then
                                                spread.Sheets(0).SetValue(intRowCounter, intFirstNumberColumn + intCurrentMonth + 1, decSaveAmts(intCurrentMonth + 1) / decTotalBalances(intCurrentMonth + 1, intLoopCtr))
                                            Else
                                                spread.Sheets(0).SetValue(intRowCounter, intFirstNumberColumn + intCurrentMonth + 1, 0)
                                            End If
                                        End If
                                    Else

                                        spread.Sheets(0).Cells(intRowCounter, intFirstNumberColumn + intCurrentMonth + 1).BackColor = Drawing.Color.LightGray
                                    End If
                            End Select


                        Next

                        Exit For
                    Next
                    ''    End If

                    Logger.Log.Info(String.Format("Scorecard FpSpread_Load - Balance For Each Execution Begin"))

                    Logger.Log.Info(String.Format("Scorecard FpSpread_Load - Balance For Each Execution End"))



                Next
            End If

        End Sub

        
        Private Sub StoreTotalBal(ByVal balance As Balance, ByRef decTotalBalances As Array, ByVal intPeriod As Integer)
            Logger.Log.Info(String.Format("Scorecard FpSpread_Load - StoreTotalBal Execution Begin"))
            Dim decPeriodAmts(11) As Decimal
            Dim intCtr As Integer

            If blnArchBudget = False Then
                GetBudgetAmts(balance, decPeriodAmts)
            Else
                GetArchivedBudgetAmts(balance, decPeriodAmts)
            End If

            For intCtr = 0 To 11
                decTotalBalances(intCtr, 0) = decPeriodAmts(intCtr)
            Next
            GetActualAmts(balance, decPeriodAmts)
            For intCtr = 0 To 11
                decTotalBalances(intCtr, 1) = decPeriodAmts(intCtr)
            Next

            Logger.Log.Info(String.Format("Scorecard FpSpread_Load - StoreTotalBal Execution End"))
        End Sub
        Private Function ComputePercent(ByVal decItemAmt As Decimal, ByVal decTotalAmt As Decimal) As Decimal
            Logger.Log.Info(String.Format("Scorecard FpSpread_Load - ComputePercent Execution Begin"))
            Try
                ComputePercent = (decItemAmt / decTotalAmt) '* 100
            Catch ex As Exception
                ComputePercent = 0
            Finally
                Logger.Log.Info(String.Format("Scorecard FpSpread_Load - ComputePercent Execution End"))
            End Try
        End Function

        Private Function ComputeTotal(ByVal intSelPeriod As Integer, ByVal decTotalBalances() As Decimal) As Decimal
            Logger.Log.Info(String.Format("Scorecard FpSpread_Load - ComputeTotal Execution Begin"))
            Dim intCtr As Integer

            ComputeTotal = 0
            For intCtr = 0 To intSelPeriod
                ComputeTotal += decTotalBalances(intCtr)
            Next
            Logger.Log.Info(String.Format("Scorecard FpSpread_Load - ComputeTotal Execution End"))
        End Function

        <AcceptVerbs(HttpVerbs.Get)> _
        <CustomActionFilter()>
        Public Function GetSavedReports(ByVal SelectedAnalysisId As Integer) As JsonResult

            If (Not Session("UserType") Is Nothing) And (Not Session("UserInfo") Is Nothing) Then
                UserType = DirectCast(Session("UserType"), UserRole)
                UserInfo = DirectCast(Session("UserInfo"), User)
                CustomerId = If(UserType.UserRoleId = UserRoles.SAU, UserInfo.CustomerId, Nothing)

                'When user logged in, it will show the company list based on analysisId               

                If (Not Session("SelectedAnalysisFromDropdown") Is Nothing) Then
                    SelectedAnalysisId = Session("SelectedAnalysisFromDropdown").ToString()
                End If

            End If

            Dim data As SelectList = utility.PouplateSavedReports(SelectedAnalysisId)
            Return Json(data, JsonRequestBehavior.AllowGet)

        End Function

        Private Sub SetScorecardSpreadProperties(ByVal spdScorecard As FpSpread, ByVal intPeriod As Integer)
            Try
                Logger.Log.Info(String.Format("ScorecardController:SetSpreadProperties method execution Starts"))
                Dim intColCount As Integer = 0
                Dim intNumberColumn As Integer = 0
                Dim intPercentColumn As Integer = 0
                Dim intFirstNumberColumn As Integer = 6

                'set column count
                intNumberColumn = intPeriod + 2
                intPercentColumn = intPeriod + 2
                intColCount = intFirstNumberColumn + intNumberColumn
                CommonProcedures.SetCommonProperties(spdScorecard, intColCount, False, intFirstNumberColumn, intNumberColumn, 5, "ALC")
                With spdScorecard.Sheets(0)
                    .Columns(-1).Resizable = False
                    .Columns(0).Width = 0
                    .GridLineColor = Drawing.Color.DarkGray
                End With
                spdScorecard.VerticalScrollBarPolicy = FarPoint.Web.Spread.ScrollBarPolicy.Never
                spdScorecard.HorizontalScrollBarPolicy = FarPoint.Web.Spread.ScrollBarPolicy.Never

                SetScorecardColumnWidths(spdScorecard, intColCount, intFirstNumberColumn, 5, intNumberColumn, False, 4)
                With spdScorecard.Sheets(0)
                    .Columns(-1).Resizable = False
                    .Columns(0).Width = 0
                    .GridLineColor = Drawing.Color.DarkGray
                End With
                'load column headers
                CommonProcedures.SetScorecardColumnHeader(spdScorecard, intPeriod, intColCount, intFirstNumberColumn, intFirstFiscal, True)

                spdScorecard.Height = 1320


                Logger.Log.Info(String.Format("ScorecardController:SetSpreadProperties method execution Ends"))
            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("SetSpreadProperties : Error occured while processing Revenue & Expense Chart -", ex.Message)
                Logger.Log.Error(String.Format("ScorecardController:SetSpreadProperties method execution ends with Error message: {0}, StackTrace: {1}", ex.Message, ex.StackTrace))
            End Try

        End Sub

        Public Sub SetScorecardColumnWidths(ByRef spdAnalytics As FpSpread, ByVal intColCount As Integer, ByVal intFirstNumberColumn As Integer, ByVal intFormat As Integer, ByVal intNumberColumn As Integer, ByVal blnSTColVisible As Boolean, ByVal intDescCol As Integer)
            Dim intColWidth As Integer
            Dim intDescColWidth As Integer
            Dim intSpdWidth As Integer
            Dim intScrollBarWidth = 18
            Dim intAddWidth As Integer = 44 
            If blnSTColVisible = True Then
                intAddWidth = 66
            End If
            Try
                With spdAnalytics.Sheets(0)
                    Logger.Log.Info(String.Format("SetColumnWidths method execution Starts"))
                    .Columns(0, 2).Width = 22
                    .Columns(3).Width = 0
                    If intFirstNumberColumn = 6 Then
                        .Columns(5).Width = 0
                    End If
                    Select Case intNumberColumn
                        Case Is <= 4
                            intDescColWidth = 250
                            intColWidth = 85
                        Case 5 To 8
                            intDescColWidth = 200
                            intColWidth = Math.Round((780 - 200) / intNumberColumn, 0)
                            If intColWidth > 85 Then
                                intColWidth = 85
                            End If
                        Case Else
                            intDescColWidth = 150
                            intColWidth = 65
                    End Select

                    'Select Case intNumberColumn
                    '    Case Is <= 4
                    '        intDescColWidth = 300
                    '        intColWidth = 105
                    '    Case 5 To 7
                    '        intDescColWidth = 250
                    '        intColWidth = Math.Round((1000 - 300) / intNumberColumn, 0)
                    '        If intColWidth > 90 Then
                    '            intColWidth = 90
                    '        End If
                    '    Case Else
                    '        intDescColWidth = 250
                    '        intColWidth = Math.Round((1000 - 250) / intNumberColumn, 0)
                    'End Select
                    .Columns(intDescCol).Width = intDescColWidth + 22
                    .Columns(intFirstNumberColumn, intColCount - 1).Width = intColWidth
                    intSpdWidth = intAddWidth + intDescColWidth + (intColWidth * intNumberColumn) + intScrollBarWidth
                    

                    If spdAnalytics.VerticalScrollBarPolicy = FarPoint.Web.Spread.ScrollBarPolicy.Never Then
                        spdAnalytics.Width = intSpdWidth - 14
                    Else
                        spdAnalytics.Width = intSpdWidth

                    End If
                    'hide last row
                    .Rows(.RowCount - 1).Visible = False
                    'hide checkbox col
                    .Columns(2).Width = 0
                    .ActiveColumn = 1
                    .ActiveRow = 1 
                    Logger.Log.Info(String.Format("SetColumnWidths method execution Ends"))
                End With

            Catch ex As Exception
                Logger.Log.Error(String.Format("SetColumnWidths method execution ends with Error message: {0}, StackTrace: {1}", ex.Message, ex.StackTrace))
            End Try
        End Sub

    End Class



End Namespace