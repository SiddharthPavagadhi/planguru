﻿Imports System.Collections.Generic
Imports System.Linq
Imports System.Data

Public Class ChartFormatRepository
    Inherits GenericRepository(Of ChartFormat)
    Implements IChartFormatRepository

    'Implements IDisposable
    Private disposed As Boolean = False

    Public Sub New(context As DataAccess)
        MyBase.New(context)
    End Sub

    Public Function GetChartFormats() As IEnumerable(Of ChartFormat) Implements IChartFormatRepository.GetChartFormats
        Return context.ChartFormats.ToList()
    End Function

    Public Function GetChartFormatById(ChartFormatId As Integer) As ChartFormat Implements IChartFormatRepository.GetChartFormatById
        Return context.ChartFormats.Find(ChartFormatId)
    End Function

    Public Sub Save() Implements IChartFormatRepository.Save
        context.SaveChanges()
    End Sub
    Protected Overridable Sub Dispose(disposing As Boolean)
        If Not Me.disposed Then
            If disposing Then
                context.Dispose()
            End If
        End If
        Me.disposed = True
    End Sub

    Public Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
End Class
