﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Customer
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.btnExportData = New System.Windows.Forms.Button()
        Me.lblError = New System.Windows.Forms.Label()
        Me.txtCustomerId = New System.Windows.Forms.TextBox()
        Me.lblCustomerId = New System.Windows.Forms.Label()
        Me.CustErrorProvider = New System.Windows.Forms.ErrorProvider(Me.components)
        CType(Me.CustErrorProvider, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnExportData
        '
        Me.btnExportData.Location = New System.Drawing.Point(203, 76)
        Me.btnExportData.Name = "btnExportData"
        Me.btnExportData.Size = New System.Drawing.Size(75, 23)
        Me.btnExportData.TabIndex = 0
        Me.btnExportData.Text = "Export Data"
        Me.btnExportData.UseVisualStyleBackColor = True
        '
        'lblError
        '
        Me.lblError.AutoSize = True
        Me.lblError.ForeColor = System.Drawing.Color.Red
        Me.lblError.Location = New System.Drawing.Point(27, 115)
        Me.lblError.MaximumSize = New System.Drawing.Size(300, 0)
        Me.lblError.Name = "lblError"
        Me.lblError.Size = New System.Drawing.Size(0, 13)
        Me.lblError.TabIndex = 1
        '
        'txtCustomerId
        '
        Me.txtCustomerId.Location = New System.Drawing.Point(110, 36)
        Me.txtCustomerId.Name = "txtCustomerId"
        Me.txtCustomerId.Size = New System.Drawing.Size(168, 20)
        Me.txtCustomerId.TabIndex = 2
        Me.txtCustomerId.Tag = "Please provide Customer ID."
        '
        'lblCustomerId
        '
        Me.lblCustomerId.AutoSize = True
        Me.lblCustomerId.Location = New System.Drawing.Point(27, 39)
        Me.lblCustomerId.Name = "lblCustomerId"
        Me.lblCustomerId.Size = New System.Drawing.Size(68, 13)
        Me.lblCustomerId.TabIndex = 3
        Me.lblCustomerId.Text = "Customer ID:"
        '
        'CustErrorProvider
        '
        Me.CustErrorProvider.ContainerControl = Me
        '
        'Customer
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(346, 179)
        Me.Controls.Add(Me.lblCustomerId)
        Me.Controls.Add(Me.txtCustomerId)
        Me.Controls.Add(Me.lblError)
        Me.Controls.Add(Me.btnExportData)
        Me.Name = "Customer"
        Me.Text = "Customer Info"
        CType(Me.CustErrorProvider, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnExportData As System.Windows.Forms.Button
    Friend WithEvents lblError As System.Windows.Forms.Label
    Friend WithEvents txtCustomerId As System.Windows.Forms.TextBox
    Friend WithEvents lblCustomerId As System.Windows.Forms.Label
    Friend WithEvents CustErrorProvider As System.Windows.Forms.ErrorProvider

End Class
