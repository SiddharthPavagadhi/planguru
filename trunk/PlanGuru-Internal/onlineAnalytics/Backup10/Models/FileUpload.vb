﻿Imports System.ComponentModel.DataAnnotations
Imports System.Data.Entity
Imports System.Collections.Generic

<Table("FileUploadedResult")>
Public Class FileUploadedResult

    <Key()> _
    <DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)> _
    Public Property FileId() As Integer

    Public Property TableId() As Integer

    Public Property SuccessCount() As Integer

    Public Property FailCount() As Integer

    Public Property TotalCount() As Integer

    Public Property FileProcessId() As Integer

End Class

<Table("FileProcess")>
Public Class FileProcess

    <Key()> _
    <DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)> _
    Public Property FileProcessId() As Integer

    <StringLength(100)> _
    Public Property FileName() As String

    Public Property AnalysisId() As Integer

    Public Property UserId() As Integer

    Public Property UploadedDate() As DateTime

    Public Property CompanyId() As Integer

End Class