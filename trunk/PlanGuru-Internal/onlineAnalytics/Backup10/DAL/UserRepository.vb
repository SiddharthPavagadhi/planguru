﻿Imports System.Collections.Generic
Imports System.Linq
Imports System.Data
Public Class UserRepository
    Inherits GenericRepository(Of User)
    Implements IUserRepository
    Implements IDisposable
    'Private context As DataAccess
    Private disposed As Boolean = False

    Public Sub New(context As DataAccess)
        'Me.context = context
        MyBase.New(context)
    End Sub

    Public Function ValidateUserAccount(UserName As String, Password As String) As User Implements IUserRepository.ValidateUserAccount

        Return (From user In context.Users.Where(Function(u) u.UserName.Equals(UserName) And u.Password.Equals(Password)) Select user).SingleOrDefault()

    End Function
    Public Function UsernameAvailibility(UserName As String) As IQueryable(Of User) Implements IUserRepository.UsernameAvailibility

        Return (From user In context.Users.Where(Function(u) u.UserName.ToLower().Equals(UserName.ToLower()) And u.Status <= 2) Select user)

    End Function

    Public Function EmailAddressAvailibility(EmailAddress As String) As IQueryable(Of User) Implements IUserRepository.EmailAddressAvailibility

        Return (From user In context.Users.Where(Function(u) u.UserEmail.ToLower().Equals(EmailAddress.ToLower()) And u.Status <= 2) Select user)

    End Function
    'This GetCountryCodes Function will return list of Country code with Name.
    Public Function GetUsers() As IEnumerable(Of User) Implements IUserRepository.GetUsers
        Return context.Users.ToList()
    End Function

    Public Function GetUser(customerId As Integer, UserRoleId As Integer) As Integer Implements IUserRepository.GetUser
        Dim getUserList

        getUserList = context.Users.Where(Function(a) a.CustomerId = customerId And a.UserRoleId = UserRoleId).Select(Function(u) u.UserId).FirstOrDefault()

        Return getUserList
    End Function
    Public Function GetUsers(customerId As Integer, Optional userId As Integer = Nothing) As IEnumerable(Of User)
        Dim getUserList
        If (userId = Nothing) Then
            getUserList = (context.Users.Where(Function(a) a.CustomerId = customerId And (a.Status = StatusE.Active Or a.Status = StatusE.Pending)))
        Else
            getUserList = (context.Users.Where(Function(a) a.CustomerId = customerId And a.UserId <> userId And (a.Status = StatusE.Active Or a.Status = StatusE.Pending)))
        End If
        Return (getUserList)
    End Function

    Public Function PreceededUsers(customerId As Integer, userRoleId As Integer) As IEnumerable(Of User) Implements IUserRepository.PreceededUsers

        Dim preceeded_Users = Nothing

        If (customerId <> Nothing) Then
            preceeded_Users = (context.Users.Where(Function(a) a.CustomerId = customerId And a.UserRoleId < userRoleId And a.UserRoleId > UserRoles.PGASupport).ToList())
        End If

        Return (preceeded_Users)
    End Function

    Public Function GetUserByID(userId As Integer) As User Implements IUserRepository.GetUserById
        Return context.Users.Find(userId)
    End Function
    Public Sub UpdateUser(user As User) Implements IUserRepository.UpdateUser
        context.Entry(user).State = EntityState.Modified
    End Sub
    Public Sub DeleteUser(userId As Integer) Implements IUserRepository.DeleteUser
        Dim user As User = context.Users.Find(userId)
        context.Users.Remove(user)
    End Sub
    Public Sub DeleteNonSAUUser(userId As Integer) Implements IUserRepository.DeleteNonSAUUser
        Logger.Log.Info(("Start Execution of DeleteNonSAUUser"))
        Dim utility As New Utility
        Logger.Log.Info(("Start Query :Delete the UserAnalysisMapping For: " & userId))
        utility.UserAnalysisMappingRepository.DeleteUserAnalysisMappingByUserId(userId)
        utility.UserAnalysisMappingRepository.Save()
        Logger.Log.Info(("Start Query :Delete the UserCompanyMapping For: " & userId))
        utility.UserCompanyMappingRepository.DeleteUserCompanyMappingByUserId(userId)
        utility.UserCompanyMappingRepository.Save()
        Logger.Log.Info(("Start Query :Delete the Dashboard Items For: " & userId))
        utility.DashboardRepository.DeleteDashboardItemsByUserId(userId)
        Logger.Log.Info("End Execution of DeleteNonSAUUser")
    End Sub

    Public Sub Save() Implements IUserRepository.Save
        context.SaveChanges()
    End Sub
    Protected Overridable Sub Dispose(disposing As Boolean)
        If Not Me.disposed Then
            If disposing Then
                context.Dispose()
            End If
        End If
        Me.disposed = True
    End Sub

    Public Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

End Class
