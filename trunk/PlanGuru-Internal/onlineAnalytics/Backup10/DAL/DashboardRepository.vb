﻿Imports System.Collections.Generic
Imports System.Linq
Imports System.Data

Public Class DashboardRepository
    Inherits GenericRepository(Of Dashboard)
    Implements IDashboardRepository
    'Implements IDisposable
    Private disposed As Boolean = False

    Public Sub New(context As DataAccess)
        MyBase.New(context)
    End Sub
    Public Function GetDashboardDetail() As System.Collections.Generic.IEnumerable(Of Dashboard) Implements IDashboardRepository.GetDashboardDetail
        Return context.Dashboards.ToList()
    End Function

    Public Function GetDashboardDetail(userId As Integer, analysisId As Integer, isValid As Boolean) As System.Collections.Generic.IEnumerable(Of Dashboard) Implements IDashboardRepository.GetDashboardDetail
        Return context.Dashboards.Where(Function(d) d.UserId = userId And d.AnalysisId = analysisId And d.IsValid = isValid).ToList()
    End Function

    Public Function GetDashboardDetailById(DashboardId As Integer) As Dashboard Implements IDashboardRepository.GetDashboardDetailById
        Return context.Dashboards.Find(DashboardId)
    End Function

    Public Sub UpdateDashboardDetail(dashboardDetail As Dashboard) Implements IDashboardRepository.UpdateDashboardDetail
        context.Entry(dashboardDetail).State = EntityState.Modified
    End Sub

    Public Sub DeleteDashboardDetail(DashboardId As Integer) Implements IDashboardRepository.DeleteDashboardDetail
        Dim dashboard As Dashboard = context.Dashboards.Find(DashboardId)
        context.Dashboards.Remove(dashboard)
    End Sub

    Public Sub ReOrderDashboardItems(dashboardItem As Integer, UserId As Integer) Implements IDashboardRepository.ReOrderDashboardItems

    End Sub

    Public Function GetDashboardItemCount(analysisId As Integer, userId As Integer) As Integer Implements IDashboardRepository.GetDashboardItemCount
        Return context.Dashboards.Where(Function(d) d.AnalysisId = analysisId And d.UserId = userId).Count()
    End Function

    Public Function GetDashboardSharedItemCount(analysisId As Integer) As Integer Implements IDashboardRepository.GetDashboardSharedItemCount
        Return context.Dashboards.Where(Function(d) d.AnalysisId = analysisId And d.UserId = "-999").Count()
    End Function

    Public Sub Save() Implements IDashboardRepository.Save
        context.SaveChanges()
    End Sub

    Protected Overridable Sub Dispose(disposing As Boolean)
        If Not Me.disposed Then
            If disposing Then
                context.Dispose()
            End If
        End If
        Me.disposed = True
    End Sub

    Public Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Public Function DeleteDashboardItemsByAnalysis(analysisId As Integer) Implements IDashboardRepository.DeleteDashboardItemsByAnalysis
        Logger.Log.Info(("Start Query : Get the list of Accounts by AnalysisId: " & analysisId))
        Dim dashboard = (From d In context.Dashboards.Where(Function(a) a.AnalysisId = analysisId)
                                       Select d)
        If Not (dashboard Is Nothing And dashboard.Count > 0) Then
            Logger.Log.Info(("Dashboard items count : " & dashboard.Count))
            Logger.Log.Info("Start deleting : Dashboard items")
            dashboard.ToList().ForEach(Sub(item) context.Dashboards.Remove(item))
            context.SaveChanges()
            Logger.Log.Info(("End deleting : Dashboard items"))
        End If
        Return dashboard.Count
    End Function

    Public Function DeleteDashboardItemsByUserId(userId As Integer) Implements IDashboardRepository.DeleteDashboardItemsByUserId
        Logger.Log.Info(("Start Query : Get the list of Dashboard items by UserId: " & userId))
        Dim dashboardItems = (From d In context.Dashboards.Where(Function(a) a.UserId = userId)
                                       Select d)
        If Not (dashboardItems Is Nothing) And dashboardItems.Count > 0 Then
            Logger.Log.Info(("Dashboard items count : " & dashboardItems.Count))
            Logger.Log.Info("Start deleting : Dashboard items")
            dashboardItems.ToList().ForEach(Sub(item) context.Dashboards.Remove(item))
            Logger.Log.Info(("End Deleting : Dashboard items"))
        End If
        Return dashboardItems.Count
    End Function
End Class
