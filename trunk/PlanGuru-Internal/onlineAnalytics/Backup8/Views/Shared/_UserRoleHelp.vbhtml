﻿@Imports onlineAnalytics

<div id="user_role_help_container" style="display: none; font-family: Arial;font-size: 14px;margin:0px 20px;">
    <br />
    <h3>PlanGuru Analytics User Roles</h3>
    <p style="margin: 15px 0px;">
        PlanGuru Analytics offers 3 different user types.</p>
    <ul style="margin: 0px 0px 0px 40px;">
        <li style="list-style: disc; padding: 0px 0px 0px 13px;"><b>SSU</b>
            <div style="margin: 0px 0px 0px 0px;text-align:justify;">
                The "Subscription Supervisor User" will have access to all company and analysis files created within your PlanGuru Analytics subscription.
                This role is helpful if you want to share administrative responsibilities with others. 
                Do not give this role out to users who should be restricted from seeing certain analysis files.</div>
        </li>
        <br />
        <li style="list-style: disc; padding: 0px 0px 0px 13px;"><b>CAU</b>
            <div style="margin: 0px 0px 0px 0px;text-align:justify;">
                The "Company Administrative User" gives the user elevated permissions over the regular user, including the ability to add saved reports and edit the analysis level dashboard and scorecard.  Once associated with a Company the CAU can see all analysis files within that company.  This role is good for power users of PlanGuru Analytics, who you might still want to restrict from seeing all analysis files within the subscription.
            </div>
        </li>
        <br />
        <li style="list-style: disc; padding: 0px 0px 0px 13px;"><b>CRU</b>
            <div style="margin: 0px 0px 0px 0px;text-align:justify;">
                The "Company Regular User" is reserved for the most basic users of PlanGuru Analytics.  This user can view saved reports, shared dashboards and the scorecard features but they cannot edit them.  In addition these users only have access to the specific analysis files they're given permissions to.
            </div>
        </li>
        <br />
    </ul>   
    <br />    
</div>
