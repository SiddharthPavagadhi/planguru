﻿Public Class ReportHighChart
    Public Property isHttpPost() As Boolean

    Public Property SelectedRows() As ArrayList

    Public Property ChartType() As Integer

    Public Property NumbofCols() As Integer

    Public Property ChartValues() As Decimal(,)

    Public Property Format() As Integer

    Public Property ChartDesc() As String

    Public Property Controller() As String

    Public Property ShowasPerc() As Boolean

    Public Property StartCol() As Integer

    Public Property SeriesData() As List(Of SeriesData)

    Public Property SubLabels() As List(Of String)

    Public Property HighChartType() As String

    Public Property OptionalYAxisData() As List(Of Decimal)

    Public Property showTrend() As Boolean

    Public Property isThousand() As Boolean

    Public Property chartWidth() As String


End Class

Public Class SeriesData
    Public Property SeriesTitle() As String

    Public Property SeriesData() As List(Of String)
End Class
