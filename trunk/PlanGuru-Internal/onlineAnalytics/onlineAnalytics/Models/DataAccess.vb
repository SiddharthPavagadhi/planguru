﻿Imports System.Data.Entity
Imports System.Data.Entity.Database
Imports System.Data.Entity.ModelConfiguration.Conventions
Imports System.Data.Entity.Infrastructure

Public Class DataAccess
    Inherits DbContext

    Public Property AcctTypes As DbSet(Of AcctType)
    Public Property Accounts As DbSet(Of Account)
    Public Property Balances As DbSet(Of Balance)
    Public Property Dashboards As DbSet(Of Dashboard)
    Public Property DashboardDetails As DbSet(Of DashboardDetail)
    'Master tables
    Public Property UserRoles() As DbSet(Of UserRole)
    Public Property UserRolePermissions() As DbSet(Of UserRolePermission)
    Public Property CustomerRequestTypes() As DbSet(Of CustomerRequestType)
    Public Property ChartFormats() As DbSet(Of ChartFormat)
    Public Property ChartTypes() As DbSet(Of ChartType)
    Public Property ChartFormatTypes() As DbSet(Of ChartFormatType)

    'Subscription tables
    Public Property Customers() As DbSet(Of Customer)
    Public Property Users() As DbSet(Of User)
    Public Property Status() As DbSet(Of Status)
    Public Property Companies() As DbSet(Of Company)
    Public Property CountryCodes() As DbSet(Of CountryCode)
    Public Property StateCodes() As DbSet(Of StateCode)
    'Mapping tables
    Public Property UserCompanyMappings() As DbSet(Of UserCompanyMapping)
    Public Property UserRolePermissionMappings() As DbSet(Of UserRolePermissionMapping)
    Public Property UserAnalysisMappings() As DbSet(Of UserAnalysisMapping)
    Public Property CustomerResponseMappings() As DbSet(Of CustomerResponseMapping)

    'Analysis tables
    Public Property Analyses() As DbSet(Of Analysis)

    'SubscriptionTransactionRequestResponse table
    Public Property SubscriptionTransactionRequestResponses() As DbSet(Of SubscriptionTransactionRequestResponse)

    'Email Table
    Public Property EmailInfos() As DbSet(Of EmailInfo)

    '*****************File upload Tables********************
    Public Property UserTabless() As DbSet(Of UserTables)
    Public Property FileUploadedResults() As DbSet(Of FileUploadedResult)
    Public Property FileProcesss() As DbSet(Of FileProcess)
    '*******************************************************                                  

    'SaveView Table to save report name of selected analysis
    Public Property SaveView() As DbSet(Of SaveView)

    'Scorecard Table to save scorecard of selected analysis
    Public Property Scorecards() As DbSet(Of Scorecard)

    'Scorecard Table to save scorecard of selected analysis
    Public Property SubscriptionPlans() As DbSet(Of SubscriptionPlan)


    Protected Overrides Sub OnModelCreating(modelBuilder As DbModelBuilder)
        Logger.Log.Info(String.Format("DataBase Creation started "))
        Try
            modelBuilder.Conventions.Remove(Of PluralizingTableNameConvention)()

            'modelBuilder.Entity(Of Customer).ToTable("Customer")
            'modelBuilder.Entity(Of User).ToTable("User")
            'modelBuilder.Entity(Of Company).ToTable("Company")

            MyBase.OnModelCreating(modelBuilder)
            modelBuilder.Conventions.Remove(Of IncludeMetadataConvention)()

            Logger.Log.Info(String.Format("DataBase Creation ended "))
        Catch ex As Exception
            Logger.Log.Error(String.Format("Unable to create Database with Message- {0} " + Environment.NewLine + "Stack Trace: {1} ", ex.Message, ex.StackTrace))
        Finally
            Logger.Log.Info(String.Format("Execution Ended"))
        End Try
    End Sub
End Class
