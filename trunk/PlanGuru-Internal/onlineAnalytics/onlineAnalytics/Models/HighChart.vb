﻿Public Class HighChart
    Public Property HighChartTitle() As String

    Public Property HighChartSubTitle() As String

    Public Property HighChartType() As String

    Public Property HighChartFormatType() As Integer

    Public Property intColCount() As Integer

    Public Property decChartAmts() As Decimal(,)

    Public Property strXLabels() As String()

    Public Property intChartIndex() As Integer

    Public Property intFormat() As Integer

    Public Property blnShowasPerc() As Nullable(Of Boolean)

    Public Property blnSmoothed() As Nullable(Of Boolean)

    Public Property blnMarkers() As Nullable(Of Boolean)

    Public Property blnShowTrend() As Nullable(Of Boolean)

    Public Property strAppendLabel() As String

    Public Property blnPercentLabel() As Boolean

    Public Property intLastActual() As Integer

    Public Property intDecPlaces() As Integer

    Public Property blnShowBudget() As Boolean

    Public Property chartSerieses() As String()

    Public Property rowCounts() As Integer

    Public Property seriesLabels() As List(Of String)

End Class

Public Class HighChartModelForDonutAndPie
    Public Property decChartAmtsForDonut() As Decimal(,)

    Public Property strXLabelsForDonut() As List(Of String)
End Class
