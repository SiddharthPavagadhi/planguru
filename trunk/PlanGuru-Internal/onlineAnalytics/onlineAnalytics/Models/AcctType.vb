﻿Imports System.ComponentModel.DataAnnotations

Public Class AcctType

    <Key(), Column(Order:=0)>
    <DatabaseGenerated(DatabaseGeneratedOption.None)>
    Public Property AcctTypeId As Integer

    <Key(), Column(Order:=1)>
    Public Property AnalysisId As Integer

    <Required()>
    <StringLength(50)>
    Public Property TypeDesc As String
    <StringLength(50)>
    Public Property ClassDesc As String
    <StringLength(50)>
    Public Property SubclassDesc As String

    'Public Overridable Property Accounts() As ICollection(Of Account)

End Class
