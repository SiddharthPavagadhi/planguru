﻿function Prevalidate(spread, selectedValue,controllerName) {
    var rc = spread.GetRowCount();
    var count = 0;
    for (var i = 0; i < rc; i++) {
        var chkboxval = null;
        if (controllerName === "RE" || controllerName === "ALC") {
            chkboxval = spread.GetValue(i, 2);
        }
        else if (controllerName === "RA" || controllerName === "CF" || controllerName === "OM")
        {
            chkboxval = spread.GetValue(i, 1);
        }
        if (chkboxval == "true") {
            count++;
        }
    }

    if ((selectedValue === "1" || selectedValue === "4" || selectedValue === "3") && count > 3) {
        return false;
    }
    else {
        return true;
    }
}