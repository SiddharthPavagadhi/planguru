﻿Imports FarPoint.Mvc.Spread
Imports System.Data.Entity
Namespace onlineAnalytics

    <CustAuthFilter()>
    Public Class RAController
        Inherits System.Web.Mvc.Controller
        Dim sFormat As Integer = 0
        Dim sPeriod As Integer = Val(DateTime.Today.ToString("MM")) - 2
        Dim sHPeriod As Integer = 1
        Dim intFirstNumberColumn As Integer = 6
        Dim intAnalysisID As Integer
        Dim intFirstFiscal As Integer
        Private intFirstYear As Integer
        Private isViewer As Boolean = False
        Private accountRepository As IAccountRepository
        Private accountTypeRepository As IAccountTypeRepository
        Private balanceRepository As IBalanceRepository
        Private chartFormatRepository As IChartFormatRepository
        Private chartTypeRepository As IChartTypeRepository
        Private saveViewRepository As ISaveViewRepository
        Private UserInfo As User
        Private typeDescList() As String = {"ratio"}
        Private blnArchBudget As Boolean
        Private blnShowBudgetChecked As Boolean

        Public Sub New()
            Me.chartFormatRepository = New ChartFormatRepository(New DataAccess())
            Me.chartTypeRepository = New ChartTypeRepository(New DataAccess())
            Me.accountRepository = New AccountRepository(New DataAccess())
            Me.accountTypeRepository = New AccountTypeRepository(New DataAccess())
            Me.balanceRepository = New BalanceRepository(New DataAccess())
            Me.saveViewRepository = New SaveViewRepository(New DataAccess())
        End Sub

        Public Sub New(chartFormatRepository As IChartFormatRepository)
            Me.chartFormatRepository = chartFormatRepository
        End Sub

        Public Sub New(chartTypeRepository As IChartTypeRepository)
            Me.chartTypeRepository = chartTypeRepository
        End Sub

        Public Sub New(accountRepository As IAccountRepository)
            Me.accountRepository = accountRepository
        End Sub

        Public Sub New(accountTypeRepository As IAccountTypeRepository)
            Me.accountTypeRepository = accountTypeRepository
        End Sub

        Public Sub New(balanceRepository As IBalanceRepository)
            Me.balanceRepository = balanceRepository
        End Sub

        Public Sub New(saveViewRepository As ISaveViewRepository)
            Me.saveViewRepository = saveViewRepository
        End Sub

        'Display Account type & Account items with options.
        'List of Companies, List of Month, List of Format & Chart Type
        'Options to select (positive / negative) variance, percentage
        ' GET: /RE
        <HttpGet()> _
        <CustomActionFilter()>
        Function Index(<MvcSpread("spdAnalytics")> ByVal spdAnalytics As FpSpread, ByVal formValues As FormValues) As ActionResult
            Dim highChart As ReportHighChart = New ReportHighChart
            Dim setupCountObj As SetupCount
            Dim selectedAnalysis As Integer
            Try
                Logger.Log.Info(String.Format("RAController Index (HttpGet) method execution starts"))
                blnArchBudget = DirectCast(Session("BudgetArch"), Boolean)
                blnShowBudgetChecked = DirectCast(Session("ShowBudget"), Boolean)
                If (Not Session("UserType") Is Nothing) And (Not Session("UserInfo") Is Nothing) Then
                    UserInfo = DirectCast(Session("UserInfo"), User)
                    sPeriod = Session("sPeriod")
                    sFormat = Session("sFormat")
                    Try
                        sHPeriod = DirectCast(Session("HPeriod"), Integer)
                    Catch ex As Exception
                        sHPeriod = 1
                    End Try
                    If (UserInfo.AnalysisId = 0) Then
                        Return RedirectToAction("Index", "Dashboard")
                    Else
                        intAnalysisID = UserInfo.AnalysisId
                        intFirstFiscal = UserInfo.fiscalMonthOfSelectedCompany
                    End If
                End If

                If (UserInfo.AnalysisId > 0) Then
                    selectedAnalysis = UserInfo.AnalysisId
                Else
                    If (Not Session("SelectedAnalysisFromDropdown") Is Nothing) Then
                        selectedAnalysis = Session("SelectedAnalysisFromDropdown")
                    Else
                        selectedAnalysis = -1
                    End If
                End If
                setupCountObj = Utility.CheckRequiredSetupCount(UserInfo, selectedAnalysis)
                ViewBag.Setup = setupCountObj

                Dim intCtr As Integer
                Dim periods As List(Of SelectListItem) = New List(Of SelectListItem)
                'For intCtr = 0 To sPeriod                   
                For intCtr = 0 To 11
                    periods.Add(New SelectListItem With {.Text = CommonProcedures.GetMonth(intCtr, intFirstFiscal), .Value = intCtr + 1, .Selected = False})
                Next
                ViewData("Periods") = New SelectList(periods, "value", "text", "May")

                Dim Formats As List(Of SelectListItem) = New List(Of SelectListItem)

                For Each chartFormat As ChartFormat In chartFormatRepository.GetChartFormats()
                    Select Case chartFormat.ChartFormatId
                        Case 1, 3, 4, 6 To 11, 13
                            Formats.Add(New SelectListItem With {.Text = chartFormat.Format, .Value = chartFormat.ChartFormatId, .Selected = False})
                    End Select
                Next
                ViewData("Formats") = New SelectList(Formats, "value", "text")

                Dim filterOn As List(Of SelectListItem) = New List(Of SelectListItem) From {
                    New SelectListItem With {.Text = "Current period amount", .Value = "Current", .Selected = False},
                    New SelectListItem With {.Text = "Variance amount", .Value = "Variance", .Selected = False},
                    New SelectListItem With {.Text = "Percent variance", .Value = "PercentVar", .Selected = True}
                }
                ViewData("FilterOn") = New SelectList(filterOn, "value", "text", "selected")

                Dim chartTypes As List(Of SelectListItem) = New List(Of SelectListItem)
                If sFormat < 6 Then
                    chartTypes.Add(New SelectListItem With {.Text = "None", .Value = 1, .Selected = True})
                    chartTypes.Add(New SelectListItem With {.Text = "Bar", .Value = 2, .Selected = False})
                Else
                    For Each chartType As ChartType In chartTypeRepository.GetChartTypes()
                        If chartType.ChartTypeId <> 6 And chartType.ChartTypeId <> 7 Then
                            chartTypes.Add(New SelectListItem With {.Text = chartType.Type, .Value = chartType.ChartTypeId, .Selected = False})
                        End If

                    Next
                End If

                ViewData("ChartType") = New SelectList(chartTypes, "value", "text", "selected")
                'ew 1/14 chg for historical periods
                Dim hperiods As List(Of SelectListItem) = New List(Of SelectListItem)
                intFirstYear = IIf(UserInfo.FirstYear <> Nothing And UserInfo.FirstYear > 0, UserInfo.FirstYear, DateTime.Now.Year())
                Dim intHistPeriods As Integer = DirectCast(Session("NumberofHYears"), Integer)
                For intCtr = 1 To intHistPeriods
                    If intFirstFiscal = 1 Then
                        hperiods.Add(New SelectListItem With {.Text = intFirstYear - intCtr, .Value = intCtr + 1, .Selected = False})
                    Else
                        Dim strYear As String = intFirstYear - (intCtr + 1) & "/" & intFirstYear - intCtr
                        hperiods.Add(New SelectListItem With {.Text = strYear, .Value = intCtr + 1, .Selected = False})
                    End If
                Next
                ViewData("HPeriods") = New SelectList(hperiods, "value", "text")

                ViewData("blnHighVar") = False
                ViewData("blnShowPercent") = 0
                ViewData("blnShowBudget") = blnShowBudgetChecked
                ViewData("intPeriod") = sPeriod + 1
                ViewData("intFormat") = sFormat + 1
                ViewData("intHPeriod") = sHPeriod + 1
                ViewData("intChartType") = 1
                ViewData("blnShowTrend") = False
                ViewData("blnUseFilter") = False
                ViewData("intDept") = 1
                ViewData("Postback") = False
                If blnArchBudget = True Then
                    ViewData("strCheckboxCaption") = "Show forecast amounts"
                Else
                    ViewData("strCheckboxCaption") = "Show budget amounts"
                End If
                Logger.Log.Info(String.Format("RAController Index (HttpGet) method execution Ends"))
            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Error occured in Quick Reports --> Ratios - ", ex.Message)
                Logger.Log.Error(String.Format("RAController Index (HttpGet) method execution ends with Error message: {0}, StackTrace: {1}", ex.Message, ex.StackTrace))
            End Try
            Return View(highChart)
        End Function

        'Post 
        'It will render the chart based on selected options available for accounts.
        <HttpPost()> _
        <CustomActionFilter()>
        Function Index(<MvcSpread("spdAnalytics")> ByVal spdAnalytics As FpSpread, <MvcSpread("spdChart")> ByVal spdChart As FpSpread, ByVal formValues As FormValues) As ActionResult
            Dim highChart As ReportHighChart = New ReportHighChart
            Dim intCounter As Integer
            Dim setupCountObj As SetupCount
            Dim selectedAnalysis As Integer
            Logger.Log.Info(String.Format("RAController Index (HttpPost) method execution Starts"))
            Try
                blnArchBudget = DirectCast(Session("BudgetArch"), Boolean)
                blnShowBudgetChecked = DirectCast(Session("ShowBudget"), Boolean)
                If (Not Session("UserType") Is Nothing) And (Not Session("UserInfo") Is Nothing) Then
                    UserInfo = DirectCast(Session("UserInfo"), User)
                    If (UserInfo.AnalysisId = 0) Then
                        Return RedirectToAction("Index", "Dashboard")
                    Else
                        intAnalysisID = UserInfo.AnalysisId
                        intFirstFiscal = UserInfo.fiscalMonthOfSelectedCompany
                    End If
                End If

                If (UserInfo.AnalysisId > 0) Then
                    selectedAnalysis = UserInfo.AnalysisId
                Else
                    If (Not Session("SelectedAnalysisFromDropdown") Is Nothing) Then
                        selectedAnalysis = Session("SelectedAnalysisFromDropdown")
                    Else
                        selectedAnalysis = -1
                    End If
                End If
                setupCountObj = Utility.CheckRequiredSetupCount(UserInfo, selectedAnalysis)
                ViewBag.Setup = setupCountObj

                'If (spdChart.Sheets(0).Charts.Count > 0) Then
                '    spdChart.Sheets(0).Charts.Remove(spdChart.Sheets(0).Charts(0))
                'End If
            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Error occured in Quick Reports --> Ratios - ", ex.Message)
                Logger.Log.Error(String.Format("RAController Index (HttpPost) method execution ends with Error message: {0}, StackTrace: {1}", ex.Message, ex.StackTrace))
            End Try
            Try
                spdAnalytics.Sheets(0).RowCount = 0
                'ew 2/15 
                If formValues.blnShowBudget = True Then
                    Session("ShowBudget") = True
                End If

                intFirstYear = IIf(UserInfo.FirstYear <> Nothing And UserInfo.FirstYear > 0, UserInfo.FirstYear, DateTime.Now.Year())
                SetRASpreadProperties(spdAnalytics, formValues.intFormat - 1, formValues.intPeriod - 1, formValues.intHPeriod - 1)
                If formValues.intChartType <> 1 Then
                    highChart = Charts.BuildChartWithHighChart(Me.ControllerContext.Controller, spdAnalytics, formValues.intChartType, formValues.intFormat - 1, intFirstNumberColumn, formValues.blnShowasPer, formValues.blnShowTrend, False)
                    'Charts.BuildChart(Me.ControllerContext.Controller, spdChart, spdAnalytics, formValues.intChartType, formValues.intFormat - 1, intFirstNumberColumn, formValues.blnShowasPer, formValues.blnShowTrend, False)
                    ViewData("blnUpdateChart") = True
                End If

                Dim periods As List(Of SelectListItem) = New List(Of SelectListItem)
                'For intCounter = 0 To sPeriod
                For intCounter = 0 To 11
                    periods.Add(New SelectListItem With {.Text = CommonProcedures.GetMonth(intCounter, intFirstFiscal), .Value = intCounter + 1, .Selected = False})
                Next
                ViewData("Periods") = New SelectList(periods, "value", "text", "May")
                Dim Formats As List(Of SelectListItem) = New List(Of SelectListItem)
                For Each chartFormat As ChartFormat In chartFormatRepository.GetChartFormats()
                    Select Case chartFormat.ChartFormatId
                        Case 1, 3, 4, 6 To 11, 13
                            Formats.Add(New SelectListItem With {.Text = chartFormat.Format, .Value = chartFormat.ChartFormatId, .Selected = False})
                    End Select
                Next
                ViewData("Formats") = New SelectList(Formats, "value", "text")

                Dim filterOn As List(Of SelectListItem) = New List(Of SelectListItem) From {
                    New SelectListItem With {.Text = "Current period amount", .Value = "Current", .Selected = False},
                    New SelectListItem With {.Text = "Variance amount", .Value = "Variance", .Selected = False},
                    New SelectListItem With {.Text = "Percent variance", .Value = "PercentVar", .Selected = True}
                }
                ViewData("FilterOn") = New SelectList(filterOn, "value", "text", "selected")

                Dim chartTypes As List(Of SelectListItem) = New List(Of SelectListItem)
                If formValues.intFormat < 6 Then
                    chartTypes.Add(New SelectListItem With {.Text = "None", .Value = 1, .Selected = False})
                    chartTypes.Add(New SelectListItem With {.Text = "Bar", .Value = 2, .Selected = False})
                Else
                    For Each chartType As ChartType In chartTypeRepository.GetChartTypes()
                        If chartType.ChartTypeId <> 6 And chartType.ChartTypeId <> 7 Then
                            chartTypes.Add(New SelectListItem With {.Text = chartType.Type, .Value = chartType.ChartTypeId, .Selected = False})
                        End If

                    Next
                End If

                ViewData("ChartType") = New SelectList(chartTypes, "value", "text", "selected")
                ViewData("intChartType") = formValues.intChartType
                'ew 1/14 chg for historical periods
                Dim hperiods As List(Of SelectListItem) = New List(Of SelectListItem)
                Dim intHistPeriods As Integer = DirectCast(Session("NumberofHYears"), Integer)
                For intCtr = 1 To intHistPeriods
                    If intFirstFiscal = 1 Then
                        hperiods.Add(New SelectListItem With {.Text = intFirstYear - intCtr, .Value = intCtr + 1, .Selected = False})
                    Else
                        Dim strYear As String = intFirstYear - (intCtr + 1) & "/" & intFirstYear - intCtr
                        hperiods.Add(New SelectListItem With {.Text = strYear, .Value = intCtr + 1, .Selected = False})
                    End If
                Next
                ViewData("HPeriods") = New SelectList(hperiods, "value", "text")

                ViewData("blnHighVar") = False
                ViewData("blnShowBudget") = blnShowBudgetChecked
                ViewData("Period") = formValues.intPeriod - 1
                ViewData("intDept") = 1
                ViewData("intFormat") = formValues.intFormat
                ViewData("intHPeriod") = formValues.intHPeriod
                If blnArchBudget = True Then
                    ViewData("strCheckboxCaption") = "Show forecasted amounts"
                Else
                    ViewData("strCheckboxCaption") = "Show budgeted amounts"
                End If
                ViewData("postback") = True

                Session("sPeriod") = formValues.intPeriod - 1
                Session("sFormat") = formValues.intFormat - 1
                If formValues.intFormat = 13 Then
                    Session("HPeriod") = formValues.intHPeriod - 1
                End If
                Logger.Log.Info(String.Format("RAController Index (HttpPost) method execution Ends"))
            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Error occured in Quick Reports --> Ratios - ", ex.Message)
                Logger.Log.Error(String.Format("RAController Index (HttpPost) method execution ends with Error message: {0}, StackTrace: {1}", ex.Message, ex.StackTrace))
            End Try
            Return View(highChart)
        End Function

        <MvcSpreadEvent("Load", "spdAnalytics", DirectCast(Nothing, String()))> _
        Private Sub FpSpread_Load(ByVal sender As Object, ByVal e As EventArgs)
            Try
                Logger.Log.Info(String.Format("RAController:FpSpread_Load method execution Starts"))
                Dim spread As FpSpread = DirectCast(sender, FpSpread)
                If Not spread.Page.IsPostBack Then

                    SetRASpreadProperties(spread, sFormat, sPeriod, sHPeriod)
                End If
                Logger.Log.Info(String.Format("RAController:FpSpread_Load method execution Ends"))
            Catch ex As Exception
                Logger.Log.Error(String.Format("RAController FpSpread_Load method execution ends with Error message: {0}, StackTrace: {1}", ex.Message, ex.StackTrace))
            End Try

        End Sub

        <MvcSpreadEvent("PreRender", "spdAnalytics", DirectCast(Nothing, String()))> _
        Private Sub FpSpread_PreRender(ByVal sender As Object, ByVal e As EventArgs)
            Try
                Logger.Log.Info(String.Format("RAController:FpSpread_PreRender method execution Starts"))
                Dim spread As FpSpread = DirectCast(sender, FpSpread)
                Logger.Log.Info(String.Format("RAController:FpSpread_PreRender method execution Ends"))
            Catch ex As Exception
                Logger.Log.Error(String.Format("RAController FpSpread_PreRender method execution ends with Error message: {0}, StackTrace: {1}", ex.Message, ex.StackTrace))
            End Try
        End Sub

        Private Sub SetRASpreadProperties(ByVal spdAnalytics As FpSpread, ByVal intDropDownSel As Integer, ByVal intPeriod As Integer, ByVal intHPeriod As Integer)
            Try
                Logger.Log.Info(String.Format("RAController:SetSpreadProperties method execution Starts"))
                Dim chkbox As New FarPoint.Web.Spread.CheckBoxCellType
                Dim txtcell As New FarPoint.Web.Spread.GeneralCellType
                Dim intColCount As Integer = 0
                Dim intNumberColumn As Integer = 0
                Dim intPercentColumn As Integer = 0
                Dim intCtr As Integer
                Dim intColCtr As Integer
                Dim intFormat As Integer
                Dim blnShowBudget As Boolean

                intFormat = CommonProcedures.ConvertFormat(intDropDownSel, blnShowBudget)
                'set column count
                Select Case intFormat
                    Case 0 To 4
                        intColCount = intFirstNumberColumn + 4
                        intNumberColumn = 3
                        intPercentColumn = 1
                    Case 5 'current year trend
                        If blnShowBudget = True Then
                            intNumberColumn = 12
                            intPercentColumn = 12
                        Else
                            intNumberColumn = intPeriod + 1
                            intPercentColumn = intPeriod + 1
                        End If
                        intColCount = intFirstNumberColumn + intNumberColumn
                    Case 6, 9, 10, 11
                        intNumberColumn = 12
                        intPercentColumn = 11
                        intColCount = intFirstNumberColumn + 12
                    Case 7, 8
                        intNumberColumn = 6
                        intPercentColumn = 6
                        intColCount = intFirstNumberColumn + 6
                End Select
                CommonProcedures.SetCommonProperties(spdAnalytics, intColCount, False, intFirstNumberColumn, intNumberColumn, intFormat, "RA")
                'load rows
                Select Case intFormat
                    Case 0 To 4
                        LoadSpread0to4(spdAnalytics, intFormat, intPeriod, intNumberColumn, intFirstNumberColumn, intColCount)
                        spdAnalytics.Sheets(0).Columns(intFirstNumberColumn, intFirstNumberColumn + 3).Visible = True
                    Case 5
                        LoadSpread5(spdAnalytics, intFormat, intPeriod, intNumberColumn, intFirstNumberColumn, intColCount, blnShowBudget)
                        spdAnalytics.Sheets(0).Columns(intFirstNumberColumn, intFirstNumberColumn + intNumberColumn - 1).Visible = True
                    Case 6
                        LoadSpread6(spdAnalytics, intFormat, intPeriod, intNumberColumn, intFirstNumberColumn, intColCount)
                        spdAnalytics.Sheets(0).Columns(intFirstNumberColumn, intFirstNumberColumn + intNumberColumn - 1).Visible = True
                    Case 7, 8
                        LoadSpread7to8(spdAnalytics, intFormat, intPeriod, intNumberColumn, intFirstNumberColumn, intColCount)
                        Dim blnLoop As Boolean = True
                        intCtr = 0
                        intNumberColumn = 6
                        For intColCtr = intFirstNumberColumn To intFirstNumberColumn + 5
                            spdAnalytics.Sheets(0).Columns(intColCtr).Visible = True
                        Next
                        Dim decValue As Decimal
                        intCtr = -1
                        Do While blnLoop = True
                            intCtr += 1
                            If spdAnalytics.Sheets(0).GetValue(intCtr, 2) = "Detail" Then
                                For intColCtr = intFirstNumberColumn To intFirstNumberColumn + 5
                                    decValue = spdAnalytics.Sheets(0).GetValue(intCtr, intColCtr)
                                    If spdAnalytics.Sheets(0).GetValue(intCtr, intColCtr) = 0 Then
                                        spdAnalytics.Sheets(0).Columns(intColCtr).Visible = False
                                        intColCount -= 1
                                        intNumberColumn -= 1
                                    Else
                                        blnLoop = False
                                        Exit For
                                    End If
                                Next
                            End If
                            If intCtr = spdAnalytics.Sheets(0).RowCount - 1 Then
                                blnLoop = False
                            End If
                        Loop
                    Case 9
                        LoadSpread9(spdAnalytics, intFormat, intPeriod, intNumberColumn, intFirstNumberColumn, intColCount)
                        spdAnalytics.Sheets(0).Columns(intFirstNumberColumn, intFirstNumberColumn + intNumberColumn - 1).Visible = True
                    Case 10
                        LoadSpread10(spdAnalytics, intFormat, intPeriod, intNumberColumn, intFirstNumberColumn, intColCount, intHPeriod)
                        spdAnalytics.Sheets(0).Columns(intFirstNumberColumn, intFirstNumberColumn + intNumberColumn - 1).Visible = True
                    Case 11
                        LoadSpread10(spdAnalytics, intFormat, intPeriod, intNumberColumn, intFirstNumberColumn, intColCount, 0)
                        spdAnalytics.Sheets(0).Columns(intFirstNumberColumn, intFirstNumberColumn + intNumberColumn - 1).Visible = True
                End Select
                'set col widths
                CommonProcedures.SetColumnWidths(spdAnalytics, intColCount, intFirstNumberColumn, intFormat, intNumberColumn, False, 4, "RA")
                spdAnalytics.Sheets(0).Columns(1).Visible = False
                'load column headers
                Dim strHeadingTextBudget As String
                If blnArchBudget = False Then
                    strHeadingTextBudget = "Budget"
                Else
                    strHeadingTextBudget = "Forecast"
                End If
                CommonProcedures.SetColumnHeader(spdAnalytics, intFormat, intPeriod, intColCount, intFirstNumberColumn, intFirstFiscal, False, strHeadingTextBudget, intHPeriod, "RA", intFirstYear, blnShowBudget)
                Logger.Log.Info(String.Format("RAController:SetSpreadProperties method execution Ends"))
            Catch ex As Exception
                Logger.Log.Error(String.Format("RAController:SetSpreadProperties method execution ends with Error message: {0}, StackTrace: {1}", ex.Message, ex.StackTrace))
            End Try

        End Sub

        <CustomActionFilter()>
        Public Sub LoadSpread0to4(ByVal spdAnalytics As FpSpread, ByVal intFormat As Integer, ByVal intPeriod As Integer, ByVal intNumberColumn As Integer, ByVal intFirstNumColumn As Integer, ByVal intColumnCount As Integer)
            Try
                Logger.Log.Info(String.Format("RAController:LoadSpread0to4 method execution Starts"))
                Using utility As New Utility
                    Dim intRowCounter As Integer = -1
                    Dim intColumnCounter As Integer
                    Dim strYearType(5) As String
                    Dim strPeriods(2) As String
                    Dim decAccumTotals(1) As Decimal
                    Dim decPeriodAmts(11) As Decimal
                    Dim decSaveAmts(7) As Decimal
                    Dim intSpanRows(1) As Integer
                    Dim chkbox As New FarPoint.Web.Spread.CheckBoxCellType
                    Dim percentcell As New FarPoint.Web.Spread.PercentCellType
                    Dim deccell As New FarPoint.Web.Spread.DoubleCellType
                    Dim decIncAccumTotals(1) As Decimal
                    Dim strBrowser As String = Request.Browser.Browser

                    If (isViewer) Then
                        chkbox.OnClientClick = "return false"
                    Else
                        chkbox.OnClientClick = CommonProcedures.CheckboxClicked(1)
                    End If

                    Dim selectedAccounts = (From a In utility.AccountRepository.GetAccountRecordsByAnalysisId(intAnalysisID)
                                           Join ac In utility.AccountTypeRepository.GetAccountTypeRecordsByAnalysisId(intAnalysisID, typeDescList)
                                           On a.AcctTypeId Equals ac.AcctTypeId Where a.AnalysisId = ac.AnalysisId
                                           Order By a.SortSequence
                                           Select a.AccountId, a.AcctTypeId, a.AnalysisId, a.AcctDescriptor, a.SortSequence, a.Description, a.Subgrouping, a.TotalType, a.NumberFormat, ac.TypeDesc, ac.ClassDesc
                                         ).ToList()

                    Dim balanceAcc = (From b In utility.BalanceRepository.GetBalanceRecordsByAnalysisId(intAnalysisID)
                                   Join acc In selectedAccounts On b.AccountId Equals acc.AccountId
                                   Where b.AnalysisId = acc.AnalysisId
                                   Select b).ToList()

                    For Each account In selectedAccounts
                        CommonProcedures.AddRow(spdAnalytics, intRowCounter, account.Description, account.AcctDescriptor, account.TypeDesc, strBrowser, 4)
                        Dim intNumberFormat As Integer = account.NumberFormat
                        Select Case intNumberFormat
                            Case 1 To 5
                                deccell.DecimalDigits = 3
                                spdAnalytics.Sheets(0).Cells(intRowCounter, intFirstNumColumn, intRowCounter, intFirstNumColumn + 2).CellType = deccell
                            Case 99
                                spdAnalytics.Sheets(0).Cells(intRowCounter, intFirstNumColumn, intRowCounter, intFirstNumColumn + 2).CellType = percentcell
                        End Select
                        Select Case account.AcctDescriptor
                            Case "Heading"
                                CommonProcedures.FormatHeaderRow(spdAnalytics, intRowCounter, intSpanRows, False, 4)
                                spdAnalytics.Sheets(0).SetValue(intRowCounter, 0, "")
                        End Select
                        'save accountID 
                        spdAnalytics.Sheets(0).SetValue(intRowCounter, 5, account.AccountId)
                        'save amounts  
                        If account.AcctDescriptor = "Detail" Then
                            Array.Clear(decPeriodAmts, 0, 12)
                            Array.Clear(decSaveAmts, 0, 5)
                            spdAnalytics.Sheets(0).Cells(intRowCounter, 2).CellType = chkbox
                            Dim accountId As Integer = account.AccountId
                            For Each balance As Balance In (From b In balanceAcc.Where(Function(a) a.AccountId = accountId And a.AnalysisId = intAnalysisID) Select b)
                                Select Case intFormat
                                    Case 0
                                        If blnArchBudget = False Then
                                            GetBudgetAmts(balance, decPeriodAmts)
                                        Else
                                            GetArchivedBudgetAmts(balance, decPeriodAmts)
                                        End If
                                        decSaveAmts(0) = decPeriodAmts(intPeriod)
                                        GetActualAmts(balance, decPeriodAmts)
                                        decSaveAmts(1) = decPeriodAmts(intPeriod)
                                    Case 1
                                        If blnArchBudget = False Then
                                            GetBudgetAmts(balance, decPeriodAmts)
                                        Else
                                            GetArchivedBudgetAmts(balance, decPeriodAmts)
                                        End If
                                        For intColumnCounter = 0 To intPeriod
                                            decSaveAmts(0) += decPeriodAmts(intColumnCounter)
                                        Next
                                        GetActualAmts(balance, decPeriodAmts)
                                        For intColumnCounter = 0 To intPeriod
                                            decSaveAmts(1) += decPeriodAmts(intColumnCounter)
                                        Next
                                    Case 2
                                        GetActualAmts(balance, decPeriodAmts)
                                        If intPeriod = 0 Then
                                            decSaveAmts(1) = decPeriodAmts(intPeriod)
                                            GetH5Amts(balance, decPeriodAmts)
                                            decSaveAmts(0) = decPeriodAmts(11)
                                        Else
                                            decSaveAmts(0) = decPeriodAmts(intPeriod - 1)
                                            decSaveAmts(1) = decPeriodAmts(intPeriod)
                                        End If
                                    Case 3
                                        GetH5Amts(balance, decPeriodAmts)
                                        decSaveAmts(0) = decPeriodAmts(intPeriod)
                                        GetActualAmts(balance, decPeriodAmts)
                                        decSaveAmts(1) = decPeriodAmts(intPeriod)
                                    Case 4
                                        GetH5Amts(balance, decPeriodAmts)
                                        For intColumnCounter = 0 To intPeriod
                                            decSaveAmts(0) += decPeriodAmts(intColumnCounter)
                                        Next
                                        GetActualAmts(balance, decPeriodAmts)
                                        For intColumnCounter = 0 To intPeriod
                                            decSaveAmts(1) += decPeriodAmts(intColumnCounter)
                                        Next
                                End Select
                            Next

                            'store values 
                            spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumColumn, decSaveAmts(0))
                            spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumColumn + 1, decSaveAmts(1))
                            'compute variances
                            CommonProcedures.ComputeVariances(decSaveAmts, account.TypeDesc, decAccumTotals, False)
                            spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumColumn + 2, decSaveAmts(2))
                            spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumColumn + 3, decSaveAmts(3))
                        End If
                    Next
                End Using
                Logger.Log.Info(String.Format("RAController:LoadSpread0to4 method execution Ends"))
            Catch ex As Exception
                Logger.Log.Error(String.Format("RAController:LoadSpread0to4 method execution ends with Error message: {0}, StackTrace: {1}", ex.Message, ex.StackTrace))
            End Try

        End Sub

        <CustomActionFilter()>
        Public Sub LoadSpread5(ByVal spdAnalytics As FpSpread, ByVal intFormat As Integer, ByRef intCurrentMonth As Integer, ByVal intNumberColumn As Integer, ByVal intFirstNumberColumn As Integer, ByVal intColumnCount As Integer, ByVal blnShowBudget As Boolean)
            Try
                Logger.Log.Info(String.Format("RAController:LoadSpread5to6 method execution Starts"))

                Using utility As New Utility()
                    Dim intRowCounter As Integer = -1
                    Dim intColumnCounter As Integer
                    Dim intLastActualCol As Integer = -1
                    Dim strYearType(5) As String
                    Dim strPeriods(2) As String
                    Dim decAccumTotals(12) As Decimal
                    Dim decPeriodAmts(11) As Decimal
                    Dim decSaveAmts(12) As Decimal
                    Dim intPeriods As Integer = 11
                    Dim intSpanRows(1) As Integer
                    Dim decIncAccumTotals(intNumberColumn) As Decimal
                    Dim chkbox As New FarPoint.Web.Spread.CheckBoxCellType
                    Dim percentcell As New FarPoint.Web.Spread.PercentCellType
                    Dim deccell As New FarPoint.Web.Spread.DoubleCellType
                    Dim strBrowser As String = Request.Browser.Browser

                    If (isViewer) Then
                        chkbox.OnClientClick = "return false"
                    Else
                        chkbox.OnClientClick = CommonProcedures.CheckboxClicked(1)
                    End If

                    Dim selectedAccounts = (From a In utility.AccountRepository.GetAccountRecordsByAnalysisId(intAnalysisID)
                                           Join ac In utility.AccountTypeRepository.GetAccountTypeRecordsByAnalysisId(intAnalysisID, typeDescList)
                                           On a.AcctTypeId Equals ac.AcctTypeId Where a.AnalysisId = ac.AnalysisId
                                           Order By a.SortSequence
                                           Select a.AccountId, a.AcctTypeId, a.AnalysisId, a.AcctDescriptor, a.SortSequence, a.Description, a.Subgrouping, a.TotalType, a.NumberFormat, ac.TypeDesc, ac.ClassDesc
                                         ).ToList()

                    Dim balanceAcc = (From b In utility.BalanceRepository.GetBalanceRecordsByAnalysisId(intAnalysisID)
                                    Join acc In selectedAccounts On b.AccountId Equals acc.AccountId
                                    Where b.AnalysisId = acc.AnalysisId
                                    Select b).ToList()

                    If blnShowBudget = True Then
                        For Each account In selectedAccounts
                            Dim accountId As Integer = account.AccountId
                            For Each balance As Balance In (From b In utility.BalanceRepository.GetBalances.Where(Function(a) a.AccountId = accountId And a.AnalysisId = intAnalysisID) Select b)
                                GetActualAmts(balance, decPeriodAmts)
                                For intColumnCounter = intCurrentMonth To 0 Step -1
                                    If decPeriodAmts(intColumnCounter) <> 0 Then
                                        If intColumnCounter > intLastActualCol Then
                                            intLastActualCol = intColumnCounter
                                        End If
                                    End If
                                Next
                            Next
                        Next
                        If intLastActualCol < intCurrentMonth Then
                            intCurrentMonth = intLastActualCol
                        End If
                    End If


                    For Each account In selectedAccounts
                        'Dim accountTypeId As Integer = account.AcctTypeId
                        'Dim acctType = (From a In utility.AccountTypeRepository.GetAccountTypes.Where(Function(at) at.AcctTypeId = accountTypeId) Select a).FirstOrDefault()
                        CommonProcedures.AddRow(spdAnalytics, intRowCounter, account.Description, account.AcctDescriptor, account.TypeDesc, strBrowser, 4)
                        Dim intNumberFormat As Integer = account.NumberFormat
                        Select Case intNumberFormat
                            Case 1 To 5
                                deccell.DecimalDigits = 3
                                spdAnalytics.Sheets(0).Cells(intRowCounter, intFirstNumberColumn, intRowCounter, spdAnalytics.Sheets(0).ColumnCount - 1).CellType = deccell
                            Case 99
                                spdAnalytics.Sheets(0).Cells(intRowCounter, intFirstNumberColumn, intRowCounter, spdAnalytics.Sheets(0).ColumnCount - 1).CellType = percentcell
                        End Select
                        Select Case account.AcctDescriptor
                            Case "Heading"
                                CommonProcedures.FormatHeaderRow(spdAnalytics, intRowCounter, intSpanRows, False, 4)
                                spdAnalytics.Sheets(0).SetValue(intRowCounter, 0, "")
                        End Select
                        'save accountID 
                        spdAnalytics.Sheets(0).SetValue(intRowCounter, 5, account.AccountId)
                        'save amounts 
                        If account.AcctDescriptor = "Detail" Then
                            Array.Clear(decPeriodAmts, 0, 12)
                            Array.Clear(decSaveAmts, 0, 13)
                            spdAnalytics.Sheets(0).Cells(intRowCounter, 2).CellType = chkbox
                            Dim accountId As Integer = account.AccountId
                            For Each balance As Balance In (From b In balanceAcc.Where(Function(a) a.AccountId = accountId And a.AnalysisId = intAnalysisID) Select b)
                                GetActualAmts(balance, decPeriodAmts)
                                For intColumnCounter = 0 To intCurrentMonth
                                    spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumberColumn + intColumnCounter, decPeriodAmts(intColumnCounter))
                                    decSaveAmts(intColumnCounter) = decPeriodAmts(intColumnCounter)
                                    '     decSaveAmts(intPeriods + 1) += decPeriodAmts(intColumnCounter)
                                Next
                                If blnShowBudget = True Then
                                    GetBudgetAmts(balance, decPeriodAmts)
                                    For intColumnCounter = intCurrentMonth + 1 To intPeriods
                                        spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumberColumn + intColumnCounter, decPeriodAmts(intColumnCounter))
                                        decSaveAmts(intColumnCounter) = decPeriodAmts(intColumnCounter)
                                        '       decSaveAmts(intPeriods + 1) += decPeriodAmts(intColumnCounter)
                                        spdAnalytics.Sheets(0).Cells(intRowCounter, intFirstNumberColumn + intColumnCounter).ForeColor = Drawing.Color.FromArgb(5, 51, 97)
                                    Next
                                End If
                            Next

                            'If blnShowBudget = True Then
                            '    spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumberColumn + intPeriods + 1, decSaveAmts(intPeriods + 1))
                            '    spdAnalytics.Sheets(0).Cells(intRowCounter, intFirstNumberColumn + intPeriods + 1).ForeColor = Drawing.Color.Blue
                            'Else
                            '    spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumberColumn + intCurrentMonth + 1, decSaveAmts(intPeriods + 1))
                            'End If
                        End If
                    Next
                End Using
                Logger.Log.Info(String.Format("RAController:LoadSpread5to6 method execution Ends"))
            Catch ex As Exception
                Logger.Log.Error(String.Format("RAController:LoadSpread5to6 method execution ends with Error message: {0}, StackTrace: {1}", ex.Message, ex.StackTrace))
            End Try


        End Sub

        <CustomActionFilter()>
        Public Sub LoadSpread6(ByVal spdAnalytics As FpSpread, ByVal intFormat As Integer, ByVal intPeriod As Integer, ByVal intNumberColumn As Integer, ByVal intFirstNumberColumn As Integer, ByVal intColumnCount As Integer)
            Try
                Logger.Log.Info(String.Format("RAController:LoadSpread5to6 method execution Starts"))

                Using utility As New Utility()
                    Dim intRowCounter As Integer = -1
                    Dim intColumnCounter As Integer
                    Dim strYearType(5) As String
                    Dim strPeriods(2) As String
                    Dim decAccumTotals(12) As Decimal
                    Dim decPeriodAmts(11) As Decimal
                    Dim decSaveAmts(12) As Decimal
                    Dim intSpanRows(1) As Integer
                    Dim decIncAccumTotals(intNumberColumn) As Decimal
                    Dim chkbox As New FarPoint.Web.Spread.CheckBoxCellType
                    Dim percentcell As New FarPoint.Web.Spread.PercentCellType
                    Dim deccell As New FarPoint.Web.Spread.DoubleCellType
                    Dim strBrowser As String = Request.Browser.Browser

                    If (isViewer) Then
                        chkbox.OnClientClick = "return false"
                    Else
                        chkbox.OnClientClick = CommonProcedures.CheckboxClicked(1)
                    End If

                    Dim selectedAccounts = (From a In utility.AccountRepository.GetAccountRecordsByAnalysisId(intAnalysisID)
                                           Join ac In utility.AccountTypeRepository.GetAccountTypeRecordsByAnalysisId(intAnalysisID, typeDescList)
                                           On a.AcctTypeId Equals ac.AcctTypeId Where a.AnalysisId = ac.AnalysisId
                                           Order By a.SortSequence
                                           Select a.AccountId, a.AcctTypeId, a.AnalysisId, a.AcctDescriptor, a.SortSequence, a.Description, a.Subgrouping, a.TotalType, a.NumberFormat, ac.TypeDesc, ac.ClassDesc
                                         ).ToList()

                    Dim balanceAcc = (From b In utility.BalanceRepository.GetBalanceRecordsByAnalysisId(intAnalysisID)
                                   Join acc In selectedAccounts On b.AccountId Equals acc.AccountId
                                   Where b.AnalysisId = acc.AnalysisId
                                   Select b).ToList()


                    For Each account In selectedAccounts
                        'Dim accountTypeId As Integer = account.AcctTypeId
                        'Dim acctType = (From a In utility.AccountTypeRepository.GetAccountTypes.Where(Function(at) at.AcctTypeId = accountTypeId) Select a).FirstOrDefault()
                        CommonProcedures.AddRow(spdAnalytics, intRowCounter, account.Description, account.AcctDescriptor, account.TypeDesc, strBrowser, 4)
                        Dim intNumberFormat As Integer = account.NumberFormat
                        Select Case intNumberFormat
                            Case 1 To 5
                                deccell.DecimalDigits = 3
                                spdAnalytics.Sheets(0).Cells(intRowCounter, intFirstNumberColumn, intRowCounter, spdAnalytics.Sheets(0).ColumnCount - 1).CellType = deccell
                            Case 99
                                spdAnalytics.Sheets(0).Cells(intRowCounter, intFirstNumberColumn, intRowCounter, spdAnalytics.Sheets(0).ColumnCount - 1).CellType = percentcell
                        End Select
                        Select Case account.AcctDescriptor
                            Case "Heading"
                                CommonProcedures.FormatHeaderRow(spdAnalytics, intRowCounter, intSpanRows, False, 4)
                                spdAnalytics.Sheets(0).SetValue(intRowCounter, 0, "")
                        End Select
                        'save accountID 
                        spdAnalytics.Sheets(0).SetValue(intRowCounter, 5, account.AccountId)
                        'save amounts
                        If account.AcctDescriptor = "Detail" Then
                            Array.Clear(decPeriodAmts, 0, 12)
                            Array.Clear(decSaveAmts, 0, 13)
                            spdAnalytics.Sheets(0).Cells(intRowCounter, 2).CellType = chkbox
                            Dim accountId As Integer = account.AccountId
                            For Each balance As Balance In (From b In balanceAcc.Where(Function(a) a.AccountId = accountId And a.AnalysisId = intAnalysisID) Select b)
                                If intPeriod < 11 Then
                                    GetH5Amts(balance, decPeriodAmts)
                                    For intColumnCounter = intPeriod + 1 To 11
                                        spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumberColumn + intColumnCounter - (intPeriod + 1), decPeriodAmts(intColumnCounter))
                                        decSaveAmts(intColumnCounter) = decPeriodAmts(intColumnCounter)
                                        '     decSaveAmts(12) += decPeriodAmts(intColumnCounter)
                                    Next
                                End If
                                GetActualAmts(balance, decPeriodAmts)
                                For intColumnCounter = 0 To intPeriod
                                    spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumberColumn + intColumnCounter + (11 - intPeriod), decPeriodAmts(intColumnCounter))
                                    decSaveAmts(intColumnCounter) = decPeriodAmts(intColumnCounter)
                                    '   decSaveAmts(12) += decPeriodAmts(intColumnCounter)
                                Next
                            Next
                            'If intFormat = 6 Then
                            '    spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumberColumn + 12, decSaveAmts(12))
                            'End If

                        End If

                    Next
                End Using
                Logger.Log.Info(String.Format("RAController:LoadSpread5to6 method execution Ends"))
            Catch ex As Exception
                Logger.Log.Error(String.Format("RAController:LoadSpread5to6 method execution ends with Error message: {0}, StackTrace: {1}", ex.Message, ex.StackTrace))
            End Try


        End Sub

        <CustomActionFilter()>
        Public Sub LoadSpread7to8(ByVal spdAnalytics As FpSpread, ByVal intFormat As Integer, ByVal intPeriod As Integer, ByVal intNumberColumn As Integer, ByVal intFirstNumberColumn As Integer, ByVal intColumnCount As Integer)
            Try
                Logger.Log.Info(String.Format("RAController:LoadSpread7to8 method execution Starts"))
                Using utility As New Utility

                    Dim intRowCounter As Integer = -1
                    Dim intColumnCounter As Integer
                    Dim strYearType(5) As String
                    Dim strPeriods(2) As String
                    Dim decPeriodAmts(11) As Decimal
                    Dim decAccumTotals(5) As Decimal
                    Dim decSaveAmts(5) As Decimal
                    Dim intSpanRows(1) As Integer
                    Dim decIncAccumTotals(intNumberColumn) As Decimal
                    Dim chkbox As New FarPoint.Web.Spread.CheckBoxCellType
                    Dim percentcell As New FarPoint.Web.Spread.PercentCellType
                    Dim deccell As New FarPoint.Web.Spread.DoubleCellType
                    Dim strBrowser As String = Request.Browser.Browser
                    If (isViewer) Then
                        chkbox.OnClientClick = "return false"
                    Else
                        chkbox.OnClientClick = CommonProcedures.CheckboxClicked(1)
                    End If

                    Dim selectedAccounts = (From a In utility.AccountRepository.GetAccountRecordsByAnalysisId(intAnalysisID)
                                           Join ac In utility.AccountTypeRepository.GetAccountTypeRecordsByAnalysisId(intAnalysisID, typeDescList)
                                           On a.AcctTypeId Equals ac.AcctTypeId Where a.AnalysisId = ac.AnalysisId
                                           Order By a.SortSequence
                                           Select a.AccountId, a.AcctTypeId, a.AnalysisId, a.AcctDescriptor, a.SortSequence, a.Description, a.Subgrouping, a.TotalType, a.NumberFormat, ac.TypeDesc, ac.ClassDesc
                                         ).ToList()

                    Dim balanceAcc = (From b In utility.BalanceRepository.GetBalanceRecordsByAnalysisId(intAnalysisID)
                                   Join acc In selectedAccounts On b.AccountId Equals acc.AccountId
                                   Where b.AnalysisId = acc.AnalysisId
                                   Select b).ToList()

                    For Each account In selectedAccounts
                        CommonProcedures.AddRow(spdAnalytics, intRowCounter, account.Description, account.AcctDescriptor, account.TypeDesc, strBrowser, 4)
                        Dim intNumberFormat As Integer = account.NumberFormat
                        Select Case intNumberFormat
                            Case 1 To 5
                                deccell.DecimalDigits = 3
                                spdAnalytics.Sheets(0).Cells(intRowCounter, intFirstNumberColumn, intRowCounter, spdAnalytics.Sheets(0).ColumnCount - 1).CellType = deccell
                            Case 99
                                spdAnalytics.Sheets(0).Cells(intRowCounter, intFirstNumberColumn, intRowCounter, spdAnalytics.Sheets(0).ColumnCount - 1).CellType = percentcell
                        End Select
                        Select Case account.AcctDescriptor
                            Case "Heading"
                                CommonProcedures.FormatHeaderRow(spdAnalytics, intRowCounter, intSpanRows, False, 4)
                                spdAnalytics.Sheets(0).SetValue(intRowCounter, 0, "")
                        End Select
                        'save accountID 
                        spdAnalytics.Sheets(0).SetValue(intRowCounter, 5, account.AccountId)
                        'save amounts 
                        If account.AcctDescriptor = "Detail" Then
                            Array.Clear(decPeriodAmts, 0, 12)
                            Array.Clear(decSaveAmts, 0, 5)
                            spdAnalytics.Sheets(0).Cells(intRowCounter, 2).CellType = chkbox
                            Dim accountId As Integer = account.AccountId
                            For Each balance As Balance In (From b In balanceAcc.Where(Function(a) a.AccountId = accountId And a.AnalysisId = intAnalysisID) Select b)
                                '   For Each balance As Balance In (From b In balanceAcc.Where(Function(a) a.AccountId = accountId And a.AnalysisId = intAnalysisID) Select b)
                                For intTypeCol = 0 To 5
                                    Select Case intTypeCol
                                        Case 0
                                            GetH1Amts(balance, decPeriodAmts)
                                        Case 1
                                            GetH2Amts(balance, decPeriodAmts)
                                        Case 2
                                            GetH3Amts(balance, decPeriodAmts)
                                        Case 3
                                            GetH4Amts(balance, decPeriodAmts)
                                        Case 4
                                            GetH5Amts(balance, decPeriodAmts)
                                        Case 5
                                            GetActualAmts(balance, decPeriodAmts)
                                    End Select
                                    Select Case intFormat
                                        Case 7
                                            spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumberColumn + intTypeCol, decPeriodAmts(intPeriod))
                                            decSaveAmts(intTypeCol) = decPeriodAmts(intPeriod)

                                        Case 8
                                            decSaveAmts(intTypeCol) = 0
                                            For intColumnCounter = 0 To intPeriod
                                                decSaveAmts(intTypeCol) += decPeriodAmts(intColumnCounter)
                                            Next
                                            spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumberColumn + intTypeCol, decSaveAmts(intTypeCol))
                                    End Select
                                Next
                            Next

                        End If

                    Next
                End Using
                Logger.Log.Info(String.Format("RAController:LoadSpread7to8 method execution Ends"))
            Catch ex As Exception
                Logger.Log.Error(String.Format("RAController:LoadSpread7to8 method execution ends with Error message: {0}, StackTrace: {1}", ex.Message, ex.StackTrace))
            End Try


        End Sub
        <CustomActionFilter()>
        Public Sub LoadSpread9(ByVal spdAnalytics As FpSpread, ByVal intFormat As Integer, ByVal intPeriod As Integer, ByVal intNumberColumn As Integer, ByVal intFirstNumberColumn As Integer, ByVal intColumnCount As Integer)
            Try
                Logger.Log.Info(String.Format("RAController:LoadSpread5to6 method execution Starts"))

                Using utility As New Utility()
                    Dim intRowCounter As Integer = -1
                    Dim intColumnCounter As Integer
                    Dim strYearType(5) As String
                    Dim strPeriods(2) As String
                    Dim decAccumTotals(12) As Decimal
                    Dim decPeriodAmts(11) As Decimal
                    Dim decSaveAmts(12) As Decimal
                    Dim intSpanRows(1) As Integer
                    Dim decIncAccumTotals(intNumberColumn) As Decimal
                    Dim chkbox As New FarPoint.Web.Spread.CheckBoxCellType
                    Dim percentcell As New FarPoint.Web.Spread.PercentCellType
                    Dim deccell As New FarPoint.Web.Spread.DoubleCellType
                    Dim strBrowser As String = Request.Browser.Browser

                    If (isViewer) Then
                        chkbox.OnClientClick = "return false"
                    Else
                        chkbox.OnClientClick = CommonProcedures.CheckboxClicked(1)
                    End If

                    Dim selectedAccounts = (From a In utility.AccountRepository.GetAccountRecordsByAnalysisId(intAnalysisID)
                                           Join ac In utility.AccountTypeRepository.GetAccountTypeRecordsByAnalysisId(intAnalysisID, typeDescList)
                                           On a.AcctTypeId Equals ac.AcctTypeId Where a.AnalysisId = ac.AnalysisId
                                           Order By a.SortSequence
                                           Select a.AccountId, a.AcctTypeId, a.AnalysisId, a.AcctDescriptor, a.SortSequence, a.Description, a.Subgrouping, a.TotalType, a.NumberFormat, ac.TypeDesc, ac.ClassDesc
                                         ).ToList()

                    Dim balanceAcc = (From b In utility.BalanceRepository.GetBalanceRecordsByAnalysisId(intAnalysisID)
                                   Join acc In selectedAccounts On b.AccountId Equals acc.AccountId
                                   Where b.AnalysisId = acc.AnalysisId
                                   Select b).ToList()


                    For Each account In selectedAccounts
                        'Dim accountTypeId As Integer = account.AcctTypeId
                        'Dim acctType = (From a In utility.AccountTypeRepository.GetAccountTypes.Where(Function(at) at.AcctTypeId = accountTypeId) Select a).FirstOrDefault()
                        CommonProcedures.AddRow(spdAnalytics, intRowCounter, account.Description, account.AcctDescriptor, account.TypeDesc, strBrowser, 4)
                        Dim intNumberFormat As Integer = account.NumberFormat
                        Select Case intNumberFormat
                            Case 1 To 5
                                deccell.DecimalDigits = 3
                                spdAnalytics.Sheets(0).Cells(intRowCounter, intFirstNumberColumn, intRowCounter, spdAnalytics.Sheets(0).ColumnCount - 1).CellType = deccell
                            Case 99
                                spdAnalytics.Sheets(0).Cells(intRowCounter, intFirstNumberColumn, intRowCounter, spdAnalytics.Sheets(0).ColumnCount - 1).CellType = percentcell
                        End Select
                        Select Case account.AcctDescriptor
                            Case "Heading"
                                CommonProcedures.FormatHeaderRow(spdAnalytics, intRowCounter, intSpanRows, False, 4)
                                spdAnalytics.Sheets(0).SetValue(intRowCounter, 0, "")
                        End Select
                        'save accountID 
                        spdAnalytics.Sheets(0).SetValue(intRowCounter, 5, account.AccountId)
                        'save amounts
                        If account.AcctDescriptor = "Detail" Then
                            Array.Clear(decPeriodAmts, 0, 12)
                            Array.Clear(decSaveAmts, 0, 13)
                            spdAnalytics.Sheets(0).Cells(intRowCounter, 2).CellType = chkbox
                            Dim accountId As Integer = account.AccountId
                            For Each balance As Balance In (From b In balanceAcc.Where(Function(a) a.AccountId = accountId And a.AnalysisId = intAnalysisID) Select b)
                                If intPeriod < 11 Then
                                    GetBudgetAmts(balance, decPeriodAmts)
                                    For intColumnCounter = intPeriod + 1 To 11
                                        spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumberColumn + intColumnCounter - (intPeriod + 1), decPeriodAmts(intColumnCounter))
                                        decSaveAmts(intColumnCounter) = decPeriodAmts(intColumnCounter)
                                    Next
                                End If
                                GetBudget2Amts(balance, decPeriodAmts)
                                For intColumnCounter = 0 To intPeriod
                                    spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumberColumn + intColumnCounter + (11 - intPeriod), decPeriodAmts(intColumnCounter))
                                    decSaveAmts(intColumnCounter) = decPeriodAmts(intColumnCounter)
                                Next
                            Next
                            'If intFormat = 6 Then
                            '    spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumberColumn + 12, decSaveAmts(12))
                            'End If

                        End If

                    Next
                End Using
                Logger.Log.Info(String.Format("RAController:LoadSpread5to6 method execution Ends"))
            Catch ex As Exception
                Logger.Log.Error(String.Format("RAController:LoadSpread5to6 method execution ends with Error message: {0}, StackTrace: {1}", ex.Message, ex.StackTrace))
            End Try


        End Sub
        Public Sub LoadSpread10(ByVal spdAnalytics As FpSpread, ByVal intFormat As Integer, ByVal intPeriod As Integer, ByVal intNumberColumn As Integer, ByVal intFirstNumberColumn As Integer, ByVal intColumnCount As Integer, ByVal intHPeriod As Integer)
            Try
                Logger.Log.Info(String.Format("RAController:LoadSpread5to6 method execution Starts"))

                Using utility As New Utility()
                    Dim intRowCounter As Integer = -1
                    Dim intColumnCounter As Integer
                    Dim strYearType(5) As String
                    Dim strPeriods(2) As String
                    Dim decAccumTotals(12) As Decimal
                    Dim decPeriodAmts(11) As Decimal
                    Dim decSaveAmts(12) As Decimal
                    Dim intSpanRows(1) As Integer
                    Dim decIncAccumTotals(intNumberColumn) As Decimal
                    Dim chkbox As New FarPoint.Web.Spread.CheckBoxCellType
                    Dim percentcell As New FarPoint.Web.Spread.PercentCellType
                    Dim deccell As New FarPoint.Web.Spread.DoubleCellType
                    Dim strBrowser As String = Request.Browser.Browser

                    If (isViewer) Then
                        chkbox.OnClientClick = "return false"
                    Else
                        chkbox.OnClientClick = CommonProcedures.CheckboxClicked(1)
                    End If

                    Dim selectedAccounts = (From a In utility.AccountRepository.GetAccountRecordsByAnalysisId(intAnalysisID)
                                           Join ac In utility.AccountTypeRepository.GetAccountTypeRecordsByAnalysisId(intAnalysisID, typeDescList)
                                           On a.AcctTypeId Equals ac.AcctTypeId Where a.AnalysisId = ac.AnalysisId
                                           Order By a.SortSequence
                                           Select a.AccountId, a.AcctTypeId, a.AnalysisId, a.AcctDescriptor, a.SortSequence, a.Description, a.Subgrouping, a.TotalType, a.NumberFormat, ac.TypeDesc, ac.ClassDesc
                                         ).ToList()

                    Dim balanceAcc = (From b In utility.BalanceRepository.GetBalanceRecordsByAnalysisId(intAnalysisID)
                                   Join acc In selectedAccounts On b.AccountId Equals acc.AccountId
                                   Where b.AnalysisId = acc.AnalysisId
                                   Select b).ToList()


                    For Each account In selectedAccounts
                        'Dim accountTypeId As Integer = account.AcctTypeId
                        'Dim acctType = (From a In utility.AccountTypeRepository.GetAccountTypes.Where(Function(at) at.AcctTypeId = accountTypeId) Select a).FirstOrDefault()
                        CommonProcedures.AddRow(spdAnalytics, intRowCounter, account.Description, account.AcctDescriptor, account.TypeDesc, strBrowser, 4)
                        Dim intNumberFormat As Integer = account.NumberFormat
                        Select Case intNumberFormat
                            Case 1 To 5
                                deccell.DecimalDigits = 3
                                spdAnalytics.Sheets(0).Cells(intRowCounter, intFirstNumberColumn, intRowCounter, spdAnalytics.Sheets(0).ColumnCount - 1).CellType = deccell
                            Case 99
                                spdAnalytics.Sheets(0).Cells(intRowCounter, intFirstNumberColumn, intRowCounter, spdAnalytics.Sheets(0).ColumnCount - 1).CellType = percentcell
                        End Select
                        Select Case account.AcctDescriptor
                            Case "Heading"
                                CommonProcedures.FormatHeaderRow(spdAnalytics, intRowCounter, intSpanRows, False, 4)
                                spdAnalytics.Sheets(0).SetValue(intRowCounter, 0, "")
                        End Select
                        'save accountID 
                        spdAnalytics.Sheets(0).SetValue(intRowCounter, 5, account.AccountId)
                        'save amounts
                        If account.AcctDescriptor = "Detail" Then
                            Array.Clear(decPeriodAmts, 0, 12)
                            Array.Clear(decSaveAmts, 0, 13)
                            spdAnalytics.Sheets(0).Cells(intRowCounter, 2).CellType = chkbox
                            Dim accountId As Integer = account.AccountId
                            For Each balance As Balance In (From b In balanceAcc.Where(Function(a) a.AccountId = accountId And a.AnalysisId = intAnalysisID) Select b)
                                Select Case intHPeriod
                                    Case 5
                                        GetH1Amts(balance, decPeriodAmts)
                                    Case 4
                                        GetH2Amts(balance, decPeriodAmts)
                                    Case 3
                                        GetH3Amts(balance, decPeriodAmts)
                                    Case 2
                                        GetH4Amts(balance, decPeriodAmts)
                                    Case 1
                                        GetH5Amts(balance, decPeriodAmts)
                                    Case 0
                                        If blnArchBudget = False Then
                                            GetBudgetAmts(balance, decPeriodAmts)
                                        Else
                                            GetArchivedBudgetAmts(balance, decPeriodAmts)
                                        End If
                                End Select
                                For intColumnCounter = 0 To 11
                                    spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumberColumn + intColumnCounter, decPeriodAmts(intColumnCounter))
                                    decSaveAmts(intColumnCounter) = decPeriodAmts(intColumnCounter)
                                Next
                            Next
                        End If

                    Next
                End Using
                Logger.Log.Info(String.Format("RAController:LoadSpread5to6 method execution Ends"))
            Catch ex As Exception
                Logger.Log.Error(String.Format("RAController:LoadSpread5to6 method execution ends with Error message: {0}, StackTrace: {1}", ex.Message, ex.StackTrace))
            End Try


        End Sub

        <HttpPost()>
        <CustomActionFilter()>
        Function AddViews(viewname As String, accountid As String, ByVal formValues As FormValues) As ActionResult
            Try

                Dim selectedAnalysis As Integer
                Logger.Log.Info(String.Format("RAController:AddViews method execution Starts"))
                If (Not Session("UserType") Is Nothing) And (Not Session("UserInfo") Is Nothing) Then
                    UserInfo = DirectCast(Session("UserInfo"), User)
                Else
                    Return RedirectToAction("Index", "Home")
                End If

                If (UserInfo.AnalysisId > 0) Then
                    selectedAnalysis = UserInfo.AnalysisId
                Else
                    If (Not Session("SelectedAnalysisFromDropdown") Is Nothing) Then
                        selectedAnalysis = Session("SelectedAnalysisFromDropdown")
                    Else
                        selectedAnalysis = -1
                    End If
                End If
                Dim controller As String = "RA"
                If formValues.intFormat = 7 Then
                    formValues.blnShowBudget = True
                End If
                formValues.intFormat = CommonProcedures.FormatIDtoSave(formValues.intFormat)
                Dim result As Integer = Utility.SaveViews(viewname, controller, accountid, selectedAnalysis, formValues)
                Logger.Log.Info(String.Format("RAController:AddViews method execution Ends"))
                Return RedirectToAction("Index")
            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Ratios : Error occured while Add View - ", ex.Message)
                Logger.Log.Error(String.Format("RAController:AddViews method execution ends with Error message: {0}, StackTrace: {1}", ex.Message, ex.StackTrace))
                Return RedirectToAction("Index")
            End Try

        End Function

        <HttpGet()>
        <CustomActionFilter()>
        Function Viewer(Id As Integer, <MvcSpread("spdAnalytics")> ByVal spdAnalytics As FpSpread, ByVal formValues As FormValues) As ActionResult
            Dim highChart As ReportHighChart = New ReportHighChart
            isViewer = True
            Dim savedView As SaveView = saveViewRepository.GetSavedViewDetail(Id)
            Dim setupCountObj As SetupCount = Nothing
            Dim selectedAnalysis As Integer = savedView.analysisID
            Try
                Logger.Log.Info(String.Format("RAController Viewer (HttpGet) method execution starts"))
                blnArchBudget = DirectCast(Session("BudgetArch"), Boolean)
                If (Not Session("UserType") Is Nothing) And (Not Session("UserInfo") Is Nothing) Then
                    UserInfo = DirectCast(Session("UserInfo"), User)
                    sPeriod = Session("sPeriod")
                    sFormat = Session("sFormat")

                    If (UserInfo.AnalysisId = 0) Then
                        Return RedirectToAction("Index", "Dashboard")
                    Else
                        intAnalysisID = savedView.analysisID 'UserInfo.AnalysisId
                        intFirstFiscal = UserInfo.fiscalMonthOfSelectedCompany
                    End If
                End If

                setupCountObj = Utility.CheckRequiredSetupCount(UserInfo, selectedAnalysis)
                ViewBag.Setup = setupCountObj

                Dim intCtr As Integer
                Dim periods As List(Of SelectListItem) = New List(Of SelectListItem)
                'intNumberofPeriods = NumberofPeriods(sPeriod, intFirstFiscal)
                For intCtr = 0 To 11
                    periods.Add(New SelectListItem With {.Text = CommonProcedures.GetMonth(intCtr, intFirstFiscal), .Value = intCtr + 1, .Selected = False})
                Next
                ViewData("Periods") = New SelectList(periods, "value", "text", "May")

                ViewData("blnHighVar") = savedView.highlightva
                ViewData("blnShowPercent") = savedView.showaspercent
                ViewData("intPeriod") = sPeriod + 1
                ViewData("intFormat") = savedView.format + 1
                ViewData("intChartType") = savedView.charttype
                ViewData("blnShowTrend") = savedView.trend
                ViewData("blnShowBudget") = savedView.showbudget
                ViewData("blnUseFilter") = savedView.filter
                ViewData("intDept") = 1
                ViewData("Postback") = False
                Session("intPeriod") = sPeriod + 1
                Dim accID As String = String.Empty
                Dim array() As Nullable(Of Integer) = {savedView.account1, savedView.account2, savedView.account3, savedView.account4, savedView.account5, savedView.account6, savedView.account7, savedView.account8, savedView.account9, savedView.account10}
                For AcId As Nullable(Of Integer) = 0 To array.Length - 1
                    If (Not IsNothing(array(AcId))) Then
                        accID = accID & array(AcId).ToString() & " "
                    End If
                Next
                accID = accID.TrimEnd(" ")
                ViewData("AccountID") = accID
                Logger.Log.Info(String.Format("RAController Viewer (HttpGet) method execution Ends"))
            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Error occured while loading Ratios Chart Viewer-", ex.Message)
                Logger.Log.Error(String.Format("RAController Viewer (HttpGet) method execution ends with Error message: {0}, StackTrace: {1}", ex.Message, ex.StackTrace))
            End Try
            Return View("Viewer",highChart)
        End Function

        <HttpPost()> _
        <CustomActionFilter()>
        Function Viewer(Id As Integer, <MvcSpread("spdAnalytics")> ByVal spdAnalytics As FpSpread, <MvcSpread("spdChart")> ByVal spdChart As FpSpread, ByVal formValues As FormValues) As ActionResult
            Dim highChart As ReportHighChart = New ReportHighChart
            Dim setupCountObj As SetupCount
            isViewer = True
            Dim savedView As SaveView = saveViewRepository.GetSavedViewDetail(Id)
            Dim selectedAnalysis As Integer = savedView.analysisID

            Logger.Log.Info(String.Format("RAController Viewer (HttpPost) method execution Starts"))
            Try
                blnArchBudget = DirectCast(Session("BudgetArch"), Boolean)
                If (Not Session("UserType") Is Nothing) And (Not Session("UserInfo") Is Nothing) Then
                    UserInfo = DirectCast(Session("UserInfo"), User)
                    If (UserInfo.AnalysisId = 0) Then
                        Return RedirectToAction("Index", "Dashboard")
                    Else
                        intAnalysisID = savedView.analysisID 'UserInfo.AnalysisId
                        intFirstFiscal = UserInfo.fiscalMonthOfSelectedCompany
                    End If
                End If

                setupCountObj = Utility.CheckRequiredSetupCount(UserInfo, selectedAnalysis)
                ViewBag.Setup = setupCountObj

                'If (spdChart.Sheets(0).Charts.Count > 0) Then
                '    spdChart.Sheets(0).Charts.Remove(spdChart.Sheets(0).Charts(0))
                'End If

            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Error occured while loading Ratios Viewer - ", ex.Message)
                Logger.Log.Error(String.Format("RAController Viewer (HttpPost) method execution ends with Error message: {0}, StackTrace: {1}", ex.Message, ex.StackTrace))
            End Try
            Try
                spdAnalytics.Sheets(0).RowCount = 0
                savedView.format = CommonProcedures.ConvertSavedFormatID(savedView.format, savedView.showbudget)
                SetRASpreadProperties(spdAnalytics, savedView.format - 1, formValues.intPeriod - 1, formValues.intHPeriod)

                If (savedView.charttype <> 1 And Not (savedView.account1 Is Nothing)) Then
                    highChart = Charts.BuildChartWithHighChart(Me.ControllerContext.Controller, spdAnalytics, savedView.charttype, savedView.format - 1, intFirstNumberColumn, savedView.showaspercent, savedView.trend, False)
                    'Charts.BuildChart(Me.ControllerContext.Controller, spdChart, spdAnalytics, savedView.charttype, savedView.format - 1, intFirstNumberColumn, savedView.showaspercent, savedView.trend, False)
                    ViewData("blnUpdateChart") = True
                End If

                Dim periods As List(Of SelectListItem) = New List(Of SelectListItem)
                'intNumberofPeriods = NumberofPeriods(sPeriod, intFirstFiscal)
                'For intCtr = 0 To intNumberofPeriods
                For intCtr = 0 To 11
                    periods.Add(New SelectListItem With {.Text = CommonProcedures.GetMonth(intCtr, intFirstFiscal), .Value = intCtr + 1, .Selected = False})
                Next
                ViewData("Periods") = New SelectList(periods, "value", "text", "May")

                ViewData("FilterOn") = savedView.filteron
                ViewData("ChartType") = savedView.charttype
                ViewData("blnHighVar") = savedView.highlightva
                ViewData("blnShowPercent") = savedView.showaspercent
                ViewData("intPeriod") = formValues.intPeriod - 1
                ViewData("Period") = formValues.intPeriod - 1
                ViewData("intFormat") = savedView.format
                ViewData("intChartType") = savedView.charttype
                ViewData("blnShowTrend") = savedView.trend
                ViewData("blnShowBudget") = savedView.showbudget
                ViewData("blnUseFilter") = savedView.filter
                ViewData("intDept") = 1
                ViewData("postback") = True
                Dim accID As String = String.Empty
                Dim array() As Nullable(Of Integer) = {savedView.account1, savedView.account2, savedView.account3, savedView.account4, savedView.account5, savedView.account6, savedView.account7, savedView.account8, savedView.account9, savedView.account10}
                For AcId As Nullable(Of Integer) = 0 To array.Length - 1
                    If (Not IsNothing(array(AcId))) Then
                        accID = accID & array(AcId).ToString() & " "
                    End If
                Next
                accID = accID.TrimEnd(" ")
                ViewData("AccountID") = accID

                Session("sPeriod") = formValues.intPeriod - 1
                '    Session("sFormat") = savedView.format - 1
                Logger.Log.Info(String.Format("RAController Viewer (HttpPost) method execution Ends"))
            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Error occured while loading Ratios Viewer - ", ex.Message)
                Logger.Log.Error(String.Format("RAController Viewer (HttpPost) method execution ends with Error message: {0}, StackTrace: {1}", ex.Message, ex.StackTrace))
            End Try
            Return View("Viewer",highChart)
        End Function

        Protected Overrides Sub Dispose(disposing As Boolean)
            accountRepository.Dispose()
            accountTypeRepository.Dispose()
            balanceRepository.Dispose()
            chartFormatRepository.Dispose()
            chartTypeRepository.Dispose()

            MyBase.Dispose(disposing)
        End Sub

    End Class
End Namespace
