﻿Imports System.Web.UI.WebControls.Expressions
Imports FarPoint.Mvc.Spread
Imports System.Data.Entity
Namespace onlineAnalytics

    <CustAuthFilter()>
    Public Class DashboardController
        Inherits System.Web.Mvc.Controller

        '
        ' GET: /Dashboard
        Private utility As New Utility()
        Private userRepository As IUserRepository
        Private accountRepository As IAccountRepository
        Private accountTypeRepository As IAccountTypeRepository
        Private dashboardRepository As IDashboardRepository
        Private balanceRepository As IBalanceRepository
        Private dashboardDetailRepository As IDashboardDetailRepository
        Private context As DataAccess

        Private UserType As UserRole
        Private UserInfo As User
        Private dashboardItems As Dashboard
        Private intFirstFiscal As Integer
        Private CustomerId As String = Nothing
        Private intUserID As Integer
        Private intAnalysisID As Integer
        Private intChartWidth As Integer = 297
        Private intChartHeight As Integer = 220
        Private intSelPeriod As Integer
        Private intSaveAnalyisID As Integer
        Private orderIndex As Integer = 0
        Private sPeriod As Integer = Val(DateTime.Today.ToString("MM")) - 2
        ' Private sPeriod As Integer
        Private sFormat As Integer = 1
        Private blnArchBudget As Boolean = False
        Private typeDescList() As String = {"revenue", "expense"}


        Public Sub New()
            Me.userRepository = New UserRepository(New DataAccess())
            Me.dashboardRepository = New DashboardRepository(New DataAccess())
            Me.accountRepository = New AccountRepository(New DataAccess())
            Me.accountTypeRepository = New AccountTypeRepository(New DataAccess())
            Me.balanceRepository = New BalanceRepository(New DataAccess())
            Me.dashboardDetailRepository = New DashboardDetailRepository(New DataAccess())
        End Sub

        Public Sub New(userRepository As IUserRepository)
            Me.userRepository = userRepository
        End Sub
        Public Sub New(dashboardRepository As IDashboardRepository)
            Me.dashboardRepository = dashboardRepository
        End Sub

        Public Sub New(accountRepository As IAccountRepository)
            Me.accountRepository = accountRepository
        End Sub

        Public Sub New(accountTypeRepository As IAccountTypeRepository)
            Me.accountTypeRepository = accountTypeRepository
        End Sub

        Public Sub New(balanceRepository As IBalanceRepository)
            Me.balanceRepository = balanceRepository
        End Sub

        Public Sub New(dashboardDetailRepository As IDashboardDetailRepository)
            Me.dashboardDetailRepository = dashboardDetailRepository
        End Sub

        Private Function dashboardOrderIndex(ByRef value As Integer) As Integer
            orderIndex = value + 1
            Return orderIndex
        End Function


        <AcceptVerbs(HttpVerbs.Get)>
        Function IsSessionAlive() As JsonResult

            Dim data As Boolean
            If (Session.IsNewSession) Then
                data = False
            Else
                data = True
            End If

            Return Json(data, JsonRequestBehavior.AllowGet)
        End Function

        <CustomActionFilter()>
        Function Index(<MvcSpread("spdDashboard")> spdDashboard As FpSpread, <MvcSpread("spdAnalysisDashboard")> spdAnalysisDashboard As FpSpread) As ActionResult

            Try
                Logger.Log.Info(String.Format("DashboardController Index (HttpGet) method execution starts"))

                If (Not Session("UserInfo") Is Nothing) Then
                    UserInfo = DirectCast(Session("UserInfo"), User)
                    If UserInfo.isSharedAnalysis Then
                        UserInfo.currentTabIndex = 1
                    Else
                        UserInfo.currentTabIndex = 0
                    End If
                End If


                Me.IndexViewer(sPeriod, False)

                Return View()
            Catch ex As Exception
                Me.IndexViewer(sPeriod, False)
                TempData("ErrorMessage") = String.Concat("Error occured during processing dashboard index page -", ex.Message)
                Logger.Log.Error(String.Format("DashboardController Index (HttpGet) - Error occured during processing dashboard index page - with Message- {0} " + Environment.NewLine + "Stack Trace: {1} ", ex.Message, ex.StackTrace))
            Finally
                Logger.Log.Info(String.Format("DashboardController Index (HttpGet) Execution Ended"))
            End Try
            Return View()
        End Function


        <HttpPost()> _
      <CustomActionFilter()>
        Function Index(<MvcSpread("spdDashboard")> spdDashboard As FpSpread, ByVal formValues As FormValues, <MvcSpread("spdAnalysisDashboard")> spdAnalysisDashboard As FpSpread) As ActionResult
            Logger.Log.Info(String.Format("Dashboard postback Index Execution Started"))
            Try
                Session("sPeriod") = formValues.intPeriod - 1
                Me.IndexViewer(formValues.intPeriod - 1, True)

                Return View()
            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Error occured during processing postback dashboard index page -", ex.Message)
                Logger.Log.Error(String.Format("Error occured during processing dashboard postback index page - with Message- {0} " + Environment.NewLine + "Stack Trace: {1} ", ex.Message, ex.StackTrace))
            Finally
                Logger.Log.Info(String.Format("Dashboard postback Index Execution Ended"))
            End Try
            Return View()
        End Function

        Private Sub IndexViewer(Optional period As Integer = 0, Optional post As Boolean = False)

            Logger.Log.Info(String.Format("Dashboard IndexViewer Execution Started"))
            Dim setupCountObj As SetupCount
            Dim selectedAnalysis As Integer
            Dim intHistYears As Integer
            Dim intCurrentYear As Integer = DateTime.Today.Year
            Dim intFirstYear As Integer

            If (Not Session("UserType") Is Nothing) And (Not Session("UserInfo") Is Nothing) Then
                UserType = DirectCast(Session("UserType"), UserRole)
                UserInfo = DirectCast(Session("UserInfo"), User)
                intAnalysisID = UserInfo.AnalysisId
                intUserID = UserInfo.UserId
                intFirstFiscal = UserInfo.fiscalMonthOfSelectedCompany
                intFirstYear = IIf(UserInfo.FirstYear <> Nothing And UserInfo.FirstYear > 0, UserInfo.FirstYear, DateTime.Now.Year())
            End If
            Dim intSavedYear As Integer = UserInfo.FirstYear
            Session("BudgetArch") = ArchiveBudgetPresent(intHistYears)
            Session("NumberofHYears") = intHistYears
            Session("ShowBudget") = False
            Session("ShowasPercent") = False

            blnArchBudget = DirectCast(Session("BudgetArch"), Boolean)
            Logger.Log.Info(String.Format("Dashboard CommonProcedures.NumberofPeriods Execution Begin"))
            '   Dim intNumberofPeriods As Integer = CommonProcedures.NumberofPeriods(intCurrentMonth, intFirstFiscal)
            Logger.Log.Info(String.Format("Dashboard CommonProcedures.NumberofPeriods Execution End"))
            If (Not Session("sPeriod") Is Nothing) Then
                intSelPeriod = Session("sPeriod")
                sFormat = Session("sFormat")
            Else
                If intFirstFiscal = 1 Then
                    If intCurrentYear = intFirstYear Then
                        If period = -1 Then
                            intSelPeriod = 0
                        Else
                            intSelPeriod = period
                        End If

                    Else
                        intSelPeriod = 11
                    End If
                Else
                    If intCurrentYear = intFirstYear Then
                        If Val(DateTime.Today.ToString("MM")) > intFirstFiscal Then
                            intSelPeriod = 11
                        Else
                            intSelPeriod = (12 - intFirstFiscal) + sPeriod + 1
                        End If
                    ElseIf intCurrentYear > intFirstYear Then
                        intSelPeriod = 11
                    Else
                        intSelPeriod = (12 - intFirstFiscal) + sPeriod + 1
                        If intSelPeriod > 12 Then
                            intSelPeriod -= 12
                        End If
                    End If

                End If
            End If
            If (UserInfo.AnalysisId > 0) Then
                selectedAnalysis = UserInfo.AnalysisId
            Else
                If (Not Session("SelectedAnalysisFromDropdown") Is Nothing) Then
                    selectedAnalysis = Session("SelectedAnalysisFromDropdown")
                Else
                    selectedAnalysis = -1
                End If
            End If

            Logger.Log.Info(String.Format("Dashboard CheckRequiredSetupCount Execution Begin"))
            setupCountObj = utility.CheckRequiredSetupCount(UserInfo, selectedAnalysis)
            ViewBag.Setup = setupCountObj
            Logger.Log.Info(String.Format("Dashboard CheckRequiredSetupCount Execution End"))

            If (setupCountObj.countDataUploadedOfSelectedAnalysis > 0 And setupCountObj.countOfDashboardItem = 0) Then
                orderIndex = 0
                Logger.Log.Info(String.Format("Dashboard selectedAccumAccounts Query Execution Begin"))
                Dim selectedAccumAccounts = (From a In utility.AccountRepository.GetAccountRecordsByAnalysisId(intAnalysisID, New String() {"total"})
                                             Join at In utility.AccountTypeRepository.GetAccountTypeRecordsByAnalysisId(intAnalysisID, New String() {"revenue"})
                                             On a.AcctTypeId Equals at.AcctTypeId And a.AnalysisId Equals at.AnalysisId
                                             Order By a.SortSequence
                                             Select a.AccountId, a.AcctTypeId, a.AnalysisId, a.AcctDescriptor, a.SortSequence, a.Description, a.Subgrouping, at.TypeDesc, at.ClassDesc
                                             ).ToList()
                Logger.Log.Info(String.Format("Dashboard selectedAccumAccounts Query Execution End"))

                If (selectedAccumAccounts.Count > 0) Then
                    context = New DataAccess()
                    For Each account In selectedAccumAccounts
                        Dim dashboardItemList As New List(Of Dashboard)() From {
                                   New Dashboard() With {.Order = dashboardOrderIndex(orderIndex), .DashDescription = account.Description, .ChartFormatId = 2, .ChartTypeId = 2, .ShowAsPercent = 0, .ShowTrendline = 0, .ShowBudget = 1, .Period1Goal = 0, .GoalGrowthRate = 0, .AccountId = account.AccountId, .UserId = UserInfo.UserId, .AnalysisId = intAnalysisID, .Option1 = 0, .Option2 = 0, .Option3 = 0, .IsValid = True, .CreatedBy = UserInfo.UserId, .UpdatedBy = UserInfo.UserId, .ChartFormatTypeId = 1}, _
                                   New Dashboard() With {.Order = dashboardOrderIndex(orderIndex), .DashDescription = account.Description, .ChartFormatId = 6, .ChartTypeId = 4, .ShowAsPercent = 0, .ShowTrendline = 0, .ShowBudget = 1, .Period1Goal = 0, .GoalGrowthRate = 0, .AccountId = account.AccountId, .UserId = UserInfo.UserId, .AnalysisId = intAnalysisID, .Option1 = 0, .Option2 = 0, .Option3 = 0, .IsValid = True, .CreatedBy = UserInfo.UserId, .UpdatedBy = UserInfo.UserId, .ChartFormatTypeId = 1}, _
                                   New Dashboard() With {.Order = dashboardOrderIndex(orderIndex), .DashDescription = account.Description, .ChartFormatId = 7, .ChartTypeId = 4, .ShowAsPercent = 0, .ShowTrendline = 0, .ShowBudget = 1, .Period1Goal = 0, .GoalGrowthRate = 0, .AccountId = account.AccountId, .UserId = UserInfo.UserId, .AnalysisId = intAnalysisID, .Option1 = 0, .Option2 = 0, .Option3 = 0, .IsValid = True, .CreatedBy = UserInfo.UserId, .UpdatedBy = UserInfo.UserId, .ChartFormatTypeId = 1} _
                   }
                        dashboardItemList.ForEach(Function(u) context.Dashboards.Add(u))
                        context.SaveChanges()

                        Dim dashboardDetailItemList As New List(Of DashboardDetail)() From {
                                  New DashboardDetail() With {.DashboardId = dashboardItemList(0).DashboardId, .DashboardDescription = account.Description, .AccountId = account.AccountId}, _
                                  New DashboardDetail() With {.DashboardId = dashboardItemList(1).DashboardId, .DashboardDescription = account.Description, .AccountId = account.AccountId}, _
                                  New DashboardDetail() With {.DashboardId = dashboardItemList(2).DashboardId, .DashboardDescription = account.Description, .AccountId = account.AccountId} _
                  }
                        dashboardDetailItemList.ForEach(Function(u) context.DashboardDetails.Add(u))
                        context.SaveChanges()

                    Next
                End If

                Logger.Log.Info(String.Format("Dashboard selectedAccumAccounts1 Query Execution Begin"))
                Dim selectedAccumAccounts1 = (From a In utility.AccountRepository.GetAccountRecordsByAnalysisId(intAnalysisID, New String() {"nitotal"})
                                          Join at In utility.AccountTypeRepository.GetAccountTypeRecordsByAnalysisId(intAnalysisID)
                                          On a.AcctTypeId Equals at.AcctTypeId And a.AnalysisId Equals at.AnalysisId
                                          Order By a.SortSequence
                                          Select a.AccountId, a.AcctTypeId, a.AnalysisId, a.AcctDescriptor, a.SortSequence, a.Description, a.Subgrouping, at.TypeDesc, at.ClassDesc
                                          ).ToList()
                Logger.Log.Info(String.Format("Dashboard selectedAccumAccounts1 Query Execution End"))

                If (selectedAccumAccounts1.Count > 0) Then
                    context = New DataAccess()
                    For Each account In selectedAccumAccounts1
                        Dim dashboardItemList As New List(Of Dashboard)() From {
                                   New Dashboard() With {.Order = dashboardOrderIndex(orderIndex), .DashDescription = account.Description, .ChartFormatId = 2, .ChartTypeId = 2, .ShowAsPercent = 0, .ShowTrendline = 0, .ShowBudget = 1, .Period1Goal = 0, .GoalGrowthRate = 0, .AccountId = account.AccountId, .UserId = UserInfo.UserId, .AnalysisId = intAnalysisID, .Option1 = 0, .Option2 = 0, .Option3 = 0, .IsValid = True, .CreatedBy = UserInfo.UserId, .UpdatedBy = UserInfo.UserId, .ChartFormatTypeId = 1}, _
                                   New Dashboard() With {.Order = dashboardOrderIndex(orderIndex), .DashDescription = account.Description, .ChartFormatId = 6, .ChartTypeId = 4, .ShowAsPercent = 0, .ShowTrendline = 0, .ShowBudget = 1, .Period1Goal = 0, .GoalGrowthRate = 0, .AccountId = account.AccountId, .UserId = UserInfo.UserId, .AnalysisId = intAnalysisID, .Option1 = 0, .Option2 = 0, .Option3 = 0, .IsValid = True, .CreatedBy = UserInfo.UserId, .UpdatedBy = UserInfo.UserId, .ChartFormatTypeId = 1}, _
                                   New Dashboard() With {.Order = dashboardOrderIndex(orderIndex), .DashDescription = account.Description, .ChartFormatId = 7, .ChartTypeId = 4, .ShowAsPercent = 0, .ShowTrendline = 0, .ShowBudget = 1, .Period1Goal = 0, .GoalGrowthRate = 0, .AccountId = account.AccountId, .UserId = UserInfo.UserId, .AnalysisId = intAnalysisID, .Option1 = 0, .Option2 = 0, .Option3 = 0, .IsValid = True, .CreatedBy = UserInfo.UserId, .UpdatedBy = UserInfo.UserId, .ChartFormatTypeId = 1} _
                   }
                        dashboardItemList.ForEach(Function(u) context.Dashboards.Add(u))
                        context.SaveChanges()

                        Dim dashboardDetailItemList As New List(Of DashboardDetail)() From {
                                  New DashboardDetail() With {.DashboardId = dashboardItemList(0).DashboardId, .DashboardDescription = account.Description, .AccountId = account.AccountId}, _
                                  New DashboardDetail() With {.DashboardId = dashboardItemList(1).DashboardId, .DashboardDescription = account.Description, .AccountId = account.AccountId}, _
                                  New DashboardDetail() With {.DashboardId = dashboardItemList(2).DashboardId, .DashboardDescription = account.Description, .AccountId = account.AccountId} _
                  }
                        dashboardDetailItemList.ForEach(Function(u) context.DashboardDetails.Add(u))
                        context.SaveChanges()

                    Next
                End If

            End If
            If UserInfo.isSharedAnalysis Then
                If (setupCountObj.countDataUploadedOfSelectedAnalysis > 0 And setupCountObj.countOfDashboardSharedItem = 0) Then
                    orderIndex = 0
                    Logger.Log.Info(String.Format("Shared Dashboard selectedAccumAccounts Query Execution Begin"))
                    Dim selectedAccumAccounts = (From a In utility.AccountRepository.GetAccountRecordsByAnalysisId(intAnalysisID, New String() {"total"})
                                                 Join at In utility.AccountTypeRepository.GetAccountTypeRecordsByAnalysisId(intAnalysisID, New String() {"revenue"})
                                                 On a.AcctTypeId Equals at.AcctTypeId And a.AnalysisId Equals at.AnalysisId
                                                 Order By a.SortSequence
                                                 Select a.AccountId, a.AcctTypeId, a.AnalysisId, a.AcctDescriptor, a.SortSequence, a.Description, a.Subgrouping, at.TypeDesc, at.ClassDesc
                                                 ).ToList()
                    Logger.Log.Info(String.Format("Shared Dashboard selectedAccumAccounts Query Execution End"))

                    If (selectedAccumAccounts.Count > 0) Then
                        context = New DataAccess()
                        For Each account In selectedAccumAccounts
                            Dim dashboardItemList As New List(Of Dashboard)() From {
                                       New Dashboard() With {.Order = dashboardOrderIndex(orderIndex), .DashDescription = account.Description, .ChartFormatId = 2, .ChartTypeId = 2, .ShowAsPercent = 0, .ShowTrendline = 0, .ShowBudget = 1, .Period1Goal = 0, .GoalGrowthRate = 0, .AccountId = account.AccountId, .UserId = "-999", .AnalysisId = intAnalysisID, .Option1 = 0, .Option2 = 0, .Option3 = 0, .IsValid = True, .CreatedBy = UserInfo.UserId, .UpdatedBy = UserInfo.UserId, .ChartFormatTypeId = 1}, _
                                       New Dashboard() With {.Order = dashboardOrderIndex(orderIndex), .DashDescription = account.Description, .ChartFormatId = 6, .ChartTypeId = 4, .ShowAsPercent = 0, .ShowTrendline = 0, .ShowBudget = 1, .Period1Goal = 0, .GoalGrowthRate = 0, .AccountId = account.AccountId, .UserId = "-999", .AnalysisId = intAnalysisID, .Option1 = 0, .Option2 = 0, .Option3 = 0, .IsValid = True, .CreatedBy = UserInfo.UserId, .UpdatedBy = UserInfo.UserId, .ChartFormatTypeId = 1}, _
                                       New Dashboard() With {.Order = dashboardOrderIndex(orderIndex), .DashDescription = account.Description, .ChartFormatId = 7, .ChartTypeId = 4, .ShowAsPercent = 0, .ShowTrendline = 0, .ShowBudget = 1, .Period1Goal = 0, .GoalGrowthRate = 0, .AccountId = account.AccountId, .UserId = "-999", .AnalysisId = intAnalysisID, .Option1 = 0, .Option2 = 0, .Option3 = 0, .IsValid = True, .CreatedBy = UserInfo.UserId, .UpdatedBy = UserInfo.UserId, .ChartFormatTypeId = 1} _
                       }
                            dashboardItemList.ForEach(Function(u) context.Dashboards.Add(u))
                            context.SaveChanges()

                            Dim dashboardDetailItemList As New List(Of DashboardDetail)() From {
                                  New DashboardDetail() With {.DashboardId = dashboardItemList(0).DashboardId, .DashboardDescription = account.Description, .AccountId = account.AccountId}, _
                                  New DashboardDetail() With {.DashboardId = dashboardItemList(1).DashboardId, .DashboardDescription = account.Description, .AccountId = account.AccountId}, _
                                  New DashboardDetail() With {.DashboardId = dashboardItemList(2).DashboardId, .DashboardDescription = account.Description, .AccountId = account.AccountId} _
                  }
                            dashboardDetailItemList.ForEach(Function(u) context.DashboardDetails.Add(u))
                            context.SaveChanges()

                        Next
                    End If

                    Logger.Log.Info(String.Format("Shared Dashboard selectedAccumAccounts1 Query Execution Begin"))
                    Dim selectedAccumAccounts1 = (From a In utility.AccountRepository.GetAccountRecordsByAnalysisId(intAnalysisID, New String() {"nitotal"})
                                              Join at In utility.AccountTypeRepository.GetAccountTypeRecordsByAnalysisId(intAnalysisID)
                                              On a.AcctTypeId Equals at.AcctTypeId And a.AnalysisId Equals at.AnalysisId
                                              Order By a.SortSequence
                                              Select a.AccountId, a.AcctTypeId, a.AnalysisId, a.AcctDescriptor, a.SortSequence, a.Description, a.Subgrouping, at.TypeDesc, at.ClassDesc
                                              ).ToList()
                    Logger.Log.Info(String.Format("Shared Dashboard selectedAccumAccounts1 Query Execution End"))

                    If (selectedAccumAccounts1.Count > 0) Then
                        context = New DataAccess()
                        For Each account In selectedAccumAccounts1
                            Dim dashboardItemList As New List(Of Dashboard)() From {
                                       New Dashboard() With {.Order = dashboardOrderIndex(orderIndex), .DashDescription = account.Description, .ChartFormatId = 2, .ChartTypeId = 2, .ShowAsPercent = 0, .ShowTrendline = 0, .ShowBudget = 1, .Period1Goal = 0, .GoalGrowthRate = 0, .AccountId = account.AccountId, .UserId = "-999", .AnalysisId = intAnalysisID, .Option1 = 0, .Option2 = 0, .Option3 = 0, .IsValid = True, .CreatedBy = UserInfo.UserId, .UpdatedBy = UserInfo.UserId, .ChartFormatTypeId = 1}, _
                                       New Dashboard() With {.Order = dashboardOrderIndex(orderIndex), .DashDescription = account.Description, .ChartFormatId = 6, .ChartTypeId = 4, .ShowAsPercent = 0, .ShowTrendline = 0, .ShowBudget = 1, .Period1Goal = 0, .GoalGrowthRate = 0, .AccountId = account.AccountId, .UserId = "-999", .AnalysisId = intAnalysisID, .Option1 = 0, .Option2 = 0, .Option3 = 0, .IsValid = True, .CreatedBy = UserInfo.UserId, .UpdatedBy = UserInfo.UserId, .ChartFormatTypeId = 1}, _
                                       New Dashboard() With {.Order = dashboardOrderIndex(orderIndex), .DashDescription = account.Description, .ChartFormatId = 7, .ChartTypeId = 4, .ShowAsPercent = 0, .ShowTrendline = 0, .ShowBudget = 1, .Period1Goal = 0, .GoalGrowthRate = 0, .AccountId = account.AccountId, .UserId = "-999", .AnalysisId = intAnalysisID, .Option1 = 0, .Option2 = 0, .Option3 = 0, .IsValid = True, .CreatedBy = UserInfo.UserId, .UpdatedBy = UserInfo.UserId, .ChartFormatTypeId = 1} _
                       }
                            dashboardItemList.ForEach(Function(u) context.Dashboards.Add(u))
                            context.SaveChanges()

                            Dim dashboardDetailItemList As New List(Of DashboardDetail)() From {
                                  New DashboardDetail() With {.DashboardId = dashboardItemList(0).DashboardId, .DashboardDescription = account.Description, .AccountId = account.AccountId}, _
                                  New DashboardDetail() With {.DashboardId = dashboardItemList(1).DashboardId, .DashboardDescription = account.Description, .AccountId = account.AccountId}, _
                                  New DashboardDetail() With {.DashboardId = dashboardItemList(2).DashboardId, .DashboardDescription = account.Description, .AccountId = account.AccountId} _
                  }
                            dashboardDetailItemList.ForEach(Function(u) context.DashboardDetails.Add(u))
                            context.SaveChanges()
                        Next
                    End If

                End If
            End If

            Dim Periods As List(Of SelectListItem) = New List(Of SelectListItem)
            For intCtr = 0 To 11
                Periods.Add(New SelectListItem With {.Text = CommonProcedures.GetMonth(intCtr, intFirstFiscal), .Value = intCtr + 1, .Selected = False})
            Next
            ViewData("Periods") = New SelectList(Periods, "value", "text", "May")
            ViewData("intPeriod") = intSelPeriod + 1
            ViewData("intDept") = 1
            Session("sPeriod") = intSelPeriod
            Logger.Log.Info(String.Format("Dashboard IndexViewer Execution Ended"))
        End Sub

        <AcceptVerbs(HttpVerbs.Get)> _
        <CustomActionFilter()>
        Public Function GetCompanies(Optional SelectedCompany As Integer = Nothing) As JsonResult

            If (Not Session("UserType") Is Nothing) And (Not Session("UserInfo") Is Nothing) Then
                UserType = DirectCast(Session("UserType"), UserRole)
                UserInfo = DirectCast(Session("UserInfo"), User)
                CustomerId = If(UserType.UserRoleId = UserRoles.SAU Or UserType.UserRoleId = UserRoles.SSU, UserInfo.CustomerId, Nothing)

                'When user logged in, it will show the company list based on analysisId
                If (SelectedCompany = 0) Then
                    SelectedCompany = Convert.ToInt64(utility.AnalysisRepository().GetAnalyses().Where(Function(b) b.AnalysisId = UserInfo.AnalysisId).Select(Function(a) a.CompanyId).FirstOrDefault())
                End If

                If (Not Session("SelectedCompanyFromDropdown") Is Nothing) Then
                    SelectedCompany = Session("SelectedCompanyFromDropdown").ToString()
                End If

            End If

            Dim data As SelectList = utility.PouplateCompanyList(UserType, UserInfo, CustomerId, SelectedCompany)

            Return Json(data, JsonRequestBehavior.AllowGet)

        End Function

        <CustomActionFilter()>
        <AcceptVerbs(HttpVerbs.Get)>
        Public Function GetAnalyses(Optional selectedCompany As Integer = Nothing, Optional source As String = Nothing) As JsonResult
            Dim msg = Nothing
            Dim selectedAnalysis As Integer
            Try
                If (Not Session("UserType") Is Nothing) And (Not Session("UserInfo") Is Nothing) Then
                    UserType = DirectCast(Session("UserType"), UserRole)
                    UserInfo = DirectCast(Session("UserInfo"), User)
                    CustomerId = If(UserType.UserRoleId = UserRoles.SAU Or UserType.UserRoleId = UserRoles.SSU, UserInfo.CustomerId, Nothing)

                    UserInfo.selectedCompany = selectedCompany
                    UserInfo.fiscalMonthOfSelectedCompany = utility.CompaniesRepository.GetCompanyByID(selectedCompany).FiscalMonthStart
                    Session("UserInfo") = UserInfo

                    'When user logged in, it will show the analysis list based on analysisId
                    'If (selectedAnalysis = 0) Then
                    selectedAnalysis = UserInfo.AnalysisId
                    'End If

                    'check for option1 for given AnalysisID, if 1 then set isSharedAnalysis to true
                    Dim analysisDetail As Analysis = utility.AnalysisRepository.GetAnalysisById(selectedAnalysis)
                    If analysisDetail Is Nothing Then
                        UserInfo.isSharedAnalysis = False
                    Else
                        UserInfo.isSharedAnalysis = Convert.ToBoolean(analysisDetail.Option1)
                        UserInfo.FirstYear = Convert.ToInt32(analysisDetail.FirstYear)
                    End If


                    If (selectedCompany > 0) Then
                        Session("SelectedCompanyFromDropdown") = selectedCompany
                    End If

                End If

                Dim data As SelectList = utility.PouplateAnalysisList(UserType, UserInfo, CustomerId, selectedCompany, selectedAnalysis)

                If (data.Count = 0) Then
                    Session("SelectedAnalysisFromDropdown") = -1
                Else

                    Dim isExists As Boolean = data.Where(Function(a) a.Value = UserInfo.AnalysisId.ToString()).Any()

                    Session("SelectedAnalysisFromDropdown") = IIf(isExists = True, UserInfo.AnalysisId, 0)

                End If

                If (Request.UrlReferrer.ToString().ToLower().Contains("analysis") And source = "user") Then
                    Session("action") = "GetAnalyses"
                    msg = New With {.result = "success", .data = data, .urlReferrer = Request.UrlReferrer.ToString()}
                Else
                    msg = New With {.result = "success", .data = data}
                End If

            Catch ex As Exception
                msg = New With {.result = "error", .errorMsg = ex.Message.ToString()}
            Finally

            End Try
            Return Json(msg, JsonRequestBehavior.AllowGet)

        End Function

        <AcceptVerbs(HttpVerbs.Get)> _
        <CustomActionFilter()>
        Public Function SetDefaultAnalysis(Optional SelectedAnalysis As Integer = Nothing) As JsonResult
            Dim msg As New Hashtable()
            Try
                If (Not Session("UserType") Is Nothing) And (Not Session("UserInfo") Is Nothing) Then
                    UserType = DirectCast(Session("UserType"), UserRole)
                    UserInfo = DirectCast(Session("UserInfo"), User)

                    If (SelectedAnalysis > 0) And (SelectedAnalysis <> UserInfo.AnalysisId) Then

                        Dim user = utility.UserRepository.GetUserByID(UserInfo.UserId)
                        user.AnalysisId = SelectedAnalysis
                        utility.UserRepository.Update(user)
                        utility.UserRepository.Save()

                        UserInfo.AnalysisId = SelectedAnalysis
                        UserInfo.currentTabIndex = 0

                        Dim analysisDetail As Analysis = utility.AnalysisRepository.GetAnalysisById(SelectedAnalysis)
                        If analysisDetail Is Nothing Then
                            UserInfo.isSharedAnalysis = False
                        Else
                            UserInfo.isSharedAnalysis = Convert.ToBoolean(analysisDetail.Option1)
                        End If
                        Session("UserInfo") = UserInfo
                        Session("action") = "SetDefaultAnalysis"

                        msg.Add("Result", "success")
                        msg.Add("UrlReferrer", Request.UrlReferrer)
                    End If
                End If
            Catch ex As Exception
                msg.Add("Result", "error")
                msg.Add("error", ex.Message.ToString())
            End Try

            Return Json(msg, JsonRequestBehavior.AllowGet)
        End Function

        <MvcSpreadEvent("Load", "spdAnalysisDashboard", DirectCast(Nothing, String()))> _
        Private Sub FpSpreadShared_Load(sender As Object, e As EventArgs)
            Dim spread As FpSpread = DirectCast(sender, FpSpread)
            Dim decChartAmts(0, 12) As Decimal
            Dim decSaveAmts(12) As Decimal
            Dim strXLabels(12) As String
            Dim intCurrentMonth As Integer
            Dim intPeriods As Integer
            Dim strChartDesc As String

            'It will get the records which match with selected analysis & dashboard items selected by Logged-in user.
            'Dim dashboardItems = utility.DashboardRepository.GetDashboardDetail.Where(Function(d) d.UserId = UserInfo.UserId And d.AnalysisId = UserInfo.AnalysisId And d.IsValid = True)

            Logger.Log.Info(String.Format("Dashboard FpSpreadShared_Load- dashboardItems Query Execution Begin"))
            If UserInfo.isSharedAnalysis Then


                Dim dashboardItems = (From d In utility.DashboardRepository.GetDashboardDetail(-999, UserInfo.AnalysisId, True)
                                       Join a In utility.AccountRepository.GetAccountRecordsByAnalysisId(UserInfo.AnalysisId) On a.AccountId Equals d.AccountId And a.AnalysisId Equals d.AnalysisId
                                       Join at In utility.AccountTypeRepository.GetAccountTypeRecordsByAnalysisId(UserInfo.AnalysisId) On at.AcctTypeId Equals a.AcctTypeId And at.AnalysisId Equals a.AnalysisId
                                       Order By d.Order Ascending
                                       Select New With {d.DashboardId, d.Order, d.DashDescription, d.ChartFormatId, d.ChartTypeId, d.ShowAsPercent, _
                                                        d.ShowTrendline, d.ShowGoal, d.ShowBudget, d.Period1Goal, d.GoalGrowthRate, d.AccountId, d.AnalysisId, at.TypeDesc, a.NumberFormat}
                                       ).ToList()

                Logger.Log.Info(String.Format("Dashboard FpSpreadShared_Load - dashboardItems Query Execution End"))

                Logger.Log.Info(String.Format("Dashboard:FpSpreadShared_Load --> Balance Query execution Starts"))
                Dim balanceDash = (From b In utility.BalanceRepository.GetBalanceRecordsByAnalysisId(intAnalysisID)
                                Join di In dashboardItems On b.AccountId Equals di.AccountId And di.AnalysisId Equals b.AnalysisId
                                Select b).ToList()

                Logger.Log.Info(String.Format("Dashboard:FpSpreadShared_Load --> Balance Query execution Starts"))

                If (Not dashboardItems Is Nothing) Then
                    SetDashboardProperties(spread, dashboardItems.Count)
                    For Each account In dashboardItems
                        Dim accountId As Integer = account.AccountId
                        Dim analysisId As Integer = account.AnalysisId
                        Dim intFormat As Integer = account.ChartFormatId
                        Dim blnShowBudget As Boolean = account.ShowBudget
                        Dim blnShowasPercent As Boolean = account.ShowAsPercent
                        Dim blnPercentLabel As Boolean = account.ShowAsPercent
                        Dim decTotalBalances(11) As Decimal
                        Dim strYearType(5) As String
                        Dim intCtr As Integer
                        Dim intOrder As Integer = account.Order
                        Dim intBalCtr As Integer = -1
                        Dim intLastActual As Integer = -1
                        Dim strAppendLabel As String = ""
                        Dim intDecPlaces As Integer = 0
                        Array.Clear(decChartAmts, 0, 12)
                        intCurrentMonth = intSelPeriod
                        If blnShowasPercent = True Then
                            'get TypeDesc for the account used for this dashboard item
                            Select Case account.TypeDesc.ToLower()
                                Case "assets", "liabilities and equity"                        '      
                                    'get balances for account with AcctDescriptor of "ATotal"

                                    Logger.Log.Info(String.Format("Dashboard FpSpreadShared_Load - Case : assets, liabilities and equity - balanceAccum Query Execution Begin"))
                                    Dim balanceAccum = (From b In utility.BalanceRepository.GetBalanceRecordsByAnalysisId(analysisId)
                                                          Join a In utility.AccountRepository.GetAccountRecordsByAnalysisId(analysisId) On a.AccountId Equals b.AccountId And a.AnalysisId Equals b.AnalysisId
                                                          Select b).ToList()
                                    Logger.Log.Info(String.Format("Dashboard FpSpreadShared_Load - Case : assets, liabilities and equity - balanceAccum Query Execution Begin"))

                                    Logger.Log.Info(String.Format("Dashboard FpSpreadShared_Load - Case : assets, liabilities and equity - balanceAccum For Each Execution Begin"))
                                    For Each balance As Balance In (From b In balanceAccum)

                                        StoreTotalBal(balance, intFormat, decTotalBalances, intSelPeriod, blnShowBudget)
                                        Exit For
                                    Next
                                    Logger.Log.Info(String.Format("Dashboard FpSpreadShared_Load - Case : assets, liabilities and equity - balanceAccum For Each Execution End"))
                                Case "revenue", "expense"

                                    Logger.Log.Info(String.Format("Dashboard FpSpreadShared_Load - Case : revenue, expense - balanceAccum Query Execution Begin"))
                                    Dim balanceAccum = (From b In utility.BalanceRepository.GetBalanceRecordsByAnalysisId(analysisId)
                                                         Join a In utility.AccountRepository.GetAccountRecordsByAnalysisId(analysisId) On a.AccountId Equals b.AccountId And a.AnalysisId Equals b.AnalysisId
                                                         Join at In utility.AccountTypeRepository.GetAccountTypeRecordsByAnalysisId(analysisId, New String() {"revenue"}) On at.AcctTypeId Equals a.AcctTypeId And at.AnalysisId Equals a.AnalysisId
                                                         Select b).ToList()
                                    Logger.Log.Info(String.Format("Dashboard FpSpreadShared_Load - Case : revenue, expense - balanceAccum Query Execution Begin"))

                                    Dim count As Integer = balanceAccum.Count
                                    Logger.Log.Info(String.Format("Dashboard FpSpreadShared_Load - Case : revenue, expense - balanceAccum For Each Execution Begin"))
                                    For Each balance As Balance In (From b In balanceAccum)

                                        StoreTotalBal(balance, intFormat, decTotalBalances, intSelPeriod, blnShowBudget)
                                    Next
                                    Logger.Log.Info(String.Format("Dashboard FpSpreadShared_Load - Case : revenue, expense - balanceAccum For Each Execution End"))
                                Case Else
                                    blnShowasPercent = False
                            End Select
                        End If
                        Logger.Log.Info(String.Format("Dashboard FpSpreadShared_Load - Balance For Each Execution Begin"))
                        'It will get the record from balance of selected analysis and account item selected by user.
                        For Each balance As Balance In (From b In balanceDash.Where(Function(a) a.AccountId = accountId And a.AnalysisId = analysisId) Select b)
                            If account.NumberFormat = 99 Then
                                blnPercentLabel = True
                            Else
                                If account.TypeDesc.ToLower() = "cashflow" Then
                                    intDecPlaces = 0
                                Else
                                    intDecPlaces = account.NumberFormat
                                End If
                            End If
                            Select Case account.ChartFormatId
                                Case 1, 2
                                    If blnArchBudget = False Then
                                        CommonProcedures.GetBudgetAmts(balance, decSaveAmts)
                                    Else
                                        CommonProcedures.GetArchivedBudgetAmts(balance, decSaveAmts)
                                    End If

                                    If account.ChartFormatId = 1 Then
                                        If blnShowasPercent = False Then
                                            decChartAmts(0, 0) = decSaveAmts(intSelPeriod)
                                        Else
                                            decChartAmts(0, 0) = ComputePercent(decSaveAmts(intSelPeriod), decTotalBalances(0))
                                        End If
                                    Else
                                        For intCtr = 0 To intSelPeriod
                                            decChartAmts(0, 0) += decSaveAmts(intCtr)
                                        Next
                                        If blnShowasPercent = True Then
                                            '    decTotalAccum = ComputeTotal(intSelPeriod, decTotalBalances)
                                            decChartAmts(0, 0) = ComputePercent(decChartAmts(0, 0), decTotalBalances(0))
                                        End If
                                    End If
                                    CommonProcedures.GetActualAmts(balance, decSaveAmts)
                                    If account.ChartFormatId = 1 Then
                                        If blnShowasPercent = False Then
                                            decChartAmts(0, 1) = decSaveAmts(intSelPeriod)
                                        Else
                                            decChartAmts(0, 1) = ComputePercent(decSaveAmts(intSelPeriod), decTotalBalances(1))
                                        End If
                                    Else
                                        For intCtr = 0 To intSelPeriod
                                            decChartAmts(0, 1) += decSaveAmts(intCtr)
                                        Next
                                        If blnShowasPercent = True Then
                                            '   decTotalAccum = ComputeTotal(intSelPeriod, decTotalBalances)
                                            decChartAmts(0, 1) = ComputePercent(decChartAmts(0, 1), decTotalBalances(1))
                                        End If
                                    End If
                                    strXLabels(0) = "Budget"
                                    strXLabels(1) = "Actual"
                                    intPeriods = 1
                                    intLastActual = 0
                                Case 3
                                    If intSelPeriod = 0 Then
                                        CommonProcedures.GetH5Amts(balance, decSaveAmts)
                                        If blnShowasPercent = False Then
                                            decChartAmts(0, 0) = decSaveAmts(11)
                                        Else
                                            decChartAmts(0, 0) = ComputePercent(decSaveAmts(11), decTotalBalances(0))
                                        End If
                                        CommonProcedures.GetActualAmts(balance, decSaveAmts)
                                        If blnShowasPercent = False Then
                                            decChartAmts(0, 1) = decSaveAmts(intSelPeriod)
                                        Else
                                            decChartAmts(0, 1) = ComputePercent(decSaveAmts(intSelPeriod), decTotalBalances(1))
                                        End If
                                    Else
                                        CommonProcedures.GetActualAmts(balance, decSaveAmts)
                                        If blnShowasPercent = False Then
                                            decChartAmts(0, 0) = decSaveAmts(intSelPeriod - 1)
                                            decChartAmts(0, 1) = decSaveAmts(intSelPeriod)
                                        Else
                                            decChartAmts(0, 0) = ComputePercent(decSaveAmts(intSelPeriod - 1), decTotalBalances(0))
                                            decChartAmts(0, 1) = ComputePercent(decSaveAmts(intSelPeriod), decTotalBalances(1))
                                        End If
                                    End If
                                    strXLabels(0) = "Prior"
                                    strXLabels(1) = "Current"
                                    intPeriods = 1
                                    intLastActual = 0
                                Case 4, 5
                                    CommonProcedures.GetH5Amts(balance, decSaveAmts)
                                    If account.ChartFormatId = 4 Then
                                        decChartAmts(0, 0) = decSaveAmts(intSelPeriod)
                                    Else
                                        For intCtr = 0 To intSelPeriod
                                            decChartAmts(0, 0) += decSaveAmts(intCtr)
                                        Next
                                    End If
                                    If blnShowasPercent = True Then
                                        decChartAmts(0, 0) = ComputePercent(decChartAmts(0, 0), decTotalBalances(0))
                                    End If
                                    CommonProcedures.GetActualAmts(balance, decSaveAmts)
                                    If account.ChartFormatId = 4 Then
                                        decChartAmts(0, 1) = decSaveAmts(intSelPeriod)
                                    Else
                                        For intCtr = 0 To intSelPeriod
                                            decChartAmts(0, 1) += decSaveAmts(intCtr)
                                        Next
                                    End If
                                    If blnShowasPercent = True Then
                                        decChartAmts(0, 1) = ComputePercent(decChartAmts(0, 1), decTotalBalances(1))
                                    End If
                                    strXLabels(0) = "Prior"
                                    strXLabels(1) = "Current"
                                    intPeriods = 1
                                    intLastActual = 0
                                Case 6 'current year trend                                
                                    If blnShowBudget = False Then
                                        intPeriods = intSelPeriod
                                        GetActualAmts(balance, decSaveAmts)
                                        For intCtr = 0 To intSelPeriod
                                            If blnShowasPercent = False Then
                                                decChartAmts(0, intCtr) = decSaveAmts(intCtr)
                                            Else
                                                If decTotalBalances(intCtr) <> 0 Then
                                                    decChartAmts(0, intCtr) = ComputePercent(decSaveAmts(intCtr), decTotalBalances(intCtr))
                                                End If
                                            End If
                                        Next
                                    Else
                                        intPeriods = 11
                                        GetBudgetAmts(balance, decSaveAmts)
                                        For intCtr = 0 To 11
                                            If blnShowasPercent = False Then
                                                decChartAmts(0, intCtr) = decSaveAmts(intCtr)
                                            Else
                                                If decTotalBalances(intCtr) <> 0 Then
                                                    decChartAmts(0, intCtr) = ComputePercent(decSaveAmts(intCtr), decTotalBalances(intCtr))
                                                End If
                                            End If
                                        Next
                                        GetActualAmts(balance, decSaveAmts)

                                        For intCtr = intSelPeriod To 0 Step -1
                                            If intLastActual = -1 Then
                                                If decSaveAmts(intCtr) <> 0 Then
                                                    intLastActual = intCtr
                                                    If blnShowasPercent = False Then
                                                        decChartAmts(0, intCtr) = decSaveAmts(intCtr)
                                                    Else
                                                        If decTotalBalances(intCtr) <> 0 Then
                                                            decChartAmts(0, intCtr) = ComputePercent(decSaveAmts(intCtr), decTotalBalances(intCtr))
                                                        End If
                                                    End If
                                                End If
                                            Else
                                                If blnShowasPercent = False Then
                                                    decChartAmts(0, intCtr) = decSaveAmts(intCtr)
                                                Else
                                                    If decTotalBalances(intCtr) <> 0 Then
                                                        decChartAmts(0, intCtr) = ComputePercent(decSaveAmts(intCtr), decTotalBalances(intCtr))
                                                    End If
                                                End If
                                            End If
                                        Next
                                        If intLastActual < intSelPeriod Then
                                            intCurrentMonth = intLastActual
                                        End If
                                    End If
                                Case 7 'last 12 months
                                    GetH5Amts(balance, decSaveAmts)
                                    If intSelPeriod < 11 Then
                                        For intCtr = 0 To (11 - (intSelPeriod + 1))
                                            decChartAmts(0, intCtr) = decSaveAmts((intSelPeriod + 1) + intCtr)
                                            If blnShowasPercent = True Then
                                                decChartAmts(0, intCtr) = ComputePercent(decChartAmts(0, intCtr), decTotalBalances(intCtr))
                                            End If
                                        Next
                                    End If
                                    GetActualAmts(balance, decSaveAmts)
                                    For intCtr = 0 To intSelPeriod
                                        decChartAmts(0, (11 - intSelPeriod) + intCtr) = decSaveAmts(intCtr)
                                        If blnShowasPercent = True Then
                                            decChartAmts(0, intCtr + (11 - intSelPeriod)) = ComputePercent(decChartAmts(0, intCtr + (11 - intSelPeriod)), decTotalBalances(intCtr + (11 - intSelPeriod)))
                                        End If
                                    Next
                                    intPeriods = 11
                                Case 8, 9  'multiyear trend                               
                                    GetH1Amts(balance, decSaveAmts)
                                    StoreMultiYear(intFormat, decSaveAmts, decChartAmts, intBalCtr, intSelPeriod, blnShowasPercent, decTotalBalances, 0)
                                    GetH2Amts(balance, decSaveAmts)
                                    StoreMultiYear(intFormat, decSaveAmts, decChartAmts, intBalCtr, intSelPeriod, blnShowasPercent, decTotalBalances, 1)
                                    GetH3Amts(balance, decSaveAmts)
                                    StoreMultiYear(intFormat, decSaveAmts, decChartAmts, intBalCtr, intSelPeriod, blnShowasPercent, decTotalBalances, 2)
                                    GetH4Amts(balance, decSaveAmts)
                                    StoreMultiYear(intFormat, decSaveAmts, decChartAmts, intBalCtr, intSelPeriod, blnShowasPercent, decTotalBalances, 3)
                                    GetH5Amts(balance, decSaveAmts)
                                    StoreMultiYear(intFormat, decSaveAmts, decChartAmts, intBalCtr, intSelPeriod, blnShowasPercent, decTotalBalances, 4)
                                    GetActualAmts(balance, decSaveAmts)
                                    StoreMultiYear(intFormat, decSaveAmts, decChartAmts, intBalCtr, intSelPeriod, blnShowasPercent, decTotalBalances, 5)
                                    intPeriods = intBalCtr
                                Case 10 'rolling forcast
                                    GetBudgetAmts(balance, decSaveAmts)
                                    If intSelPeriod < 11 Then
                                        For intCtr = 0 To (11 - (intSelPeriod + 1))
                                            decChartAmts(0, intCtr) = decSaveAmts((intSelPeriod + 1) + intCtr)
                                            If blnShowasPercent = True Then
                                                decChartAmts(0, intCtr) = ComputePercent(decChartAmts(0, intCtr), decTotalBalances(intCtr))
                                            End If
                                        Next
                                    End If
                                    GetBudget2Amts(balance, decSaveAmts)
                                    For intCtr = 0 To intSelPeriod
                                        decChartAmts(0, (11 - intSelPeriod) + intCtr) = decSaveAmts(intCtr)
                                        If blnShowasPercent = True Then
                                            decChartAmts(0, intCtr + (11 - intSelPeriod)) = ComputePercent(decChartAmts(0, intCtr + (11 - intSelPeriod)), decTotalBalances(intCtr + (11 - intSelPeriod)))
                                        End If
                                    Next
                                    intPeriods = 11
                                Case 13
                                    intPeriods = 11
                                    If blnArchBudget = False Then
                                        CommonProcedures.GetBudgetAmts(balance, decSaveAmts)
                                    Else
                                        CommonProcedures.GetArchivedBudgetAmts(balance, decSaveAmts)
                                    End If
                                    For intCtr = 0 To intPeriods
                                        decChartAmts(0, intCtr) = decSaveAmts(intCtr)
                                        If blnShowasPercent = True Then
                                            decChartAmts(0, intCtr) = ComputePercent(decChartAmts(0, intCtr), decTotalBalances(intCtr))
                                        End If
                                    Next
                            End Select
                            Exit For
                        Next
                        Logger.Log.Info(String.Format("Dashboard FpSpreadShared_Load - Balance For Each Execution End"))

                        'load XLables
                        Select Case account.ChartFormatId
                            Case 1, 2 'budget vs actual
                                strXLabels(0) = "Budget"
                                strXLabels(1) = "Actual"
                            Case 3
                                If intCurrentMonth = 0 Then
                                    strXLabels(0) = CommonProcedures.GetMonth(11, intFirstFiscal)
                                Else
                                    strXLabels(0) = CommonProcedures.GetMonth(intCurrentMonth - 1, intFirstFiscal)
                                End If

                                strXLabels(1) = CommonProcedures.GetMonth(intCurrentMonth, intFirstFiscal)
                            Case 4, 5
                                Dim intYear As Integer = Year(Now)
                                strXLabels(0) = CommonProcedures.GetAbbrMonth(intCurrentMonth, intFirstFiscal) & " " & Year(Now) - 1
                                strXLabels(1) = CommonProcedures.GetAbbrMonth(intCurrentMonth, intFirstFiscal) & " " & Year(Now)
                            Case 6, 13
                                If intPeriods > 6 Then
                                    For intCtr = 0 To intPeriods
                                        strXLabels(intCtr) = CommonProcedures.GetMonthFirstLetter(intCtr, intFirstFiscal)
                                    Next
                                Else
                                    For intCtr = 0 To intPeriods
                                        strXLabels(intCtr) = CommonProcedures.GetAbbrMonth(intCtr, intFirstFiscal)
                                    Next
                                End If
                            Case 7, 10
                                If intCurrentMonth < 11 Then
                                    For intCtr = 0 To (11 - (intCurrentMonth + 1))
                                        strXLabels(intCtr) = CommonProcedures.GetMonthFirstLetter(intCtr + (11 - (11 - intCurrentMonth)) + 1, intFirstFiscal)
                                    Next
                                    For intCtr = 0 To intCurrentMonth
                                        strXLabels(intCtr + (11 - intCurrentMonth)) = CommonProcedures.GetMonthFirstLetter(intCtr, intFirstFiscal)
                                    Next
                                Else
                                    For intCtr = 0 To intCurrentMonth
                                        strXLabels(intCtr) = CommonProcedures.GetMonthFirstLetter(intCtr, intFirstFiscal)
                                    Next
                                End If
                            Case 8, 9
                                For intCtr = 0 To intPeriods
                                    strXLabels(intCtr) = Year(Now) - intPeriods + intCtr
                                Next
                        End Select
                        'check amount sizes
                        If CommonProcedures.CheckBalanceSize(decChartAmts, 0, 11) = True Then
                            strChartDesc = account.DashDescription & " (in thousands)"
                        Else
                            strChartDesc = account.DashDescription

                        End If
                        If blnShowasPercent = True Then
                            Select Case account.TypeDesc.ToLower()
                                Case "assets", "liabilities and equity"                        '      
                                    strAppendLabel = " (as % of assets)"
                                Case "revenue", "expense"
                                    strAppendLabel = " (as % of revenue)"
                            End Select
                        Else
                            strAppendLabel = ""
                        End If
                        'set chart title
                        Logger.Log.Info(String.Format("Dashboard FpSpread_Load- SetChartTitle Execution Begin"))
                        SetChartTitle(spread, account.Order, strChartDesc, account.ChartFormatId, account.ShowAsPercent)
                        Logger.Log.Info(String.Format("Dashboard FpSpread_Load- SetChartTitle Execution End"))

                        Select Case account.ChartTypeId
                            Case 2 'bar
                                BuildBarChart(spread, intPeriods, decChartAmts, strXLabels, account.Order - 1, account.ChartFormatId, strAppendLabel, blnPercentLabel, intLastActual, intDecPlaces, blnShowBudget)
                            Case 4 'line 
                                BuildLineChart(spread, intPeriods, decChartAmts, strXLabels, account.Order - 1, account.ChartFormatId, False, True, True, account.ShowTrendline, strAppendLabel, blnPercentLabel, intCurrentMonth, intDecPlaces, blnShowBudget)
                            Case 5 'area
                                BuildAreaChart(spread, intPeriods, decChartAmts, strXLabels, account.Order - 1, account.ChartFormatId, strAppendLabel, blnPercentLabel, intDecPlaces, blnShowBudget)
                        End Select
                    Next
                End If
            End If
        End Sub

        <MvcSpreadEvent("Load", "spdDashboard", DirectCast(Nothing, String()))> _
        Private Sub FpSpread_Load(sender As Object, e As EventArgs)
            Dim spread As FpSpread = DirectCast(sender, FpSpread)
            Dim decChartAmts(0, 12) As Decimal
            Dim decSaveAmts(12) As Decimal
            Dim strXLabels(12) As String
            Dim intCurrentMonth As Integer
            Dim intPeriods As Integer
            Dim strChartDesc As String

            'It will get the records which match with selected analysis & dashboard items selected by Logged-in user.
            'Dim dashboardItems = utility.DashboardRepository.GetDashboardDetail.Where(Function(d) d.UserId = UserInfo.UserId And d.AnalysisId = UserInfo.AnalysisId And d.IsValid = True)


            Logger.Log.Info(String.Format("Dashboard FpSpread_Load- dashboardItems Query Execution Begin"))

            Dim dashboardItems = (From d In utility.DashboardRepository.GetDashboardDetail(UserInfo.UserId, UserInfo.AnalysisId, True)
                                   Join a In utility.AccountRepository.GetAccountRecordsByAnalysisId(UserInfo.AnalysisId) On a.AccountId Equals d.AccountId And a.AnalysisId Equals d.AnalysisId
                                   Join at In utility.AccountTypeRepository.GetAccountTypeRecordsByAnalysisId(UserInfo.AnalysisId) On at.AcctTypeId Equals a.AcctTypeId And at.AnalysisId Equals a.AnalysisId
                                   Select New With {d.DashboardId, d.Order, d.DashDescription, d.ChartFormatId, d.ChartTypeId, d.ShowAsPercent, _
                                                    d.ShowTrendline, d.ShowGoal, d.ShowBudget, d.Period1Goal, d.GoalGrowthRate, d.AccountId, d.AnalysisId, at.TypeDesc, a.NumberFormat}
                                   ).ToList()

            Logger.Log.Info(String.Format("Dashboard FpSpread_Load - dashboardItems Query Execution End"))

            Logger.Log.Info(String.Format("Dashboard:FpSpread_Load --> Balance Query execution Starts"))
            Dim balanceDash = (From b In utility.BalanceRepository.GetBalanceRecordsByAnalysisId(intAnalysisID)
                            Join di In dashboardItems On b.AccountId Equals di.AccountId And di.AnalysisId Equals b.AnalysisId
                            Select b).ToList()

            Logger.Log.Info(String.Format("Dashboard:FpSpread_Load --> Balance Query execution Starts"))

            If (Not dashboardItems Is Nothing) Then
                SetDashboardProperties(spread, dashboardItems.Count)
                For Each account In dashboardItems
                    Dim accountId As Integer = account.AccountId
                    Dim analysisId As Integer = account.AnalysisId
                    Dim intFormat As Integer = account.ChartFormatId
                    Dim blnShowBudget As Boolean = account.ShowBudget
                    Dim blnShowasPercent As Boolean = account.ShowAsPercent
                    Dim blnPercentLabel As Boolean = account.ShowAsPercent
                    Dim decTotalBalances(11) As Decimal
                    Dim strYearType(5) As String
                    Dim intCtr As Integer
                    Dim intOrder As Integer = account.Order
                    Dim intBalCtr As Integer = -1
                    Dim intLastActual As Integer = -1
                    Dim strAppendLabel As String = ""
                    Dim intDecPlaces As Integer
                    Array.Clear(decChartAmts, 0, 12)
                    intCurrentMonth = intSelPeriod
                    '   If blnShowasPercent = True Then
                    'get TypeDesc for the account used for this dashboard item
                    Select Case account.TypeDesc.ToLower()
                        Case "assets", "liabilities and equity"                        '      
                            'get balances for account with AcctDescriptor of "ATotal"

                            Logger.Log.Info(String.Format("Dashboard FpSpread_Load - Case : assets, liabilities and equity - balanceAccum Query Execution Begin"))
                            Dim balanceAccum = (From b In utility.BalanceRepository.GetBalanceRecordsByAnalysisId(analysisId)
                                                  Join a In utility.AccountRepository.GetAccountRecordsByAnalysisId(analysisId) On a.AccountId Equals b.AccountId And a.AnalysisId Equals b.AnalysisId
                                                  Select b).ToList()
                            Logger.Log.Info(String.Format("Dashboard FpSpread_Load - Case : assets, liabilities and equity - balanceAccum Query Execution Begin"))

                            Logger.Log.Info(String.Format("Dashboard FpSpread_Load - Case : assets, liabilities and equity - balanceAccum For Each Execution Begin"))
                            For Each balance As Balance In (From b In balanceAccum)

                                StoreTotalBal(balance, intFormat, decTotalBalances, intSelPeriod, blnShowBudget)
                                Exit For
                            Next
                            Logger.Log.Info(String.Format("Dashboard FpSpread_Load - Case : assets, liabilities and equity - balanceAccum For Each Execution End"))
                        Case "revenue", "expense"

                            Logger.Log.Info(String.Format("Dashboard FpSpread_Load - Case : revenue, expense - balanceAccum Query Execution Begin"))
                            Dim balanceAccum = (From b In utility.BalanceRepository.GetBalanceRecordsByAnalysisId(analysisId)
                                                 Join a In utility.AccountRepository.GetAccountRecordsByAnalysisId(analysisId) On a.AccountId Equals b.AccountId And a.AnalysisId Equals b.AnalysisId
                                                 Join at In utility.AccountTypeRepository.GetAccountTypeRecordsByAnalysisId(analysisId, New String() {"revenue"}) On at.AcctTypeId Equals a.AcctTypeId And at.AnalysisId Equals a.AnalysisId
                                                 Select b).ToList()
                            Logger.Log.Info(String.Format("Dashboard FpSpread_Load - Case : revenue, expense - balanceAccum Query Execution Begin"))

                            Dim count As Integer = balanceAccum.Count
                            Logger.Log.Info(String.Format("Dashboard FpSpread_Load - Case : revenue, expense - balanceAccum For Each Execution Begin"))
                            For Each balance As Balance In (From b In balanceAccum)
                                StoreTotalBal(balance, intFormat, decTotalBalances, intSelPeriod, blnShowBudget)
                            Next
                            Logger.Log.Info(String.Format("Dashboard FpSpread_Load - Case : revenue, expense - balanceAccum For Each Execution End"))
                        Case Else
                            blnShowasPercent = False
                    End Select
                    '    End If
                    Logger.Log.Info(String.Format("Dashboard FpSpread_Load - Balance For Each Execution Begin"))
                    'It will get the record from balance of selected analysis and account item selected by user.
                    For Each balance As Balance In (From b In balanceDash.Where(Function(a) a.AccountId = accountId And a.AnalysisId = analysisId) Select b)
                        If account.NumberFormat = 99 Then
                            blnPercentLabel = True
                        Else
                            If account.TypeDesc.ToLower() = "cashflow" Then
                                intDecPlaces = 0
                            Else
                                intDecPlaces = account.NumberFormat
                            End If
                        End If
                        Select Case account.ChartFormatId
                            Case 1, 2
                                If blnArchBudget = False Then
                                    CommonProcedures.GetBudgetAmts(balance, decSaveAmts)
                                Else
                                    CommonProcedures.GetArchivedBudgetAmts(balance, decSaveAmts)
                                End If
                                If account.ChartFormatId = 1 Then
                                    If blnShowasPercent = False Then
                                        decChartAmts(0, 0) = decSaveAmts(intSelPeriod)
                                    Else
                                        decChartAmts(0, 0) = ComputePercent(decSaveAmts(intSelPeriod), decTotalBalances(0))
                                    End If
                                Else
                                    For intCtr = 0 To intSelPeriod
                                        decChartAmts(0, 0) += decSaveAmts(intCtr)
                                    Next
                                    If blnShowasPercent = True Then
                                        '    decTotalAccum = ComputeTotal(intSelPeriod, decTotalBalances)
                                        decChartAmts(0, 0) = ComputePercent(decChartAmts(0, 0), decTotalBalances(0))
                                    End If
                                End If
                                CommonProcedures.GetActualAmts(balance, decSaveAmts)
                                If account.ChartFormatId = 1 Then
                                    If blnShowasPercent = False Then
                                        decChartAmts(0, 1) = decSaveAmts(intSelPeriod)
                                    Else
                                        decChartAmts(0, 1) = ComputePercent(decSaveAmts(intSelPeriod), decTotalBalances(1))
                                    End If
                                Else
                                    For intCtr = 0 To intSelPeriod
                                        decChartAmts(0, 1) += decSaveAmts(intCtr)
                                    Next
                                    If blnShowasPercent = True Then
                                        '   decTotalAccum = ComputeTotal(intSelPeriod, decTotalBalances)
                                        decChartAmts(0, 1) = ComputePercent(decChartAmts(0, 1), decTotalBalances(1))
                                    End If
                                End If
                                strXLabels(0) = "Budget"
                                strXLabels(1) = "Actual"
                                intPeriods = 1
                                intLastActual = 0
                            Case 3
                                If intSelPeriod = 0 Then
                                    CommonProcedures.GetH5Amts(balance, decSaveAmts)
                                    If blnShowasPercent = False Then
                                        decChartAmts(0, 0) = decSaveAmts(11)
                                    Else
                                        decChartAmts(0, 0) = ComputePercent(decSaveAmts(11), decTotalBalances(0))
                                    End If
                                    CommonProcedures.GetActualAmts(balance, decSaveAmts)
                                    If blnShowasPercent = False Then
                                        decChartAmts(0, 1) = decSaveAmts(intSelPeriod)
                                    Else
                                        decChartAmts(0, 1) = ComputePercent(decSaveAmts(intSelPeriod), decTotalBalances(1))
                                    End If
                                Else
                                    CommonProcedures.GetActualAmts(balance, decSaveAmts)
                                    If blnShowasPercent = False Then
                                        decChartAmts(0, 0) = decSaveAmts(intSelPeriod - 1)
                                        decChartAmts(0, 1) = decSaveAmts(intSelPeriod)
                                    Else
                                        decChartAmts(0, 0) = ComputePercent(decSaveAmts(intSelPeriod - 1), decTotalBalances(0))
                                        decChartAmts(0, 1) = ComputePercent(decSaveAmts(intSelPeriod), decTotalBalances(1))
                                    End If
                                End If
                                strXLabels(0) = "Prior"
                                strXLabels(1) = "Current"
                                intPeriods = 1
                                intLastActual = 0
                            Case 4, 5
                                CommonProcedures.GetH5Amts(balance, decSaveAmts)
                                If account.ChartFormatId = 4 Then
                                    decChartAmts(0, 0) = decSaveAmts(intSelPeriod)
                                Else
                                    For intCtr = 0 To intSelPeriod
                                        decChartAmts(0, 0) += decSaveAmts(intCtr)
                                    Next
                                End If
                                If blnShowasPercent = True Then
                                    decChartAmts(0, 0) = ComputePercent(decChartAmts(0, 0), decTotalBalances(0))
                                End If
                                CommonProcedures.GetActualAmts(balance, decSaveAmts)
                                If account.ChartFormatId = 4 Then
                                    decChartAmts(0, 1) = decSaveAmts(intSelPeriod)
                                Else
                                    For intCtr = 0 To intSelPeriod
                                        decChartAmts(0, 1) += decSaveAmts(intCtr)
                                    Next
                                End If
                                If blnShowasPercent = True Then
                                    decChartAmts(0, 1) = ComputePercent(decChartAmts(0, 1), decTotalBalances(1))
                                End If
                                strXLabels(0) = "Prior"
                                strXLabels(1) = "Current"
                                intPeriods = 1
                                intLastActual = 0
                            Case 6 'current year trend                                
                                If blnShowBudget = False Then
                                    intPeriods = intSelPeriod
                                    GetActualAmts(balance, decSaveAmts)
                                    For intCtr = 0 To intSelPeriod
                                        If blnShowasPercent = False Then
                                            decChartAmts(0, intCtr) = decSaveAmts(intCtr)
                                        Else
                                            If decTotalBalances(intCtr) <> 0 Then
                                                decChartAmts(0, intCtr) = ComputePercent(decSaveAmts(intCtr), decTotalBalances(intCtr))
                                            End If
                                        End If
                                    Next
                                Else
                                    intPeriods = 11
                                    GetBudgetAmts(balance, decSaveAmts)
                                    For intCtr = 0 To 11
                                        If blnShowasPercent = False Then
                                            decChartAmts(0, intCtr) = decSaveAmts(intCtr)
                                        Else
                                            If decTotalBalances(intCtr) <> 0 Then
                                                decChartAmts(0, intCtr) = ComputePercent(decSaveAmts(intCtr), decTotalBalances(intCtr))
                                            End If
                                        End If
                                    Next
                                    GetActualAmts(balance, decSaveAmts)

                                    For intCtr = intSelPeriod To 0 Step -1
                                        If intLastActual = -1 Then
                                            If decSaveAmts(intCtr) <> 0 Then
                                                intLastActual = intCtr
                                                If blnShowasPercent = False Then
                                                    decChartAmts(0, intCtr) = decSaveAmts(intCtr)
                                                Else
                                                    If decTotalBalances(intCtr) <> 0 Then
                                                        decChartAmts(0, intCtr) = ComputePercent(decSaveAmts(intCtr), decTotalBalances(intCtr))
                                                    End If
                                                End If
                                            End If
                                        Else
                                            If blnShowasPercent = False Then
                                                decChartAmts(0, intCtr) = decSaveAmts(intCtr)
                                            Else
                                                If decTotalBalances(intCtr) <> 0 Then
                                                    decChartAmts(0, intCtr) = ComputePercent(decSaveAmts(intCtr), decTotalBalances(intCtr))
                                                End If
                                            End If
                                        End If
                                    Next
                                    If intLastActual < intSelPeriod Then
                                        intCurrentMonth = intLastActual
                                    End If
                                End If


                            Case 7 'last 12 months
                                GetH5Amts(balance, decSaveAmts)
                                If intSelPeriod < 11 Then
                                    For intCtr = 0 To (11 - (intSelPeriod + 1))
                                        decChartAmts(0, intCtr) = decSaveAmts((intSelPeriod + 1) + intCtr)
                                        If blnShowasPercent = True Then
                                            decChartAmts(0, intCtr) = ComputePercent(decChartAmts(0, intCtr), decTotalBalances(intCtr))
                                        End If
                                    Next
                                End If
                                GetActualAmts(balance, decSaveAmts)
                                For intCtr = 0 To intSelPeriod
                                    decChartAmts(0, (11 - intSelPeriod) + intCtr) = decSaveAmts(intCtr)
                                    If blnShowasPercent = True Then
                                        decChartAmts(0, intCtr + (11 - intSelPeriod)) = ComputePercent(decChartAmts(0, intCtr + (11 - intSelPeriod)), decTotalBalances(intCtr + (11 - intSelPeriod)))
                                    End If
                                Next
                                intPeriods = 11
                            Case 8, 9  'multiyear trend                               
                                GetH1Amts(balance, decSaveAmts)
                                StoreMultiYear(intFormat, decSaveAmts, decChartAmts, intBalCtr, intSelPeriod, blnShowasPercent, decTotalBalances, 0)
                                GetH2Amts(balance, decSaveAmts)
                                StoreMultiYear(intFormat, decSaveAmts, decChartAmts, intBalCtr, intSelPeriod, blnShowasPercent, decTotalBalances, 1)
                                GetH3Amts(balance, decSaveAmts)
                                StoreMultiYear(intFormat, decSaveAmts, decChartAmts, intBalCtr, intSelPeriod, blnShowasPercent, decTotalBalances, 2)
                                GetH4Amts(balance, decSaveAmts)
                                StoreMultiYear(intFormat, decSaveAmts, decChartAmts, intBalCtr, intSelPeriod, blnShowasPercent, decTotalBalances, 3)
                                GetH5Amts(balance, decSaveAmts)
                                StoreMultiYear(intFormat, decSaveAmts, decChartAmts, intBalCtr, intSelPeriod, blnShowasPercent, decTotalBalances, 4)
                                GetActualAmts(balance, decSaveAmts)
                                StoreMultiYear(intFormat, decSaveAmts, decChartAmts, intBalCtr, intSelPeriod, blnShowasPercent, decTotalBalances, 5)
                                intPeriods = intBalCtr
                            Case 10 'rolling forcast
                                GetBudgetAmts(balance, decSaveAmts)
                                If intSelPeriod < 11 Then
                                    For intCtr = 0 To (11 - (intSelPeriod + 1))
                                        decChartAmts(0, intCtr) = decSaveAmts((intSelPeriod + 1) + intCtr)
                                        If blnShowasPercent = True Then
                                            decChartAmts(0, intCtr) = ComputePercent(decChartAmts(0, intCtr), decTotalBalances(intCtr))
                                        End If
                                    Next
                                End If
                                GetBudget2Amts(balance, decSaveAmts)
                                For intCtr = 0 To intSelPeriod
                                    decChartAmts(0, (11 - intSelPeriod) + intCtr) = decSaveAmts(intCtr)
                                    If blnShowasPercent = True Then
                                        decChartAmts(0, intCtr + (11 - intSelPeriod)) = ComputePercent(decChartAmts(0, intCtr + (11 - intSelPeriod)), decTotalBalances(intCtr + (11 - intSelPeriod)))
                                    End If
                                Next
                                intPeriods = 11
                            Case 13
                                intPeriods = 11
                                If blnArchBudget = False Then
                                    CommonProcedures.GetBudgetAmts(balance, decSaveAmts)
                                Else
                                    CommonProcedures.GetArchivedBudgetAmts(balance, decSaveAmts)
                                End If
                                For intCtr = 0 To intPeriods
                                    decChartAmts(0, intCtr) = decSaveAmts(intCtr)
                                    If blnShowasPercent = True Then
                                        decChartAmts(0, intCtr) = ComputePercent(decChartAmts(0, intCtr), decTotalBalances(intCtr))
                                    End If
                                Next
                        End Select
                        Exit For
                    Next
                    Logger.Log.Info(String.Format("Dashboard FpSpread_Load - Balance For Each Execution End"))

                    'load XLables
                    Select Case account.ChartFormatId
                        Case 1, 2 'budget vs actual
                            strXLabels(0) = "Budget"
                            strXLabels(1) = "Actual"
                        Case 3
                            If intCurrentMonth = 0 Then
                                strXLabels(0) = CommonProcedures.GetMonth(11, intFirstFiscal)
                            Else
                                strXLabels(0) = CommonProcedures.GetMonth(intCurrentMonth - 1, intFirstFiscal)
                            End If

                            strXLabels(1) = CommonProcedures.GetMonth(intCurrentMonth, intFirstFiscal)
                        Case 4, 5
                            Dim intYear As Integer = Year(Now)
                            strXLabels(0) = CommonProcedures.GetAbbrMonth(intCurrentMonth, intFirstFiscal) & " " & Year(Now) - 1
                            strXLabels(1) = CommonProcedures.GetAbbrMonth(intCurrentMonth, intFirstFiscal) & " " & Year(Now)
                        Case 6, 13
                            If intPeriods > 6 Then
                                For intCtr = 0 To intPeriods
                                    strXLabels(intCtr) = CommonProcedures.GetMonthFirstLetter(intCtr, intFirstFiscal)
                                Next
                            Else
                                For intCtr = 0 To intPeriods
                                    strXLabels(intCtr) = CommonProcedures.GetAbbrMonth(intCtr, intFirstFiscal)
                                Next
                            End If
                        Case 7, 10
                            If intCurrentMonth < 11 Then
                                For intCtr = 0 To (11 - (intCurrentMonth + 1))
                                    strXLabels(intCtr) = CommonProcedures.GetMonthFirstLetter(intCtr + (11 - (11 - intCurrentMonth)) + 1, intFirstFiscal)
                                Next
                                For intCtr = 0 To intCurrentMonth
                                    strXLabels(intCtr + (11 - intCurrentMonth)) = CommonProcedures.GetMonthFirstLetter(intCtr, intFirstFiscal)
                                Next
                            Else
                                For intCtr = 0 To intCurrentMonth
                                    strXLabels(intCtr) = CommonProcedures.GetMonthFirstLetter(intCtr, intFirstFiscal)
                                Next
                            End If
                        Case 8, 9
                            For intCtr = 0 To intPeriods
                                strXLabels(intCtr) = Year(Now) - intPeriods + intCtr
                            Next


                    End Select
                    'check amount sizes
                    If CommonProcedures.CheckBalanceSize(decChartAmts, 0, 11) = True Then
                        strChartDesc = account.DashDescription & " (in thousands)"
                    Else
                        strChartDesc = account.DashDescription

                    End If
                    If blnShowasPercent = True Then
                        Select Case account.TypeDesc.ToLower()
                            Case "assets", "liabilities and equity"                        '      
                                strAppendLabel = " (as % of assets)"
                            Case "revenue", "expense"
                                strAppendLabel = " (as % of revenue)"
                        End Select
                    Else
                        strAppendLabel = ""
                    End If
                    'set chart title
                    SetChartTitle(spread, account.Order, strChartDesc, account.ChartFormatId, account.ShowAsPercent)
                    Select Case account.ChartTypeId
                        Case 2 'bar
                            BuildBarChart(spread, intPeriods, decChartAmts, strXLabels, account.Order - 1, account.ChartFormatId, strAppendLabel, blnPercentLabel, intLastActual, intDecPlaces, blnShowBudget)
                        Case 4 'line 
                            BuildLineChart(spread, intPeriods, decChartAmts, strXLabels, account.Order - 1, account.ChartFormatId, False, True, True, account.ShowTrendline, strAppendLabel, blnPercentLabel, intCurrentMonth, intDecPlaces, blnShowBudget)
                        Case 5 'area
                            BuildAreaChart(spread, intPeriods, decChartAmts, strXLabels, account.Order - 1, account.ChartFormatId, strAppendLabel, blnPercentLabel, intDecPlaces, blnShowBudget)
                    End Select
                Next
            End If

        End Sub

        Private Sub SetDashboardProperties(ByVal spdDashboard As FpSpread, ByVal intChartCount As Integer)
            Dim intCtr As Integer
            Dim intColCtr As Integer
            Dim intChartRowsCtr As Integer
            Dim intChartCtr As Integer
            Dim intRowCtr As Integer
            Dim strXLabels(11) As String
            Dim intHeight As Integer


            With spdDashboard
                .RowHeader.Visible = False
                .ColumnHeader.Visible = False
                .CommandBar.Visible = False
                .BorderColor = Drawing.Color.White
                .VerticalScrollBarPolicy = FarPoint.Web.Spread.ScrollBarPolicy.Never
                .Width = 930
                .HorizontalScrollBarPolicy = FarPoint.Web.Spread.ScrollBarPolicy.Never
                .Sheets(0).DefaultStyle.Font.Size = FontSize.XXLarge
                .Sheets(0).DefaultStyle.Font.Bold = True
                .Sheets(0).SelectionBackColorStyle = FarPoint.Web.Spread.SelectionBackColorStyles.None
                .Sheets(0).GridLineColor = Drawing.Color.White
                Select Case intChartCount
                    Case 1 To 3
                        intChartRowsCtr = 0
                    Case 4 To 6
                        intChartRowsCtr = 1
                    Case 7 To 9
                        intChartRowsCtr = 2
                    Case 10 To 12
                        intChartRowsCtr = 3
                    Case 13 To 15
                        intChartRowsCtr = 4
                End Select
                Dim intspdRows As Integer = (intChartRowsCtr * 3) - 1
                .Sheets(0).RowCount = 12 ' (intChartRowsCtr * 3) - 1

                .Sheets(0).ColumnCount = 5
                .Sheets(0).Columns(0).Width = 300
                .Sheets(0).Columns(1).Width = 15
                .Sheets(0).Columns(2).Width = 300
                .Sheets(0).Columns(3).Width = 15
                .Sheets(0).Columns(4).Width = 300

            End With

            'set row heights
            With spdDashboard.Sheets(0)
                For intCtr = 0 To intChartRowsCtr
                    intHeight = 255 + (intCtr * 255)
                    For intRowCtr = 0 To 2
                        Select Case intRowCtr
                            Case 0
                                .AddRows((intCtr * 3), 1)
                                .Rows(intCtr * 3).Height = 22
                                For intColCtr = 0 To 4
                                    Select Case intColCtr
                                        Case 0, 2, 4
                                            intChartCtr += 1
                                            If intChartCtr <= intChartCount Then
                                                .Cells(intCtr * 3, intColCtr).BackColor = Drawing.Color.FromArgb(237, 237, 237)
                                                .Cells(intCtr * 3, intColCtr).ForeColor = Drawing.Color.FromArgb(39, 111, 166)
                                                .Cells(intCtr * 3, intColCtr).HorizontalAlign = HorizontalAlign.Center
                                                .Cells(intCtr * 3, intColCtr).VerticalAlign = VerticalAlign.Middle
                                                '.Cells(intCtr * 3, intColCtr).Border.BorderColor = Drawing.Color.LightGray
                                                '.Cells(intCtr * 3, intColCtr).Border.BorderSize = 3
                                                '.Cells(intCtr * 3, intColCtr).Border.BorderStyleBottom = BorderStyle.None
                                            End If
                                    End Select
                                Next
                            Case 1
                                .AddRows((intCtr * 3) + 1, 1)
                                .Rows((intCtr * 3) + 1).Height = 220
                            Case 2
                                If intCtr <> intChartRowsCtr Then
                                    .AddRows((intCtr * 3) + 2, 1)
                                    .Rows((intCtr * 3) + 2).Height = 20
                                End If
                        End Select
                    Next
                Next
            End With

            If intHeight < 500 Then
                spdDashboard.Height = 500
            Else
                spdDashboard.Height = intHeight
            End If
        End Sub

        Private Sub BuildLineChart(ByRef spdDashboard As FpSpread, intColCount As Integer, decValues(,) As Decimal, strXLabels() As String, intChartIndex As Integer, intFormat As Integer, blnShowasPerc As Boolean, blnSmoothed As Boolean, blnMarkers As Boolean, ByVal blnShowTrend As Boolean, ByVal strAppendLabel As String, ByVal blnPercentLabel As Boolean, ByVal intCurrentPeriod As Integer, ByVal intDecPlaces As Integer, ByVal blnShowBudget As Boolean)
            Logger.Log.Info(String.Format("Dashboard FpSpread_Load - BuildLineChart Execution Begin"))
            Dim intColCtr As Integer
            Dim intRowCount As Integer = 1
            Dim decIntercept As Decimal = 0
            Dim decSlope As Decimal = 0
            Dim dblCoeff As Double = 0
            Dim sseries As New FarPoint.Web.Chart.LineSeries
            Dim tseries As New FarPoint.Web.Chart.LineSeries
            Dim plotArea As New FarPoint.Web.Chart.YPlotArea()
            Dim label As New FarPoint.Web.Chart.LabelArea
            Dim chart As New FarPoint.Web.Spread.Chart.SpreadChart()
            Dim model As New FarPoint.Web.Chart.ChartModel()
            Dim NoMarker As New FarPoint.Web.Chart.NoMarker()
            Dim LabFont As System.Drawing.Font
            Dim plotFont As System.Drawing.Font
            plotFont = New System.Drawing.Font("Arial", 8)

            '    sseries.LabelVisible = True
            LabFont = New System.Drawing.Font("Arial", 8)
            plotArea.XAxis.LabelTextFont = plotFont
            plotArea.YAxes(0).LabelTextFont = plotFont
            plotArea.Location = New System.Drawing.PointF(0.2F, 0.125F)
            plotArea.Size = New System.Drawing.SizeF(0.7F, 0.7F)
            With sseries
                .SeriesName = "Actual"
                For intColCtr = 0 To intColCount
                    .Values.Add(decValues(0, intColCtr))
                    .CategoryNames.Add(strXLabels(intColCtr))
                    If intColCtr > intCurrentPeriod And intFormat = 6 Then
                        .PointFills(intColCtr) = New FarPoint.Web.Chart.SolidFill(Drawing.Color.FromArgb(0, 112, 192))
                    Else
                        .PointFills(intColCtr) = New FarPoint.Web.Chart.SolidFill(Drawing.Color.FromArgb(112, 176, 240))
                    End If
                Next
            End With
            If blnMarkers = False Then
                sseries.PointMarker = NoMarker
            Else
                sseries.PointMarker = New FarPoint.Web.Chart.BuiltinMarker(FarPoint.Web.Chart.MarkerShape.Circle, 5.0F)
            End If
            If blnSmoothed = True Then
                sseries.SmoothedLine = True
            End If
            sseries.LineBorder = New FarPoint.Web.Chart.SolidLine(Drawing.Color.FromArgb(0, 126, 57))
            plotArea.Series.Add(sseries)
            If blnPercentLabel = True Then
                plotArea.YAxes(0).LabelNumberFormat = "#,##0%"
            Else
                Select Case intDecPlaces
                    Case 0
                        plotArea.YAxes(0).LabelNumberFormat = "#,##0"
                    Case 1
                        plotArea.YAxes(0).LabelNumberFormat = "#,##0.0"
                    Case 2
                        plotArea.YAxes(0).LabelNumberFormat = "#,##0.00"
                    Case 3
                        plotArea.YAxes(0).LabelNumberFormat = "#,##0.000"
                    Case 4
                        plotArea.YAxes(0).LabelNumberFormat = "#,##0.0000"
                    Case 5
                        plotArea.YAxes(0).LabelNumberFormat = "#,##0.00000"
                    Case Else
                        plotArea.YAxes(0).LabelNumberFormat = "#,##0"
                End Select
            End If
            If blnShowTrend = True Then
                'get trend amounts
                ComputeTrend(decIntercept, decSlope, dblCoeff, decValues, intColCount)
                With tseries
                    .SeriesName = "Trend"
                    For intColCtr = 0 To intColCount
                        .Values.Add(decIntercept + (intColCtr * decSlope))
                    Next
                End With
                tseries.PointMarker = NoMarker
                tseries.LineBorder = New FarPoint.Web.Chart.SolidLine(Drawing.Color.Red, 0.5F)
                Dim slope As New FarPoint.Web.Chart.LabelArea
                Dim strText As String
                If decSlope > 0 Then
                    strText = "Trend - Increasing at rate of "
                Else
                    strText = "Trend - Decreasing at rate of "
                End If
                If blnShowasPerc = False Then
                    strText = strText & Format(Double.Parse(Math.Round(decSlope, 0)), "###,###,###")
                    Select Case intFormat
                        Case 1, 3, 4, 6, 7
                            strText = strText & " per month"
                        Case Else
                            strText = strText & " per year"
                    End Select
                Else
                    strText = strText & Math.Round(decSlope, 2)
                    Select Case intFormat
                        Case 1, 3, 4, 6, 7
                            strText = strText & "% per month"
                        Case Else
                            strText = strText & "% per year"
                    End Select
                End If
                slope.Text = strText
                slope.Location = New System.Drawing.PointF(0.1F, 0.93F)
                slope.TextFont = LabFont
                model.LabelAreas.Add(slope)
                plotArea.Series.Add(tseries)
            End If
            SetLabelProperties(intFormat, label, strAppendLabel, blnShowBudget)
            model.LabelAreas.Add(label)
            model.PlotAreas.Add(plotArea)
            chart.Model = model
            SetChartProperties(chart, intChartIndex)
            spdDashboard.Sheets(0).Charts.Add(chart)
            Logger.Log.Info(String.Format("Dashboard FpSpread_Load - BuildLineChart Execution End"))
        End Sub

        Private Sub BuildBarChart(ByVal spdDashboard As FpSpread, ByVal intColCount As Integer, ByVal decValues(,) As Decimal, ByVal strXLabels() As String, ByVal intChartIndex As Integer, ByVal intFormat As Integer, ByVal strAppendLabel As String, ByVal blnPercentLabel As Boolean, ByVal intLastActual As Integer, ByVal intDecPlaces As Integer, ByVal blnShowBudget As Boolean)
            Logger.Log.Info(String.Format("Dashboard FpSpread_Load - BuildBarChart Execution Begin"))
            Dim intColCtr As Integer
            Dim intRowCount As Integer = 1
            'Dim cseries As New FarPoint.Web.Chart.ClusteredBarSeries
            'Dim sseries As New FarPoint.Web.Chart.StackedBarSeries
            Dim label As New FarPoint.Web.Chart.LabelArea
            Dim plotArea As New FarPoint.Web.Chart.YPlotArea()
            Dim chart As New FarPoint.Web.Spread.Chart.SpreadChart()
            Dim model As New FarPoint.Web.Chart.ChartModel()
            Dim aSeries As FarPoint.Web.Chart.BarSeries
            aSeries = New FarPoint.Web.Chart.BarSeries
            Dim plotFont As System.Drawing.Font
            plotFont = New System.Drawing.Font("Arial", 8)

            With aSeries
                .SeriesName = "Actual"
                For intColCtr = 0 To intColCount
                    .Values.Add(decValues(0, intColCtr))
                    '   If intSelRowCtr = 0 Then
                    .CategoryNames.Add(strXLabels(intColCtr))
                    Select Case intFormat
                        Case 6
                            If intColCtr > intLastActual Then
                                .BarFills(intColCtr) = New FarPoint.Web.Chart.SolidFill(Drawing.Color.FromArgb(0, 112, 192))
                            Else
                                .BarFills(intColCtr) = New FarPoint.Web.Chart.SolidFill(Drawing.Color.FromArgb(112, 176, 240))
                            End If
                        Case 1 To 5
                            If intColCtr = 0 Then
                                .BarFills(intColCtr) = New FarPoint.Web.Chart.SolidFill(Drawing.Color.FromArgb(0, 112, 192))
                            Else
                                .BarFills(intColCtr) = New FarPoint.Web.Chart.SolidFill(Drawing.Color.FromArgb(112, 176, 240))
                            End If
                        Case Else
                            .BarFills(intColCtr) = New FarPoint.Web.Chart.SolidFill(Drawing.Color.FromArgb(0, 112, 192))
                    End Select
                Next
            End With
            'set properties of plot area
            plotArea.XAxis.LabelTextFont = plotFont
            plotArea.YAxes(0).LabelTextFont = plotFont
            plotArea.Location = New System.Drawing.PointF(0.2F, 0.125F)
            plotArea.Size = New System.Drawing.SizeF(0.7F, 0.7F)
            plotArea.Series.Add(aSeries)
            If blnPercentLabel = True Then
                plotArea.YAxes(0).LabelNumberFormat = "#,##0%"
            Else
                Select Case intDecPlaces
                    Case 0
                        plotArea.YAxes(0).LabelNumberFormat = "#,##0"
                    Case 1
                        plotArea.YAxes(0).LabelNumberFormat = "#,##0.0"
                    Case 2
                        plotArea.YAxes(0).LabelNumberFormat = "#,##0.00"
                    Case 3
                        plotArea.YAxes(0).LabelNumberFormat = "#,##0.000"
                    Case 4
                        plotArea.YAxes(0).LabelNumberFormat = "#,##0.0000"
                    Case 5
                        plotArea.YAxes(0).LabelNumberFormat = "#,##0.00000"
                    Case Else
                        plotArea.YAxes(0).LabelNumberFormat = "#,##0"
                End Select

            End If
            model.PlotAreas.Add(plotArea)
            SetLabelProperties(intFormat, label, strAppendLabel, blnShowBudget)
            model.LabelAreas.Add(label)
            chart.Model = model
            SetChartProperties(chart, intChartIndex)
            spdDashboard.Sheets(0).Charts.Add(chart)
            Logger.Log.Info(String.Format("Dashboard FpSpread_Load - BuildBarChart Execution End"))
        End Sub

        Private Sub BuildAreaChart(ByVal spdDashboard As FpSpread, ByVal intColCount As Integer, ByVal decValues(,) As Decimal, ByVal strXLabels() As String, ByVal intChartIndex As Integer, ByVal intFormat As Integer, ByVal strAppendLabel As String, ByVal blnPercentLabel As Boolean, ByVal intDecPlaces As Integer, ByVal blnShowBudget As Boolean)
            Logger.Log.Info(String.Format("Dashboard FpSpread_Load - BuildAreaChart Execution Begin"))
            Dim intColCtr As Integer
            Dim intRowCount As Integer = 1

            Dim label As New FarPoint.Web.Chart.LabelArea()
            Dim plotArea As New FarPoint.Web.Chart.YPlotArea()
            Dim chart As New FarPoint.Web.Spread.Chart.SpreadChart()
            Dim model As New FarPoint.Web.Chart.ChartModel()
            Dim aSeries As FarPoint.Web.Chart.AreaSeries
            aSeries = New FarPoint.Web.Chart.AreaSeries
            Dim plotFont As System.Drawing.Font
            plotFont = New System.Drawing.Font("Arial", 8)

            With aSeries
                .SeriesName = "Actual"
                For intColCtr = 0 To intColCount
                    .Values.Add(decValues(0, intColCtr))
                    '   If intSelRowCtr = 0 Then
                    .CategoryNames.Add(strXLabels(intColCtr))
                    .AreaFills.Add(New FarPoint.Web.Chart.SolidFill(Drawing.Color.FromArgb(112, 176, 240)))
                Next
            End With
            'set properties of plot area
            plotArea.XAxis.LabelTextFont = plotFont
            plotArea.YAxes(0).LabelTextFont = plotFont
            plotArea.Location = New System.Drawing.PointF(0.2F, 0.125F)
            plotArea.Size = New System.Drawing.SizeF(0.7F, 0.7F)
            plotArea.Series.Add(aSeries)
            If blnPercentLabel = True Then
                plotArea.YAxes(0).LabelNumberFormat = "#,##0%"
            Else
                Select Case intDecPlaces
                    Case 0
                        plotArea.YAxes(0).LabelNumberFormat = "#,##0"
                    Case 1
                        plotArea.YAxes(0).LabelNumberFormat = "#,##0.0"
                    Case 2
                        plotArea.YAxes(0).LabelNumberFormat = "#,##0.00"
                    Case 3
                        plotArea.YAxes(0).LabelNumberFormat = "#,##0.000"
                    Case 4
                        plotArea.YAxes(0).LabelNumberFormat = "#,##0.0000"
                    Case 5
                        plotArea.YAxes(0).LabelNumberFormat = "#,##0.00000"
                    Case Else
                        plotArea.YAxes(0).LabelNumberFormat = "#,##0"
                End Select
            End If
            model.PlotAreas.Add(plotArea)
            SetLabelProperties(intFormat, label, strAppendLabel, blnShowBudget)
            model.LabelAreas.Add(label)
            chart.Model = model
            SetChartProperties(chart, intChartIndex)
            spdDashboard.Sheets(0).Charts.Add(chart)
            Logger.Log.Info(String.Format("Dashboard FpSpread_Load - BuildAreaChart Execution End"))
        End Sub

        Private Function GetLeftPos(ByVal intChartIndex As Integer) As Integer
            Select Case intChartIndex
                Case 0, 3, 6, 9, 12
                    GetLeftPos = 1
                Case 1, 4, 7, 10, 13
                    GetLeftPos = 317
                Case Else
                    GetLeftPos = 631
            End Select
        End Function

        Private Function GetTopPos(ByVal intChartIndex As Integer) As Integer
            Dim strBrowser As String
            strBrowser = Request.Browser.Browser
            GetTopPos = 0
            Select Case strBrowser
                Case "Firefox"
                    Select Case intChartIndex
                        Case 0 To 2
                            GetTopPos = 21
                        Case 3 To 5
                            GetTopPos = 283
                        Case 6 To 8
                            GetTopPos = 545
                        Case 9 To 11
                            GetTopPos = 807
                        Case 12 To 14
                            GetTopPos = 1069
                    End Select
                Case Else
                    Select Case intChartIndex
                        Case 0 To 2
                            GetTopPos = 21
                        Case 3 To 5
                            GetTopPos = 287
                        Case 6 To 8
                            GetTopPos = 551
                        Case 9 To 11
                            GetTopPos = 817
                        Case 12 To 14
                            GetTopPos = 1081
                    End Select
            End Select
        End Function

        Private Sub ComputeTrend(ByRef decIntercept As Decimal, ByRef decSlope As Decimal, ByRef dblCoeff As Double, ByVal decValues(,) As Decimal, intNumbColCount As Integer)
            Try
                Dim intDataPoints As Integer = intNumbColCount
                Dim XFactors(intNumbColCount - 1) As Decimal
                Dim YFactors(intNumbColCount - 1) As Decimal
                Dim intCtr As Integer
                Dim sx, sy, sxy, sx2, sy2, XX, YY, temp As Single

                'load values into arrays
                For intCtr = 0 To intNumbColCount - 1
                    XFactors(intCtr) = intCtr
                    YFactors(intCtr) = decValues(0, intCtr)
                Next
                'compute slope, intercept and coeff
                For intCtr = 0 To intDataPoints - 1
                    XX = XFactors(intCtr)
                    YY = YFactors(intCtr)
                    sx += XX
                    sy += YY
                    sxy = sxy + XX * YY
                    sx2 = sx2 + XX * XX
                    sy2 = sy2 + YY * YY
                Next
                If (intDataPoints * sx2 - sx * sx) <> 0 Then
                    decSlope = (intDataPoints * sxy - sx * sy) / (intDataPoints * sx2 - sx * sx)
                Else
                    decSlope = 0
                End If
                If intDataPoints <> 0 Then
                    decIntercept = (sy - decSlope * sx) / intDataPoints
                Else
                    decIntercept = 0
                End If
                temp = (intDataPoints * sx2 - sx * sx) * (intDataPoints * sy2 - sy * sy)
                If temp <> 0 Then
                    dblCoeff = (intDataPoints * sxy - sx * sy) / Math.Sqrt(temp)
                End If
            Catch ex As Exception

            End Try


        End Sub

        Private Sub SetChartTitle(ByRef spdDashboard As FpSpread, ByVal intChartOrder As Integer, ByVal strDesc As String, intChartFormat As Integer, blnShowasPercent As Boolean)
            Dim intRow As Integer
            Dim intCol As Integer
            ' Dim textcell As New FarPoint.Web.Spread.TextCellType
            'textcell.Multiline = True
            Select Case intChartOrder
                Case 1 To 3
                    intRow = 0
                Case 4 To 6
                    intRow = 3
                Case 7 To 9
                    intRow = 6
                Case 10 To 12
                    intRow = 9
                Case 13 To 15
                    intRow = 12
            End Select
            Select Case intChartOrder
                Case 1, 4, 7, 10, 13
                    intCol = 0
                Case 2, 5, 8, 11, 14
                    intCol = 2
                Case 3, 6, 9, 12, 15
                    intCol = 4
            End Select

            With spdDashboard.Sheets(0)
                Dim intDescLen As Integer = Len(strDesc)
                Select Case intDescLen
                    Case Is > 43
                        .Cells(intRow, intCol).Font.Size = FontSize.Medium
                        .Cells(intRow, intCol).Font.Bold = True
                    Case Is > 37
                        .Cells(intRow, intCol).Font.Size = FontSize.Large
                        .Cells(intRow, intCol).Font.Bold = True
                End Select

                .SetText(intRow, intCol, strDesc)
            End With

        End Sub

        Private Sub SetChartProperties(ByRef chart As FarPoint.Web.Spread.Chart.SpreadChart, ByVal intChartIndex As Integer)

            chart.Width = intChartWidth
            chart.Height = intChartHeight
            chart.Left = GetLeftPos(intChartIndex)
            chart.Top = GetTopPos(intChartIndex)
            chart.CanMove = False
            chart.CanSelect = False
            chart.Model.Fill = New FarPoint.Web.Chart.SolidFill(Drawing.Color.FromArgb(248, 248, 248))
            chart.BorderColor = Drawing.Color.FromArgb(248, 248, 248)
        End Sub

        Private Sub StoreMultiYear(ByVal intFormat As Integer, ByVal decSaveAmts() As Decimal, ByRef decChartAmts(,) As Decimal, ByRef intBalCtr As Integer, ByVal intSelPeriod As Integer, ByVal blnShowasPercent As Boolean, ByVal decTotalBalances() As Decimal, ByVal intIndex As Integer)
            Dim decAmttoStore As Decimal

            Select Case intFormat
                Case 8
                    If blnShowasPercent = False Then
                        decAmttoStore = decSaveAmts(intSelPeriod)
                    Else
                        decAmttoStore = ComputePercent(decSaveAmts(intSelPeriod), decTotalBalances(intIndex))
                    End If
                Case 9
                    decAmttoStore = 0
                    For intCtr = 0 To intSelPeriod
                        decAmttoStore += decSaveAmts(intCtr)
                    Next
                    If blnShowasPercent = True Then
                        decAmttoStore = ComputePercent(decAmttoStore, decTotalBalances(intIndex))
                    End If
            End Select
            If decAmttoStore <> 0 Or intBalCtr <> -1 Then
                intBalCtr += 1
                decChartAmts(0, intBalCtr) = decAmttoStore
            End If
        End Sub

        Private Sub SetLabelProperties(intChartFormat As Integer, label As FarPoint.Web.Chart.LabelArea, ByVal strAppendLabel As String, ByVal blnShowBudget As Boolean)
            Dim StrDesc As String = ""
            Dim decXPos As Decimal

            Dim LabFont As System.Drawing.Font
            LabFont = New System.Drawing.Font("Arial", 9, Drawing.FontStyle.Regular)
            label.TextFont = LabFont

            Select Case intChartFormat
                Case 1
                    StrDesc = "Budget vs Actual"
                Case 2
                    StrDesc = "Budget vs Actual YTD"
                Case 3
                    StrDesc = "Current vs Prior Month"
                Case 4
                    StrDesc = "Current vs Prior Yr"
                Case 5
                    StrDesc = "Current vs Prior Yr YTD"
                Case 6
                    If blnShowBudget = False Then
                        StrDesc = "Current Year Results"
                    Else
                        StrDesc = "Current Year Forecast"
                    End If

                Case 7
                    StrDesc = "Last 12 Months"
                Case 8
                    StrDesc = "Multi Year Trend"
                Case 9
                    StrDesc = "Multi Year Trend YTD"
                Case 10
                    StrDesc = "12 month rolling forecast"
                Case 13
                    StrDesc = "Current year budget"
            End Select
            If strAppendLabel <> "" Then
                StrDesc = StrDesc & strAppendLabel
            End If
            Dim intLength As Integer = Len(StrDesc)
            decXPos = (1 - (intLength * 0.018)) / 2
            label.Location = New System.Drawing.PointF(decXPos, 0.01F)
            label.Text = StrDesc
        End Sub
        Private Sub StoreTotalBal(ByVal balance As Balance, ByVal intFormat As Integer, ByRef decTotalBalances() As Decimal, ByVal intPeriod As Integer, ByVal blnShowBudget As Boolean)
            Logger.Log.Info(String.Format("Dashboard FpSpread_Load - StoreTotalBal Execution Begin"))
            Dim decPeriodAmts(11) As Decimal


            Select Case intFormat
                Case 1, 2 'budget vs actual
                    If blnArchBudget = False Then
                        CommonProcedures.GetBudgetAmts(balance, decPeriodAmts)
                    Else
                        CommonProcedures.GetArchivedBudgetAmts(balance, decPeriodAmts)
                    End If
                    '  GetBudgetAmts(balance, decPeriodAmts)
                    CommonProcedures.AccumRunningTotals(intFormat - 1, decTotalBalances, intPeriod, decPeriodAmts, 0)
                    GetActualAmts(balance, decPeriodAmts)
                    CommonProcedures.AccumRunningTotals(intFormat - 1, decTotalBalances, intPeriod, decPeriodAmts, 1)
                Case 3  'current vs prior period
                    GetActualAmts(balance, decPeriodAmts)
                    If intPeriod = 0 Then
                        CommonProcedures.AccumRunningTotals(intFormat - 1, decTotalBalances, intPeriod, decPeriodAmts, 1)
                        GetH5Amts(balance, decPeriodAmts)
                        CommonProcedures.AccumRunningTotals(intFormat - 1, decTotalBalances, 11, decPeriodAmts, 0)
                    Else
                        CommonProcedures.AccumRunningTotals(intFormat - 1, decTotalBalances, intPeriod - 1, decPeriodAmts, 0)
                        CommonProcedures.AccumRunningTotals(intFormat - 1, decTotalBalances, intPeriod, decPeriodAmts, 1)
                    End If
                Case 4, 5
                    GetH5Amts(balance, decPeriodAmts)
                    CommonProcedures.AccumRunningTotals(intFormat - 1, decTotalBalances, intPeriod, decPeriodAmts, 0)
                    GetActualAmts(balance, decPeriodAmts)
                    CommonProcedures.AccumRunningTotals(intFormat - 1, decTotalBalances, intPeriod, decPeriodAmts, 1)
                Case 6 'current year trend
                    GetActualAmts(balance, decPeriodAmts)
                    For intColumnCounter = 0 To intPeriod
                        decTotalBalances(intColumnCounter) = decPeriodAmts(intColumnCounter)
                    Next
                    If intPeriod < 11 Then
                        GetBudgetAmts(balance, decPeriodAmts)
                        For intColumnCounter = intPeriod + 1 To 11
                            decTotalBalances(intColumnCounter) = decPeriodAmts(intColumnCounter)
                        Next
                    End If
                Case 7 'last 12 months
                    GetH5Amts(balance, decPeriodAmts)
                    For intColumnCounter = 0 To (11 - (intSelPeriod + 1))
                        decTotalBalances(intColumnCounter) = decPeriodAmts((intSelPeriod + 1) + intColumnCounter)
                    Next
                    GetActualAmts(balance, decPeriodAmts)
                    For intColumnCounter = 0 To intSelPeriod
                        decTotalBalances((11 - intSelPeriod) + intColumnCounter) = decPeriodAmts(intColumnCounter)
                    Next
                Case 8, 9  'multi-yr trend
                    GetH1Amts(balance, decPeriodAmts)
                    CommonProcedures.AccumRunningTotals(intFormat - 1, decTotalBalances, intPeriod, decPeriodAmts, 0)
                    GetH2Amts(balance, decPeriodAmts)
                    CommonProcedures.AccumRunningTotals(intFormat - 1, decTotalBalances, intPeriod, decPeriodAmts, 1)
                    GetH3Amts(balance, decPeriodAmts)
                    CommonProcedures.AccumRunningTotals(intFormat - 1, decTotalBalances, intPeriod, decPeriodAmts, 2)
                    GetH4Amts(balance, decPeriodAmts)
                    CommonProcedures.AccumRunningTotals(intFormat - 1, decTotalBalances, intPeriod, decPeriodAmts, 3)
                    GetH5Amts(balance, decPeriodAmts)
                    CommonProcedures.AccumRunningTotals(intFormat - 1, decTotalBalances, intPeriod, decPeriodAmts, 4)
                    GetActualAmts(balance, decPeriodAmts)
                    CommonProcedures.AccumRunningTotals(intFormat - 1, decTotalBalances, intPeriod, decPeriodAmts, 5)
                Case 10
                    GetBudgetAmts(balance, decPeriodAmts)
                    For intColumnCounter = 0 To (11 - (intSelPeriod + 1))
                        decTotalBalances(intColumnCounter) = decPeriodAmts((intSelPeriod + 1) + intColumnCounter)
                    Next
                    GetBudget2Amts(balance, decPeriodAmts)
                    For intColumnCounter = 0 To intSelPeriod
                        decTotalBalances((11 - intSelPeriod) + intColumnCounter) = decPeriodAmts(intColumnCounter)
                    Next
                Case 13
                    If blnArchBudget = False Then
                        CommonProcedures.GetBudgetAmts(balance, decPeriodAmts)
                    Else
                        CommonProcedures.GetArchivedBudgetAmts(balance, decPeriodAmts)
                    End If
                    For intColumnCounter = 0 To 11
                        decTotalBalances(intColumnCounter) = decPeriodAmts(intColumnCounter)
                    Next
                Case 14
                    CommonProcedures.GetBudgetAmts(balance, decPeriodAmts)
                    For intColumnCounter = 0 To 11
                        decTotalBalances(intColumnCounter) = decPeriodAmts(intColumnCounter)
                    Next
                Case 15

                Case 16
                    CommonProcedures.GetBudgetAmts(balance, decPeriodAmts)
                    For intColumnCounter = 0 To 11
                        decTotalBalances(intColumnCounter) = decPeriodAmts(intColumnCounter)
                    Next
                Case 17

                Case 18
                    CommonProcedures.GetH1Amts(balance, decPeriodAmts)
                    For intColumnCounter = 0 To 11
                        decTotalBalances(intColumnCounter) = decPeriodAmts(intColumnCounter)
                    Next
                Case 19

            End Select
            Logger.Log.Info(String.Format("Dashboard FpSpread_Load - StoreTotalBal Execution End"))
        End Sub
        Private Function ComputePercent(ByVal decItemAmt As Decimal, ByVal decTotalAmt As Decimal) As Decimal
            Logger.Log.Info(String.Format("Dashboard FpSpread_Load - ComputePercent Execution Begin"))
            Try
                ComputePercent = (decItemAmt / decTotalAmt) '* 100
            Catch ex As Exception
                ComputePercent = 0
            Finally
                Logger.Log.Info(String.Format("Dashboard FpSpread_Load - ComputePercent Execution End"))
            End Try
        End Function

        Private Function ComputeTotal(ByVal intSelPeriod As Integer, ByVal decTotalBalances() As Decimal) As Decimal
            Logger.Log.Info(String.Format("Dashboard FpSpread_Load - ComputeTotal Execution Begin"))
            Dim intCtr As Integer

            ComputeTotal = 0
            For intCtr = 0 To intSelPeriod
                ComputeTotal += decTotalBalances(intCtr)
            Next
            Logger.Log.Info(String.Format("Dashboard FpSpread_Load - ComputeTotal Execution End"))
        End Function

        <AcceptVerbs(HttpVerbs.Get)> _
        <CustomActionFilter()>
        Public Function GetSavedReports(ByVal SelectedAnalysisId As Integer) As JsonResult

            If (Not Session("UserType") Is Nothing) And (Not Session("UserInfo") Is Nothing) Then
                UserType = DirectCast(Session("UserType"), UserRole)
                UserInfo = DirectCast(Session("UserInfo"), User)
                CustomerId = If(UserType.UserRoleId = UserRoles.SAU, UserInfo.CustomerId, Nothing)

                'When user logged in, it will show the company list based on analysisId               

                If (Not Session("SelectedAnalysisFromDropdown") Is Nothing) Then
                    SelectedAnalysisId = Session("SelectedAnalysisFromDropdown").ToString()
                End If

            End If

            Dim data As SelectList = utility.PouplateSavedReports(SelectedAnalysisId)
            Return Json(data, JsonRequestBehavior.AllowGet)

        End Function

        Public Function SetSession(dataToSave As Integer) As JsonResult
            If (Not Session("UserType") Is Nothing) And (Not Session("UserInfo") Is Nothing) Then
                UserInfo = DirectCast(Session("UserInfo"), User)
            End If
            If (String.IsNullOrEmpty(dataToSave)) Then
                Return Json(New With {.message = "Empty data to save"}, JsonRequestBehavior.AllowGet)
            End If
            UserInfo.currentTabIndex = dataToSave
            Return Json(New With {.message = "Saved"}, JsonRequestBehavior.AllowGet)
        End Function

        Private Function ArchiveBudgetPresent(ByRef intHistYears As Integer) As Boolean
            Dim decPeriodAmts(11) As Decimal
            Dim intCtr As Integer

            Logger.Log.Info(String.Format("DashboardController:IsArchiveBudget execution Ends"))
            ArchiveBudgetPresent = False
            Dim selectedAccounts = (From a In utility.AccountRepository.GetAccountRecordsByAnalysisId(intAnalysisID)
                                           Join ac In utility.AccountTypeRepository.GetAccountTypeRecordsByAnalysisId(intAnalysisID, typeDescList)
                                           On a.AcctTypeId Equals ac.AcctTypeId Where a.AnalysisId = ac.AnalysisId
                                           Order By a.SortSequence
                                           Select a.AccountId, a.AcctTypeId, a.AnalysisId, a.AcctDescriptor, a.SortSequence, a.Description, a.Subgrouping, ac.TypeDesc, ac.ClassDesc
                                         ).ToList()

            Dim balanceAcc = (From b In utility.BalanceRepository.GetBalanceRecordsByAnalysisId(intAnalysisID)
                           Join acc In selectedAccounts On b.AccountId Equals acc.AccountId
                           Where acc.AnalysisId = b.AnalysisId
                           Select b).ToList()

            For Each account In selectedAccounts
                Select Case True
                    Case account.AcctDescriptor = "NITotal"
                        Dim accountId As Integer = account.AccountId

                        For Each balance As Balance In (From b In balanceAcc.Where(Function(a) a.AccountId = accountId And a.AnalysisId = intAnalysisID) Select b)
                            'determine if there are archived budget amounts
                            GetArchivedBudgetAmts(balance, decPeriodAmts)
                            For intColumnCounter = 0 To 11
                                If decPeriodAmts(intColumnCounter) <> 0 Then
                                    ArchiveBudgetPresent = True
                                    Exit For
                                End If
                            Next
                            'determine number of historical periods with data
                            Dim blnAmtFound As Boolean
                            intHistYears = 5
                            For intCtr = 0 To 4
                                Select Case intCtr
                                    Case 0
                                        GetH1Amts(balance, decPeriodAmts)
                                    Case 1
                                        GetH2Amts(balance, decPeriodAmts)
                                    Case 2
                                        GetH3Amts(balance, decPeriodAmts)
                                    Case 3
                                        GetH4Amts(balance, decPeriodAmts)
                                    Case 4
                                        GetH5Amts(balance, decPeriodAmts)
                                End Select
                                For intColumnCounter = 0 To 11
                                    If decPeriodAmts(intColumnCounter) <> 0 Then
                                        blnAmtFound = True
                                        Exit For
                                    End If
                                Next
                                If blnAmtFound = True Then
                                    intHistYears -= intCtr
                                    Exit For
                                End If
                            Next
                        Next


                        Exit For
                End Select
            Next
            Logger.Log.Info(String.Format("DashboardController:IsArchiveBudget execution Ends"))
        End Function

        <AcceptVerbs(HttpVerbs.Get)> _
        <CustomActionFilter()>
        Public Function GetDashBoardChartCount() As Integer
            UserInfo = DirectCast(Session("UserInfo"), User)
            Dim dashboardItems = (From d In utility.DashboardRepository.GetDashboardDetail(UserInfo.UserId, UserInfo.AnalysisId, True)
                                   Join a In utility.AccountRepository.GetAccountRecordsByAnalysisId(UserInfo.AnalysisId) On a.AccountId Equals d.AccountId And a.AnalysisId Equals d.AnalysisId
                                   Join at In utility.AccountTypeRepository.GetAccountTypeRecordsByAnalysisId(UserInfo.AnalysisId) On at.AcctTypeId Equals a.AcctTypeId And at.AnalysisId Equals a.AnalysisId
                                   Select New With {d.DashboardId, d.Order, d.DashDescription, d.ChartFormatId, d.ChartTypeId, d.ShowAsPercent, _
                                                    d.ShowTrendline, d.ShowGoal, d.ShowBudget, d.Period1Goal, d.GoalGrowthRate, d.AccountId, d.AnalysisId, at.TypeDesc, a.NumberFormat}
                                   ).ToList().Count
            Return dashboardItems


        End Function


#Region "HighChart Related Things"
        <AcceptVerbs(HttpVerbs.Get)> _
       <CustomActionFilter()>
        Public Function GetChartData() As JsonResult
            'Dim spread As FpSpread = DirectCast(sender, FpSpread)

            Dim decSaveAmts(12) As Decimal

            Dim intCurrentMonth As Integer
            Dim intPeriods As Integer
            Dim strChartDesc As String
            Dim list As New List(Of HighChart)

            UserInfo = DirectCast(Session("UserInfo"), User)
            intAnalysisID = UserInfo.AnalysisId
            intSelPeriod = Session("sPeriod")
            intFirstFiscal = UserInfo.fiscalMonthOfSelectedCompany
            blnArchBudget = DirectCast(Session("BudgetArch"), Boolean)
            Dim arrayLength As Integer
            Dim rowCount As Integer
            'It will get the records which match with selected analysis & dashboard items selected by Logged-in user.
            'Dim dashboardItems = utility.DashboardRepository.GetDashboardDetail.Where(Function(d) d.UserId = UserInfo.UserId And d.AnalysisId = UserInfo.AnalysisId And d.IsValid = True)


            Logger.Log.Info(String.Format("Dashboard FpSpread_Load- dashboardItems Query Execution Begin"))

            Dim dashboardItems = (From d In utility.DashboardRepository.GetDashboardDetail(UserInfo.UserId, UserInfo.AnalysisId, True)
                                   Join a In utility.AccountRepository.GetAccountRecordsByAnalysisId(UserInfo.AnalysisId) On a.AccountId Equals d.AccountId And a.AnalysisId Equals d.AnalysisId
                                   Join at In utility.AccountTypeRepository.GetAccountTypeRecordsByAnalysisId(UserInfo.AnalysisId) On at.AcctTypeId Equals a.AcctTypeId And at.AnalysisId Equals a.AnalysisId Order By d.Order Ascending
                                   Select New With {d.DashboardId, d.Order, d.DashDescription, d.ChartFormatId, d.ChartFormatTypeId, d.ChartTypeId, d.ShowAsPercent, _
                                                    d.ShowTrendline, d.ShowGoal, d.ShowBudget, d.Period1Goal, d.GoalGrowthRate, d.AccountId, d.AnalysisId, at.TypeDesc, a.NumberFormat}
                                   ).ToList()

            Logger.Log.Info(String.Format("Dashboard FpSpread_Load - dashboardItems Query Execution End"))

            Logger.Log.Info(String.Format("Dashboard:FpSpread_Load --> Balance Query execution Starts"))
            Dim balanceDash = (From b In utility.BalanceRepository.GetBalanceRecordsByAnalysisId(intAnalysisID)
                            Join di In dashboardItems On b.AccountId Equals di.AccountId And di.AnalysisId Equals b.AnalysisId
                            Select b).ToList()

            Logger.Log.Info(String.Format("Dashboard:FpSpread_Load --> Balance Query execution Starts"))

            If (Not dashboardItems Is Nothing) Then
                'SetDashboardProperties(spread, dashboardItems.Count)
                For Each account In dashboardItems
                    rowCount = dashboardDetailRepository.GetNumberOfRowCount(account.DashboardId)
                    arrayLength = ((13 * rowCount) - 1)
                    Dim highChart As HighChart = New HighChart
                    Dim highchartDonut As HighChartModelForDonutAndPie = New HighChartModelForDonutAndPie
                    Dim decChartAmts(0, arrayLength) As Decimal
                    Dim strXLabels(12) As String
                    Dim accountId As Integer = account.AccountId
                    Dim analysisId As Integer = account.AnalysisId
                    Dim intFormat As Integer = account.ChartFormatId
                    Dim blnShowBudget As Boolean = account.ShowBudget
                    Dim blnShowasPercent As Boolean = account.ShowAsPercent
                    Dim blnPercentLabel As Boolean = account.ShowAsPercent
                    Dim decTotalBalances(11) As Decimal
                    Dim strYearType(5) As String
                    Dim intCtr As Integer
                    Dim intOrder As Integer = account.Order
                    Dim intBalCtr As Integer = -1
                    Dim intLastActual As Integer = -1
                    Dim strAppendLabel As String = ""
                    Dim intDecPlaces As Integer
                    Dim decChartAmtsForSecondSeries(0, 12) As Decimal
                    Dim seriesLabel As New List(Of String)
                    If (arrayLength <> -1) Then
                        Array.Clear(decChartAmts, 0, arrayLength)
                    End If
                    intCurrentMonth = intSelPeriod
                    '   If blnShowasPercent = True Then
                    'get TypeDesc for the account used for this dashboard item
                    If blnShowasPercent = True Then
                        Select Case account.TypeDesc.ToLower()
                            Case "assets", "liabilities and equity"                        '      
                                'get balances for account with AcctDescriptor of "ATotal"

                                Logger.Log.Info(String.Format("Dashboard FpSpread_Load - Case : assets, liabilities and equity - balanceAccum Query Execution Begin"))
                                Dim balanceAccum = (From b In utility.BalanceRepository.GetBalanceRecordsByAnalysisId(analysisId)
                                                      Join a In utility.AccountRepository.GetAccountRecordsByAnalysisId(analysisId) On a.AccountId Equals b.AccountId And a.AnalysisId Equals b.AnalysisId
                                                      Select b).ToList()
                                Logger.Log.Info(String.Format("Dashboard FpSpread_Load - Case : assets, liabilities and equity - balanceAccum Query Execution Begin"))

                                Logger.Log.Info(String.Format("Dashboard FpSpread_Load - Case : assets, liabilities and equity - balanceAccum For Each Execution Begin"))
                                For Each balance As Balance In (From b In balanceAccum)

                                    StoreTotalBal(balance, intFormat, decTotalBalances, intSelPeriod, blnShowBudget)
                                    Exit For
                                Next
                                Logger.Log.Info(String.Format("Dashboard FpSpread_Load - Case : assets, liabilities and equity - balanceAccum For Each Execution End"))
                            Case "revenue", "expense"

                                Logger.Log.Info(String.Format("Dashboard FpSpread_Load - Case : revenue, expense - balanceAccum Query Execution Begin"))
                                Dim balanceAccum = (From b In utility.BalanceRepository.GetBalanceRecordsByAnalysisId(analysisId)
                                                     Join a In utility.AccountRepository.GetAccountRecordsByAnalysisId(analysisId) On a.AccountId Equals b.AccountId And a.AnalysisId Equals b.AnalysisId
                                                     Join at In utility.AccountTypeRepository.GetAccountTypeRecordsByAnalysisId(analysisId, New String() {"revenue"}) On at.AcctTypeId Equals a.AcctTypeId And at.AnalysisId Equals a.AnalysisId
                                                     Select b).ToList()
                                Logger.Log.Info(String.Format("Dashboard FpSpread_Load - Case : revenue, expense - balanceAccum Query Execution Begin"))

                                Dim count As Integer = balanceAccum.Count
                                Logger.Log.Info(String.Format("Dashboard FpSpread_Load - Case : revenue, expense - balanceAccum For Each Execution Begin"))
                                For Each balance As Balance In (From b In balanceAccum)
                                    StoreTotalBal(balance, intFormat, decTotalBalances, intSelPeriod, blnShowBudget)
                                Next
                                Logger.Log.Info(String.Format("Dashboard FpSpread_Load - Case : revenue, expense - balanceAccum For Each Execution End"))
                            Case Else
                                blnShowasPercent = False
                        End Select
                    End If

                    '    End If
                    Logger.Log.Info(String.Format("Dashboard FpSpread_Load - Balance For Each Execution Begin"))

                    If account.ChartTypeId = 2 Or account.ChartTypeId = 4 Or account.ChartTypeId = 5 Then
                        'It will get the record from balance of selected analysis and account item selected by user.
                        For Each balance As Balance In (From b In balanceDash.Where(Function(a) a.AccountId = accountId And a.AnalysisId = analysisId) Select b)
                            If account.NumberFormat = 99 Then
                                blnPercentLabel = True
                            Else
                                If account.TypeDesc.ToLower() = "cashflow" Then
                                    intDecPlaces = 0
                                Else
                                    intDecPlaces = account.NumberFormat
                                End If
                            End If
                            Select Case account.ChartFormatId
                                Case 1, 2
                                    If blnArchBudget = False Then
                                        CommonProcedures.GetBudgetAmts(balance, decSaveAmts)
                                    Else
                                        CommonProcedures.GetArchivedBudgetAmts(balance, decSaveAmts)
                                    End If
                                    If account.ChartFormatId = 1 Then
                                        If blnShowasPercent = False Then
                                            decChartAmts(0, 0) = decSaveAmts(intSelPeriod)
                                        Else
                                            decChartAmts(0, 0) = ComputePercent(decSaveAmts(intSelPeriod), decTotalBalances(0))
                                        End If
                                    Else
                                        For intCtr = 0 To intSelPeriod
                                            decChartAmts(0, 0) += decSaveAmts(intCtr)
                                        Next
                                        If blnShowasPercent = True Then
                                            '    decTotalAccum = ComputeTotal(intSelPeriod, decTotalBalances)
                                            decChartAmts(0, 0) = ComputePercent(decChartAmts(0, 0), decTotalBalances(0))
                                        End If
                                    End If
                                    CommonProcedures.GetActualAmts(balance, decSaveAmts)
                                    If account.ChartFormatId = 1 Then
                                        If blnShowasPercent = False Then
                                            decChartAmts(0, 1) = decSaveAmts(intSelPeriod)
                                        Else
                                            decChartAmts(0, 1) = ComputePercent(decSaveAmts(intSelPeriod), decTotalBalances(1))
                                        End If
                                    Else
                                        For intCtr = 0 To intSelPeriod
                                            decChartAmts(0, 1) += decSaveAmts(intCtr)
                                        Next
                                        If blnShowasPercent = True Then
                                            '   decTotalAccum = ComputeTotal(intSelPeriod, decTotalBalances)
                                            decChartAmts(0, 1) = ComputePercent(decChartAmts(0, 1), decTotalBalances(1))
                                        End If
                                    End If
                                    strXLabels(0) = "Budget"
                                    strXLabels(1) = "Actual"
                                    intPeriods = 1
                                    intLastActual = 0
                                Case 3
                                    If intSelPeriod = 0 Then
                                        CommonProcedures.GetH5Amts(balance, decSaveAmts)
                                        If blnShowasPercent = False Then
                                            decChartAmts(0, 0) = decSaveAmts(11)
                                        Else
                                            decChartAmts(0, 0) = ComputePercent(decSaveAmts(11), decTotalBalances(0))
                                        End If
                                        CommonProcedures.GetActualAmts(balance, decSaveAmts)
                                        If blnShowasPercent = False Then
                                            decChartAmts(0, 1) = decSaveAmts(intSelPeriod)
                                        Else
                                            decChartAmts(0, 1) = ComputePercent(decSaveAmts(intSelPeriod), decTotalBalances(1))
                                        End If
                                    Else
                                        CommonProcedures.GetActualAmts(balance, decSaveAmts)
                                        If blnShowasPercent = False Then
                                            decChartAmts(0, 0) = decSaveAmts(intSelPeriod - 1)
                                            decChartAmts(0, 1) = decSaveAmts(intSelPeriod)
                                        Else
                                            decChartAmts(0, 0) = ComputePercent(decSaveAmts(intSelPeriod - 1), decTotalBalances(0))
                                            decChartAmts(0, 1) = ComputePercent(decSaveAmts(intSelPeriod), decTotalBalances(1))
                                        End If
                                    End If
                                    strXLabels(0) = "Prior"
                                    strXLabels(1) = "Current"
                                    intPeriods = 1
                                    intLastActual = 0
                                Case 4, 5
                                    CommonProcedures.GetH5Amts(balance, decSaveAmts)
                                    If account.ChartFormatId = 4 Then
                                        decChartAmts(0, 0) = decSaveAmts(intSelPeriod)
                                    Else
                                        For intCtr = 0 To intSelPeriod
                                            decChartAmts(0, 0) += decSaveAmts(intCtr)
                                        Next
                                    End If
                                    If blnShowasPercent = True Then
                                        decChartAmts(0, 0) = ComputePercent(decChartAmts(0, 0), decTotalBalances(0))
                                    End If
                                    CommonProcedures.GetActualAmts(balance, decSaveAmts)
                                    If account.ChartFormatId = 4 Then
                                        decChartAmts(0, 1) = decSaveAmts(intSelPeriod)
                                    Else
                                        For intCtr = 0 To intSelPeriod
                                            decChartAmts(0, 1) += decSaveAmts(intCtr)
                                        Next
                                    End If
                                    If blnShowasPercent = True Then
                                        decChartAmts(0, 1) = ComputePercent(decChartAmts(0, 1), decTotalBalances(1))
                                    End If
                                    strXLabels(0) = "Prior"
                                    strXLabels(1) = "Current"
                                    intPeriods = 1
                                    intLastActual = 0
                                Case 6 'current year trend                                
                                    If blnShowBudget = False Then
                                        intPeriods = intSelPeriod
                                        GetActualAmts(balance, decSaveAmts)
                                        For intCtr = 0 To intSelPeriod
                                            If blnShowasPercent = False Then
                                                decChartAmts(0, intCtr) = decSaveAmts(intCtr)
                                            Else
                                                If decTotalBalances(intCtr) <> 0 Then
                                                    decChartAmts(0, intCtr) = ComputePercent(decSaveAmts(intCtr), decTotalBalances(intCtr))
                                                End If
                                            End If
                                        Next
                                    Else
                                        intPeriods = 11
                                        GetBudgetAmts(balance, decSaveAmts)
                                        For intCtr = 0 To 11
                                            If blnShowasPercent = False Then
                                                decChartAmts(0, intCtr) = decSaveAmts(intCtr)
                                            Else
                                                If decTotalBalances(intCtr) <> 0 Then
                                                    decChartAmts(0, intCtr) = ComputePercent(decSaveAmts(intCtr), decTotalBalances(intCtr))
                                                End If
                                            End If
                                        Next
                                        GetActualAmts(balance, decSaveAmts)

                                        For intCtr = intSelPeriod To 0 Step -1
                                            If intLastActual = -1 Then
                                                If decSaveAmts(intCtr) <> 0 Then
                                                    intLastActual = intCtr
                                                    If blnShowasPercent = False Then
                                                        decChartAmts(0, intCtr) = decSaveAmts(intCtr)
                                                    Else
                                                        If decTotalBalances(intCtr) <> 0 Then
                                                            decChartAmts(0, intCtr) = ComputePercent(decSaveAmts(intCtr), decTotalBalances(intCtr))
                                                        End If
                                                    End If
                                                End If
                                            Else
                                                If blnShowasPercent = False Then
                                                    decChartAmts(0, intCtr) = decSaveAmts(intCtr)
                                                Else
                                                    If decTotalBalances(intCtr) <> 0 Then
                                                        decChartAmts(0, intCtr) = ComputePercent(decSaveAmts(intCtr), decTotalBalances(intCtr))
                                                    End If
                                                End If
                                            End If
                                        Next
                                        If intLastActual < intSelPeriod Then
                                            intCurrentMonth = intLastActual
                                        End If
                                    End If


                                Case 7 'last 12 months
                                    GetH5Amts(balance, decSaveAmts)
                                    If intSelPeriod < 11 Then
                                        For intCtr = 0 To (11 - (intSelPeriod + 1))
                                            decChartAmts(0, intCtr) = decSaveAmts((intSelPeriod + 1) + intCtr)
                                            If blnShowasPercent = True Then
                                                decChartAmts(0, intCtr) = ComputePercent(decChartAmts(0, intCtr), decTotalBalances(intCtr))
                                            End If
                                        Next
                                    End If
                                    GetActualAmts(balance, decSaveAmts)
                                    For intCtr = 0 To intSelPeriod
                                        decChartAmts(0, (11 - intSelPeriod) + intCtr) = decSaveAmts(intCtr)
                                        If blnShowasPercent = True Then
                                            decChartAmts(0, intCtr + (11 - intSelPeriod)) = ComputePercent(decChartAmts(0, intCtr + (11 - intSelPeriod)), decTotalBalances(intCtr + (11 - intSelPeriod)))
                                        End If
                                    Next
                                    intPeriods = 11
                                Case 8, 9  'multiyear trend                               
                                    GetH1Amts(balance, decSaveAmts)
                                    StoreMultiYear(intFormat, decSaveAmts, decChartAmts, intBalCtr, intSelPeriod, blnShowasPercent, decTotalBalances, 0)
                                    GetH2Amts(balance, decSaveAmts)
                                    StoreMultiYear(intFormat, decSaveAmts, decChartAmts, intBalCtr, intSelPeriod, blnShowasPercent, decTotalBalances, 1)
                                    GetH3Amts(balance, decSaveAmts)
                                    StoreMultiYear(intFormat, decSaveAmts, decChartAmts, intBalCtr, intSelPeriod, blnShowasPercent, decTotalBalances, 2)
                                    GetH4Amts(balance, decSaveAmts)
                                    StoreMultiYear(intFormat, decSaveAmts, decChartAmts, intBalCtr, intSelPeriod, blnShowasPercent, decTotalBalances, 3)
                                    GetH5Amts(balance, decSaveAmts)
                                    StoreMultiYear(intFormat, decSaveAmts, decChartAmts, intBalCtr, intSelPeriod, blnShowasPercent, decTotalBalances, 4)
                                    GetActualAmts(balance, decSaveAmts)
                                    StoreMultiYear(intFormat, decSaveAmts, decChartAmts, intBalCtr, intSelPeriod, blnShowasPercent, decTotalBalances, 5)
                                    intPeriods = intBalCtr
                                Case 10 'rolling forcast
                                    GetBudgetAmts(balance, decSaveAmts)
                                    If intSelPeriod < 11 Then
                                        For intCtr = 0 To (11 - (intSelPeriod + 1))
                                            decChartAmts(0, intCtr) = decSaveAmts((intSelPeriod + 1) + intCtr)
                                            If blnShowasPercent = True Then
                                                decChartAmts(0, intCtr) = ComputePercent(decChartAmts(0, intCtr), decTotalBalances(intCtr))
                                            End If
                                        Next
                                    End If
                                    GetBudget2Amts(balance, decSaveAmts)
                                    For intCtr = 0 To intSelPeriod
                                        decChartAmts(0, (11 - intSelPeriod) + intCtr) = decSaveAmts(intCtr)
                                        If blnShowasPercent = True Then
                                            decChartAmts(0, intCtr + (11 - intSelPeriod)) = ComputePercent(decChartAmts(0, intCtr + (11 - intSelPeriod)), decTotalBalances(intCtr + (11 - intSelPeriod)))
                                        End If
                                    Next
                                    intPeriods = 11
                                Case 13
                                    intPeriods = 11
                                    If blnArchBudget = False Then
                                        CommonProcedures.GetBudgetAmts(balance, decSaveAmts)
                                    Else
                                        CommonProcedures.GetArchivedBudgetAmts(balance, decSaveAmts)
                                    End If
                                    For intCtr = 0 To intPeriods
                                        decChartAmts(0, intCtr) = decSaveAmts(intCtr)
                                        If blnShowasPercent = True Then
                                            decChartAmts(0, intCtr) = ComputePercent(decChartAmts(0, intCtr), decTotalBalances(intCtr))
                                        End If
                                    Next
                            End Select
                            Exit For
                        Next
                        Logger.Log.Info(String.Format("Dashboard FpSpread_Load - Balance For Each Execution End"))
                        If rowCount = 2 Then
                            intLastActual = -1
                            intAnalysisID = UserInfo.AnalysisId
                            intSelPeriod = Session("sPeriod")
                            intFirstFiscal = UserInfo.fiscalMonthOfSelectedCompany
                            blnArchBudget = DirectCast(Session("BudgetArch"), Boolean)

                            Dim FirstSeriesTitle = dashboardDetailRepository.GetSeriesLabel(account.DashboardId, account.AccountId)
                            seriesLabel.Add(FirstSeriesTitle)
                            Dim decSaveAmtsForSecondSeries(12) As Decimal

                            Array.Clear(decChartAmtsForSecondSeries, 0, 12)
                            Array.Clear(decSaveAmtsForSecondSeries, 0, 12)
                            Dim AccountIdForSecondSeries = dashboardDetailRepository.GetSecondSeriesAccountId(account.DashboardId, account.AccountId)
                            Dim balanceDashForSecondSeries = (From b In utility.BalanceRepository.GetBalanceRecordsByAnalysisId(intAnalysisID)
                                                              Where b.AccountId = AccountIdForSecondSeries.AccountId And b.AnalysisId = account.AnalysisId Select b).ToList()
                            seriesLabel.Add(AccountIdForSecondSeries.DashboardDescription)
                            For Each balanceDashForSecond As Balance In balanceDashForSecondSeries
                                If account.NumberFormat = 99 Then
                                    blnPercentLabel = True
                                Else
                                    If account.TypeDesc.ToLower() = "cashflow" Then
                                        intDecPlaces = 0
                                    Else
                                        intDecPlaces = account.NumberFormat
                                    End If
                                End If
                                Select Case account.ChartFormatId
                                    Case 1, 2
                                        If blnArchBudget = False Then
                                            CommonProcedures.GetBudgetAmts(balanceDashForSecond, decSaveAmtsForSecondSeries)
                                        Else
                                            CommonProcedures.GetArchivedBudgetAmts(balanceDashForSecond, decSaveAmtsForSecondSeries)
                                        End If
                                        If account.ChartFormatId = 1 Then
                                            If blnShowasPercent = False Then
                                                decChartAmtsForSecondSeries(0, 0) = decSaveAmtsForSecondSeries(intSelPeriod)
                                            Else
                                                decChartAmtsForSecondSeries(0, 0) = ComputePercent(decSaveAmtsForSecondSeries(intSelPeriod), decTotalBalances(0))
                                            End If
                                        Else
                                            For intCtr = 0 To intSelPeriod
                                                decChartAmtsForSecondSeries(0, 0) += decSaveAmtsForSecondSeries(intCtr)
                                            Next
                                            If blnShowasPercent = True Then
                                                '    decTotalAccum = ComputeTotal(intSelPeriod, decTotalBalances)
                                                decChartAmtsForSecondSeries(0, 0) = ComputePercent(decChartAmtsForSecondSeries(0, 0), decTotalBalances(0))
                                            End If
                                        End If
                                        CommonProcedures.GetActualAmts(balanceDashForSecond, decSaveAmtsForSecondSeries)
                                        If account.ChartFormatId = 1 Then
                                            If blnShowasPercent = False Then
                                                decChartAmtsForSecondSeries(0, 1) = decSaveAmtsForSecondSeries(intSelPeriod)
                                            Else
                                                decChartAmtsForSecondSeries(0, 1) = ComputePercent(decSaveAmtsForSecondSeries(intSelPeriod), decTotalBalances(1))
                                            End If
                                        Else
                                            For intCtr = 0 To intSelPeriod
                                                decChartAmtsForSecondSeries(0, 1) += decSaveAmtsForSecondSeries(intCtr)
                                            Next
                                            If blnShowasPercent = True Then
                                                '   decTotalAccum = ComputeTotal(intSelPeriod, decTotalBalances)
                                                decChartAmtsForSecondSeries(0, 1) = ComputePercent(decChartAmtsForSecondSeries(0, 1), decTotalBalances(1))
                                            End If
                                        End If
                                        strXLabels(0) = "Budget"
                                        strXLabels(1) = "Actual"
                                        intPeriods = 1
                                        intLastActual = 0
                                    Case 3
                                        If intSelPeriod = 0 Then
                                            CommonProcedures.GetH5Amts(balanceDashForSecond, decSaveAmtsForSecondSeries)
                                            If blnShowasPercent = False Then
                                                decChartAmtsForSecondSeries(0, 0) = decSaveAmtsForSecondSeries(11)
                                            Else
                                                decChartAmtsForSecondSeries(0, 0) = ComputePercent(decSaveAmtsForSecondSeries(11), decTotalBalances(0))
                                            End If
                                            CommonProcedures.GetActualAmts(balanceDashForSecond, decSaveAmtsForSecondSeries)
                                            If blnShowasPercent = False Then
                                                decChartAmtsForSecondSeries(0, 1) = decSaveAmtsForSecondSeries(intSelPeriod)
                                            Else
                                                decChartAmtsForSecondSeries(0, 1) = ComputePercent(decSaveAmtsForSecondSeries(intSelPeriod), decTotalBalances(1))
                                            End If
                                        Else
                                            CommonProcedures.GetActualAmts(balanceDashForSecond, decSaveAmtsForSecondSeries)
                                            If blnShowasPercent = False Then
                                                decChartAmtsForSecondSeries(0, 0) = decSaveAmtsForSecondSeries(intSelPeriod - 1)
                                                decChartAmtsForSecondSeries(0, 1) = decSaveAmtsForSecondSeries(intSelPeriod)
                                            Else
                                                decChartAmtsForSecondSeries(0, 0) = ComputePercent(decSaveAmtsForSecondSeries(intSelPeriod - 1), decTotalBalances(0))
                                                decChartAmtsForSecondSeries(0, 1) = ComputePercent(decSaveAmtsForSecondSeries(intSelPeriod), decTotalBalances(1))
                                            End If
                                        End If
                                        strXLabels(0) = "Prior"
                                        strXLabels(1) = "Current"
                                        intPeriods = 1
                                        intLastActual = 0
                                    Case 4, 5
                                        CommonProcedures.GetH5Amts(balanceDashForSecond, decSaveAmtsForSecondSeries)
                                        If account.ChartFormatId = 4 Then
                                            decChartAmtsForSecondSeries(0, 0) = decSaveAmtsForSecondSeries(intSelPeriod)
                                        Else
                                            For intCtr = 0 To intSelPeriod
                                                decChartAmtsForSecondSeries(0, 0) += decSaveAmtsForSecondSeries(intCtr)
                                            Next
                                        End If
                                        If blnShowasPercent = True Then
                                            decChartAmtsForSecondSeries(0, 0) = ComputePercent(decChartAmtsForSecondSeries(0, 0), decTotalBalances(0))
                                        End If
                                        CommonProcedures.GetActualAmts(balanceDashForSecond, decSaveAmtsForSecondSeries)
                                        If account.ChartFormatId = 4 Then
                                            decChartAmtsForSecondSeries(0, 1) = decSaveAmtsForSecondSeries(intSelPeriod)
                                        Else
                                            For intCtr = 0 To intSelPeriod
                                                decChartAmtsForSecondSeries(0, 1) += decSaveAmtsForSecondSeries(intCtr)
                                            Next
                                        End If
                                        If blnShowasPercent = True Then
                                            decChartAmtsForSecondSeries(0, 1) = ComputePercent(decChartAmtsForSecondSeries(0, 1), decTotalBalances(1))
                                        End If
                                        strXLabels(0) = "Prior"
                                        strXLabels(1) = "Current"
                                        intPeriods = 1
                                        intLastActual = 0
                                    Case 6 'current year trend                                
                                        If blnShowBudget = False Then
                                            intPeriods = intSelPeriod
                                            GetActualAmts(balanceDashForSecond, decSaveAmtsForSecondSeries)
                                            For intCtr = 0 To intSelPeriod
                                                If blnShowasPercent = False Then
                                                    decChartAmtsForSecondSeries(0, intCtr) = decSaveAmtsForSecondSeries(intCtr)
                                                Else
                                                    If decTotalBalances(intCtr) <> 0 Then
                                                        decChartAmtsForSecondSeries(0, intCtr) = ComputePercent(decSaveAmtsForSecondSeries(intCtr), decTotalBalances(intCtr))
                                                    End If
                                                End If
                                            Next
                                        Else
                                            intPeriods = 11
                                            GetBudgetAmts(balanceDashForSecond, decSaveAmtsForSecondSeries)
                                            For intCtr = 0 To 11
                                                If blnShowasPercent = False Then
                                                    decChartAmtsForSecondSeries(0, intCtr) = decSaveAmtsForSecondSeries(intCtr)
                                                Else
                                                    If decTotalBalances(intCtr) <> 0 Then
                                                        decChartAmtsForSecondSeries(0, intCtr) = ComputePercent(decSaveAmtsForSecondSeries(intCtr), decTotalBalances(intCtr))
                                                    End If
                                                End If
                                            Next
                                            GetActualAmts(balanceDashForSecond, decSaveAmtsForSecondSeries)

                                            For intCtr = intSelPeriod To 0 Step -1
                                                If intLastActual = -1 Then
                                                    If decSaveAmts(intCtr) <> 0 Then
                                                        intLastActual = intCtr
                                                        If blnShowasPercent = False Then
                                                            decChartAmtsForSecondSeries(0, intCtr) = decSaveAmtsForSecondSeries(intCtr)
                                                        Else
                                                            If decTotalBalances(intCtr) <> 0 Then
                                                                decChartAmtsForSecondSeries(0, intCtr) = ComputePercent(decSaveAmtsForSecondSeries(intCtr), decTotalBalances(intCtr))
                                                            End If
                                                        End If
                                                    End If
                                                Else
                                                    If blnShowasPercent = False Then
                                                        decChartAmtsForSecondSeries(0, intCtr) = decSaveAmtsForSecondSeries(intCtr)
                                                    Else
                                                        If decTotalBalances(intCtr) <> 0 Then
                                                            decChartAmtsForSecondSeries(0, intCtr) = ComputePercent(decSaveAmtsForSecondSeries(intCtr), decTotalBalances(intCtr))
                                                        End If
                                                    End If
                                                End If
                                            Next
                                            If intLastActual < intSelPeriod Then
                                                intCurrentMonth = intLastActual
                                            End If
                                        End If


                                    Case 7 'last 12 months
                                        GetH5Amts(balanceDashForSecond, decSaveAmtsForSecondSeries)
                                        If intSelPeriod < 11 Then
                                            For intCtr = 0 To (11 - (intSelPeriod + 1))
                                                decChartAmtsForSecondSeries(0, intCtr) = decSaveAmtsForSecondSeries((intSelPeriod + 1) + intCtr)
                                                If blnShowasPercent = True Then
                                                    decChartAmtsForSecondSeries(0, intCtr) = ComputePercent(decChartAmtsForSecondSeries(0, intCtr), decTotalBalances(intCtr))
                                                End If
                                            Next
                                        End If
                                        GetActualAmts(balanceDashForSecond, decSaveAmtsForSecondSeries)
                                        For intCtr = 0 To intSelPeriod
                                            decChartAmtsForSecondSeries(0, (11 - intSelPeriod) + intCtr) = decSaveAmtsForSecondSeries(intCtr)
                                            If blnShowasPercent = True Then
                                                decChartAmtsForSecondSeries(0, intCtr + (11 - intSelPeriod)) = ComputePercent(decChartAmtsForSecondSeries(0, intCtr + (11 - intSelPeriod)), decTotalBalances(intCtr + (11 - intSelPeriod)))
                                            End If
                                        Next
                                        intPeriods = 11
                                    Case 8, 9  'multiyear trend                               
                                        GetH1Amts(balanceDashForSecond, decSaveAmtsForSecondSeries)
                                        StoreMultiYear(intFormat, decSaveAmtsForSecondSeries, decChartAmtsForSecondSeries, intBalCtr, intSelPeriod, blnShowasPercent, decTotalBalances, 0)
                                        GetH2Amts(balanceDashForSecond, decSaveAmtsForSecondSeries)
                                        StoreMultiYear(intFormat, decSaveAmtsForSecondSeries, decChartAmtsForSecondSeries, intBalCtr, intSelPeriod, blnShowasPercent, decTotalBalances, 1)
                                        GetH3Amts(balanceDashForSecond, decSaveAmtsForSecondSeries)
                                        StoreMultiYear(intFormat, decSaveAmtsForSecondSeries, decChartAmtsForSecondSeries, intBalCtr, intSelPeriod, blnShowasPercent, decTotalBalances, 2)
                                        GetH4Amts(balanceDashForSecond, decSaveAmtsForSecondSeries)
                                        StoreMultiYear(intFormat, decSaveAmtsForSecondSeries, decChartAmtsForSecondSeries, intBalCtr, intSelPeriod, blnShowasPercent, decTotalBalances, 3)
                                        GetH5Amts(balanceDashForSecond, decSaveAmtsForSecondSeries)
                                        StoreMultiYear(intFormat, decSaveAmtsForSecondSeries, decChartAmtsForSecondSeries, intBalCtr, intSelPeriod, blnShowasPercent, decTotalBalances, 4)
                                        GetActualAmts(balanceDashForSecond, decSaveAmtsForSecondSeries)
                                        StoreMultiYear(intFormat, decSaveAmtsForSecondSeries, decChartAmtsForSecondSeries, intBalCtr, intSelPeriod, blnShowasPercent, decTotalBalances, 5)
                                        intPeriods = intBalCtr
                                    Case 10 'rolling forcast
                                        GetBudgetAmts(balanceDashForSecond, decSaveAmtsForSecondSeries)
                                        If intSelPeriod < 11 Then
                                            For intCtr = 0 To (11 - (intSelPeriod + 1))
                                                decChartAmtsForSecondSeries(0, intCtr) = decSaveAmtsForSecondSeries((intSelPeriod + 1) + intCtr)
                                                If blnShowasPercent = True Then
                                                    decChartAmtsForSecondSeries(0, intCtr) = ComputePercent(decChartAmtsForSecondSeries(0, intCtr), decTotalBalances(intCtr))
                                                End If
                                            Next
                                        End If
                                        GetBudget2Amts(balanceDashForSecond, decSaveAmtsForSecondSeries)
                                        For intCtr = 0 To intSelPeriod
                                            decChartAmtsForSecondSeries(0, (11 - intSelPeriod) + intCtr) = decSaveAmtsForSecondSeries(intCtr)
                                            If blnShowasPercent = True Then
                                                decChartAmtsForSecondSeries(0, intCtr + (11 - intSelPeriod)) = ComputePercent(decChartAmtsForSecondSeries(0, intCtr + (11 - intSelPeriod)), decTotalBalances(intCtr + (11 - intSelPeriod)))
                                            End If
                                        Next
                                        intPeriods = 11
                                    Case 13
                                        intPeriods = 11
                                        If blnArchBudget = False Then
                                            CommonProcedures.GetBudgetAmts(balanceDashForSecond, decSaveAmtsForSecondSeries)
                                        Else
                                            CommonProcedures.GetArchivedBudgetAmts(balanceDashForSecond, decSaveAmtsForSecondSeries)
                                        End If
                                        For intCtr = 0 To intPeriods
                                            decChartAmtsForSecondSeries(0, intCtr) = decSaveAmtsForSecondSeries(intCtr)
                                            If blnShowasPercent = True Then
                                                decChartAmtsForSecondSeries(0, intCtr) = ComputePercent(decChartAmts(0, intCtr), decTotalBalances(intCtr))
                                            End If
                                        Next
                                End Select
                                Exit For
                            Next
                            Logger.Log.Info(String.Format("Dashboard FpSpread_Load - Balance For Each Execution End"))

                        End If
                        If rowCount = 2 Then
                            Dim j As Integer
                            If account.ChartFormatId = 8 Or account.ChartFormatId = 9 Then
                                intPeriods = (intPeriods - 6)
                                For j = 0 To 5
                                    decChartAmts(0, (j + 5)) = decChartAmtsForSecondSeries(0, (j + 6))
                                Next
                            Else
                                For j = 0 To 12
                                    decChartAmts(0, (j + 13)) = decChartAmtsForSecondSeries(0, j)
                                Next
                            End If

                        End If

                        'load XLables
                        Select Case account.ChartFormatId
                            Case 1, 2 'budget vs actual
                                strXLabels(0) = "Budget"
                                strXLabels(1) = "Actual"
                            Case 3
                                If intCurrentMonth = 0 Then
                                    strXLabels(0) = CommonProcedures.GetMonth(11, intFirstFiscal)
                                Else
                                    strXLabels(0) = CommonProcedures.GetMonth(intCurrentMonth - 1, intFirstFiscal)
                                End If

                                strXLabels(1) = CommonProcedures.GetMonth(intCurrentMonth, intFirstFiscal)
                            Case 4, 5
                                Dim intYear As Integer = Year(Now)
                                strXLabels(0) = CommonProcedures.GetAbbrMonth(intCurrentMonth, intFirstFiscal) & " " & Year(Now) - 1
                                strXLabels(1) = CommonProcedures.GetAbbrMonth(intCurrentMonth, intFirstFiscal) & " " & Year(Now)
                            Case 6, 13
                                If intPeriods > 6 Then
                                    For intCtr = 0 To intPeriods
                                        strXLabels(intCtr) = CommonProcedures.GetMonthFirstLetter(intCtr, intFirstFiscal)
                                    Next
                                Else
                                    For intCtr = 0 To intPeriods
                                        strXLabels(intCtr) = CommonProcedures.GetAbbrMonth(intCtr, intFirstFiscal)
                                    Next
                                End If
                            Case 7, 10
                                If intCurrentMonth < 11 Then
                                    For intCtr = 0 To (11 - (intCurrentMonth + 1))
                                        strXLabels(intCtr) = CommonProcedures.GetMonthFirstLetter(intCtr + (11 - (11 - intCurrentMonth)) + 1, intFirstFiscal)
                                    Next
                                    For intCtr = 0 To intCurrentMonth
                                        strXLabels(intCtr + (11 - intCurrentMonth)) = CommonProcedures.GetMonthFirstLetter(intCtr, intFirstFiscal)
                                    Next
                                Else
                                    For intCtr = 0 To intCurrentMonth
                                        strXLabels(intCtr) = CommonProcedures.GetMonthFirstLetter(intCtr, intFirstFiscal)
                                    Next
                                End If
                            Case 8, 9
                                For intCtr = 0 To intPeriods
                                    strXLabels(intCtr) = Year(Now) - intPeriods + intCtr
                                Next


                        End Select
                        'check amount sizes
                        If CommonProcedures.CheckBalanceSize(decChartAmts, 0, 11) = True Then
                            strChartDesc = account.DashDescription & " (in thousands)"
                        Else
                            strChartDesc = account.DashDescription

                        End If
                        If blnShowasPercent = True Then
                            Select Case account.TypeDesc.ToLower()
                                Case "assets", "liabilities and equity"                        '      
                                    strAppendLabel = " (as % of assets)"
                                Case "revenue", "expense"
                                    strAppendLabel = " (as % of revenue)"
                            End Select
                        Else
                            strAppendLabel = ""
                        End If
                    ElseIf account.ChartTypeId = 6 Or account.ChartTypeId = 7 Then
                        highchartDonut = GetDonutAndPieData(account.DashboardId, UserInfo.AnalysisId, account.ChartFormatId, account.ChartFormatTypeId, arrayLength, account.ShowAsPercent, decTotalBalances, blnArchBudget)
                        intPeriods = Session("sPeriod")
                        strChartDesc = account.DashDescription
                    End If

                    'set chart title

                    Select Case account.ChartTypeId
                        Case 2 'bar

                            highChart.HighChartTitle = strChartDesc
                            highChart.HighChartSubTitle = GetSubTitle(account.ChartFormatId, strAppendLabel, blnShowBudget)
                            highChart.HighChartType = "column"
                            highChart.intColCount = intPeriods
                            highChart.decChartAmts = decChartAmts
                            highChart.strXLabels = strXLabels
                            highChart.intChartIndex = account.Order - 1
                            highChart.intFormat = account.ChartFormatId
                            highChart.strAppendLabel = strAppendLabel
                            highChart.blnPercentLabel = blnPercentLabel
                            highChart.intLastActual = intLastActual
                            highChart.intDecPlaces = intDecPlaces
                            highChart.blnShowBudget = blnShowBudget
                            highChart.rowCounts = rowCount
                            highChart.seriesLabels = seriesLabel
                            highChart.HighChartFormatType = account.ChartFormatTypeId

                            'BuildBarChart(spread, intPeriods, decChartAmts, strXLabels, account.Order - 1, account.ChartFormatId, strAppendLabel, blnPercentLabel, intLastActual, intDecPlaces, blnShowBudget)

                        Case 4

                            highChart.HighChartTitle = strChartDesc
                            highChart.HighChartSubTitle = GetSubTitle(account.ChartFormatId, strAppendLabel, blnShowBudget)
                            highChart.HighChartType = "spline"
                            highChart.intColCount = intPeriods
                            highChart.decChartAmts = decChartAmts
                            highChart.strXLabels = strXLabels
                            highChart.intChartIndex = account.Order - 1
                            highChart.intFormat = account.ChartFormatId
                            highChart.blnShowasPerc = False
                            highChart.blnSmoothed = True
                            highChart.blnMarkers = True
                            highChart.blnShowTrend = account.ShowTrendline
                            highChart.strAppendLabel = strAppendLabel
                            highChart.blnPercentLabel = blnPercentLabel
                            highChart.intLastActual = intCurrentMonth
                            highChart.intDecPlaces = intDecPlaces
                            highChart.blnShowBudget = blnShowBudget
                            highChart.rowCounts = rowCount
                            highChart.seriesLabels = seriesLabel
                            highChart.HighChartFormatType = account.ChartFormatTypeId
                            'line 
                            'BuildLineChart(spread, intPeriods, decChartAmts, strXLabels, account.Order - 1, account.ChartFormatId, False, True, True, account.ShowTrendline, strAppendLabel, blnPercentLabel, intCurrentMonth, intDecPlaces, blnShowBudget)
                            'Private Sub BuildLineChart(ByRef spdDashboard As FpSpread, intColCount As Integer, decValues(,) As Decimal, strXLabels() As String, intChartIndex As Integer, intFormat As Integer, blnShowasPerc As Boolean, blnSmoothed As Boolean, blnMarkers As Boolean, ByVal blnShowTrend As Boolean, ByVal strAppendLabel As String, ByVal blnPercentLabel As Boolean, ByVal intCurrentPeriod As Integer, ByVal intDecPlaces As Integer, ByVal blnShowBudget As Boolean)
                        Case 5 'area

                            highChart.HighChartTitle = strChartDesc
                            highChart.HighChartSubTitle = GetSubTitle(account.ChartFormatId, strAppendLabel, blnShowBudget)
                            highChart.HighChartType = "area"
                            highChart.intColCount = intPeriods
                            highChart.decChartAmts = decChartAmts
                            highChart.strXLabels = strXLabels
                            highChart.intChartIndex = account.Order - 1
                            highChart.intFormat = account.ChartFormatId
                            highChart.strAppendLabel = strAppendLabel
                            highChart.blnPercentLabel = blnPercentLabel
                            highChart.intLastActual = intLastActual
                            highChart.intDecPlaces = intDecPlaces
                            highChart.blnShowBudget = blnShowBudget
                            highChart.rowCounts = rowCount
                            highChart.seriesLabels = seriesLabel
                            highChart.HighChartFormatType = account.ChartFormatTypeId
                        Case 6
                            highChart.HighChartTitle = strChartDesc
                            highChart.HighChartSubTitle = GetSubTitle(account.ChartFormatId, strAppendLabel, blnShowBudget)
                            highChart.HighChartType = "donut"
                            highChart.intColCount = intPeriods
                            highChart.decChartAmts = highchartDonut.decChartAmtsForDonut
                            highChart.strXLabels = strXLabels
                            highChart.intChartIndex = account.Order - 1
                            highChart.intFormat = account.ChartFormatId
                            highChart.strAppendLabel = strAppendLabel
                            highChart.blnPercentLabel = blnPercentLabel
                            highChart.intLastActual = intLastActual
                            highChart.intDecPlaces = intDecPlaces
                            highChart.blnShowBudget = blnShowBudget
                            highChart.rowCounts = rowCount
                            highChart.seriesLabels = highchartDonut.strXLabelsForDonut
                            highChart.HighChartFormatType = account.ChartFormatTypeId


                        Case 7
                            highChart.HighChartTitle = strChartDesc
                            highChart.HighChartSubTitle = GetSubTitle(account.ChartFormatId, strAppendLabel, blnShowBudget)
                            highChart.HighChartType = "pie"
                            highChart.intColCount = intPeriods
                            highChart.decChartAmts = highchartDonut.decChartAmtsForDonut
                            highChart.strXLabels = strXLabels
                            highChart.intChartIndex = account.Order - 1
                            highChart.intFormat = account.ChartFormatId
                            highChart.strAppendLabel = strAppendLabel
                            highChart.blnPercentLabel = blnPercentLabel
                            highChart.intLastActual = intLastActual
                            highChart.intDecPlaces = intDecPlaces
                            highChart.blnShowBudget = blnShowBudget
                            highChart.rowCounts = rowCount
                            highChart.seriesLabels = highchartDonut.strXLabelsForDonut
                            highChart.HighChartFormatType = account.ChartFormatTypeId

                    End Select
                    list.Add(highChart)
                Next
            End If
            Return Json(list, JsonRequestBehavior.AllowGet)
        End Function

        <AcceptVerbs(HttpVerbs.Get)> _
        <CustomActionFilter()>
        Public Function GetChartDataForSharedDashboard() As JsonResult
            'Dim decChartAmts(0, 12) As Decimal
            Dim decSaveAmts(12) As Decimal
            ' Dim strXLabels(12) As String
            Dim intCurrentMonth As Integer
            Dim intPeriods As Integer
            Dim strChartDesc As String

            Dim list As New List(Of HighChart)

            UserInfo = DirectCast(Session("UserInfo"), User)
            intAnalysisID = UserInfo.AnalysisId
            intSelPeriod = Session("sPeriod")
            intFirstFiscal = UserInfo.fiscalMonthOfSelectedCompany
            blnArchBudget = DirectCast(Session("BudgetArch"), Boolean)
            Dim arrayLength As Integer
            Dim rowCount As Integer
            'It will get the records which match with selected analysis & dashboard items selected by Logged-in user.
            'Dim dashboardItems = utility.DashboardRepository.GetDashboardDetail.Where(Function(d) d.UserId = UserInfo.UserId And d.AnalysisId = UserInfo.AnalysisId And d.IsValid = True)

            Logger.Log.Info(String.Format("Dashboard FpSpreadShared_Load- dashboardItems Query Execution Begin"))
            If UserInfo.isSharedAnalysis Then


                Dim dashboardItems = (From d In utility.DashboardRepository.GetDashboardDetail(-999, UserInfo.AnalysisId, True)
                                       Join a In utility.AccountRepository.GetAccountRecordsByAnalysisId(UserInfo.AnalysisId) On a.AccountId Equals d.AccountId And a.AnalysisId Equals d.AnalysisId
                                       Join at In utility.AccountTypeRepository.GetAccountTypeRecordsByAnalysisId(UserInfo.AnalysisId) On at.AcctTypeId Equals a.AcctTypeId And at.AnalysisId Equals a.AnalysisId Order By d.Order Ascending
                                       Select New With {d.DashboardId, d.Order, d.DashDescription, d.ChartFormatId, d.ChartTypeId, d.ChartFormatTypeId, d.ShowAsPercent, _
                                                        d.ShowTrendline, d.ShowGoal, d.ShowBudget, d.Period1Goal, d.GoalGrowthRate, d.AccountId, d.AnalysisId, at.TypeDesc, a.NumberFormat}
                                       ).ToList()

                Logger.Log.Info(String.Format("Dashboard FpSpreadShared_Load - dashboardItems Query Execution End"))

                Logger.Log.Info(String.Format("Dashboard:FpSpreadShared_Load --> Balance Query execution Starts"))
                Dim balanceDash = (From b In utility.BalanceRepository.GetBalanceRecordsByAnalysisId(intAnalysisID)
                                Join di In dashboardItems On b.AccountId Equals di.AccountId And di.AnalysisId Equals b.AnalysisId
                                Select b).ToList()

                Logger.Log.Info(String.Format("Dashboard:FpSpreadShared_Load --> Balance Query execution Starts"))

                If (Not dashboardItems Is Nothing) Then
                    For Each account In dashboardItems
                        rowCount = dashboardDetailRepository.GetNumberOfRowCount(account.DashboardId)
                        arrayLength = ((13 * rowCount) - 1)
                        Dim highChart As HighChart = New HighChart
                        Dim highchartDonut As HighChartModelForDonutAndPie = New HighChartModelForDonutAndPie
                        Dim decChartAmts(0, arrayLength) As Decimal
                        Dim strXLabels(12) As String
                        Dim accountId As Integer = account.AccountId
                        Dim analysisId As Integer = account.AnalysisId
                        Dim intFormat As Integer = account.ChartFormatId
                        Dim blnShowBudget As Boolean = account.ShowBudget
                        Dim blnShowasPercent As Boolean = account.ShowAsPercent
                        Dim blnPercentLabel As Boolean = account.ShowAsPercent
                        Dim decTotalBalances(11) As Decimal
                        Dim strYearType(5) As String
                        Dim intCtr As Integer
                        Dim intOrder As Integer = account.Order
                        Dim intBalCtr As Integer = -1
                        Dim intLastActual As Integer = -1
                        Dim strAppendLabel As String = ""
                        Dim intDecPlaces As Integer = 0
                        Dim decChartAmtsForSecondSeries(0, 12) As Decimal
                        Dim seriesLabel As New List(Of String)
                        If (arrayLength <> -1) Then
                            Array.Clear(decChartAmts, 0, arrayLength)
                        End If
                        intCurrentMonth = intSelPeriod
                        If blnShowasPercent = True Then
                            'get TypeDesc for the account used for this dashboard item
                            Select Case account.TypeDesc.ToLower()
                                Case "assets", "liabilities and equity"                        '      
                                    'get balances for account with AcctDescriptor of "ATotal"

                                    Logger.Log.Info(String.Format("Dashboard FpSpreadShared_Load - Case : assets, liabilities and equity - balanceAccum Query Execution Begin"))
                                    Dim balanceAccum = (From b In utility.BalanceRepository.GetBalanceRecordsByAnalysisId(analysisId)
                                                          Join a In utility.AccountRepository.GetAccountRecordsByAnalysisId(analysisId) On a.AccountId Equals b.AccountId And a.AnalysisId Equals b.AnalysisId
                                                          Select b).ToList()
                                    Logger.Log.Info(String.Format("Dashboard FpSpreadShared_Load - Case : assets, liabilities and equity - balanceAccum Query Execution Begin"))

                                    Logger.Log.Info(String.Format("Dashboard FpSpreadShared_Load - Case : assets, liabilities and equity - balanceAccum For Each Execution Begin"))
                                    For Each balance As Balance In (From b In balanceAccum)

                                        StoreTotalBal(balance, intFormat, decTotalBalances, intSelPeriod, blnShowBudget)
                                        Exit For
                                    Next
                                    Logger.Log.Info(String.Format("Dashboard FpSpreadShared_Load - Case : assets, liabilities and equity - balanceAccum For Each Execution End"))
                                Case "revenue", "expense"

                                    Logger.Log.Info(String.Format("Dashboard FpSpreadShared_Load - Case : revenue, expense - balanceAccum Query Execution Begin"))
                                    Dim balanceAccum = (From b In utility.BalanceRepository.GetBalanceRecordsByAnalysisId(analysisId)
                                                         Join a In utility.AccountRepository.GetAccountRecordsByAnalysisId(analysisId) On a.AccountId Equals b.AccountId And a.AnalysisId Equals b.AnalysisId
                                                         Join at In utility.AccountTypeRepository.GetAccountTypeRecordsByAnalysisId(analysisId, New String() {"revenue"}) On at.AcctTypeId Equals a.AcctTypeId And at.AnalysisId Equals a.AnalysisId
                                                         Select b).ToList()
                                    Logger.Log.Info(String.Format("Dashboard FpSpreadShared_Load - Case : revenue, expense - balanceAccum Query Execution Begin"))

                                    Dim count As Integer = balanceAccum.Count
                                    Logger.Log.Info(String.Format("Dashboard FpSpreadShared_Load - Case : revenue, expense - balanceAccum For Each Execution Begin"))
                                    For Each balance As Balance In (From b In balanceAccum)

                                        StoreTotalBal(balance, intFormat, decTotalBalances, intSelPeriod, blnShowBudget)
                                    Next
                                    Logger.Log.Info(String.Format("Dashboard FpSpreadShared_Load - Case : revenue, expense - balanceAccum For Each Execution End"))
                                Case Else
                                    blnShowasPercent = False
                            End Select
                        End If
                        Logger.Log.Info(String.Format("Dashboard FpSpreadShared_Load - Balance For Each Execution Begin"))
                        If account.ChartTypeId = 2 Or account.ChartTypeId = 4 Or account.ChartTypeId = 5 Then
                            'It will get the record from balance of selected analysis and account item selected by user.
                            For Each balance As Balance In (From b In balanceDash.Where(Function(a) a.AccountId = accountId And a.AnalysisId = analysisId) Select b)
                                If account.NumberFormat = 99 Then
                                    blnPercentLabel = True
                                Else
                                    If account.TypeDesc.ToLower() = "cashflow" Then
                                        intDecPlaces = 0
                                    Else
                                        intDecPlaces = account.NumberFormat
                                    End If
                                End If
                                Select Case account.ChartFormatId
                                    Case 1, 2
                                        If blnArchBudget = False Then
                                            CommonProcedures.GetBudgetAmts(balance, decSaveAmts)
                                        Else
                                            CommonProcedures.GetArchivedBudgetAmts(balance, decSaveAmts)
                                        End If

                                        If account.ChartFormatId = 1 Then
                                            If blnShowasPercent = False Then
                                                decChartAmts(0, 0) = decSaveAmts(intSelPeriod)
                                            Else
                                                decChartAmts(0, 0) = ComputePercent(decSaveAmts(intSelPeriod), decTotalBalances(0))
                                            End If
                                        Else
                                            For intCtr = 0 To intSelPeriod
                                                decChartAmts(0, 0) += decSaveAmts(intCtr)
                                            Next
                                            If blnShowasPercent = True Then
                                                '    decTotalAccum = ComputeTotal(intSelPeriod, decTotalBalances)
                                                decChartAmts(0, 0) = ComputePercent(decChartAmts(0, 0), decTotalBalances(0))
                                            End If
                                        End If
                                        CommonProcedures.GetActualAmts(balance, decSaveAmts)
                                        If account.ChartFormatId = 1 Then
                                            If blnShowasPercent = False Then
                                                decChartAmts(0, 1) = decSaveAmts(intSelPeriod)
                                            Else
                                                decChartAmts(0, 1) = ComputePercent(decSaveAmts(intSelPeriod), decTotalBalances(1))
                                            End If
                                        Else
                                            For intCtr = 0 To intSelPeriod
                                                decChartAmts(0, 1) += decSaveAmts(intCtr)
                                            Next
                                            If blnShowasPercent = True Then
                                                '   decTotalAccum = ComputeTotal(intSelPeriod, decTotalBalances)
                                                decChartAmts(0, 1) = ComputePercent(decChartAmts(0, 1), decTotalBalances(1))
                                            End If
                                        End If
                                        strXLabels(0) = "Budget"
                                        strXLabels(1) = "Actual"
                                        intPeriods = 1
                                        intLastActual = 0
                                    Case 3
                                        If intSelPeriod = 0 Then
                                            CommonProcedures.GetH5Amts(balance, decSaveAmts)
                                            If blnShowasPercent = False Then
                                                decChartAmts(0, 0) = decSaveAmts(11)
                                            Else
                                                decChartAmts(0, 0) = ComputePercent(decSaveAmts(11), decTotalBalances(0))
                                            End If
                                            CommonProcedures.GetActualAmts(balance, decSaveAmts)
                                            If blnShowasPercent = False Then
                                                decChartAmts(0, 1) = decSaveAmts(intSelPeriod)
                                            Else
                                                decChartAmts(0, 1) = ComputePercent(decSaveAmts(intSelPeriod), decTotalBalances(1))
                                            End If
                                        Else
                                            CommonProcedures.GetActualAmts(balance, decSaveAmts)
                                            If blnShowasPercent = False Then
                                                decChartAmts(0, 0) = decSaveAmts(intSelPeriod - 1)
                                                decChartAmts(0, 1) = decSaveAmts(intSelPeriod)
                                            Else
                                                decChartAmts(0, 0) = ComputePercent(decSaveAmts(intSelPeriod - 1), decTotalBalances(0))
                                                decChartAmts(0, 1) = ComputePercent(decSaveAmts(intSelPeriod), decTotalBalances(1))
                                            End If
                                        End If
                                        strXLabels(0) = "Prior"
                                        strXLabels(1) = "Current"
                                        intPeriods = 1
                                        intLastActual = 0
                                    Case 4, 5
                                        CommonProcedures.GetH5Amts(balance, decSaveAmts)
                                        If account.ChartFormatId = 4 Then
                                            decChartAmts(0, 0) = decSaveAmts(intSelPeriod)
                                        Else
                                            For intCtr = 0 To intSelPeriod
                                                decChartAmts(0, 0) += decSaveAmts(intCtr)
                                            Next
                                        End If
                                        If blnShowasPercent = True Then
                                            decChartAmts(0, 0) = ComputePercent(decChartAmts(0, 0), decTotalBalances(0))
                                        End If
                                        CommonProcedures.GetActualAmts(balance, decSaveAmts)
                                        If account.ChartFormatId = 4 Then
                                            decChartAmts(0, 1) = decSaveAmts(intSelPeriod)
                                        Else
                                            For intCtr = 0 To intSelPeriod
                                                decChartAmts(0, 1) += decSaveAmts(intCtr)
                                            Next
                                        End If
                                        If blnShowasPercent = True Then
                                            decChartAmts(0, 1) = ComputePercent(decChartAmts(0, 1), decTotalBalances(1))
                                        End If
                                        strXLabels(0) = "Prior"
                                        strXLabels(1) = "Current"
                                        intPeriods = 1
                                        intLastActual = 0
                                    Case 6 'current year trend                                
                                        If blnShowBudget = False Then
                                            intPeriods = intSelPeriod
                                            GetActualAmts(balance, decSaveAmts)
                                            For intCtr = 0 To intSelPeriod
                                                If blnShowasPercent = False Then
                                                    decChartAmts(0, intCtr) = decSaveAmts(intCtr)
                                                Else
                                                    If decTotalBalances(intCtr) <> 0 Then
                                                        decChartAmts(0, intCtr) = ComputePercent(decSaveAmts(intCtr), decTotalBalances(intCtr))
                                                    End If
                                                End If
                                            Next
                                        Else
                                            intPeriods = 11
                                            GetBudgetAmts(balance, decSaveAmts)
                                            For intCtr = 0 To 11
                                                If blnShowasPercent = False Then
                                                    decChartAmts(0, intCtr) = decSaveAmts(intCtr)
                                                Else
                                                    If decTotalBalances(intCtr) <> 0 Then
                                                        decChartAmts(0, intCtr) = ComputePercent(decSaveAmts(intCtr), decTotalBalances(intCtr))
                                                    End If
                                                End If
                                            Next
                                            GetActualAmts(balance, decSaveAmts)

                                            For intCtr = intSelPeriod To 0 Step -1
                                                If intLastActual = -1 Then
                                                    If decSaveAmts(intCtr) <> 0 Then
                                                        intLastActual = intCtr
                                                        If blnShowasPercent = False Then
                                                            decChartAmts(0, intCtr) = decSaveAmts(intCtr)
                                                        Else
                                                            If decTotalBalances(intCtr) <> 0 Then
                                                                decChartAmts(0, intCtr) = ComputePercent(decSaveAmts(intCtr), decTotalBalances(intCtr))
                                                            End If
                                                        End If
                                                    End If
                                                Else
                                                    If blnShowasPercent = False Then
                                                        decChartAmts(0, intCtr) = decSaveAmts(intCtr)
                                                    Else
                                                        If decTotalBalances(intCtr) <> 0 Then
                                                            decChartAmts(0, intCtr) = ComputePercent(decSaveAmts(intCtr), decTotalBalances(intCtr))
                                                        End If
                                                    End If
                                                End If
                                            Next
                                            If intLastActual < intSelPeriod Then
                                                intCurrentMonth = intLastActual
                                            End If
                                        End If
                                    Case 7 'last 12 months
                                        GetH5Amts(balance, decSaveAmts)
                                        If intSelPeriod < 11 Then
                                            For intCtr = 0 To (11 - (intSelPeriod + 1))
                                                decChartAmts(0, intCtr) = decSaveAmts((intSelPeriod + 1) + intCtr)
                                                If blnShowasPercent = True Then
                                                    decChartAmts(0, intCtr) = ComputePercent(decChartAmts(0, intCtr), decTotalBalances(intCtr))
                                                End If
                                            Next
                                        End If
                                        GetActualAmts(balance, decSaveAmts)
                                        For intCtr = 0 To intSelPeriod
                                            decChartAmts(0, (11 - intSelPeriod) + intCtr) = decSaveAmts(intCtr)
                                            If blnShowasPercent = True Then
                                                decChartAmts(0, intCtr + (11 - intSelPeriod)) = ComputePercent(decChartAmts(0, intCtr + (11 - intSelPeriod)), decTotalBalances(intCtr + (11 - intSelPeriod)))
                                            End If
                                        Next
                                        intPeriods = 11
                                    Case 8, 9  'multiyear trend                               
                                        GetH1Amts(balance, decSaveAmts)
                                        StoreMultiYear(intFormat, decSaveAmts, decChartAmts, intBalCtr, intSelPeriod, blnShowasPercent, decTotalBalances, 0)
                                        GetH2Amts(balance, decSaveAmts)
                                        StoreMultiYear(intFormat, decSaveAmts, decChartAmts, intBalCtr, intSelPeriod, blnShowasPercent, decTotalBalances, 1)
                                        GetH3Amts(balance, decSaveAmts)
                                        StoreMultiYear(intFormat, decSaveAmts, decChartAmts, intBalCtr, intSelPeriod, blnShowasPercent, decTotalBalances, 2)
                                        GetH4Amts(balance, decSaveAmts)
                                        StoreMultiYear(intFormat, decSaveAmts, decChartAmts, intBalCtr, intSelPeriod, blnShowasPercent, decTotalBalances, 3)
                                        GetH5Amts(balance, decSaveAmts)
                                        StoreMultiYear(intFormat, decSaveAmts, decChartAmts, intBalCtr, intSelPeriod, blnShowasPercent, decTotalBalances, 4)
                                        GetActualAmts(balance, decSaveAmts)
                                        StoreMultiYear(intFormat, decSaveAmts, decChartAmts, intBalCtr, intSelPeriod, blnShowasPercent, decTotalBalances, 5)
                                        intPeriods = intBalCtr
                                    Case 10 'rolling forcast
                                        GetBudgetAmts(balance, decSaveAmts)
                                        If intSelPeriod < 11 Then
                                            For intCtr = 0 To (11 - (intSelPeriod + 1))
                                                decChartAmts(0, intCtr) = decSaveAmts((intSelPeriod + 1) + intCtr)
                                                If blnShowasPercent = True Then
                                                    decChartAmts(0, intCtr) = ComputePercent(decChartAmts(0, intCtr), decTotalBalances(intCtr))
                                                End If
                                            Next
                                        End If
                                        GetBudget2Amts(balance, decSaveAmts)
                                        For intCtr = 0 To intSelPeriod
                                            decChartAmts(0, (11 - intSelPeriod) + intCtr) = decSaveAmts(intCtr)
                                            If blnShowasPercent = True Then
                                                decChartAmts(0, intCtr + (11 - intSelPeriod)) = ComputePercent(decChartAmts(0, intCtr + (11 - intSelPeriod)), decTotalBalances(intCtr + (11 - intSelPeriod)))
                                            End If
                                        Next
                                        intPeriods = 11
                                    Case 13
                                        intPeriods = 11
                                        If blnArchBudget = False Then
                                            CommonProcedures.GetBudgetAmts(balance, decSaveAmts)
                                        Else
                                            CommonProcedures.GetArchivedBudgetAmts(balance, decSaveAmts)
                                        End If
                                        For intCtr = 0 To intPeriods
                                            decChartAmts(0, intCtr) = decSaveAmts(intCtr)
                                            If blnShowasPercent = True Then
                                                decChartAmts(0, intCtr) = ComputePercent(decChartAmts(0, intCtr), decTotalBalances(intCtr))
                                            End If
                                        Next
                                End Select
                                Exit For
                            Next
                            Logger.Log.Info(String.Format("Dashboard FpSpreadShared_Load - Balance For Each Execution End"))
                            If rowCount = 2 Then
                                intLastActual = -1
                                intAnalysisID = UserInfo.AnalysisId
                                intSelPeriod = Session("sPeriod")
                                intFirstFiscal = UserInfo.fiscalMonthOfSelectedCompany
                                blnArchBudget = DirectCast(Session("BudgetArch"), Boolean)

                                Dim FirstSeriesTitle = dashboardDetailRepository.GetSeriesLabel(account.DashboardId, account.AccountId)
                                seriesLabel.Add(FirstSeriesTitle)
                                Dim decSaveAmtsForSecondSeries(12) As Decimal

                                Array.Clear(decChartAmtsForSecondSeries, 0, 12)
                                Array.Clear(decSaveAmtsForSecondSeries, 0, 12)
                                Dim AccountIdForSecondSeries = dashboardDetailRepository.GetSecondSeriesAccountId(account.DashboardId, account.AccountId)
                                Dim balanceDashForSecondSeries = (From b In utility.BalanceRepository.GetBalanceRecordsByAnalysisId(intAnalysisID)
                                                                  Where b.AccountId = AccountIdForSecondSeries.AccountId And b.AnalysisId = account.AnalysisId Select b).ToList()
                                seriesLabel.Add(AccountIdForSecondSeries.DashboardDescription)
                                For Each balanceDashForSecond As Balance In balanceDashForSecondSeries
                                    If account.NumberFormat = 99 Then
                                        blnPercentLabel = True
                                    Else
                                        If account.TypeDesc.ToLower() = "cashflow" Then
                                            intDecPlaces = 0
                                        Else
                                            intDecPlaces = account.NumberFormat
                                        End If
                                    End If
                                    Select Case account.ChartFormatId
                                        Case 1, 2
                                            If blnArchBudget = False Then
                                                CommonProcedures.GetBudgetAmts(balanceDashForSecond, decSaveAmtsForSecondSeries)
                                            Else
                                                CommonProcedures.GetArchivedBudgetAmts(balanceDashForSecond, decSaveAmtsForSecondSeries)
                                            End If
                                            If account.ChartFormatId = 1 Then
                                                If blnShowasPercent = False Then
                                                    decChartAmtsForSecondSeries(0, 0) = decSaveAmtsForSecondSeries(intSelPeriod)
                                                Else
                                                    decChartAmtsForSecondSeries(0, 0) = ComputePercent(decSaveAmtsForSecondSeries(intSelPeriod), decTotalBalances(0))
                                                End If
                                            Else
                                                For intCtr = 0 To intSelPeriod
                                                    decChartAmtsForSecondSeries(0, 0) += decSaveAmtsForSecondSeries(intCtr)
                                                Next
                                                If blnShowasPercent = True Then
                                                    '    decTotalAccum = ComputeTotal(intSelPeriod, decTotalBalances)
                                                    decChartAmtsForSecondSeries(0, 0) = ComputePercent(decChartAmtsForSecondSeries(0, 0), decTotalBalances(0))
                                                End If
                                            End If
                                            CommonProcedures.GetActualAmts(balanceDashForSecond, decSaveAmtsForSecondSeries)
                                            If account.ChartFormatId = 1 Then
                                                If blnShowasPercent = False Then
                                                    decChartAmtsForSecondSeries(0, 1) = decSaveAmtsForSecondSeries(intSelPeriod)
                                                Else
                                                    decChartAmtsForSecondSeries(0, 1) = ComputePercent(decSaveAmtsForSecondSeries(intSelPeriod), decTotalBalances(1))
                                                End If
                                            Else
                                                For intCtr = 0 To intSelPeriod
                                                    decChartAmtsForSecondSeries(0, 1) += decSaveAmtsForSecondSeries(intCtr)
                                                Next
                                                If blnShowasPercent = True Then
                                                    '   decTotalAccum = ComputeTotal(intSelPeriod, decTotalBalances)
                                                    decChartAmtsForSecondSeries(0, 1) = ComputePercent(decChartAmtsForSecondSeries(0, 1), decTotalBalances(1))
                                                End If
                                            End If
                                            strXLabels(0) = "Budget"
                                            strXLabels(1) = "Actual"
                                            intPeriods = 1
                                            intLastActual = 0
                                        Case 3
                                            If intSelPeriod = 0 Then
                                                CommonProcedures.GetH5Amts(balanceDashForSecond, decSaveAmtsForSecondSeries)
                                                If blnShowasPercent = False Then
                                                    decChartAmtsForSecondSeries(0, 0) = decSaveAmtsForSecondSeries(11)
                                                Else
                                                    decChartAmtsForSecondSeries(0, 0) = ComputePercent(decSaveAmtsForSecondSeries(11), decTotalBalances(0))
                                                End If
                                                CommonProcedures.GetActualAmts(balanceDashForSecond, decSaveAmtsForSecondSeries)
                                                If blnShowasPercent = False Then
                                                    decChartAmtsForSecondSeries(0, 1) = decSaveAmtsForSecondSeries(intSelPeriod)
                                                Else
                                                    decChartAmtsForSecondSeries(0, 1) = ComputePercent(decSaveAmtsForSecondSeries(intSelPeriod), decTotalBalances(1))
                                                End If
                                            Else
                                                CommonProcedures.GetActualAmts(balanceDashForSecond, decSaveAmtsForSecondSeries)
                                                If blnShowasPercent = False Then
                                                    decChartAmtsForSecondSeries(0, 0) = decSaveAmtsForSecondSeries(intSelPeriod - 1)
                                                    decChartAmtsForSecondSeries(0, 1) = decSaveAmtsForSecondSeries(intSelPeriod)
                                                Else
                                                    decChartAmtsForSecondSeries(0, 0) = ComputePercent(decSaveAmtsForSecondSeries(intSelPeriod - 1), decTotalBalances(0))
                                                    decChartAmtsForSecondSeries(0, 1) = ComputePercent(decSaveAmtsForSecondSeries(intSelPeriod), decTotalBalances(1))
                                                End If
                                            End If
                                            strXLabels(0) = "Prior"
                                            strXLabels(1) = "Current"
                                            intPeriods = 1
                                            intLastActual = 0
                                        Case 4, 5
                                            CommonProcedures.GetH5Amts(balanceDashForSecond, decSaveAmtsForSecondSeries)
                                            If account.ChartFormatId = 4 Then
                                                decChartAmtsForSecondSeries(0, 0) = decSaveAmtsForSecondSeries(intSelPeriod)
                                            Else
                                                For intCtr = 0 To intSelPeriod
                                                    decChartAmtsForSecondSeries(0, 0) += decSaveAmtsForSecondSeries(intCtr)
                                                Next
                                            End If
                                            If blnShowasPercent = True Then
                                                decChartAmtsForSecondSeries(0, 0) = ComputePercent(decChartAmtsForSecondSeries(0, 0), decTotalBalances(0))
                                            End If
                                            CommonProcedures.GetActualAmts(balanceDashForSecond, decSaveAmtsForSecondSeries)
                                            If account.ChartFormatId = 4 Then
                                                decChartAmtsForSecondSeries(0, 1) = decSaveAmtsForSecondSeries(intSelPeriod)
                                            Else
                                                For intCtr = 0 To intSelPeriod
                                                    decChartAmtsForSecondSeries(0, 1) += decSaveAmtsForSecondSeries(intCtr)
                                                Next
                                            End If
                                            If blnShowasPercent = True Then
                                                decChartAmtsForSecondSeries(0, 1) = ComputePercent(decChartAmtsForSecondSeries(0, 1), decTotalBalances(1))
                                            End If
                                            strXLabels(0) = "Prior"
                                            strXLabels(1) = "Current"
                                            intPeriods = 1
                                            intLastActual = 0
                                        Case 6 'current year trend                                

                                            If blnShowBudget = False Then
                                                intPeriods = intSelPeriod
                                                GetActualAmts(balanceDashForSecond, decSaveAmtsForSecondSeries)
                                                For intCtr = 0 To intSelPeriod
                                                    If blnShowasPercent = False Then
                                                        decChartAmtsForSecondSeries(0, intCtr) = decSaveAmtsForSecondSeries(intCtr)
                                                    Else
                                                        If decTotalBalances(intCtr) <> 0 Then
                                                            decChartAmtsForSecondSeries(0, intCtr) = ComputePercent(decSaveAmtsForSecondSeries(intCtr), decTotalBalances(intCtr))
                                                        End If
                                                    End If
                                                Next
                                            Else
                                                intPeriods = 11
                                                GetBudgetAmts(balanceDashForSecond, decSaveAmtsForSecondSeries)
                                                For intCtr = 0 To 11
                                                    If blnShowasPercent = False Then
                                                        decChartAmtsForSecondSeries(0, intCtr) = decSaveAmtsForSecondSeries(intCtr)
                                                    Else
                                                        If decTotalBalances(intCtr) <> 0 Then
                                                            decChartAmtsForSecondSeries(0, intCtr) = ComputePercent(decSaveAmtsForSecondSeries(intCtr), decTotalBalances(intCtr))
                                                        End If
                                                    End If
                                                Next
                                                GetActualAmts(balanceDashForSecond, decSaveAmtsForSecondSeries)

                                                For intCtr = intSelPeriod To 0 Step -1
                                                    If intLastActual = -1 Then
                                                        If decSaveAmts(intCtr) <> 0 Then
                                                            intLastActual = intCtr
                                                            If blnShowasPercent = False Then
                                                                decChartAmtsForSecondSeries(0, intCtr) = decSaveAmtsForSecondSeries(intCtr)
                                                            Else
                                                                If decTotalBalances(intCtr) <> 0 Then
                                                                    decChartAmtsForSecondSeries(0, intCtr) = ComputePercent(decSaveAmtsForSecondSeries(intCtr), decTotalBalances(intCtr))
                                                                End If
                                                            End If
                                                        End If
                                                    Else
                                                        If blnShowasPercent = False Then
                                                            decChartAmtsForSecondSeries(0, intCtr) = decSaveAmtsForSecondSeries(intCtr)
                                                        Else
                                                            If decTotalBalances(intCtr) <> 0 Then
                                                                decChartAmtsForSecondSeries(0, intCtr) = ComputePercent(decSaveAmtsForSecondSeries(intCtr), decTotalBalances(intCtr))
                                                            End If
                                                        End If
                                                    End If
                                                Next
                                                If intLastActual < intSelPeriod Then
                                                    intCurrentMonth = intLastActual
                                                End If
                                            End If


                                        Case 7 'last 12 months
                                            GetH5Amts(balanceDashForSecond, decSaveAmtsForSecondSeries)
                                            If intSelPeriod < 11 Then
                                                For intCtr = 0 To (11 - (intSelPeriod + 1))
                                                    decChartAmtsForSecondSeries(0, intCtr) = decSaveAmtsForSecondSeries((intSelPeriod + 1) + intCtr)
                                                    If blnShowasPercent = True Then
                                                        decChartAmtsForSecondSeries(0, intCtr) = ComputePercent(decChartAmtsForSecondSeries(0, intCtr), decTotalBalances(intCtr))
                                                    End If
                                                Next
                                            End If
                                            GetActualAmts(balanceDashForSecond, decSaveAmtsForSecondSeries)
                                            For intCtr = 0 To intSelPeriod
                                                decChartAmtsForSecondSeries(0, (11 - intSelPeriod) + intCtr) = decSaveAmtsForSecondSeries(intCtr)
                                                If blnShowasPercent = True Then
                                                    decChartAmtsForSecondSeries(0, intCtr + (11 - intSelPeriod)) = ComputePercent(decChartAmtsForSecondSeries(0, intCtr + (11 - intSelPeriod)), decTotalBalances(intCtr + (11 - intSelPeriod)))
                                                End If
                                            Next
                                            intPeriods = 11
                                        Case 8, 9  'multiyear trend                               
                                            GetH1Amts(balanceDashForSecond, decSaveAmtsForSecondSeries)
                                            StoreMultiYear(intFormat, decSaveAmtsForSecondSeries, decChartAmtsForSecondSeries, intBalCtr, intSelPeriod, blnShowasPercent, decTotalBalances, 0)
                                            GetH2Amts(balanceDashForSecond, decSaveAmtsForSecondSeries)
                                            StoreMultiYear(intFormat, decSaveAmtsForSecondSeries, decChartAmtsForSecondSeries, intBalCtr, intSelPeriod, blnShowasPercent, decTotalBalances, 1)
                                            GetH3Amts(balanceDashForSecond, decSaveAmtsForSecondSeries)
                                            StoreMultiYear(intFormat, decSaveAmtsForSecondSeries, decChartAmtsForSecondSeries, intBalCtr, intSelPeriod, blnShowasPercent, decTotalBalances, 2)
                                            GetH4Amts(balanceDashForSecond, decSaveAmtsForSecondSeries)
                                            StoreMultiYear(intFormat, decSaveAmtsForSecondSeries, decChartAmtsForSecondSeries, intBalCtr, intSelPeriod, blnShowasPercent, decTotalBalances, 3)
                                            GetH5Amts(balanceDashForSecond, decSaveAmtsForSecondSeries)
                                            StoreMultiYear(intFormat, decSaveAmtsForSecondSeries, decChartAmtsForSecondSeries, intBalCtr, intSelPeriod, blnShowasPercent, decTotalBalances, 4)
                                            GetActualAmts(balanceDashForSecond, decSaveAmtsForSecondSeries)
                                            StoreMultiYear(intFormat, decSaveAmtsForSecondSeries, decChartAmtsForSecondSeries, intBalCtr, intSelPeriod, blnShowasPercent, decTotalBalances, 5)
                                            intPeriods = intBalCtr
                                        Case 10 'rolling forcast
                                            GetBudgetAmts(balanceDashForSecond, decSaveAmtsForSecondSeries)
                                            If intSelPeriod < 11 Then
                                                For intCtr = 0 To (11 - (intSelPeriod + 1))
                                                    decChartAmtsForSecondSeries(0, intCtr) = decSaveAmtsForSecondSeries((intSelPeriod + 1) + intCtr)
                                                    If blnShowasPercent = True Then
                                                        decChartAmtsForSecondSeries(0, intCtr) = ComputePercent(decChartAmtsForSecondSeries(0, intCtr), decTotalBalances(intCtr))
                                                    End If
                                                Next
                                            End If
                                            GetBudget2Amts(balanceDashForSecond, decSaveAmtsForSecondSeries)
                                            For intCtr = 0 To intSelPeriod
                                                decChartAmtsForSecondSeries(0, (11 - intSelPeriod) + intCtr) = decSaveAmtsForSecondSeries(intCtr)
                                                If blnShowasPercent = True Then
                                                    decChartAmtsForSecondSeries(0, intCtr + (11 - intSelPeriod)) = ComputePercent(decChartAmtsForSecondSeries(0, intCtr + (11 - intSelPeriod)), decTotalBalances(intCtr + (11 - intSelPeriod)))
                                                End If
                                            Next
                                            intPeriods = 11
                                        Case 13
                                            intPeriods = 11
                                            If blnArchBudget = False Then
                                                CommonProcedures.GetBudgetAmts(balanceDashForSecond, decSaveAmtsForSecondSeries)
                                            Else
                                                CommonProcedures.GetArchivedBudgetAmts(balanceDashForSecond, decSaveAmtsForSecondSeries)
                                            End If
                                            For intCtr = 0 To intPeriods
                                                decChartAmtsForSecondSeries(0, intCtr) = decSaveAmtsForSecondSeries(intCtr)
                                                If blnShowasPercent = True Then
                                                    decChartAmtsForSecondSeries(0, intCtr) = ComputePercent(decChartAmtsForSecondSeries(0, intCtr), decTotalBalances(intCtr))
                                                End If
                                            Next
                                    End Select
                                    Exit For
                                Next
                                Logger.Log.Info(String.Format("Dashboard FpSpread_Load - Balance For Each Execution End"))

                            End If
                            If rowCount = 2 Then
                                Dim j As Integer
                                If account.ChartFormatId = 8 Or account.ChartFormatId = 9 Then
                                    intPeriods = (intPeriods - 6)
                                    For j = 0 To 5
                                        decChartAmts(0, (j + 5)) = decChartAmtsForSecondSeries(0, (j + 6))
                                    Next
                                Else
                                    For j = 0 To 12
                                        decChartAmts(0, (j + 13)) = decChartAmtsForSecondSeries(0, j)
                                    Next
                                End If
                            End If

                            'load XLables
                            Select Case account.ChartFormatId
                                Case 1, 2 'budget vs actual
                                    strXLabels(0) = "Budget"
                                    strXLabels(1) = "Actual"
                                Case 3
                                    If intCurrentMonth = 0 Then
                                        strXLabels(0) = CommonProcedures.GetMonth(11, intFirstFiscal)
                                    Else
                                        strXLabels(0) = CommonProcedures.GetMonth(intCurrentMonth - 1, intFirstFiscal)
                                    End If

                                    strXLabels(1) = CommonProcedures.GetMonth(intCurrentMonth, intFirstFiscal)
                                Case 4, 5
                                    Dim intYear As Integer = Year(Now)
                                    strXLabels(0) = CommonProcedures.GetAbbrMonth(intCurrentMonth, intFirstFiscal) & " " & Year(Now) - 1
                                    strXLabels(1) = CommonProcedures.GetAbbrMonth(intCurrentMonth, intFirstFiscal) & " " & Year(Now)
                                Case 6, 13
                                    If intPeriods > 6 Then
                                        For intCtr = 0 To intPeriods
                                            strXLabels(intCtr) = CommonProcedures.GetMonthFirstLetter(intCtr, intFirstFiscal)
                                        Next
                                    Else
                                        For intCtr = 0 To intPeriods
                                            strXLabels(intCtr) = CommonProcedures.GetAbbrMonth(intCtr, intFirstFiscal)
                                        Next
                                    End If
                                Case 7, 10
                                    If intCurrentMonth < 11 Then
                                        For intCtr = 0 To (11 - (intCurrentMonth + 1))
                                            strXLabels(intCtr) = CommonProcedures.GetMonthFirstLetter(intCtr + (11 - (11 - intCurrentMonth)) + 1, intFirstFiscal)
                                        Next
                                        For intCtr = 0 To intCurrentMonth
                                            strXLabels(intCtr + (11 - intCurrentMonth)) = CommonProcedures.GetMonthFirstLetter(intCtr, intFirstFiscal)
                                        Next
                                    Else
                                        For intCtr = 0 To intCurrentMonth
                                            strXLabels(intCtr) = CommonProcedures.GetMonthFirstLetter(intCtr, intFirstFiscal)
                                        Next
                                    End If
                                Case 8, 9
                                    For intCtr = 0 To intPeriods
                                        strXLabels(intCtr) = Year(Now) - intPeriods + intCtr
                                    Next
                            End Select
                            'check amount sizes
                            If CommonProcedures.CheckBalanceSize(decChartAmts, 0, 11) = True Then
                                strChartDesc = account.DashDescription & " (in thousands)"
                            Else
                                strChartDesc = account.DashDescription

                            End If
                            If blnShowasPercent = True Then
                                Select Case account.TypeDesc.ToLower()
                                    Case "assets", "liabilities and equity"                        '      
                                        strAppendLabel = " (as % of assets)"
                                    Case "revenue", "expense"
                                        strAppendLabel = " (as % of revenue)"
                                End Select
                            Else
                                strAppendLabel = ""
                            End If
                        ElseIf account.ChartTypeId = 6 Or account.ChartTypeId = 7 Then
                            highchartDonut = GetDonutAndPieData(account.DashboardId, UserInfo.AnalysisId, account.ChartFormatId, account.ChartFormatTypeId, arrayLength, account.ShowAsPercent, decTotalBalances, blnArchBudget)
                            intPeriods = Session("sPeriod")
                            strChartDesc = account.DashDescription
                        End If
                        'set chart title
                        'SetChartTitle(spread, account.Order, strChartDesc, account.ChartFormatId, account.ShowAsPercent)
                        Select Case account.ChartTypeId
                            Case 2
                                highChart.HighChartTitle = strChartDesc
                                highChart.HighChartSubTitle = GetSubTitle(account.ChartFormatId, strAppendLabel, blnShowBudget)
                                highChart.HighChartType = "column"
                                highChart.intColCount = intPeriods
                                highChart.decChartAmts = decChartAmts
                                highChart.strXLabels = strXLabels
                                highChart.intChartIndex = account.Order - 1
                                highChart.intFormat = account.ChartFormatId
                                highChart.strAppendLabel = strAppendLabel
                                highChart.blnPercentLabel = blnPercentLabel
                                highChart.intLastActual = intLastActual
                                highChart.intDecPlaces = intDecPlaces
                                highChart.blnShowBudget = blnShowBudget
                                highChart.rowCounts = rowCount
                                highChart.seriesLabels = seriesLabel
                                highChart.HighChartFormatType = account.ChartFormatTypeId
                                'bar
                                'BuildBarChart(spread, intPeriods, decChartAmts, strXLabels, account.Order - 1, account.ChartFormatId, strAppendLabel, blnPercentLabel, intLastActual, intDecPlaces, blnShowBudget)
                            Case 4
                                highChart.HighChartTitle = strChartDesc
                                highChart.HighChartSubTitle = GetSubTitle(account.ChartFormatId, strAppendLabel, blnShowBudget)
                                highChart.HighChartType = "spline"
                                highChart.intColCount = intPeriods
                                highChart.decChartAmts = decChartAmts
                                highChart.strXLabels = strXLabels
                                highChart.intChartIndex = account.Order - 1
                                highChart.intFormat = account.ChartFormatId
                                highChart.blnShowasPerc = False
                                highChart.blnSmoothed = True
                                highChart.blnMarkers = True
                                highChart.blnShowTrend = account.ShowTrendline
                                highChart.strAppendLabel = strAppendLabel
                                highChart.blnPercentLabel = blnPercentLabel
                                highChart.intLastActual = intCurrentMonth
                                highChart.intDecPlaces = intDecPlaces
                                highChart.blnShowBudget = blnShowBudget
                                highChart.rowCounts = rowCount
                                highChart.seriesLabels = seriesLabel
                                highChart.HighChartFormatType = account.ChartFormatTypeId
                                'line 
                                'BuildLineChart(spread, intPeriods, decChartAmts, strXLabels, account.Order - 1, account.ChartFormatId, False, True, True, account.ShowTrendline, strAppendLabel, blnPercentLabel, intCurrentMonth, intDecPlaces, blnShowBudget)
                            Case 5
                                highChart.HighChartTitle = strChartDesc
                                highChart.HighChartSubTitle = GetSubTitle(account.ChartFormatId, strAppendLabel, blnShowBudget)
                                highChart.HighChartType = "area"
                                highChart.intColCount = intPeriods
                                highChart.decChartAmts = decChartAmts
                                highChart.strXLabels = strXLabels
                                highChart.intChartIndex = account.Order - 1
                                highChart.intFormat = account.ChartFormatId
                                highChart.strAppendLabel = strAppendLabel
                                highChart.blnPercentLabel = blnPercentLabel
                                highChart.intLastActual = intLastActual
                                highChart.intDecPlaces = intDecPlaces
                                highChart.blnShowBudget = blnShowBudget
                                highChart.rowCounts = rowCount
                                highChart.seriesLabels = seriesLabel
                                highChart.HighChartFormatType = account.ChartFormatTypeId
                            Case 6
                                highChart.HighChartTitle = strChartDesc
                                highChart.HighChartSubTitle = GetSubTitle(account.ChartFormatId, strAppendLabel, blnShowBudget)
                                highChart.HighChartType = "donut"
                                highChart.intColCount = intPeriods
                                highChart.decChartAmts = highchartDonut.decChartAmtsForDonut
                                highChart.strXLabels = strXLabels
                                highChart.intChartIndex = account.Order - 1
                                highChart.intFormat = account.ChartFormatId
                                highChart.strAppendLabel = strAppendLabel
                                highChart.blnPercentLabel = blnPercentLabel
                                highChart.intLastActual = intLastActual
                                highChart.intDecPlaces = intDecPlaces
                                highChart.blnShowBudget = blnShowBudget
                                highChart.rowCounts = rowCount
                                highChart.seriesLabels = highchartDonut.strXLabelsForDonut
                                highChart.HighChartFormatType = account.ChartFormatTypeId


                            Case 7
                                highChart.HighChartTitle = strChartDesc
                                highChart.HighChartSubTitle = GetSubTitle(account.ChartFormatId, strAppendLabel, blnShowBudget)
                                highChart.HighChartType = "pie"
                                highChart.intColCount = intPeriods
                                highChart.decChartAmts = highchartDonut.decChartAmtsForDonut
                                highChart.strXLabels = strXLabels
                                highChart.intChartIndex = account.Order - 1
                                highChart.intFormat = account.ChartFormatId
                                highChart.strAppendLabel = strAppendLabel
                                highChart.blnPercentLabel = blnPercentLabel
                                highChart.intLastActual = intLastActual
                                highChart.intDecPlaces = intDecPlaces
                                highChart.blnShowBudget = blnShowBudget
                                highChart.rowCounts = rowCount
                                highChart.seriesLabels = highchartDonut.strXLabelsForDonut
                                highChart.HighChartFormatType = account.ChartFormatTypeId

                                'area
                                'BuildAreaChart(spread, intPeriods, decChartAmts, strXLabels, account.Order - 1, account.ChartFormatId, strAppendLabel, blnPercentLabel, intDecPlaces, blnShowBudget)
                        End Select
                        list.Add(highChart)
                    Next
                End If
            End If
            Return Json(list, JsonRequestBehavior.AllowGet)
        End Function








        Private Function GetSubTitle(intChartFormat As Integer, ByVal strAppendLabel As String, ByVal blnShowBudget As Boolean) As String
            Dim StrDesc As String = ""

            Select Case intChartFormat
                Case 1
                    StrDesc = "Budget vs Actual"
                Case 2
                    StrDesc = "Budget vs Actual YTD"
                Case 3
                    StrDesc = "Current vs Prior Month"
                Case 4
                    StrDesc = "Current vs Prior Yr"
                Case 5
                    StrDesc = "Current vs Prior Yr YTD"
                Case 6
                    If blnShowBudget = False Then
                        StrDesc = "Current Year Results"
                    Else
                        StrDesc = "Current Year Forecast"
                    End If

                Case 7
                    StrDesc = "Last 12 Months"
                Case 8
                    StrDesc = "Multi Year Trend"
                Case 9
                    StrDesc = "Multi Year Trend YTD"
                Case 10
                    StrDesc = "12 month rolling forecast"
                Case 13
                    StrDesc = "Current year budget"
                Case 14
                    StrDesc = "Current Period"
                Case 15
                    StrDesc = "Current Period YTD"
                Case 16
                    StrDesc = "Prior Period"
                Case 17
                    StrDesc = "Prior Period YTD"
                Case 18
                    StrDesc = "Same Period Prior Year"
                Case 19
                    StrDesc = "Same Period Prior YTD"
            End Select
            If strAppendLabel <> "" Then
                StrDesc = StrDesc & strAppendLabel
            End If
            Return StrDesc

        End Function



        Private Function GetDonutAndPieData(dashboardId As Integer, analysisId As Integer, chartFormatId As Integer, chartFormatTypeId As Integer, arrayLength As Integer, blnShowasPercent As Boolean, decTotalBalances() As Decimal,blnArchBudget As Boolean) As HighChartModelForDonutAndPie
            Dim donutAndPie As New HighChartModelForDonutAndPie()
            Dim AccountIdList = dashboardDetailRepository.GetAccountIdList(dashboardId)
            Dim decChartAmts(0, arrayLength) As Decimal
            Dim seriesLabel As New List(Of String)
            Dim counter
            For counter = 0 To (AccountIdList.Count - 1)
                Dim decSaveAmts(12) As Decimal
                Array.Clear(decSaveAmts, 0, 12)
                Dim decSaveAmountForTrend(12) As Decimal
                Array.Clear(decSaveAmountForTrend, 0, 12)
                Dim seriesTitle As String = ""
                Dim balanceDashForSecondSeries As New Balance()
                balanceDashForSecondSeries = (From b In utility.BalanceRepository.GetBalanceRecordsByAnalysisId(analysisId)
                                                             Where b.AccountId = AccountIdList(counter) And b.AnalysisId = analysisId Select b).FirstOrDefault()
                Select Case chartFormatId
                    Case 18
                        CommonProcedures.GetH5Amts(balanceDashForSecondSeries, decSaveAmts)
                        For intCtr = (counter * 13) To ((counter * 13) + 11)
                            If blnShowasPercent = False Then
                                decChartAmts(0, intCtr) = decSaveAmts(intCtr - (counter * 13))
                            Else
                                If decTotalBalances(intCtr) <> 0 Then
                                    decChartAmts(0, intCtr) = ComputePercent(decSaveAmts(intCtr - (counter * 13)), decTotalBalances(intCtr))
                                End If
                            End If
                        Next
                    Case 19
                        CommonProcedures.GetH5Amts(balanceDashForSecondSeries, decSaveAmts)
                        For intCtr = (counter * 13) To ((counter * 13) + 11)
                            If blnShowasPercent = False Then
                                If intCtr = (counter * 13) Then
                                    decChartAmts(0, intCtr) = decSaveAmts(intCtr - (counter * 13))
                                Else
                                    decChartAmts(0, intCtr) = decChartAmts(0, (intCtr - 1)) + decSaveAmts(intCtr - (counter * 13))
                                End If
                            Else
                                If decTotalBalances(intCtr) <> 0 Then
                                    decChartAmts(0, intCtr) = ComputePercent(decSaveAmts(intCtr - (counter * 13)), decTotalBalances(intCtr))
                                End If
                            End If
                        Next
                    Case 14, 16
                        Select Case chartFormatTypeId
                            Case 2
                                If blnArchBudget = False Then
                                    CommonProcedures.GetBudgetAmts(balanceDashForSecondSeries, decSaveAmts)
                                Else
                                    CommonProcedures.GetArchivedBudgetAmts(balanceDashForSecondSeries, decSaveAmts)
                                End If

                                For intCtr = (counter * 13) To ((counter * 13) + 11)
                                    If blnShowasPercent = False Then
                                        decChartAmts(0, intCtr) = decSaveAmts(intCtr - (counter * 13))
                                    Else
                                        If decTotalBalances(intCtr) <> 0 Then
                                            decChartAmts(0, intCtr) = ComputePercent(decSaveAmts(intCtr - (counter * 13)), decTotalBalances(intCtr))
                                        End If
                                    End If
                                Next
                            Case 3
                                CommonProcedures.GetBudgetAmts(balanceDashForSecondSeries, decSaveAmts)
                                For intCtr = (counter * 13) To ((counter * 13) + 11)
                                    If blnShowasPercent = False Then
                                        decChartAmts(0, intCtr) = decSaveAmts(intCtr - (counter * 13))
                                    Else
                                        If decTotalBalances(intCtr) <> 0 Then
                                            decChartAmts(0, intCtr) = ComputePercent(decSaveAmts(intCtr - (counter * 13)), decTotalBalances(intCtr))
                                        End If
                                    End If
                                Next
                            Case 4
                                CommonProcedures.GetActualAmts(balanceDashForSecondSeries, decSaveAmts)
                                For intCtr = (counter * 13) To ((counter * 13) + 11)
                                    If blnShowasPercent = False Then
                                        decChartAmts(0, intCtr) = decSaveAmts(intCtr - (counter * 13))
                                    Else
                                        If decTotalBalances(intCtr) <> 0 Then
                                            decChartAmts(0, intCtr) = ComputePercent(decSaveAmts(intCtr - (counter * 13)), decTotalBalances(intCtr))
                                        End If
                                    End If
                                Next
                        End Select
                    Case 15, 17
                        Select Case chartFormatTypeId
                            Case 2
                                If blnArchBudget = False Then
                                    CommonProcedures.GetBudgetAmts(balanceDashForSecondSeries, decSaveAmts)
                                Else
                                    CommonProcedures.GetArchivedBudgetAmts(balanceDashForSecondSeries, decSaveAmts)
                                End If
                                For intCtr = (counter * 13) To ((counter * 13) + 11)
                                    If blnShowasPercent = False Then
                                        If intCtr = (counter * 13) Then
                                            decChartAmts(0, intCtr) = decSaveAmts(intCtr - (counter * 13))
                                        Else
                                            decChartAmts(0, intCtr) = decChartAmts(0, (intCtr - 1)) + decSaveAmts(intCtr - (counter * 13))
                                        End If


                                    Else
                                        If decTotalBalances(intCtr) <> 0 Then
                                            decChartAmts(0, intCtr) = ComputePercent(decSaveAmts(intCtr - (counter * 13)), decTotalBalances(intCtr))
                                        End If
                                    End If
                                Next
                            Case 3
                                CommonProcedures.GetBudgetAmts(balanceDashForSecondSeries, decSaveAmts)
                                For intCtr = (counter * 13) To ((counter * 13) + 11)
                                    If blnShowasPercent = False Then
                                        If intCtr = (counter * 13) Then
                                            decChartAmts(0, intCtr) = decSaveAmts(intCtr - (counter * 13))
                                        Else
                                            decChartAmts(0, intCtr) = decChartAmts(0, (intCtr - 1)) + decSaveAmts(intCtr - (counter * 13))
                                        End If
                                    Else
                                        If decTotalBalances(intCtr) <> 0 Then
                                            decChartAmts(0, intCtr) = ComputePercent(decSaveAmts(intCtr - (counter * 13)), decTotalBalances(intCtr))
                                        End If
                                    End If
                                Next
                            Case 4
                                CommonProcedures.GetActualAmts(balanceDashForSecondSeries, decSaveAmts)
                                For intCtr = (counter * 13) To ((counter * 13) + 11)
                                    If blnShowasPercent = False Then
                                        If intCtr = (counter * 13) Then
                                            decChartAmts(0, intCtr) = decSaveAmts(intCtr - (counter * 13))
                                        Else
                                            decChartAmts(0, intCtr) = decChartAmts(0, (intCtr - 1)) + decSaveAmts(intCtr - (counter * 13))
                                        End If
                                    Else
                                        If decTotalBalances(intCtr) <> 0 Then
                                            decChartAmts(0, intCtr) = ComputePercent(decSaveAmts(intCtr - (counter * 13)), decTotalBalances(intCtr))
                                        End If
                                    End If
                                Next
                        End Select
                    Case 7
                        CommonProcedures.GetActualAmts(balanceDashForSecondSeries, decSaveAmts)
                        CommonProcedures.GetH5Amts(balanceDashForSecondSeries, decSaveAmountForTrend)

                        decChartAmts(0, (counter * 13)) = decSaveAmts(0) + SumRange(decSaveAmountForTrend, 1, 12)
                        decChartAmts(0, ((counter * 13) + 1)) = SumRange(decSaveAmts, 0, 2) + SumRange(decSaveAmountForTrend, 2, 11)
                        decChartAmts(0, ((counter * 13) + 2)) = SumRange(decSaveAmts, 0, 3) + SumRange(decSaveAmountForTrend, 3, 10)
                        decChartAmts(0, ((counter * 13) + 3)) = SumRange(decSaveAmts, 0, 4) + SumRange(decSaveAmountForTrend, 4, 9)
                        decChartAmts(0, ((counter * 13) + 4)) = SumRange(decSaveAmts, 0, 5) + SumRange(decSaveAmountForTrend, 5, 8)
                        decChartAmts(0, ((counter * 13) + 5)) = SumRange(decSaveAmts, 0, 6) + SumRange(decSaveAmountForTrend, 6, 7)
                        decChartAmts(0, ((counter * 13) + 6)) = SumRange(decSaveAmts, 0, 7) + SumRange(decSaveAmountForTrend, 7, 6)
                        decChartAmts(0, ((counter * 13) + 7)) = SumRange(decSaveAmts, 0, 8) + SumRange(decSaveAmountForTrend, 8, 5)
                        decChartAmts(0, ((counter * 13) + 8)) = SumRange(decSaveAmts, 0, 9) + SumRange(decSaveAmountForTrend, 9, 4)
                        decChartAmts(0, ((counter * 13) + 9)) = SumRange(decSaveAmts, 0, 10) + SumRange(decSaveAmountForTrend, 10, 3)
                        decChartAmts(0, ((counter * 13) + 10)) = SumRange(decSaveAmts, 0, 11) + SumRange(decSaveAmountForTrend, 11, 2)
                        decChartAmts(0, ((counter * 13) + 11)) = SumRange(decSaveAmts, 0, 12) + SumRange(decSaveAmountForTrend, 12, 1)
                        decChartAmts(0, ((counter * 13) + 12)) = SumRange(decSaveAmts, 0, 13) + decSaveAmountForTrend(12)






                End Select
                seriesTitle = dashboardDetailRepository.GetSeriesLabel(dashboardId, AccountIdList(counter))
                seriesLabel.Add(seriesTitle)
            Next
            donutAndPie.decChartAmtsForDonut = decChartAmts
            donutAndPie.strXLabelsForDonut = seriesLabel
            Return donutAndPie
        End Function


        Public Function SumRange(ByRef dblArray() As Decimal, Optional ByVal StartRange As Long = -1, Optional ByVal Length As Long = -1)

            '-1 on StartRange indicates start at low bound
            If StartRange = -1 Then
                StartRange = LBound(dblArray)
            End If

            '-1 on Length indicates to span all elements to the end of the array
            If Length = -1 Then
                Length = UBound(dblArray) - StartRange - 1
            End If

            Dim dTotal As Double
            Dim lNdx As Long

            For lNdx = StartRange To StartRange + Length - 1
                dTotal = dTotal + dblArray(lNdx)
            Next lNdx

            SumRange = dTotal

        End Function


#End Region





    End Class



End Namespace