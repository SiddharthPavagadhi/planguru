﻿Imports FarPoint.Mvc.Spread
Imports System.Data.Entity
Imports System.Runtime.CompilerServices
Imports System.Linq.Expressions
Imports System.Collections.Generic
Imports System.Linq
Imports System.Web
Imports System.Web.Mvc
Imports System.Web.Routing

Public Module CommonProcedures
    Dim intChkBoxCol As Integer = 2
    Dim intRowTypeCol As Integer = 3
    ' Dim intDescCol As Integer = 4

    <Extension()>
    Public Function LabelFor(Of TModel, TValue)(html As HtmlHelper(Of TModel), expression As Expression(Of Func(Of TModel, TValue)), htmlAttributes As Object) As MvcHtmlString
        Return LabelFor(html, expression, New RouteValueDictionary(htmlAttributes))
    End Function

    Public Function LabelFor(Of TModel, TValue)(html As HtmlHelper(Of TModel), expression As Expression(Of Func(Of TModel, TValue)), htmlAttributes As IDictionary(Of String, Object)) As MvcHtmlString
        Dim metadata As ModelMetadata = ModelMetadata.FromLambdaExpression(expression, html.ViewData)
        Dim htmlFieldName As String = ExpressionHelper.GetExpressionText(expression)
        Dim labelText As String = If(metadata.DisplayName, If(metadata.PropertyName, htmlFieldName.Split("."c).Last()))

        If [String].IsNullOrEmpty(labelText) Then
            Return MvcHtmlString.Empty
        End If

        Dim tag As New TagBuilder("label")
        tag.MergeAttributes(htmlAttributes)
        tag.Attributes.Add("for", html.ViewContext.ViewData.TemplateInfo.GetFullHtmlFieldId(htmlFieldName))
        tag.SetInnerText(labelText)

        Return MvcHtmlString.Create(tag.ToString(TagRenderMode.Normal))
    End Function

    Public Function GetFormat(intCtr As String, strStmt As String) As String
        Select Case intCtr
            Case 0
                GetFormat = "Budget vs Actual"
            Case 1
                GetFormat = "Budget vs Actual YTD"
            Case 2
                GetFormat = "This period vs last period"
            Case 3
                GetFormat = "This period vs same period prior year"
            Case 4
                GetFormat = "This period vs same period prior year YTD"
            Case 5
                GetFormat = "Current year"
            Case 6
                GetFormat = "Trend for the last 12 months"
            Case 7
                GetFormat = "Multi-year trend"
            Case 8
                GetFormat = "Year to date multi-year trend"
            Case 9
                GetFormat = "12 month rolling forecast"
            Case Else
                GetFormat = ""
        End Select
    End Function
    Public Function NumberofPeriods(intPeriod As Integer, intFirstFical As Integer) As Integer
        If intFirstFical = 1 Then
            NumberofPeriods = intPeriod
        Else
            NumberofPeriods = intPeriod - (intFirstFical - 1)
            If NumberofPeriods < 0 Then
                NumberofPeriods += 11
            End If
        End If
    End Function

    Public Function GetMonth(intPeriod As Integer, intFirstFisalMonth As Integer) As String
        Dim intMonth As Integer
        Select Case True
            Case intFirstFisalMonth = 1
                intMonth = intPeriod
            Case intPeriod = 12
                intMonth = 12
            Case Else
                intMonth = intPeriod + (intFirstFisalMonth - 1)
                If intMonth > 11 Then
                    intMonth -= 12
                End If
        End Select

        Select Case intMonth
            Case 0
                GetMonth = "January"
            Case 1
                GetMonth = "February"
            Case 2
                GetMonth = "March"
            Case 3
                GetMonth = "April"
            Case 4
                GetMonth = "May"
            Case 5
                GetMonth = "June"
            Case 6
                GetMonth = "July"
            Case 7
                GetMonth = "August"
            Case 8
                GetMonth = "September"
            Case 9
                GetMonth = "October"
            Case 10
                GetMonth = "November"
            Case 11
                GetMonth = "December"
            Case Else
                GetMonth = "Total"
        End Select
    End Function
    Public Function GetChartDesc(intCtr As Integer) As String
        GetChartDesc = ""
        Select Case intCtr
            Case 0
                GetChartDesc = "None"
            Case 1
                GetChartDesc = "Bar"
            Case 2
                GetChartDesc = "Stacked bar"
            Case 3
                GetChartDesc = "Line"
            Case 4
                GetChartDesc = "Area"
                'Case 5
                '    GetChartDesc = "Pie"
        End Select

    End Function
    Public Function GetAbbrMonth(intPeriod As Integer, intFirstFiscalMonth As Integer) As String
        Dim intMonth As Integer
        If intFirstFiscalMonth = 1 Then
            intMonth = intPeriod
        Else
            intMonth = intPeriod + (intFirstFiscalMonth - 1)
            If intMonth > 11 Then
                intMonth -= 12
            End If
        End If
        Select Case intMonth
            Case 0
                GetAbbrMonth = "Jan"
            Case 1
                GetAbbrMonth = "Feb"
            Case 2
                GetAbbrMonth = "Mar"
            Case 3
                GetAbbrMonth = "Apr"
            Case 4
                GetAbbrMonth = "May"
            Case 5
                GetAbbrMonth = "June"
            Case 6
                GetAbbrMonth = "July"
            Case 7
                GetAbbrMonth = "Aug"
            Case 8
                GetAbbrMonth = "Sept"
            Case 9
                GetAbbrMonth = "Oct"
            Case 10
                GetAbbrMonth = "Nov"
            Case 11
                GetAbbrMonth = "Dec"
            Case Else
                GetAbbrMonth = ""
        End Select
    End Function
    Public Function GetMonthFirstLetter(ByVal intPeriod As Integer, ByVal intFirstFiscalMonth As Integer) As String
        Dim intMonth As Integer
        If intFirstFiscalMonth = 1 Then
            intMonth = intPeriod
        Else
            intMonth = intPeriod + (intFirstFiscalMonth - 1)
            If intMonth > 11 Then
                intMonth -= 12
            End If
        End If
        Select Case intMonth
            Case 0
                GetMonthFirstLetter = "J"
            Case 1
                GetMonthFirstLetter = "F"
            Case 2
                GetMonthFirstLetter = "M"
            Case 3
                GetMonthFirstLetter = "A"
            Case 4
                GetMonthFirstLetter = "M"
            Case 5
                GetMonthFirstLetter = "J"
            Case 6
                GetMonthFirstLetter = "J"
            Case 7
                GetMonthFirstLetter = "A"
            Case 8
                GetMonthFirstLetter = "S"
            Case 9
                GetMonthFirstLetter = "O"
            Case 10
                GetMonthFirstLetter = "N"
            Case 11
                GetMonthFirstLetter = "D"
            Case Else
                GetMonthFirstLetter = ""

        End Select
    End Function


    Public Sub BuildColsArray(intFormat As Integer, intPeriod As Integer, ByVal strYearType() As String)

        Select Case intFormat
            Case 0, 1 'budget vs actual
                strYearType(0) = "CB"
                strYearType(1) = "CA"
            Case 2
                strYearType(0) = "H1"
                strYearType(1) = "CA"
            Case 3, 4
                strYearType(0) = "H1"
                strYearType(1) = "CA"
            Case 5 'Current year trend
                strYearType(0) = "CA"
                strYearType(1) = "CB"
            Case 6 ' Trend for the last 12 months
                strYearType(0) = "H1"
                strYearType(1) = "CA"
            Case 7, 8 'Multi-year trend for the selected period
                strYearType(0) = "H5"
                strYearType(1) = "H4"
                strYearType(2) = "H3"
                strYearType(3) = "H2"
                strYearType(4) = "H1"
                strYearType(5) = "CA"
        End Select

    End Sub
    Public Function CheckboxClicked(intCol As Integer) As String

        CheckboxClicked = "var spread = document.getElementById('spdAnalytics');" _
                        & "var rc = spread.GetRowCount();" _
                        & "var count = 0;" _
                        & "for (var i = 0; i < rc; i++) {" _
                            & "var cellval = spread.GetValue(i," & intCol & ");" _
                            & "if (cellval == 'true') {" _
                                & "count++;" _
                            & "};" _
                        & "};" _
                        & "var selectedindex = $('#intChartType').get(0).selectedIndex;" _
                        & "var selectedValue = $('#intFormat').val();" _
                        & "if ((selectedValue === '1' || selectedValue === '4' || selectedValue === '3') && count > 3)" _
                        & "{" _
                        & "$.msgBox({" _
                        & "title: 'Alert'," _
                        & "content: 'You can not select more than 3 factors for this chart format.'," _
                        & "type: 'Error', " _
                        & "buttons: [{ value: 'Ok' }]," _
                        & "afterShow: function () {" _
                        & "$('[name=Ok]').focus();" _
                        & "$('#spdAnalytics').find('input:checkbox').attr('checked', false);" _
                        & "}" _
                        & "})" _
                        & "}" _
                        & "else" _
                        & "{" _
                        & "if (selectedindex > 0) {" _
                            & "if (count == 1) {" _
                                & "if (selectedindex == 3) {" _
                                    & "$('#ShowTrend').show();" _
                                & "}" _
                                & "$('#UpdateChart').show();" _
                            & "}" _
                            & "else if (count > 1) {" _
                                & "$('#ShowTrend').hide();" _
                                & "$('#blnShowTrend').attr('checked', false);" _
                                & "$('#UpdateChart').show();" _
                            & "}" _
                            & "else {" _
                                & "$('#ShowTrend').hide();" _
                                & "$('#blnShowTrend').attr('checked', false);" _
                            & "}" _
                        & "}" _
                        & "}"

        'CheckboxClicked = "var spread = document.getElementById('spdAnalytics');" _
        '               & "var rc = spread.GetRowCount();" _
        '               & "var count = 0;" _
        '               & "for (var i = 0; i < rc; i++) {" _
        '                   & "var cellval = spread.GetValue(i," & intCol & ");" _
        '                   & "if (cellval == 'true') {" _
        '                       & "count++;" _
        '                   & "};" _
        '               & "};" _
        '               & "var selectedindex = $('#intChartType').get(0).selectedIndex;" _
        '               & "if (selectedindex == 0) {" _
        '                   & "$('#UpdateChart').hide();" _
        '               & "}" _
        '               & "else if (selectedindex > 0) {" _
        '                   & "if (count == 1) {" _
        '                       & "if (selectedindex == 3) {" _
        '                           & "$('#ShowTrend').show();" _
        '                       & "}" _
        '                       & "$('#UpdateChart').show();" _
        '                   & "}" _
        '                   & "else if (count > 1) {" _
        '                       & "$('#ShowTrend').hide();" _
        '                       & "$('#UpdateChart').show();" _
        '                   & "}" _
        '                   & "else {" _
        '                       & "$('#ShowTrend').hide();" _
        '                       & "$('#UpdateChart').hide();" _
        '                   & "}" _
        '               & "}"

    End Function
    Public Function DSChkBoxClicked() As String
        DSChkBoxClicked = "var spread = document.getElementById('spdRE');" _
                        & "var rc = spread.GetRowCount();" _
                        & "var count = 0;" _
                        & "for (var i = 0; i < rc - 1; i++) {" _
                            & "var cellval = spread.GetValue(i,1);" _
                            & "if (cellval == 'true') {" _
                                & "count++;" _
                                & "if (count > 1) {" _
                                    & "alert('Only one item can be included in a dashboard chart');" _
                                    & "spread.SetValue(i,1,false);" _
                                    & "break;" _
                                & "}" _
                            & "};" _
                        & "};"


    End Function

    Public Sub AddTotalRow(ByRef spdAnalytics As FpSpread, ByVal intFormat As Integer, ByVal intNumberCols As Integer, ByVal intFirstNumCol As Integer, ByVal intColCount As Integer, ByVal decIncTotalAccum() As Decimal, ByRef intRowCtr As Integer, ByVal strRowType As String, ByVal strDesc As String, ByVal intPeriod As Integer, ByVal intDescCol As Integer)
        Dim intColCtr As Integer

        intRowCtr += 1
        spdAnalytics.Sheets(0).AddRows(intRowCtr, 1)
        spdAnalytics.Sheets(0).Rows(intRowCtr).Height = 18
        spdAnalytics.Sheets(0).SetValue(intRowCtr, intDescCol, strDesc)
        spdAnalytics.Sheets(0).Cells(intRowCtr, intDescCol).Font.Bold = True
        spdAnalytics.Sheets(0).Cells(intRowCtr, intDescCol).Font.Size = FontSize.Large
        spdAnalytics.Sheets(0).SetValue(intRowCtr, 2, strRowType)
        For intColCtr = intFirstNumCol To intColCount - 1
            Select Case intFormat
                Case 0 To 4
                    Select Case intColCtr
                        Case 4, 5
                            spdAnalytics.Sheets(0).SetValue(intRowCtr, intColCtr, decIncTotalAccum(intColCtr - intFirstNumCol))
                        Case 6
                            spdAnalytics.Sheets(0).SetValue(intRowCtr, intColCtr, decIncTotalAccum(0) - decIncTotalAccum(1))
                        Case 7
                            Try
                                spdAnalytics.Sheets(0).SetValue(intRowCtr, intColCtr, (decIncTotalAccum(0) - decIncTotalAccum(1)) / decIncTotalAccum(0))
                            Catch ex As Exception

                            End Try
                    End Select
                Case 6
                    If intPeriod < 11 Then
                        If (intColCtr - intFirstNumCol) < (11 - intPeriod) Then
                            spdAnalytics.Sheets(0).SetValue(intRowCtr, intColCtr, decIncTotalAccum((intColCtr - intFirstNumCol) + intPeriod + 1))
                        Else
                            If (intColCtr - intFirstNumCol) = 12 Then
                                spdAnalytics.Sheets(0).SetValue(intRowCtr, intColCtr, decIncTotalAccum(intColCtr - intFirstNumCol))
                            Else
                                spdAnalytics.Sheets(0).SetValue(intRowCtr, intColCtr, decIncTotalAccum((intColCtr - intFirstNumCol) - (11 - intPeriod)))
                            End If
                        End If
                    Else
                        spdAnalytics.Sheets(0).SetValue(intRowCtr, intColCtr, decIncTotalAccum(intColCtr))
                    End If
                Case Else
                    spdAnalytics.Sheets(0).SetValue(intRowCtr, intColCtr, decIncTotalAccum(intColCtr - intFirstNumCol))
            End Select
            spdAnalytics.Sheets(0).Cells(intRowCtr, intColCtr).Border.BorderStyleTop = BorderStyle.Double
            spdAnalytics.Sheets(0).Cells(intRowCtr, intColCtr).Border.BorderColorTop = Drawing.Color.Gray
            'spdAnalytics.Sheets(0).Rows(intRowCtr).Height = 18
        Next


    End Sub
    Public Sub AddRow(ByRef spdAnalytics As FpSpread, ByRef intRowCtr As Integer, ByVal strAcctDesc As String, ByVal strAcctDescriptor As String, ByVal strAcctType As String, strBrowser As String, ByVal intDescCol As Integer)
        intRowCtr += 1
        With spdAnalytics.Sheets(0)
            .AddRows(intRowCtr, 1)
            .SetValue(intRowCtr, intDescCol, strAcctDesc)
            .SetValue(intRowCtr, intRowTypeCol, strAcctDescriptor)
            Select Case strAcctDescriptor
                Case "Detail", "SHeading"
                    If strAcctType <> "NonFinancial" Then
                        .Cells(intRowCtr, intDescCol).Margin = New FarPoint.Web.Spread.Inset(5, 0, 0, 0)
                        If strBrowser <> "IE" Then
                            .Cells(intRowCtr, 0).Border.BorderColorBottom = Drawing.Color.White
                        End If
                    End If
                    .SetValue(intRowCtr, intDescCol, strAcctDesc)
                Case "SDetail"
                    .Cells(intRowCtr, intDescCol).Margin = New FarPoint.Web.Spread.Inset(10, 6, 0, 0)
                    If strBrowser <> "IE" Then
                        .Cells(intRowCtr, 0).Border.BorderColorBottom = Drawing.Color.White
                        .Cells(intRowCtr, 1).Border.BorderColorBottom = Drawing.Color.White
                    End If
                Case "STotal"
                    .Cells(intRowCtr, intDescCol).Margin = New FarPoint.Web.Spread.Inset(5, 0, 0, 0)
                    If strBrowser <> "IE" Then
                        .Cells(intRowCtr, 0).Border.BorderColorBottom = Drawing.Color.White
                    End If
                Case "SubDetail"
                    If strBrowser <> "IE" Then
                        .Cells(intRowCtr, 0).Border.BorderColorBottom = Drawing.Color.White
                    End If
                    .Cells(intRowCtr, intDescCol).Margin = New FarPoint.Web.Spread.Inset(5, 0, 0, 0)
                    .SetValue(intRowCtr, intDescCol, strAcctDesc)
                Case "SubEnd"
                    .Cells(intRowCtr, intDescCol).Margin = New FarPoint.Web.Spread.Inset(5, 0, 0, 0)
                    .SetValue(intRowCtr, intDescCol, strAcctDesc)
                Case Else
                    .SetValue(intRowCtr, intDescCol, strAcctDesc)
            End Select
        End With

    End Sub
    Public Sub FormatHeaderRow(ByRef spdAnalytics As FpSpread, ByVal intRowCtr As Integer, ByRef intSpanRows() As Integer, ByRef blnTotalFlag As Boolean, ByVal intDescCol As Integer)
        Dim imagecell As New FarPoint.Web.Spread.ImageCellType
        imagecell.ImageUrl = "~\content\images\spacer.png"

        With spdAnalytics.Sheets(0)
            .Cells(intRowCtr, 0).Font.Bold = True
            .Cells(intRowCtr, 0).Font.Size = FontSize.Large
            .Cells(intRowCtr, 2).CellType = imagecell
            .Cells(intRowCtr, 0).HorizontalAlign = HorizontalAlign.Center
            .Cells(intRowCtr, 0).Border.BorderColorBottom = Drawing.Color.White
            .Cells(intRowCtr, intDescCol).Font.Bold = True
            .Cells(intRowCtr, intDescCol).Font.Size = FontSize.Large
            .Cells(intRowCtr, intDescCol).Margin = New FarPoint.Web.Spread.Inset(0, 6, 0, 0)
            .SetValue(intRowCtr, 0, "+")
            .AddSpanCell(intRowCtr, intDescCol, 1, spdAnalytics.Sheets(0).ColumnCount - 3)
        End With
        intSpanRows(0) = intRowCtr
        blnTotalFlag = False
    End Sub
    Public Sub FormatSHeaderRow(ByRef spdAnalytics As FpSpread, ByVal intRowCtr As Integer, ByRef intSpanRows() As Integer, ByRef blnTotalFlag As Boolean, ByVal intDescCol As Integer)
        Dim imagecell As New FarPoint.Web.Spread.ImageCellType
        imagecell.ImageUrl = "~\content\images\spacer.png"

        With spdAnalytics.Sheets(0)
            .Cells(intRowCtr, 0).Font.Bold = True
            .Cells(intRowCtr, 0).Font.Size = FontSize.Large
            .Cells(intRowCtr, 2).CellType = imagecell
            .Cells(intRowCtr, 1).HorizontalAlign = HorizontalAlign.Center
            .Cells(intRowCtr, 1).Border.BorderColorBottom = Drawing.Color.White
            .SetValue(intRowCtr, 1, "+")
            .AddSpanCell(intRowCtr, intDescCol, 1, spdAnalytics.Sheets(0).ColumnCount - 3)
        End With
        intSpanRows(0) = intRowCtr
        blnTotalFlag = False
    End Sub

    Public Sub FormatHeaderRatioRow(ByRef spdAnalytics As FpSpread, ByVal intRowCtr As Integer, ByRef intSpanRows() As Integer, ByRef blnTotalFlag As Boolean, ByVal intDescCol As Integer)
        Dim imagecell As New FarPoint.Web.Spread.ImageCellType
        imagecell.ImageUrl = "~\content\images\spacer.png"

        With spdAnalytics.Sheets(0)
            .Cells(intRowCtr, 0).Font.Bold = True
            .Cells(intRowCtr, 0).Font.Size = FontSize.Large
            .Cells(intRowCtr, 1).CellType = imagecell
            .Cells(intRowCtr, 0).HorizontalAlign = HorizontalAlign.Center
            .Cells(intRowCtr, 0).Border.BorderColorBottom = Drawing.Color.White
            .Cells(intRowCtr, intDescCol).Font.Bold = True
            .Cells(intRowCtr, intDescCol).Font.Size = FontSize.Large
            ' .SetValue(intRowCtr, 1, "+")
            .AddSpanCell(intRowCtr, intDescCol, 1, spdAnalytics.Sheets(0).ColumnCount - 3)
        End With
        intSpanRows(0) = intRowCtr
        blnTotalFlag = False
    End Sub
    Public Sub FormatGroupHeader(ByRef spdAnalytics As FpSpread, ByVal intRowCtr As Integer, ByRef intSpanRows() As Integer, ByRef blnTotalFlag As Boolean, blnLastRowVis As Boolean, ByVal intDescCol As Integer)
        With spdAnalytics.Sheets(0)
            .Cells(intRowCtr, 0).Font.Bold = True
            .Cells(intRowCtr, 0).Font.Size = FontSize.Large
            .Cells(intRowCtr, 0).HorizontalAlign = HorizontalAlign.Center
            If blnLastRowVis = True Then
                .Cells(intRowCtr, 0).Border.BorderColorBottom = Drawing.Color.White
                '  .SetValue(intRowCtr, 0, "")
            Else
                .SetValue(intRowCtr, 0, "+")
            End If
            .AddSpanCell(intRowCtr, intDescCol, 1, spdAnalytics.Sheets(0).ColumnCount - 3)
        End With


    End Sub
    Public Sub FormatGroupEnd(ByRef spdAnalytics As FpSpread, ByVal intRowCtr As Integer, ByRef intSpanRows() As Integer, ByRef blnTotalFlag As Boolean, blnLastRowVis As Boolean, intHeaderRow As Integer)
        With spdAnalytics.Sheets(0)
            If blnLastRowVis = True Then
                .Cells(intRowCtr, 0).Font.Bold = True
                .Cells(intRowCtr, 0).Font.Size = FontSize.Large
                .Cells(intRowCtr, 0).HorizontalAlign = HorizontalAlign.Center
                .SetValue(intRowCtr, 0, "+")
            Else
                '   .AddSpanCell(intHeaderRow, 0, 2, 1)
            End If
        End With

    End Sub
    Public Sub FormatTotalRow(ByRef spdAnalytics As FpSpread, ByVal intRowCtr As Integer, ByRef intSpanRows() As Integer, ByRef blnTotalFlag As Boolean, ByVal intFirstNumCol As Integer, ByVal intNumberCols As Integer, ByVal strClassDesc As String, ByVal strBrowser As String, ByVal intDescCol As Integer)
        If intFirstNumCol <> 0 Then
            For intColCtr = intFirstNumCol To spdAnalytics.Sheets(0).ColumnCount - 1
                If strBrowser <> "IE" Then
                    spdAnalytics.Sheets(0).Cells(intRowCtr - 1, intColCtr).Border.BorderStyleBottom = BorderStyle.Double
                    spdAnalytics.Sheets(0).Cells(intRowCtr - 1, intColCtr).Border.BorderColorBottom = Drawing.Color.Gray
                End If
                spdAnalytics.Sheets(0).Cells(intRowCtr, intColCtr).Font.Bold = True
                spdAnalytics.Sheets(0).Cells(intRowCtr, intColCtr).Font.Size = FontSize.Large
            Next

        End If
        spdAnalytics.Sheets(0).Cells(intRowCtr, 0).Font.Bold = True
        spdAnalytics.Sheets(0).Cells(intRowCtr, 0).Font.Size = FontSize.Large
        spdAnalytics.Sheets(0).Cells(intRowCtr, 0).HorizontalAlign = HorizontalAlign.Center
        spdAnalytics.Sheets(0).Cells(intRowCtr, intDescCol).Font.Bold = True
        spdAnalytics.Sheets(0).Cells(intRowCtr, intDescCol).Font.Size = FontSize.Large
        spdAnalytics.Sheets(0).SetValue(intRowCtr, 0, "+")
        intSpanRows(1) = intRowCtr
        Select Case strClassDesc
            Case "Cost of sales"
                blnTotalFlag = True
        End Select
    End Sub

    Public Sub FormatSTotalRow(ByRef spdAnalytics As FpSpread, ByVal intRowCtr As Integer, ByVal intFirstNumCol As Integer, ByVal strBrowser As String)
        If intFirstNumCol <> 0 Then
            For intColCtr = intFirstNumCol To spdAnalytics.Sheets(0).ColumnCount - 1
                If strBrowser <> "IE" Then
                    spdAnalytics.Sheets(0).Cells(intRowCtr - 1, intColCtr).Border.BorderStyleBottom = BorderStyle.Double
                    spdAnalytics.Sheets(0).Cells(intRowCtr - 1, intColCtr).Border.BorderColorBottom = Drawing.Color.Gray
                End If
                'spdAnalytics.Sheets(0).Cells(intRowCtr, intColCtr).Font.Bold = True
                'spdAnalytics.Sheets(0).Cells(intRowCtr, intColCtr).Font.Size = FontSize.Large
            Next
        End If
        spdAnalytics.Sheets(0).Cells(intRowCtr, 1).Font.Bold = True
        spdAnalytics.Sheets(0).Cells(intRowCtr, 1).Font.Size = FontSize.Large
        spdAnalytics.Sheets(0).Cells(intRowCtr, 1).HorizontalAlign = HorizontalAlign.Center
        spdAnalytics.Sheets(0).SetValue(intRowCtr, 1, "+")

    End Sub
    Public Sub FormatGrandTotalRow(ByRef spdAnalytics As FpSpread, ByVal intRowCtr As Integer, ByRef blnTotalFlag As Boolean, ByVal intFirstNumCol As Integer, ByVal intNumberCols As Integer, ByVal strClassDesc As String, ByVal strBrowser As String, ByVal intDescCol As Integer)
        If intFirstNumCol <> 0 Then
            For intColCtr = intFirstNumCol To spdAnalytics.Sheets(0).ColumnCount - 1
                If strBrowser <> "IE" Then
                    spdAnalytics.Sheets(0).Cells(intRowCtr - 1, intColCtr).Border.BorderStyleBottom = BorderStyle.Double
                    spdAnalytics.Sheets(0).Cells(intRowCtr - 1, intColCtr).Border.BorderColorBottom = Drawing.Color.Gray
                End If
                spdAnalytics.Sheets(0).Cells(intRowCtr, intColCtr).Font.Bold = True
                spdAnalytics.Sheets(0).Cells(intRowCtr, intColCtr).Font.Size = FontSize.Large
            Next
        End If
        spdAnalytics.Sheets(0).Cells(intRowCtr, 0).Font.Bold = True
        spdAnalytics.Sheets(0).Cells(intRowCtr, 0).Font.Size = FontSize.Large
        spdAnalytics.Sheets(0).Cells(intRowCtr, 0).HorizontalAlign = HorizontalAlign.Center
        spdAnalytics.Sheets(0).Cells(intRowCtr, intDescCol).Font.Bold = True
        spdAnalytics.Sheets(0).Cells(intRowCtr, intDescCol).Font.Size = FontSize.Large

    End Sub
    Public Sub AccumRunningTotals(ByVal intFormat As Integer, ByRef decAccumTotals() As Decimal, ByVal intPeriod As Integer, ByVal decPeriodAmts() As Decimal, intIndex As Integer)
        Dim intColCtr As Integer

        Select Case intFormat
            Case 0, 3
                decAccumTotals(intIndex) += decPeriodAmts(intPeriod)

                For intColCtr = 0 To intPeriod
                    decAccumTotals(intIndex + 2) += decPeriodAmts(intColCtr)
                Next
            Case 2
                Select Case intIndex
                    Case 0
                        If intPeriod = 0 Then
                            decAccumTotals(intIndex) += decPeriodAmts(11)
                        End If
                    Case Else
                        If intPeriod = 0 Then
                            decAccumTotals(1) += decPeriodAmts(intPeriod)
                        Else
                            decAccumTotals(0) += decPeriodAmts(intPeriod - 1)
                            decAccumTotals(1) += decPeriodAmts(intPeriod)
                        End If
                End Select
            Case 7
                decAccumTotals(intIndex) += decPeriodAmts(intPeriod)
            Case 8
                For intColCtr = 0 To intPeriod
                    decAccumTotals(intIndex) += decPeriodAmts(intColCtr)
                Next
        End Select
    End Sub
    Public Sub ComputeVariances(ByVal decSaveAmts() As Decimal, strAcctType As String, ByRef decAccumTotals() As Decimal, ByRef blnShowasPercent As Boolean)

        decSaveAmts(0) = Math.Round(decSaveAmts(0), 4)
        decSaveAmts(1) = Math.Round(decSaveAmts(1), 4)
        decSaveAmts(2) = decSaveAmts(0) - decSaveAmts(1)
        If decSaveAmts(0) <> 0 Then
            decSaveAmts(3) = Math.Round(decSaveAmts(2) / decSaveAmts(0), 4)
        Else
            decSaveAmts(3) = 0
        End If
        decSaveAmts(0) = Math.Round(decSaveAmts(4), 4)
        decSaveAmts(1) = Math.Round(decSaveAmts(5), 4)
        decSaveAmts(6) = decSaveAmts(4) - decSaveAmts(5)
        If decSaveAmts(4) <> 0 Then
            decSaveAmts(7) = Math.Round(decSaveAmts(6) / decSaveAmts(4), 4)
        Else
            decSaveAmts(4) = 0
        End If
        Select Case strAcctType
            Case "Revenue", "Reverse", "Assets", "CashFlow", "Ratio", "NonFinancial"
                decSaveAmts(2) = decSaveAmts(2) * -1
                decSaveAmts(3) = decSaveAmts(3) * -1
                decSaveAmts(6) = decSaveAmts(6) * -1
                decSaveAmts(7) = decSaveAmts(7) * -1
            Case Else
                If decSaveAmts(0) < 0 Then
                    decSaveAmts(2) = decSaveAmts(2) * -1
                    decSaveAmts(3) = decSaveAmts(3) * -1
                    decSaveAmts(7) = decSaveAmts(6) * -1
                    decSaveAmts(7) = decSaveAmts(7) * -1
                End If
        End Select
    End Sub
    'Public Sub ComputeVariances(ByVal decSaveAmts() As Decimal, strAcctType As String, ByRef decAccumTotals() As Decimal, ByRef blnShowasPercent As Boolean)

    '    decSaveAmts(0) = Math.Round(decSaveAmts(0), 4)
    '    decSaveAmts(1) = Math.Round(decSaveAmts(1), 4)
    '    decSaveAmts(2) = decSaveAmts(0) - decSaveAmts(1)
    '    If decSaveAmts(0) <> 0 Then
    '        decSaveAmts(3) = decSaveAmts(2) / decSaveAmts(0)
    '    Else
    '        decSaveAmts(3) = 0
    '    End If
    '    Select Case strAcctType
    '        Case "Revenue", "Reverse", "Assets", "CashFlow", "Ratio", "NonFinancial"
    '            decSaveAmts(2) = decSaveAmts(2) * -1
    '            decSaveAmts(3) = decSaveAmts(3) * -1
    '        Case Else
    '            If decSaveAmts(0) < 0 Then
    '                decSaveAmts(2) = decSaveAmts(2) * -1
    '                decSaveAmts(3) = decSaveAmts(3) * -1
    '            End If
    '    End Select
    'End Sub

    Public Sub GetBudgetAmts(ByVal balance As Balance, ByRef decPeriodAmts() As Decimal)
        Array.Clear(decPeriodAmts, 0, 11)
        decPeriodAmts(0) = balance.B101
        decPeriodAmts(1) = balance.B102
        decPeriodAmts(2) = balance.B103
        decPeriodAmts(3) = balance.B104
        decPeriodAmts(4) = balance.B105
        decPeriodAmts(5) = balance.B106
        decPeriodAmts(6) = balance.B107
        decPeriodAmts(7) = balance.B108
        decPeriodAmts(8) = balance.B109
        decPeriodAmts(9) = balance.B110
        decPeriodAmts(10) = balance.B111
        decPeriodAmts(11) = balance.B112
    End Sub
    Public Sub GetBudget2Amts(ByVal balance As Balance, ByRef decPeriodAmts() As Decimal)
        Array.Clear(decPeriodAmts, 0, 11)
        decPeriodAmts(0) = balance.B201
        decPeriodAmts(1) = balance.B202
        decPeriodAmts(2) = balance.B203
        decPeriodAmts(3) = balance.B204
        decPeriodAmts(4) = balance.B205
        decPeriodAmts(5) = balance.B206
        decPeriodAmts(6) = balance.B207
        decPeriodAmts(7) = balance.B208
        decPeriodAmts(8) = balance.B209
        decPeriodAmts(9) = balance.B210
        decPeriodAmts(10) = balance.B211
        decPeriodAmts(11) = balance.B212
    End Sub
    Public Sub GetArchivedBudgetAmts(ByVal balance As Balance, ByRef decPeriodAmts() As Decimal)
        Array.Clear(decPeriodAmts, 0, 11)
        decPeriodAmts(0) = balance.B301
        decPeriodAmts(1) = balance.B302
        decPeriodAmts(2) = balance.B303
        decPeriodAmts(3) = balance.B304
        decPeriodAmts(4) = balance.B305
        decPeriodAmts(5) = balance.B306
        decPeriodAmts(6) = balance.B307
        decPeriodAmts(7) = balance.B308
        decPeriodAmts(8) = balance.B309
        decPeriodAmts(9) = balance.B310
        decPeriodAmts(10) = balance.B311
        decPeriodAmts(11) = balance.B312
    End Sub
    Public Sub GetActualAmts(ByVal balance As Balance, ByRef decPeriodAmts() As Decimal)
        Array.Clear(decPeriodAmts, 0, 11)
        decPeriodAmts(0) = balance.A101
        decPeriodAmts(1) = balance.A102
        decPeriodAmts(2) = balance.A103
        decPeriodAmts(3) = balance.A104
        decPeriodAmts(4) = balance.A105
        decPeriodAmts(5) = balance.A106
        decPeriodAmts(6) = balance.A107
        decPeriodAmts(7) = balance.A108
        decPeriodAmts(8) = balance.A109
        decPeriodAmts(9) = balance.A110
        decPeriodAmts(10) = balance.A111
        decPeriodAmts(11) = balance.A112
    End Sub
    Public Sub GetH1Amts(ByVal balance As Balance, ByRef decPeriodAmts() As Decimal)
        Array.Clear(decPeriodAmts, 0, 11)
        decPeriodAmts(0) = balance.H101
        decPeriodAmts(1) = balance.H102
        decPeriodAmts(2) = balance.H103
        decPeriodAmts(3) = balance.H104
        decPeriodAmts(4) = balance.H105
        decPeriodAmts(5) = balance.H106
        decPeriodAmts(6) = balance.H107
        decPeriodAmts(7) = balance.H108
        decPeriodAmts(8) = balance.H109
        decPeriodAmts(9) = balance.H110
        decPeriodAmts(10) = balance.H111
        decPeriodAmts(11) = balance.H112
    End Sub
    Public Sub GetH2Amts(ByVal balance As Balance, ByRef decPeriodAmts() As Decimal)
        Array.Clear(decPeriodAmts, 0, 11)
        decPeriodAmts(0) = balance.H201
        decPeriodAmts(1) = balance.H202
        decPeriodAmts(2) = balance.H203
        decPeriodAmts(3) = balance.H204
        decPeriodAmts(4) = balance.H205
        decPeriodAmts(5) = balance.H206
        decPeriodAmts(6) = balance.H207
        decPeriodAmts(7) = balance.H208
        decPeriodAmts(8) = balance.H209
        decPeriodAmts(9) = balance.H210
        decPeriodAmts(10) = balance.H211
        decPeriodAmts(11) = balance.H212
    End Sub
    Public Sub GetH3Amts(ByVal balance As Balance, ByRef decPeriodAmts() As Decimal)
        Array.Clear(decPeriodAmts, 0, 11)
        decPeriodAmts(0) = balance.H301
        decPeriodAmts(1) = balance.H302
        decPeriodAmts(2) = balance.H303
        decPeriodAmts(3) = balance.H304
        decPeriodAmts(4) = balance.H305
        decPeriodAmts(5) = balance.H306
        decPeriodAmts(6) = balance.H307
        decPeriodAmts(7) = balance.H308
        decPeriodAmts(8) = balance.H309
        decPeriodAmts(9) = balance.H310
        decPeriodAmts(10) = balance.H311
        decPeriodAmts(11) = balance.H312
    End Sub
    Public Sub GetH4Amts(ByVal balance As Balance, ByRef decPeriodAmts() As Decimal)
        Array.Clear(decPeriodAmts, 0, 11)
        decPeriodAmts(0) = balance.H401
        decPeriodAmts(1) = balance.H402
        decPeriodAmts(2) = balance.H403
        decPeriodAmts(3) = balance.H404
        decPeriodAmts(4) = balance.H405
        decPeriodAmts(5) = balance.H406
        decPeriodAmts(6) = balance.H407
        decPeriodAmts(7) = balance.H408
        decPeriodAmts(8) = balance.H409
        decPeriodAmts(9) = balance.H410
        decPeriodAmts(10) = balance.H411
        decPeriodAmts(11) = balance.H412
    End Sub
    Public Sub GetH5Amts(ByVal balance As Balance, ByRef decPeriodAmts() As Decimal)
        Array.Clear(decPeriodAmts, 0, 11)
        decPeriodAmts(0) = balance.H501
        decPeriodAmts(1) = balance.H502
        decPeriodAmts(2) = balance.H503
        decPeriodAmts(3) = balance.H504
        decPeriodAmts(4) = balance.H505
        decPeriodAmts(5) = balance.H506
        decPeriodAmts(6) = balance.H507
        decPeriodAmts(7) = balance.H508
        decPeriodAmts(8) = balance.H509
        decPeriodAmts(9) = balance.H510
        decPeriodAmts(10) = balance.H511
        decPeriodAmts(11) = balance.H512
    End Sub
    <CustomActionFilter()>
    Public Sub SetColumnHeader(ByRef spdAnalytics As FpSpread, ByVal intFormat As Integer, ByVal intPeriod As Integer, ByVal intColumnCount As Integer, ByVal intFirstNumberColumn As Integer, intFirstFiscal As Integer, ByVal blnShowTotalCol As Boolean, ByVal strHeadingTextBudget As String, ByVal intHPeriod As Integer, ByVal strController As String, ByVal intFirstYear As Integer, ByVal blnShowBudget As Boolean)
        Try
            Logger.Log.Info(String.Format("SetColumnHeader method execution Starts"))
            Dim strHeadingText As String = ""
            Dim intPeriodCounter As Integer
            Dim intFirstPeriod As Integer
            Dim intCurrentMonth As Integer = Val(DateTime.Today.ToString("MM")) - 2
            Dim intCurrentYrPeriods As Integer = 11
            Dim intColNumber As Integer = 0
            Dim imagecell As New FarPoint.Web.Spread.ImageCellType
            Dim s As New FarPoint.Web.Spread.StyleInfo




            s.HorizontalAlign = HorizontalAlign.Center
            s.VerticalAlign = VerticalAlign.Middle
            imagecell.ImageUrl = "~\content\images\icon-chart.png"

            spdAnalytics.Sheets(0).ColumnHeaderAutoText = FarPoint.Web.Spread.HeaderAutoText.Blank
            'spdAnalytics.Sheets(0).ColumnHeaderSpanModel.Add(0, 0, 1, 5)
            'spdAnalytics.Sheets(0).ColumnHeaderSpanModel.Add(1, 0, 1, 3)
            With spdAnalytics.Sheets(0).ColumnHeader
                .RowCount = 0
                .RowCount = 2
                .DefaultStyle = s
                .Rows(0, 1).BackColor = System.Drawing.Color.FromArgb(199, 220, 248)
                .Rows(0).Border.BorderStyle = BorderStyle.Solid
                .Rows(0).Border.BorderSize = 0.75
                .Rows(0).Border.BorderColor = Drawing.Color.Gray
                .Rows(1).Border.BorderStyle = BorderStyle.Solid
                .Rows(1).Border.BorderSize = 0.75
                .Rows(1).Border.BorderColor = Drawing.Color.Gray
                .Rows(0).Font.Size = FontSize.Large
                .Rows(1).Font.Size = FontSize.Large
                .Rows(0).Font.Bold = True
                .Height = 40
                .Rows(0).Height = 20
                .Rows(1).Height = 20
                .Cells(1, 2).CellType = imagecell
                .Cells(0, 0).Border.BorderStyleTop = BorderStyle.Solid
                .Cells(0, 0).Border.BorderSizeTop = 0.75
                .Cells(0, 0).Border.BorderColorTop = Drawing.Color.Gray
                .Cells(0, 0).Border.BorderStyleBottom = BorderStyle.None
                .Cells(1, 0, 1, 3).Border.BorderStyleTop = BorderStyle.None
                .Cells(1, 3).Border.BorderStyleTop = BorderStyle.None
                .Cells(1, 2).Border.BorderStyleRight = BorderStyle.None
                .Cells(1, 2).Border.BorderStyleLeft = BorderStyle.None
                .Cells(1, 0).Border.BorderStyleBottom = BorderStyle.Solid
                .Cells(1, 0).Border.BorderSizeBottom = 0.75
                .Cells(1, 0).Border.BorderColorBottom = Drawing.Color.Gray
                .Cells(1, 2).Border.BorderStyleBottom = BorderStyle.Solid
                .Cells(1, 2).Border.BorderSizeBottom = 0.75
                .Cells(1, 2).Border.BorderColorBottom = Drawing.Color.Gray
                .Cells(1, 3).Border.BorderStyleBottom = BorderStyle.Solid
                .Cells(1, 3).Border.BorderSizeBottom = 0.75
                .Cells(1, 3).Border.BorderColorBottom = Drawing.Color.Gray
            End With
            spdAnalytics.Sheets(0).ColumnHeaderSpanModel.Add(0, 0, 1, 6)
            spdAnalytics.Sheets(0).ColumnHeaderSpanModel.Add(1, 0, 1, 2)
            spdAnalytics.Sheets(0).ColumnHeaderSpanModel.Add(1, 3, 1, 3)
            'set column headings for row 1 
            Select Case intFormat
                Case 0
                    strHeadingText = "Budget vs Actual -" & CommonProcedures.GetMonth(intPeriod, intFirstFiscal)
                Case 2
                    strHeadingText = "Current vs Prior Month - " & CommonProcedures.GetMonth(intPeriod, intFirstFiscal)
                Case 3
                    strHeadingText = "Current vs Prior Year - " & CommonProcedures.GetMonth(intPeriod, intFirstFiscal)
                Case 5
                    If blnShowBudget = True Then
                        strHeadingText = "Current Year Forecast"
                    Else
                        strHeadingText = "Current Year Actual"
                    End If

                Case 6
                    strHeadingText = "Trend For The Last 12 Months"
                Case 7
                    strHeadingText = "Multi-year Trend for " & CommonProcedures.GetMonth(intPeriod, intFirstFiscal)
                Case 8
                    strHeadingText = "Multi-year YTD Trend Through " & CommonProcedures.GetMonth(intPeriod, intFirstFiscal)
                Case 9
                    strHeadingText = "12 Month Rolling Forecast"
                Case 10
                    If intFirstFiscal = 1 Then
                        strHeadingText = intFirstYear - intHPeriod
                    Else
                        strHeadingText = intFirstYear - (intHPeriod + 1) & "/" & intFirstYear - intHPeriod
                    End If
                Case 11
                    strHeadingText = "Current Year Budget"

            End Select
            With spdAnalytics.Sheets(0)
                Select Case intFormat
                    Case 7, 8
                        For intPeriodCounter = intFirstNumberColumn To .ColumnCount - 1
                            If .Columns(intPeriodCounter).Visible = True Then
                                Dim intCols As Integer = .ColumnCount
                                .ColumnHeaderSpanModel.Add(0, intPeriodCounter, 1, .ColumnCount - intPeriodCounter)
                                .ColumnHeader.Cells(0, intPeriodCounter).Text = strHeadingText
                                Exit For
                            End If
                        Next
                    Case 5
                        If blnShowTotalCol = True Then
                            Select Case True
                                Case intPeriod = -1
                                    .ColumnHeaderSpanModel.Add(0, intFirstNumberColumn + intPeriod + 1, 1, intColumnCount - (intFirstNumberColumn + intPeriod + 1))
                                    .ColumnHeader.Cells(0, intFirstNumberColumn + intPeriod + 1).Text = strHeadingTextBudget
                                    .ColumnHeader.Cells(0, intFirstNumberColumn + intPeriod + 1).BackColor = Drawing.Color.FromArgb(179, 197, 221)
                                    'Case intPeriod < (intColumnCount - (intFirstNumberColumn + 2))
                                    '    .ColumnHeaderSpanModel.Add(0, intFirstNumberColumn, 1, intPeriod + 1)
                                    '    .ColumnHeader.Cells(0, intFirstNumberColumn).Text = strHeadingText
                                    '    .ColumnHeaderSpanModel.Add(0, intFirstNumberColumn + intPeriod + 1, 1, intColumnCount - (intFirstNumberColumn + intPeriod + 1))
                                    '    .ColumnHeader.Cells(0, intFirstNumberColumn + intPeriod + 1).Text = strHeadingTextBudget
                                    '    .ColumnHeader.Cells(0, intFirstNumberColumn + intPeriod + 1).BackColor = Drawing.Color.FromArgb(179, 197, 221)
                                Case Else
                                    .ColumnHeaderSpanModel.Add(0, intFirstNumberColumn, 1, intColumnCount - intFirstNumberColumn)
                                    .ColumnHeader.Cells(0, intFirstNumberColumn).Text = strHeadingText
                            End Select
                        Else
                            Select Case True
                                Case intPeriod = -1
                                    .ColumnHeader.Cells(0, intFirstNumberColumn).Text = strHeadingText
                                    .ColumnHeaderSpanModel.Add(0, intFirstNumberColumn + intPeriod + 1, 1, intColumnCount - (intFirstNumberColumn + intPeriod + 1))
                                    .ColumnHeader.Cells(0, intFirstNumberColumn + intPeriod + 1).Text = strHeadingTextBudget
                                    .ColumnHeader.Cells(0, intFirstNumberColumn + intPeriod + 1).BackColor = Drawing.Color.FromArgb(179, 197, 221)
                                    'Case intPeriod < (intColumnCount - (intFirstNumberColumn + 1))
                                    '    .ColumnHeaderSpanModel.Add(0, intFirstNumberColumn, 1, intPeriod + 1)
                                    '    .ColumnHeader.Cells(0, intFirstNumberColumn).Text = strHeadingText
                                    '    .ColumnHeaderSpanModel.Add(0, intFirstNumberColumn + intPeriod + 1, 1, intColumnCount - (intFirstNumberColumn + intPeriod + 1))
                                    '    .ColumnHeader.Cells(0, intFirstNumberColumn + intPeriod + 1).Text = strHeadingTextBudget
                                    '    .ColumnHeader.Cells(0, intFirstNumberColumn + intPeriod + 1).BackColor = Drawing.Color.FromArgb(179, 197, 221)
                                Case Else
                                    .ColumnHeaderSpanModel.Add(0, intFirstNumberColumn, 1, intColumnCount - intFirstNumberColumn)
                                    .ColumnHeader.Cells(0, intFirstNumberColumn).Text = strHeadingText
                            End Select
                        End If
                    Case 6, 9, 10, 11
                        .ColumnHeaderSpanModel.Add(0, intFirstNumberColumn, 1, 13)
                        .ColumnHeader.Cells(0, intFirstNumberColumn).Text = strHeadingText
                    Case 0 To 4
                        .ColumnHeaderSpanModel.Add(0, intFirstNumberColumn, 1, 4)
                        .ColumnHeader.Cells(0, intFirstNumberColumn).Text = strHeadingText
                        Select Case strController
                            Case "RE", "CF", "OM"
                                Select Case intFormat
                                    Case 0
                                        .ColumnHeaderSpanModel.Add(0, intFirstNumberColumn + 4, 1, 4)
                                        .ColumnHeader.Cells(0, intFirstNumberColumn + 4).Text = "Budget vs Actual - YTD"
                                    Case 3
                                        .ColumnHeaderSpanModel.Add(0, intFirstNumberColumn + 4, 1, 4)
                                        .ColumnHeader.Cells(0, intFirstNumberColumn + 4).Text = "Current vs prior year - YTD"
                                End Select
                        End Select

                End Select

                'set column headings for row 2
                Select Case intFormat
                    Case 0 To 4
                        .ColumnHeader.Cells(1, intFirstNumberColumn + 2).Text = "Variance"
                        .ColumnHeader.Cells(1, intFirstNumberColumn + 2).BackColor = Drawing.Color.FromArgb(219, 219, 219)
                        .ColumnHeader.Cells(1, intFirstNumberColumn + 3).Text = "% Var"
                        .ColumnHeader.Cells(1, intFirstNumberColumn + 3).BackColor = Drawing.Color.FromArgb(219, 219, 219)
                        Select Case strController
                            Case "RE", "CF", "OM"
                                Select Case intFormat
                                    Case 0, 3
                                        .ColumnHeader.Cells(1, intFirstNumberColumn + 6).Text = "Variance"
                                        .ColumnHeader.Cells(1, intFirstNumberColumn + 6).BackColor = Drawing.Color.FromArgb(219, 219, 219)
                                        .ColumnHeader.Cells(1, intFirstNumberColumn + 7).Text = "% Var"
                                        .ColumnHeader.Cells(1, intFirstNumberColumn + 7).BackColor = Drawing.Color.FromArgb(219, 219, 219)
                                End Select
                        End Select

                        .ColumnHeader.Cells(1, intFirstNumberColumn).BackColor = Drawing.Color.FromArgb(179, 197, 221)
                        .ColumnHeader.Cells(1, intFirstNumberColumn + 1).BackColor = Drawing.Color.FromArgb(200, 221, 248)

                        Select Case intFormat
                            Case 0
                                .ColumnHeader.Cells(1, intFirstNumberColumn).Text = "Budget"
                                .ColumnHeader.Cells(1, intFirstNumberColumn + 1).Text = "Actual"
                                Select Case strController
                                    Case "RE", "CF", "OM"
                                        .ColumnHeader.Cells(1, intFirstNumberColumn + 4).Text = "Budget"
                                        .ColumnHeader.Cells(1, intFirstNumberColumn + 5).Text = "Actual"
                                        .ColumnHeader.Cells(1, intFirstNumberColumn + 4).BackColor = Drawing.Color.FromArgb(179, 197, 221)
                                        .ColumnHeader.Cells(1, intFirstNumberColumn + 5).BackColor = Drawing.Color.FromArgb(200, 221, 248)
                                End Select

                            Case 2, 3
                                .ColumnHeader.Cells(1, intFirstNumberColumn).Text = "Prior"
                                .ColumnHeader.Cells(1, intFirstNumberColumn + 1).Text = "Current"
                                Select Case strController
                                    Case "RE", "CF", "OM"
                                        If intFormat = 3 Then
                                            .ColumnHeader.Cells(1, intFirstNumberColumn + 4).Text = "Prior"
                                            .ColumnHeader.Cells(1, intFirstNumberColumn + 5).Text = "Current"
                                            .ColumnHeader.Cells(1, intFirstNumberColumn + 4).BackColor = Drawing.Color.FromArgb(179, 197, 221)
                                            .ColumnHeader.Cells(1, intFirstNumberColumn + 5).BackColor = Drawing.Color.FromArgb(200, 221, 248)
                                        End If
                                End Select


                        End Select

                    Case 5, 10, 11
                        If blnShowTotalCol = True Then
                            For intPeriodCounter = 0 To (intColumnCount - (intFirstNumberColumn + 2))
                                intColNumber = intFirstNumberColumn + intPeriodCounter
                                If intColNumber <= .ColumnCount - 1 Then
                                    .ColumnHeader.Cells(1, intFirstNumberColumn + intPeriodCounter).Text = CommonProcedures.GetMonth(intPeriodCounter, intFirstFiscal)
                                    If intPeriodCounter > intPeriod And intFormat = 5 Then
                                        .ColumnHeader.Cells(1, intFirstNumberColumn + intPeriodCounter).BackColor = Drawing.Color.FromArgb(179, 197, 221)
                                    End If
                                End If
                            Next
                            intColNumber = intFirstNumberColumn + intPeriodCounter
                            If intColNumber <= .ColumnCount - 1 Then
                                .ColumnHeader.Cells(1, intFirstNumberColumn + intPeriodCounter).Text = CommonProcedures.GetMonth(12, intFirstFiscal)
                                If intPeriodCounter - 1 > intPeriod And intFormat = 5 Then
                                    .ColumnHeader.Cells(1, intFirstNumberColumn + intPeriodCounter).BackColor = Drawing.Color.FromArgb(179, 197, 221)
                                End If
                            End If
                        Else
                            For intPeriodCounter = 0 To (intColumnCount - (intFirstNumberColumn + 1))
                                intColNumber = intFirstNumberColumn + intPeriodCounter
                                If intColNumber <= .ColumnCount - 1 Then
                                    .ColumnHeader.Cells(1, intFirstNumberColumn + intPeriodCounter).Text = CommonProcedures.GetMonth(intPeriodCounter, intFirstFiscal)
                                End If
                                If intPeriodCounter > intPeriod And intFormat = 5 Then
                                    .ColumnHeader.Cells(1, intFirstNumberColumn + intPeriodCounter).BackColor = Drawing.Color.FromArgb(179, 197, 221)
                                End If
                            Next
                        End If
                    Case 6, 9 'last 12 months
                        If intPeriod < 11 Then
                            intFirstPeriod = intPeriod - 11
                        End If
                        For intPeriodCounter = intFirstPeriod To intFirstPeriod + 11
                            If intPeriodCounter < 0 Then
                                intColNumber = intFirstNumberColumn + intPeriodCounter + (intFirstPeriod * -1)
                                If intColNumber <= .ColumnCount - 1 Then
                                    .ColumnHeader.Cells(1, intFirstNumberColumn + intPeriodCounter + (intFirstPeriod * -1)).Text = CommonProcedures.GetMonth(intPeriodCounter + 12, intFirstFiscal)
                                End If
                            Else
                                intColNumber = intFirstNumberColumn + intPeriodCounter + (intFirstPeriod * -1)
                                If intColNumber <= .ColumnCount - 1 Then
                                    .ColumnHeader.Cells(1, intFirstNumberColumn + intPeriodCounter + (intFirstPeriod * -1)).Text = CommonProcedures.GetMonth(intPeriodCounter, intFirstFiscal)
                                End If
                            End If
                        Next
                        intColNumber = intFirstNumberColumn + 12
                        If intColNumber <= .ColumnCount - 1 Then
                            .ColumnHeader.Cells(1, intFirstNumberColumn + 12).Text = CommonProcedures.GetMonth(12, intFirstFiscal)
                        End If
                    Case 7, 8

                        For intPeriodCounter = 0 To intColumnCount - (intFirstNumberColumn + 1)
                            intColNumber = 11 - intPeriodCounter
                            If intColNumber <= .ColumnCount - 1 Then
                                .ColumnHeader.Cells(1, 11 - intPeriodCounter).Text = intFirstYear - intPeriodCounter
                            End If
                        Next
                End Select
            End With

            Logger.Log.Info(String.Format("SetColumnHeader method execution Ends"))
        Catch ex As Exception
            Logger.Log.Error(String.Format("SetColumnHeader method execution ends with Error message: {0}, StackTrace: {1}", ex.Message, ex.StackTrace))
        End Try
    End Sub
    Public Sub SetScorecardColumnHeader(ByRef spdAnalytics As FpSpread, ByVal intPeriod As Integer, ByVal intColumnCount As Integer, ByVal intFirstNumberColumn As Integer, intFirstFiscal As Integer, ByVal blnShowTotalCol As Boolean)
        Try
            Logger.Log.Info(String.Format("SetColumnHeader method execution Starts"))
            Dim strHeadingText As String = ""
            Dim strHeadingTextBudget As String = "Budget"
            Dim intPeriodCounter As Integer
            Dim intCurrentYear As Integer = Year(Now)
            Dim intCurrentMonth As Integer = Val(DateTime.Today.ToString("MM")) - 2
            Dim intCurrentYrPeriods As Integer = 11
            Dim intColNumber As Integer = 0
            Dim imagecell As New FarPoint.Web.Spread.ImageCellType
            Dim s As New FarPoint.Web.Spread.StyleInfo
            s.HorizontalAlign = HorizontalAlign.Center
            s.VerticalAlign = VerticalAlign.Middle
            imagecell.ImageUrl = "~\content\images\icon-chart.png"

            spdAnalytics.Sheets(0).ColumnHeaderAutoText = FarPoint.Web.Spread.HeaderAutoText.Blank
            With spdAnalytics.Sheets(0).ColumnHeader
                .RowCount = 1
                .DefaultStyle = s
                .Rows(0).BackColor = System.Drawing.Color.FromArgb(199, 220, 248)
                .Rows(0).Border.BorderStyle = BorderStyle.Solid
                .Rows(0).Border.BorderSize = 0.75
                .Rows(0).Border.BorderColor = Drawing.Color.Gray
                .Rows(0).Font.Size = FontSize.Large
            End With
            spdAnalytics.Sheets(0).ColumnHeaderSpanModel.Add(0, 0, 1, 6)
            With spdAnalytics.Sheets(0)
                If blnShowTotalCol = True Then
                    For intPeriodCounter = 0 To (intColumnCount - (intFirstNumberColumn + 2))
                        intColNumber = intFirstNumberColumn + intPeriodCounter
                        If intColNumber <= .ColumnCount - 1 Then
                            .ColumnHeader.Cells(0, intFirstNumberColumn + intPeriodCounter).Text = CommonProcedures.GetMonth(intPeriodCounter, intFirstFiscal)
                            If intPeriodCounter > intPeriod Then
                                .ColumnHeader.Cells(0, intFirstNumberColumn + intPeriodCounter).BackColor = Drawing.Color.FromArgb(179, 197, 221)
                            End If
                        End If
                    Next
                    intColNumber = intFirstNumberColumn + intPeriodCounter
                    If intColNumber <= .ColumnCount - 1 Then
                        .ColumnHeader.Cells(0, intFirstNumberColumn + intPeriodCounter).Text = CommonProcedures.GetMonth(12, intFirstFiscal)
                        If intPeriodCounter - 1 > intPeriod Then
                            .ColumnHeader.Cells(0, intFirstNumberColumn + intPeriodCounter).BackColor = Drawing.Color.FromArgb(179, 197, 221)
                        End If
                    End If
                Else
                    For intPeriodCounter = 0 To (intColumnCount - (intFirstNumberColumn + 1))
                        intColNumber = intFirstNumberColumn + intPeriodCounter
                        If intColNumber <= .ColumnCount - 1 Then
                            .ColumnHeader.Cells(0, intFirstNumberColumn + intPeriodCounter).Text = CommonProcedures.GetMonth(intPeriodCounter, intFirstFiscal)
                        End If
                        If intPeriodCounter > intPeriod Then
                            .ColumnHeader.Cells(0, intFirstNumberColumn + intPeriodCounter).BackColor = Drawing.Color.FromArgb(179, 197, 221)
                        End If
                    Next
                End If
            End With
            Logger.Log.Info(String.Format("SetColumnHeader method execution Ends"))
        Catch ex As Exception
            Logger.Log.Error(String.Format("SetColumnHeader method execution ends with Error message: {0}, StackTrace: {1}", ex.Message, ex.StackTrace))
        End Try
    End Sub

    Public Sub SetCommonProperties(ByRef spdAnalytics As FpSpread, ByVal intColCount As Integer, ByVal blnShowasPercent As Boolean, ByVal intFirstNumberColumn As Integer, ByVal intNumberColumn As Integer, ByVal intFormat As Integer, ByVal strController As String)
        Try
            Logger.Log.Info(String.Format("SetCommonProperites method execution Starts"))
            '     spdAnalytics.BorderColor = Drawing.Color.White
            spdAnalytics.BorderStyle = BorderStyle.None
            spdAnalytics.HorizontalScrollBarPolicy = FarPoint.Web.Spread.ScrollBarPolicy.Never
            spdAnalytics.VerticalScrollBarPolicy = FarPoint.Web.Spread.ScrollBarPolicy.AsNeeded
            spdAnalytics.Sheets(0).GridLineColor = Drawing.Color.LightGray
            spdAnalytics.CommandBar.Visible = False
            With spdAnalytics.Sheets(0)
                .Columns(-1).Resizable = False
                .GridLineColor = Drawing.Color.LightGray
                .DefaultStyle.Font.Size = FontSize.Large
                .DefaultStyle.ForeColor = Drawing.Color.Black
                .RowHeader.Visible = False
                .SelectionBackColorStyle = FarPoint.Web.Spread.SelectionBackColorStyles.None
                .ColumnCount = intColCount
                .RowCount = 1
                'set row properties
                .Rows(-1).VerticalAlign = VerticalAlign.Bottom
                'lock cells
                .Columns(0).Locked = True
                .Columns(0).Width = 20
                .Columns(0).HorizontalAlign = HorizontalAlign.Center
                .Columns(intRowTypeCol, intColCount - 1).Locked = True
                'set cell types for number cols
                Dim generalCellType As New FarPoint.Web.Spread.GeneralCellType
                Dim numberFormatInfo As New System.Globalization.NumberFormatInfo
                numberFormatInfo.NumberDecimalDigits = 0
                generalCellType.NumberFormat = numberFormatInfo
                Dim pfi As New FarPoint.Web.Spread.PercentCellType
                pfi.DecimalDigits = 2
                If blnShowasPercent = False Then
                    Select Case intFormat
                        Case 0, 3
                            .Columns(intFirstNumberColumn, intFirstNumberColumn + 2).CellType = generalCellType
                            .Columns(intFirstNumberColumn + 3).CellType = pfi
                            Select Case strController
                                Case "RE", "CF", "OM"
                                    .Columns(intFirstNumberColumn + 4, intFirstNumberColumn + 6).CellType = generalCellType
                                    .Columns(intFirstNumberColumn + 7).CellType = pfi
                            End Select
                        Case 1
                            .Columns(intFirstNumberColumn, intFirstNumberColumn + 2).CellType = generalCellType
                            .Columns(intFirstNumberColumn + intNumberColumn, intColCount - 1).CellType = pfi
                        Case Else
                            .Columns(intFirstNumberColumn, intFirstNumberColumn + intNumberColumn - 1).CellType = generalCellType

                    End Select
                Else
                    Select Case intFormat
                        Case 0, 3
                            Select Case strController
                                Case "RE", "CF", "OM"
                                    .Columns(intFirstNumberColumn, intFirstNumberColumn + 7).CellType = pfi
                                Case Else
                                    .Columns(intFirstNumberColumn, intFirstNumberColumn + 3).CellType = pfi
                            End Select
                        Case Else
                            .Columns(intFirstNumberColumn, intFirstNumberColumn + intNumberColumn - 1).CellType = pfi
                    End Select

                End If
                .Columns(intFirstNumberColumn, intColCount - 1).HorizontalAlign = HorizontalAlign.Right

            End With


            Logger.Log.Info(String.Format("SetCommonProperties method execution Ends"))
        Catch ex As Exception
            Logger.Log.Error(String.Format("SetCommonProperties method execution ends with Error message: {0}, StackTrace: {1}", ex.Message, ex.StackTrace))
        End Try
    End Sub

    Public Sub SetColumnWidths(ByRef spdAnalytics As FpSpread, ByVal intColCount As Integer, ByVal intFirstNumberColumn As Integer, ByVal intFormat As Integer, ByVal intNumberColumn As Integer, ByVal blnSTColVisible As Boolean, ByVal intDescCol As Integer, ByVal strController As String)
        Dim intColWidth As Integer
        Dim intDescColWidth As Integer
        Dim intSpdWidth As Integer
        Dim intScrollBarWidth = 18
        Dim intAddWidth As Integer = 44
        Dim intCtr As Integer
        If blnSTColVisible = True Then
            intAddWidth = 66
        End If
        Try
            With spdAnalytics.Sheets(0)
                Logger.Log.Info(String.Format("SetColumnWidths method execution Starts"))
                .Columns(0, 2).Width = 22
                .Columns(intRowTypeCol).Width = 0
                If intFirstNumberColumn = 6 Then
                    .Columns(5).Width = 0
                End If
                Select Case intFormat
                    Case 0, 3
                        .Columns(intDescCol).Width = 250 + 22
                        .Columns(intFirstNumberColumn, intColCount - 1).Width = 85
                        Select Case strController
                            Case "RE", "CF", "OM"
                                intSpdWidth = intAddWidth + 250 + 680 + intScrollBarWidth
                            Case Else
                                intSpdWidth = intAddWidth + 250 + 340 + intScrollBarWidth
                        End Select

                    Case 2
                        .Columns(intDescCol).Width = 250 + 22
                        .Columns(intFirstNumberColumn, intColCount - 1).Width = 85
                        intSpdWidth = intAddWidth + 250 + 340 + intScrollBarWidth
                    Case 5
                        Select Case intNumberColumn
                            Case Is <= 4
                                intDescColWidth = 250
                                intColWidth = 85
                            Case 5 To 8
                                intDescColWidth = 200
                                intColWidth = Math.Round((780 - 200) / intNumberColumn, 0)
                                If intColWidth > 85 Then
                                    intColWidth = 85
                                End If
                            Case Else
                                intDescColWidth = 150
                                intColWidth = 65
                        End Select
                        .Columns(intDescCol).Width = intDescColWidth + 22
                        .Columns(intFirstNumberColumn, intColCount - 1).Width = intColWidth
                        intSpdWidth = intAddWidth + intDescColWidth + (intColWidth * intNumberColumn) + intScrollBarWidth
                    Case 6, 9, 10, 11
                        .Columns(intDescCol).Width = 150 + 22
                        .Columns(intFirstNumberColumn, intColCount - 1).Width = 65
                        intColWidth = 65
                        intSpdWidth = intAddWidth + 150 + (intColWidth * intNumberColumn) + intScrollBarWidth
                    Case 7, 8
                        intDescColWidth = 250
                        .Columns(intDescCol).Width = intDescColWidth + 22
                        Select Case intNumberColumn
                            Case Is <= 4
                                intColWidth = 95
                            Case 5
                                intColWidth = 80
                            Case 6
                                intColWidth = 65
                        End Select
                        For intCtr = 0 To 5
                            If .Columns(intCtr + intFirstNumberColumn).Visible = True Then
                                .Columns(intFirstNumberColumn + intCtr).Width = intColWidth
                            End If
                        Next
                        intSpdWidth = intAddWidth + intDescColWidth + (intColWidth * intNumberColumn) + intScrollBarWidth + 10
                End Select
                spdAnalytics.Width = intSpdWidth
                'hide last row
                .Rows(.RowCount - 1).Visible = False
                'hide checkbox col
                .Columns(2).Width = 0
                .ActiveColumn = intRowTypeCol
                .ActiveRow = 1
                spdAnalytics.Height = 620
                Logger.Log.Info(String.Format("SetColumnWidths method execution Ends"))
            End With

        Catch ex As Exception
            Logger.Log.Error(String.Format("SetColumnWidths method execution ends with Error message: {0}, StackTrace: {1}", ex.Message, ex.StackTrace))
        End Try
    End Sub

    Public Function CheckBalanceSize(ByRef decChartAmts(,) As Decimal, intArrayDem As Integer, intPeriods As Integer) As Boolean
        Dim intCtr As Integer
        Dim intPeriodCtr As Integer
        CheckBalanceSize = False
        For intCtr = 0 To intArrayDem
            For intPeriodCtr = 0 To intPeriods
                If decChartAmts(intCtr, intPeriodCtr) > 1000000 Then
                    CheckBalanceSize = True
                    Exit For
                End If
            Next
        Next
        For intCtr = 0 To intArrayDem
            If CheckBalanceSize = True Then
                For intPeriodCtr = 0 To intPeriods
                    decChartAmts(intCtr, intPeriodCtr) = Math.Round(decChartAmts(intCtr, intPeriodCtr) / 1000, 0)
                Next
            End If
        Next

    End Function

    Public Function ConvertFormat(ByVal intDropDownSel As Integer, ByRef blnShowBudget As Boolean) As Integer
        Select Case intDropDownSel
            Case 0 To 5
                ConvertFormat = intDropDownSel
            Case 6
                ConvertFormat = 5
                blnShowBudget = True
            Case 7
                ConvertFormat = 11
            Case 8
                ConvertFormat = 9
            Case 9
                ConvertFormat = 6
            Case 10
                ConvertFormat = 7
            Case 11
                ConvertFormat = 8
            Case 12
                ConvertFormat = 10

        End Select

    End Function

    Public Function FormatIDtoSave(ByVal intDropDownSel As Integer) As Integer
        Select Case intDropDownSel
            Case 7
                FormatIDtoSave = 6
            Case 8 'current year budget
                FormatIDtoSave = 13
            Case 9 'rolling forecast
                FormatIDtoSave = 10
            Case 10 'last 12 months
                FormatIDtoSave = 7
            Case 11 'multi year
                FormatIDtoSave = 8
            Case 12 'ytd multi
                FormatIDtoSave = 9
            Case 13 'historical period
                FormatIDtoSave = 12
            Case Else
                FormatIDtoSave = intDropDownSel
        End Select
    End Function

    Public Function ConvertSavedFormatID(ByVal intSavedFormatID As Integer, ByVal blnShowBudget As Boolean) As Integer
        Select Case intSavedFormatID
            Case 6
                If blnShowBudget = True Then
                    ConvertSavedFormatID = 7
                Else
                    ConvertSavedFormatID = 6
                End If
            Case 7
                ConvertSavedFormatID = 10
            Case 8
                ConvertSavedFormatID = 11
            Case 9
                ConvertSavedFormatID = 12
            Case 10
                ConvertSavedFormatID = 9
            Case 12
                ConvertSavedFormatID = 13
            Case 13
                ConvertSavedFormatID = 8
            Case Else
                ConvertSavedFormatID = intSavedFormatID
        End Select

    End Function

End Module
