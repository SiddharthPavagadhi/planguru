﻿@ModelType onlineAnalytics.Dashboard
@Imports Farpoint.Web.Spread
@Imports Farpoint.Mvc.Spread
@Imports  onlineAnalytics
@Code
    ViewData("Title") = "PlanGuru Analytics | Add Dashboard Item"
    If Not Session("action") Is Nothing Then
        
        If (Session("action") = "SetDefaultAnalysis" or Session("action") = "GetAnalyses") Then
            Layout = Nothing
            Session.Remove("action")
        End If
    Else
        Layout = "~/Views/Shared/AnalyticsMaster.vbhtml"
    End If
    
    Dim ua As New UserAccess
    If Not (Session("UserAccess") Is Nothing) Then
        ua = DirectCast(Session("UserAccess"), UserAccess)
    End If
End Code
<script src="@Url.Content("~/Scripts/jquery.validate.min.js")" type="text/javascript"></script>
<script src="@Url.Content("~/Scripts/jquery.validate.unobtrusive.min.js")" type="text/javascript"></script>
<div class="headerTable" style="float: right">
    <table>
        <tr>
            <td align="left" width="70%">
                <div class="breadscrum"><a href='@Url.Action("Index", "Dashboard")'>Dashboard</a><span> > </span><a href='@Url.Action("Index", "DashboardSetup")'>Dashboard Item List</a><span> > </span>Add Dashboard Item</div>                
            </td>                        
        </tr>
    </table>
</div>
<div style="margin-bottom: 20px;">    
    @If Not (DirectCast(TempData("Message"), String) Is Nothing) Then
        @<label class="success">@TempData("Message").ToString()
        </label>                         
    End If
    @If Not (DirectCast(TempData("ErrorMessage"), String) Is Nothing) Then
        @<label class="error">@TempData("ErrorMessage").ToString()
        </label>                         
    End If    
    <div style="font-size: 13px; display: -moz-box; width: 99.9%; border: 1px solid #AAAAAA;">
        @Using Html.BeginForm("Create", "DashboardSetup")
            @Html.AntiForgeryToken()   
            @Html.ValidationSummary(True)       
            @<div>
                @Html.ValidationMessageFor(Function(m) m.AccountId, Nothing, New With {.style = "margin-left:20px;font-family:Arial;font-size: 0.85em;"})
                @Html.HiddenFor(Function(m) m.AccountId)
                 @Html.HiddenFor(Function(m) m.AcctTypeId)
        @Html.HiddenFor(Function(m) m.AccountIdList)
        @Html.HiddenFor(Function(m) m.AccountTypeIdList)
            </div>
            @<div id="tabs" style="float: left; font-size:11px; width: 77%; min-height: 450px; height: auto;">
                <ul>
                    <li><a href="#tabs-1">Revenue and expenses</a></li>
                    <li><a href="#tabs-2">Balance Sheet</a></li>
                    <li><a href="#tabs-3">Cash flow</a></li>
                    <li><a href="#tabs-4">Financial ratios</a></li>
                    <li><a href="#tabs-5">Non-financial data</a></li>
                </ul>
                <div id="tabs-1">
                    <div id="REspread">
                        @Html.FpSpread("spdRE", Sub(x)
                                                    x.RowHeader.Visible = False
                                                    x.Height = 380
                                                    x.Width = 800
                                              
                                              
                                                End Sub)
                    </div>
                </div>
                <div id="tabs-2">
                    <div id="BSspread">
                        @Html.FpSpread("spdBS", Sub(x)
                                                    x.RowHeader.Visible = False
                                                    x.Height = 380
                                                    x.Width = 800
                                              
                                                End Sub)
                    </div>
                </div>
                <div id="tabs-3">
                    <div id="CFspread">
                        @Html.FpSpread("spdCF", Sub(x)
                                                    x.RowHeader.Visible = False
                                                    x.Height = 380
                                                    x.Width = 800                                              
                                                End Sub)
                    </div>
                </div>
                <div id="tabs-4">
                    <div id="FRspread">                    
                        @Html.FpSpread("spdFR", Sub(x)
                                                         x.RowHeader.Visible = False
                                                         x.Height = 380
                                                         x.Width = 800
                                                     End Sub)
                    </div>
                </div>
                <div id="tabs-5">
                    <div id="OMspread">                    
                        @Html.FpSpread("spdOM", Sub(x)
                                                    x.RowHeader.Visible = False
                                                    x.Height = 380
                                                    x.Width = 800                                              
                                                End Sub)
                    </div>
                </div>
            </div>
            @<div style="float: right; width: 22%;">
                <fieldset style="min-height: 441px; height: auto; border: none;">
                    <div style="float: right;">
                        <ol class="round">
                            <li>
                                @Html.LabelFor(Function(m) m.ChartTypeId)<br />
                                @Html.DropDownList("ChartTypeId", DirectCast(ViewBag.ChartType, SelectList), "Select Chart Type", New With {.Class = "select"})<br />
                                @Html.ValidationMessageFor(Function(m) m.ChartTypeId)
                            </li> 
                           
                            <li>
                                @Html.LabelFor(Function(m) m.ChartFormatId)<br />
                                @Html.DropDownList("ChartFormatId", DirectCast(ViewBag.Formats, SelectList), "Select Chart Format", New With {.Class = "select", .style = "width:200px;"})<br />
                                @Html.ValidationMessageFor(Function(m) m.ChartFormatId)
                            </li>
                            <li id="ChartFormatTypeLi">
                                @Html.LabelFor(Function(m) m.ChartFormatTypeId)<br />
                                @Html.DropDownList("ChartFormatTypeId", DirectCast(ViewBag.ChartFormatType, SelectList), "Select Chart Format Type", New With {.Class = "select"})<br />
                                @Html.ValidationMessageFor(Function(m) m.ChartFormatTypeId)
                            </li>            
                            <li>
                                @Html.LabelFor(Function(m) m.DashDescription)<br />
                                @Html.TextBoxFor(Function(m) m.DashDescription, New With {.Class = "select"})<br />
                                @Html.ValidationMessageFor(Function(m) m.DashDescription)
                            </li>              
                            <li id="ShowPercent">
                                @Html.LabelFor(Function(m) m.ShowAsPercent)
                                @Html.CheckBoxFor(Function(m) m.ShowAsPercent, New With {.class = "styled_checkbox"})<br />                                
                            </li>
                            <li id="ShowTrend">
                                @Html.LabelFor(Function(m) m.ShowTrendline)
                                @Html.CheckBoxFor(Function(m) m.ShowTrendline, New With {.class = "styled_checkbox"})<br />                                
                            </li>
                         @*   <li id="Show_Budget">
                                @Html.LabelFor(Function(m) m.ShowBudget)
                                @Html.CheckBoxFor(Function(m) m.ShowBudget, New With {.class = "styled_checkbox"})<br />                                
                            </li>      *@                       
                            <li>
                                <input id="causepost" type="submit" value="Add Dashboard Item" class="secondbutton_example" onclick="buttonSubmitClicked(event)" />
                            </li>
                        </ol>
                    </div>
                </fieldset>
                @* <div id="ShowTrend" style="margin-top: 10px; margin-left: 10px;">
                        @Html.CheckBox("blnShowTrend", New With {.Style = "border: 1px solid Silver; background-color:Silver; margin-right: 5px;"})
                        @Html.Label("Show trend")
                    </div><div id="Smoothed" style="margin-top: 10px; margin-left: 10px;">
                        @Html.CheckBox("blnSmoothed", New With {.Style = "border: 1px solid Silver; background-color:Silver; margin-right: 5px;"})
                        @Html.Label("Smooth line")
                    </div>
                    <div id="AddDashboardItem" style="float: left;">
                        <input id="causepost" type="submit" value="Add Dashboard Item" class="secondbutton_example" />
                    </div>*@
            </div>            
        End Using        
    </div>
</div>
<script type="text/javascript">

    var selectedElement = "";
    var selectedElementDescription = "";
    var checkboxCount = 0;
    var accountidlist = "";
    var accounttypeidlist = "";
    var DashboardDescription = "";
   
    $(".success").attr("style", "margin:0;padding: 10px 20px 10px 50px;");
    $(".error").attr("style", "margin:0;padding: 10px 20px 10px 50px;");

   
    function DSChkBoxClicked(checkedElement, selectedAccount, selectedAcctType,Description) {        
        var chkElement = "#" + checkedElement
        
        if ($(chkElement).is(':checked')) {
            if ($("select#ChartTypeId").val() == "") {
                $.msgBox({
                    title: "Select Chart Type",
                    content: "Please select chart type.",
                    type: "alert",
                    modal: true,
                    buttons: [{ value: "Ok" }],
                    success: function (result) {
                        if (result == "Ok") {
                            $(chkElement).attr("checked", false);
                        }
                    }
                });
               
            }
            else {
                Checked(chkElement, selectedAccount, selectedAcctType, Description);
                checkboxCount += 1;
            }
        }
        else {
              
            if ((accountidlist.indexOf(',') != -1) && (accounttypeidlist.indexOf(',') != -1)) {
                accountidlist = removeValue(accountidlist, selectedAccount);
                accounttypeidlist = removeValue(accounttypeidlist, selectedAcctType);
                $("#AccountIdList").val(accountidlist);
                $("#AccountTypeIdList").val(accounttypeidlist);
                if ($("select#ChartTypeId").val() == "2" || $("select#ChartTypeId").val() == "4" || $("select#ChartTypeId").val() == "5") {
                    $("#DashDescription").val(DashboardDescription.trim());
                }

               
              
            }
            else if (accountidlist != "" && accounttypeidlist != "")
            {
                accountidlist = "";
                accounttypeidlist = "";
                DashboardDescription = "";
                $("#DashDescription").val("");
                selectedElement = "";
                $("#AccountId").val('');
                $("#AcctTypeId").val('');
            }
            checkboxCount -= 1;

            if (($('#ChartTypeId option:selected').text().toLowerCase() == "line") && ($('#ChartFormatId option:selected').text().toLowerCase() != "budget vs actual" && $('#ChartFormatId option:selected').text().toLowerCase() != "budget vs actual ytd" && $('#ChartFormatId option:selected').text().toLowerCase() != "this period vs last period" && $('#ChartFormatId option:selected').text().toLowerCase() != "this period vs same period prior year" && $('#ChartFormatId option:selected').text().toLowerCase() != "this period vs same period prior year ytd" && $('#ChartFormatId option:selected').text().toLowerCase() != "select chart format") && (checkboxCount == 1 || checkboxCount == 0)) {
                $('#ShowTrend').show();
            }
            else if (checkboxCount > 1) {
                $('#ShowTrend').hide();
                $('#ShowTrendline').attr('checked', false);
            }
       }
    }

    function Checked(chkElement, selectedAccount, selectedAcctType, Description) {
        if ($("select#ChartTypeId").val() == "2" || $("select#ChartTypeId").val() == "4" || $("select#ChartTypeId").val() == "5") {
            if (($("#AccountId").val() == "" || $("#AccountId").val() == 0) && checkboxCount == 0) {

                selectedElement = chkElement;
                selectedElementDescription = Description;
                $("#AccountId").val(selectedAccount);
                $("#AcctTypeId").val(selectedAcctType);
                accountidlist = String(selectedAccount);
                accounttypeidlist = String(selectedAcctType);
                $("#AccountIdList").val(accountidlist);
                $("#AccountTypeIdList").val(accounttypeidlist);
                DashboardDescription = Description;
                $("#DashDescription").val(DashboardDescription.trim());
            }
            else if (checkboxCount == 1) {
                accountidlist = accountidlist + ',' + selectedAccount;
                accounttypeidlist = accounttypeidlist + ',' + selectedAcctType;
                $("#AccountIdList").val(accountidlist);
                $("#AccountTypeIdList").val(accounttypeidlist);
                DashboardDescription = Description;
                $("#DashDescription").val("");
                $('#ShowTrend').hide();
                $('#ShowTrendline').attr('checked', false);
            }
            else {

                $.msgBox({
                    title: "Maximum Two Items",
                    content: "You can select maximum two items.",
                    type: "alert",
                    modal: true,
                    buttons: [{ value: "Ok" }],
                    success: function (result) {
                        if (result == "Ok") {
                            $(chkElement).attr("checked", false);
                            checkboxCount -= 1;
                        }
                    }
                });
                $("#DashDescription").val("");
            }
        }
        else if($("select#ChartTypeId").val() == "6" || $("select#ChartTypeId").val() == "7")
        {
            if (($("#AccountId").val() == "" || $("#AccountId").val() == 0) && checkboxCount == 0) {

                selectedElement = chkElement;
                selectedElementDescription = Description;
                $("#AccountId").val(selectedAccount);
                $("#AcctTypeId").val(selectedAcctType);
                accountidlist = String(selectedAccount);
                accounttypeidlist = String(selectedAcctType);
                $("#AccountIdList").val(accountidlist);
                $("#AccountTypeIdList").val(accounttypeidlist);
                DashboardDescription = Description;
                $("#DashDescription").val(DashboardDescription.trim());
            }
            else if (checkboxCount < 15) {
                accountidlist = accountidlist + ',' + selectedAccount;
                accounttypeidlist = accounttypeidlist + ',' + selectedAcctType;
                $("#AccountIdList").val(accountidlist);
                $("#AccountTypeIdList").val(accounttypeidlist);
                DashboardDescription = Description;
                $("#DashDescription").val("");
                $('#ShowTrend').hide();
                $('#ShowTrendline').attr('checked', false);
            }
            else {

                $.msgBox({
                    title: "Maximum Fifteen Items",
                    content: "You can select maximum fifteen items.",
                    type: "alert",
                    modal: true,
                    buttons: [{ value: "Ok" }],
                    success: function (result) {
                        if (result == "Ok") {
                            $(chkElement).attr("checked", false);
                            checkboxCount -= 1;
                        }
                    }
                });
                $("#DashDescription").val("");
            }
        }
    }

    $(function () {

        $('#Show_Budget').hide();
        $('#ShowBudget').prop("checked", true);

        $("#tabs").tabs();
        $("#tabs").attr("class", "ui-tabs ui-widget");

        $(document).ready(function () {
           
            CallShowTrend();

            HideShowChartFormat();

            $('#ChartTypeId').change(function () {
                $('#ChartFormatId').val('')
                $('#ChartFormatTypeId').val('1')
                CallShowTrend();
                HideShowChartFormat();
            });

            function CallShowTrend() {
                var selectedvalue = $('#ChartTypeId option:selected').text().toLowerCase();
                var selectedValueOfChartFormat = $('#ChartFormatId option:selected').text().toLowerCase();
                if (selectedvalue == "select chart type" && selectedValueOfChartFormat == "select chart format") {
                    $('#ShowTrend').hide();
                    $('#ShowTrendline').attr('checked', false);
                    $("#ShowPercent").hide();
                }
                else {
                    if ((selectedvalue == "line") && (selectedValueOfChartFormat != "budget vs actual" && selectedValueOfChartFormat != "budget vs actual ytd" && selectedValueOfChartFormat != "this period vs last period" && selectedValueOfChartFormat != "this period vs same period prior year" && selectedValueOfChartFormat != "this period vs same period prior year ytd" && selectedValueOfChartFormat != "select chart format") && (checkboxCount == 1 || checkboxCount == 0)) {
                        $('#ShowTrend').show();
                    }
                    else if ((checkboxCount > 1) || (selectedvalue != "line") || (selectedValueOfChartFormat == "budget vs actual") || (selectedValueOfChartFormat == "budget vs actual ytd") || (selectedValueOfChartFormat == "this period vs last period") || (selectedValueOfChartFormat == "this period vs same period prior year") || (selectedValueOfChartFormat == "this period vs same period prior year ytd") || (selectedValueOfChartFormat == "select chart format")) {
                        $('#ShowTrend').hide();
                        $('#ShowTrendline').attr('checked', false);
                    }

                    if ($("select#ChartTypeId").val() == "6" || $("select#ChartTypeId").val() == "7") {
                        $("#ChartFormatTypeLi").show();
                        $("#ShowPercent").hide();
                        $('#ShowTrend').hide();
                        $('#ShowTrendline').attr('checked', false);
                        $('#ShowAsPercent').attr('checked', false);

                    }
                    else {
                        $("#ChartFormatTypeLi").hide();
                        $("select#ChartFormatTypeId").val("1");
                        $("#ShowPercent").show();
                    }

                    if (($("select#ChartTypeId").val() == "2" || $("select#ChartTypeId").val() == "4" || $("select#ChartTypeId").val() == "5") && checkboxCount > 2) {
                        $('[id^=checkbox]').attr("checked", false)
                        checkboxCount = 0;
                        accountidlist = "";
                        accounttypeidlist = "";
                        DashboardDescription = "";
                        $("#AccountId").val("");
                        $("#AcctTypeId").val("");
                        $("#AccountIdList").val("");
                        $("#AccountTypeIdList").val("");
                    }
                }
            }

            function HideShowChartFormat() {
                if($("select#ChartTypeId").val() == "2" || $("select#ChartTypeId").val() == "4" || $("select#ChartTypeId").val() == "5" || $("select#ChartTypeId").val() == "")
                {
                    $("select#ChartFormatId option[value = '14']").hide();
                    $("select#ChartFormatId option[value = '15']").hide();
                    $("select#ChartFormatId option[value = '16']").hide();
                    $("select#ChartFormatId option[value = '17']").hide();
                    $("select#ChartFormatId option[value = '18']").hide();
                    $("select#ChartFormatId option[value = '19']").hide();
                    $("select#ChartFormatId option[value = '1']").show();
                    $("select#ChartFormatId option[value = '2']").show();
                    $("select#ChartFormatId option[value = '3']").show();
                    $("select#ChartFormatId option[value = '4']").show();
                    $("select#ChartFormatId option[value = '5']").show();
                    $("select#ChartFormatId option[value = '6']").show();
                    $("select#ChartFormatId option[value = '7']").show();
                    $("select#ChartFormatId option[value = '8']").show();
                    $("select#ChartFormatId option[value = '9']").show();
                    $("select#ChartFormatId option[value = '10']").show();
                    $("select#ChartFormatId option[value = '11']").show();
                    $("select#ChartFormatId option[value = '12']").show();
                }
                else
                {
                    $("select#ChartFormatId option[value = '14']").show();
                    $("select#ChartFormatId option[value = '15']").show();
                    $("select#ChartFormatId option[value = '16']").show();
                    $("select#ChartFormatId option[value = '17']").show();
                    $("select#ChartFormatId option[value = '18']").show();
                    $("select#ChartFormatId option[value = '19']").show();
                    $("select#ChartFormatId option[value = '1']").hide();
                    $("select#ChartFormatId option[value = '2']").hide();
                    $("select#ChartFormatId option[value = '3']").hide();
                    $("select#ChartFormatId option[value = '4']").hide();
                    $("select#ChartFormatId option[value = '5']").hide();
                    $("select#ChartFormatId option[value = '6']").hide();
                    $("select#ChartFormatId option[value = '7']").hide();
                    $("select#ChartFormatId option[value = '8']").hide();
                    $("select#ChartFormatId option[value = '9']").hide();
                    $("select#ChartFormatId option[value = '10']").show();
                    $("select#ChartFormatId option[value = '11']").hide();
                    $("select#ChartFormatId option[value = '12']").hide();
                }
            }

            CallShowBudget();

            $("#ChartFormatId").change(function () {
                CallShowBudget();
            });

            function CallShowBudget() {
                var selectedFormat = $("#ChartFormatId").val();
                if (selectedFormat == 6) {
                    $('#Show_Budget').show();
                    $('#ShowBudget').prop("checked", true);
                }
                else {
                    $('#ShowBudget').prop("checked", true);
                    $('#Show_Budget').hide();
                }

                CallShowTrend();
               

                if(selectedFormat == "18" || selectedFormat == "19" || selectedFormat == "10" )
                {
                    $("#ChartFormatTypeLi").hide();
                    $("select#ChartFormatTypeId").val("1");
                }
            }

            CallShowPercent();

            $('#tabs').tabs({
                activate: function (event, ui) {
                    CallShowPercent();
                }
            });

            function CallShowPercent() {
                var activeTab = $("#tabs").tabs("option", "active");
                if (activeTab == 0 || activeTab == 1 )
                {
                    if (($('#ChartTypeId option:selected').text().toLowerCase() == "select chart type" && $('#ChartFormatId option:selected').text().toLowerCase() == "select chart format") || ($('#ChartTypeId option:selected').text().toLowerCase() == "donut") || ($('#ChartTypeId option:selected').text().toLowerCase() == "pie")) {
                        $("#ShowPercent").hide();
                    }
                    else {
                        $("#ShowPercent").show();
                    }
                }
                else {
                    $("#ShowPercent").hide();
                }
            }

            $("div > input:checkbox").each(function (index) {
                $("<label>").attr("for", this.id = "checkbox" + index)
                .attr("class", "styled_checkbox dashboard")
                .insertAfter(this);
            });

            $("li > input:checkbox").each(function (index) {
                $("<label>").attr("for", this.id)
                .attr("class", "float-left chk-dashboard")
                .insertAfter(this);
            });

            $("#tabs").removeClass("ui-corner-all");
            $("#footerpanel").removeClass("plangurufooter").addClass("plangurufooterfordashboard");
            IsSessionAlive();
            //hide detail rows  
            var spread = document.getElementById("spdRE");
            setRow(spread);

            var spread = document.getElementById("spdBS");
            setRow(spread);

            var spread = document.getElementById("spdCF");
            setRow(spread);

            //            var spread = document.getElementById("spdFR");
            //            setRow(spread);

            var spread = document.getElementById("spdOM");
            setRow(spread);

        });

        function setRow(spread) {

            var rc = spread.GetRowCount();
            var showclass = false
            for (var i = 0; i < rc; i++) {
                var cellval = spread.GetValue(i, 2);
                if (cellval == "Detail" || cellval == "SDetail" || cellval == "SHeading" || cellval == "STotal") {

                    if (spread.id == "spdOM") {
                        showclass = true
                        spread.Rows(i).style.display = "table-row";
                    }
                    else if (showclass == false) {
                        spread.Rows(i).style.display = "none";
                    }
                }
                if (cellval == "OpCash") {
                    if (showclass == false) {
                        spread.Rows(i).style.display = "none";
                    }
                }
                if (cellval == "Heading") {
                    var cellvalue = spread.GetValue(i, 0);
                    if (cellvalue == "-") {
                        showclass = true
                        spread.Rows(i).style.display = "table-row";
                    }
                    else {
                        showclass = false
                        spread.Rows(i).style.display = "none";
                    }
                }
            }
        }

        window.onload = function () {
            $(".wrapper").show();
            var spreadRE = document.getElementById("spdRE");
            var spreadBS = document.getElementById("spdBS");
            var spreadCF = document.getElementById("spdCF");
            var spreadFR = document.getElementById("spdFR");
            var spreadOM = document.getElementById("spdOM");

            tabEventListener(spreadRE);
            tabEventListener(spreadBS);
            tabEventListener(spreadCF);
            tabEventListener(spreadFR);
            tabEventListener(spreadOM);
        }

        function tabEventListener(spread) {
            if (document.all) {
                if (spread.addEventListener) {
                    spread.addEventListener("ActiveCellChanged", cellChanged, false);
                }
                else {
                    spread.onActiveCellChanged = cellChanged;
                }
            }
            else {
                spread.addEventListener("ActiveCellChanged", cellChanged, false);
            }
        }

        function cellChanged(event) {
            //IsSessionAlive();
            if (event.col == 0) {

                var spread = document.getElementById(event.target.id);  //document.getElementById("spdRE");
                var cellval = spread.GetValue(event.row, 0);
                var rc = spread.GetRowCount();

                if (cellval == "-") {
                    for (var i = event.row; i < rc; i++) {
                        var rowtype = spread.GetValue(i, 2);
                        if (rowtype == "Total") {
                            cell = document.getElementById(event.target.id).GetCellByRowCol(i, 0);
                            cell.removeAttribute("FpCellType");
                            spread.SetValue(i, 0, "+", false);
                            cell.setAttribute("FpCellType", "readonly");
                            spread.Rows(i).style.display = "table-row";
                            break;
                        }
                        else if (rowtype == "Heading") {
                            cell = document.getElementById(event.target.id).GetCellByRowCol(i, 0);
                            cell.removeAttribute("FpCellType");
                            spread.SetValue(i, 0, "+", false);
                            cell.setAttribute("FpCellType", "readonly");
                            spread.Rows(i).style.display = "none";
                        }
                        else {

                            spread.Rows(i).style.display = "none";
                        }
                    }
                }
                else if (cellval == "+") {
                    for (var i = event.row; i > -1; i--) {
                        var rowtype = spread.GetValue(i, 2);
                        if (rowtype == "Total") {
                            cell = document.getElementById(event.target.id).GetCellByRowCol(i, 0);
                            cell.removeAttribute("FpCellType");
                            spread.SetValue(i, 0, "", false);
                            cell.setAttribute("FpCellType", "readonly");
                            if ($('#blnUseFilter').is(':checked')) {
                                spread.Rows(i).style.display = "none";
                            }
                        }
                        else if (rowtype == "Heading") {
                            cell = document.getElementById(event.target.id).GetCellByRowCol(i, 0);
                            cell.removeAttribute("FpCellType");
                            spread.SetValue(i, 0, "-", false);
                            cell.setAttribute("FpCellType", "readonly");
                            spread.Rows(i).style.display = "table-row";
                            break
                        }
                        else if (rowtype == "SHeading") {
                            spread.Rows(i).style.display = "none";
                        }
                        else {
                            spread.Rows(i).style.display = "table-row";
                        }
                    }
                }
                spread.SetActiveCell(event.row, 1)
            }
        }

        $("#ChartFormatTypeLi").hide();

    });

    var removeValue = function (list, value) {
        separator = ",";
        var values = list.split(separator);
        for (var i = 0 ; i < values.length ; i++) {
            if (values[i] == value) {
                values.splice(i, 1);
                return values.join(separator);
            }
        }
        return list;
    }

    $(window).load(function () {
        $('[for^=spd]').hide()
        $("select#ChartFormatTypeId option:first-child").val(1);
    });

    function buttonSubmitClicked(event)
    {
        if (($("select#ChartTypeId").val() == "6" || $("select#ChartTypeId").val() == "7") && ($("#ChartFormatId").val() == "14" || $("#ChartFormatId").val() == "15" || $("#ChartFormatId").val() == "16" || $("#ChartFormatId").val() == "17") && ($("select#ChartFormatTypeId").val() == "1"))
        {
            event.preventDefault();
            $.msgBox({
                title: "Chart Format Type",
                content: "Please select chart format type.",
                type: "alert",
                modal: true,
                buttons: [{ value: "Ok" }]
            });
        }
    }
</script>
