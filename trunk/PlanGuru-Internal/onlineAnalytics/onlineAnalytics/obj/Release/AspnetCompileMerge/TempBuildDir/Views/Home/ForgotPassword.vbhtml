﻿@ModelType  onlineAnalytics.ForgotPassword
@Code
    Layout = Nothing
End Code

<!DOCTYPE html>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Forget User Name-Panguru Subscription</title>
    <link href="../Content/Site.css" rel="stylesheet" />
    <link href="../Content/css/newstyle.css" rel="stylesheet" />
    <script type="text/javascript" language="javascript">
        var msgBoxImagePath = '@Url.Content("~/Content/MsgBoxImages/")';
    </script>
    <link href="@Url.Content("~/Content/msgBoxLight.css")" rel="stylesheet" type="text/css" />
    <script src="@Url.Content("~/Scripts/jquery-1.9.1.min.js")" type="text/javascript"></script>
    <script src="@Url.Content("~/Scripts/modernizr-1.7.min.js")" type="text/javascript"></script>
    <script src="@Url.Content("~/Scripts/jquery-ui-1.10.0.custom.js")" type="text/javascript"></script>
    <script src="@Url.Content("~/Scripts/jquery.msgBox.js")" type="text/javascript"></script>
    <style>
        #login:hover {
            cursor: pointer;
        }

        .forgotUsername, .forgotPassword {
            margin-left: -4px;
            padding-top: 3px;
            height: 22px;
        }

            .forgotUsername:hover, .forgotPassword:hover {
                cursor: pointer;
            }

        input.watermark {
            color: #999;
        }

        input {
            padding: 4px;
            text-indent: 5px;
            width: 200px;
            font-size: 14px;
        }

        #Logo {
            margin-top: 100px;
            margin-left: 95px;
        }
    </style>
</head>
<body style=" background-color: White; width: 960px; font-family: 'Arial'; margin: 0px auto 0px auto; padding: 0; color: #000; font-size: 14px; font-weight: normal; outline: none;">
    <div class="container">
        <img src="../Content/Images/logo-planguru-analytics-new.png" alt="Planguru Subscription Portal Logo" id="Logo">


        <!--content-->
        <div class="content">

            <div class="left">

                <p>Please log in with your Planguru Analytics Username and Password.</p>
                <br />
                <p><b>Don't have an account?</b><br /><a href="https://www.planguru.com/products/analytics/" style="color:#ffffff;" target="_blank">Click here to learn more about Planguru Analytics</a></p>


            </div>
            <div class="right">
                @Using Html.BeginForm("ForgotPassword", "Home")
                    @Html.AntiForgeryToken()
                    @Html.ValidationSummary(True)



                    @<div class="input-container" style="margin-bottom:20px;">

                        <label style="float: left; margin-bottom: 6px; color: grey; font-weight: 600;">Forgot Password:</label><br />
                        @Html.TextBoxFor(Function(m) m.userName, New With {.id = "username"})<br />
                        <span style="float:left">@Html.ValidationMessageFor(Function(m) m.userName)</span>

                    </div>
                    @<div class="input-container" style="margin-bottom:20px;">

                    @Html.TextBoxFor(Function(m) m.emailAddress, New With {.id = "email"})<br />
                        <span style="float:left">@Html.ValidationMessageFor(Function(m) m.emailAddress)</span>

                    </div>

                    @<div class="bottom pull_center">

                        <input type="submit" class="input" value="Send me an email" id="login" />
                        <a href="#" class="pull_right" id="back" style="margin-bottom: -20px;">&lt;&lt;&nbsp;Back</a>
                    </div>



                End Using

            </div>
        </div>
        <!--content over-->

        <div class="contact_us">
            <h2>Need help? Contact us:</h2>
            <p>US: (888) 822-6300 </p>
            <a href="mailto:support@planguru.com">support@planguru.com</a>

        </div>
        <div style="float: left; margin-left: 20px;">

            @If Not (DirectCast(TempData("Message"), String) Is Nothing) Then
                @<label class="success">
                    @TempData("Message").ToString()
                </label>
            End If
            @If Not (DirectCast(TempData("ErrorMessage"), String) Is Nothing) Then
                @<label class="error">
                    @TempData("ErrorMessage").ToString()
                </label>
            End If
            
        </div>



    </div>
</body>
</html>
<script type="text/javascript">



    $(document).ready(function (event) {

        var usernamewatermark = 'Enter Username';
        var emailwatermark = 'Enter Email';

        //init, set watermark text and class
        $('#username').val(usernamewatermark).addClass('watermark');
        $('#email').val(emailwatermark).addClass('watermark');

        //if blur and no value inside, set watermark text and class again.
        $('#username').blur(function () {
            if ($(this).val().length == 0) {
                $(this).val(usernamewatermark).addClass('watermark');
                $(this).css("color", " #999");
            }
        });

        //if focus and text is watermrk, set it to empty and remove the watermark class
        $('#username').focus(function () {
            if ($(this).val() == usernamewatermark) {
                $(this).val('').removeClass('watermark');
                $(this).css("color", "black");
            }
        });




        //init, set watermark text and class
        $('#email').val(emailwatermark).addClass('watermark');

        //if blur and no value inside, set watermark text and class again.
        $('#email').blur(function () {
            if ($(this).val().length == 0) {
                $(this).val(emailwatermark).addClass('watermark');
                $(this).css("color", " #999");
            }
        });

        //if focus and text is watermrk, set it to empty and remove the watermark class
        $('#email').focus(function () {
            if ($(this).val() == emailwatermark) {
                $(this).val('').removeClass('watermark');
                $(this).css("color", "black");
            }
        });

        $("#back").click(function () {
            window.location.href = '@Url.Action("Index", "Home")'
        });


    });



</script>