﻿var dashboardChartData = new Array();
var sharedDashboardChartData = new Array();
var containerId = 0;
var isPopupOpen = false;
var counter = 0;
$(function () {

    hs.Expander.prototype.onAfterExpand = addChart;


    function addChart() {
        var chart = $("#hc-test" + counter).highcharts();
        if (chart) {
            chart.destroy();
        }
        if ($('#dashboardView').css('display') == "block") {
            drawChartForHighSlide("#hc-test" + counter, dashboardChartData[containerId]);
        }
        else if ($('#sharedDashboardView').css('display') == "block") {
            drawChartForHighSlide("#hc-test" + counter, sharedDashboardChartData[containerId]);
        }

    }





    $.ajax({
        url: 'Dashboard/GetChartData',
        dataType: "json",
        type: "GET",
        contentType: 'application/json; charset=utf-8',
        async: false,
        processData: false,
        cache: false,
        delay: 15,
        success: function (data) {
            if (data.length > 0) {
                for (i = 0; i < data.length; i++) {
                    var div = $('<div id=containerd' + i + ' class="chartblock"></div>');
                    $(div).appendTo('#spread');
                    drawChart("#containerd" + i, data[i]);
                    dashboardChartData.push(data[i]);
                }
            }

        },
        error: function (xhr) {

        }
    });

    $.ajax({
        url: 'Dashboard/GetChartDataForSharedDashboard',
        dataType: "json",
        type: "GET",
        contentType: 'application/json; charset=utf-8',
        async: false,
        processData: false,
        cache: false,
        delay: 15,
        success: function (data) {
            if (data.length > 0) {
                for (i = 0; i < data.length; i++) {
                    var div = $('<div id=containers' + i + ' class="chartblock"></div>');
                    $(div).appendTo('#analysis_spread');
                    drawChart("#containers" + i, data[i]);
                    sharedDashboardChartData.push(data[i]);
                }
            }

        },
        error: function (xhr) {

        }
    });

});

function drawChart(container, data) {
    if (data.HighChartType == "spline" || data.HighChartType == "column" || data.HighChartType == "area") {
        var model = data;
        var series = new Array();
        var series1 = new Array();

        var tempseries = new Array();
        var intervalValue;
        var tickinterval;
        var trendComment;
        var yAxisData = new Array();
        var yAxisData1 = new Array();
        var yAxisValues = new Array();
        var xAxisData = new Array();
        var xAxisDisplayData = new Array();
        var firstseriesname = data.HighChartTitle;

        for (var i = 0 ; i < data.strXLabels.length ; i++) {
            if (data.strXLabels[i] != null) {
                var serie = new Array(data.strXLabels[i], data.decChartAmts[i]);
                series.push(serie);
            }
        }




        if (data.rowCounts == 2) {

            

            if (data.intFormat == 8 || data.intFormat == 9)
            {
                for (var k = 5 ; k < (data.strXLabels.length - 3) ; k++) {
                    if ((data.strXLabels[k - 5]) != null && data.decChartAmts[k] != 0) {
                        series1.push(data.decChartAmts[k]);
                    }
                }
            }

            else {
                for (var k = 13 ; k < (data.strXLabels.length + 13) ; k++) {
                    if ((data.strXLabels[k - 13]) != null || data.decChartAmts[k] != 0) {
                        series1.push(data.decChartAmts[k]);
                    }
                }
            }



            if (data.HighChartType == "spline" || data.HighChartType == "area") {

                for (var j = 0 ; j < series1.length ; j++) {
                    if (j > data.intLastActual && data.intFormat == 6) {
                        var yaxis = {
                            y: series1[j], marker: { fillColor: '#66b09d' }
                        }
                        yAxisData1.push(yaxis);
                    }
                    else {
                        var yaxis = {
                            y: series1[j], marker: { fillColor: '#94ddcb' }
                        }
                        yAxisData1.push(yaxis);
                    }

                }
            }
            else if (data.HighChartType == "column") {
                for (var j = 0 ; j < series1.length ; j++) {
                    if (data.intFormat == 6) {
                        if (j > data.intLastActual) {
                            var yaxis = {
                                y: series1[j], color: '#66b09d'
                            }
                            yAxisData1.push(yaxis);
                        }
                        else {
                            var yaxis = {
                                y: series1[j], color: '#94ddcb'
                            }
                            yAxisData1.push(yaxis);
                        }
                    }
                    else if (data.intFormat == 1 || data.intFormat == 2 || data.intFormat == 3 || data.intFormat == 4 || data.intFormat == 5) {
                        if (j == 0) {
                            var yaxis = {
                                y: series1[j], color: '#66b09d'
                            }
                            yAxisData1.push(yaxis);
                        }
                        else {
                            var yaxis = {
                                y: series1[j], color: '#94ddcb'
                            }
                            yAxisData1.push(yaxis);
                        }
                    }
                    else {
                        var yaxis = {
                            y: series1[j], color: '#66b09d'
                        }
                        yAxisData1.push(yaxis);
                    }
                }

            }
            firstseriesname = data.seriesLabels[0]

        }




        if (data.HighChartType == "spline" || data.HighChartType == "area") {

            for (var j = 0 ; j < series.length ; j++) {
                if (j > data.intLastActual && data.intFormat == 6) {
                    var yaxis = {
                        y: series[j][1], marker: { fillColor: '#0070C0' }
                    }
                    yAxisData.push(yaxis);
                }
                else {
                    var yaxis = {
                        y: series[j][1], marker: { fillColor: '#70B0F0' }
                    }
                    yAxisData.push(yaxis);
                }

            }
        }
        else if (data.HighChartType == "column") {
            for (var j = 0 ; j < series.length ; j++) {
                if (data.intFormat == 6) {
                    if (j > data.intLastActual) {
                        var yaxis = {
                            y: series[j][1], color: '#0070C0'
                        }
                        yAxisData.push(yaxis);
                    }
                    else {
                        var yaxis = {
                            y: series[j][1], color: '#70B0F0'
                        }
                        yAxisData.push(yaxis);
                    }
                }
                else if (data.intFormat == 1 || data.intFormat == 2 || data.intFormat == 3 || data.intFormat == 4 || data.intFormat == 5) {
                    if (j == 0) {
                        var yaxis = {
                            y: series[j][1], color: '#0070C0'
                        }
                        yAxisData.push(yaxis);
                    }
                    else {
                        var yaxis = {
                            y: series[j][1], color: '#70B0F0'
                        }
                        yAxisData.push(yaxis);
                    }
                }
                else {
                    var yaxis = {
                        y: series[j][1], color: '#0070C0'
                    }
                    yAxisData.push(yaxis);
                }
            }

        }

        for (var j = 0 ; j < series.length; j++) {
            yAxisValues.push(series[j][1]);
        }

        for (var k = 0 ; k < series.length; k++) {
            xAxisData.push(series[k][0]);
        }

        for (var l = 0; l < series.length; l++) {
            xAxisDisplayData.push(formatxaxis(xAxisData[l], xAxisData, l));
        }


        if (data.blnPercentLabel == false) {
            if (data.blnShowTrend == true && data.HighChartType == "spline" && series.length > 1) {
                trendComment = GetTrendComment(yAxisValues, data.blnPercentLabel, data.intFormat, data.intDecPlaces);
                $(container).highcharts({

                    chart: {
                        backgroundColor: "#f7f6f4",
                        type: data.HighChartType,
                        events: {
                            click: function () {
                                counter++;
                                var X;
                                if ($('#dashboardView').css('display') == "block") {
                                    if (((parseInt(this.renderTo.id.replace("containerd", "")) + 1) % 3) == 1) {
                                        X = 150;
                                    }
                                    else if (((parseInt(this.renderTo.id.replace("containerd", "")) + 1) % 3) == 2) {
                                        X = 500
                                    }
                                    else if (((parseInt(this.renderTo.id.replace("containerd", "")) + 1) % 3) == 0) {
                                        X = 1150
                                    }
                                    containerId = (parseInt(this.renderTo.id.replace("containerd", "")));
                                }
                                else if ($('#sharedDashboardView').css('display') == "block") {
                                    if (((parseInt(this.renderTo.id.replace("containers", "")) + 1) % 3) == 1) {
                                        X = 150;
                                    }
                                    else if (((parseInt(this.renderTo.id.replace("containers", "")) + 1) % 3) == 2) {
                                        X = 500
                                    }
                                    else if (((parseInt(this.renderTo.id.replace("containers", "")) + 1) % 3) == 0) {
                                        X = 1150
                                    }
                                    containerId = (parseInt(this.renderTo.id.replace("containers", "")));
                                }
                                hs.htmlExpand(null, {
                                    pageOrigin: {
                                        x: (this.mouseDownX + X),
                                        y: (this.mouseDownY + 150)
                                    },
                                    maincontentText: '<div id="hc-test' + counter + '"></div>',
                                    width: 530,
                                    height: 473
                                });



                            }
                        }
                    },
                    title: {
                        text: data.HighChartTitle,
                        style: {
                            color: '#276FA6',
                            fontWeight: 'bold',
                            backgroundColor: '#d3d3d3',
                            fontSize: '14px'
                        }
                    },
                    subtitle: {
                        text: data.HighChartSubTitle
                    },
                    xAxis: {
                        categories: xAxisDisplayData,
                        lineColor: '#000000',
                        tickLength: 8,
                        tickWidth: 1,
                        tickColor: "#000000"
                    },

                    yAxis: {
                        lineWidth: 1,
                        lineColor: '#000000',
                        gridLineColor: '#000000',
                        gridLineWidth: 1,
                        tickLength: 8,
                        tickWidth: 1,
                        tickColor: "#000000",
                        labels: {
                            formatter: function () {
                                return formatcurrency(this.value, 0);
                            }
                        },
                        title: {
                            text: ''
                        }

                    },
                    tooltip: {


                        formatter: function () {
                            return formatxaxis(this.key, xAxisData, this.point.index) + ' :<b> ' + formatcurrency(this.y, data.intDecPlaces) + '</b>';
                        }

                    },
                    credits: {
                        enabled: true,
                        text: trendComment,
                        position: {
                            align: 'center'
                        },
                        style: {
                            color: 'black',
                            fontSize: '12px'
                        },
                        href: '#'
                    },
                    exporting:
                        {
                            enabled: false
                        },
                    series: [{
                        data: yAxisData,
                        name: firstseriesname
                    },
                    {
                        type: 'line',
                        name: 'Trend',
                        color: '#f37823',
                        marker: {
                            enabled: false
                        },
                        data: (function () {
                            return fitData(yAxisValues).data;
                        })()
                    }]
                });

            }
            else {
                $(container).highcharts({

                    chart: {
                        backgroundColor: "#f7f6f4",
                        type: data.HighChartType,
                        events: {
                            click: function () {
                                counter++;
                                var X;
                                if ($('#dashboardView').css('display') == "block") {
                                    if (((parseInt(this.renderTo.id.replace("containerd", "")) + 1) % 3) == 1) {
                                        X = 150;
                                    }
                                    else if (((parseInt(this.renderTo.id.replace("containerd", "")) + 1) % 3) == 2) {
                                        X = 500
                                    }
                                    else if (((parseInt(this.renderTo.id.replace("containerd", "")) + 1) % 3) == 0) {
                                        X = 1150
                                    }
                                    containerId = (parseInt(this.renderTo.id.replace("containerd", "")));
                                }
                                else if ($('#sharedDashboardView').css('display') == "block") {
                                    if (((parseInt(this.renderTo.id.replace("containers", "")) + 1) % 3) == 1) {
                                        X = 150;
                                    }
                                    else if (((parseInt(this.renderTo.id.replace("containers", "")) + 1) % 3) == 2) {
                                        X = 500
                                    }
                                    else if (((parseInt(this.renderTo.id.replace("containers", "")) + 1) % 3) == 0) {
                                        X = 1150
                                    }
                                    containerId = (parseInt(this.renderTo.id.replace("containers", "")));
                                }
                                hs.htmlExpand(null, {
                                    pageOrigin: {
                                        x: (this.mouseDownX + X),
                                        y: (this.mouseDownY + 150)
                                    },
                                    maincontentText: '<div id="hc-test' + counter + '"></div>',
                                    width: 530,
                                    height: 473
                                });



                            }
                        }
                    },
                    title: {
                        text: data.HighChartTitle,
                        style: {
                            color: '#276FA6',
                            fontWeight: 'bold',
                            backgroundColor: '#d3d3d3',
                            fontSize: '14px'
                        }
                    },
                    subtitle: {
                        text: data.HighChartSubTitle
                    },
                    xAxis: {
                        categories: xAxisDisplayData,
                        lineColor: '#000000',
                        tickLength: 8,
                        tickWidth: 1,
                        tickColor: "#000000"
                    },

                    yAxis: {
                        lineWidth: 1,
                        lineColor: '#000000',
                        gridLineColor: '#000000',
                        gridLineWidth: 1,
                        tickLength: 8,
                        tickWidth: 1,
                        tickColor: "#000000",
                        labels: {
                            formatter: function () {
                                return formatcurrency(this.value, 0);
                            }
                        },
                        title: {
                            text: ''
                        }

                    },
                    tooltip: {

                        formatter: function () {
                            return formatxaxis(this.key, xAxisData, this.point.index) + ' :<b> ' + formatcurrency(this.y, data.intDecPlaces) + '</b>';
                        }
                    },
                    credits: {
                        enabled: false
                    },
                    exporting:
                        {
                            enabled: false
                        },
                    series: [{
                        data: yAxisData,
                        name: firstseriesname
                    }]
                });
            }



        }
        else {
            intervalValue = GetIntervals(series);
            if (intervalValue >= 0 && intervalValue < 30) {
                tickinterval = 0.05;
            }
            else if (intervalValue >= 30 && intervalValue < 60) {
                tickinterval = 0.10;
            }
            else if (intervalValue >= 60 && intervalValue < 90) {
                tickinterval = 0.15;
            }
            else if (intervalValue >= 90 && intervalValue < 120) {
                tickinterval = 0.20;
            }
            else if (intervalValue >= 120 && intervalValue < 150) {
                tickinterval = 0.25;
            }
            else if (intervalValue >= 150 && intervalValue < 180) {
                tickinterval = 0.30;
            }
            else if (intervalValue >= 180 && intervalValue < 210) {
                tickinterval = 0.35;
            }
            else if (intervalValue >= 210 && intervalValue < 240) {
                tickinterval = 0.40;
            }
            else if (intervalValue >= 240 && intervalValue < 270) {
                tickinterval = 0.45;
            }
            else if (intervalValue >= 270 && intervalValue < 300) {
                tickinterval = 0.50;
            }
            else if (intervalValue >= 300) {
                tickinterval = 0.80;
            }


            if (data.blnShowTrend == true && data.HighChartType == "spline" && series.length > 1) {
                trendComment = GetTrendComment(yAxisValues, data.blnPercentLabel, data.intFormat, data.intDecPlaces);
                $(container).highcharts({

                    chart: {
                        backgroundColor: "#f7f6f4",
                        type: data.HighChartType,
                        events: {
                            click: function () {
                                counter++;
                                var X;
                                if ($('#dashboardView').css('display') == "block") {
                                    if (((parseInt(this.renderTo.id.replace("containerd", "")) + 1) % 3) == 1) {
                                        X = 150;
                                    }
                                    else if (((parseInt(this.renderTo.id.replace("containerd", "")) + 1) % 3) == 2) {
                                        X = 500
                                    }
                                    else if (((parseInt(this.renderTo.id.replace("containerd", "")) + 1) % 3) == 0) {
                                        X = 1150
                                    }
                                    containerId = (parseInt(this.renderTo.id.replace("containerd", "")));
                                }
                                else if ($('#sharedDashboardView').css('display') == "block") {
                                    if (((parseInt(this.renderTo.id.replace("containers", "")) + 1) % 3) == 1) {
                                        X = 150;
                                    }
                                    else if (((parseInt(this.renderTo.id.replace("containers", "")) + 1) % 3) == 2) {
                                        X = 500
                                    }
                                    else if (((parseInt(this.renderTo.id.replace("containers", "")) + 1) % 3) == 0) {
                                        X = 1150
                                    }
                                    containerId = (parseInt(this.renderTo.id.replace("containers", "")));
                                }
                                hs.htmlExpand(null, {
                                    pageOrigin: {
                                        x: (this.mouseDownX + X),
                                        y: (this.mouseDownY + 150)
                                    },
                                    maincontentText: '<div id="hc-test' + counter + '"></div>',
                                    width: 530,
                                    height: 473
                                });



                            }
                        }
                    },
                    title: {
                        text: data.HighChartTitle,
                        style: {
                            color: '#276FA6',
                            fontWeight: 'bold',
                            backgroundColor: '#d3d3d3',
                            fontSize: '14px'
                        }
                    },
                    subtitle: {
                        text: data.HighChartSubTitle
                    },
                    xAxis: {
                        categories: xAxisDisplayData,
                        lineColor: '#000000',
                        tickLength: 8,
                        tickWidth: 1,
                        tickColor: "#000000"
                    },

                    yAxis: {
                        lineWidth: 1,
                        lineColor: '#000000',
                        gridLineColor: '#000000',
                        gridLineWidth: 1,
                        tickLength: 8,
                        tickWidth: 1,
                        tickColor: "#000000",
                        tickInterval: tickinterval,


                        labels: {
                            formatter: function () {
                                return (Math.round(this.value * 100) + '%');
                            }
                        },
                        title: {
                            text: ''
                        }

                    },
                    tooltip: {
                        formatter: function () {
                            return formatxaxis(this.key, xAxisData, this.point.index) + ' :<b> ' + Highcharts.numberFormat((this.y * 100), 2, '.') + '%</b>';
                        }
                    },
                    credits: {
                        enabled: true,
                        text: trendComment,
                        position: {
                            align: 'center'
                        },
                        style: {
                            color: 'black',
                            fontSize: '12px'
                        },
                        href: '#'
                    },
                    exporting:
                        {
                            enabled: false
                        },
                    series: [{
                        data: yAxisData,
                        name: firstseriesname
                    },
                     {
                         type: 'line',
                         name: 'Trend',
                         color: '#f37823',
                         marker: {
                             enabled: false
                         },
                         data: (function () {
                             return fitData(yAxisValues).data;
                         })()
                     }
                    ]
                });
            }
            else {

                $(container).highcharts({

                    chart: {
                        backgroundColor: "#f7f6f4",
                        type: data.HighChartType,
                        events: {
                            click: function () {
                                counter++;
                                var X;
                                if ($('#dashboardView').css('display') == "block") {
                                    if (((parseInt(this.renderTo.id.replace("containerd", "")) + 1) % 3) == 1) {
                                        X = 150;
                                    }
                                    else if (((parseInt(this.renderTo.id.replace("containerd", "")) + 1) % 3) == 2) {
                                        X = 500
                                    }
                                    else if (((parseInt(this.renderTo.id.replace("containerd", "")) + 1) % 3) == 0) {
                                        X = 1150
                                    }
                                    containerId = (parseInt(this.renderTo.id.replace("containerd", "")));
                                }
                                else if ($('#sharedDashboardView').css('display') == "block") {
                                    if (((parseInt(this.renderTo.id.replace("containers", "")) + 1) % 3) == 1) {
                                        X = 150;
                                    }
                                    else if (((parseInt(this.renderTo.id.replace("containers", "")) + 1) % 3) == 2) {
                                        X = 500
                                    }
                                    else if (((parseInt(this.renderTo.id.replace("containers", "")) + 1) % 3) == 0) {
                                        X = 1150
                                    }
                                    containerId = (parseInt(this.renderTo.id.replace("containers", "")));
                                }
                                hs.htmlExpand(null, {
                                    pageOrigin: {
                                        x: (this.mouseDownX + X),
                                        y: (this.mouseDownY + 150)
                                    },
                                    maincontentText: '<div id="hc-test' + counter + '"></div>',
                                    width: 530,
                                    height: 473
                                });



                            }
                        }
                    },
                    title: {
                        text: data.HighChartTitle,
                        style: {
                            color: '#276FA6',
                            fontWeight: 'bold',
                            backgroundColor: '#d3d3d3',
                            fontSize: '14px'
                        }
                    },
                    subtitle: {
                        text: data.HighChartSubTitle
                    },
                    xAxis: {
                        categories: xAxisDisplayData,
                        lineColor: '#000000',
                        tickLength: 8,
                        tickWidth: 1,
                        tickColor: "#000000"
                    },

                    yAxis: {
                        lineWidth: 1,
                        lineColor: '#000000',
                        gridLineColor: '#000000',
                        gridLineWidth: 1,
                        tickLength: 8,
                        tickWidth: 1,
                        tickColor: "#000000",
                        tickInterval: tickinterval,


                        labels: {
                            formatter: function () {
                                return (Math.round(this.value * 100) + '%');
                            }
                        },
                        title: {
                            text: ''
                        }

                    },
                    tooltip: {
                        formatter: function () {
                            return formatxaxis(this.key, xAxisData, this.point.index) + ' :<b> ' + Highcharts.numberFormat((this.y * 100), 2, '.') + '%</b>';
                        }
                    },
                    credits: {
                        enabled: false
                    },
                    exporting:
                        {
                            enabled: false
                        },
                    series: [{
                        data: yAxisData,
                        name: firstseriesname
                    }]
                });
            }


        }

        if (data.rowCounts == 2) {
            $(container).highcharts().addSeries({ name: data.seriesLabels[1], data: yAxisData1, color: '#94ddcb' });
        }
    }
    else if (data.HighChartType == "donut" || data.HighChartType == "pie") {
        var chartData = new Array();
        var donutData = new Array();
        var datavalue;
        var selectedPeriodValue = data.intColCount
        var innerSizeDonut = '';
        if (data.intFormat == 14 || data.intFormat == 15 || data.intFormat == 18 || data.intFormat == 19 || data.intFormat == 7) {
            for (var j = selectedPeriodValue ; j < data.decChartAmts.length; j = j + 13) {
                chartData.push(data.decChartAmts[j]);
            }
        }
        else if (data.intFormat == 16 || data.intFormat == 17) {
            for (var j = selectedPeriodValue ; j < data.decChartAmts.length; j = j + 13) {
                chartData.push(data.decChartAmts[j - 1]);
            }
        }

        for (var k = 0 ; k < chartData.length ; k++) {
            if (chartData[k] >= 0) {
                var seriesColor = GetSeriesColor(k);
                donutData.push({ name: data.seriesLabels[k], y: chartData[k], color: seriesColor })
            }
        }

        if (data.HighChartType == 'donut') {
            innerSizeDonut = '40%'
        }
        else if (data.HighChartType == 'pie') {
            innerSizeDonut = '0%'
        }

        $(container).highcharts({

            chart: {
                type: 'pie',
                backgroundColor: "#f7f6f4",
                events: {
                    click: function () {
                        counter++;
                        var X;
                        if ($('#dashboardView').css('display') == "block") {
                            if (((parseInt(this.renderTo.id.replace("containerd", "")) + 1) % 3) == 1) {
                                X = 150;
                            }
                            else if (((parseInt(this.renderTo.id.replace("containerd", "")) + 1) % 3) == 2) {
                                X = 500
                            }
                            else if (((parseInt(this.renderTo.id.replace("containerd", "")) + 1) % 3) == 0) {
                                X = 1150
                            }
                            containerId = (parseInt(this.renderTo.id.replace("containerd", "")));
                        }
                        else if ($('#sharedDashboardView').css('display') == "block") {
                            if (((parseInt(this.renderTo.id.replace("containers", "")) + 1) % 3) == 1) {
                                X = 150;
                            }
                            else if (((parseInt(this.renderTo.id.replace("containers", "")) + 1) % 3) == 2) {
                                X = 500
                            }
                            else if (((parseInt(this.renderTo.id.replace("containers", "")) + 1) % 3) == 0) {
                                X = 1150
                            }
                            containerId = (parseInt(this.renderTo.id.replace("containers", "")));
                        }
                        hs.htmlExpand(null, {
                            pageOrigin: {
                                x: (this.mouseDownX + X),
                                y: (this.mouseDownY + 150)
                            },
                            maincontentText: '<div id="hc-test' + counter + '"></div>',
                            width: 530,
                            height: 473
                        });



                    }
                }
            },
            title: {
                text: data.HighChartTitle,
                style: {
                    color: '#276FA6',
                    fontWeight: 'bold',
                    backgroundColor: '#d3d3d3',
                    fontSize: '14px'
                }
            },
            subtitle: {
                text: data.HighChartSubTitle
            },
            yAxis: {

            },
            plotOptions: {
                pie: {
                    shadow: false
                }
            },
            tooltip: {
                formatter: function () {
                    if (data.blnPercentLabel == false) {
                        return this.point.name + ': <b>' + formatcurrency(this.y, 0) + ' : ' + Math.round(this.percentage * 100) / 100 + ' % </b>';
                    }
                    else {
                        return this.point.name + ':<b>' + formatcurrency(this.y, 2) + '%</b>';
                    }
                }
            },
            credits: {
                enabled: false
            },
            legend: {
                maxHeight: 60
            },
            exporting: {
                enabled: false
            },
            series: [{
                data: donutData,
                size: '100%',
                innerSize: innerSizeDonut,
                showInLegend: true,
                dataLabels: {
                    enabled: false
                }
            }]
        });
    }


}

function GetYAxisValueForPercentage(value) {
    var yAxisData = new Array();
    for (var j = 0 ; j < value.length; j++) {
        yAxisData.push(value[j][1]);
    }
    var max = parseInt(Math.max.apply(Math, yAxisData) * 100);
    if (max > 10) {
        max = max + (10 - (max % 10));
    }
    else if (max > 1 && max < 10) {
        max = 10;
    }
    //alert(max);

    return max;


}



function GetIntervals(value) {
    var yAxisData = new Array();
    for (var j = 0 ; j < value.length; j++) {
        yAxisData.push(value[j][1]);
    }
    var max = parseInt(Math.max.apply(Math, yAxisData) * 100);
    var min = parseInt(Math.min.apply(Math, yAxisData) * 100);
    return (max - min);


}

function GetTrendComment(YaxisData, percentLabel, intFormat, decPlaces) {
    var comment;
    if (YaxisData.length > 1) {
        var differenceValue = (fitData(YaxisData).data[1][1] - fitData(YaxisData).data[0][1]);
        if (percentLabel == false) {
            if (differenceValue > 0) {
                comment = "Trend - Increasing at rate of " + differenceValue.toFixed(decPlaces);
            }
            else if (differenceValue < 0) {
                comment = "Trend - Decreasing at rate of " + (-differenceValue).toFixed(decPlaces);
            }
            else if (differenceValue == 0) {
                comment = "Trend - Increasing at rate of " + 0;
            }
        }
        else {
            if (differenceValue > 0) {
                comment = "Trend - Increasing at rate of " + (differenceValue * 100).toFixed(decPlaces);
            }
            else if (differenceValue < 0) {
                comment = "Trend - Decreasing at rate of " + ((-differenceValue) * 100).toFixed(decPlaces);
            }
            else if (differenceValue == 0) {
                comment = "Trend - Increasing at rate of " + 0;
            }
        }

        if (percentLabel == true) {
            comment = comment + "%"
        }

        if (intFormat == 1 || intFormat == 3 || intFormat == 4 || intFormat == 6 || intFormat == 7) {
            comment = comment + " per month";
        }
        else {
            comment = comment + " per year";
        }
    }
    else if (percentLabel == true) {
        comment = "Trend - Increasing at rate of " + 0 + "% per month";
    }
    else {
        comment = "Trend - Increasing at rate of " + 0 + " per month";
    }
    return comment;
}

function fitOneDimensionalData(source_data) {
    var trend_source_data = [];
    for (var i = source_data.length; i-- > 0;) {
        trend_source_data[i] = [i, source_data[i]]
    }
    var regression_data = fitData(trend_source_data).data
    var trend_line_data = [];
    for (var i = regression_data.length; i-- > 0;) {
        trend_line_data[i] = regression_data[i][1];
    }
    return trend_line_data;
}

function formatcurrency(value, decPlaces) {
    if (decPlaces != 0) {
        return value.toFixed(decPlaces).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString();
    }
    else {
        return parseInt(value).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
    }
}

function formatxaxis(value, seriesXaxis, index) {

    if (value == "F") {
        return "Feb";
    }
    else if (value == "S") {
        return "Sep";
    }
    else if (value == "O") {
        return "Oct";
    }
    else if (value == "N") {
        return "Nov";
    }
    else if (value == "D") {
        return "Dec";
    }
    else if (value == "J") {
        if (seriesXaxis[(index - 1)] == "J" || seriesXaxis[(index + 1)] == "A") {
            return "Jul";
        }
        else if (seriesXaxis[(index - 1)] == "M" || seriesXaxis[(index + 1)] == "J") {
            return "Jun";
        }
        else {
            return "Jan";
        }
    }
    else if (value == "M") {
        if (seriesXaxis[(index - 1)] == "F" || seriesXaxis[(index + 1)] == "A") {
            return "Mar";
        }
        else {
            return "May";
        }
    }
    else if (value == "A") {
        if (seriesXaxis[(index - 1)] == "M" || seriesXaxis[(index + 1)] == "M") {
            return "Apr";
        }
        else {
            return "Aug";
        }
    }
    else {
        return value;
    }


}

function drawChartForHighSlide(container, data) {
    if (data.HighChartType == "spline" || data.HighChartType == "column" || data.HighChartType == "area") {
        var model = data;
        var series = new Array();
        var series1 = new Array();

        var tempseries = new Array();
        var intervalValue;
        var tickinterval;
        var trendComment;
        var yAxisData = new Array();
        var yAxisData1 = new Array();
        var yAxisValues = new Array();
        var xAxisData = new Array();
        var xAxisDisplayData = new Array();
        var firstseriesname = data.HighChartTitle;

        for (var i = 0 ; i < data.strXLabels.length ; i++) {
            if (data.strXLabels[i] != null) {
                var serie = new Array(data.strXLabels[i], data.decChartAmts[i]);
                series.push(serie);
            }
        }

        if (data.rowCounts == 2) {

            if (data.intFormat == 8 || data.intFormat == 9) {
                for (var k = 5 ; k < (data.strXLabels.length - 3) ; k++) {
                    if ((data.strXLabels[k - 5]) != null && data.decChartAmts[k] != 0) {
                        series1.push(data.decChartAmts[k]);
                    }
                }
            }

            else {
                for (var k = 13 ; k < (data.strXLabels.length + 13) ; k++) {
                    if ((data.strXLabels[k - 13]) != null || data.decChartAmts[k] != 0) {
                        series1.push(data.decChartAmts[k]);
                    }
                }
            }


            if (data.HighChartType == "spline" || data.HighChartType == "area") {

                for (var j = 0 ; j < series1.length ; j++) {
                    if (j > data.intLastActual && data.intFormat == 6) {
                        var yaxis = {
                            y: series1[j], marker: { fillColor: '#66b09d' }
                        }
                        yAxisData1.push(yaxis);
                    }
                    else {
                        var yaxis = {
                            y: series1[j], marker: { fillColor: '#94ddcb' }
                        }
                        yAxisData1.push(yaxis);
                    }

                }
            }
            else if (data.HighChartType == "column") {
                for (var j = 0 ; j < series1.length ; j++) {
                    if (data.intFormat == 6) {
                        if (j > data.intLastActual) {
                            var yaxis = {
                                y: series1[j], color: '#66b09d'
                            }
                            yAxisData1.push(yaxis);
                        }
                        else {
                            var yaxis = {
                                y: series1[j], color: '#94ddcb'
                            }
                            yAxisData1.push(yaxis);
                        }
                    }
                    else if (data.intFormat == 1 || data.intFormat == 2 || data.intFormat == 3 || data.intFormat == 4 || data.intFormat == 5) {
                        if (j == 0) {
                            var yaxis = {
                                y: series1[j], color: '#66b09d'
                            }
                            yAxisData1.push(yaxis);
                        }
                        else {
                            var yaxis = {
                                y: series1[j], color: '#94ddcb'
                            }
                            yAxisData1.push(yaxis);
                        }
                    }
                    else {
                        var yaxis = {
                            y: series1[j], color: '#66b09d'
                        }
                        yAxisData1.push(yaxis);
                    }
                }

            }
            firstseriesname = data.seriesLabels[0]

        }


        if (data.HighChartType == "spline" || data.HighChartType == "area") {

            for (var j = 0 ; j < series.length ; j++) {
                if (j > data.intLastActual && data.intFormat == 6) {
                    var yaxis = {
                        y: series[j][1], marker: { fillColor: '#0070C0' }
                    }
                    yAxisData.push(yaxis);
                }
                else {
                    var yaxis = {
                        y: series[j][1], marker: { fillColor: '#70B0F0' }
                    }
                    yAxisData.push(yaxis);
                }

            }
        }
        else if (data.HighChartType == "column") {
            for (var j = 0 ; j < series.length ; j++) {
                if (data.intFormat == 6) {
                    if (j > data.intLastActual) {
                        var yaxis = {
                            y: series[j][1], color: '#0070C0'
                        }
                        yAxisData.push(yaxis);
                    }
                    else {
                        var yaxis = {
                            y: series[j][1], color: '#70B0F0'
                        }
                        yAxisData.push(yaxis);
                    }
                }
                else if (data.intFormat == 1 || data.intFormat == 2 || data.intFormat == 3 || data.intFormat == 4 || data.intFormat == 5) {
                    if (j == 0) {
                        var yaxis = {
                            y: series[j][1], color: '#0070C0'
                        }
                        yAxisData.push(yaxis);
                    }
                    else {
                        var yaxis = {
                            y: series[j][1], color: '#70B0F0'
                        }
                        yAxisData.push(yaxis);
                    }
                }
                else {
                    var yaxis = {
                        y: series[j][1], color: '#0070C0'
                    }
                    yAxisData.push(yaxis);
                }
            }

        }

        for (var j = 0 ; j < series.length; j++) {
            yAxisValues.push(series[j][1]);
        }

        for (var k = 0 ; k < series.length; k++) {
            xAxisData.push(series[k][0]);
        }

        for (var l = 0; l < series.length; l++) {
            xAxisDisplayData.push(formatxaxis(xAxisData[l], xAxisData, l));
        }


        if (data.blnPercentLabel == false) {
            if (data.blnShowTrend == true && data.HighChartType == "spline" && series.length > 1) {
                trendComment = GetTrendComment(yAxisValues, data.blnPercentLabel, data.intFormat, data.intDecPlaces);
                $(container).highcharts({

                    chart: {
                        backgroundColor: "#f7f6f4",
                        type: data.HighChartType
                    },
                    title: {
                        text: data.HighChartTitle,
                        style: {
                            color: '#276FA6',
                            fontWeight: 'bold',
                            backgroundColor: '#d3d3d3',
                            fontSize: '14px'
                        }
                    },
                    subtitle: {
                        text: data.HighChartSubTitle
                    },
                    xAxis: {
                        categories: xAxisDisplayData,
                        lineColor: '#000000',
                        tickLength: 8,
                        tickWidth: 1,
                        tickColor: "#000000"
                    },

                    yAxis: {
                        lineWidth: 1,
                        lineColor: '#000000',
                        gridLineColor: '#000000',
                        gridLineWidth: 1,
                        tickLength: 8,
                        tickWidth: 1,
                        tickColor: "#000000",
                        labels: {
                            formatter: function () {
                                return formatcurrency(this.value, 0);
                            }
                        },
                        title: {
                            text: ''
                        }

                    },
                    tooltip: {


                        formatter: function () {
                            return formatxaxis(this.key, xAxisData, this.point.index) + ' :<b> ' + formatcurrency(this.y, data.intDecPlaces) + '</b>';
                        }

                    },
                    credits: {
                        enabled: true,
                        text: trendComment,
                        position: {
                            align: 'center'
                        },
                        style: {
                            color: 'black',
                            fontSize: '12px'
                        },
                        href: '#'
                    },
                    exporting:
                        {
                            enabled: false
                        },
                    series: [{
                        data: yAxisData,
                        name: firstseriesname
                    },
                    {
                        type: 'line',
                        name: 'Trend',
                        color: '#f37823',
                        marker: {
                            enabled: false
                        },
                        data: (function () {
                            return fitData(yAxisValues).data;
                        })()
                    }]
                });
            }
            else {
                $(container).highcharts({

                    chart: {
                        backgroundColor: "#f7f6f4",
                        type: data.HighChartType,
                    },
                    title: {
                        text: data.HighChartTitle,
                        style: {
                            color: '#276FA6',
                            fontWeight: 'bold',
                            backgroundColor: '#d3d3d3',
                            fontSize: '14px'
                        }
                    },
                    subtitle: {
                        text: data.HighChartSubTitle
                    },
                    xAxis: {
                        categories: xAxisDisplayData,
                        lineColor: '#000000',
                        tickLength: 8,
                        tickWidth: 1,
                        tickColor: "#000000"
                    },

                    yAxis: {
                        lineWidth: 1,
                        lineColor: '#000000',
                        gridLineColor: '#000000',
                        gridLineWidth: 1,
                        tickLength: 8,
                        tickWidth: 1,
                        tickColor: "#000000",
                        labels: {
                            formatter: function () {
                                return formatcurrency(this.value, 0);
                            }
                        },
                        title: {
                            text: ''
                        }

                    },
                    tooltip: {

                        formatter: function () {
                            return formatxaxis(this.key, xAxisData, this.point.index) + ' :<b> ' + formatcurrency(this.y, data.intDecPlaces) + '</b>';
                        }
                    },
                    credits: {
                        enabled: false
                    },
                    exporting:
                        {
                            enabled: false
                        },
                    series: [{
                        data: yAxisData,
                        name: firstseriesname
                    }]
                });
            }



        }
        else {
            intervalValue = GetIntervals(series);
            if (intervalValue >= 0 && intervalValue < 30) {
                tickinterval = 0.05;
            }
            else if (intervalValue >= 30 && intervalValue < 60) {
                tickinterval = 0.10;
            }
            else if (intervalValue >= 60 && intervalValue < 90) {
                tickinterval = 0.15;
            }
            else if (intervalValue >= 90 && intervalValue < 120) {
                tickinterval = 0.20;
            }
            else if (intervalValue >= 120 && intervalValue < 150) {
                tickinterval = 0.25;
            }
            else if (intervalValue >= 150 && intervalValue < 180) {
                tickinterval = 0.30;
            }
            else if (intervalValue >= 180 && intervalValue < 210) {
                tickinterval = 0.35;
            }
            else if (intervalValue >= 210 && intervalValue < 240) {
                tickinterval = 0.40;
            }
            else if (intervalValue >= 240 && intervalValue < 270) {
                tickinterval = 0.45;
            }
            else if (intervalValue >= 270 && intervalValue < 300) {
                tickinterval = 0.50;
            }
            else if (intervalValue >= 300) {
                tickinterval = 0.80;
            }


            if (data.blnShowTrend == true && data.HighChartType == "spline" && series.length > 1) {
                trendComment = GetTrendComment(yAxisValues, data.blnPercentLabel, data.intFormat, data.intDecPlaces);
                $(container).highcharts({

                    chart: {
                        backgroundColor: "#f7f6f4",
                        type: data.HighChartType,
                    },
                    title: {
                        text: data.HighChartTitle,
                        style: {
                            color: '#276FA6',
                            fontWeight: 'bold',
                            backgroundColor: '#d3d3d3',
                            fontSize: '14px'
                        }
                    },
                    subtitle: {
                        text: data.HighChartSubTitle
                    },
                    xAxis: {
                        categories: xAxisDisplayData,
                        lineColor: '#000000',
                        tickLength: 8,
                        tickWidth: 1,
                        tickColor: "#000000"
                    },

                    yAxis: {
                        lineWidth: 1,
                        lineColor: '#000000',
                        gridLineColor: '#000000',
                        gridLineWidth: 1,
                        tickLength: 8,
                        tickWidth: 1,
                        tickColor: "#000000",
                        tickInterval: tickinterval,


                        labels: {
                            formatter: function () {
                                return (Math.round(this.value * 100) + '%');
                            }
                        },
                        title: {
                            text: ''
                        }

                    },
                    tooltip: {
                        formatter: function () {
                            return formatxaxis(this.key, xAxisData, this.point.index) + ' :<b> ' + Highcharts.numberFormat((this.y * 100), 2, '.') + '%</b>';
                        }
                    },
                    credits: {
                        enabled: true,
                        text: trendComment,
                        position: {
                            align: 'center'
                        },
                        style: {
                            color: 'black',
                            fontSize: '12px'
                        },
                        href: '#'
                    },
                    exporting:
                        {
                            enabled: false
                        },
                    series: [{
                        data: yAxisData,
                        name: firstseriesname
                    },
                     {
                         type: 'line',
                         name: 'Trend',
                         color: '#f37823',
                         marker: {
                             enabled: false
                         },
                         data: (function () {
                             return fitData(yAxisValues).data;
                         })()
                     }
                    ]
                });
            }
            else {

                $(container).highcharts({

                    chart: {
                        backgroundColor: "#f7f6f4",
                        type: data.HighChartType,
                    },
                    title: {
                        text: data.HighChartTitle,
                        style: {
                            color: '#276FA6',
                            fontWeight: 'bold',
                            backgroundColor: '#d3d3d3',
                            fontSize: '14px'
                        }
                    },
                    subtitle: {
                        text: data.HighChartSubTitle
                    },
                    xAxis: {
                        categories: xAxisDisplayData,
                        lineColor: '#000000',
                        tickLength: 8,
                        tickWidth: 1,
                        tickColor: "#000000"
                    },

                    yAxis: {
                        lineWidth: 1,
                        lineColor: '#000000',
                        gridLineColor: '#000000',
                        gridLineWidth: 1,
                        tickLength: 8,
                        tickWidth: 1,
                        tickColor: "#000000",
                        tickInterval: tickinterval,


                        labels: {
                            formatter: function () {
                                return (Math.round(this.value * 100) + '%');
                            }
                        },
                        title: {
                            text: ''
                        }

                    },
                    tooltip: {
                        formatter: function () {
                            return formatxaxis(this.key, xAxisData, this.point.index) + ' :<b> ' + Highcharts.numberFormat((this.y * 100), 2, '.') + '%</b>';
                        }
                    },
                    credits: {
                        enabled: false
                    },
                    exporting:
                        {
                            enabled: false
                        },
                    series: [{
                        data: yAxisData,
                        name: firstseriesname
                    }]
                });
            }


        }

        if (data.rowCounts == 2) {
            $(container).highcharts().addSeries({ name: data.seriesLabels[1], data: yAxisData1, color: '#94ddcb' });
        }
    }
    else if (data.HighChartType == "donut" || data.HighChartType == "pie") {
        var chartData = new Array();
        var donutData = new Array();
        var datavalue;
        var selectedPeriodValue = data.intColCount
        var innerSizeDonut = '';
        if (data.intFormat == 14 || data.intFormat == 15 || data.intFormat == 18 || data.intFormat == 19 || data.intFormat == 7) {
            for (var j = selectedPeriodValue ; j < data.decChartAmts.length; j = j + 13) {
                chartData.push(data.decChartAmts[j]);
            }
        }
        else if (data.intFormat == 16 || data.intFormat == 17) {
            for (var j = selectedPeriodValue ; j < data.decChartAmts.length; j = j + 13) {
                chartData.push(data.decChartAmts[j - 1]);
            }
        }

        for (var k = 0 ; k < chartData.length ; k++) {
            if (chartData[k] >= 0) {
                var seriesColor = GetSeriesColor(k);
                donutData.push({ name: data.seriesLabels[k], y: chartData[k], color: seriesColor })
            }
        }

        if (data.HighChartType == 'donut') {
            innerSizeDonut = '40%'
        }
        else if (data.HighChartType == 'pie') {
            innerSizeDonut = '0%'
        }

        $(container).highcharts({

            chart: {
                type: 'pie',
                backgroundColor: "#f7f6f4"
            },
            title: {
                text: data.HighChartTitle,
                style: {
                    color: '#276FA6',
                    fontWeight: 'bold',
                    backgroundColor: '#d3d3d3',
                    fontSize: '14px'
                }
            },
            subtitle: {
                text: data.HighChartSubTitle
            },
            yAxis: {

            },
            plotOptions: {
                pie: {
                    shadow: false
                }
            },
            tooltip: {
                formatter: function () {
                    if (data.blnPercentLabel == false) {
                        return this.point.name + ': <b>' + formatcurrency(this.y, 0) + ' : ' + Math.round(this.percentage * 100) / 100 + ' % </b>';
                    }
                    else {
                        return this.point.name + ':<b>' + formatcurrency(this.y, 2) + '%</b>';
                    }
                }
            },
            credits: {
                enabled: false
            },
            exporting: {
                enabled: false
            },
            legend: {
                maxHeight: 60
            },
            series: [{
                data: donutData,
                size: '100%',
                innerSize: innerSizeDonut,
                showInLegend: true,
                dataLabels: {
                    enabled: false
                }
            }]
        });
    }




}


function GetSeriesColor(SeriesIndex) {
    var colorCode;
    switch (SeriesIndex) {
        case 0:
            colorCode = '#1083a3';
            break;
        case 1:
            colorCode = '#f19632';
            break;
        case 2:
            colorCode = '#66b09d';
            break;
        case 3:
            colorCode = '#dd5822';
            break;
        case 4:
            colorCode = '#0e6684';
            break;
        case 5:
            colorCode = '#eecf4d';
            break;
        case 6:
            colorCode = '#90ac47';
            break;
        case 7:
            colorCode = '#c7341f'
            break;
        case 8:
            colorCode = '#139fc2'
            break;
        case 9:
            colorCode = '#f27723'
            break;
        case 10:
            colorCode = '#1083a3';
            break;
        case 11:
            colorCode = '#f19632';
            break;
        case 12:
            colorCode = '#66b09d';
            break;
        case 13:
            colorCode = '#dd5822';
            break;
        case 14:
            colorCode = '#0e6684';
            break;
        case 15:
            colorCode = '#eecf4d';
            break;
        case 16:
            colorCode = '#90ac47';
            break;
        case 17:
            colorCode = '#c7341f'
            break;
        case 18:
            colorCode = '#139fc2'
            break;
        case 19:
            colorCode = '#f27723'
            break;
        case 20:
            colorCode = '#1083a3';
            break;
        case 21:
            colorCode = '#f19632';
            break;
        case 22:
            colorCode = '#66b09d';
            break;
        case 23:
            colorCode = '#dd5822';
            break;
        case 24:
            colorCode = '#0e6684';
            break;
        case 25:
            colorCode = '#eecf4d';
            break;
        case 26:
            colorCode = '#90ac47';
            break;
        case 27:
            colorCode = '#c7341f'
            break;
        case 28:
            colorCode = '#139fc2'
            break;
        case 29:
            colorCode = '#f27723'
            break;
        default:
            colorCode = '#1083a3';
    }
    return colorCode;
}



var spread = document.getElementById('spdAnalytics');
var rc = spread.GetRowCount();
var count = 0;
for (var i = 0; i < rc; i++) {
    var cellval = spread.GetValue(i, 2);
    if (cellval == 'true') {
        count++;
    };
};
var selectedindex = $('#intChartType').get(0).selectedIndex;
var selectedValue = $('#intFormat').val();
if ((selectedValue === '1' || selectedValue === '4' || selectedValue === '3') && count > 3) {
    $.msgBox({
        title: 'Alert',
        content: 'You can not select more than 3 factors for this chart format.',
        type: 'Error',
        buttons: [{
            value: 'Ok'
        }],
        afterShow: function () {
            $('[name=Ok]').focus();
            $('#spdAnalytics').find('input:checkbox').attr('checked', false);
        }
    })
} else {
    if(selectedindex > 0) {
        if(count == 1) {
            if(selectedindex == 3) {
                $('#ShowTrend').show();
            }
            $('#UpdateChart').show();
        } else if (count > 1) {
            $('#ShowTrend').hide();
            $('#blnShowTrend').attr('checked', false);
            $('#UpdateChart').show();
        } else {
            $('#ShowTrend').hide();
            $('#blnShowTrend').attr('checked', false);
        }
    }
};