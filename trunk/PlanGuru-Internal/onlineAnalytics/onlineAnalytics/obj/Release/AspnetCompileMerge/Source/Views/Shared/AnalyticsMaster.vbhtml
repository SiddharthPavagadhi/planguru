﻿@Imports  onlineAnalytics
<!DOCTYPE html>
<html>
<head>
    <!-- to get the absolute path for the images into alert used inside the jquery.msgBox.js file-->
    <script type="text/javascript" language="javascript">
        var isSessionAliveCalled = 'false';
        var msgBoxImagePath = '@Url.Content("~/Content/MsgBoxImages/")'; 
    </script>
    <title>@ViewData("Title")</title>
    <link href="@Url.Content("~/Content/jquery-ui-1.10.0.custom.css")" rel="stylesheet" type="text/css" />
    <link href="@Url.Content("~/Content/css/styles.css")" rel="stylesheet" type="text/css" />
    <link href="@Url.Content("~/Content/msgBoxLight.css")" rel="stylesheet" type="text/css" />
    <script src="@Url.Content("~/Scripts/jquery-1.9.1.min.js")" type="text/javascript"></script>
    <script src="@Url.Content("~/Scripts/jquery-ui-1.10.0.custom.js")" type="text/javascript"></script>
    <script src="@Url.Content("~/Scripts/dropdown.js")" type="text/javascript"></script>
    <script src="@Url.Content("~/Scripts/jquery.maskedinput.min.js")" type="text/javascript"></script>
    <script src="@Url.Content("~/Scripts/jquery.msgBox.js")" type="text/javascript"></script>
    <script src="@Url.Content("~/Scripts/Highslide-full.min.js")" type="text/javascript"></script>
    <script src="@Url.Content("~/Scripts/Highslide.config.js")" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        window.ViewerControlId = "";
    </script>
</head>
<body>
    @Code
        Dim UserInfo As New User()
        Dim UserName As String = String.Empty
        Dim ua As New UserAccess
        Dim Administrator As Boolean = False
        If Not (Session("UserInfo") Is Nothing) Then
            UserInfo = DirectCast(Session("UserInfo"), User)
            UserName = String.Concat("Welcome <b ><a href=", Url.Action("EditProfile", "User").ToString(), ">", UserInfo.FirstName, " ", UserInfo.LastName, "</a>", "</b>")
            If (UserInfo.UserRoleId = UserRoles.PGAAdmin Or UserInfo.UserRoleId = UserRoles.PGASupport) Then
                Administrator = True
            End If

        End If

        If Not (Session("UserAccess") Is Nothing) Then
            ua = DirectCast(Session("UserAccess"), UserAccess)
        End If
    End code
    <div class="wrapper" onclick="hs.close()" style="display:none;">
        <div class="main-container">
            <!--Header Container -->
            <div class="header">
                <div class="logo-panel">
                    <a href="#">
                        @If (ua.Branding And (UserInfo.Customer.PlanTypeId = PlanType.BP Or UserInfo.Customer.PlanTypeId = PlanType.AAM Or UserInfo.Customer.PlanTypeId = PlanType.AAY Or UserInfo.Customer.PlanTypeId = PlanType.BNPM Or UserInfo.Customer.PlanTypeId = PlanType.BNPMUPG Or UserInfo.Customer.PlanTypeId = PlanType.BNPY Or UserInfo.Customer.PlanTypeId = PlanType.BNPYUPG) And UserInfo.Customer.LogoImageFile <> String.Empty) Then
                            @<img id="branding_logo" src='@Url.Content("~/Branding_Logos/" + UserInfo.Customer.LogoImageFile)' alt="Planguru Analytics" style="max-width:365px;max-height:78px;" />
                        Else
                            @<img src='@Url.Content("~/Content/Images/planguru_logo.png")' alt="Planguru Analytics" />
                        End If
                    </a>
                </div>
                <div class="header-right">
                    <div class="user-panel" style="font-size: 12px;">
                        <div class="welcome-user">
                            <span>
                                <a class="needhelp" target="_blank" href="https://planguru.zendesk.com/categories/20127686-PlanGuru-Analytics">
                                    Need Help?
                                </a> <span class="helpdivider">|</span>@MvcHtmlString.Create(UserName)
                            </span>
                            <a href='@Url.Action("Logout", "User")' class="logout-section" style="padding-top:5px; width:20px;">
                                <img src='@Url.Content("~/Content/Images/Log-out.png")' title="Logout" />
                            </a>
                        </div>
                    </div>
                    <div class="select-panel topnav-panel">
                        <div class="top-nav">
                            <ul>
                                <li class='dropdown-last'>
                                    <a id="SelectedCompany" href="#"></a>
                                    <div class='top-nav-dropdown-additional'>
                                        <ul id="CompanyId"></ul>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="top-nav">
                            <ul>
                                <li class='dropdown-last'>
                                    <a id="SelectedAnalysis" href="#"></a>
                                    <div class='top-nav-dropdown-additional'>
                                        <ul id="AnalysisId"></ul>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        @Code
                            If UserInfo.UserRoleId <> UserRoles.CRU Then
                        @<div class="top-nav">
                            <ul>
                                <li class="dropdown-last" style="padding-right: 0px;">
                                    <a href="#">Manage Account</a>
                                    <div class="top-nav-dropdown-additional">
                                        <ul>
                                            @If (ua.ViewCompanies And UserInfo.UserRoleId <> UserRoles.PGAAdmin) Then
                                                @<li><a href='@Url.Action("Index", "Company")'>Companies</a></li>
                                                End If
                                            @If (ua.ViewAnalyses And UserInfo.UserRoleId <> UserRoles.PGAAdmin) Then
                                                @<li><a href='@Url.Action("Index", "Analysis")'>Analyses</a></li>
                                                End If
                                            @If (ua.ViewUser) Then
                                                @<li><a href='@Url.Action("Index", "User")'>Users</a></li>
                                                End If
                                            @If (ua.Branding And (UserInfo.Customer.PlanTypeId = PlanType.BP Or UserInfo.Customer.PlanTypeId = PlanType.AAM Or UserInfo.Customer.PlanTypeId = PlanType.AAY or UserInfo.Customer.PlanTypeId = PlanType.BNPM or UserInfo.Customer.PlanTypeId = PlanType.BNPMUPG or UserInfo.Customer.PlanTypeId = PlanType.BNPY or UserInfo.Customer.PlanTypeId = PlanType.BNPYUPG)) Then
                                                @<li><a href='@Url.Action("Index", "Branding")'>Branding</a></li>
                                                End If
                                            @Code
                                                    If (Administrator) Then
                                                @<li><a href='@Url.Action("Index", "Subscribe")'>Subscription</a></li>
                                                @<li><a href='@Url.Action("SearchCustomer", "Subscribe")'>Search customers</a></li>
                                                    ElseIf (UserInfo.UserRoleId = UserRoles.SAU Or UserInfo.UserRoleId = UserRoles.SSU) Then
                                                @<li class="dropdown-last">
                                                    <a href="#">Edit Subscription<img style="margin-left:5px;vertical-align:middle;" src='@Url.Content("~/Content/Images/rightarrow.png")' /></a>
                                                    <div class="top-nav-sub-dropdown">
                                                        <ul>
                                                            @If (ua.UpdateSubscription) Then
                                                                @<li><a href='@Url.Action("EditSubscription", "Subscribe")' target="_blank">Change Billing Info</a></li>
                                                                    End If
                                                            @If (ua.CancelSubscription) Then
                                                                @<li>
                                                                    <a href="#" class="CancelSubscription" data='@Url.Action("CancelSubscription", "Subscribe")'>
                                                                        Cancel Subscription
                                                                    </a>
                                                                </li>
                                                                    End If
                                                            <li><a href='@Url.Action("EditProfile", "User")'>Change Other Info</a></li>
                                                            @If (UserInfo.UserRoleId = UserRoles.SAU) Then
                                                                @<li><a href="#" class="ChangeSubscription">Change your subscription plan</a></li>
                                                                    End If
                                                        </ul>
                                                    </div>
                                                </li>
                                                    End If
                                            End Code
                                        </ul>
                                    </div>
                                </li>
                            </ul>
                        </div>
                            End If
                        End Code
                    </div>
                </div>
            </div>
            <!--Header Container -->
            <!--Nav Panel -->
            <div class="nav-panel" style="background-color:@UserInfo.Customer.PrimaryColor;">
                <ul>
                    <li><a href='@Url.Action("Index", "Dashboard")' data="Dashboard">Dashboard</a></li>
                    <li><a href='@Url.Action("Index", "Scorecard")' data="Scorecard">Scorecard</a></li>
                    <li class='dropdown-last'>
                        <a href='#'>Quick Reports</a>
                        <div class='top-nav-dropdown-savedreport' style="background-color:@UserInfo.Customer.PrimaryColor;">
                            <ul>
                                <li><a href='@Url.Action("Index", "RE")' data="RE">Revenue &amp; Expenses</a></li>
                                <li><a href='@Url.Action("Index", "ALC")' data="ALC">Balance Sheet</a></li>
                                <li><a href='@Url.Action("Index", "CF")' data="CF">Cash Flow</a></li>
                                <li><a href='@Url.Action("Index", "RA")' data="RA">Ratios</a></li>
                                <li><a href='@Url.Action("Index", "OM")' data="OM">Non-financial data</a></li>
                            </ul>
                        </div>
                    </li>
                    <li class='dropdown-last'>
                        <a href='@Url.Action("Index", "SV")' data="SV">Saved Reports</a>
                        <div class='top-nav-dropdown-savedreport' style="background-color:@UserInfo.Customer.PrimaryColor;">
                            <ul id="SavedReports"></ul>
                        </div>
                    </li>
                </ul>
            </div>
            <!-- Nav Panel
            -->
            <div class="center-panel" id="centerpanel">
                <div style="margin: 0px 0px 0px 0px; padding-bottom: 0px;">
                    @RenderBody()
                </div>
            </div>
            @If (ua.Branding And (UserInfo.Customer.PlanTypeId = PlanType.BP Or UserInfo.Customer.PlanTypeId = PlanType.AAM Or UserInfo.Customer.PlanTypeId = PlanType.AAY Or UserInfo.Customer.PlanTypeId = PlanType.BNPM Or UserInfo.Customer.PlanTypeId = PlanType.BNPMUPG Or UserInfo.Customer.PlanTypeId = PlanType.BNPY Or UserInfo.Customer.PlanTypeId = PlanType.BNPYUPG) And (UserInfo.Customer.FootNote <> String.Empty)) Then
                @<div style="display: inline-block;margin:10px 0px;padding:10px 10px 10px 10px;width:98%;">@UserInfo.Customer.FootNote.ToString()</div>
            End If
        </div>
        <div class="plangurufooter" id="footerpanel" style="font-size: 11px; padding-top: 0px;
            margin-top: 0px;">
            <div class="footer-panel">
                &copy; Planguru Analytics 2018
            </div>
        </div>
        @If (ua.Branding And (UserInfo.Customer.PlanTypeId = PlanType.BP Or UserInfo.Customer.PlanTypeId = PlanType.AAM Or UserInfo.Customer.PlanTypeId = PlanType.AAY Or UserInfo.Customer.PlanTypeId = PlanType.BNPM Or UserInfo.Customer.PlanTypeId = PlanType.BNPMUPG Or UserInfo.Customer.PlanTypeId = PlanType.BNPY Or UserInfo.Customer.PlanTypeId = PlanType.BNPYUPG)) Then
            @<div class="main-container header footer-panel-logo">
                <img src="@Url.Content("~/Content/Images/logo-Powered-Analytics.jpg")" alt="Powered by: PlanGuru Analytics" />
            </div>
        End If
    </div>

    <div id="loading" style="text-align: center;">
    </div>
    <div id="validation">
    </div>
    @If (UserInfo.UserRoleId = UserRoles.SAU Or UserInfo.UserRoleId = UserRoles.SSU) Then
        @Html.Partial("~/Views/Shared/_Change_your_subscription.vbhtml")
        @Html.Partial("~/Views/Shared/_UserRoleHelp.vbhtml")
    End If
</body>

</html>
<script language="javascript" type="text/javascript">
    function resizeImage() {
        var window_height = document.body.clientHeight;
        var window_width = document.body.clientWidth;

        var image_width = $("#branding_logo").width();
        var image_height = $("#branding_logo").height();

        var height_ratio = image_height / window_height;
        var width_ratio = image_width / window_width;

        //alert(window_height); alert(window_width);      
        
        if (height_ratio > width_ratio) {

            $("#branding_logo").width('auto');
            $("#branding_logo").height('100%');
        }
        else {
            $("#branding_logo").width('100%');
            $("#branding_logo").height('auto');
        }
    }

    var pathArray = window.location.href.split('/');
    var url;
    if (pathArray.length > 2) {
        var protocol = pathArray[0];
        var host = pathArray[2];
        var app = pathArray[3];
        url = protocol + '//' + host + "/" + app;

    } else {
        var protocol = pathArray[0];
        var host = pathArray[2];
        url = protocol + '//' + host;
    }

    $(".CancelSubscription").click(function (event) {
        if (isSessionAliveCalled == 'false') { IsSessionAlive(); }
        event.preventDefault();
        var url = $(this).attr('data');

        $.msgBox({
            title: "Confirm",
            content: "Deleting a subscription will delete all records in the database for the subscription.  Do you want to continue?",
            type: "confirm",
            buttons: [{ value: "Yes" }, { value: "No"}],
            success: function (result) {
                if (result == "Yes") {
                    window.location = url;
                }
            },
            afterShow: function () { $('[name=No]').focus(); }
        });

    });


    function resetPasswordConfirmation(UserName, PostUrl) {
        IsSessionAlive();
        //var url = $(".resetPassword").attr('data');                
        $.msgBox({
            title: "Confirm",
            content: "Are you sure you want to reset password for " + UserName + "?",
            type: "confirm",
            buttons: [{ value: "Yes" }, { value: "No"}],
            success: function (result) {
                if (result == "Yes") {
                    window.location = PostUrl;
                }
            },
            afterShow: function () { $('[name=No]').focus(); }
        });
    }


    $(".ChangeSubscription").click(function (event) {
        event.preventDefault();

        $(".ui-dialog-titlebar").show();
        subscriptionDialog.dialog('open');


    });

    var theDialog = $('#loading').dialog({
        open: function (event, ui) { $(".ui-dialog-titlebar-close").hide(); },
        center_modal: true,
        autoOpen: false,
        modal: true,
        close: false,
        closeOnEscape: false,
        width: 200,
        modal: true,
        minHeight: 10,
        show: 'fade',
        hide: 'fade',
        resizable: 'false',
        draggable: false
    });

    var subscriptionDialog = $('#choose_subscription_container').dialog({
        title: "Change your subscription plan",
        open: function (event, ui) { $(".ui-dialog-titlebar-close").hide(); },
        center_modal: true,
        autoOpen: false,
        modal: true,
        close: true,
        closeOnEscape: true,
        width: 700,
        minHeight: 10,
        show: 'fade',
        hide: 'fade',
        resizable: 'false',
        draggable: false, buttons: { "OK": function () {
            $(this).dialog("close");
        }
        }
    });

var user_role_help_Dialog = $('#user_role_help_container').dialog({
        title: "Select a User Role",
        open: function (event, ui) { $(".ui-dialog-titlebar-close").hide(); },
        center_modal: true,
        autoOpen: false,
        modal: true,
        close: true,
        closeOnEscape: true,
        width: 800,
        minHeight: 10,
        show: 'fade',
        hide: 'fade',
        resizable: 'false',
        draggable: false, buttons: { "OK": function () {
            $(this).dialog("close");
        }
        }
    });

   
    function getCompanies(_selectedCompanyId, _selectedCompany, source) {

        if (isSessionAliveCalled == 'false') { IsSessionAlive(); }

        $.ajax({
            type: "GET",
            url: '@Url.Action("GetCompanies", "Dashboard")',
            global: false,
            cache: false,
            data: { SelectedCompany: 0 },
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (msg) {
                var _selectedCompany
                if (msg !== undefined && (msg != "")) {

                    if (msg.length > 0) {
                        $("#SelectedCompany").html("Select Company");
                        $("#SelectedCompany").attr("data", 0);
                        _selectedCompanyId = msg[0].Value;
                    }
                    for (var company_index = 0; company_index < msg.length; company_index++) {

                        if (msg[company_index].Selected == true) {
                            $("#SelectedCompany").html(msg[company_index].Text);
                            $("#SelectedCompany").attr("data", msg[company_index].Value);
                            _selectedCompanyId = msg[company_index].Value;
                            _selectedCompany = msg[company_index].Text;
                        }

                        $("#CompanyId").append("<li><a class='companyList' href='#' data=" + msg[company_index].Value + " >" + msg[company_index].Text + "</a></li>");
                    }
                    getAnalyses(_selectedCompanyId, _selectedCompany, source);
                    companyListEvent();
                }
                else {
                    $("#SelectedCompany").html("No Company Assigned");
                    $("#SelectedCompany").attr("data", 0);

                    $("#SelectedAnalysis").html("No Analysis Assigned");
                    $("#SelectedAnalysis").attr("data", 0);

                    $("#CompanyId").html("");
                    $("#AnalysisId").html("");
                }

            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                // var msg = JSON.parse(XMLHttpRequest.responseText);
                //theDialog.dialog('close');
            }
        });
    }

    function companyListEvent() {

        if (isSessionAliveCalled == 'false') { IsSessionAlive(); }
        $(".companyList").click(function (event) {
            event.preventDefault();

            _selectedCompanyId = $(this).attr('data');
            _selectedCompany = $(this).text();

            if (_selectedCompany != "" && _selectedCompanyId != "") {
                $("#SelectedCompany").html(_selectedCompany);
                $("#SelectedCompany").attr("data", _selectedCompanyId);
            }

            getAnalyses(_selectedCompanyId, _selectedCompany, 'user');
        });
    }

    function getAnalyses(_selectedCompanyId, _selectedCompany, source) {

        if (isSessionAliveCalled == 'false') { IsSessionAlive(); }

        $.ajax({
            type: "GET",
            url: '@Url.Action("GetAnalyses", "Dashboard")',
            global: false,
            cache: false,
            data: { selectedCompany: _selectedCompanyId, source: source },
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            beforeSend: function () {
                var imgSrc = '@Url.Content("~/Content/Images/loading.gif")';
                $(".ui-dialog-titlebar").hide();
                $('#loading').html("Please wait.... <img src=" + imgSrc + " />");
                theDialog.dialog('open');
            },
            success: function (msg) {
                theDialog.dialog('close');

                if ((msg.result == 'success') && (msg.data != "") && (msg.data != undefined)) {

                    if (msg.data.length > 0) {

                        $("#SelectedAnalysis").html("");
                        $("#AnalysisId").html("");

                        $("#SelectedAnalysis").html("Select Analysis");
                        $("#SelectedAnalysis").attr("data", 0);
                        //_selectedAnalysisId = msg[0].Value;

                    }
                    _selectedAnalysisId = ""
                    for (analysis_index = 0; analysis_index < msg.data.length; analysis_index++) {

                        if (msg.data[analysis_index].Selected == true)
                        { $("#SelectedAnalysis").html(msg.data[analysis_index].Text); _selectedAnalysisId = msg.data[analysis_index].Value; }

                        $("#AnalysisId").append("<li><a class='saveAnalysis' href='#' data=" + msg.data[analysis_index].Value + " >" + msg.data[analysis_index].Text + "</a></li>");
                    }
                    if (_selectedAnalysisId != "" && _selectedAnalysisId != undefined)
                        getSavedReports(_selectedAnalysisId);

                    saveAnalysisEvent();

                    if (msg.data.length > 0 && ($("#SelectedAnalysis").html() == "Select Analysis")) {
                        $(window.ViewerControlId).attr("style", "display:none");
                        $("#validationMessage").html('Please select an analysis of the selected company from the dropdown list to open the dashboard view. ');
                        $("#validationMessage").attr("style", "display:inline-block");
                    } else {
                        $(window.ViewerControlId).attr("style", "display:block");
                        if ($("#countOfDataUploaded").val() == "0") {
                            $("#validationMessage").html('Data has not been uploaded for selected analysis.');
                            $("#validationMessage").show();
                        }
                        else { $("#validationMessage").hide(); }
                    }
                }
                else if ((msg.result == 'success') && (msg.data == "") && (msg.data != undefined)) {
                    $("#SelectedAnalysis").html("No Analysis Assigned");
                    $("#SelectedAnalysis").attr("data", 0);

                    $("#AnalysisId").html("");

                    $(window.ViewerControlId).attr("style", "display:none");
                    $("#validationMessage").html('You have not been given access to any analysis for the selected company.  Please contact your administrator.');
                    $("#validationMessage").attr("style", "display:inline-block");
                }
                else if ((msg.result == 'error')) {

                }

                if (msg.urlReferrer != undefined && msg.urlReferrer != "" && $("#validationMessage").html() == '') {

                    $('#centerpanel').load(msg.urlReferrer, function () {
                        //$(window.ViewerControlId).attr("style", "display:block");                       
                    });
                } else if (msg.urlReferrer != undefined && msg.urlReferrer != "" && $("#validationMessage").html() == undefined) {

                    $('#centerpanel').load(msg.urlReferrer, function () {
                        //$(window.ViewerControlId).attr("style", "display:block");                       
                    });
                }


            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                // var msg = JSON.parse(XMLHttpRequest.responseText);
                theDialog.dialog('close');
            }
        });
    }

    function saveAnalysisEvent() {

        if (isSessionAliveCalled == 'false') { IsSessionAlive(); }

        $(".saveAnalysis").click(function (event) {

            event.preventDefault();

            _selectedAnalysisId = $(this).attr('data');
            _selectedAnalysis = $(this).text();

            if (_selectedAnalysis != "" && _selectedAnalysisId != "") {
                $("#SelectedAnalysis").html(_selectedAnalysis);
                $("#SelectedAnalysis").attr("data", _selectedAnalysisId);
            }

            saveAnalysis(_selectedAnalysisId);

        });
    }


    function saveAnalysis(_selectedAnalysisId) {
        
        if (isSessionAliveCalled == 'false') { IsSessionAlive(); }
        
        $.ajax({
            type: "GET",
            url: '@Url.Action("SetDefaultAnalysis", "Dashboard")',
            global: false,
            cache: false,
            data: { SelectedAnalysis: _selectedAnalysisId },
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (msg) {
                if (msg.Result = "success") {

                    window.location = '@Url.Action("Index", "Dashboard")';

                    //window.location.href = msg.UrlReferrer;

                    /*
                    $('#centerpanel').load(msg.UrlReferrer, function () {

                    $(window.ViewerControlId).attr("style", "display:block");
                    if ($("#SelectedAnalysis").html() == "Select Analysis") {

                    $(window.ViewerControlId).attr("style", "display:none");
                    $("#validationMessage").html('Please select analysis of the selected company, to get the dashboard view.');
                    $("#validationMessage").attr("style", "display:inline-block");
                    } else {
                    $(window.ViewerControlId).attr("style", "display:block");
                    if ($("#countOfDataUploaded").val() == "0") {
                    $("#validationMessage").html('Data has not been uploaded for selected analysis.')
                    $("#validationMessage").show();
                    }
                    else { $("#validationMessage").hide(); }
                    }
                    });
                    */
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                // var msg = JSON.parse(XMLHttpRequest.responseText);
                //theDialog.dialog('close');
            }
        });
    }

    function IsSessionAlive() {
       
        if (isSessionAliveCalled == 'false') {
            isSessionAliveCalled = 'true';
            $.ajax({
                type: "GET",
                url: '@Url.Action("IsSessionAlive", "Dashboard")',
                global: false,
                cache: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (msg) {

                    if (msg == undefined || (msg == "") || msg == false) {
                       
                        window.location = '@Url.Action("Index", "Home", New With {.session = "-1"})';
                    }

                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    // var msg = JSON.parse(XMLHttpRequest.responseText);
                    //theDialog.dialog('close');
                }
            });
        }
    }

    $(document).ready(function () {
        var pgurl = window.location.href.substr(window.location.href.lastIndexOf("/") + 1);
       


        $(".nav-panel ul li a").each(function () {
            if ($(this).attr("data") == pgurl || $(this).attr("data") == '') {
                $(this).css("background-color", '@UserInfo.Customer.SecondaryColor');
                $(this).addClass("selected");
            }
        })

        $(".top-nav-dropdown-savedreport ul li a").each(function () {
            if ($(this).attr("data") == pgurl || $(this).attr("data") == '') {
                $(this).css("background-color", '@UserInfo.Customer.SecondaryColor');
                $(this).addClass("selected");
            }
        })       

        $.ajaxSetup({ cache: false });
        var _selectedCompanyId;
        var _selectedCompany;

        if (isSessionAliveCalled == 'false') {
            IsSessionAlive();
        }

        if ($('#SelectedCompany').text() == "") {
            getCompanies(_selectedCompanyId, _selectedCompany, 'load');
        }


        $("#updatesetup").addClass("secondbutton_example").removeClass("ui-button ui-widget ui-state-default ui-corner-all");
        $("#updatesetup").hover(function () {
            $(this).removeClass("ui-state-hover");
            $(this).addClass("secondbutton_example");
        });

        $("#updatesetup").click(function () {
            IsSessionAlive();
            $(this).removeClass("ui-state-focus");
            $(this).addClass("secondbutton_example");
        });


        $(".add").click(function (event) {
            if ($(this).attr("href") != undefined && $(this).attr("href") != null && $(this).attr("href") != '') {
                window.document.location = $(this).attr("href");
            }
        });
    });

    function getSavedReports(_selectedAnalysisId) {

        if (isSessionAliveCalled == 'false') { IsSessionAlive(); }

        $.ajax({
            type: "GET",
            url: '@Url.Action("GetSavedReports", "Dashboard")',
            global: false,
            cache: false,
            data: { selectedAnalysisId: _selectedAnalysisId },
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (msg) {
                if (msg !== undefined && (msg != "")) {
                    var editurl = '@Url.Action("Views", "SV")';
                    var cru = '@UserRoles.CRU.GetHashCode()';
                    var userinfo = '@UserInfo.UserRoleId';
                    $("#SavedReports").html("");
                    for (var savedreport_index = 0; savedreport_index < msg.length; savedreport_index++) {
                        var idcontrolVal = msg[savedreport_index].Value;
                        var id_controller = idcontrolVal.split("_")
                        var controller = id_controller[0]
                        var url;
                        if (controller.trim() == "RE")
                            url = '@Url.Action("Viewer", "RE", New With {.Id = "-1"})'
                        else if (controller.trim() == "ALC")
                            url = '@Url.Action("Viewer", "ALC", New With {.Id = "-1"})'
                        else if (controller.trim() == "CF")
                            url = '@Url.Action("Viewer", "CF", New With {.Id = "-1"})'
                        else if (controller.trim() == "RA")
                            url = '@Url.Action("Viewer", "RA", New With {.Id = "-1"})'
                        else if (controller.trim() == "OM")
                            url = '@Url.Action("Viewer", "OM", New With {.Id = "-1"})'

                        url = url.replace("-1", id_controller[1]);
                        var pgurl_page = window.location.href.substr(window.location.href.indexOf("/", 7));

                        
                        if (url == pgurl_page) {

                            $("#SavedReports").append("<li><a class='savedReport selected' style='background-color:@UserInfo.Customer.SecondaryColor' href='" + url + "' data='" + msg[savedreport_index].Value + "' >" + msg[savedreport_index].Text + "</a></li>");

                        } else {

                            $("#SavedReports").append("<li><a class='savedReport' href='" + url + "' data='" + msg[savedreport_index].Value + "' >" + msg[savedreport_index].Text + "</a></li>");
                        }

                        //document.forms[0].action = url; onclick = 'javascriptdocument.forms[0].submit();'

                    }
                    if (userinfo != cru)
                        $("#SavedReports").append("<li><a class='savedReport' href='" + editurl + "'>Edit / Delete Saved Reports</a></li>");

                }
                else {
                    $("#SavedReports").html("");
                }

            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
            }
        });        
    }

    $(document).ajaxComplete(function (event, xhr, settings) {
      
            $(".wrapper").show();
       
    });

    $(document).ajaxSend(function (event, jqxhr, settings) {
        $(".wrapper").hide();
        var imgSrc = '@Url.Content("~/Content/Images/loading.gif")';
        $('#loading').html("Please wait.... <img src=" + imgSrc + " />");

    });


   
</script>
