﻿@Imports  onlineAnalytics
<!DOCTYPE html>
<html>
<head>
    <!-- to get the absolute path for the images into alert used inside the jquery.msgBox.js file-->
    <script type="text/javascript" language="javascript">
        var msgBoxImagePath = '@Url.Content("~/Content/MsgBoxImages/")';
    </script>
    <title>@ViewData("Title")</title>
    <link href="@Url.Content("~/Content/jquery-ui-1.10.0.custom.css")" rel="stylesheet" type="text/css" />
    <link href="@Url.Content("~/Content/Site.css")" rel="stylesheet" type="text/css" />
    <link href="@Url.Content("~/Content/msgBoxLight.css")" rel="stylesheet" type="text/css" />
    <script src="@Url.Content("~/Scripts/jquery-1.9.1.min.js")" type="text/javascript"></script>
    <script src="@Url.Content("~/Scripts/modernizr-1.7.min.js")" type="text/javascript"></script>
    <script src="@Url.Content("~/Scripts/jquery-ui-1.10.0.custom.js")" type="text/javascript"></script>
    <script src="@Url.Content("~/Scripts/jquery.msgBox.js")" type="text/javascript"></script>
</head>
<body>
    <style type="text/css">
        #loginPanel tr td a
        {
            color: White;
            cursor: pointer;
            text-align: center;
            width: auto;
            height: auto;
            text-decoration: none;
            padding: 3px 20px; /*background-color: #107FC7; */
            background-color: #4D90FE;
            background-image: -moz-linear-gradient(center top , #4D90FE, #4787ED);
            background-repeat: repeat-x;
        }
        
        #loginPanel tr td a:hover
        {
            /*background-color: #95B8D3;*/
            background-color: #0072BB;
            background-image: -moz-linear-gradient(center top , #0072BB, #0072BB);
            background-repeat: repeat-x;
            cursor: pointer;
            color: White;
            text-align: center;
            cursor: pointer;
            width: auto;
            padding: 3px 20px;
            height: auto !important;
        }
        .LoginButton
        {
            /*background-color: #276FA6;*/ /* background-image: url("/onlineAnalytics/Content/Images/login.jpg");*/
            border: 1px solid #183650;
            color: #FFFFFF;
            padding: 1px;
            text-align: center;
            text-shadow: -1px -1px 0 rgba(0, 0, 0, 0.3);
            width: 60px;
            background-repeat: no-repeat;
        }
        .LoginButton:hover
        {
            cursor: pointer;
        }
        .forgotUsername, .forgotPassword
        {
            margin-right: 30px;
            padding-top: 3px;
            height: 22px;
        }
        .forgotUsername:hover, .forgotPassword:hover
        {
            cursor: pointer;
        }
        input.watermark
        {
            color: #999;
        }
        input
        {
            padding: 4px;
            text-indent: 5px;
            width: 200px;
            font-size: 14px;
        }
        
        .LoginBtnMargin
        {
            margin-top:20px;
            text-align:center;     
            padding:0px !important;
             
        }
              
              
        .button_example
        {
            width:70px;
            height:30px;
            text-indent: 1px;
            border: 1px solid #276fa6;
        }

    </style>   
    <div style="width:29%;margin-left:32%;margin-top:15%;">
        @RenderBody()
    </div>
    @code
        Dim sessionExpired = Request.QueryString("session")
        If (sessionExpired = "-1") Then
        @<label class="timeout">Your session has expired, Please login again.
        </label>    
        End If
    End Code
    <div id="loading" style="text-align: center;">
    </div>
    <div id="validation" style="text-align: center;">
    </div>
</body>
</html>
<script type="text/javascript">
    var pathArray = window.location.href.split('/');
    var url;
    if (pathArray.length > 2) {
        var protocol = pathArray[0];
        var host = pathArray[2];
        var app = pathArray[3];
        url = protocol + '//' + host + "/" + app;

    } else {
        var protocol = pathArray[0];
        var host = pathArray[2];
        url = protocol + '//' + host;
    }

    var theDialog = $('#loading').dialog({
        open: function (event, ui) { $(".ui-dialog-titlebar-close").hide(); },
        center_modal: true,
        autoOpen: false,
        modal: true,
        close: false,
        closeOnEscape: false,
        width: 200,
        modal: true,
        minHeight: 10,
        show: 'fade',
        hide: 'fade',
        resizable: 'false',
        draggable: false
    });

    var validationDialog = $('#validation').dialog({
        title: "Login",
        open: function (event, ui) { $(".ui-dialog-titlebar-close").hide(); },
        center_modal: true,
        autoOpen: false,
        modal: true,
        close: true,
        closeOnEscape: true,
        width: 500,
        minHeight: 10,
        show: 'fade',
        hide: 'fade',
        resizable: 'false',
        draggable: false,
        buttons: { "OK": function () {
            $(this).dialog("close");
        }
        }
    });


    $(document).ready(function (event) {

        var usernamewatermark = 'Username';

        //init, set watermark text and class
        $('#Username').val(usernamewatermark).addClass('watermark');

        //if blur and no value inside, set watermark text and class again.
        $('#Username').blur(function () {
            if ($(this).val().length == 0) {
                $(this).val(usernamewatermark).addClass('watermark');
                $(this).css("color", " #999");
            }
        });

        //if focus and text is watermrk, set it to empty and remove the watermark class
        $('#Username').focus(function () {
            if ($(this).val() == usernamewatermark) {
                $(this).val('').removeClass('watermark');
                $(this).css("color", "black");
            }
        });

        var passwatermark = 'Password';

        //init, set watermark text and class
        $('#Password').attr('type', 'text');
        $('#Password').val(passwatermark).addClass('watermark');

        //if blur and no value inside, set watermark text and class again.
        $('#Password').blur(function () {
            if ($(this).val().length == 0) {
                $('#Password').attr('type', 'text');
                $(this).val(passwatermark).addClass('watermark');
                $(this).css("color", " #999");
            }
        });

        //if focus and text is watermrk, set it to empty and remove the watermark class
        $('#Password').focus(function () {
            if ($(this).val() == passwatermark) {
                $('#Password').attr('type', 'password');
                $(this).val('').removeClass('watermark');
                $(this).css("color", "black");
            }
        });

        $("#updatesetup").addClass("secondbutton_example").removeClass("ui-button ui-widget ui-state-default ui-corner-all");
        $("#updatesetup").hover(function () {
            $(this).removeClass("ui-state-hover");
            $(this).addClass("secondbutton_example");
        });
        $("#updatesetup").click(function () {
            $(this).removeClass("ui-state-focus");
            $(this).addClass("secondbutton_example");
        });

        $(".forgotUsername").click(function () {
            window.location.href = '@Url.Action("ForgotUsername", "Home")'
        });

        $(".forgotPassword").click(function () {
            window.location.href = '@Url.Action("ForgotPassword", "Home")'
        });
    });

    $('#Password').keypress('click', function (e) {

        if (e.which == 13) {
            $('#login').trigger("click");
        }

    });


    $('#Vanitylogin').click(function (event) {
        
        event.preventDefault();

        if ($('#Username').val() == "" || $('#Username').val().toLowerCase() == 'username') {

            $.msgBox({
                title: "Login",
                content: "Enter your Username.",
                type: "Info",
                modal: true,
                buttons: [{ type: "submit", value: "Ok"}],
                afterShow: function () { $('[name=Ok]').focus(); }
            });
        }
        else if ($('#Password').val() == "" || $('#Password').val().toLowerCase() == 'password') {

            //$('#password').focus();
            $.msgBox({
                title: "Login",
                content: "Enter your Password.",
                type: "Info",
                modal: true,
                buttons: [{ value: "Ok"}],
                afterShow: function () { $('[name=Ok]').focus(); }
            });

        }
        else if ($('#Username').val() != "" && $('#Password').val() != "") {

            $.ajax({
                type: "GET",
                url: '@Url.Action("Authenticate", "User")',
                global: false,
                cache: false,
                data: { userName: $('#Username').val(), password: $('#Password').val(), returnUrl: url + "/" },
                dataType: "json",
                beforeSend: function () {
                    var imgSrc = '@Url.Content("~/Content/Images/loading.gif")';

                    $(".ui-dialog-titlebar").hide();
                    $('#loading').html("Please wait.... <img src=" + imgSrc + " />");
                    theDialog.dialog('open');
                },
                success: function (msg) {

                    theDialog.dialog('close');
                    if (msg.Success) {

                        window.location = msg.Url;
                    }

                    if (msg.Error) {

                        $.msgBox({
                            title: "Login",
                            content: msg.Error,
                            type: "Error",
                            buttons: [{ value: "Ok"}],
                            afterShow: function () { $('[name=Ok]').focus(); }
                        });
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {

                    theDialog.dialog('close');
                    //alert(XMLHttpRequest.responseText); alert(textStatus); alert(errorThrown);


                    $(".ui-dialog-titlebar").show();
                    $(".ui-dialog-titlebar").text("Login");
                    //$('#validation').html(msg);
                    $('#validation').html(textStatus + ":" + errorThrown);
                    validationDialog.dialog('open');
                }
            });
        }
    });

    /*
    if ((navigator.userAgent.match('.NET CLR 2.0.50727') ||
             navigator.userAgent.match('.NET CLR 3.5.30729') ||
             navigator.userAgent.match('.NET CLR 3.0.30729') ||
             navigator.userAgent.match('.NET4.0C') ||
             navigator.userAgent.match('.NET4.0E'))
           && navigator.userAgent.match('rv:11.0')) {
        $.msgBox({
            title: "Home",
            content: "PlanGuru Analytics is currently not compatible with Internet Explorer 11. Please access the application using Chrome, Firefox, or Safari.",
            type: "Info",
            modal: true,
            buttons: [{ value: "Ok"}],
            afterShow: function () { $('[name=Ok]').focus(); }
        });
    }*/

</script>

