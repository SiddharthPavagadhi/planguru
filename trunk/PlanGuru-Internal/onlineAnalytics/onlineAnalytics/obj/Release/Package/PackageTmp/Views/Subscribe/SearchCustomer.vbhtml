﻿@ModelType PagedList.IPagedList(Of onlineAnalytics.Customer)
@Imports  onlineAnalytics
@Code
    ViewData("Title") = "PlanGuru Analytics | Search Customer"
    If Not Session("action") Is Nothing Then
        
        If (Session("action") = "SetDefaultAnalysis" Or Session("action") = "GetAnalyses") Then
            Layout = Nothing
            Session.Remove("action")
        End If
    Else
        Layout = "~/Views/Shared/AnalyticsMaster.vbhtml"
    End If
    
    Dim index As Integer = 1
    
    Dim ua As New UserAccess
    If Not (Session("UserAccess") Is Nothing) Then
        ua = DirectCast(Session("UserAccess"), UserAccess)
    End If
End Code
<div class="headerTable" style="float: right">
    <table>
        <tr>
            <td align="left" width="70%">
                Search Customer
            </td>
            @Code
                If (ua.ViewUser = True) Then
                    'Below href function Jquery function call written in AnalyticsMaster.vbhtml on $(".add").click     
                @<td align="right" width="12%" class="add button_example" href='@Url.Action("Index", "User")'>
                    <img src='@Url.Content("~/Content/Images/List.png")' class="usersIcon" style="margin-right:5px;float:left;"/>User
                    List
                </td>
                End If
            End Code
        </tr>
    </table>
</div>
@Using Html.BeginForm("SearchCustomer", "Subscribe")
    @Html.AntiForgeryToken()   
    @Html.ValidationSummary(True)      
    @<fieldset style="float: left;">
        @Html.Partial("_SearchCustomerPartial", New onlineAnalytics.Customer)
        <div style="float: left; margin-left: 20px;">
            @If Not (DirectCast(TempData("Message"), String) Is Nothing) Then
                @<label class="success">@TempData("Message").ToString()
                </label>                         
    End If
            @If Not (DirectCast(TempData("ErrorMessage"), String) Is Nothing) Then
                @<label class="error">@TempData("ErrorMessage").ToString()
                </label>                         
    End If
        </div>
         @If Model.Count > 0 Then
                             
                  @<div class="CSSTableGenerator">
            <table>
                <thead>
                    <tr>
                        <th style="width: 15%">
             Customer(No)
                        </th>
                        <th style="width: 30%">
                            Customer Full Name
                        </th>
                        <th style="width: 20%">
                            Customer Email
                        </th>
                        <th style="width: 30%">
                            Company Name
                        </th>
                        <th style="width: 5%">
                            Edit
                        </th>
                    </tr>
                </thead>
                @For Each item In Model
        Dim currentItem = item
        Dim cssClass As String = If(index Mod 2 = 0, "even", "")
                  
                    @<tr class='@cssClass'>
                        <td>
                            @Html.DisplayFor(Function(modelItem) currentItem.CustomerId)
                        </td>
                        <td>
                            @Html.DisplayFor(Function(modelItem) currentItem.CustomerFullName)
                        </td>
                        <td>
                            @Html.DisplayFor(Function(modelItem) currentItem.CustomerEmail)
                        </td>
                        <td>
                            @Html.DisplayFor(Function(modelItem) currentItem.CustomerCompanyName)
                        </td>
                        <td>
                            <a href='@Url.Action("PlanGuru_EditSubscription", "Subscribe", New With {.customerId = currentItem.CustomerId})'>
                                <img alt="Edit Subscription" src='@Url.Content("~/Content/Images/edit.png")' title="Edit Subscription" />
                            </a>
                        </td>
                    </tr>
        index += 1
    Next
            </table>
        </div>
             
         End If
    </fieldset>
   
   
End Using


<script type="text/javascript" language="javascript">


    $("#resetBtn").click(function () {
        $("#SearchCustId").val('');
        $("#SearchCustName").val('');
        $("#SAUName").val('');
        $("#SearchCustTelephone").val('');
        $(".CSSTableGenerator").hide();
    });
    $(window).load(function () {
        $(".wrapper").show();
    })
    
</script>