﻿@ModelType  onlineAnalytics.ReportHighChart
@Imports Farpoint.Web.Spread
@Imports Farpoint.Mvc.Spread
@Imports  onlineAnalytics
@Code    
    ViewData("Title") = "PlanGuru Analytics | Balance Sheet Saved Report"
    If Not Session("action") Is Nothing Then
        
        If (Session("action") = "SetDefaultAnalysis" Or Session("action") = "GetAnalyses") Then
            Layout = Nothing
            Session.Remove("action")
        End If
    Else
        Layout = "~/Views/Shared/AnalyticsMaster.vbhtml"
    End If
    
    Dim UserInfo As User = Nothing
    Dim ua As New UserAccess
    
    Dim objSetup As New SetupCount
    
    If Not (Session("UserInfo") Is Nothing) Then
        UserInfo = DirectCast(Session("UserInfo"), User)
    End If
    
    If (Not ViewBag.Setup Is Nothing) Then
        objSetup = DirectCast(ViewBag.Setup, SetupCount)
    End If
End Code
<script src="@Url.Content("~/Scripts/Highcharts.js")" type="text/javascript"></script>
<script src="@Url.Content("~/Scripts/Exporting.js")" type="text/javascript"></script>
<script src="@Url.Content("~/Scripts/regression.js")" type="text/javascript"></script>
<script src="@Url.Content("~/Scripts/Highslide-full.min.js")" type="text/javascript"></script>
<script src="@Url.Content("~/Scripts/Highslide.config.js")" type="text/javascript"></script>
<link href="@Url.Content("~/Content/css/highslide.css")" rel="stylesheet" type="text/css" />
<script type="text/javascript">
    var donutData = new Array();
    var counter = 0;
    var donutChartTitle = "";
    var donutChartIndex;
    var numberOfColumns;
    var showAsPercentageForDonut = false;
    $(function () {
        window.ViewerControlId = "#ALCView";   
        var model = @Html.Raw(Json.Encode(Model));

        hs.Expander.prototype.onAfterExpand = addChart;

        function addChart() {
            var finalData = new Array();
            var chart = $("#hc-test" + counter).highcharts();
            if (chart) {
                chart.destroy();
            }
            for(var k = 0 ; k < donutData.length ; k++)
            {
                if((k == donutChartIndex) || ((k % numberOfColumns) == donutChartIndex))
                {
                    if(donutData[k].y >= 0)
                    {
                        finalData.push(donutData[k]);
                    }
                }

            }

            $("#hc-test" + counter).highcharts({

                chart: {
                    type: 'pie'
                },
                title: {
                    text: donutChartTitle,
                    style: {
                        color: '#000000',
                        fontWeight: 'bold',
                        backgroundColor: '#d3d3d3',
                        fontSize: '16px'
                    }
                },
                yAxis: {

                },
                plotOptions: {
                    pie: {
                        shadow: false
                    }
                },
                tooltip: {



                    formatter: function() {
                        if(showAsPercentageForDonut == false)
                        {
                            return this.point.name +': <b>' + formatcurrency(this.y,0) + ' : '+ Math.round(this.percentage*100)/100 + ' % </b>';
                        }
                        else
                        {
                            return this.point.name +':<b>' + formatcurrency(this.y,2) + '%</b>';
                        }
                    }
                },
                credits: {
                    enabled : false
                },
                exporting: {
                    enabled: false
                },
                series: [{
                    data: finalData,
                    size: '100%',
                    innerSize: '40%',
                    showInLegend:true,
                    dataLabels: {
                        enabled: false
                    }
                }]
            });


        }

        function formatcurrency(value, decPlaces) {
            if (decPlaces != 0) {
                return value.toFixed(decPlaces).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString();
            }
            else {
                return parseInt(value).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
            }
        }

        function GetTrendComment(YaxisData, percentLabel, intFormat,isThousand, decPlaces) {
            var comment;
            if (YaxisData.length > 1) {
                var differenceValue = (fitData(YaxisData).data[1][1] - fitData(YaxisData).data[0][1]);
                if (percentLabel == false) {
                    if (differenceValue > 0) {
                        comment = "Trend - Increasing at rate of " + differenceValue.toFixed(decPlaces);
                    }
                    else if (differenceValue < 0) {
                        comment = "Trend - Decreasing at rate of " + (-differenceValue).toFixed(decPlaces);
                    }
                    else if (differenceValue == 0) {
                        comment = "Trend - Increasing at rate of " + 0;
                    }
                }
                else {
                    if (differenceValue > 0) {
                        comment = "Trend - Increasing at rate of " + (differenceValue).toFixed(decPlaces);
                    }
                    else if (differenceValue < 0) {
                        comment = "Trend - Decreasing at rate of " + ((-differenceValue)).toFixed(decPlaces);
                    }
                    else if (differenceValue == 0) {
                        comment = "Trend - Increasing at rate of " + 0;
                    }
                }

                if (percentLabel == true) {
                    comment = comment + "%"
                }

                if(isThousand == true)
                {
                    comment = comment + " thousand"
                }

                if (intFormat == 0 || intFormat == 2 || intFormat == 3 || intFormat == 5 || intFormat == 6 || intFormat == 9) {
                    comment = comment + " per month";
                }
                else {
                    comment = comment + " per year";
                }
            }
            else if (percentLabel == true) {
                comment = "Trend - Increasing at rate of " + 0 + "% per month";
            }
            else {
                comment = "Trend - Increasing at rate of " + 0 + " per month";
            }
            return comment;
        }

        function GetSeriesColor(SeriesIndex)
        {
            var colorCode;
            switch (SeriesIndex) {
                case 0:
                    colorCode = '#1083a3';
                    break;
                case 1:
                    colorCode = '#f19632';
                    break;
                case 2:
                    colorCode = '#66b09d';
                    break;
                case 3:
                    colorCode = '#dd5822';
                    break;
                case 4:
                    colorCode = '#0e6684';
                    break;
                case 5:
                    colorCode = '#eecf4d';
                    break;
                case 6:
                    colorCode = '#90ac47';
                    break;
                case 7:
                    colorCode = '#c7341f'
                    break;
                case 8:
                    colorCode = '#139fc2'
                    break;
                case 9:
                    colorCode = '#f27723'
                    break;
                case 10:
                    colorCode = '#1083a3';
                    break;
                case 11:
                    colorCode = '#f19632';
                    break;
                case 12:
                    colorCode = '#66b09d';
                    break;
                case 13:
                    colorCode = '#dd5822';
                    break;
                case 14:
                    colorCode = '#0e6684';
                    break;
                case 15:
                    colorCode = '#eecf4d';
                    break;
                case 16:
                    colorCode = '#90ac47';
                    break;
                case 17:
                    colorCode = '#c7341f'
                    break;
                case 18:
                    colorCode = '#139fc2'
                    break;
                case 19:
                    colorCode = '#f27723'
                    break;
                case 20:
                    colorCode = '#1083a3';
                    break;
                case 21:
                    colorCode = '#f19632';
                    break;
                case 22:
                    colorCode = '#66b09d';
                    break;
                case 23:
                    colorCode = '#dd5822';
                    break;
                case 24:
                    colorCode = '#0e6684';
                    break;
                case 25:
                    colorCode = '#eecf4d';
                    break;
                case 26:
                    colorCode = '#90ac47';
                    break;
                case 27:
                    colorCode = '#c7341f'
                    break;
                case 28:
                    colorCode = '#139fc2'
                    break;
                case 29:
                    colorCode = '#f27723'
                    break;
                default:
                    colorCode = '#1083a3';
            }
            return colorCode;
        }

        function getCursorValue(selectedRow)
        {
            if(selectedRow > 1)
            {
                return 'pointer';
            }
            else
            {
                return null;
            }
        }

        function getCrossHairValue(selectedRow)
        {
            if(selectedRow > 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        if(model != null)
        {
            if(model.isHttpPost == false)
            {
                $("#chart").hide();
            }
            else
            {
                $("#chart").show();
                /* Chart width related Logic */
                if(model.Format == 2 || model.Format == 0 || model.Format == 3)
                {
                    $("#chart").width(parseInt(606)).height(200);
                }
                if(model.Format == 5)
                {
                    if(model.NumbofCols == 4 || model.NumbofCols == 8)
                    {
                        $("#chart").width((parseInt(model.chartWidth.replace('px','') - 65))).height(200);
                    }
                    else
                    {
                        $("#chart").width((parseInt(model.chartWidth.replace('px','') - 18))).height(200);
                    }
                }
                if(model.Format == 6 || model.Format == 7 || model.Format == 8 || model.Format == 9 || model.Format == 12)
                {
                    $("#chart").width((parseInt(model.chartWidth.replace('px','') - 18))).height(200);
                }
                if(model.Format == 10 || model.Format == 11)
                {
                    $("#chart").width((parseInt(model.chartWidth.replace('px','') - 78))).height(200);
                }
                /* End Chart width related Logic */
                var xAxisData = new Array();
                var series1yAxisData = new Object();
                var series2yAxisData = new Object();
                var trendComment;

                if(model.Format == 0 || model.Format == 2 || model.Format == 3)
                {
                    if(model.SeriesData != "" && model.SeriesData != null)
                    {
                        for (var k = 0 ; k < model.SeriesData[0].SeriesData.length; k++) {
                            xAxisData.push(model.SeriesData[0].SeriesData[k]);
                        }
                    }

                    if(model.Format == 0 || model.Format == 3)
                    {
                        if(model.SelectedRows.length == 1)
                        {
                            series1yAxisData.Name = model.SeriesData[0].SeriesTitle;
                            series1yAxisData.Data = new Array();
                            for(var j = 0 ; j < 1 ; j++)
                            {
                                series1yAxisData.Data.push(model.OptionalYAxisData[j]);
                            }

                            series2yAxisData.Name = model.SeriesData[1].SeriesTitle;
                            series2yAxisData.Data = new Array();
                            for(var j = 1 ; j < 2 ; j++)
                            {
                                series2yAxisData.Data.push(model.OptionalYAxisData[j]);
                            }

                        }


                        else if(model.SelectedRows.length == 2)
                        {
                            series1yAxisData.Name = model.SeriesData[0].SeriesTitle;
                            series1yAxisData.Data = new Array();
                            for(var j = 0 ; j < 2 ; j++)
                            {
                                series1yAxisData.Data.push(model.OptionalYAxisData[j]);
                            }

                            series2yAxisData.Name = model.SeriesData[1].SeriesTitle;
                            series2yAxisData.Data = new Array();
                            for(var j = 2 ; j < 4 ; j++)
                            {
                                series2yAxisData.Data.push(model.OptionalYAxisData[j]);
                            }

                        }
                        else if(model.SelectedRows.length > 2)
                        {
                            series1yAxisData.Name = model.SeriesData[0].SeriesTitle;
                            series1yAxisData.Data = new Array();
                            for(var j = 0 ; j < 3 ; j++)
                            {
                                series1yAxisData.Data.push(model.OptionalYAxisData[j]);
                            }

                            series2yAxisData.Name = model.SeriesData[1].SeriesTitle;
                            series2yAxisData.Data = new Array();
                            for(var j = 3 ; j < 6 ; j++)
                            {
                                series2yAxisData.Data.push(model.OptionalYAxisData[j]);
                            }
                        }
                    }

                    if(model.Format == 2)
                    {
                        if(model.SelectedRows.length == 1)
                        {
                            series1yAxisData.Name = model.SeriesData[0].SeriesTitle;
                            series1yAxisData.Data = new Array();
                            for(var j = 0 ; j < 1 ; j++)
                            {
                                series1yAxisData.Data.push(model.OptionalYAxisData[j]);
                            }

                            series2yAxisData.Name = model.SeriesData[1].SeriesTitle;
                            series2yAxisData.Data = new Array();
                            for(var j = 1 ; j < 2 ; j++)
                            {
                                series2yAxisData.Data.push(model.OptionalYAxisData[j]);
                            }

                        }


                        else if(model.SelectedRows.length == 2)
                        {
                            series1yAxisData.Name = model.SeriesData[0].SeriesTitle;
                            series1yAxisData.Data = new Array();
                            for(var j = 0 ; j < 2 ; j++)
                            {
                                series1yAxisData.Data.push(model.OptionalYAxisData[j]);
                            }

                            series2yAxisData.Name = model.SeriesData[1].SeriesTitle;
                            series2yAxisData.Data = new Array();
                            for(var j = 2 ; j < 4 ; j++)
                            {
                                series2yAxisData.Data.push(model.OptionalYAxisData[j]);
                            }

                        }
                        else if(model.SelectedRows.length > 2)
                        {
                            series1yAxisData.Name = model.SeriesData[0].SeriesTitle;
                            series1yAxisData.Data = new Array();
                            for(var j = 0 ; j < 3 ; j++)
                            {
                                series1yAxisData.Data.push(model.OptionalYAxisData[j]);
                            }

                            series2yAxisData.Name = model.SeriesData[1].SeriesTitle;
                            series2yAxisData.Data = new Array();
                            for(var j = 3 ; j < 6 ; j++)
                            {
                                series2yAxisData.Data.push(model.OptionalYAxisData[j]);
                            }
                        }
                    }




                    $("#chart").highcharts({

                        chart: {
                            backgroundColor: "#f7f6f4",
                            type: model.HighChartType,
                        },
                        title: {
                            text: model.ChartDesc,
                            style: {
                                color: '#276FA6',
                                fontWeight: 'bold',
                                backgroundColor: '#d3d3d3',
                                fontSize: '14px'
                            }
                        },
                        xAxis: {
                            categories: xAxisData,
                            lineColor: '#000000',
                            tickLength: 8,
                            tickWidth: 1,
                            tickColor: "#000000"
                        },

                        yAxis: {
                            lineWidth: 1,
                            lineColor: '#000000',
                            gridLineColor: '#000000',
                            gridLineWidth: 1,
                            tickLength: 8,
                            tickWidth: 1,
                            tickColor: "#000000",
                            labels: {
                                formatter: function () {
                                    if(model.ShowasPerc == false)
                                    {
                                        return formatcurrency(this.value, 0);
                                    }
                                    else
                                    {
                                        return (Math.round(this.value * 100) + '%');
                                    }
                                }
                            },
                            title: {
                                text: ''
                            }

                        },

                        tooltip: {
                            formatter: function () {
                                if(model.ShowasPerc == false)
                                {
                                    return '<b>' + this.series.name + '</b><br/>' + this.key + ' :<b> ' + formatcurrency(this.y, 0) + '</b>';
                                }
                                else
                                {
                                    return '<b>' + this.series.name + '</b><br/>' + this.key + ' :<b> ' + Highcharts.numberFormat((this.y * 100), 2, '.') + '%</b>';
                                }
                            }
                        },
                        credits: {
                            enabled: false
                        },
                        exporting:
                            {
                                enabled: false
                            },
                        series: [{
                            data: series1yAxisData.Data,
                            name: series1yAxisData.Name,
                            color: '#1083a3'
                        },
                        {
                            data: series2yAxisData.Data,
                            name: series2yAxisData.Name,
                            color: '#f19632'
                        }],
                        legend: {
                            align:'left',
                            verticalAlign: 'top',
                            floating: false,
                            layout:'vertical'
                        }
                    });
                }
                else
                {
                    var cursorValue = getCursorValue(model.SelectedRows.length);
                    var crossHairValue = getCrossHairValue(model.SelectedRows.length);

                    if(model.SeriesData != "" && model.SeriesData != null)
                    {
                        for (var k = 0 ; k < model.SeriesData[0].SeriesData.length; k++) {
                            xAxisData.push(model.SeriesData[0].SeriesData[k]);
                        }
                    }

                    if(model.ChartType == 3)
                    {

                        $("#chart").highcharts({

                            chart: {
                                backgroundColor: "#f7f6f4",
                                type: model.HighChartType,
                            },
                            title: {
                                text: model.ChartDesc,
                                style: {
                                    color: '#276FA6',
                                    fontWeight: 'bold',
                                    backgroundColor: '#d3d3d3',
                                    fontSize: '14px'
                                }
                            },
                            xAxis: {
                                categories: xAxisData,
                                lineColor: '#000000',
                                tickLength: 8,
                                tickWidth: 1,
                                tickColor: "#000000"
                            },

                            yAxis: {
                                lineWidth: 1,
                                lineColor: '#000000',
                                gridLineColor: '#000000',
                                gridLineWidth: 1,
                                tickLength: 8,
                                tickWidth: 1,
                                tickColor: "#000000",
                                labels: {
                                    formatter: function () {
                                        if(model.ShowasPerc == false)
                                        {
                                            return formatcurrency(this.value, 0);
                                        }
                                        else
                                        {
                                            return this.value + '%';
                                        }
                                    }
                                },
                                title: {
                                    text: ''
                                }

                            },
                            tooltip: {
                                formatter: function () {
                                    if(model.ShowasPerc == false)
                                    {
                                        return '<b>' + this.series.name + '</b><br/>' + this.key + ' :<b> ' + formatcurrency(this.y, 0) + '</b>';
                                    }
                                    else
                                    {
                                        return '<b>' + this.series.name + '</b><br/>' + this.key + ' :<b> ' + formatcurrency(this.y, 2) + '%</b>';
                                    }
                                },
                                crosshairs: crossHairValue
                            },
                            plotOptions: {
                                column: {
                                    stacking: 'normal',
                                    dataLabels: {
                                        enabled: false,
                                        color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
                                        style: {
                                            textShadow: '0 0 3px black'
                                        }
                                    }
                                },
                                series:{
                                    cursor:cursorValue

                                }
                            },

                            credits: {
                                enabled: false
                            },
                            exporting:
                                {
                                    enabled: false
                                },
                            series: [

                            ],
                            legend: {
                                align:'left',
                                verticalAlign: 'top',
                                floating: false,
                                layout:'vertical',
                                width:130,
                                labelFormatter: function() {
                                    var words = this.name.split(/[\s]+/);
                                    var numWordsPerLine = 2;
                                    var str = [];

                                    for (var word in words) {
                                        if (word > 0 && word % numWordsPerLine == 0)
                                            str.push('<br>');

                                        str.push(words[word]);
                                    }

                                    return str.join(' ');
                                }
                            }
                        });
                    }
                    else if(model.showTrend == true && model.ChartType == 4)
                    {
                        var singleSeriesData = new Array();
                        for(var j = 0; j < model.NumbofCols; j++)
                        {
                            singleSeriesData.push(model.ChartValues[j]);
                        }
                        trendComment = GetTrendComment(singleSeriesData,model.ShowasPerc,model.Format,model.isThousand,0)
                        $("#chart").highcharts({

                            chart: {
                                backgroundColor: "#f7f6f4",
                                type: model.HighChartType,
                            },
                            title: {
                                text: model.ChartDesc,
                                style: {
                                    color: '#276FA6',
                                    fontWeight: 'bold',
                                    backgroundColor: '#d3d3d3',
                                    fontSize: '14px'
                                }
                            },
                            xAxis: {
                                categories: xAxisData,
                                lineColor: '#000000',
                                tickLength: 8,
                                tickWidth: 1,
                                tickColor: "#000000"
                            },

                            yAxis: {
                                lineWidth: 1,
                                lineColor: '#000000',
                                gridLineColor: '#000000',
                                gridLineWidth: 1,
                                tickLength: 8,
                                tickWidth: 1,
                                tickColor: "#000000",
                                labels: {
                                    formatter: function () {
                                        if(model.ShowasPerc == false)
                                        {
                                            return formatcurrency(this.value, 0);
                                        }
                                        else
                                        {
                                            return this.value + '%';
                                        }

                                    }
                                },
                                title: {
                                    text: ''
                                }

                            },
                            tooltip: {
                                formatter: function () {
                                    if(model.ShowasPerc == false)
                                    {
                                        return '<b>' + this.series.name + '</b><br/>' + this.key + ' :<b> ' + formatcurrency(this.y, 0) + '</b>';
                                    }
                                    else
                                    {
                                        return '<b>' + this.series.name + '</b><br/>' + this.key + ' :<b> ' + formatcurrency(this.y, 2) + '%</b>';
                                    }
                                }
                            },

                            credits: {
                                enabled: true,
                                text: trendComment,
                                position: {
                                    align: 'center'
                                },
                                style: {
                                    color: 'black',
                                    fontSize: '12px',
                                    marginTop: '5px',
                                    marginBottom: '5px'
                                },
                                href: '#'
                            },
                            exporting:
                                {
                                    enabled: false
                                },
                            series: [

                            ],
                            legend: {
                                align:'left',
                                verticalAlign: 'top',
                                floating: false,
                                layout:'vertical',
                                width:130,
                                labelFormatter: function() {
                                    var words = this.name.split(/[\s]+/);
                                    var numWordsPerLine = 2;
                                    var str = [];

                                    for (var word in words) {
                                        if (word > 0 && word % numWordsPerLine == 0)
                                            str.push('<br>');

                                        str.push(words[word]);
                                    }

                                    return str.join(' ');
                                }
                            }
                        });
                    }
                    else
                    {
                        $("#chart").highcharts({

                            chart: {
                                backgroundColor: "#f7f6f4",
                                type: model.HighChartType,
                            },
                            title: {
                                text: model.ChartDesc,
                                style: {
                                    color: '#276FA6',
                                    fontWeight: 'bold',
                                    backgroundColor: '#d3d3d3',
                                    fontSize: '14px'
                                }
                            },
                            xAxis: {
                                categories: xAxisData,
                                lineColor: '#000000',
                                tickLength: 8,
                                tickWidth: 1,
                                tickColor: "#000000"
                            },

                            yAxis: {
                                labels: {
                                    formatter: function () {
                                        if(model.ShowasPerc == false)
                                        {
                                            return formatcurrency(this.value, 0);
                                        }
                                        else
                                        {
                                            return this.value + '%';
                                        }

                                    }
                                },
                                title: {
                                    text: ''
                                }

                            },
                            plotOptions: {
                                series:{
                                    cursor:cursorValue
                                }
                            },
                            tooltip: {
                                formatter: function () {
                                    if(model.ShowasPerc == false)
                                    {
                                        return '<b>' + this.series.name + '</b><br/>' + this.key + ' :<b> ' + formatcurrency(this.y, 0) + '</b>';
                                    }
                                    else
                                    {
                                        return '<b>' + this.series.name + '</b><br/>' + this.key + ' :<b> ' + formatcurrency(this.y, 2) + '%</b>';
                                    }
                                },
                                crosshairs: crossHairValue
                            },

                            credits: {
                                enabled: false
                            },
                            exporting:
                                {
                                    enabled: false
                                },
                            series: [

                            ],
                            legend: {
                                align:'left',
                                verticalAlign: 'top',
                                floating: false,
                                layout:'vertical',
                                width:130,
                                labelFormatter: function() {
                                    var words = this.name.split(/[\s]+/);
                                    var numWordsPerLine = 2;
                                    var str = [];

                                    for (var word in words) {
                                        if (word > 0 && word % numWordsPerLine == 0)
                                            str.push('<br>');

                                        str.push(words[word]);
                                    }

                                    return str.join(' ');
                                }
                            }
                        });
                    }

                    if(model.showTrend == false)
                    {
                        var i = model.NumbofCols;
                        var l = 1;
                        if(l <= i)
                        {
                            for(var k = 0 ; k < model.SelectedRows.length ; k++)
                            {


                                var singleSeriesData = new Array();
                                var seriesColor = GetSeriesColor(k);
                                var name = model.SeriesData[k].SeriesTitle.replace(/\n/g, " ");

                                for(var j = ((i * l) - i) ; j < (i * l) ; j++)
                                {
                                    singleSeriesData.push(model.ChartValues[j]);
                                    donutData.push({name:name,y:model.ChartValues[j], color: seriesColor });
                                }

                                var data = singleSeriesData;
                                if(model.SelectedRows.length > 1)
                                {
                                    $("#chart").highcharts().addSeries({name : name , data : data, color: seriesColor,
                                        events: {
                                            click: function (event)
                                            {
                                                counter++;
                                                donutChartIndex = event.point.index;
                                                if(model.isThousand == true)
                                                {
                                                    donutChartTitle = event.point.category +  " (in thousands)";
                                                }
                                                else
                                                {
                                                    donutChartTitle = event.point.category;
                                                }
                                                numberOfColumns = model.NumbofCols;
                                                showAsPercentageForDonut = model.ShowasPerc;
                                                hs.htmlExpand(null, {
                                                    pageOrigin: {
                                                        x: (event.screenX),
                                                        y: (event.screenY)
                                                    },
                                                    maincontentText: '<div id="hc-test' + counter + '"></div>',
                                                    width: 530,
                                                    height: 473
                                                });
                                            }
                                        }
                                    });
                                }
                                else
                                {
                                    $("#chart").highcharts().addSeries({name : name , data : data, color: seriesColor});
                                }
                                l++;
                            }
                        }

                    }

                    if(model.showTrend == true && model.ChartType == 4)
                    {
                        var singleSeriesData = new Array();
                        var name = model.SeriesData[0].SeriesTitle.replace(/\n/g, " ");

                        for(var j = 0; j < model.NumbofCols; j++)
                        {
                            singleSeriesData.push(model.ChartValues[j]);
                        }

                        var data = singleSeriesData;

                        $("#chart").highcharts().addSeries({name : name , data : data,color: '#1083a3'})
                        $("#chart").highcharts().addSeries({
                            type: 'line',
                            name: 'Trend',
                            color: '#f37823',
                            marker: {
                                enabled: false
                            },
                            data: (function () {
                                return fitData(singleSeriesData).data;
                            })()
                        })
                    }

                }

              



            }

        }
        else
        {
            $("#chart").hide();
        }
    });

    $(window).load(function () {
        $(".wrapper").show();
    })
</script>
<div style="float: left; display: inline-block; width: 100%;">
    @If Not (DirectCast(TempData("Message"), String) Is Nothing) Then
        @<label class="success">@TempData("Message").ToString()
        </label>                         
    End If
    @If Not (DirectCast(TempData("ErrorMessage"), String) Is Nothing) Then
        @<label class="error" style="margin-top: 0px;">@TempData("ErrorMessage").ToString()
        </label>                         
    End If
</div>
<div id="outerwrapper" style="display:none">
    @code   
        @Html.Hidden("countOfDataUploaded", objSetup.countDataUploadedOfSelectedAnalysis)        
        @<label id="validationMessage" class="info" style="display: none;">
            @If ((Not IsNothing(ViewBag.Setup)) AndAlso objSetup.AanlysisCount > 0 And objSetup.selectedAnalysis = 0) Then
                @MvcHtmlString.Create("Please select company & one of the respective analysis from top-menu selection, to get the dashboard view.")
            ElseIf ((Not IsNothing(ViewBag.Setup)) AndAlso objSetup.selectedAnalysis > 0 And objSetup.countDataUploadedOfSelectedAnalysis = 0) Then
                @MvcHtmlString.Create("Data has not been uploaded for selected analysis.")
            End If
        </label> 
        
        If (Not UserInfo Is Nothing) Then
                   
            If (UserInfo.UserRoleId <= UserRoles.SAU) Then
                    
                If (objSetup.CompanyCount = 0 Or objSetup.AanlysisCount = 0) Then
        @<label class="info">You have to create below listed action item, to get the dashboard
            view.</label> 
                End If
                If (objSetup.CompanyCount = 0) Then
        @<div class="secondbutton_example" style='margin: 20px; width: 200px;'>
            <a href='@Url.Action("Create", "Company")' style="text-decoration:none;color: #FFFFFF;">
                Add Company</a>
        </div>  
                End If
                If (objSetup.CompanyCount > 0 And objSetup.AanlysisCount = 0) Then
        @<div class="secondbutton_example" style='margin: 20px; width: 200px;'>
            <a href='@Url.Action("Create", "Analysis")'  style="text-decoration:none;color: #FFFFFF;">
                Add Analysis</a>
        </div>  
                End If
                If (objSetup.CompanyCount = 0 Or objSetup.AanlysisCount = 0) Then
        @<div class="secondbutton_example" style='margin: 20px; width: 200px;'>
            <a href='@Url.Action("CreateUser", "User")'  style="text-decoration:none;color: #FFFFFF;">
                Add User</a>
        </div>  
                End If
            ElseIf (UserInfo.UserRoleId > UserRoles.SAU) Then
                
                If (objSetup.CompanyCount = 0) Then
        @<label class="info">You have not been given access to any company. Please contact your
            administrator. / analysis.</label>
                ElseIf (objSetup.AanlysisCount = 0) Then
        @<label class="info">You have not been given access to any analysis files for the selected
            company. Please contact your administrator.</label> 
                End If
                
            End If
        End If
        
        If (objSetup.CompanyCount > 0 And objSetup.AanlysisCount > 0 And objSetup.selectedAnalysis > 0 And objSetup.countDataUploadedOfSelectedAnalysis > 0) Then
            Using Html.BeginForm
        @<div id="ALCView" style="display: none; width: 100%;">
            <div id="sidebar">
                <div>
                    <img src='@Url.Content("~/Content/images/period.png")' alt="Period" />
                </div>
                <div>
                    @Html.DropDownList("intPeriod", DirectCast(ViewData("Periods"), SelectList), New With {.class = "REselect"})
                </div>
                <br/>
                <div id="optionsimage">
                    <img src='@Url.Content("~/Content/images/options.png")' alt="Options" />
                </div>
                <div id="chkHighlightVar" class="indent0" style="margin-top: 5px;">
                    @Html.CheckBox("blnHighVar", ViewData("blnHighVar")) Highlight variances:
                </div>
                <div id="HighlightVarControls" style="display: none;">
                    <div id="chkHighlightPosVar" class="indent10">
                        @Html.CheckBox("blnHighPosVar", True) + Variances > than:
                        @Html.TextBox("intHighPosVar", ViewData("intPosVarAmt"), New With {.Style = "width: 12px; margin-left: 5px; font-size: 10px; border: 1px solid #DDD;text-align: right; "})%
                        <span id="valintHighPosVar" style="color: red; text-align: right; float: right; width: 165px;
                            margin-right: 10px; display: none;">Numeric characters only.</span>
                    </div>
                    <div id="chkHighlightNegVar" class="indent10">
                        @Html.CheckBox("blnHighNegVar", True) - Variances > than: -
                        @Html.TextBox("intHighNegVar", ViewData("intNegVarAmt"), New With {.Style = "width: 12px; font-size: 10px; border: 1px solid #DDD; text-align: right;"})%
                        <span id="valintHighNegVar" style="color: red; text-align: right; float: right; width: 165px;
                            margin-right: 10px; display: none;">Numeric characters only.</span>
                    </div>
                </div>
                <div id="CheckBoxFilter" class="indent0">
                    @Html.CheckBox("blnUseFilter") Apply filter:
                </div>
                <div id="UseFilterControls" style="display: none;">
                    <div class="indent10">
                        @Html.DropDownList("intFilterOn", DirectCast(ViewData("FilterOn"), SelectList), New With {.class = "REselect"})
                    </div>
                    <div>
                        <label class="indent10">
                            @Html.RadioButton("blnFilterPosVariance", True, New With {.id = "radioPosVar", .checked = "checked", .style = "width: 20px;"})
                            Amounts > than:
                        </label>
                    </div>
                    <div>
                        <label class="indent10">
                            @Html.RadioButton("blnFilterPosVariance", False, New With {.id = "radioNegVar", .style = "width: 20px;"})
                            Amounts < than:
                        </label>
                    </div>
                    <div class="indent30" >
                       @Html.TextBox("decFilterAmt", ViewData("intFilterAmt"), New With {.Style = "width: 50px; margin-left: 5px; font-size: 10px; border: 1px solid #DDD;text-align: right; "})
                        <span id="valDecFilterAmt" style="color: red; text-align: right; float: right; width: 165px;
                            margin-right: 10px; display: none;">Numeric characters only.</span>
                    </div>
                </div>
                <br />
                <div id="UpdateChart" style="display: none; margin: 15px 0 0 0; float: left;">
                    <input id="causepost" type="submit" value="View Chart" class="secondbutton_example" />
                </div> 
                <div>
                    <h2 class="savedViewsListHeading">Saved Reports</h2>
                    <ul id="SavedReportsList" class="savedViewsList">
                    </ul>
                </div>                
            </div>
            <div id="main" style="display: inline-block; width: 79%;">
                <div id="chart" style="display: none;">
                    @*@Html.FpSpread("spdChart", Sub(x)
                                                   x.RowHeader.Visible = False
                                                   x.ColumnHeader.Visible = False
                                                   x.Height = 200
                                                   x.Width = 500
                                           
                                               End Sub)*@
                </div>
                <div id="spread" style="display: none;">
                    @Html.FpSpread("spdAnalytics", Sub(x)
                                                   
                                                       x.RowHeader.Visible = False
                                                       x.ActiveSheetView.PageSize = 1000
                                                   End Sub)
                </div>
            </div>
        </div>   
            End Using
        End If
       
    End Code
</div>
<script type="text/javascript">
        function setSelectedPageNav() {
            var pathName = document.location.pathname;            
            if ($("#SavedReportsList li a") != null) {                
                var currentLink = $("#SavedReportsList li a[href='" + pathName + "']");                
                currentLink.addClass("activelink");
            }
        }           
        $('#intPeriod').change(function () {            
            $('#causepost').trigger('click')
        });

        //on load
        $(document).ready(function () {                            
            IsSessionAlive();
            getSavedReportsList();

            $("input:checkbox").each(function (index) {
                $("<label>").attr("for", this.id)
                .attr("class", "styled_checkbox dashboard")
                .insertAfter(this);
            });

            var selectedindex = @ViewData("intChartType");   
            
            var accID = '@ViewData("AccountID")';
            var splitID = accID.split(" ");
            
            var postback = '@ViewData("Postback")';
            if(postback == "True") $('#outerwrapper').show();
            if(postback == "True" && accID != "" && selectedindex > 1)
            {                
                $('#chart').show()
                $('#chart').css('height', '200px');
            }

            if (selectedindex > 1) {                               
                $('#sidebar').css('height', '800px');                
                var spread = document.getElementById("spdAnalytics");
                var colcount = spread.GetColCount();
                spread.SetColWidth(2, 22);
                if (colcount < 10) {
                    spread.SetColWidth(4, 250);
                }
                else if (colcount > 13) {
                    spread.SetColWidth(4, 150);
                }
                else {
                    spread.SetColWidth(4, 200);
                }
            }
            else {
                $('#sidebar').css('height', '600px')
            }
            $('#spread').show()


//            hide detail rows  
            if (document.getElementById("spdAnalytics")) {
                var spread = document.getElementById("spdAnalytics");
                var rc = spread.GetRowCount();
                var showclass = false
                var showsubtotal = false
                for (var i = 0; i < rc; i++) {
                    
                    // pre-select the account id's in the viewer  
                    for(var j=0; j < splitID.length; j++){
                        if(splitID[j] == spread.GetValue(i, 5)) {
                            
                            spread.SetValue(i, 2, true, false); 
                        }
                    }
                    

                    var cellval = spread.GetValue(i, 3);
                    if (cellval == "Detail") {
                        if (showclass == false) {
                            spread.Rows(i).style.display = "none";
                        }
                    }
                    if (cellval == "SDetail" || cellval == "SHeading" || cellval == "STotal") {
                        if (showclass == false) {
                            spread.Rows(i).style.display = "none";
                        }
                        else if (showclass == true) {
                            if (cellval == "SHeading") {
                                cellval = spread.GetValue(i, 1);
                                if (cellval == "-") {
                                    showsubtotal = true
                                }
                                else {
                                    showsubtotal = false
                                    spread.Rows(i).style.display = "none";
                                }
                            }
                            else if (cellval == "SDetail") {
                                if (showsubtotal == false) {
                                    spread.Rows(i).style.display = "none";
                                }
                            }
                        }
                    }
                    if (cellval == "Heading") {
                        var cellvalue = spread.GetValue(i, 0);
                        if (cellvalue == "-") {
                            showclass = true
                            spread.Rows(i).style.display = "table-row";
                        }
                        else {
                            showclass = false
                            spread.Rows(i).style.display = "none";
                        }
                    }
                }
            }
            
//            ew 6/15 code for highlight and filter
            var selectedFormat = '@ViewData("intFormat")';   
            if (selectedFormat > 4) {
                $('#optionsimage').hide()
                $('#chkHighlightVar').hide()
                $('#HighlightVarControls').hide() 
                $('#CheckBoxFilter').hide()
                $('#UseFilterControls').hide()
            }
            else { 
                if ($("#blnHighVar").is(':checked')) { 
                    var HighPosVar = '@ViewData("blnPosVar")' 
                    if (HighPosVar == "True") { 
                        $('#blnHighPosVar').attr('checked', true);
                    }  
                    var HighNegVar = '@ViewData("blnNegVar")' 
                    if (HighNegVar == "True") { 
                        $('#blnHighNegVar').attr('checked', true);
                    }  
                    $('#HighlightVarControls').show()
                    AddHighlight()
                };
                if ($("#blnUseFilter").is(':checked')) { 
                    $('#UseFilterControls').show()
                    var posvar = '@ViewData("blnFilterPosVar")'  
                    if (posvar == "False") {  
                        $('#radioNegVar').attr('checked', true);
                        $('#radioPosVar').removeAttr('checked');
                    }  
                    AddFilter() 
                }
            }

            //adjust width
            var browsewidth = $(window).width();
            $('#intBrowserWidth').val(browsewidth)
            var spdWidth = $('#spdAnalytics').width();
            var calcwidth = spdWidth + 250;
            if (calcwidth > 1100) {
                if (calcwidth < browsewidth) {                    
                     $(".header").width('1275');
                    $(".nav-panel").width('1275');
                    $(".main-container").width('1275px');
                }
                else {
                }
            }
//            
            if(postback == "False")
            {
                $('#spread').hide();                
                $('#causepost').trigger('click');
            }            
        });

        window.onload = function () {
            if (document.getElementById("spdAnalytics")) {
                var spread = document.getElementById("spdAnalytics");
                if (document.all) {
                    if (spread.addEventListener) {
                        spread.addEventListener("ActiveCellChanged", cellChanged, false);
                    }
                    else {
                        spread.onActiveCellChanged = cellChanged;
                    }
                }
                else {
                    spread.addEventListener("ActiveCellChanged", cellChanged, false);
                }
            }                        
        }

        function cellChanged(event) {
            if (event.col == 0 && document.getElementById("spdAnalytics")) {
                var spread = document.getElementById("spdAnalytics");
                var cellval = spread.GetValue(event.row, 0);
                var showsubtotal = false
                var rc = spread.GetRowCount();
                if (cellval == "-") {
                    for (var i = event.row; i < rc; i++) {
                        var rowtype = spread.GetValue(i, 3);
                        if (rowtype == "Total") {
                            cell = document.getElementById("spdAnalytics").GetCellByRowCol(i, 0);
                            cell.removeAttribute("FpCellType");
                            spread.SetValue(i, 0, "+", false);
                            cell.setAttribute("FpCellType", "readonly");
                            spread.Rows(i).style.display = "table-row";
                            break;
                        }
                        else if (rowtype == "Heading") {
                            cell = document.getElementById("spdAnalytics").GetCellByRowCol(i, 0);
                            cell.removeAttribute("FpCellType");
                            spread.SetValue(i, 0, "+", false);
                            cell.setAttribute("FpCellType", "readonly");
                            spread.Rows(i).style.display = "none";
                        }
                        else {

                            spread.Rows(i).style.display = "none";
                        }
                    }
                }
                else if (cellval == "+") {
                    for (var i = event.row; i > -1; i--) {
                        var rowtype = spread.GetValue(i, 3);
                        if (rowtype == "Total") {
                            cell = document.getElementById("spdAnalytics").GetCellByRowCol(i, 0);
                            cell.removeAttribute("FpCellType");
                            spread.SetValue(i, 0, "", false);
                            cell.setAttribute("FpCellType", "readonly");
                            if ($('#blnUseFilter').is(':checked')) {
                                spread.Rows(i).style.display = "none";
                            }
                        }
                        else if (rowtype == "Heading") {
                            cell = document.getElementById("spdAnalytics").GetCellByRowCol(i, 0);
                            cell.removeAttribute("FpCellType");
                            spread.SetValue(i, 0, "-", false);
                            cell.setAttribute("FpCellType", "readonly");
                            spread.Rows(i).style.display = "table-row";
                            break
                        }
                        else if (rowtype == "SHeading") {
                            if (showsubtotal == true) {
                                spread.Rows(i).style.display = "table-row";
                            }
                        }
                        else if (rowtype == "SDetail") {
                            if (showsubtotal == true) {
                                spread.Rows(i).style.display = "table-row";
                            }
                        }
                        else if (rowtype == "STotal") {
                            cellval = spread.GetValue(i, 1);
                            if (cellval == "") {
                                showsubtotal = true
                            }
                            if (cellval == "+") {
                                showsubtotal = false
                            }
                            spread.Rows(i).style.display = "table-row";
                        }
//                        else if ($('#blnUseFilter').is(':checked')) {
//                            HideUnFilterRow(i)
//                        }
                        else {
                            spread.Rows(i).style.display = "table-row";
                        }
                    }
                }
                spread.SetActiveCell(event.row, 2)
            }
            if (event.col == 1 && document.getElementById("spdAnalytics")) {
                var spread = document.getElementById("spdAnalytics");
                var cellval = spread.GetValue(event.row, 1);
                var rc = spread.GetRowCount();
                if (cellval == "-") {
                    for (var i = event.row; i < rc; i++) {
                        var rowtype = spread.GetValue(i, 3);
                        if (rowtype == "SHeading") {
                            cell = document.getElementById("spdAnalytics").GetCellByRowCol(i, 1);
                            cell.removeAttribute("FpCellType");
                            spread.SetValue(i, 1, "+", false);
                            cell.setAttribute("FpCellType", "readonly");
                            spread.Rows(i).style.display = "none";
                        }
                        if (rowtype == "STotal") {
                            cell = document.getElementById("spdAnalytics").GetCellByRowCol(i, 1);
                            cell.removeAttribute("FpCellType");
                            spread.SetValue(i, 1, "+", false);
                            cell.setAttribute("FpCellType", "readonly");
                            break
                        }
                        else {
                            spread.Rows(i).style.display = "none";
                        }
                    }
                }
                if (cellval == "+") {
                    for (var i = event.row; i > -1; i--) {
                        var rowtype = spread.GetValue(i, 3);
                        if (rowtype == "STotal") {
                            cell = document.getElementById("spdAnalytics").GetCellByRowCol(i, 1);
                            cell.removeAttribute("FpCellType");
                            spread.SetValue(i, 1, "", false);
                            cell.setAttribute("FpCellType", "readonly");
                        }
                        if (rowtype == "SHeading") {
                            cell = document.getElementById("spdAnalytics").GetCellByRowCol(i, 1);
                            cell.removeAttribute("FpCellType");
                            spread.SetValue(i, 1, "-", false);
                            cell.setAttribute("FpCellType", "readonly");
                            spread.Rows(i).style.display = "table-row";
                            break
                        }
                        else {
                            spread.Rows(i).style.display = "table-row";
                        }
                    }
                }

            }
        }

        $('#blnHighVar').change(function () {
            if ($(this).is(':checked')) {
                $('#HighlightVarControls').show()
                 if ($("#blnUseFilter").is(':checked')) {
                    $('#UseFilterControls').hide()
                    $('#blnUseFilter').removeAttr('checked');
                    RemoveFilter()
                };    
                AddHighlight()
            }
            else {
                $('#HighlightVarControls').hide()
                RemoveBackgroundColor()

            }
        });

        $('#blnHighPosVar').change(function () {
            AddHighlight()
        })

        $('#blnHighNegVar').change(function () {
            AddHighlight()
        })

        $('#intHighPosVar').change(function () {
            AddHighlight()
        })

        $('#intHighNegVar').change(function () {
            AddHighlight()
        })

        function AddHighlight() {
            if (document.getElementById("spdAnalytics")) {
                var spread = document.getElementById("spdAnalytics");
                if ($("#blnHighPosVar").is(':checked')) {
                    var highposvar = "checked"
                };
                if ($("#blnHighNegVar").is(':checked')) {
                    var highnegvar = "checked"
                };
                var posvaramt = parseFloat($("#intHighPosVar").val());
                var negvaramt = parseFloat($("#intHighNegVar").val());
                negvaramt = -negvaramt;
                var rc = spread.GetRowCount();
                for (var i = 0; i < rc; i++) {
                    var rowtype = spread.GetValue(i, 3);
                    if (rowtype == "Detail" || rowtype == "SDetail" || rowtype == "STotal") {
                        var value = parseFloat(spread.GetValue(i, 9).replace(',', ''));
                        if (value > posvaramt) {
                            if (highposvar == "checked") {
                                SetBackGroundColor(i, "Green")
                            }
                            else {
                                SetBackGroundColor(i, "Black")
                            }
                        }
                        else if (value < negvaramt) {
                            if (highnegvar == "checked") {
                                SetBackGroundColor(i, "Red")
                            }
                            else {
                                SetBackGroundColor(i, "Black")
                            }
                        }
                        else {

                            SetBackGroundColor(i, "Black")
                        }
                    }
                    else {
                        SetBackGroundColor(i, "Black")
                    }
                }
            }
        }

         function AddBackgroundColor() {
            if (document.getElementById("spdAnalytics")) {
                var spread = document.getElementById("spdAnalytics");
                var rc = spread.GetRowCount();
                var tf = false
                for (var i = 0; i < rc; i++) {
                    if (tf == false) {
                        SetBackGroundColor(i, "#fef5do")
                        spread.SetSelectedRange(i, 3, i, 8)
                        tf = true
                    }
                    else {
                        SetBackGroundColor(i, "White")
                        spread.SetSelectedRange(i, 3, i, 8)
                        tf = false
                    }
                }
            }
        }

        function RemoveBackgroundColor() {
            if (document.getElementById("spdAnalytics")) {
                var spread = document.getElementById("spdAnalytics");
                var rc = spread.GetRowCount();
                var tf = false
                for (var i = 0; i < rc; i++) {
                    SetBackGroundColor(i, "Black")
                }
            }
        }

        function SetBackGroundColor(row, color) {
            if (document.getElementById("spdAnalytics")) {
                var spread = document.getElementById("spdAnalytics");
                var colcount = spread.GetColCount();
                for (var i = 6; i < 10; i++) {
                    spread.Cells(row, i).style.color = color
                }
            }
        }

        $('#blnUseFilter').change(function () {
            
            if ($(this).is(':checked')) {
                $('#UseFilterControls').show()
                $('#HighlightVarControls').hide()
                $('#blnHighVar').removeAttr('checked')
                RemoveBackgroundColor()
                AddFilter()
            }
            else {
                $('#UseFilterControls').hide()
                RemoveFilter()
            }
        });

        $('#strFilterOn').change(function () {
            AddFilter()
        })

        $('#radioPosVar').change(function () {
            $('#radioPosVar').attr('checked', true);
            AddFilter()
        })

        $('#radioNegVar').change(function () {
            $('#radioPosVar').removeAttr('checked')
            AddFilter()
        })

        $('#decFilterAmt').change(function () {
            AddFilter()
        })

        function AddFilter() {
            if (document.getElementById("spdAnalytics")) {
                
                var spread = document.getElementById("spdAnalytics");
                var selindex = $('#intFilterOn').get(0).selectedIndex;
                var filteramt = $('#decFilterAmt').val();
                 var posvar = "unchecked"
                if ($("#radioPosVar").is(':checked')) {
                    posvar = "checked"
                };  
                var rc = spread.GetRowCount();
                var amount = parseFloat(filteramt);
                var showclass = false
                var showsubclass = false
                var coladj = selindex 
                //                spread.SetColWidth(0, 0);
                if (selindex > 2) {
                    coladj = 1 + selindex
                }  
                for (var i = 0; i < rc; i++) {
                    var rowtype = spread.GetValue(i, 3); 
                    if (rowtype == "Detail" || rowtype == "SDetail") {
                        //                      
                      var value = parseInt(spread.GetValue(i, coladj + 7).replace(',', '', 'g')); 
                        if (value < filteramt) {
                            if (posvar == "checked") {
                                spread.Rows(i).style.display = "none";
                            }
                            else {
                                spread.Rows(i).style.display = "table-row";
                            }
                        }
                        else if (value > filteramt) {
                            if (posvar == "checked") {
                                spread.Rows(i).style.display = "table-row";
                            }
                            else {
                                spread.Rows(i).style.display = "none";
                            }
                        }
                    }
                    else if (rowtype == "Heading") {                        
                        spread.Rows(i).style.display = "table-row";
                        cell = document.getElementById("spdAnalytics").GetCellByRowCol(i, 0);
                        cell.removeAttribute("FpCellType");
                        spread.Cells(i, 0).style.color = "white"
                        var cellvalue = spread.GetValue(i, 0); 
                        if (cellvalue == "-") {
                            spread.SetValue(i, 0, "o", false);
                        }
                        else {
                            spread.SetValue(i, 0, "x", false);

                        }
                        cell.setAttribute("FpCellType", "readonly");
                    }
                    else if (rowtype == "Total") {
                        spread.Rows(i).style.display = "none";
                    }
                    else {
                        spread.Rows(i).style.display = "none";
                    }
                } 
            } 
        }

        function RemoveFilter() {
            if (document.getElementById("spdAnalytics")) {
                var spread = document.getElementById("spdAnalytics");
                var rc = spread.GetRowCount(); 
                var cellvalue = ""
                var headingvalue = ""
                var showclass = false
                var showsubtotal = false
                for (var i = 0; i < rc; i++) {
                    var rowtype = spread.GetValue(i, 3);
                    if (rowtype == "Detail" || rowtype == "SDetail" || rowtype == "STotal") {
                        if (showclass == false) {
                            spread.Rows(i).style.display = "none";
                        }
                        else {
                            if (rowtype == "SDetail") {
                                
                                if (showsubtotal == true) {
                                    spread.Rows(i).style.display = "table-row";
                                }
                                else {
                                    spread.Rows(i).style.display = "none";
                                }
                            }
                            else {
                                spread.Rows(i).style.display = "table-row";
                            }
                        }
                    }
                    else if (rowtype == "Heading") {                                                
                        cell = document.getElementById("spdAnalytics").GetCellByRowCol(i, 0);
                        cell.removeAttribute("FpCellType");
                        spread.Cells(i, 0).style.color = "black"
                        var cellvalue = spread.GetValue(i, 0);
                        var headingvalue = spread.GetValue(i, 4);
                                                
                        
                        if (cellvalue == "o") {
                            spread.SetValue(i, 0, "-", false); 
                            showclass = true
                        }
                        else {
                            spread.SetValue(i, 0, "+", false);
                            spread.Rows(i).style.display = "none";
                            showclass = false
                        }
                        cell.setAttribute("FpCellType", "readonly");
                      
                        
                    }
                    else if (rowtype == "SHeading") { 
                        if (showclass == false) {
                            spread.Rows(i).style.display = "none";
                        }
                        else {
                            cellvalue = spread.GetValue(i, 1);
                            if (cellvalue == "-") {
                                showsubtotal = true
                                spread.Rows(i).style.display = "table-row";
                            }
                            else {
                                showsubtotal = false
                                spread.Rows(i).style.display = "none";
                            }
                        }
                    }
                    else if (rowtype == "Total") {
                        if (showclass == false) {
                            cell = document.getElementById("spdAnalytics").GetCellByRowCol(i, 0);
                            cell.removeAttribute("FpCellType");
                            spread.SetValue(i, 0, "+", false);
                            cell.setAttribute("FpCellType", "readonly");
                        }
                        spread.Rows(i).style.display = "table-row";
                    }
                    else {
                        
                       
                        spread.Rows(i).style.display = "table-row";
                    }
                }
            }
        }


//        function AddFilter() {
//            
//            if (document.getElementById("spdAnalytics")) {
//                var spread = document.getElementById("spdAnalytics");
//                var selindex = $('#intFilterOn').get(0).selectedIndex;
//                var filteramt = $('#decFilterAmt').val();
//                var selradio = $('#radioPosVar').attr('checked');
//                var rc = spread.GetRowCount();
//                var amount = parseFloat(filteramt);
//                var showclass = false
//                var showsubclass = false
//                for (var i = 0; i < rc; i++) {
//                    var rowtype = spread.GetValue(i, 3);
//                    if (rowtype == "Detail" || rowtype == "SDetail" || rowtype == "STotal") {
//                        if (showclass == false) {
//                            spread.Rows(i).style.display = "none";
//                        }
//                        else {
//                            var value = parseFloat(spread.GetValue(i, selindex + 7).replace(',', '', 'g'));
//                            if (value < filteramt) {
//                                if (selradio == "checked") {
//                                    spread.Rows(i).style.display = "none";
//                                }
//                            }
//                            else {
//                                if (selradio == "checked") {
//                                    if (rowtype == "SDetail") {
//                                        if (showsubtotal == true) {
//                                            spread.Rows(i).style.display = "table-row";
//                                        }
//                                        else {
//                                            spread.Rows(i).style.display = "none";
//                                        }
//                                    }
//                                    else {
//                                        spread.Rows(i).style.display = "table-row";
//                                    }
//                                }
//                                else {

//                                }
//                            }
//                        }
//                    }
//                    else if (rowtype == "SHeading") {
//                        cellvalue = spread.GetValue(i, 1);
//                        if (cellvalue == "-") {
//                            showsubtotal = true
//                        }
//                        else {
//                            showsubtotal = false
//                        }
//                    }
//                    else if (rowtype == "Heading") {
//                        var cellvalue = spread.GetValue(i, 0);
//                        if (cellvalue == "-") {
//                            showclass = true
//                        }
//                        else {
//                            showclass = false
//                        }
//                    }
//                    else if (rowtype == "Total") {
//                        if (showclass == true) {
//                            spread.Rows(i).style.display = "none";
//                        }
//                    }
//                    else if (rowtype == "GP") {
//                        spread.Rows(i).style.display = "none";
//                    }
//                    else if (rowtype == "IO") {
//                        spread.Rows(i).style.display = "none";
//                    }
//                    else if (rowtype == "NI") {
//                        spread.Rows(i).style.display = "none";
//                    }
//                    else {

//                    }
//                }
//            }
//        }

//        function RemoveFilter() {
//            if (document.getElementById("spdAnalytics")) {
//                var spread = document.getElementById("spdAnalytics");
//                var rc = spread.GetRowCount();
//                var cellvalue = ""
//                var showclass = false
//                var showsubtotal = false
//                for (var i = 0; i < rc; i++) {
//                    var rowtype = spread.GetValue(i, 3);
//                    if (rowtype == "Detail" || rowtype == "SDetail" || rowtype == "STotal") {
//                        if (showclass == false) {
//                            spread.Rows(i).style.display = "none";
//                        }
//                        else {
//                            if (rowtype == "SDetail") {
//                                if (showsubtotal == true) {
//                                    spread.Rows(i).style.display = "table-row";
//                                }
//                                else {
//                                    spread.Rows(i).style.display = "none";
//                                }
//                            }
//                            else {
//                                spread.Rows(i).style.display = "table-row";
//                            }
//                        }
//                    }
//                    else if (rowtype == "Heading") {
//                        cellvalue = spread.GetValue(i, 0);
//                        if (cellvalue == "-") {
//                            showclass = true
//                        }
//                        else {
//                            showclass = false
//                        }
//                    }
//                    else if (rowtype == "SHeading") {
//                        if (showclass == false) {
//                            spread.Rows(i).style.display = "none";
//                        }
//                        else {
//                            cellvalue = spread.GetValue(i, 1);
//                            if (cellvalue == "-") {
//                                showsubtotal = true
//                                spread.Rows(i).style.display = "table-row";
//                            }
//                            else {
//                                showsubtotal = false
//                                spread.Rows(i).style.display = "none";
//                            }
//                        }
//                    }
//                    else {
//                        spread.Rows(i).style.display = "table-row";
//                    }
//                }
//            }
//        }

//        function HideUnFilterRow(row) {
//            var selindex = $('#intFilterOn').get(0).selectedIndex;
//            var filteramt = $('#decFilterAmt').val();
//            var amount = parseFloat(filteramt);
//            var selradio = $('#radioPosVar').attr('checked');
//            var spread = document.getElementById("spdAnalytics");
//            var value = parseFloat(spread.GetValue(row, selindex + 6).replace(',', '', 'g'));
//            if (value < filteramt) {
//                if (selradio == "checked") {
//                    spread.Rows(row).style.display = "none";
//                }
//                else {
//                    spread.Rows(row).style.display = "table-row";
//                }
//            }
//            else {
//                if (selradio == "checked") {
//                    spread.Rows(row).style.display = "table-row";

//                }
//                else {
//                    spread.Rows(row).style.display = "none";
//                }
//            }
//        }
        function getSavedReportsList() {

        if (isSessionAliveCalled == 'false') { IsSessionAlive(); }

        $.ajax({
            type: "GET",
            url: '@Url.Action("GetSavedViewsList", "SV")',
            global: false,
            cache: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (msg) {
                var _selectedCompany
                if (msg !== undefined && (msg != "")) {

                    for (var savedreport_index = 0; savedreport_index < msg.length; savedreport_index++) {
                        var idcontrolVal = msg[savedreport_index].Value;
                        var id_controller = idcontrolVal.split("_");
                        var controller = id_controller[0]
                        var url;
                        if (controller.trim() == "RE")
                            url = '@Url.Action("Viewer", "RE", New With {.Id = "-1"})'
                        else if (controller.trim() == "ALC")
                            url = '@Url.Action("Viewer", "ALC", New With {.Id = "-1"})'
                        else if (controller.trim() == "CF")
                            url = '@Url.Action("Viewer", "CF", New With {.Id = "-1"})'
                        else if (controller.trim() == "RA")
                            url = '@Url.Action("Viewer", "RA", New With {.Id = "-1"})'
                        else if (controller.trim() == "OM")
                            url = '@Url.Action("Viewer", "OM", New With {.Id = "-1"})'

                        url = url.replace("-1", id_controller[1]);
                        $("#SavedReportsList").append("<li><a  class='savedviews' href='" + url + "' data='" + msg[savedreport_index].Value + "' >" + msg[savedreport_index].Text + "</a></li>");
                    }
                }
                else {
                    $("#SavedReportsList").html("No Saved Reports found ! ");
                }
                setSelectedPageNav();
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
            }
        });
    }
</script>
