﻿Imports System.Collections.Generic
Imports System.Linq
Imports System.Data
Imports System.Data.SqlClient

Public Class SaveViewRepository
    Inherits GenericRepository(Of SaveView)
    Implements ISaveViewRepository
    Implements IDisposable

    Private disposed As Boolean = False

    Public Sub New(context As DataAccess)
        'Me.context = context
        MyBase.New(context)
    End Sub

    Public Function GetSavedViewDetail() As System.Collections.Generic.IEnumerable(Of SaveView) Implements ISaveViewRepository.GetSavedViewDetail
        Return context.SaveView.ToList()
    End Function

    Public Function GetSavedViews(analysisID As Integer) As System.Collections.Generic.IEnumerable(Of SaveView) Implements ISaveViewRepository.GetSavedViews
        Return context.SaveView.Where(Function(a) a.analysisID = analysisID).OrderBy(Function(o) o.Order).ToList()
    End Function

    Public Function GetSavedViewDetail(ID As Integer) As SaveView Implements ISaveViewRepository.GetSavedViewDetail
        Return context.SaveView.Where(Function(a) a.ID = ID).FirstOrDefault
    End Function

    Public Function DeleteSavedViewItemsByAnalysis(analysisId As Integer) Implements ISaveViewRepository.DeleteSavedViewItemsByAnalysis
        Logger.Log.Info(("Start Query : Get the list of saved view by AnalysisId: " & analysisId))
        Dim savedView = (From d In context.SaveView.Where(Function(a) a.analysisID = analysisId)
                                       Select d)
        If Not (savedView Is Nothing And savedView.Count > 0) Then
            Logger.Log.Info(("SavedView items count : " & savedView.Count))
            Logger.Log.Info("Start deleting : SavedView items")
            savedView.ToList().ForEach(Sub(item) context.SaveView.Remove(item))
            context.SaveChanges()
            Logger.Log.Info(("End deleting : SavedView items"))
        End If
        Return savedView.Count
    End Function

    Public Sub DeleteSavedView(savedViewID As Integer) Implements ISaveViewRepository.DeleteSavedView
        Logger.Log.Info(("Start Execution of DeleteSavedView"))
        Dim utility As New Utility
        Dim savedview As SaveView = context.SaveView.Find(savedViewID)
        context.SaveView.Remove(savedview)
        Logger.Log.Info("Execution Ended for DeleteSavedView")
    End Sub

    Public Sub InsertSavedView(savedview As SaveView) Implements ISaveViewRepository.InsertSavedView
        context.SaveView.Add(savedview)
    End Sub

    Public Sub UpdateSavedViewName(savedview As SaveView) Implements ISaveViewRepository.UpdateSavedViewName
        context.Database.ExecuteSqlCommand("UPDATE dbo.SaveView SET viewname = @viewname WHERE ID = @id", New SqlParameter("viewname", savedview.viewname), New SqlParameter("ID", savedview.ID))
        context.SaveChanges()
    End Sub

    Public Sub Save() Implements ISaveViewRepository.Save
        context.SaveChanges()
    End Sub
    
    Protected Overridable Sub Dispose(disposing As Boolean)
        If Not Me.disposed Then
            If disposing Then
                context.Dispose()
            End If
        End If
        Me.disposed = True
    End Sub

    Public Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

End Class
