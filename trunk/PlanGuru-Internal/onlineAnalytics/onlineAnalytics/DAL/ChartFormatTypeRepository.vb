﻿Imports System.Collections.Generic
Imports System.Linq
Imports System.Data

Public Class ChartFormatTypeRepository
    Inherits GenericRepository(Of ChartFormatType)
    Implements IchartFormatTypeRepository


    Private disposed As Boolean = False

    Public Sub New(context As DataAccess)
        MyBase.New(context)
    End Sub

    Public Function GetChartFormatTypes() As IEnumerable(Of ChartFormatType) Implements IchartFormatTypeRepository.GetChartFormatTypes
        Return context.ChartFormatTypes.ToList()
    End Function

    Protected Overridable Sub Dispose(disposing As Boolean)
        If Not Me.disposed Then
            If disposing Then
                context.Dispose()
            End If
        End If
        Me.disposed = True
    End Sub

    Public Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub


End Class
