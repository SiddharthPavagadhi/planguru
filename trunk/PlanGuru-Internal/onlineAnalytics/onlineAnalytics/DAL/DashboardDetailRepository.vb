﻿Imports System.Collections.Generic
Imports System.Linq
Imports System.Data

Public Class DashboardDetailRepository
    Inherits GenericRepository(Of DashboardDetail)
    Implements IDashboardDetailRepository

    Private disposed As Boolean = False

    Public Sub New(context As DataAccess)
        MyBase.New(context)
    End Sub

    Public Sub Save() Implements IDashboardDetailRepository.Save
        context.SaveChanges()
    End Sub

    Public Sub DeleteDashboardDetails(DashboardId As Integer) Implements IDashboardDetailRepository.DeleteDashboardDetails

        Dim DashboardDetailObjects = context.DashboardDetails.Where(Function(d) d.DashboardId = DashboardId)
        For Each DashboardDetailObject As DashboardDetail In DashboardDetailObjects
            context.DashboardDetails.Remove(DashboardDetailObject)
        Next
        context.SaveChanges()
    End Sub

    Public Sub DeleteDashboardDetailsWithAnalysisId(AnalysisId As Integer) Implements IDashboardDetailRepository.DeleteDashboardDetailsWithAnalysisId
        Logger.Log.Info(("Start Query : Get the list of Dashboard Items by AnalysisId: " & AnalysisId))
        Dim dashboard = (From d In context.Dashboards.Where(Function(a) a.AnalysisId = AnalysisId)
                                       Select d).ToList()
        If Not (dashboard Is Nothing) And dashboard.Count > 0 Then
            For Each singleItem In dashboard
                Dim dashboardDetailList = (From dashboardDetail In context.DashboardDetails.Where(Function(d) d.DashboardId = singleItem.DashboardId) Select dashboardDetail)
                If Not (dashboardDetailList Is Nothing) And dashboardDetailList.Count > 0 Then
                    dashboardDetailList.ToList().ForEach(Sub(item) context.DashboardDetails.Remove(item))
                    context.SaveChanges()
                End If
                Logger.Log.Info(("End deleting : Dashboard Detail items"))
            Next
        End If
    End Sub

    Public Function GetNumberOfRowCount(DashboardId As Integer) As Integer Implements IDashboardDetailRepository.GetNumberOfRowCount

        Return context.DashboardDetails.Where(Function(d) d.DashboardId = DashboardId).Count()
        
    End Function

    Public Function GetSecondSeriesAccountId(DashboardId As Integer, AccountId As Integer) As DashboardDetail Implements IDashboardDetailRepository.GetSecondSeriesAccountId
        Return context.DashboardDetails.Where(Function(a) a.DashboardId = DashboardId And a.AccountId <> AccountId).FirstOrDefault()

    End Function

    Public Function GetSeriesLabel(DashboardId As Integer, AccountId As Integer) As String Implements IDashboardDetailRepository.GetSeriesLabel
        Return context.DashboardDetails.Where(Function(a) a.DashboardId = DashboardId And a.AccountId = AccountId).Select(Function(u) u.DashboardDescription).FirstOrDefault()
    End Function

    Public Function GetCommaSepratedAccountId(DashboardId As Integer) As String Implements IDashboardDetailRepository.GetCommaSepratedAccountId
        Dim AccountIdList = context.DashboardDetails.Where(Function(a) a.DashboardId = DashboardId).Select(Function(b) b.AccountId).ToList()
        If AccountIdList.Count = 1 Then
            Return AccountIdList(0).ToString()
        Else
            Return String.Join(",", AccountIdList)
        End If
    End Function

    Public Function GetAccountIdList(DashboardId As Integer) As List(Of Integer) Implements IDashboardDetailRepository.GetAccountIdList
        Return context.DashboardDetails.Where(Function(a) a.DashboardId = DashboardId).Select(Function(b) b.AccountId).ToList()
    End Function

    Public Function GetCommaSepratedAccountTypeId(DashboardId As Integer) As String Implements IDashboardDetailRepository.GetCommaSepratedAccountTypeId
        Dim AccountTypeIdList = context.DashboardDetails.Where(Function(a) a.DashboardId = DashboardId).Select(Function(b) b.AcctTypeId).ToList()
        If AccountTypeIdList.Count = 1 Then
            Return AccountTypeIdList(0).ToString()
        Else
            Return String.Join(",", AccountTypeIdList)
        End If
    End Function

    Protected Overridable Sub Dispose(disposing As Boolean)
        If Not Me.disposed Then
            If disposing Then
                context.Dispose()
            End If
        End If
        Me.disposed = True
    End Sub

    Public Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub




End Class
