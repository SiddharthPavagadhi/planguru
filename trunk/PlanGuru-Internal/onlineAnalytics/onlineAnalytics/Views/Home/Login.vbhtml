﻿@ModelType  onlineAnalytics.LoginModel
@Imports  onlineAnalytics
@Code
    ViewData("Title") = "PlanGuru Analytics | Login"
    Layout = "~/Views/Shared/_VanityLayout.vbhtml"
        
    Dim logo As String = String.Empty
    
    If Not (String.IsNullOrEmpty(ViewBag.Logo)) Then
        logo = ViewBag.Logo
    End If
    
End Code


<script src="@Url.Content("~/Scripts/jquery.validate.min.js")" type="text/javascript"></script>
<script src="@Url.Content("~/Scripts/jquery.validate.unobtrusive.min.js")" type="text/javascript"></script>

@Using Html.BeginForm("Login", "User")
    @Html.AntiForgeryToken()   
    @Html.ValidationSummary(True)    
     
@<div >
        @If Not (String.IsNullOrEmpty(logo)) Then
                @<img id="branding_logo" src='@Url.Content("~/Branding_Logos/" + logo)' alt="Planguru Analytics" style="max-width:275px;max-height:78px;"/>     
            Else
                @<img src='@Url.Content("~/Content/Images/planguru_logo.png")' alt="Planguru Analytics" style="max-width:275px;max-height:78px;"/>
            End If
</div>
    @<fieldset>
        <div style="width:100;">           
            <div style="width:100%;">
                <ol class="round">
                    <li>
                        @Html.TextBoxFor(Function(m) m.Username)<br />
                        <a href='#' class="forgotUsername float-right"  title="Forgot Username?" >Forgot Username?</a>
                    </li>
                    <li>
                        @Html.PasswordFor(Function(m) m.Password)<br />
                        <a href='#' class="forgotPassword float-right" title="Forgot Password?">Forgot Password?</a>
                    </li>
                </ol>
            </div>                   
            <div class="input-form LoginBtnMargin">
                <!--input type="submit" value="LOG IN" class="button_example" /-->
                 <input type="button" value="LOG IN" id="Vanitylogin" class="button_example" />
            </div>
            <div style="float:left;margin-left:20px;">
                 @If Not (DirectCast(TempData("Message"), String) Is Nothing) Then
                    @<label class="success">@TempData("Message").ToString()
                    </label>                         
                End If
                @If Not (DirectCast(TempData("ErrorMessage"), String) Is Nothing) Then
                    @<label class="error">@TempData("ErrorMessage").ToString()
                    </label>                         
                End If
            </div>
        </div>
    </fieldset>                                  
End Using