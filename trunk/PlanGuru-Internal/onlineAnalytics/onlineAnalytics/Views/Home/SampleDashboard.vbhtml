﻿@Imports Farpoint.Web.Spread
@Imports Farpoint.Mvc.Spread
@Imports  onlineAnalytics
@Code
    ViewData("Title") = "Sample Dashboard"
    Layout = "~/Views/Shared/_Layout.vbhtml"
End Code
<script type="text/javascript">
    $(function () {
        $("#updatesetup")
            .button()
            .click(function (event) {
                document.location = '@Url.Action("DashboardSetup", "Home")';
            });
    });
       
</script>
<table style="width: 100%;margin:2px auto;" class="ceter-top-select">
    <tr>
        <td>
            <span>Period</span>
            @Html.DropDownList("intPeriod", DirectCast(ViewData("Periods"), SelectList), New With {.Class = "select"})
        </td>
        <td>
            <input type="submit" value="Change Dashboard Setup" class="secondbutton_example"
                style="margin-left: 511px;" id="updatesetup" />
        </td>
    </tr>
</table>
<fieldset>
<div id="spread" style="padding-top: 1px; display: inline-block;">
    @Html.FpSpread("spdDashboard", Sub(x)
                                       x.RowHeader.Visible = False
                                       x.ActiveSheetView.PageSize = 1000
                                   End Sub)
</div>
</fieldset>