﻿@ModelType  onlineAnalytics.ForgotPassword 
@Code
    ViewData("Title") = "PlanGuru Analytics | ForgotPassword"
End Code
<script src="@Url.Content("~/Scripts/jquery.validate.min.js")" type="text/javascript"></script>
<script src="@Url.Content("~/Scripts/jquery.validate.unobtrusive.min.js")" type="text/javascript"></script>
<div class="headerTable" >
    <table width="100%">
        <tr>
            <td style="border: none !important; text-align: left;">
                Forgot Password
            </td>
        </tr>
    </table>
</div>
<div style="margin:0px;padding:0px;">
    @Using Html.BeginForm("ForgotPassword", "Home")
        @Html.AntiForgeryToken()   
        @Html.ValidationSummary(True)    
        @<fieldset>
            <div style="width:100%;">
                <div style="float: left; width: 33%;">
                    <ol class="round">
                        <li>
                            @Html.LabelFor(Function(m) m.userName)<br />
                            @Html.TextBoxFor(Function(m) m.userName)<br />
                            @Html.ValidationMessageFor(Function(m) m.userName)
                        </li>
                    </ol>
                </div>
                 <div style="float: left; width: 33%;">
                    <ol class="round">
                        <li>
                            @Html.LabelFor(Function(m) m.emailAddress)<br />
                            @Html.TextBoxFor(Function(m) m.emailAddress, New With {.Style = "width:250px;"})<br />
                            @Html.ValidationMessageFor(Function(m) m.emailAddress)
                        </li>
                    </ol>
                </div>
                <div class="input-form">
                    <table cellpadding="0" cellspacing="0" style="margin:10px 0px 0px 0px;">
                        <tr>
                            <td style="text-align:left;padding-right:20px;">
                                <input type="submit" value="Reset Password" class="secondbutton_example" />
                            </td>                     
                        </tr>
                    </table>
                </div>
                <div style="float: left; margin-left: 20px;">
                   @If Not (DirectCast(TempData("InfoMessage"), String) Is Nothing) Then
                        @<label class="info">@TempData("InfoMessage").ToString()</label>                         
                    End If
                    @If Not (DirectCast(TempData("Message"), String) Is Nothing) Then
                        @<label class="success">@TempData("Message").ToString()</label>
                    End If
                    @If Not (DirectCast(TempData("ErrorMessage"), String) Is Nothing) Then
                        @<label class="error">@TempData("ErrorMessage").ToString()</label>
                    End If
                </div>
            </div>
        </fieldset>                                  
    End Using
</div>

