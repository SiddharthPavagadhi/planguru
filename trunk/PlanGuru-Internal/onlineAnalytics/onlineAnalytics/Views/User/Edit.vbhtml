﻿@ModelType onlineAnalytics.User
@Imports System.Collections.Generic
@Imports  onlineAnalytics
@Code
    ViewData("Title") = "PlanGuru Analytics | Edit User"
    If Not Session("action") Is Nothing Then
        
        If (Session("action") = "SetDefaultAnalysis" or Session("action") = "GetAnalyses") Then
            Layout = Nothing
            Session.Remove("action")
        End If
    Else
        Layout = "~/Views/Shared/AnalyticsMaster.vbhtml"
    End If
    
    Dim ua As New UserAccess
    If Not (Session("UserAccess") Is Nothing) Then
        ua = DirectCast(Session("UserAccess"), UserAccess)
    End If
        
End Code
<script src="@Url.Content("~/Scripts/jquery.validate.min.js")" type="text/javascript"></script>
<script src="@Url.Content("~/Scripts/jquery.validate.unobtrusive.min.js")" type="text/javascript"></script>
@*<div style="float: left; padding-top: 5px; padding-left: 3px;">
    <label style="font-size: 20px;">
        Edit User</label>
</div>*@
@*<div class="bredcurm-panel">
    <div class="bredcrumb-nav">
        <label>
            User</label></div>
</div>*@
<div class="headerTable" style="float: right">
    <table>
        <tr>
            <td align="left" width="70%">
                Edit User
            </td>
            @Code
                If (ua.ViewUser = True) Then
                    'Below href function Jquery function call written in AnalyticsMaster.vbhtml on $(".add").click     
                @<td align="right" width="12%" class="addblue button_example" href='@Url.Action("Index", "User")'>
                    <img src='@Url.Content("~/Content/Images/List.png")' class="usersIcon" style="margin-right:5px;float:left;"/>User List
                </td>
                End If
            End Code
        </tr>
    </table>
</div>
<br />
<br />
<div class="center-panel">
    @Using Html.BeginForm("Edit", "User")
        @Html.AntiForgeryToken()   
        @Html.ValidationSummary(True)    
       If (Not IsNothing(Model)) Then
            @<fieldset>
            <div style="width: 100;">
                <div style="float: left; width: 33%;">
                    <ol class="round">
                        <li>
                            @Html.LabelFor(Function(m) m.UserName)<br />
                            @Html.TextBoxFor(Function(m) m.UserName)<br />
                            @Html.ValidationMessageFor(Function(m) m.UserName)
                        </li>
                        @* <li>
                        @Html.LabelFor(Function(m) m.Password)<br />
                        @Html.PasswordFor(Function(m) m.Password)<br />
                        @Html.ValidationMessageFor(Function(m) m.Password)
                    </li>*@
                        <li>
                            @Html.LabelFor(Function(m) m.UserEmail)<br />
                            @Html.TextBoxFor(Function(m) m.UserEmail)<br />
                            @Html.ValidationMessageFor(Function(m) m.UserEmail)
                        </li>
                    </ol>
                </div>
                <div style="float: left; width: 33%;">
                    <ol class="round">
                        <li>
                            @Html.LabelFor(Function(m) m.FirstName)<br />
                            @Html.TextBoxFor(Function(m) m.FirstName)<br />
                            @Html.ValidationMessageFor(Function(m) m.FirstName)
                        </li>
                        <li>
                            @Html.LabelFor(Function(m) m.LastName)<br />
                            @Html.TextBoxFor(Function(m) m.LastName)<br />
                            @Html.ValidationMessageFor(Function(m) m.LastName)
                        </li>
                    </ol>
                </div>
                <div style="float: left; width: 33%;">
                    <ol class="round">
                        <li>
                            @Html.LabelFor(Function(m) m.UserRoleId)<img src='@Url.Content("~/Content/Images/help.png")' alt="User Role Help" title="User Role Help" class="user_role_help" /><br />
                            @Html.DropDownList("UserRoleId", DirectCast(ViewBag.SelectedRoles, SelectList), "Select User Role", New With {.Class = "select", .Style = "width:206px;"})<br />
                            @Html.ValidationMessageFor(Function(m) m.UserRoleId)
                        </li>
                        <li>
                            @Html.LabelFor(Function(m) m.CustomerId)<br />
                            @Html.DropDownList("CustomerId", DirectCast(ViewBag.SelectedCustomer, SelectList), "Select Customer", New With {.Class = "select", .Style = "width:206px;"})<br />
                            @Html.ValidationMessageFor(Function(m) m.CustomerId)
                        </li>
                    </ol>
                </div>
                <div class="input-form">
                    <input id="Submit" type="submit" value="Update User details" class="cancelbutton" />
                    <input id="Cancel" type="button" value="Cancel" class="cancelbutton" data='@Url.Action("Index", "User")' />
                </div>                
                @Html.HiddenFor(Function(m) m.UserId)
            </div>
        </fieldset>           
        End If
        @<div style="float: left; margin-left: 20px;">
            @If Not (DirectCast(TempData("Message"), String) Is Nothing) Then
                @<label class="success">@TempData("Message").ToString()
                </label>                         
            End If
            @If Not (DirectCast(TempData("ErrorMessage"), String) Is Nothing) Then
                @<label class="error">@TempData("ErrorMessage").ToString()
                </label>                         
            End If
        </div>
    End Using
</div>
<script type="text/javascript" language="javascript">

    $(document).ready(function () {
        IsSessionAlive();
        var msg;
        //Input Mask for landline phone number
        $("#ContactTelephone").mask("(999) 999-9999");
        //$("#FiscalMonthStart").mask("9?9");


        $("#Cancel").click(function (event) {
            IsSessionAlive();
            //event.preventDefault();
            var url = $(this).attr('data');
            window.location = url;
        });

        $(".user_role_help").click(function (event) {
            event.preventDefault();

            $(".ui-dialog-titlebar").show();
            user_role_help_Dialog.dialog('open');

        });
    });

    $(window).load(function () {
        $(".wrapper").show();
    })
</script>
