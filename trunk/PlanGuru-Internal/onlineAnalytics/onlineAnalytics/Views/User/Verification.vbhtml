﻿@ModelType onlineAnalytics.UserVerification
@Imports System.Collections.Generic
@Imports  onlineAnalytics
@Code
    ViewData("Title") = "Verification"
    
    
End Code
<script src="@Url.Content("~/Scripts/jquery.validate.min.js")" type="text/javascript"></script>
<script src="@Url.Content("~/Scripts/jquery.validate.unobtrusive.min.js")" type="text/javascript"></script>
<div class="headerTable" style="float: right">
    <table>
        <tr>
            <td align="left" width="70%">
                Verification
            </td>
            <td align="right" width="12%" style="padding-left: 20px;">
                User
            </td>
        </tr>
    </table>
</div>
<div class="center-panel">
    @Using Html.BeginForm("Verification", "User")
                           
        @Html.AntiForgeryToken()   
        @Html.ValidationSummary(True)    
        If (Not IsNothing(Model)) Then                    
            @<fieldset>
            <div style="width: 100;">                               
                @If Not (Model Is Nothing) Then
                @Html.HiddenFor(Function(m) m.User_Id)
                @Html.HiddenFor(Function(m) m.UserRoleId)
                If (Model.UserRoleId = UserRoles.SAU) Then
                    @<div style="float: left; width: 33%;">
                        <ol class="round">
                            <li>
                                @Html.LabelFor(Function(m) m.CurrentPassword)<br />
                                @Html.PasswordFor(Function(m) m.CurrentPassword, New With {.tabindex = 1})<br />
                                @Html.ValidationMessageFor(Function(m) m.CurrentPassword)
                            </li>
                            <li>
                                @Html.LabelFor(Function(m) m.NewPassword)<br />
                                @Html.PasswordFor(Function(m) m.NewPassword, New With {.tabindex = 3})<br />
                                @Html.ValidationMessageFor(Function(m) m.NewPassword)
                            </li>
                            
                        </ol>
                    </div>
                Else
                    @Html.HiddenFor(Function(m) m.UserName)
                End If
                @<div style="float: left; width: 33%;">
                    <ol class="round">
                        @If (Model.UserRoleId > UserRoles.SAU) Then
                            @<li>
                                @Html.LabelFor(Function(m) m.CurrentPassword)<br />
                                @Html.PasswordFor(Function(m) m.CurrentPassword)<br />
                                @Html.ValidationMessageFor(Function(m) m.CurrentPassword)
                            </li>
                        End If
                       
                        <li>
                            @Html.LabelFor(Function(m) m.UserName)<br />
                            @Html.TextBoxFor(Function(m) m.UserName, New With {.tabindex = 2})<br />
                            @Html.ValidationMessageFor(Function(m) m.UserName)
                        </li>
                        <li>
                            @Html.LabelFor(Function(m) m.ConfirmNewPassword)<br />
                            @Html.PasswordFor(Function(m) m.ConfirmNewPassword, New With {.tabindex = 4})<br />
                            @Html.ValidationMessageFor(Function(m) m.ConfirmNewPassword)
                        </li>
                    </ol>
                </div>
                @<div class="input-form">
                    <input type="submit" value="Reset" class="secondbutton_example" />
                </div>
                End If                              
            </div>
        </fieldset>     
        End if
        @<div style="float: left; margin-left: 20px;">
            @If Not (DirectCast(TempData("Message"), String) Is Nothing) Then
                @<label class="success">@TempData("Message").ToString()
                </label>                         
            End If
            @If Not (DirectCast(TempData("ErrorMessage"), String) Is Nothing) Then
                @<label class="error">@TempData("ErrorMessage").ToString()
                </label>                         
            End If
         </div>
    End Using
</div>
<script type="text/javascript" language="javascript">

    var status = '@ViewBag.Status';
    var role = '@ViewBag.UserRoleId';
    

    if ($("#UserName") && status == 2 && role == 3) {
        $("#UserName").attr("readonly", true);
    }   

</script>
