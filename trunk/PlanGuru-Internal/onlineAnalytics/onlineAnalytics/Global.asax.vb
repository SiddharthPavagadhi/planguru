﻿' Note: For instructions on enabling IIS6 or IIS7 classic mode, 
' visit http://go.microsoft.com/?LinkId=9394802
Imports System.Data.Entity
Imports onlineAnalytics

Public Class MvcApplication
    Inherits System.Web.HttpApplication

    Shared Sub RegisterGlobalFilters(ByVal filters As GlobalFilterCollection)
        filters.Add(New HandleErrorAttribute())
        'filters.Add(New CustomHandleErrorAttribute())
        'filters.Add(New HandleErrorWithPersistentViewDataAttribute())
    End Sub

    Shared Sub RegisterRoutes(ByVal routes As RouteCollection)
        routes.IgnoreRoute("{resource}.axd/{*pathInfo}")

        ' MapRoute takes the following parameters, in order:
        ' (1) Route name
        ' (2) URL with parameters
        ' (3) Parameter defaults
        routes.MapRoute( _
            "Default", _
            "{controller}/{action}/{id}", _
            New With {.controller = "Home", .action = "Index", .id = UrlParameter.Optional})

        routes.MapRoute( _
            "Analysis", _
            "{controller}/{action}/{page}", _
            New With {.controller = "Analysis", .action = "Index", .id = UrlParameter.Optional})

        routes.MapRoute( _
            "SelectSubscription", _
            "{controller}/{action}/{selectedPlan}", _
            New With {.controller = "SelectSubscription", .action = "Subscribe", .id = UrlParameter.Optional})

    End Sub

    Sub Application_Start()
        'Database.SetInitializer(New SampleData())
        Logger.InitLogger()
        If (System.Configuration.ConfigurationManager.AppSettings("DropDB") <> "" And System.Configuration.ConfigurationManager.AppSettings("DropDB").Equals("True".ToLower())) Then
            Database.SetInitializer(Of DataAccess)(New SampleData())
        End If
        AreaRegistration.RegisterAllAreas()
        RegisterGlobalFilters(GlobalFilters.Filters)
        RegisterRoutes(RouteTable.Routes)
    End Sub

    Sub Application_BeginRequest()
        Select Case Request.Url.Scheme
            Case "https"
                Response.AddHeader("Strict-Transport-Security", "max-age=31536000")
                Exit Select
            Case "http"
                If (System.Configuration.ConfigurationManager.AppSettings("HostedOnSSL").ToLower() = "true") Then
                    Dim path = "https://" + Request.Url.Host + Request.Url.PathAndQuery
                    Response.Status = "301 Moved Permanently"
                    Response.AddHeader("Location", path)
                    Exit Select
                End If
        End Select

    End Sub

    Sub Application_EndRequest()


    End Sub

    'Protected Sub Application_Error(sender As Object, e As EventArgs)
    '    Dim httpContext = DirectCast(sender, MvcApplication).Context

    '    Dim currentRouteData = RouteTable.Routes.GetRouteData(New HttpContextWrapper(httpContext))
    '    Dim currentController = " "
    '    Dim currentAction = " "

    '    If currentRouteData IsNot Nothing Then
    '        If currentRouteData.Values("controller") IsNot Nothing AndAlso Not [String].IsNullOrEmpty(currentRouteData.Values("controller").ToString()) Then
    '            currentController = currentRouteData.Values("controller").ToString()
    '        End If

    '        If currentRouteData.Values("action") IsNot Nothing AndAlso Not [String].IsNullOrEmpty(currentRouteData.Values("action").ToString()) Then
    '            currentAction = currentRouteData.Values("action").ToString()
    '        End If
    '    End If

    '    Dim ex = Server.GetLastError()

    '    Dim controller = New onlineAnalytics.CustomErrorController()
    '    Dim routeData = New RouteData()
    '    Dim action = "Index"

    '    If TypeOf ex Is HttpException Then
    '        Dim httpEx = TryCast(ex, HttpException)

    '        Select Case httpEx.GetHttpCode()
    '            Case 404
    '                action = "NotFound"
    '                Exit Select
    '            Case Else
    '                ' others if any
    '                action = "Index"
    '                Exit Select
    '        End Select
    '    End If

    '    httpContext.ClearError()
    '    httpContext.Response.Clear()
    '    httpContext.Response.StatusCode = If(TypeOf ex Is HttpException, DirectCast(ex, HttpException).GetHttpCode(), 500)
    '    httpContext.Response.TrySkipIisCustomErrors = True
    '    routeData.Values("controller") = "CustomError"
    '    routeData.Values("action") = action

    '    controller.ViewData.Model = New HandleErrorInfo(ex, currentController, currentAction)
    '    DirectCast(controller, IController).Execute(New RequestContext(New HttpContextWrapper(httpContext), routeData))
    'End Sub

End Class
