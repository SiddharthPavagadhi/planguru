﻿Imports System.Collections.Generic
Imports System.Linq
Imports System.Data


Public Class UserRolePermissionMappingRepository
    Inherits GenericRepository(Of UserRolePermissionMapping)
    Implements IUserRolePermissionMappingRepository
    Implements IDisposable
    Private disposed As Boolean = False

    Public Sub New(context As DataAccess)
        'Me.context = context
        MyBase.New(context)
    End Sub

    Public Function GetUserRolePermissionMapping() As IEnumerable(Of UserRolePermissionMapping) Implements IUserRolePermissionMappingRepository.GetUserRolePermissionMapping
        Return context.UserRolePermissionMappings.ToList()
    End Function

    Protected Overridable Sub Dispose(disposing As Boolean)
        If Not Me.disposed Then
            If disposing Then
                context.Dispose()
            End If
        End If
        Me.disposed = True
    End Sub

    Public Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
End Class
