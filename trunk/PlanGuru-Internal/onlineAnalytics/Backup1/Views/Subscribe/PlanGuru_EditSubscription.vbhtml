﻿@ModelType onlineAnalytics.Customer
@Imports onlineAnalytics
@Imports System.Collections.Generic
@Code
    ViewData("Title") = "PlanGuru EditSubscription"
    If Not Session("action") Is Nothing Then
        
        If (Session("action") = "SetDefaultAnalysis" Or Session("action") = "GetAnalyses") Then
            Layout = Nothing
            Session.Remove("action")
        End If
        
    Else
        Layout = "~/Views/Shared/AnalyticsMaster.vbhtml"
    End If
    
    Dim ua As New UserAccess
    If Not (Session("UserAccess") Is Nothing) Then
        ua = DirectCast(Session("UserAccess"), UserAccess)
    End If
End Code
<script src="@Url.Content("~/Scripts/jquery.validate.min.js")" type="text/javascript"></script>
<script src="@Url.Content("~/Scripts/jquery.validate.unobtrusive.min.js")" type="text/javascript"></script>
<div class="headerTable" style="float: right">
    <table>
        <tr>
            <td align="left" width="70%">
               Edit Subscriber
            </td>
            @Code
                If (ua.AddSubscription = True) Then
                @<td align="right" width="12%" class="add button_example" href='@Url.Action("Subscribe", "Subscribe")'>
                    <img src='@Url.Content("~/Content/Images/plus.gif")' class="plusIcon" /> Subscription
                </td>
                End If
            End Code
        </tr>
    </table>
</div>
<br />
<br />
<div class="center-panel">
@Using Html.BeginForm("PlanGuru_EditSubscription", "Subscribe")
    @Html.AntiForgeryToken()   
    @Html.ValidationSummary(True) 
    @Html.HiddenFor(Function(m) m.CustomerId)
    @<fieldset>
        <div style="width: 100%;">
            <div style="float: left; width:18%;">
                <ol class="round line-height"  >
                    <li>
                        @Html.Label("First name")
                    </li>
                    <li>
                        @Html.Label("Last name")
                    </li>
                    <li>
                         @Html.Label("Email address")
                    </li>
                     <li>
                         @Html.Label("Subscriber name")
                    </li>
                    <li>
                         @Html.Label("Subscriber plan")
                    </li>
                </ol>
            </div>
            <div style="float: left; width:22%;">
                 <ol class="round line-height">
                    <li>
                        @Html.TextBoxFor(Function(m) m.CustomerFirstName)<br />
                    </li>
                    <li>
                        @Html.TextBoxFor(Function(m) m.CustomerLastName)<br />
                    </li>
                    <li>
                        @Html.TextBoxFor(Function(m) m.CustomerEmail)<br />
                    </li>
                     <li>
                        @Html.TextBoxFor(Function(m) m.CustomerCompanyName)<br />
                    </li>
                    <li>
                        @Html.DropDownList("PlanTypeId", DirectCast(ViewBag.PlanType, SelectList), New With {.Class = "select", .Style = "width:206px;"})<br />
                    </li>
                </ol>
            </div>
             <div style="float: left; width:60%;">
                 <ol class="round line-height">
                    <li>
                        @Html.ValidationMessageFor(Function(m) m.CustomerFirstName)
                    </li>
                    <li>
                        @Html.ValidationMessageFor(Function(m) m.CustomerLastName)
                    </li>
                    <li>
                        @Html.ValidationMessageFor(Function(m) m.CustomerEmail)
                    </li>
                     <li>
                        @Html.ValidationMessageFor(Function(m) m.CustomerCompanyName)
                    </li>
                    <li>
                        @Html.ValidationMessageFor(Function(m) m.PlanTypeId)
                    </li>
                </ol>                
            </div>
             <div class="input-form">
                      <input type="submit" value="Update Subscription in PGA" class="cancelbutton" style="margin-right:20px;" />
                      <input id="CancelSubscription" type="button" value="Cancel Subscription" class="cancelbutton CancelSubscription" style="margin-right:20px;" data='@Url.Action("CancelSubscription", "Subscribe", New With {.AccountCode = Model.CustomerId})'/>
                      <input id="ResetSAUPassword" type="button" value="Reset SAU Password" class="cancelbutton" style="margin-right:20px;" onclick="javascript:resetPasswordConfirmation('@Model.CustomerFirstName','@Url.Action("ResetPassword", "User", New With {.userId = Model.UserId, .pathToRedirect = "subscribe"})');"/>
                      <input id="GoBack" type="button" value="Go Back" class="cancelbutton" onclick="javascript:window.history.back();"/>
                </div>
        </div>
    </fieldset>
End Using
</div>

<script language="javascript" type="text/javascript">
/*
  $("#CancelSubscription").click(function (event) {
        if (isSessionAliveCalled == 'false') { IsSessionAlive(); }
        event.preventDefault();
        var url = $(this).attr('data');

        $.msgBox({
            title: "Confirm",
            content: "Cancellation of this subscription will close your account, and you will not able to login again. Are you sure you want to cancel this subscription?",
            type: "confirm",
            buttons: [{ value: "Yes" }, { value: "No"}],
            success: function (result) {
                if (result == "Yes") {
                    window.location = url;
                }
            },
            afterShow: function () { $('[name=No]').focus(); }
        });

    });
    */
</script>
