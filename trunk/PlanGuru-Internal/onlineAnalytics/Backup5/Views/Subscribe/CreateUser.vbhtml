﻿@ModelType onlineAnalytics.Customer
@Imports onlineAnalytics
@Imports System.Collections.Generic
@Code
    ViewData("Title") = "Subscribe"
    If Not Session("action") Is Nothing Then
        
        If (Session("action") = "SetDefaultAnalysis" or Session("action") = "GetAnalyses") Then
            Layout = Nothing
            Session.Remove("action")
        End If
    Else
        Layout = "~/Views/Shared/AnalyticsMaster.vbhtml"
    End If
End Code
<script src="@Url.Content("~/Scripts/jquery.validate.min.js")" type="text/javascript"></script>
<script src="@Url.Content("~/Scripts/jquery.validate.unobtrusive.min.js")" type="text/javascript"></script>
<h2>
    Customer Administrative User</h2>
<header>    
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"> </script>
    </header>
<script>
    $(document).ready(function () {

        //alert($('#signup_customer_state_input').outerHTML());
        $("select[name='Country']").change(function () {
            if ($(this).val() == 250) {
                $('#State').show();
                $('#signup_customer_state_input').hide();
            }
            else {
                $('#State').hide();
                $('#signup_customer_state_input').show();
            }
        });

        $("select[name='Country']").select(function () {
            if ($(this).val() == 250) {
                $('#State').show();
                $('#signup_customer_state_input').hide();
            }
            else {
                $('#State').hide();
                $('#signup_customer_state_input').show();
            }
        });

        /*
        $("select[name='signup[customer][country]']").change(function () {
        if ($(this).val() == 250) {
        $('#signup_customer_state_dropdown').show();
        $('#signup_customer_state_input').hide();
        }
        else {
        $('#signup_customer_state_dropdown').hide();
        $('#signup_customer_state_input').show();
        }
        });

        $("select[name='signup[customer][country]']").select(function () {
        if ($(this).val() == 250) {
        $('#signup_customer_state_dropdown').show();
        $('#signup_customer_state_input').hide();
        }
        else {
        $('#signup_customer_state_dropdown').hide();
        $('#signup_customer_state_input').show();
        }
        });
        */
    });
</script>
@Using Html.BeginForm("CreateUser", "Subscribe")
    @Html.AntiForgeryToken()   
    @Html.ValidationSummary(True)       
    @<div class="well">
        <h5>
            Contact Info</h5>
        <div >
            <div style="width: 33%;" class="control-group">
                <ol class="round">
                    <li>
                        @Html.LabelFor(Function(m) m.ContactFirstName)<br />
                        @Html.TextBoxFor(Function(m) m.ContactFirstName)<br />
                        @Html.ValidationMessageFor(Function(m) m.ContactFirstName)
                    </li>
                    <li>
                        @Html.LabelFor(Function(m) m.ContactLastName)<br />
                        @Html.TextBoxFor(Function(m) m.ContactLastName)<br />
                        @Html.ValidationMessageFor(Function(m) m.ContactLastName)
                    </li>
                    <li>
                        @Html.LabelFor(Function(m) m.CustomerEmail)<br />
                        @Html.TextBoxFor(Function(m) m.CustomerEmail)<br />
                        @Html.ValidationMessageFor(Function(m) m.CustomerEmail)
                    </li>
                    <li>
                        @Html.LabelFor(Function(m) m.ContactTelephone)<br />
                        @Html.TextBoxFor(Function(m) m.ContactTelephone)<br />
                        @Html.ValidationMessageFor(Function(m) m.ContactTelephone)
                    </li>
                </ol>
            </div>
            <div style="width: 33%;" class="control-group">
                <ol class="round">
                    <li>
                        @Html.LabelFor(Function(m) m.CustomerAddress1)<br />
                        @Html.TextBoxFor(Function(m) m.CustomerAddress1)<br />
                        @Html.ValidationMessageFor(Function(m) m.CustomerAddress1)
                    </li>
                    <li>
                        @Html.LabelFor(Function(m) m.CustomerAddress2)<br />
                        @Html.TextBoxFor(Function(m) m.CustomerAddress2)<br />
                        @Html.ValidationMessageFor(Function(m) m.CustomerAddress2)
                    </li>
                    <li>
                        @Html.LabelFor(Function(m) m.CustomerPostalCode)<br />
                        @Html.TextBoxFor(Function(m) m.CustomerPostalCode)<br />
                        @Html.ValidationMessageFor(Function(m) m.CustomerPostalCode)
                    </li>
                </ol>
            </div>
            <div style="width: 33%;" class="control-group">
                <ol class="round">
                    <li>@*<label class="control-label" for="signup_customer_country">Country</label><br />*@
                        @Html.LabelFor(Function(m) m.Country)<br />
                        @Html.DropDownList("Country", DirectCast(ViewBag.SelectedCountry, SelectList), "Select Country", New With {.Class = "select", .Style = "width:205px;"})<br />
                        @Html.ValidationMessageFor(Function(m) m.Country)
                    </li>
                    <li>@* <label class="control-label" for="signup_customer_state">State</label><br />*@
                        @Html.LabelFor(Function(m) m.State)<br />
                        @Html.DropDownList("State", DirectCast(ViewBag.SelectedState, SelectList), "Select State", New With {.Class = "select", .Style = "width:205px;"})
                        @Html.TextBoxFor(Function(m) m.State, New With {.id = "signup_customer_state_input", .Style = "display:none"})<br />
                        @*<input type="text" name="signup[customer][state]" id="signup_customer_state_input" hidden="hidden" />*@
                        @Html.ValidationMessageFor(Function(m) m.State)
                    </li>
                    <li>
                        @Html.LabelFor(Function(m) m.City)<br />
                        @Html.TextBoxFor(Function(m) m.City)<br />
                        @Html.ValidationMessageFor(Function(m) m.City)
                    </li>
                </ol>
            </div>
        </div>
    </div>
    @<div class="well">
        <h5>
            Billing Info</h5>
        <div>
            <div style="width: 25%;" class="control-group">
                <ol class="round">
                    <li>
                        @Html.LabelFor(Function(m) m.FirstNameOnCard)<br />
                        @Html.TextBoxFor(Function(m) m.FirstNameOnCard)<br />
                        @Html.ValidationMessageFor(Function(m) m.FirstNameOnCard)
                    </li>
                    <li>
                        @Html.LabelFor(Function(m) m.LastNameOnCard)<br />
                        @Html.TextBoxFor(Function(m) m.LastNameOnCard)<br />
                        @Html.ValidationMessageFor(Function(m) m.LastNameOnCard)
                    </li>
                    <li>
                        <label id="expiration_year" name="signup[payment_profile][expiration_year]">
                            Card Accepted</label><br />
                        <div class="cell_double" style="margin-top: 5px;">
                            <img class="cc Visa" title="Visa" alt="Visa" src="@Url.Content("~/Content/Images/payment/Visa_30x19.gif")" style="display: inline;">
                            <img class="cc_gs Visa_gs" title="Visa" alt="Visa" src="@Url.Content("~/Content/Images/payment/Visa_gs_30x19.gif")" style="display: none;">
                            <img class="cc MasterCard" title="MasterCard" alt="MasterCard" src="@Url.Content("~/Content/Images/payment/MC_30x19.gif")" style="display: inline;">
                            <img class="cc_gs MasterCard_gs" title="MasterCard" alt="MasterCard" src="@Url.Content("~/Content/Images/payment/MC_gs_30x19.gif")" style="display: none;">
                            <img class="cc AmericanExpress" title="American Express" alt="American Express" src="@Url.Content("~/Content/Images/payment/american_express_30x19.gif")" style="display: inline;">
                            <img class="cc_gs AmericanExpress_gs" title="Amex" alt="American Express" src="@Url.Content("~/Content/Images/payment/Amex_gs_30x19.gif")" style="display: none;">
                            <img class="ck OnlineCheck" title="Discover" alt="Discover" src="@Url.Content("~/Content/Images/payment/discover_30x19.gif")" style="display: none;">
                            <img class="ck_gs OnlineCheck_gs" title="Discover" alt="Discover" src="@Url.Content("~/Content/Images/payment/discover_30x19.gif")" style="display: inline;">
                        </div>
                    </li>
                </ol>
            </div>
            <div style="width: 25%;" class="control-group">
                <ol class="round">
                    <li>
                        @Html.LabelFor(Function(m) m.CreditCardNumber)<br />
                        @Html.TextBoxFor(Function(m) m.CreditCardNumber)<br />
                        @Html.ValidationMessageFor(Function(m) m.CreditCardNumber)
                    </li>
                    <li>
                        <label id="expiration_month" name="signup[payment_profile][expiration_month]">
                            Card Expiration</label><br />
                        @Html.DropDownList("Month", DirectCast(ViewBag.SelectedMonth, SelectList), New With {.Class = "select", .Style = "width:50px;float:left", .Name = "signup[payment_profile][expiration_month]"})
                        &nbsp;
                        @Html.DropDownList("Year", DirectCast(ViewBag.SelectedYear, SelectList), New With {.Class = "select", .Style = "width:80px;float:left;margin-left:20px;", .Name = "signup[payment_profile][expiration_year]"})<br />
                        <br />
                    </li>
                    <li>
                        <label id="Card Type" name="signup[payment_profile][expiration_month]">
                            Card Type</label><br />
                        @Html.DropDownList("Card Type", DirectCast(ViewBag.SelectedCardType, SelectList), "Select Card Type", New With {.Class = "select", .Style = "width:205px;"})
                        <br />
                    </li>
                </ol>
            </div>            
        </div>
    </div>
        
    @<div class="input-form" style="margin: 20px 10px 50px 20px;">
        <input type="submit" value="Subscribe" class="submit-button" />
    </div>
    @<div style="float: left; margin-left: 20px;">
        @If Not (DirectCast(TempData("Message"), String) Is Nothing) Then
            @<label class="success">@TempData("Message").ToString()
            </label>                         
    End If
        @If Not (DirectCast(TempData("ErrorMessage"), String) Is Nothing) Then
            @<label class="error">@TempData("ErrorMessage").ToString()
            </label>                         
    End If
    </div>
      
     
End Using
