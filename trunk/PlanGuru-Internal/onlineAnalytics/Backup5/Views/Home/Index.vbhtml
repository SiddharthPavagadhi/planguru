﻿@Code
    ViewData("Title") = "PlanGuru Analytics | Home Page"
    Layout = "~/Views/Shared/_Layout.vbhtml"
End Code
@*<div>
    PlanGuru Analytics is a tool that enables PlanGuru users to create a dashboard of your company's key performance indicators and analyze both financial and non-financial data in a flexible and easy to use environment.
 </div>*@ @* <body>*@
<div style="width:960px; font-family: Arial, Helvetica, sans-serif; color:#666; font-size:14px;">
	
<div style="height:20px;"></div>
    


<div style="height:350px;">
        
	<div style="width:480px; float:left;">        
		<div style="margin:20px;">
			<a href="//fast.wistia.net/embed/iframe/ubpkkwgo33?popover=true" class="wistia-popover[height=506,playerColor=7b796a,width=900]">
			    <img src='@Url.Content("~/Content/Images/analytics-screen.jpg")' width="440"  border="1px" style="border-style:solid; border-color:#dbdbdc;"></>
			</a>
            <script charset="ISO-8859-1" src="//fast.wistia.com/assets/external/popover-v1.js"></script>
			<div style="background-color:#ececec; width:412px; font-size:12px; padding:10px; padding-left:20px;">
			<span style="color:#266ea6; font-weight:bold;">We're dedicated to the continual improvement of PlanGuru Analytics</span>
			<br/>
			<span style="font-size:13px;">
			<a href="https://planguru.zendesk.com/entries/29977607-PGA-Enhancement-Ideas" target="_blank">See what's coming soon to Analytics</a> 
			| <a href="http://www.surveygizmo.com/s3/1460491/PlanGuru-Analytics-Enhancement-Priorities" target="_blank">Give us your feedback</a></span>
		</div>        
	</div>        
</div>      




<div style="width:480px; float:right;">
        <div style="margin:20px;">
        <h1 style="color:#266ea6; margin-bottom:0px; font-size:20px; font-family: Arial, Helvetica, sans-serif;">Quickly Analyze Your Financial Performance</h1><br/>PlanGuru Analytics is a tool that enables PlanGuru users to create an online dashboard of your company's key performance indicators and analyze both financial and non-financial data in a flexible and easy to use environment.
    <br/><br/>
    		Simply upload your data from PlanGuru*, set up your own dashboards and add any additional users you would like to view those reports. With built -in user management, you can control who views which reports. <br /><br /><span style="font-size:12px;"><em>*To upload data, you must own <a href="http://www.planguru.com/products/planguru/" target="_blank">PlanGuru 2013, 2014 or Version 5</a>.</em></span><br/><br/>
            <div style="float:left;"><a href='@Url.Action("ChooseSubscription", "SelectSubscription")'><img src='@Url.Content("~/Content/Images/btn-sign-up.jpg")'  alt="Sign Up - Try it Free for 30 Days"  /></a>
            </div>
            <div style="float:right; text-align: center;"><span style="color:#266ea6; margin-right:80px; font-size:26px; line-height:1.5em; ">$19.95 / mo.</span><br/><span style="color:#266ea6; margin-right:80px; line-height:.5em; font-size:16px;">per user</span>
            </div>
        </div>
        </div>
    </div>
    <div style="height:10px; width:100%;">&nbsp; </div>
    <div>
        <div style="width:480px; float:right; font-family: Arial, Helvetica, sans-serif;">
        	<div style="margin:20px;">
        		<div style="height:1px; background-color:#dbdbdc;">
                </div>
                <div style="background-color:#266ea6;  display: inline-block; height:40px;">
                	<span style="color:#FFF; margin:20px; font-size:18px; font-family: Arial, Helvetica, sans-serif; line-height:2em;">For New Subscription Administrators</span>
                </div>
        				<h1 style="color:#266ea6; margin-bottom:0px; font-size:18px; font-family: Arial, Helvetica, sans-serif;">Overview of Subscription Management</h1>In PlanGuru Analytics all users and shared data are managed within a subscription by the Subscription Administrative User. <a href="https://planguru.zendesk.com/entries/27797057-PlanGuru-Analytics-Subscription-Management-Overview" target="_blank">Learn more about managing your subscription</a><br/><br/>
    					<h1 style="color:#266ea6; margin-bottom:0px; font-size:18px; font-family: Arial, Helvetica, sans-serif;">Add employees and clients to your subscription</h1> Adding employees or clients to your subscription is quick and easy.  By adding a new user you will be creating a new login assigned to your account. <a href="https://planguru.zendesk.com/entries/27870813-Adding-users-to-your-PlanGuru-Analytics-Subscription" target="_blank">Learn more about adding new users</a><br/><br/>
              </div>
        </div>
        
        <div style="width:480px; float:left;">
             <div style="margin:20px;">
             	<div style="height:1px; background-color:#dbdbdc;">
                </div>
                <div style="background-color:#6aae69; display:inline-block; height:40px;">
                	<span style="color:#FFF; margin:20px; font-size:18px; font-family: Arial, Helvetica, sans-serif; line-height:2em;">For New Users</span>
                </div>
             		<h1 style="color:#6aae69; margin-bottom:0px; font-size:18px;">Before You Sign Up</h1> If you intend to use PlanGuru Analytics collaboratively, you'll need to understand the basics of how a subscription works. <a href="https://planguru.zendesk.com/entries/28321306-PlanGuru-Analytics-Introduction-for-new-users" target="_blank">Learn more about becoming a New User.</a>
        			<br/><br/>
       				 <h1 style="color:#6aae69; margin-bottom:0px; font-size:18px;">Setting Up Your Account</h1>If you intend to be the Subscription Administrator, simply click the Sign Up button above. If need to be added to an existing subscription, contact your colleague or advisor and request that they add you to their PlanGuru Analytics subscription.  Additional user setup is quick and easy and requires only your name and e-mail address.  
            </div>

        </div>
        
	</div>
            <div style="background-color:#ececec; float:left; width:960px; font-size:20px; padding:10px; padding-left:20px;"><span style="color:#266ea6; font-weight:bold;">Have Questions?</span> Call us at (888) 822-6300 or visit our <a href="https://planguru.zendesk.com/categories/20127686-PlanGuru-Analytics" target="_blank">Knowledge Base</a>
    		</div>
</div>
@*</body>*@
<div>
    @If Not (DirectCast(TempData("Message"), String) Is Nothing) Then
        @<label class="success">@TempData("Message").ToString()
        </label>                         
    End If
    @If Not (DirectCast(TempData("ErrorMessage"), String) Is Nothing) Then
        @<label class="error">@TempData("ErrorMessage").ToString()
        </label>                         
    End If
</div>
@*<div  style="display: none;">
            @Html.TextBox("intBrowserWidth")
</div>*@ 