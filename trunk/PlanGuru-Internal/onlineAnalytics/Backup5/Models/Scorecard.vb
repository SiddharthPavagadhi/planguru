﻿Imports System.ComponentModel.DataAnnotations

Public Class Scorecard

    Public Property ScorecardId As Integer
    Public Property Order As Integer

    <RegularExpression("^[1-9]+[0-9]*$", ErrorMessage:="Please select the item to be listed in the scorecard.")>
   <Required(ErrorMessage:="Please select the item to be listed in the scorecard.")>
    Public Property AccountId As Integer
    Public Property AnalysisId As Integer
    Public Property ScorecardDesc As String

    <Required(ErrorMessage:="Select goal missed threshold")> _
    <RegularExpression("(([0-9]\d?)|100)$", ErrorMessage:="It will accept between (0 to 100) numberic values.")> _
    <Display(Name:="Goal Missed threshold %")>
    Public Property Missed As Integer

    <Required(ErrorMessage:="Select exceeded threshold")> _
    <RegularExpression("(([0-9]\d?)|100)$", ErrorMessage:="It will accept between (0 to 100) numberic values.")> _
    <Display(Name:="Goal Exceeded threshold %")>
    Public Property Outperform As Integer

    <Display(Name:="Show as % of ")>
    Public Property ShowasPercent As Boolean

    <Display(Name:="If actual exceeds budget the variance is ")>
    Public Property VarIsFavorable As Boolean

    Public Property Option1 As Integer
    Public Property Option2 As Integer
    Public Property Option3 As Integer
    Public Property IsValid As Boolean
    Public Property CreatedBy() As String
    Public Property CreatedOn() As DateTime = DateTime.Now()
    Public Property UpdatedBy() As String
    Public Property UpdatedOn() As DateTime = DateTime.Now()

    <NotMapped()>
    Public Property selectedCheckboxId As String
    <NotMapped()>
    Public Property selectedCheckboxDescription As String
    <NotMapped()>
    Public Property selectedTabId As String

    <NotMapped()>
    Public Property AcctTypeId As Integer
    <NotMapped()>
     Public Property UserId As Integer

End Class

Public Enum PositiveVariance
    Favorable = 1
    Unfavorable = 0
End Enum