﻿Imports System.ComponentModel.DataAnnotations

Public Class Account

    'Primary key (AccountId,AccountId,AnalysisId)
    <Key(), Column(Order:=0)>
    <DatabaseGenerated(DatabaseGeneratedOption.None)>
    Public Property AccountId As Integer

    <Key(), Column(Order:=1)>
    Public Property AcctTypeId As Integer

    <Key(), Column(Order:=2)>
    Public Property AnalysisId As Integer

    <Required()>
    <StringLength(50)>
    Public Property AcctDescriptor As String
    <Required()>
    Public Property SortSequence As Integer
    <Required()>
    <StringLength(50)>
    Public Property Description As String
    <StringLength(50)>
    Public Property Subgrouping As String

    Public Property NumberFormat As Integer

    Public Property TotalType As Integer

    Public Property Option1 As Integer

    <NotMapped()>
    Public Property TypeDesc As String

    <NotMapped()>
    Public Property ClassDesc As String

    'Public Overridable Property AcctType() As AcctType
    'Public Overridable Property Analyses() As Analysis
    'Public Overridable Property Balances() As ICollection(Of Balance)

End Class

Public Class AccountWithAccountType

    Public Property AccountId As Integer

    Public Property AcctTypeId As Integer

    Public Property AnalysisId As Integer

    <StringLength(50)>
    Public Property AcctDescriptor As String

    Public Property SortSequence As Integer

    <StringLength(50)>
    Public Property Description As String

    <StringLength(50)>
    Public Property Subgrouping As String


    Public Property TypeDesc As String

    Public Property ClassDesc As String

End Class

Public Class SelectedAccount

    Public Property AccountId As Integer
    Public Property AcctTypeId As Integer
    Public Property AnalysisId As Integer
    Public Property AcctDescriptor As String
    Public Property SortSequence As Integer
    Public Property Description As String
    Public Property Subgrouping As String
    Public Property TypeDesc As String
    Public Property ClassDesc As String

End Class