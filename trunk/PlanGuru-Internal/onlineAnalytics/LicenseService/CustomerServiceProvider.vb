﻿Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Threading.Tasks
Imports LicenseService.CustomerService
Imports System.Configuration
'Imports Recurly
Imports RecurlyNew
Imports System.Globalization
Imports System.Xml



Public Class CustomerServiceProvider


    Public Function CreateCustomer(accountInfo As RecurlyNew.Account, Password As String, billinginfo As RecurlyNew.BillingInfo) As Integer


        Try

            Dim customerLicenseId As Integer = 0
            Dim CountryName As String = GetCountryCode(billinginfo.Country)
            Dim client As CustomerServerSoapClient = New CustomerServerSoapClient()
            customerLicenseId = client.CreateCustomer(Convert.ToInt32(ConfigurationManager.AppSettings("AuthorId")), ConfigurationManager.AppSettings("UserId"), ConfigurationManager.AppSettings("Password"), accountInfo.CompanyName, accountInfo.FirstName, accountInfo.LastName, billinginfo.Address1, billinginfo.Address2, billinginfo.City, billinginfo.State, billinginfo.PostalCode, CountryName, accountInfo.Email, Password, billinginfo.PhoneNumber, "", "", True, True)
            Return customerLicenseId

        Catch ex As Exception

            'Logger.Logger.Log.[Error]("Problem with creating Customer, Error is" + ex.Message)
            Return -3
        End Try

    End Function


    Private Function GetCountryCode(twoLetterCountryCode As String) As String

        Dim region = (From c In CultureInfo.GetCultures(CultureTypes.SpecificCultures)
                      Where (New RegionInfo(c.LCID)).EnglishName <> "" OrElse (New RegionInfo(c.LCID)).EnglishName IsNot Nothing
                      Order By (New RegionInfo(c.LCID)).EnglishName Ascending
                      Select New With { _
                     Key .Code = (New RegionInfo(c.LCID)).TwoLetterISORegionName, _
                     Key .Name = (New RegionInfo(c.LCID)).EnglishName _
                    }).Distinct()

        Dim code = region.Where(Function(t) t.Code.ToLower() = twoLetterCountryCode.ToLower()).FirstOrDefault()

        Return If((code Is Nothing), twoLetterCountryCode, code.Name.ToString())

    End Function


End Class


