﻿Imports System
Imports System.Collections.Generic
Imports System.Configuration
Imports System.Linq
Imports System.Text
Imports System.Threading.Tasks
Imports LicenseService.UserLicenseService
Imports System.Xml


Public Class XMLlisenceProvider

    Dim client As UserLicenseService.XmlLicenseServiceSoapClient = New UserLicenseService.XmlLicenseServiceSoapClient()

    Public Function AddLicenseUser(LicenseCustomerId As Integer, ProductOptionId As Integer, SeatQuantity As Integer, UnitPrice As Decimal) As XmlNode

        Dim sLicenseAdd As String = "<LicenseAdd xmlns=''>" +
                      "<AuthorID>" + Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings("AuthorId")) + "</AuthorID>" +
                      "<UserID>" + System.Configuration.ConfigurationManager.AppSettings("UserId") + "</UserID>" +
                      "<UserPassword>" + System.Configuration.ConfigurationManager.AppSettings("Password") + "</UserPassword>" +
                      "<ProdOptionID>" + ProductOptionId + "</ProdOptionID>" +
                      "<Quantity>" + SeatQuantity + "</Quantity>" +
                      "<UnitPrice>" + UnitPrice + "</UnitPrice>" +
                      "<Expiration>12/31/2099</Expiration>" +
                      "<ActivationCount>" + SeatQuantity + "</ActivationCount>" +
                      "<DeactivationCount>-1</DeactivationCount>" +
                      "<LicenseCounter></LicenseCounter>" +
                      "<Version></Version>" +
                      "<LicenseUpdate></LicenseUpdate>" +
                      "<Notes></Notes>" +
                      "<CustomerID>" + LicenseCustomerId + "</CustomerID>" +
                      "<DistributorID></DistributorID>" +
                      "<LicenseeEmail></LicenseeEmail>" +
                      "<LicenseeName></LicenseeName>" +
                      "</LicenseAdd>"


        Dim newNode As XmlNode = client.AddS(sLicenseAdd)
        Return newNode

    End Function

    Public Function UpdateUserDefinedFields(LicenseId As Integer, UserDefinedChar1 As String, UserDefinedChar2 As String) As XmlNode

        Dim sUpdateUserDefinedFields As String = "<UpdateUserDefinedFields xmlns=''>" +
                     "<AuthorID>" + Convert.ToInt32(ConfigurationManager.AppSettings("AuthorId")) + "</AuthorID>" +
                     "<UserID>" + ConfigurationManager.AppSettings("UserId") + "</UserID>" +
                     "<UserPassword>" + ConfigurationManager.AppSettings("Password") + "</UserPassword>" +
                     "<LicenseID>" + LicenseId + "</LicenseID>" +
                     "<UDefChar1>" + UserDefinedChar1 + "</UDefChar1>" +
                     "<UDefChar2>" + UserDefinedChar2 + "</UDefChar2>" +
                     "</UpdateUserDefinedFields>"

        Dim ResponseFromUpdateFunction As XmlNode = client.UpdateUserDefinedFieldsS(sUpdateUserDefinedFields)
        Return ResponseFromUpdateFunction

    End Function

    Public Function UpdateActivationFields(LicenseId As Integer, ActivationIncrementCount As Integer) As XmlNode

        Dim sUpdateActivationFields As String = "<UpdateActivationFields xmlns=''>" +
                                         "<AuthorID>" + Convert.ToInt32(ConfigurationManager.AppSettings("AuthorId")) + "</AuthorID>" +
                                         "<UserID>" + ConfigurationManager.AppSettings("UserId") + "</UserID>" +
                                         "<UserPassword>" + ConfigurationManager.AppSettings("Password") + "</UserPassword>" +
                                         "<LicenseID>" + LicenseId + "</LicenseID>" +
                                         "<ActivationsIncrement>" + ActivationIncrementCount + "</ActivationsIncrement> " +
                                         "</UpdateActivationFields>"

        Dim ResponseFromUpdateActivationField As XmlNode = client.UpdateActivationFields(sUpdateActivationFields)

        Return ResponseFromUpdateActivationField

    End Function
End Class



