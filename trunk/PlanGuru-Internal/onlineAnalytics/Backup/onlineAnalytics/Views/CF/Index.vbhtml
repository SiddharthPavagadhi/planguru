﻿@Imports Farpoint.Web.Spread
@Imports Farpoint.Mvc.Spread
@Imports  onlineAnalytics
@Code
    ViewData("Title") = "PlanGuru Analytics | Cash Flow"
    If Not Session("action") Is Nothing Then
        
        If (Session("action") = "SetDefaultAnalysis" Or Session("action") = "GetAnalyses") Then
            Layout = Nothing
            Session.Remove("action")
        End If
    Else
        Layout = "~/Views/Shared/AnalyticsMaster.vbhtml"
    End If
    
    Dim UserInfo As User = Nothing
    Dim ua As New UserAccess
    
    Dim objSetup As New SetupCount
    
    If Not (Session("UserInfo") Is Nothing) Then
        UserInfo = DirectCast(Session("UserInfo"), User)
    End If
    
    If (Not ViewBag.Setup Is Nothing) Then
        objSetup = DirectCast(ViewBag.Setup, SetupCount)
    End If
End Code
<script type="text/javascript">
    window.ViewerControlId = "#REView";   
</script>
<div style="display: inline-block; width: 100%;">
    @If Not (DirectCast(TempData("Message"), String) Is Nothing) Then
        @<label class="success">@TempData("Message").ToString()
        </label>                         
    End If
    @If Not (DirectCast(TempData("ErrorMessage"), String) Is Nothing) Then
        @<label class="error">@TempData("ErrorMessage").ToString()
        </label>                         
    End If
</div>
<div>
    @code   
        @Html.Hidden("countOfDataUploaded", objSetup.countDataUploadedOfSelectedAnalysis)        
        @<label id="validationMessage" class="info" style="display: none;">
            @If ((Not IsNothing(ViewBag.Setup)) AndAlso objSetup.AanlysisCount > 0 And objSetup.selectedAnalysis = 0) Then
                @MvcHtmlString.Create("Please select company & one of the respective analysis from top-menu selection, to get the dashboard view.")
            ElseIf ((Not IsNothing(ViewBag.Setup)) AndAlso objSetup.selectedAnalysis > 0 And objSetup.countDataUploadedOfSelectedAnalysis = 0) Then
                @MvcHtmlString.Create("Data has not been uploaded for selected analysis.")
            End If
        </label> 
        
        If (Not UserInfo Is Nothing AndAlso Not IsNothing(ViewBag.Setup)) Then
                   
            If (UserInfo.UserRoleId <= UserRoles.SAU) Then
                    
                If (objSetup.CompanyCount = 0 Or objSetup.AanlysisCount = 0) Then
        @<label class="info">You have to create below listed action item, to get the dashboard
            view.</label> 
                End If
                If (objSetup.CompanyCount = 0) Then
        @<div class="secondbutton_example" style='margin: 20px; width: 200px;'>
            <a href='@Url.Action("Create", "Company")' style="text-decoration:none;color: #FFFFFF;">
                Add Company</a>
        </div>  
                End If
                If (objSetup.CompanyCount > 0 And objSetup.AanlysisCount = 0) Then
        @<div class="secondbutton_example" style='margin: 20px; width: 200px;'>
            <a href='@Url.Action("Create", "Analysis")'  style="text-decoration:none;color: #FFFFFF;">
                Add Analysis</a>
        </div>  
                End If
                If (objSetup.CompanyCount = 0 Or objSetup.AanlysisCount = 0) Then
        @<div class="secondbutton_example" style='margin: 20px; width: 200px;'>
            <a href='@Url.Action("CreateUser", "User")'  style="text-decoration:none;color: #FFFFFF;">
                Add User</a>
        </div>  
                End If
            ElseIf (UserInfo.UserRoleId > UserRoles.SAU) Then
                
                If (objSetup.CompanyCount = 0) Then
        @<label class="info">You have not been given access to any company. Please contact your administrator. / analysis.</label>
                ElseIf (objSetup.AanlysisCount = 0) Then
        @<label class="info">You have not been given access to any analysis files for the selected company. Please chkShowBudgetAmmountscontact your administrator.</label> 
                End If
                
            End If
        End If
        
        If (objSetup.CompanyCount > 0 And objSetup.AanlysisCount > 0 And objSetup.selectedAnalysis > 0 And objSetup.countDataUploadedOfSelectedAnalysis > 0) Then
            Using Html.BeginForm
        @<div id="REView" style="display: none; width: 100%;">
            <div id="sidebar">
                <div>
                    <img src='@Url.Content("~/Content/images/period.png")' alt="Period" />
                </div>
                <div>
                    @Html.DropDownList("intPeriod", DirectCast(ViewData("Periods"), SelectList), New With {.class = "REselect"})
                </div>
                <br />
                <div>
                    <img src='@Url.Content("~/Content/images/format.png")' alt="Format" />
                </div>
                <div>
                    @Html.DropDownList("intFormat", DirectCast(ViewData("Formats"), SelectList), New With {.class = "REselect"})
                </div>
              
               @* @Code        
                    
                            If (ViewData("intFormat").ToString() = "6") Then
                    @<div id="chkShowBudgetAmmounts" class="indent0" style="margin-top: 5px; margin-left: 10px;">
                        @Html.CheckBox("blnShowBudget", False) @Html.Label(ViewData("strCheckboxCaption").ToString())
                    </div>
                            End If
                End code*@
               @* <div id="chkShowBudgetAmounts" class="indent0" style="margin-top: 5px; margin-left: 10px; display: none;">                            
                                @Html.CheckBox("blnShowBudget", DirectCast(ViewData("blnShowBudget"), Boolean))  @Html.Label(ViewData("strCheckboxCaption").ToString())                                    
                        </div>    *@
                <div id="hperioddd" class="indent10" style="margin-top: 5px; display: none;" >
                    @Html.DropDownList("intHPeriod", DirectCast(ViewData("HPeriods"), SelectList), New With {.class = "REselect", .Style = "width: 80px; font-size: 10px; "})
                </div>
                <div id="chkShowVar" style="display: none;" class="indent0">
                    @Html.CheckBox("blnShowVar") Show variance
                </div>
                <br />
                <div id="optionsimage">
                    <img src='@Url.Content("~/Content/images/options.png")' alt="Options" />
                </div>
                <div id="chkHighlightVar" class="indent0" style="margin-top: 5px;">
                    @Html.CheckBox("blnHighVar", ViewData("blnHighVar")) Highlight variances
                </div>
                <div id="HighlightVarControls" style="display: none;">
                    <div id="chkHighlightPosVar" class="indent10">
                        @Html.CheckBox("blnHighPosVar", True) + Variances > than:
                        @Html.TextBox("intHighPosVar", 10, New With {.Style = "width: 12px;  margin-left: 5px; font-size: 10px; border: 1px solid #DDD;text-align: right; "})%
                        <span id="valintHighPosVar" style="color: red; text-align: right; float: right; width: 165px;
                            margin-right: 10px; display: none;">Numeric characters only.</span>
                    </div>
                    <div id="chkHighlightNegVar" class="indent10">
                        @Html.CheckBox("blnHighNegVar", True) - Variances > than: -
                        @Html.TextBox("intHighNegVar", 0, New With {.Style = "width: 12px; font-size: 10px; border: 1px solid #DDD; text-align: right;"})%
                        <span id="valintHighNegVar" style="color: red; text-align: right; float: right; width: 165px;
                            margin-right: 10px; display: none;">Numeric characters only.</span>
                    </div>
                </div>
                <div id="CheckBoxFilter" class="indent0">
                    @Html.CheckBox("blnUseFilter") Apply Filter
                </div>
                <div id="UseFilterControls" style="display: none;">
                    <div class="indent10">
                        @Html.DropDownList("intFilterOn", DirectCast(ViewData("FilterOn"), SelectList), New With {.class = "REselect"})
                    </div>
                    <div>
                        <label class="indent10">
                            @Html.RadioButton("blnFilterPosVariance", True, New With {.id = "radioPosVar", .checked = "checked", .style = "width: 20px;"})
                            Amounts > than:
                        </label>
                    </div>
                    <div>
                        <label class="indent10">
                            @Html.RadioButton("blnFilterPosVariance", False, New With {.id = "radioNegVar", .style = "width: 20px;"})
                            Amounts < than:
                        </label>
                    </div>
                    <div class="indent30">
                       @Html.TextBox("decFilterAmt", 1000, New With {.Style = "width: 50px; margin-left: 5px; font-size: 10px; border: 1px solid #DDD;text-align: right; "})
                        <span id="valDecFilterAmt" style="color: red; text-align: right; float: right; width: 165px;
                            margin-right: 10px; display: none;">Numeric characters only.</span>
                    </div>
                </div>
                <br />
                <div>
                    <img src='@Url.Content("~/Content/images/charts.png")' alt="Charts" />
                </div>
                <div>
                    @Html.DropDownList("intChartType", DirectCast(ViewData("ChartType"), SelectList), New With {.class = "REselect"})
                </div>
                <div id="ShowTrend" class="indent10" style="margin-top: 5px;">
                    @Html.CheckBox("blnShowTrend")Show trend
                </div>
                <div id="UpdateChart" style="display: none; margin: 15px 0 0 0; float: left;">
                    <input id="causepost" type="submit" value="Update Selections" class="cancelbutton" />
                </div>
                <div class="link">
                    @Code
                                If Not (UserInfo Is Nothing) Then
                                If (UserInfo.UserRoleId <> UserRoles.CRU And Utility.GetViewCount(UserInfo.AnalysisId) >= Convert.ToInt32(ConfigurationManager.AppSettings("ViewCount"))) Then
                    End Code
                    <a style="cursor: default;" onclick="return false;" class="addsavedviewsdisabled"> <img class="plusIcon" src='@Url.Content("~/Content/Images/plus.gif")'> Add to Saved Reports </a>
                    @Code   
                                ElseIf (UserInfo.UserRoleId <> UserRoles.CRU) Then
                    End Code                    
                               <a href="#" id="lnkAddSavedReports" class="addsavedviews"> <img class="plusIcon" src='@Url.Content("~/Content/Images/plus.gif")'> Add to Saved Reports </a>                    
                    @Code                                               
                                    End If
                                End If
                    End Code
                </div>
            </div>
            <div id="main" style="display: inline-block; width: 79%;">
                <div id="chart" style="display: none;">
                    @Html.FpSpread("spdChart", Sub(x)
                                                   x.RowHeader.Visible = False
                                                   x.ColumnHeader.Visible = False
                                                   x.Height = 200
                                                   x.Width = 500
                                           
                                               End Sub)
                </div>
                <div id="spread" style="display: none;">
                    @Html.FpSpread("spdAnalytics", Sub(x)
                                                   
                                                       x.RowHeader.Visible = False
                                                       x.ActiveSheetView.PageSize = 1000
                                                   End Sub)
                </div>
            </div>
        </div>   
            End Using
        End If
       
    End Code
</div>

<script type="text/javascript">
    $("#lnkAddSavedReports").click(function () {
        if (document.getElementById("spdAnalytics")) {
            var accountIDs = new Array();
            var spread = document.getElementById("spdAnalytics");
            var rc = spread.GetRowCount();
            var count = 0;
            for (var i = 0; i < rc; i++) {
                var chkboxval = spread.GetValue(i, 1);
                if (chkboxval == "true") {
                    if (count < 10)                     
                        accountIDs[count] = spread.GetValue(i, 4);
                    count++;
                }
            }
            if (count > 10) {

                $.msgBox({
                    title: "Save View",
                    content: "Only top 10 selected accounts will be saved !",
                    type: "info",
                    buttons: [{ value: "Ok"}],
                    success: function (result) {
                        showView(true, accountIDs);
                    },
                    afterShow: function () { $('[name=Ok]').focus(); }
                });
            }
            else {
                showView(true, accountIDs);
            }

        }

    });


    function showView(validation, accountIDs) {

        $.msgBox({
            title: "Save View",
            content: "<span class='viewsavereport'>View Name </span><span ><input type='text' class='viewsavetext' name='reportname'  maxlength='50'/><label id='viewvalidation' class='viewvalidation'></label></span>",
            type: "info",
            buttons: [{ value: "Ok" }, { value: "Cancel"}],
            success: function (result) {

                if (result == "Ok") {                    
                        var reportname = $('[name=reportname]').val();
                        document.forms[0].action = '@Url.Action("AddViews", "CF")' + "?viewname=" + encodeURIComponent(reportname) + "&accountid=" + accountIDs;
                        document.forms[0].submit();                    
                }
            },
            afterShow: function () { if (validation == false) { $('#viewvalidation').text('Please provide the view name.'); } $('[name=Cancel]').focus(); }
        });

    }

    $(function () {
        $('#blnHighVar').change(function () {
            if ($(this).is(':checked')) {
                $('#HighlightVarControls').show()
                if ($("#blnUseFilter").is(':checked')) {
                    $('#UseFilterControls').hide()
                    $('#blnUseFilter').removeAttr('checked');
                    RemoveFilter()
                };    
                AddHighlight()
            }
            else {
                $('#HighlightVarControls').hide()
                RemoveBackgroundColor()

            }
        });

        $('#blnHighPosVar').change(function () {
            AddHighlight()
        })

        $('#blnHighNegVar').change(function () {
            AddHighlight()
        })

        $('#intHighPosVar').change(function () {

            AddHighlight()
        })

        $('#intHighNegVar').change(function () {
            AddHighlight()
        })

        function AddHighlight() {
//            alert('addhigh')
            if (document.getElementById("spdAnalytics")) {
                var spread = document.getElementById("spdAnalytics");
                if ($("#blnHighPosVar").is(':checked')) {
                    var highposvar = "checked"
                };
                if ($("#blnHighNegVar").is(':checked')) {
                    var highnegvar = "checked"
                };
                var posvaramt = parseFloat($("#intHighPosVar").val());
                var negvaramt = parseFloat($("#intHighNegVar").val());
                negvaramt = -negvaramt;
                var rc = spread.GetRowCount();
                for (var i = 0; i < rc; i++) {
                    var rowtype = spread.GetValue(i, 2);
                    if (rowtype == "Detail") {
                        var value = parseFloat(spread.GetValue(i, 8).replace(',', ''));
                        if (value > posvaramt) {
                            if (highposvar == "checked") {
                                SetBackGroundColor(i, "Green", "current")
                            }
                            else {
                                SetBackGroundColor(i, "Black", "current")
                            }
                        }
                        else if (value < negvaramt) {
                            if (highnegvar == "checked") {
                                SetBackGroundColor(i, "Red", "current")
                            }
                            else {
                                SetBackGroundColor(i, "Black", "current")
                            }
                        }
                        else {

                            SetBackGroundColor(i, "Black", "current")
                        }
                        var ytdvalue = parseFloat(spread.GetValue(i, 12).replace(',', ''));
                        if (ytdvalue > posvaramt) {
                            if (highposvar == "checked") {
                                SetBackGroundColor(i, "Green", "ytd")
                            }
                            else {
                                SetBackGroundColor(i, "Black", "ytd")
                            }
                        }
                        else if (ytdvalue < negvaramt) {
                            if (highnegvar == "checked") {
                                SetBackGroundColor(i, "Red", "ytd")
                            }
                            else {
                                SetBackGroundColor(i, "Black", "ytd")
                            }
                        }
                        else {
                            SetBackGroundColor(i, "Black", "ytd")
                        }
                    }
//                    else {
//                        SetBackGroundColor(i, "Black")
//                    }
                }
            }
        }


        $('#blnUseFilter').change(function () {
            if ($(this).is(':checked')) {
                $('#UseFilterControls').show()
                $('#HighlightVarControls').hide()
                $('#blnHighVar').removeAttr('checked')
                RemoveBackgroundColor()
                AddFilter()
            }
            else {
                $('#UseFilterControls').hide()
                RemoveFilter()
            }
        });

        $('#intFilterOn').change(function () {
            AddFilter()
        })

        $('#radioPosVar').change(function () {
            AddFilter()
        })

        $('#radioNegVar').change(function () {
            AddFilter()
        })

        $('#decFilterAmt').change(function () {
            AddFilter()
        })

        function AddFilter() {

            if (document.getElementById("spdAnalytics")) {
                var spread = document.getElementById("spdAnalytics");
                var selindex = $('#intFilterOn').get(0).selectedIndex;
                var filteramt = $('#decFilterAmt').val();
                var posvar = "unchecked"
                if ($("#radioPosVar").is(':checked')) {
                    posvar = "checked"
                }; 
                var rc = spread.GetRowCount();
                var amount = parseFloat(filteramt);
                var showclass = false
                var coladj = selindex
                if (selindex > 2) {
                    coladj = 1 + selindex
                }
                
                for (var i = 0; i < rc; i++) {
                    var rowtype = spread.GetValue(i, 2); 
                    if (rowtype == "Detail" || rowtype == "OpCash") { 
                            var value = parseFloat(spread.GetValue(i, coladj + 6).replace(',', '')); 
                            if (value < filteramt) {
                                if (posvar == "checked") {
                                    spread.Rows(i).style.display = "none";
                                }
                                else {
                                    spread.Rows(i).style.display = "table-row";
                                }
                            }
                            else if (value > filteramt) {
                                if (posvar == "checked") {
                                    spread.Rows(i).style.display = "table-row";
                                }
                                else {
                                    spread.Rows(i).style.display = "none";
                                }
                            } 
                    }
                    else if (rowtype == "Heading") {
                        spread.Rows(i).style.display = "table-row";
                        cell = document.getElementById("spdAnalytics").GetCellByRowCol(i, 0);
                        cell.removeAttribute("FpCellType");
                        spread.Cells(i, 0).style.color = "white"
                        var cellvalue = spread.GetValue(i, 0);
                        if (cellvalue == "-") {
                            spread.SetValue(i, 0, "o", false);
                        }
                        else {
                            spread.SetValue(i, 0, "x", false);

                        }
                        cell.setAttribute("FpCellType", "readonly");
                    }
                    else {
                        spread.Rows(i).style.display = "none";
                    }
                }
            }
        }

        function RemoveFilter() {

            if (document.getElementById("spdAnalytics")) {
                var spread = document.getElementById("spdAnalytics");
                var rc = spread.GetRowCount();
                var showclass = false
                for (var i = 0; i < rc; i++) {
                    var rowtype = spread.GetValue(i, 2);
                    if (rowtype == "Detail" || rowtype == "OpCash") {
                        if (showclass == false) {
                            spread.Rows(i).style.display = "none";
                        }
                        else {
                            spread.Rows(i).style.display = "table-row";
                        }
                    }
                    else if (rowtype == "Heading") {
                        cell = document.getElementById("spdAnalytics").GetCellByRowCol(i, 0);
                        cell.removeAttribute("FpCellType");
                        spread.Cells(i, 0).style.color = "black"
                        var cellvalue = spread.GetValue(i, 0);
                        var headingvalue = spread.GetValue(i, 4);


                        if (cellvalue == "o") {
                            spread.SetValue(i, 0, "-", false);
                            showclass = true
                        }
                        else {
                            spread.SetValue(i, 0, "+", false);
                            spread.Rows(i).style.display = "none";
                            showclass = false
                        }
                        cell.setAttribute("FpCellType", "readonly");
                    }
                    else {
                        spread.Rows(i).style.display = "table-row";
                    }
                }
            }
        }


      

//        function AddFilter() {

//            if (document.getElementById("spdAnalytics")) {

//                var spread = document.getElementById("spdAnalytics");
//                var selindex = $('#intFilterOn').get(0).selectedIndex;
//                var filteramt = $('#decFilterAmt').val();
//                var selradio = $('#radioPosVar').attr('checked');
//                var rc = spread.GetRowCount();
//                var amount = parseFloat(filteramt);
//                var showclass = false
//                if (selindex > 2) {
//                    coladj = 1 + selindex
//                }
//                for (var i = 0; i < rc; i++) {
//                    var rowtype = spread.GetValue(i, 2);
//                    if (rowtype == "Detail") {
//                        if (showclass == false) {
//                            spread.Rows(i).style.display = "none";
//                        }
//                        else {
//                            var value = parseFloat(spread.GetValue(i, coladj + 6).replace(',', ''));
//                            if (value < filteramt) {
//                                if (selradio == "checked") {
//                                    spread.Rows(i).style.display = "none";
//                                }
//                                else {

//                                }
//                            }
//                            else {
//                                if (selradio == "checked") {
//                                    spread.Rows(i).style.display = "table-row";
//                                }
//                                else {
//                                    spread.Rows(i).style.display = "none";
//                                }
//                            }
//                        }
//                    }
//                    else if (rowtype == "Heading") {
//                        var cellvalue = spread.GetValue(i, 0);
//                        if (cellvalue == "-") {
//                            showclass = true
//                        }
//                        else {
//                            showclass = false
//                        }
//                    }
//                    else if (rowtype == "Total") {
//                        if (showclass == true) {
//                            spread.Rows(i).style.display = "none";
//                        }
//                    }
//                    else if (rowtype == "OpRev") {
//                        spread.Rows(i).style.display = "none";
//                    }
//                    else if (rowtype == "ARChg") {
//                        spread.Rows(i).style.display = "none";
//                    }
//                    else if (rowtype == "OpCash") {
//                        spread.Rows(i).style.display = "none";
//                    }
//                    else if (rowtype == "NetIncrease") {
//                        spread.Rows(i).style.display = "none";
//                    }
//                    else if (rowtype == "BegCash") {
//                        spread.Rows(i).style.display = "none";
//                    }
//                    else if (rowtype == "EndCash") {
//                        spread.Rows(i).style.display = "none";
//                    }
//                    else if (rowtype == "OpExp") {
//                        spread.Rows(i).style.display = "none";
//                    }
//                    else if (rowtype == "APChg") {
//                        spread.Rows(i).style.display = "none";
//                    }
//                    else {

//                    }
//                }
//            }
//        }

//        function RemoveFilter() {

//            if (document.getElementById("spdAnalytics")) {
//                var spread = document.getElementById("spdAnalytics");
//                var rc = spread.GetRowCount();
//                var showclass = false
//                for (var i = 0; i < rc; i++) {
//                    var rowtype = spread.GetValue(i, 2);
//                    if (rowtype == "Detail") {
//                        if (showclass == false) {
//                            spread.Rows(i).style.display = "none";
//                        }
//                        else {
//                            spread.Rows(i).style.display = "table-row";
//                        }
//                    }
//                    else if (rowtype == "Heading") {
//                        var cellvalue = spread.GetValue(i, 0);
//                        if (cellvalue == "-") {
//                            showclass = true
//                        }
//                        else {
//                            showclass = false
//                        }
//                    }
//                    else {
//                        spread.Rows(i).style.display = "table-row";
//                    }
//                }
//            }
//        }

        function AddBackgroundColor() {
            if (document.getElementById("spdAnalytics")) {
                var spread = document.getElementById("spdAnalytics");
                var rc = spread.GetRowCount();
                var tf = false
                for (var i = 0; i < rc; i++) {
                    if (tf == false) {
                        SetBackGroundColor(i, "#fef5do", "current")
                        spread.SetSelectedRange(i, 2, i, 7)
                        tf = true
                    }
                    else {
                        SetBackGroundColor(i, "White", "current")
                        spread.SetSelectedRange(i, 2, i, 7)
                        tf = false
                    }
                }
            }
        }

        function RemoveBackgroundColor() {
            if (document.getElementById("spdAnalytics")) {
                var spread = document.getElementById("spdAnalytics");
                var rc = spread.GetRowCount();
                var tf = false
                for (var i = 0; i < rc; i++) {
                    SetBackGroundColor(i, "Black", "current")
                    SetBackGroundColor(i, "Black", "ytd")
                }
            }
        }
        function SetBackGroundColor(row, color, period) {
            
            if (document.getElementById("spdAnalytics")) {
                if (period == "current") {
                    var startcol = 5
                    var endcol = 9
                }
                if (period == "ytd") {
                    var startcol = 9
                    var endcol = 13
                }
                var spread = document.getElementById("spdAnalytics");
                var colcount = spread.GetColCount();
                for (var i = startcol; i < endcol; i++) {
                    spread.Cells(row, i).style.color = color
                }
            }
        }

        //        //charting functions
        $('#intPeriod').change(function () {
            CheckforSelectedRow()
            //            $('#causepost').trigger('click')
            $('#UpdateChart').show();
        })

        $('#intFormat').change(function () {
            CheckforSelectedRow()
            var numberOfOptions = $('select#intChartType option').length 
            $('#UpdateChart').show();
            var selectedValue = $(this).val(); 
            if (selectedValue < 5) {
                if (intSaveFormat == selectedValue - 2) {
                    $('#optionsimage').show()
                    $('#chkHighlightVar').show()
                    if ($('#blnHighVar').is(':checked')) {
                        $('#HighlightVarControls').show()
                    }
                    $('#CheckBoxFilter').show();
                    if ($('#blnUseFilter').is(':checked')) {
                        $('#UseFilterControls').show()
                    }
                }
                else {
                    $('#optionsimage').hide()
                    $('#chkHighlightVar').hide()
                    $('#HighlightVarControls').hide()
                    $('#CheckBoxFilter').hide()
                    $('#UseFilterControls').hide()
                } 
                $("#hperioddd").hide();
                if (numberOfOptions > 2) {
                    $("#intChartType").empty();
                    var opt = document.createElement("option");
                    document.getElementById("intChartType").options.add(opt);
                    opt.text = "None";
                    opt.value = 1;
                    var opt1 = document.createElement("option");
                    document.getElementById("intChartType").options.add(opt1);
                    opt1.text = "Bar";
                    opt1.value = 2;
                }
            }
            else {
//                if (selectedValue == 6) {
//                    $("#chkShowBudgetAmounts").show();
//                }
//                else {
//                    $("#chkShowBudgetAmounts").hide();
//                }
                if (selectedValue == 13) {
                    $("#hperioddd").show();
                }
                else {
                    $("#hperioddd").hide();
                }
                $('#optionsimage').hide()
                $('#chkHighlightVar').hide()
                $('#HighlightVarControls').hide()
                $('#CheckBoxFilter').hide()
                if (numberOfOptions == 2) {
                    var opt = document.createElement("option");
                    document.getElementById("intChartType").options.add(opt);
                    opt.text = "Stacked bar";
                    opt.value = 3;
                    var opt1 = document.createElement("option");
                    document.getElementById("intChartType").options.add(opt1);
                    opt1.text = "Line";
                    opt1.value = 4;
                    var opt2 = document.createElement("option");
                    document.getElementById("intChartType").options.add(opt2);
                    opt2.text = "Area";
                    opt2.value = 5;
                }
            }
        })


        $('#blnShowBudget').change(function () {
            CheckforSelectedRow()
            //            $('#causepost').trigger('click')
            $('#UpdateChart').show();
        })

        $('#intHPeriod').change(function () {
            $("#UpdateChart").show();
        })

        function CheckforSelectedRow() {
            var spread = document.getElementById("spdAnalytics");
            var count = 0
            var rc = spread.GetRowCount();
            var count = 0;
            for (var i = 0; i < rc; i++) {
                var chkboxval = spread.GetValue(i, 2);
                if (chkboxval == "true") {
                    count++;
                    break
                }
            }
            if (count == 0) {
                $('#intChartType')[0].selectedIndex = 0;
            }
        }

        $('#intChartType').change(function () {
            if ($('#intChartType').get(0)) {
                var selectedindex = $('#intChartType').get(0).selectedIndex;
                var spread = document.getElementById("spdAnalytics");
                var colcount = spread.GetColCount();
                if (selectedindex == 0) {
                    $('#ShowTrend').hide();
                    //                    $('#UpdateChart').hide();
                    if ($('#spdChart').is(':visible')) {
                        $('#causepost').trigger('click')
                    }
                }
                else {
                    spread.SetColWidth(1, 22);
                    if (colcount < 9) {
                        spread.SetColWidth(3, 250);
                    }
                    else if (colcount > 12) {
                        spread.SetColWidth(3, 150);
                    }
                    else {
                        spread.SetColWidth(3, 200);
                    }
                    var rc = spread.GetRowCount();
                    var count = 0;
                    for (var i = 0; i < rc; i++) {
                        var chkboxval = spread.GetValue(i, 1);
                        if (chkboxval == "true") {
                            count++;
                        }
                    }
                    if (count == 1) {
                        $('#UpdateChart').show();
                        if (selectedindex == 3) {
                            $('#ShowTrend').show();
                        }
                        else {
                            $('#ShowTrend').hide();
                        }
                    }
                    else if (count > 1) {
                        $('#UpdateChart').show();
                    }
                }
            }
        });

        $('#ShowTrend').change(function () {
            $('#UpdateChart').show();
        });

        $("#intHighPosVar").keyup(function () {
            $("#valintHighPosVar").hide();
            var inputVal = $(this).val();
            var numericReg = /^\d*[0-9](|.\d*[0-9]|,\d*[0-9])?$/;
            if (!numericReg.test(inputVal)) {
                $("#intHighPosVar").val('');
                $("#valintHighPosVar").show();
            }
        });

        $("#intHighNegVar").keyup(function () {
            $("#valintHighNegVar").hide();
            var inputVal = $(this).val();
            var numericReg = /^\d*[0-9](|.\d*[0-9]|,\d*[0-9])?$/;
            if (!numericReg.test(inputVal)) {
                $("#intHighNegVar").val('');
                $("#valintHighNegVar").show();
            }
        });

        $("#decFilterAmt").keyup(function () {
            $("#valDecFilterAmt").hide();
            var inputVal = $(this).val();
            var numericReg = /^\d*[0-9](|.\d*[0-9]|,\d*[0-9])?$/;
            if (!numericReg.test(inputVal)) {
                $("#decFilterAmt").val('');
                $("#valDecFilterAmt").show();
            }
        });

        //on load
        $(document).ready(function () {
            IsSessionAlive();         
            
            $("input:checkbox").each(function (index) {
                $("<label>").attr("for", this.id)
                .attr("class", "styled_checkbox dashboard")
                .insertAfter(this);
            });

            var selectedindex = 0;
            if ($('#intChartType').get(0) != undefined) {
                selectedindex = $('#intChartType').get(0).selectedIndex;
                
            }            
            if (selectedindex > 0) {
                $('#chart').show()
                $('#chart').css('height', '200px');
                $('#sidebar').css('height', '800px')
                //                $('#UpdateChart').show();
                var spread = document.getElementById("spdAnalytics");
                var colcount = spread.GetColCount();
                spread.SetColWidth(1, 22);
                if (colcount < 9) {
                    spread.SetColWidth(3, 250);
                }
                else if (colcount > 12) {
                    spread.SetColWidth(3, 150);
                }
                else {
                    spread.SetColWidth(3, 200);
                }
            }
            else {
                $('#sidebar').css('height', '600px')
            }
            $('#spread').show()

            if (selectedindex == 3) {
                $('#ShowTrend').show();
            }
            else {
                $('#ShowTrend').hide();
                $('#blnShowTrend').removeAttr('checked');
            }

            var selectedindex = 0;
            if ($('#intFormat').get(0) != undefined) {
                selectedindex = $('#intFormat').get(0).selectedIndex; 
            }
            if (selectedindex == 0) {
                intSaveFormat = selectedindex - 1
            }
            else {
                intSaveFormat = selectedindex
            } 
            if (selectedindex > 2) {
                $('#optionsimage').hide()
                $('#chkHighlightVar').hide()
                $('#HighlightVarControls').hide()
                $('#CheckBoxFilter').hide() 
                if (selectedindex == 10) {
                    $('#hperioddd').show()
                }
            }
            else {
                $('#blnHighVar').removeAttr('checked')
            }
            //adjust width
            var browsewidth = $(window).width();
            $('#intBrowserWidth').val(browsewidth)
            var spdWidth = $('#spdAnalytics').width();
            var calcwidth = spdWidth + 250;
            if (calcwidth > 1100) {
                if (calcwidth < browsewidth) {
                    //$('#main').css('padding-left', '20px');
                    $(".header").width('1275');
                    $(".nav-panel").width('1275');
                    $(".main-container").width('1275px');
                }
                else {
                }
            }
            //hide detail rows  
            if (document.getElementById("spdAnalytics")) {
                var spread = document.getElementById("spdAnalytics");
                var rc = spread.GetRowCount();
                var showclass = false
                for (var i = 0; i < rc; i++) {
                    var cellval = spread.GetValue(i, 2);
                    if (cellval == "Detail") {
                        if (showclass == false) {
                            spread.Rows(i).style.display = "none";
                        }
                    }
                    if (cellval == "OpCash") {
                        if (showclass == false) {
                            spread.Rows(i).style.display = "none";
                        }
                    }
                    if (cellval == "Heading") {
                        var cellvalue = spread.GetValue(i, 0);
                        if (cellvalue == "-") {
                            showclass = true
                            spread.Rows(i).style.display = "table-row";
                        }
                        else {
                            showclass = false
                            spread.Rows(i).style.display = "none";
                        }
                    }
                }
            }
        });

        window.onload = function () {
            if (document.getElementById("spdAnalytics")) {
                var spread = document.getElementById("spdAnalytics");
                if (document.all) {
                    if (spread.addEventListener) {
                        spread.addEventListener("ActiveCellChanged", cellChanged, false);
                    }
                    else {
                        spread.onActiveCellChanged = cellChanged;
                    }
                }
                else {
                    spread.addEventListener("ActiveCellChanged", cellChanged, false);
                }
            }
        }

        function cellChanged(event) {
            if (event.col == 0 && document.getElementById("spdAnalytics")) {
                var spread = document.getElementById("spdAnalytics");
                var cellval = spread.GetValue(event.row, 0);
                var rc = spread.GetRowCount();
                if (cellval == "-") {
                    for (var i = event.row; i < rc; i++) {
                        var rowtype = spread.GetValue(i, 2);
                        if (rowtype == "Total") {
                            cell = document.getElementById("spdAnalytics").GetCellByRowCol(i, 0);
                            cell.removeAttribute("FpCellType");
                            spread.SetValue(i, 0, "+", false);
                            cell.setAttribute("FpCellType", "readonly");
                            spread.Rows(i).style.display = "table-row";
                            break;
                        }
                        else if (rowtype == "Heading") {
                            cell = document.getElementById("spdAnalytics").GetCellByRowCol(i, 0);
                            cell.removeAttribute("FpCellType");
                            spread.SetValue(i, 0, "+", false);
                            cell.setAttribute("FpCellType", "readonly");
                            spread.Rows(i).style.display = "none";
                        }
                        else {

                            spread.Rows(i).style.display = "none";
                        }
                    }
                }
                else if (cellval == "+") {

                    for (var i = event.row; i > -1; i--) {
                        var rowtype = spread.GetValue(i, 2);
                        if (rowtype == "Total") {
                            cell = document.getElementById("spdAnalytics").GetCellByRowCol(i, 0);
                            cell.removeAttribute("FpCellType");
                            spread.SetValue(i, 0, "", false);
                            cell.setAttribute("FpCellType", "readonly");
                            if ($('#blnUseFilter').is(':checked')) {
                                spread.Rows(i).style.display = "none";
                            }
                        }
                        else if (rowtype == "Heading") {
                            cell = document.getElementById("spdAnalytics").GetCellByRowCol(i, 0);
                            cell.removeAttribute("FpCellType");
                            spread.SetValue(i, 0, "-", false);
                            cell.setAttribute("FpCellType", "readonly");
                            spread.Rows(i).style.display = "table-row";
                            break
                        }
//                        else if ($('#blnUseFilter').is(':checked')) {
//                            HideUnFilterRow(i)
//                        }
                        else {
                            spread.Rows(i).style.display = "table-row";
                        }
                    }
                }
                spread.SetActiveCell(event.row, 1)
            }
        }

        function HideUnFilterRow(row) {
            var selindex = $('#intFilterOn').get(0).selectedIndex;
            var filteramt = $('#decFilterAmt').val();
            var amount = parseFloat(filteramt);
            var selradio = $('#radioPosVar').attr('checked');
            var spread = document.getElementById("spdAnalytics");
            var coladj = selindex
            if (selindex > 2) {
                coladj = 1 + selindex
            }
            var value = parseFloat(spread.GetValue(row, coladj + 6).replace(',', ''));
            if (value < filteramt) {
                if (selradio == "checked") {
                    spread.Rows(row).style.display = "none";
                }
                else {
                    spread.Rows(row).style.display = "table-row";
                }
            }
            else {
                if (selradio == "checked") {
                    spread.Rows(row).style.display = "table-row";

                }
                else {
                    spread.Rows(row).style.display = "none";
                }
            }
        }


    });
</script>
