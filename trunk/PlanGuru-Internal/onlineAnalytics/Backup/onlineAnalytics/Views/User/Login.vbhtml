﻿@ModelType  onlineAnalytics.LoginModel 
@Code
    ViewData("Title") = "Login"
    If Not Session("action") Is Nothing Then
        
        If (Session("action") = "SetDefaultAnalysis" or Session("action") = "GetAnalyses") Then
            Layout = Nothing
            Session.Remove("action")
        End If
    Else
        Layout = "~/Views/Shared/AnalyticsMaster.vbhtml"
    End If
End Code


<script src="@Url.Content("~/Scripts/jquery.validate.min.js")" type="text/javascript"></script>
<script src="@Url.Content("~/Scripts/jquery.validate.unobtrusive.min.js")" type="text/javascript"></script>

<h2>User</h2><br />
@Using Html.BeginForm("Login", "User")
    @Html.AntiForgeryToken()   
    @Html.ValidationSummary(True)    
     
    @<fieldset>
            <legend>Sign in</legend>
        <div style="width: 100;">
            <div style="width:30%;">
                <ol class="round">
                    <li>
                        @Html.LabelFor(Function(m) m.UserName)<br />
                        @Html.TextBoxFor(Function(m) m.UserName)<br />
                        @Html.ValidationMessageFor(Function(m) m.UserName)
                    </li>
                    <li>
                        @Html.LabelFor(Function(m) m.Password)<br />
                        @Html.PasswordFor(Function(m) m.Password)<br />
                        @Html.ValidationMessageFor(Function(m) m.Password)
                    </li>
                </ol>
            </div>                   
            <div class="input-form">
                <input type="submit" value="Sign in" class="secondbutton_example" />                              
            </div>
            <div style="float:left;margin-left:20px;">
                 @If Not (DirectCast(TempData("Message"), String) Is Nothing) Then
                    @<label class="success">@TempData("Message").ToString()
                    </label>                         
                End If
                @If Not (DirectCast(TempData("ErrorMessage"), String) Is Nothing) Then
                    @<label class="error">@TempData("ErrorMessage").ToString()
                    </label>                         
                End If
            </div>
        </div>
    </fieldset>                                  
End Using