﻿Imports FarPoint.Mvc.Spread
Imports System.Data.Entity
Namespace onlineAnalytics

    <CustAuthFilter()>
    Public Class REController
        Inherits System.Web.Mvc.Controller
        Dim sFormat As Integer = 1
        Dim sPeriod As Integer = Val(DateTime.Today.ToString("MM")) - 2
        Dim sHPeriod As Integer = 1
        Dim intFirstNumberColumn As Integer = 6
        Dim intRowTypeCol As Integer = 3
        Dim blnSTColVisible As Boolean = True
        Private blnShowBudgetChecked As Boolean
        Private blnShowasPercentChecked As Boolean
        Private blnArchBudget As Boolean
        Private intUserID As Integer
        Private intAnalysisID As Integer
        Private intFirstFiscal As Integer
        Private intFirstYear As Integer
        Private intSelHPeriod As Integer
        Private intNumberofPeriods As Integer
        Private isViewer As Boolean = False
        Private accountRepository As IAccountRepository
        Private accountTypeRepository As IAccountTypeRepository
        Private balanceRepository As IBalanceRepository
        Private chartFormatRepository As IChartFormatRepository
        Private chartTypeRepository As IChartTypeRepository
        Private UserInfo As User
        Private saveViewRepository As ISaveViewRepository
        Private typeDescList() As String = {"revenue", "expense"}
        Public Sub New()
            Me.chartFormatRepository = New ChartFormatRepository(New DataAccess())
            Me.chartTypeRepository = New ChartTypeRepository(New DataAccess())
            Me.accountRepository = New AccountRepository(New DataAccess())
            Me.accountTypeRepository = New AccountTypeRepository(New DataAccess())
            Me.balanceRepository = New BalanceRepository(New DataAccess())
            Me.saveViewRepository = New SaveViewRepository(New DataAccess())
        End Sub


        Public Sub New(chartFormatRepository As IChartFormatRepository)
            Me.chartFormatRepository = chartFormatRepository
        End Sub

        Public Sub New(chartTypeRepository As IChartTypeRepository)
            Me.chartTypeRepository = chartTypeRepository
        End Sub

        Public Sub New(accountRepository As IAccountRepository)
            Me.accountRepository = accountRepository
        End Sub

        Public Sub New(accountTypeRepository As IAccountTypeRepository)
            Me.accountTypeRepository = accountTypeRepository
        End Sub

        Public Sub New(balanceRepository As IBalanceRepository)
            Me.balanceRepository = balanceRepository
        End Sub

        Public Sub New(saveViewRepository As ISaveViewRepository)
            Me.saveViewRepository = saveViewRepository
        End Sub

        'Display Account type & Account items with options.
        'List of Companies, List of Month, List of Format & Chart Type
        'Options to select (positive / negative) variance, percentage
        ' GET: /RE
        <HttpGet()> _
        <CustomActionFilter()>
        Function Index(<MvcSpread("spdAnalytics")> ByVal spdAnalytics As FpSpread, ByVal formValues As FormValues) As ActionResult
            isViewer = False
            Dim setupCountObj As SetupCount = Nothing
            Dim selectedAnalysis As Integer

            Try
                Logger.Log.Info(String.Format("REController Index (HttpGet) method execution starts"))
                blnArchBudget = DirectCast(Session("BudgetArch"), Boolean)
                blnShowBudgetChecked = DirectCast(Session("ShowBudget"), Boolean)
                blnShowasPercentChecked = DirectCast(Session("ShowasPercent"), Boolean)

                If (Not Session("UserType") Is Nothing) And (Not Session("UserInfo") Is Nothing) Then
                    UserInfo = DirectCast(Session("UserInfo"), User)
                    sPeriod = Session("sPeriod")
                    sFormat = Session("sFormat")
                    Try
                        sHPeriod = DirectCast(Session("HPeriod"), Integer)
                    Catch ex As Exception
                        sHPeriod = 1
                    End Try

                    If (UserInfo.AnalysisId = 0) Then
                        Return RedirectToAction("Index", "Dashboard")
                    Else
                        intAnalysisID = UserInfo.AnalysisId
                        intFirstFiscal = UserInfo.fiscalMonthOfSelectedCompany
                    End If
                End If

                If (UserInfo.AnalysisId > 0) Then
                    selectedAnalysis = UserInfo.AnalysisId
                Else
                    If (Not Session("SelectedAnalysisFromDropdown") Is Nothing) Then
                        selectedAnalysis = Session("SelectedAnalysisFromDropdown")
                    Else
                        selectedAnalysis = -1
                    End If
                End If

                LoadSetupCount(setupCountObj, UserInfo, selectedAnalysis)

                Dim intCtr As Integer
                Dim periods As List(Of SelectListItem) = New List(Of SelectListItem)
                intNumberofPeriods = NumberofPeriods(sPeriod, intFirstFiscal)
                'For intCtr = 0 To intNumberofPeriods
                For intCtr = 0 To 11
                    periods.Add(New SelectListItem With {.Text = CommonProcedures.GetMonth(intCtr, intFirstFiscal), .Value = intCtr + 1, .Selected = False})
                Next
                ViewData("Periods") = New SelectList(periods, "value", "text", "May")

                Dim Formats As List(Of SelectListItem) = New List(Of SelectListItem)

                For Each chartFormat As ChartFormat In chartFormatRepository.GetChartFormats()
                    Select Case chartFormat.ChartFormatId
                        Case 1, 3, 4, 6 To 13
                            Formats.Add(New SelectListItem With {.Text = chartFormat.Format, .Value = chartFormat.ChartFormatId, .Selected = False})
                    End Select
                Next
                '  Dim Formats As List(Of SelectListItem) = (From chartFormat In chartFormatRepository.GetChartFormats() Select New SelectListItem With {.Text = chartFormat.Format, .Value = chartFormat.ChartFormatId, .Selected = False}).ToList()

                ViewData("Formats") = New SelectList(Formats, "value", "text")

                If sFormat = 2 Then
                    Dim filterOn As List(Of SelectListItem) = New List(Of SelectListItem) From {
                        New SelectListItem With {.Text = "Current period amount", .Value = 1, .Selected = False},
                        New SelectListItem With {.Text = "Variance amount", .Value = 2, .Selected = False},
                        New SelectListItem With {.Text = "Percent variance", .Value = 3, .Selected = True}
                    }
                    ViewData("FilterOn") = New SelectList(filterOn, "value", "text", "selected")
                Else
                    Dim filterOn As List(Of SelectListItem) = New List(Of SelectListItem) From {
                        New SelectListItem With {.Text = "Current period amount", .Value = 1, .Selected = False},
                        New SelectListItem With {.Text = "Variance amount", .Value = 2, .Selected = False},
                        New SelectListItem With {.Text = "Percent variance", .Value = 3, .Selected = True},
                        New SelectListItem With {.Text = "YTD amount", .Value = 4, .Selected = False},
                        New SelectListItem With {.Text = "YTD Variance amount", .Value = 5, .Selected = False},
                        New SelectListItem With {.Text = "YTD Percent variance", .Value = 6, .Selected = True}
                    }
                    ViewData("FilterOn") = New SelectList(filterOn, "value", "text", "selected")
                End If

                Dim chartTypes As List(Of SelectListItem) = New List(Of SelectListItem)
                If sFormat < 6 Then
                    chartTypes.Add(New SelectListItem With {.Text = "None", .Value = 1, .Selected = True})
                    chartTypes.Add(New SelectListItem With {.Text = "Bar", .Value = 2, .Selected = False})
                Else
                    For Each chartType As ChartType In chartTypeRepository.GetChartTypes()
                        chartTypes.Add(New SelectListItem With {.Text = chartType.Type, .Value = chartType.ChartTypeId, .Selected = False})
                    Next
                End If


                'ew 1/14 chg for historical periods
                Dim hperiods As List(Of SelectListItem) = New List(Of SelectListItem)
                intFirstYear = IIf(UserInfo.FirstYear <> Nothing And UserInfo.FirstYear > 0, UserInfo.FirstYear, DateTime.Now.Year())
                Dim intHistPeriods As Integer = DirectCast(Session("NumberofHYears"), Integer)
                For intCtr = 1 To intHistPeriods
                    If intFirstFiscal = 1 Then
                        hperiods.Add(New SelectListItem With {.Text = intFirstYear - intCtr, .Value = intCtr + 1, .Selected = False})
                    Else
                        Dim strYear As String = intFirstYear - (intCtr + 1) & "/" & intFirstYear - intCtr
                        hperiods.Add(New SelectListItem With {.Text = strYear, .Value = intCtr + 1, .Selected = False})
                    End If
                Next
                ViewData("HPeriods") = New SelectList(hperiods, "value", "text")
                ViewData("ChartType") = New SelectList(chartTypes, "value", "text", "selected")
                ViewData("blnHighVar") = False
                ViewData("blnShowPercent") = blnShowasPercentChecked
                ViewData("blnShowBudget") = blnShowBudgetChecked
                ViewData("intPeriod") = sPeriod + 1 'intNumberofPeriods + 1
                ViewData("intFormat") = sFormat + 1
                ViewData("intHPeriod") = sHPeriod + 1
                ViewData("intChartType") = 1
                ViewData("blnShowTrend") = False
                ViewData("blnShowBudget") = True
                ViewData("blnUseFilter") = False
                ViewData("intDept") = 1
                ViewData("Postback") = False
                If blnArchBudget = True Then
                    ViewData("strCheckboxCaption") = "Show forecast amounts"
                Else
                    ViewData("strCheckboxCaption") = "Show budget amounts"
                End If
                Logger.Log.Info(String.Format("REController Index (HttpGet) method execution Ends"))
            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Index : Error occured while loading Revenue & Expense Chart -", ex.Message)
                Logger.Log.Error(String.Format("REController Index (HttpGet) method execution ends with Error message: {0}, StackTrace: {1}", ex.Message, ex.StackTrace))

                LoadSetupCount(setupCountObj, UserInfo, selectedAnalysis)

            End Try

            Return View()
        End Function

        Private Sub LoadSetupCount(setupCountObj As SetupCount, UserInfo As User, selectedAnalysis As Integer)
            setupCountObj = Utility.CheckRequiredSetupCount(UserInfo, selectedAnalysis)
            ViewBag.Setup = setupCountObj
        End Sub

        'Post 
        'It will render the chart based on selected options available for accounts.
        <HttpPost()> _
        <CustomActionFilter()>
        Function Index(<MvcSpread("spdAnalytics")> ByVal spdAnalytics As FpSpread, <MvcSpread("spdChart")> ByVal spdChart As FpSpread, ByVal formValues As FormValues) As ActionResult
            isViewer = False
            ' Dim intCounter As Integer
            Dim setupCountObj As SetupCount = Nothing
            Dim selectedAnalysis As Integer
            Dim strFilteron As String = formValues.strFilterOn
            '   Dim intSelHPeriod As Integer = formValues.intHPeriod - 1
            Dim blnHasTotalCol As Boolean = True
            Logger.Log.Info(String.Format("REController Index (HttpPost) method execution Starts"))
            Try
                blnArchBudget = DirectCast(Session("BudgetArch"), Boolean)
                blnShowBudgetChecked = DirectCast(Session("ShowBudget"), Boolean)
                blnShowasPercentChecked = DirectCast(Session("ShowasPercent"), Boolean)
                If (Not Session("UserType") Is Nothing) And (Not Session("UserInfo") Is Nothing) Then
                    UserInfo = DirectCast(Session("UserInfo"), User)

                    If (UserInfo.AnalysisId = 0) Then
                        Return RedirectToAction("Index", "Dashboard")
                    Else
                        intAnalysisID = UserInfo.AnalysisId
                        intFirstFiscal = UserInfo.fiscalMonthOfSelectedCompany
                    End If
                End If

                If (UserInfo.AnalysisId > 0) Then
                    selectedAnalysis = UserInfo.AnalysisId
                Else
                    If (Not Session("SelectedAnalysisFromDropdown") Is Nothing) Then
                        selectedAnalysis = Session("SelectedAnalysisFromDropdown")
                    Else
                        selectedAnalysis = -1
                    End If
                End If

                LoadSetupCount(setupCountObj, UserInfo, selectedAnalysis)

                If (spdChart.Sheets(0).Charts.Count > 0) Then
                    spdChart.Sheets(0).Charts.Remove(spdChart.Sheets(0).Charts(0))
                End If

            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Index : Error occured while loading Revenue & Expense Chart -", ex.Message)
                Logger.Log.Error(String.Format("REController Index (HttpPost) method execution ends with Error message: {0}, StackTrace: {1}", ex.Message, ex.StackTrace))
                LoadSetupCount(setupCountObj, UserInfo, selectedAnalysis)
            End Try
            Try
                spdAnalytics.Sheets(0).RowCount = 0
                'ew 2/15 

                If formValues.blnShowasPer = True Then
                    Session("ShowasPercent") = True
                End If
                intFirstYear = IIf(UserInfo.FirstYear <> Nothing And UserInfo.FirstYear > 0, UserInfo.FirstYear, DateTime.Now.Year())
                SetSpreadProperties(spdAnalytics, formValues.intFormat - 1, formValues.intPeriod - 1, formValues.blnShowasPer, formValues.intHPeriod - 1)
                '   Else
                If formValues.intChartType <> 1 Then

                    Charts.BuildChart(Me.ControllerContext.Controller, spdChart, spdAnalytics, formValues.intChartType, formValues.intFormat - 1, intFirstNumberColumn, formValues.blnShowasPer, formValues.blnShowTrend, blnHasTotalCol)
                    ViewData("blnUpdateChart") = True
                End If
                Dim periods As List(Of SelectListItem) = New List(Of SelectListItem)
                intNumberofPeriods = NumberofPeriods(sPeriod, intFirstFiscal)
                'For intCtr = 0 To intNumberofPeriods
                For intCtr = 0 To 11
                    periods.Add(New SelectListItem With {.Text = CommonProcedures.GetMonth(intCtr, intFirstFiscal), .Value = intCtr + 1, .Selected = False})
                Next
                ViewData("Periods") = New SelectList(periods, "value", "text", "May")

                Dim Formats As List(Of SelectListItem) = New List(Of SelectListItem)
                For Each chartFormat As ChartFormat In chartFormatRepository.GetChartFormats()
                    Select Case chartFormat.ChartFormatId
                        Case 1, 3, 4, 6 To 13
                            Formats.Add(New SelectListItem With {.Text = chartFormat.Format, .Value = chartFormat.ChartFormatId, .Selected = False})
                    End Select
                Next
                ViewData("Formats") = New SelectList(Formats, "value", "text")
                If formValues.intFormat = 3 Then
                    Dim filterOn As List(Of SelectListItem) = New List(Of SelectListItem) From {
                        New SelectListItem With {.Text = "Current period amount", .Value = 1, .Selected = False},
                        New SelectListItem With {.Text = "Variance amount", .Value = 2, .Selected = False},
                        New SelectListItem With {.Text = "Percent variance", .Value = 3, .Selected = True}
                    }
                    ViewData("FilterOn") = New SelectList(filterOn, "value", "text", "selected")
                Else
                    Dim filterOn As List(Of SelectListItem) = New List(Of SelectListItem) From {
                        New SelectListItem With {.Text = "Current period amount", .Value = 1, .Selected = False},
                        New SelectListItem With {.Text = "Variance amount", .Value = 2, .Selected = False},
                        New SelectListItem With {.Text = "Percent variance", .Value = 3, .Selected = True},
                        New SelectListItem With {.Text = "YTD amount", .Value = 4, .Selected = False},
                        New SelectListItem With {.Text = "YTD Variance amount", .Value = 5, .Selected = False},
                        New SelectListItem With {.Text = "YTD Percent variance", .Value = 6, .Selected = True}
                    }
                    ViewData("FilterOn") = New SelectList(filterOn, "value", "text", "selected")
                End If
                'Dim filterOn As List(Of SelectListItem) = New List(Of SelectListItem) From {
                '   New SelectListItem With {.Text = "Current period amount", .Value = 1, .Selected = False},
                '    New SelectListItem With {.Text = "Variance amount", .Value = 2, .Selected = False},
                '    New SelectListItem With {.Text = "Percent variance", .Value = 3, .Selected = True},
                '    New SelectListItem With {.Text = "YTD amount", .Value = 4, .Selected = False},
                '    New SelectListItem With {.Text = "YTD Variance amount", .Value = 5, .Selected = False},
                '    New SelectListItem With {.Text = "YTD Percent variance", .Value = 6, .Selected = True}
                '}
                'ViewData("FilterOn") = New SelectList(filterOn, "value", "text", "selected")

                'ew 1/14 chg for historical periods
                Dim hperiods As List(Of SelectListItem) = New List(Of SelectListItem)

                Dim intHistPeriods As Integer = DirectCast(Session("NumberofHYears"), Integer)
                For intCtr = 1 To intHistPeriods
                    If intFirstFiscal = 1 Then
                        hperiods.Add(New SelectListItem With {.Text = intFirstYear - intCtr, .Value = intCtr + 1, .Selected = False})
                    Else
                        Dim strYear As String = intFirstYear - (intCtr + 1) & "/" & intFirstYear - intCtr
                        hperiods.Add(New SelectListItem With {.Text = strYear, .Value = intCtr + 1, .Selected = False})
                    End If
                Next
                ViewData("HPeriods") = New SelectList(hperiods, "value", "text")

                Dim chartTypes As List(Of SelectListItem) = New List(Of SelectListItem)
                If formValues.intFormat < 6 Then
                    chartTypes.Add(New SelectListItem With {.Text = "None", .Value = 1, .Selected = False})
                    chartTypes.Add(New SelectListItem With {.Text = "Bar", .Value = 2, .Selected = False})
                Else
                    For Each chartType As ChartType In chartTypeRepository.GetChartTypes()
                        chartTypes.Add(New SelectListItem With {.Text = chartType.Type, .Value = chartType.ChartTypeId, .Selected = False})
                    Next
                End If

                ViewData("ChartType") = New SelectList(chartTypes, "value", "text", "selected")
                ViewData("intChartType") = formValues.intChartType
                ViewData("blnHighVar") = False
                ViewData("blnShowPercent") = blnShowasPercentChecked
                ViewData("blnShowBudget") = blnShowBudgetChecked
                ViewData("Period") = formValues.intPeriod - 1
                ViewData("intDept") = 1
                ViewData("intFormat") = formValues.intFormat
                ViewData("intHPeriod") = formValues.intHPeriod
                If blnArchBudget = True Then
                    ViewData("strCheckboxCaption") = "Show forecast amounts"
                Else
                    ViewData("strCheckboxCaption") = "Show budget amounts"
                End If
                ViewData("postback") = True
                Session("sPeriod") = formValues.intPeriod - 1
                Session("sFormat") = formValues.intFormat - 1
                If formValues.intFormat = 13 Then
                    Session("HPeriod") = formValues.intHPeriod - 1
                End If
                Logger.Log.Info(String.Format("REController Index (HttpPost) method execution Ends"))
            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Index : Error occured while loading Revenue & Expense Chart -", ex.Message)
                Logger.Log.Error(String.Format("REController Index (HttpPost) method execution ends with Error message: {0}, StackTrace: {1}", ex.Message, ex.StackTrace))
                LoadSetupCount(setupCountObj, UserInfo, selectedAnalysis)
            End Try
            Return View()
        End Function

        <MvcSpreadEvent("Load", "spdAnalytics", DirectCast(Nothing, String()))> _
        Private Sub FpSpread_Load(ByVal sender As Object, ByVal e As EventArgs)
            Try
                Logger.Log.Info(String.Format("REController:FpSpread_Load method execution Starts"))
                Dim spread As FpSpread = DirectCast(sender, FpSpread)
                If Not spread.Page.IsPostBack Then
                    SetSpreadProperties(spread, sFormat, intNumberofPeriods, blnShowasPercentChecked, sHPeriod)
                End If
                Logger.Log.Info(String.Format("REController:FpSpread_Load method execution Ends"))
            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("FpSpread_Load : Error occured while processing Revenue & Expense Chart -", ex.Message)
                Logger.Log.Error(String.Format("REController FpSpread_Load method execution ends with Error message: {0}, StackTrace: {1}", ex.Message, ex.StackTrace))
            End Try

        End Sub

        <MvcSpreadEvent("PreRender", "spdAnalytics", DirectCast(Nothing, String()))> _
        Private Sub FpSpread_PreRender(ByVal sender As Object, ByVal e As EventArgs)
            Try
                Logger.Log.Info(String.Format("REController:FpSpread_PreRender method execution Starts"))
                Dim spread As FpSpread = DirectCast(sender, FpSpread)
                Logger.Log.Info(String.Format("REController:FpSpread_PreRender method execution Ends"))
            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("FpSpread_PreRender : Error occured while processing Revenue & Expense Chart -", ex.Message)
                Logger.Log.Error(String.Format("REController FpSpread_PreRender method execution ends with Error message: {0}, StackTrace: {1}", ex.Message, ex.StackTrace))
            End Try
        End Sub

        Private Sub SetSpreadProperties(ByVal spdAnalytics As FpSpread, ByVal intDropDownSel As Integer, ByVal intPeriod As Integer, ByRef blnShowasPercent As Boolean, intHPeriod As Integer)
            Try
                Logger.Log.Info(String.Format("REController:SetSpreadProperties method execution Starts"))
                Dim intColCount As Integer = 0
                Dim intNumberColumn As Integer = 0
                Dim intPercentColumn As Integer = 0
                Dim intCtr As Integer
                Dim intColCtr As Integer
                Dim intFormat As Integer
                Dim blnShowBudget As Boolean

                intFormat = CommonProcedures.ConvertFormat(intDropDownSel, blnShowBudget)
                'set column count
                Select Case intFormat
                    Case 0, 3
                        intColCount = intFirstNumberColumn + 8
                        intNumberColumn = 3
                        intPercentColumn = 1
                    Case 2
                        intColCount = intFirstNumberColumn + 4
                        intNumberColumn = 3
                        intPercentColumn = 1
                    Case 5
                        If blnShowBudget = True Then
                            intNumberColumn = 13
                            intPercentColumn = 13
                        Else
                            intNumberColumn = intPeriod + 2
                            intPercentColumn = intPeriod + 2
                        End If
                        intColCount = intFirstNumberColumn + intNumberColumn
                        'intNumberColumn = intPeriod + 2
                        'intPercentColumn = intPeriod + 2
                        'intColCount = intFirstNumberColumn + intNumberColumn
                    Case 6, 9, 10, 11
                        intNumberColumn = 13
                        intPercentColumn = 13
                        intColCount = intFirstNumberColumn + 13
                    Case 7, 8
                        intNumberColumn = 6
                        intPercentColumn = 6
                        intColCount = intFirstNumberColumn + 6
                End Select
                CommonProcedures.SetCommonProperties(spdAnalytics, intColCount, blnShowasPercent, intFirstNumberColumn, intNumberColumn, intFormat, "RE")
                Dim intRowCt = spdAnalytics.Sheets(0).RowCount
                'load rows
                Select Case intFormat
                    Case 0 To 3
                        LoadSpread0to4(spdAnalytics, intFormat, intPeriod, intNumberColumn, intFirstNumberColumn, intColCount, blnShowasPercent)
                        spdAnalytics.Sheets(0).Columns(intFirstNumberColumn, intFirstNumberColumn + 3).Visible = True
                    Case 5
                        '  Dim intCurrentMonth As Integer = intPeriod
                        LoadSpread5(spdAnalytics, intFormat, intPeriod, intNumberColumn, intFirstNumberColumn, blnShowasPercent, intColCount, blnShowBudget)
                        spdAnalytics.Sheets(0).Columns(intFirstNumberColumn, intFirstNumberColumn + intNumberColumn - 1).Visible = True
                    Case 6
                        LoadSpread6(spdAnalytics, intFormat, intPeriod, intNumberColumn, intFirstNumberColumn, blnShowasPercent, intColCount)
                        spdAnalytics.Sheets(0).Columns(intFirstNumberColumn, intFirstNumberColumn + intNumberColumn - 1).Visible = True
                    Case 7, 8
                        LoadSpread7to8(spdAnalytics, intFormat, intPeriod, intNumberColumn, intFirstNumberColumn, blnShowasPercent, intColCount)
                        Dim blnLoop As Boolean = True
                        intCtr = 0
                        intNumberColumn = 6
                        For intColCtr = intFirstNumberColumn To intFirstNumberColumn + 5
                            spdAnalytics.Sheets(0).Columns(intColCtr).Visible = True
                        Next
                        intCtr = -1
                        Do While blnLoop = True
                            intCtr += 1
                            If spdAnalytics.Sheets(0).GetValue(intCtr, intRowTypeCol) = "Total" Then
                                For intColCtr = intFirstNumberColumn To intFirstNumberColumn + 5
                                    If spdAnalytics.Sheets(0).GetValue(intCtr, intColCtr) = 0 Then
                                        spdAnalytics.Sheets(0).Columns(intColCtr).Visible = False
                                        intColCount -= 1
                                        intNumberColumn -= 1
                                    Else
                                        blnLoop = False
                                        Exit For
                                    End If
                                Next
                            End If
                            If intCtr = spdAnalytics.Sheets(0).RowCount - 1 Then
                                blnLoop = False
                            End If
                        Loop
                    Case 9
                        LoadSpread9(spdAnalytics, intFormat, intPeriod, intNumberColumn, intFirstNumberColumn, blnShowasPercent, intColCount)
                        spdAnalytics.Sheets(0).Columns(intFirstNumberColumn, intFirstNumberColumn + intNumberColumn - 1).Visible = True
                    Case 10
                        LoadSpread10(spdAnalytics, intPeriod, intNumberColumn, intFirstNumberColumn, blnShowasPercent, intColCount, intHPeriod)
                        spdAnalytics.Sheets(0).Columns(intFirstNumberColumn, intFirstNumberColumn + intNumberColumn - 1).Visible = True
                    Case 11
                        LoadSpread10(spdAnalytics, intPeriod, intNumberColumn, intFirstNumberColumn, blnShowasPercent, intColCount, 0)
                        spdAnalytics.Sheets(0).Columns(intFirstNumberColumn, intFirstNumberColumn + intNumberColumn - 1).Visible = True
                End Select
                'set col widths
                CommonProcedures.SetColumnWidths(spdAnalytics, intColCount, intFirstNumberColumn, intFormat, intNumberColumn, blnSTColVisible, 4, "RE")
                If blnSTColVisible = True Then
                    spdAnalytics.Sheets(0).Columns(1).Width = 20
                Else
                    spdAnalytics.Sheets(0).Columns(1).Width = 0
                End If
                'load column headers
                Dim strHeadingTextBudget As String
                If blnArchBudget = False Then
                    strHeadingTextBudget = "Budget"
                Else
                    strHeadingTextBudget = "Forecast"
                End If
                CommonProcedures.SetColumnHeader(spdAnalytics, intFormat, intPeriod, intColCount, intFirstNumberColumn, intFirstFiscal, True, strHeadingTextBudget, intHPeriod, "RE", intFirstYear, blnShowBudget)
                Logger.Log.Info(String.Format("REController:SetSpreadProperties method execution Ends"))
            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("SetSpreadProperties : Error occured while processing Revenue & Expense Chart -", ex.Message)
                Logger.Log.Error(String.Format("REController:SetSpreadProperties method execution ends with Error message: {0}, StackTrace: {1}", ex.Message, ex.StackTrace))
            End Try

        End Sub

        <CustomActionFilter()>
        Public Sub LoadSpread0to4(ByVal spdAnalytics As FpSpread, ByVal intFormat As Integer, ByVal intPeriod As Integer, ByVal intNumberColumn As Integer, ByVal intFirstNumColumn As Integer, ByVal intColumnCount As Integer, ByRef blnShowasPercent As Boolean)
            Try
                Logger.Log.Info(String.Format("REController:LoadSpread0to4 method execution Starts"))
                Using utility As New Utility
                    Dim intRowCounter As Integer = -1
                    Dim intColumnCounter As Integer = 0
                    Dim strYearType(5) As String
                    Dim strPeriods(2) As String
                    Dim decAccumTotals(3) As Decimal
                    Dim decPeriodAmts(11) As Decimal
                    Dim decSaveAmts(7) As Decimal
                    Dim intSpanRows(1) As Integer
                    Dim chkbox As New FarPoint.Web.Spread.CheckBoxCellType
                    Dim blnTotalFlag As Boolean
                    Dim decIncAccumTotals(1) As Decimal
                    Dim strBrowser = Request.Browser.Browser
                    Dim strbrowerversion = Request.Browser.Version

                    If (isViewer) Then
                        chkbox.OnClientClick = "return false"
                    Else
                        chkbox.OnClientClick = CommonProcedures.CheckboxClicked(2)
                    End If


                    Logger.Log.Info(String.Format("REController:LoadSpread0to4 --> selectedAccounts Query execution Starts"))

                    'Dim selectedAccounts = (From a In utility.AccountRepository.GetAccountRecordsByAnalysisId(intAnalysisID)
                    '                        Join ac In utility.AccountTypeRepository.GetAccountTypeRecordsByAnalysisId(intAnalysisID, typeDescList)
                    '                        On a.AcctTypeId Equals ac.AcctTypeId Where a.AnalysisId = ac.AnalysisId
                    '                        Order By a.SortSequence
                    '                        Select a.AccountId, a.AcctTypeId, a.AnalysisId, a.AcctDescriptor, a.SortSequence, a.Description, a.Subgrouping, ac.TypeDesc, ac.ClassDesc
                    '                      ).ToList()

                    Dim selectedAccounts = (From a In utility.AccountRepository.GetSelectedAccounts(intAnalysisID, typeDescList)).ToList()

                    Logger.Log.Info(String.Format("REController:LoadSpread0to4 --> selectedAccounts Query execution Ends"))

                    Logger.Log.Info(String.Format("REController:LoadSpread0to4 --> Balance Query execution Starts"))

                    Dim balanceAcc = (From b In utility.BalanceRepository.GetBalanceRecordsByAnalysisId(intAnalysisID)
                                   Join acc In selectedAccounts On b.AccountId Equals acc.AccountId
                                   Where acc.AnalysisId = b.AnalysisId
                                   Select b).ToList()

                    Logger.Log.Info(String.Format("REController:LoadSpread0to4 --> Balance Query execution Ends"))


                    Logger.Log.Info(String.Format("REController:LoadSpread0to4 --> selectedAccounts For Each execution Starts"))

                    If blnShowasPercent = True Then
                        For Each account In selectedAccounts
                            'save amounts                         
                            If account.AcctDescriptor = "Total" And account.TypeDesc = "Revenue" Then
                                Dim accountId As Integer = account.AccountId
                                For Each balance As Balance In (From b In balanceAcc.Where(Function(a) a.AccountId = accountId And a.AnalysisId = intAnalysisID) Select b)
                                    Select Case intFormat
                                        Case 0, 3
                                            If intFormat = 0 Then
                                                If blnArchBudget = False Then
                                                    GetBudgetAmts(balance, decPeriodAmts)
                                                Else
                                                    GetArchivedBudgetAmts(balance, decPeriodAmts)
                                                End If
                                            Else
                                                GetH5Amts(balance, decPeriodAmts)
                                            End If
                                            CommonProcedures.AccumRunningTotals(intFormat, decAccumTotals, intPeriod, decPeriodAmts, 0)
                                            GetActualAmts(balance, decPeriodAmts)
                                            CommonProcedures.AccumRunningTotals(intFormat, decAccumTotals, intPeriod, decPeriodAmts, 1)
                                        Case 2
                                            GetActualAmts(balance, decPeriodAmts)
                                            If intPeriod = 0 Then
                                                CommonProcedures.AccumRunningTotals(intFormat, decAccumTotals, intPeriod, decPeriodAmts, 1)
                                                GetH5Amts(balance, decPeriodAmts)
                                                CommonProcedures.AccumRunningTotals(intFormat, decAccumTotals, intPeriod, decPeriodAmts, 0)
                                            Else
                                                CommonProcedures.AccumRunningTotals(intFormat, decAccumTotals, intPeriod - 1, decPeriodAmts, 0)
                                                CommonProcedures.AccumRunningTotals(intFormat, decAccumTotals, intPeriod, decPeriodAmts, 1)
                                            End If

                                    End Select
                                Next
                                Exit For
                            End If
                        Next
                    End If

                    For Each account In selectedAccounts
                        With account
                            AddRow(spdAnalytics, intRowCounter, .Description, .AcctDescriptor, .TypeDesc, strBrowser, 4)
                            Select Case .AcctDescriptor
                                Case "Heading"
                                    FormatHeaderRow(spdAnalytics, intRowCounter, intSpanRows, blnTotalFlag, 4)
                                Case "SHeading"
                                    FormatSHeaderRow(spdAnalytics, intRowCounter, intSpanRows, blnTotalFlag, 4)
                                Case "STotal"
                                    FormatSTotalRow(spdAnalytics, intRowCounter, intFirstNumberColumn, strBrowser)
                                Case "Total"
                                    FormatTotalRow(spdAnalytics, intRowCounter, intSpanRows, blnTotalFlag, intFirstNumColumn, intNumberColumn, .ClassDesc, strBrowser, 4)
                                Case "GPTotal", "IOTotal", "IBTTotal", "NITotal"
                                    Array.Clear(intSpanRows, 0, 1)
                                    FormatGrandTotalRow(spdAnalytics, intRowCounter, blnTotalFlag, intFirstNumberColumn, intNumberColumn, .ClassDesc, strBrowser, 4)
                            End Select
                        End With
                        'save accountID 
                        spdAnalytics.Sheets(0).SetValue(intRowCounter, 5, account.AccountId)
                        'save amounts                         
                        If account.AcctDescriptor <> "Heading" And account.AcctDescriptor <> "SHeading" Then
                            Array.Clear(decPeriodAmts, 0, 12)
                            Array.Clear(decSaveAmts, 0, 8)
                            Dim accountId As Integer = account.AccountId
                            spdAnalytics.Sheets(0).Cells(intRowCounter, 2).CellType = chkbox
                            For Each balance As Balance In (From b In balanceAcc.Where(Function(a) a.AccountId = accountId And a.AnalysisId = intAnalysisID) Select b)
                                Select Case intFormat
                                    Case 0, 3
                                        If intFormat = 0 Then
                                            If blnArchBudget = False Then
                                                GetBudgetAmts(balance, decPeriodAmts)
                                            Else
                                                GetArchivedBudgetAmts(balance, decPeriodAmts)
                                            End If
                                        Else
                                            GetH5Amts(balance, decPeriodAmts)
                                        End If
                                        decSaveAmts(0) = decPeriodAmts(intPeriod)
                                        For intColumnCounter = 0 To intPeriod
                                            decSaveAmts(4) += decPeriodAmts(intColumnCounter)
                                        Next
                                        GetActualAmts(balance, decPeriodAmts)
                                        decSaveAmts(1) = decPeriodAmts(intPeriod)
                                        For intColumnCounter = 0 To intPeriod
                                            decSaveAmts(5) += decPeriodAmts(intColumnCounter)
                                        Next
                                    Case 2
                                        GetActualAmts(balance, decPeriodAmts)
                                        If intPeriod = 0 Then
                                            decSaveAmts(1) = decPeriodAmts(intPeriod)
                                            GetH5Amts(balance, decPeriodAmts)
                                            decSaveAmts(0) = decPeriodAmts(11)
                                        Else
                                            decSaveAmts(0) = decPeriodAmts(intPeriod - 1)
                                            decSaveAmts(1) = decPeriodAmts(intPeriod)
                                        End If
                                End Select
                            Next
                            'store values
                            If blnShowasPercent = False Then
                                spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumColumn, decSaveAmts(0))
                                spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumColumn + 1, decSaveAmts(1))
                                Select Case intFormat
                                    Case 0, 3
                                        spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumColumn + 4, decSaveAmts(4))
                                        spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumColumn + 5, decSaveAmts(5))
                                End Select
                            Else
                                If decAccumTotals(0) <> 0 Then
                                    spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumColumn, decSaveAmts(0) / decAccumTotals(0))
                                    decSaveAmts(0) = decSaveAmts(0) / decAccumTotals(0)
                                Else
                                    spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumColumn, 0)
                                    decSaveAmts(0) = 0
                                End If
                                If decAccumTotals(1) <> 0 Then
                                    spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumColumn + 1, decSaveAmts(1) / decAccumTotals(1))
                                    decSaveAmts(1) = decSaveAmts(1) / decAccumTotals(1)
                                Else
                                    spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumColumn + 1, 0)
                                    decSaveAmts(1) = 0
                                End If
                                Select Case intFormat
                                    Case 0, 3
                                        If decAccumTotals(2) <> 0 Then
                                            spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumColumn + 4, decSaveAmts(4) / decAccumTotals(2))
                                            decSaveAmts(4) = decSaveAmts(4) / decAccumTotals(2)
                                        Else
                                            spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumColumn, 0)
                                            decSaveAmts(4) = 0
                                        End If
                                        If decAccumTotals(3) <> 0 Then
                                            spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumColumn + 5, decSaveAmts(5) / decAccumTotals(3))
                                            decSaveAmts(5) = decSaveAmts(5) / decAccumTotals(3)
                                        Else
                                            spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumColumn + 1, 0)
                                            decSaveAmts(5) = 0
                                        End If
                                End Select

                            End If
                            'compute(variances)
                            Select Case account.AcctDescriptor
                                Case "GPTotal", "IOTotal", "IBTTotal", "NITotal"
                                    CommonProcedures.ComputeVariances(decSaveAmts, "Reverse", decAccumTotals, blnShowasPercent)
                                Case Else
                                    Select Case account.ClassDesc
                                        Case "Other Income(Expense)"
                                            CommonProcedures.ComputeVariances(decSaveAmts, "Reverse", decAccumTotals, blnShowasPercent)
                                        Case Else
                                            CommonProcedures.ComputeVariances(decSaveAmts, account.TypeDesc, decAccumTotals, blnShowasPercent)
                                    End Select

                            End Select
                            spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumColumn + 2, decSaveAmts(2))
                            spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumColumn + 3, decSaveAmts(3))
                            Select Case intFormat
                                Case 0, 3
                                    spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumColumn + 6, decSaveAmts(6))
                                    spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumColumn + 7, decSaveAmts(7))
                            End Select

                        End If

                    Next
                    Logger.Log.Info(String.Format("REController:LoadSpread0to4 --> selectedAccounts For Each execution Ends"))
                    '    CommonProcedures.AddTotalRow(spdAnalytics, intFormat, intNumberColumn, intFirstNumColumn, intColumnCount, decIncAccumTotals, intRowCounter, "NI", "Net Income(Loss)", intPeriod)
                End Using
                Logger.Log.Info(String.Format("REController:LoadSpread0to4 method execution Ends"))
            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("LoadSpread0to4 : Error occured while processing Revenue & Expense Chart - ", ex.Message)
                Logger.Log.Error(String.Format("REController:LoadSpread0to4 method execution ends with Error message: {0}, StackTrace: {1}", ex.Message, ex.StackTrace))
            End Try

        End Sub

        <CustomActionFilter()>
        Public Sub LoadSpread5(ByVal spdAnalytics As FpSpread, ByVal intFormat As Integer, ByRef intCurrentMonth As Integer, ByVal intNumberColumn As Integer, ByVal intFirstNumberColumn As Integer, ByRef blnShowasPercent As Boolean, ByVal intColumnCount As Integer, ByVal blnShowBudget As Boolean)
            Try
                Logger.Log.Info(String.Format("REController:LoadSpread5 method execution Starts"))

                Using utility As New Utility()
                    Dim intRowCounter As Integer = -1
                    Dim intColumnCounter As Integer
                    Dim strYearType(5) As String
                    Dim strPeriods(2) As String
                    Dim decAccumTotals(12) As Decimal
                    Dim decNITotals(12) As Decimal
                    Dim decPeriodAmts(11) As Decimal
                    Dim decSaveAmts(12) As Decimal
                    Dim intSpanRows(1) As Integer
                    Dim blnTotalFlag As Boolean
                    Dim intNILastActual As Integer
                    Dim intRevLastActual As Integer
                    Dim intPeriods As Integer
                    Dim decIncAccumTotals(intNumberColumn) As Decimal
                    Dim chkbox As New FarPoint.Web.Spread.CheckBoxCellType
                    Dim strBrowser As String = Request.Browser.Browser

                    If (isViewer) Then
                        chkbox.OnClientClick = "return false"
                    Else
                        chkbox.OnClientClick = CommonProcedures.CheckboxClicked(2)
                    End If

                    If blnShowBudget = True Then
                        intPeriods = 11
                    Else
                        intPeriods = intCurrentMonth
                    End If

                    Dim selectedAccounts = (From a In utility.AccountRepository.GetAccountRecordsByAnalysisId(intAnalysisID)
                                           Join ac In utility.AccountTypeRepository.GetAccountTypeRecordsByAnalysisId(intAnalysisID, typeDescList)
                                           On a.AcctTypeId Equals ac.AcctTypeId Where a.AnalysisId = ac.AnalysisId
                                           Order By a.SortSequence
                                           Select a.AccountId, a.AcctTypeId, a.AnalysisId, a.AcctDescriptor, a.SortSequence, a.Description, a.Subgrouping, ac.TypeDesc, ac.ClassDesc
                                         ).ToList()

                    Logger.Log.Info(String.Format("REController:LoadSpread5 --> Balance Query execution Starts"))
                    Dim balanceAcc = (From b In utility.BalanceRepository.GetBalanceRecordsByAnalysisId(intAnalysisID)
                                    Join acc In selectedAccounts On b.AccountId Equals acc.AccountId
                                    Where acc.AnalysisId = b.AnalysisId
                                    Select b).ToList()
                    Logger.Log.Info(String.Format("REController:LoadSpread5 --> Balance Query execution Starts"))

                    If blnShowBudget = True Or blnShowasPercent = True Then
                        For Each account In selectedAccounts
                            'save amounts total revenue amounts for computing %    
                            Select Case True
                                Case account.AcctDescriptor = "Total" And account.TypeDesc = "Revenue"
                                    Dim accountId As Integer = account.AccountId
                                    For Each balance As Balance In (From b In balanceAcc.Where(Function(a) a.AccountId = accountId And a.AnalysisId = intAnalysisID) Select b)
                                        If blnShowBudget = True Then
                                            GetBudgetAmts(balance, decPeriodAmts)
                                        Else
                                            GetActualAmts(balance, decPeriodAmts)
                                        End If

                                        For intColumnCounter = 0 To intPeriods
                                            decAccumTotals(intColumnCounter) = decPeriodAmts(intColumnCounter)
                                        Next
                                        intRevLastActual = -1
                                        GetActualAmts(balance, decPeriodAmts)
                                        For intColumnCounter = intCurrentMonth To 0 Step -1
                                            If intRevLastActual = -1 Then
                                                If decPeriodAmts(intColumnCounter) <> 0 Then
                                                    If intRevLastActual = -1 Then
                                                        intRevLastActual = intColumnCounter
                                                    End If
                                                    decAccumTotals(intColumnCounter) = decPeriodAmts(intColumnCounter)

                                                End If
                                            Else
                                                decAccumTotals(intColumnCounter) = decPeriodAmts(intColumnCounter)
                                            End If
                                        Next
                                        For intColumnCounter = 0 To intPeriods
                                            decAccumTotals(intPeriods + 1) += decAccumTotals(intColumnCounter)
                                        Next
                                    Next
                                Case account.AcctDescriptor = "NITotal"
                                    If blnShowBudget = True Then
                                        Dim accountId As Integer = account.AccountId
                                        For Each balance As Balance In (From b In balanceAcc.Where(Function(a) a.AccountId = accountId And a.AnalysisId = intAnalysisID) Select b)
                                            GetBudgetAmts(balance, decPeriodAmts)
                                            For intColumnCounter = 0 To intPeriods
                                                decNITotals(intColumnCounter) = decPeriodAmts(intColumnCounter)
                                            Next
                                            intNILastActual = -1
                                            GetActualAmts(balance, decPeriodAmts)
                                            For intColumnCounter = intCurrentMonth To 0 Step -1
                                                If intNILastActual = -1 Then
                                                    If decPeriodAmts(intColumnCounter) <> 0 Then
                                                        intNILastActual = intColumnCounter
                                                        decNITotals(intColumnCounter) = decPeriodAmts(intColumnCounter)
                                                    End If
                                                Else
                                                    decNITotals(intColumnCounter) = decPeriodAmts(intColumnCounter)
                                                End If
                                            Next
                                            For intColumnCounter = 0 To intPeriods
                                                decNITotals(intPeriods + 1) = decNITotals(intColumnCounter)
                                            Next
                                        Next

                                    End If
                                    Exit For
                            End Select
                        Next
                    End If
                    If blnShowBudget = True Then
                        If intRevLastActual < intCurrentMonth Then
                            If intNILastActual < intRevLastActual Then
                                intCurrentMonth = intRevLastActual
                            Else
                                intCurrentMonth = intNILastActual
                            End If
                        ElseIf intNILastActual < intCurrentMonth Then
                            intCurrentMonth = intNILastActual
                        End If
                    End If
                    For Each account In selectedAccounts
                        CommonProcedures.AddRow(spdAnalytics, intRowCounter, account.Description, account.AcctDescriptor, account.TypeDesc, strBrowser, 4)
                        Select Case account.AcctDescriptor
                            Case "Heading"
                                CommonProcedures.FormatHeaderRow(spdAnalytics, intRowCounter, intSpanRows, blnTotalFlag, 4)
                            Case "SHeading"
                                FormatSHeaderRow(spdAnalytics, intRowCounter, intSpanRows, blnTotalFlag, 4)
                            Case "STotal"
                                FormatSTotalRow(spdAnalytics, intRowCounter, intFirstNumberColumn, strBrowser)
                            Case "Total"
                                CommonProcedures.FormatTotalRow(spdAnalytics, intRowCounter, intSpanRows, blnTotalFlag, intFirstNumberColumn, intNumberColumn, account.ClassDesc, strBrowser, 4)
                            Case "GPTotal", "IOTotal", "IBTTotal", "NITotal"
                                Array.Clear(intSpanRows, 0, 1)
                                CommonProcedures.FormatGrandTotalRow(spdAnalytics, intRowCounter, blnTotalFlag, intFirstNumberColumn, intNumberColumn, account.ClassDesc, strBrowser, 4)
                        End Select
                        'save accountID 
                        spdAnalytics.Sheets(0).SetValue(intRowCounter, 5, account.AccountId)
                        'save amounts                         
                        If account.AcctDescriptor <> "Heading" And account.AcctDescriptor <> "SHeading" Then
                            Array.Clear(decPeriodAmts, 0, 12)
                            Array.Clear(decSaveAmts, 0, 13)
                            Dim accountId As Integer = account.AccountId
                            spdAnalytics.Sheets(0).Cells(intRowCounter, 2).CellType = chkbox
                            For Each balance As Balance In (From b In balanceAcc.Where(Function(a) a.AccountId = accountId And a.AnalysisId = intAnalysisID) Select b)
                                'If blnShowBudget = True Then
                                '    GetBudgetAmts(balance, decPeriodAmts)
                                'Else
                                GetActualAmts(balance, decPeriodAmts)
                                'End If

                                If blnShowasPercent = False Then
                                    For intColumnCounter = 0 To intCurrentMonth
                                        spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumberColumn + intColumnCounter, decPeriodAmts(intColumnCounter))
                                        decSaveAmts(intColumnCounter) = decPeriodAmts(intColumnCounter)
                                    Next
                                Else
                                    For intColumnCounter = 0 To intCurrentMonth
                                        If decAccumTotals(intColumnCounter) <> 0 Then
                                            spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumberColumn + intColumnCounter, decPeriodAmts(intColumnCounter) / decAccumTotals(intColumnCounter))
                                            decSaveAmts(intColumnCounter) = decPeriodAmts(intColumnCounter) ' / decAccumTotals(intColumnCounter)
                                        Else
                                            spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumberColumn + intColumnCounter, 0)
                                            decSaveAmts(intColumnCounter) = 0
                                        End If
                                    Next
                                End If
                                If blnShowBudget = True Then
                                    GetBudgetAmts(balance, decPeriodAmts)
                                    For intColumnCounter = intCurrentMonth + 1 To intPeriods
                                        If blnShowasPercent = False Then
                                            spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumberColumn + intColumnCounter, decPeriodAmts(intColumnCounter))
                                            decSaveAmts(intColumnCounter) = decPeriodAmts(intColumnCounter)
                                            '    decSaveAmts(intPeriods + 1) += decPeriodAmts(intColumnCounter)
                                        Else
                                            If decAccumTotals(intColumnCounter) <> 0 Then
                                                spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumberColumn + intColumnCounter, decPeriodAmts(intColumnCounter) / decAccumTotals(intColumnCounter))
                                                decSaveAmts(intColumnCounter) = decPeriodAmts(intColumnCounter) ' / decAccumTotals(intColumnCounter)
                                            Else
                                                spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumberColumn + intColumnCounter, 0)
                                                decSaveAmts(intColumnCounter) = 0
                                            End If
                                            '    decSaveAmts(intPeriods + 1) += decPeriodAmts(intColumnCounter)
                                        End If
                                        spdAnalytics.Sheets(0).Cells(intRowCounter, intFirstNumberColumn + intColumnCounter).ForeColor = Drawing.Color.FromArgb(5, 51, 97)
                                    Next
                                End If
                                For intColumnCounter = 0 To intPeriods
                                    decSaveAmts(intPeriods + 1) += decSaveAmts(intColumnCounter)
                                Next
                                'save amounts for total column
                                If blnShowasPercent = False Then
                                    spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumberColumn + intPeriods + 1, decSaveAmts(intPeriods + 1))
                                Else
                                    If decAccumTotals(intPeriods + 1) <> 0 Then
                                        spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumberColumn + intPeriods + 1, decSaveAmts(intPeriods + 1) / decAccumTotals(intPeriods + 1))
                                    Else
                                        spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumberColumn + intPeriods + 1, 0)
                                    End If
                                End If
                                If intCurrentMonth < intPeriods Then
                                    spdAnalytics.Sheets(0).Cells(intRowCounter, intFirstNumberColumn + intPeriods + 1).ForeColor = Drawing.Color.FromArgb(5, 51, 97)
                                End If
                            Next
                        End If
                    Next
                End Using
                Logger.Log.Info(String.Format("REController:LoadSpread5 method execution Ends"))
            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("LoadSpread5 : Error occured while processing Revenue & Expense Chart - ", ex.Message)
                Logger.Log.Error(String.Format("REController:LoadSpread5 method execution ends with Error message: {0}, StackTrace: {1}", ex.Message, ex.StackTrace))
            End Try
        End Sub

        <CustomActionFilter()>
        Public Sub LoadSpread6(ByVal spdAnalytics As FpSpread, ByVal intFormat As Integer, ByVal intPeriod As Integer, ByVal intNumberColumn As Integer, ByVal intFirstNumberColumn As Integer, ByRef blnShowasPercent As Boolean, ByVal intColumnCount As Integer)
            Try
                Logger.Log.Info(String.Format("REController:LoadSpread6 method execution Starts"))

                Using utility As New Utility()
                    Dim intRowCounter As Integer = -1
                    Dim intColumnCounter As Integer
                    Dim strYearType(5) As String
                    Dim strPeriods(2) As String
                    Dim decAccumTotals(12) As Decimal
                    Dim decPeriodAmts(11) As Decimal
                    Dim decSaveAmts(12) As Decimal
                    Dim intSpanRows(1) As Integer
                    Dim blnTotalFlag As Boolean
                    Dim decIncAccumTotals(intNumberColumn) As Decimal
                    Dim chkbox As New FarPoint.Web.Spread.CheckBoxCellType
                    Dim intCurrentMonth As Integer = Val(DateTime.Today.ToString("MM")) - 2
                    Dim strBrowser As String = Request.Browser.Browser

                    If (isViewer) Then
                        chkbox.OnClientClick = "return false"
                    Else
                        chkbox.OnClientClick = CommonProcedures.CheckboxClicked(2)
                    End If

                    Dim selectedAccounts = (From a In utility.AccountRepository.GetAccountRecordsByAnalysisId(intAnalysisID)
                                           Join ac In utility.AccountTypeRepository.GetAccountTypeRecordsByAnalysisId(intAnalysisID, typeDescList)
                                           On a.AcctTypeId Equals ac.AcctTypeId Where a.AnalysisId = ac.AnalysisId
                                           Order By a.SortSequence
                                           Select a.AccountId, a.AcctTypeId, a.AnalysisId, a.AcctDescriptor, a.SortSequence, a.Description, a.Subgrouping, ac.TypeDesc, ac.ClassDesc
                                         ).ToList()

                    Dim balanceAcc = (From b In utility.BalanceRepository.GetBalanceRecordsByAnalysisId(intAnalysisID)
                                   Join acc In selectedAccounts On b.AccountId Equals acc.AccountId
                                   Where b.AnalysisId = acc.AnalysisId
                                   Select b).ToList()

                    If blnShowasPercent = True Then
                        For Each account In selectedAccounts
                            'save amounts total revenue amounts for computing %                      
                            If account.AcctDescriptor = "Total" And account.TypeDesc = "Revenue" Then
                                Dim accountId As Integer = account.AccountId
                                For Each balance As Balance In (From b In balanceAcc.Where(Function(a) a.AccountId = accountId And a.AnalysisId = intAnalysisID) Select b)
                                    GetH5Amts(balance, decPeriodAmts)
                                    If intPeriod < 11 Then
                                        For intColumnCounter = intPeriod + 1 To 11
                                            decAccumTotals(intColumnCounter) = decPeriodAmts(intColumnCounter)
                                            decAccumTotals(12) += decPeriodAmts(intColumnCounter)
                                        Next
                                    End If
                                    GetActualAmts(balance, decPeriodAmts)
                                    For intColumnCounter = 0 To intPeriod
                                        decAccumTotals(intColumnCounter) = decPeriodAmts(intColumnCounter)
                                        decAccumTotals(12) += decPeriodAmts(intColumnCounter)
                                    Next
                                Next
                                Exit For
                            End If
                        Next
                    End If

                    For Each account In selectedAccounts
                        CommonProcedures.AddRow(spdAnalytics, intRowCounter, account.Description, account.AcctDescriptor, account.TypeDesc, strBrowser, 4)
                        Select Case account.AcctDescriptor
                            Case "Heading"
                                CommonProcedures.FormatHeaderRow(spdAnalytics, intRowCounter, intSpanRows, blnTotalFlag, 4)
                            Case "SHeading"
                                FormatSHeaderRow(spdAnalytics, intRowCounter, intSpanRows, blnTotalFlag, 4)
                            Case "STotal"
                                FormatSTotalRow(spdAnalytics, intRowCounter, intFirstNumberColumn, strBrowser)
                            Case "Total"
                                CommonProcedures.FormatTotalRow(spdAnalytics, intRowCounter, intSpanRows, blnTotalFlag, intFirstNumberColumn, intNumberColumn, account.ClassDesc, strBrowser, 4)
                                'Case "Detail"
                                '    spdAnalytics.Sheets(0).Cells(intRowCounter, 0).Border.BorderColorBottom = Drawing.Color.White
                            Case "GPTotal", "IOTotal", "IBTTotal", "NITotal"
                                Array.Clear(intSpanRows, 0, 1)
                                CommonProcedures.FormatGrandTotalRow(spdAnalytics, intRowCounter, blnTotalFlag, intFirstNumberColumn, intNumberColumn, account.ClassDesc, strBrowser, 4)
                        End Select
                        'save accountID 
                        spdAnalytics.Sheets(0).SetValue(intRowCounter, 5, account.AccountId)
                        'save amounts
                        If account.AcctDescriptor <> "Heading" And account.AcctDescriptor <> "SHeading" Then
                            Array.Clear(decPeriodAmts, 0, 12)
                            Array.Clear(decSaveAmts, 0, 13)
                            Dim accountId As Integer = account.AccountId
                            spdAnalytics.Sheets(0).Cells(intRowCounter, 2).CellType = chkbox
                            For Each balance As Balance In (From b In balanceAcc.Where(Function(a) a.AccountId = accountId And a.AnalysisId = intAnalysisID) Select b)
                                If intPeriod < 11 Then
                                    GetH5Amts(balance, decPeriodAmts)
                                    For intColumnCounter = intPeriod + 1 To 11
                                        If blnShowasPercent = False Then
                                            spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumberColumn + intColumnCounter - (intPeriod + 1), decPeriodAmts(intColumnCounter))
                                            decSaveAmts(intColumnCounter) = decPeriodAmts(intColumnCounter)
                                            decSaveAmts(12) += decPeriodAmts(intColumnCounter)
                                        Else
                                            If decAccumTotals(intColumnCounter) <> 0 Then
                                                spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumberColumn + intColumnCounter - (intPeriod + 1), decPeriodAmts(intColumnCounter) / decAccumTotals(intColumnCounter))
                                                decSaveAmts(intColumnCounter) = decPeriodAmts(intColumnCounter) / decAccumTotals(intColumnCounter)
                                            Else
                                                spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumberColumn + intColumnCounter - (intPeriod + 1), 0)
                                                decSaveAmts(intColumnCounter) = 0
                                            End If
                                            decSaveAmts(12) += decPeriodAmts(intColumnCounter)
                                        End If
                                    Next
                                End If
                                GetActualAmts(balance, decPeriodAmts)
                                For intColumnCounter = 0 To intPeriod
                                    If blnShowasPercent = False Then
                                        spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumberColumn + intColumnCounter + (11 - intPeriod), decPeriodAmts(intColumnCounter))
                                        decSaveAmts(intColumnCounter) = decPeriodAmts(intColumnCounter)
                                        decSaveAmts(12) += decPeriodAmts(intColumnCounter)
                                    Else
                                        If decAccumTotals(intColumnCounter) <> 0 Then
                                            spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumberColumn + intColumnCounter + (11 - intPeriod), decPeriodAmts(intColumnCounter) / decAccumTotals(intColumnCounter))
                                            decSaveAmts(intColumnCounter) = decPeriodAmts(intColumnCounter) / decAccumTotals(intColumnCounter)
                                        Else
                                            spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumberColumn + intColumnCounter + (11 - intPeriod), 0)
                                            decSaveAmts(intColumnCounter) = 0
                                        End If
                                        decSaveAmts(12) += decPeriodAmts(intColumnCounter)
                                    End If
                                Next
                            Next
                            If blnShowasPercent = False Then
                                spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumberColumn + 12, decSaveAmts(12))
                            Else
                                If decAccumTotals(12) <> 0 Then
                                    spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumberColumn + 12, decSaveAmts(12) / decAccumTotals(12))
                                    decSaveAmts(12) = decSaveAmts(12) / decAccumTotals(12)
                                Else
                                    spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumberColumn + 12, 0)
                                    decSaveAmts(12) = 0
                                End If

                            End If
                        End If
                    Next

                End Using
                Logger.Log.Info(String.Format("REController:LoadSpread6 method execution Ends"))
            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("LoadSpread6 : Error occured while processing Revenue & Expense Chart - ", ex.Message)
                Logger.Log.Error(String.Format("REController:LoadSpread6 method execution ends with Error message: {0}, StackTrace: {1}", ex.Message, ex.StackTrace))

            End Try


        End Sub

        <CustomActionFilter()>
        Public Sub LoadSpread7to8(ByVal spdAnalytics As FpSpread, ByVal intFormat As Integer, ByVal intPeriod As Integer, ByVal intNumberColumn As Integer, ByVal intFirstNumberColumn As Integer, ByRef blnShowasPercent As Boolean, ByVal intColumnCount As Integer)
            Try
                Logger.Log.Info(String.Format("REController:LoadSpread7to8 method execution Starts"))
                Using utility As New Utility

                    Dim intRowCounter As Integer = -1
                    Dim intColumnCounter As Integer
                    Dim intTypeCol As Integer
                    Dim strYearType(5) As String
                    Dim strPeriods(2) As String
                    Dim decPeriodAmts(11) As Decimal
                    Dim decAccumTotals(5) As Decimal
                    Dim decSaveAmts(12) As Decimal
                    Dim intSpanRows(1) As Integer
                    Dim blnTotalFlag As Boolean
                    Dim decIncAccumTotals(intNumberColumn) As Decimal
                    Dim chkbox As New FarPoint.Web.Spread.CheckBoxCellType
                    Dim strBrowser As String = Request.Browser.Browser

                    If (isViewer) Then
                        chkbox.OnClientClick = "return false"
                    Else
                        chkbox.OnClientClick = CommonProcedures.CheckboxClicked(2)
                    End If

                    Dim selectedAccounts = (From a In utility.AccountRepository.GetAccountRecordsByAnalysisId(intAnalysisID)
                                           Join ac In utility.AccountTypeRepository.GetAccountTypeRecordsByAnalysisId(intAnalysisID, typeDescList)
                                           On a.AcctTypeId Equals ac.AcctTypeId Where a.AnalysisId = ac.AnalysisId
                                           Order By a.SortSequence
                                           Select a.AccountId, a.AcctTypeId, a.AnalysisId, a.AcctDescriptor, a.SortSequence, a.Description, a.Subgrouping, ac.TypeDesc, ac.ClassDesc
                                         ).ToList()

                    Dim balanceAcc = (From b In utility.BalanceRepository.GetBalanceRecordsByAnalysisId(intAnalysisID)
                                   Join acc In selectedAccounts On b.AccountId Equals acc.AccountId
                                   Where b.AnalysisId = acc.AnalysisId
                                   Select b).ToList()

                    If blnShowasPercent = True Then
                        For Each account In selectedAccounts
                            'save amounts total revenue amounts for computing %                      
                            If account.AcctDescriptor = "Total" And account.TypeDesc = "Revenue" Then
                                Dim accountId As Integer = account.AccountId
                                For Each balance As Balance In (From b In balanceAcc.Where(Function(a) a.AccountId = accountId And a.AnalysisId = intAnalysisID) Select b)
                                    GetH1Amts(balance, decPeriodAmts)
                                    CommonProcedures.AccumRunningTotals(intFormat, decAccumTotals, intPeriod, decPeriodAmts, 0)
                                    GetH2Amts(balance, decPeriodAmts)
                                    CommonProcedures.AccumRunningTotals(intFormat, decAccumTotals, intPeriod, decPeriodAmts, 1)
                                    GetH3Amts(balance, decPeriodAmts)
                                    CommonProcedures.AccumRunningTotals(intFormat, decAccumTotals, intPeriod, decPeriodAmts, 2)
                                    GetH4Amts(balance, decPeriodAmts)
                                    CommonProcedures.AccumRunningTotals(intFormat, decAccumTotals, intPeriod, decPeriodAmts, 3)
                                    GetH5Amts(balance, decPeriodAmts)
                                    CommonProcedures.AccumRunningTotals(intFormat, decAccumTotals, intPeriod, decPeriodAmts, 4)
                                    GetActualAmts(balance, decPeriodAmts)
                                    CommonProcedures.AccumRunningTotals(intFormat, decAccumTotals, intPeriod, decPeriodAmts, 5)
                                Next
                                Exit For
                            End If
                        Next
                    End If

                    For Each account In selectedAccounts

                        'Dim accountTypeId As Integer = account.AcctTypeId
                        'Dim acctType = (From a In utility.AccountTypeRepository.GetAccountTypes.Where(Function(at) at.AcctTypeId = accountTypeId) Select a).FirstOrDefault()

                        CommonProcedures.AddRow(spdAnalytics, intRowCounter, account.Description, account.AcctDescriptor, account.TypeDesc, strBrowser, 4)
                        Select Case account.AcctDescriptor
                            Case "Heading"
                                CommonProcedures.FormatHeaderRow(spdAnalytics, intRowCounter, intSpanRows, blnTotalFlag, 4)
                            Case "SHeading"
                                FormatSHeaderRow(spdAnalytics, intRowCounter, intSpanRows, blnTotalFlag, 4)
                            Case "STotal"
                                FormatSTotalRow(spdAnalytics, intRowCounter, intFirstNumberColumn, strBrowser)
                            Case "Total"
                                CommonProcedures.FormatTotalRow(spdAnalytics, intRowCounter, intSpanRows, blnTotalFlag, intFirstNumberColumn, intNumberColumn, account.ClassDesc, strBrowser, 4)
                                'Case "Detail"
                                '    spdAnalytics.Sheets(0).Cells(intRowCounter, 0).Border.BorderColorBottom = Drawing.Color.White
                            Case "GPTotal", "IOTotal", "IBTTotal", "NITotal"
                                Array.Clear(intSpanRows, 0, 1)
                                CommonProcedures.FormatGrandTotalRow(spdAnalytics, intRowCounter, blnTotalFlag, intFirstNumberColumn, intNumberColumn, account.ClassDesc, strBrowser, 4)
                        End Select
                        'save accountID 
                        spdAnalytics.Sheets(0).SetValue(intRowCounter, 5, account.AccountId)
                        'save amounts 
                        If account.AcctDescriptor <> "Heading" And account.AcctDescriptor <> "SHeading" Then
                            Array.Clear(decPeriodAmts, 0, 12)
                            Array.Clear(decSaveAmts, 0, 5)
                            Dim accountId As Integer = account.AccountId
                            spdAnalytics.Sheets(0).Cells(intRowCounter, 2).CellType = chkbox
                            For Each balance As Balance In (From b In balanceAcc.Where(Function(a) a.AccountId = accountId And a.AnalysisId = intAnalysisID) Select b)
                                For intTypeCol = 0 To 5
                                    Select Case intTypeCol
                                        Case 0
                                            GetH1Amts(balance, decPeriodAmts)
                                        Case 1
                                            GetH2Amts(balance, decPeriodAmts)
                                        Case 2
                                            GetH3Amts(balance, decPeriodAmts)
                                        Case 3
                                            GetH4Amts(balance, decPeriodAmts)
                                        Case 4
                                            GetH5Amts(balance, decPeriodAmts)
                                        Case 5
                                            GetActualAmts(balance, decPeriodAmts)
                                    End Select
                                    Select Case intFormat
                                        Case 7
                                            If blnShowasPercent = False Then
                                                spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumberColumn + intTypeCol, decPeriodAmts(intPeriod))
                                                decSaveAmts(intTypeCol) = decPeriodAmts(intPeriod)
                                            Else
                                                If decAccumTotals(intTypeCol) <> 0 Then
                                                    spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumberColumn + intTypeCol, decPeriodAmts(intPeriod) / decAccumTotals(intTypeCol))
                                                    decSaveAmts(intTypeCol) = decPeriodAmts(intPeriod) / decAccumTotals(intTypeCol)
                                                Else
                                                    spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumberColumn + intTypeCol, 0)
                                                    decSaveAmts(intTypeCol) = 0
                                                End If
                                            End If
                                        Case 8
                                            decSaveAmts(intTypeCol) = 0
                                            For intColumnCounter = 0 To intPeriod
                                                decSaveAmts(intTypeCol) += decPeriodAmts(intColumnCounter)
                                            Next
                                            If blnShowasPercent = False Then
                                                spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumberColumn + intTypeCol, decSaveAmts(intTypeCol))
                                            Else
                                                If decAccumTotals(intTypeCol) <> 0 Then
                                                    spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumberColumn + intTypeCol, decSaveAmts(intTypeCol) / decAccumTotals(intTypeCol))
                                                    decSaveAmts(intTypeCol) = decSaveAmts(intTypeCol) / decAccumTotals(intTypeCol)
                                                Else
                                                    spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumberColumn + intTypeCol, 0)
                                                    decSaveAmts(intTypeCol) = 0
                                                End If
                                            End If
                                    End Select
                                Next
                            Next
                        End If
                    Next
                End Using
                Logger.Log.Info(String.Format("REController:LoadSpread7to8 method execution Ends"))
            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("LoadSpread7to8 : Error occured while processing Revenue & Expense Chart - ", ex.Message)
                Logger.Log.Error(String.Format("REController:LoadSpread7to8 method execution ends with Error message: {0}, StackTrace: {1}", ex.Message, ex.StackTrace))
            End Try


        End Sub

        <CustomActionFilter()>
        Public Sub LoadSpread9(ByVal spdAnalytics As FpSpread, ByVal intFormat As Integer, ByVal intPeriod As Integer, ByVal intNumberColumn As Integer, ByVal intFirstNumberColumn As Integer, ByRef blnShowasPercent As Boolean, ByVal intColumnCount As Integer)
            Try
                Logger.Log.Info(String.Format("REController:LoadSpread6 method execution Starts"))

                Using utility As New Utility()
                    Dim intRowCounter As Integer = -1
                    Dim intColumnCounter As Integer
                    Dim strYearType(5) As String
                    Dim strPeriods(2) As String
                    Dim decAccumTotals(12) As Decimal
                    Dim decPeriodAmts(11) As Decimal
                    Dim decSaveAmts(12) As Decimal
                    Dim intSpanRows(1) As Integer
                    Dim blnTotalFlag As Boolean
                    Dim decIncAccumTotals(intNumberColumn) As Decimal
                    Dim chkbox As New FarPoint.Web.Spread.CheckBoxCellType
                    Dim intCurrentMonth As Integer = Val(DateTime.Today.ToString("MM")) - 2
                    Dim strBrowser As String = Request.Browser.Browser

                    If (isViewer) Then
                        chkbox.OnClientClick = "return false"
                    Else
                        chkbox.OnClientClick = CommonProcedures.CheckboxClicked(2)
                    End If

                    Dim selectedAccounts = (From a In utility.AccountRepository.GetAccountRecordsByAnalysisId(intAnalysisID)
                                           Join ac In utility.AccountTypeRepository.GetAccountTypeRecordsByAnalysisId(intAnalysisID, typeDescList)
                                           On a.AcctTypeId Equals ac.AcctTypeId Where a.AnalysisId = ac.AnalysisId
                                           Order By a.SortSequence
                                           Select a.AccountId, a.AcctTypeId, a.AnalysisId, a.AcctDescriptor, a.SortSequence, a.Description, a.Subgrouping, ac.TypeDesc, ac.ClassDesc
                                         ).ToList()

                    Dim balanceAcc = (From b In utility.BalanceRepository.GetBalanceRecordsByAnalysisId(intAnalysisID)
                                   Join acc In selectedAccounts On b.AccountId Equals acc.AccountId
                                   Where b.AnalysisId = acc.AnalysisId
                                   Select b).ToList()

                    If blnShowasPercent = True Then
                        For Each account In selectedAccounts
                            'save amounts total revenue amounts for computing %                      
                            If account.AcctDescriptor = "Total" And account.TypeDesc = "Revenue" Then
                                Dim accountId As Integer = account.AccountId
                                For Each balance As Balance In (From b In balanceAcc.Where(Function(a) a.AccountId = accountId And a.AnalysisId = intAnalysisID) Select b)
                                    GetBudgetAmts(balance, decPeriodAmts)
                                    If intPeriod < 11 Then
                                        For intColumnCounter = intPeriod + 1 To 11
                                            decAccumTotals(intColumnCounter) = decPeriodAmts(intColumnCounter)
                                            decAccumTotals(12) += decPeriodAmts(intColumnCounter)
                                        Next
                                    End If
                                    GetBudget2Amts(balance, decPeriodAmts)
                                    For intColumnCounter = 0 To intPeriod
                                        decAccumTotals(intColumnCounter) = decPeriodAmts(intColumnCounter)
                                        decAccumTotals(12) += decPeriodAmts(intColumnCounter)
                                    Next
                                Next
                                Exit For
                            End If
                        Next
                    End If

                    For Each account In selectedAccounts
                        CommonProcedures.AddRow(spdAnalytics, intRowCounter, account.Description, account.AcctDescriptor, account.TypeDesc, strBrowser, 4)
                        Select Case account.AcctDescriptor
                            Case "Heading"
                                CommonProcedures.FormatHeaderRow(spdAnalytics, intRowCounter, intSpanRows, blnTotalFlag, 4)
                            Case "SHeading"
                                FormatSHeaderRow(spdAnalytics, intRowCounter, intSpanRows, blnTotalFlag, 4)
                            Case "STotal"
                                FormatSTotalRow(spdAnalytics, intRowCounter, intFirstNumberColumn, strBrowser)
                            Case "Total"
                                CommonProcedures.FormatTotalRow(spdAnalytics, intRowCounter, intSpanRows, blnTotalFlag, intFirstNumberColumn, intNumberColumn, account.ClassDesc, strBrowser, 4)
                                'Case "Detail"
                                '    spdAnalytics.Sheets(0).Cells(intRowCounter, 0).Border.BorderColorBottom = Drawing.Color.White
                            Case "GPTotal", "IOTotal", "IBTTotal", "NITotal"
                                Array.Clear(intSpanRows, 0, 1)
                                CommonProcedures.FormatGrandTotalRow(spdAnalytics, intRowCounter, blnTotalFlag, intFirstNumberColumn, intNumberColumn, account.ClassDesc, strBrowser, 4)
                        End Select
                        'save accountID 
                        spdAnalytics.Sheets(0).SetValue(intRowCounter, 5, account.AccountId)
                        'save amounts
                        If account.AcctDescriptor <> "Heading" And account.AcctDescriptor <> "SHeading" Then
                            Array.Clear(decPeriodAmts, 0, 12)
                            Array.Clear(decSaveAmts, 0, 13)
                            Dim accountId As Integer = account.AccountId
                            spdAnalytics.Sheets(0).Cells(intRowCounter, 2).CellType = chkbox
                            For Each balance As Balance In (From b In balanceAcc.Where(Function(a) a.AccountId = accountId And a.AnalysisId = intAnalysisID) Select b)
                                If intPeriod < 11 Then
                                    GetBudgetAmts(balance, decPeriodAmts)
                                    For intColumnCounter = intPeriod + 1 To 11
                                        If blnShowasPercent = False Then
                                            spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumberColumn + intColumnCounter - (intPeriod + 1), decPeriodAmts(intColumnCounter))
                                            decSaveAmts(intColumnCounter) = decPeriodAmts(intColumnCounter)
                                            decSaveAmts(12) += decPeriodAmts(intColumnCounter)
                                        Else
                                            If decAccumTotals(intColumnCounter) <> 0 Then
                                                spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumberColumn + intColumnCounter - (intPeriod + 1), decPeriodAmts(intColumnCounter) / decAccumTotals(intColumnCounter))
                                                decSaveAmts(intColumnCounter) = decPeriodAmts(intColumnCounter) / decAccumTotals(intColumnCounter)
                                            Else
                                                spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumberColumn + intColumnCounter - (intPeriod + 1), 0)
                                                decSaveAmts(intColumnCounter) = 0
                                            End If
                                            decSaveAmts(12) += decPeriodAmts(intColumnCounter)
                                        End If
                                    Next
                                End If
                                GetBudget2Amts(balance, decPeriodAmts)
                                For intColumnCounter = 0 To intPeriod
                                    If blnShowasPercent = False Then
                                        spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumberColumn + intColumnCounter + (11 - intPeriod), decPeriodAmts(intColumnCounter))
                                        decSaveAmts(intColumnCounter) = decPeriodAmts(intColumnCounter)
                                        decSaveAmts(12) += decPeriodAmts(intColumnCounter)
                                    Else
                                        If decAccumTotals(intColumnCounter) <> 0 Then
                                            spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumberColumn + intColumnCounter + (11 - intPeriod), decPeriodAmts(intColumnCounter) / decAccumTotals(intColumnCounter))
                                            decSaveAmts(intColumnCounter) = decPeriodAmts(intColumnCounter) / decAccumTotals(intColumnCounter)
                                        Else
                                            spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumberColumn + intColumnCounter + (11 - intPeriod), 0)
                                            decSaveAmts(intColumnCounter) = 0
                                        End If
                                        decSaveAmts(12) += decPeriodAmts(intColumnCounter)
                                    End If
                                Next
                            Next
                            If blnShowasPercent = False Then
                                spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumberColumn + 12, decSaveAmts(12))
                            Else
                                If decAccumTotals(12) <> 0 Then
                                    spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumberColumn + 12, decSaveAmts(12) / decAccumTotals(12))
                                    decSaveAmts(12) = decSaveAmts(12) / decAccumTotals(12)
                                Else
                                    spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumberColumn + 12, 0)
                                    decSaveAmts(12) = 0
                                End If
                            End If
                        End If
                    Next

                End Using
                Logger.Log.Info(String.Format("REController:LoadSpread6 method execution Ends"))
            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("LoadSpread6 : Error occured while processing Revenue & Expense Chart - ", ex.Message)
                Logger.Log.Error(String.Format("REController:LoadSpread6 method execution ends with Error message: {0}, StackTrace: {1}", ex.Message, ex.StackTrace))
            End Try

        End Sub
        <CustomActionFilter()>
        Public Sub LoadSpread10(ByVal spdAnalytics As FpSpread, ByVal intPeriod As Integer, ByVal intNumberColumn As Integer, ByVal intFirstNumberColumn As Integer, ByRef blnShowasPercent As Boolean, ByVal intColumnCount As Integer, intHPeriod As Integer)
            Try
                Logger.Log.Info(String.Format("REController:LoadSpread10 method execution Starts"))

                Using utility As New Utility()
                    Dim intRowCounter As Integer = -1
                    Dim intColumnCounter As Integer
                    Dim strYearType(5) As String
                    Dim strPeriods(2) As String
                    Dim decAccumTotals(12) As Decimal
                    Dim decPeriodAmts(11) As Decimal
                    Dim decSaveAmts(12) As Decimal
                    Dim intSpanRows(1) As Integer
                    Dim blnTotalFlag As Boolean
                    Dim decIncAccumTotals(intNumberColumn) As Decimal
                    Dim chkbox As New FarPoint.Web.Spread.CheckBoxCellType
                    Dim intCurrentMonth As Integer = Val(DateTime.Today.ToString("MM")) - 2
                    Dim strBrowser As String = Request.Browser.Browser

                    If (isViewer) Then
                        chkbox.OnClientClick = "return false"
                    Else
                        chkbox.OnClientClick = CommonProcedures.CheckboxClicked(2)
                    End If

                    Dim selectedAccounts = (From a In utility.AccountRepository.GetAccountRecordsByAnalysisId(intAnalysisID)
                                           Join ac In utility.AccountTypeRepository.GetAccountTypeRecordsByAnalysisId(intAnalysisID, typeDescList)
                                           On a.AcctTypeId Equals ac.AcctTypeId Where a.AnalysisId = ac.AnalysisId
                                           Order By a.SortSequence
                                           Select a.AccountId, a.AcctTypeId, a.AnalysisId, a.AcctDescriptor, a.SortSequence, a.Description, a.Subgrouping, ac.TypeDesc, ac.ClassDesc
                                         ).ToList()

                    Dim balanceAcc = (From b In utility.BalanceRepository.GetBalanceRecordsByAnalysisId(intAnalysisID)
                                   Join acc In selectedAccounts On b.AccountId Equals acc.AccountId
                                   Where b.AnalysisId = acc.AnalysisId
                                   Select b).ToList()

                    If blnShowasPercent = True Then
                        For Each account In selectedAccounts
                            'save amounts total revenue amounts for computing %                      
                            If account.AcctDescriptor = "Total" And account.TypeDesc = "Revenue" Then
                                Dim accountId As Integer = account.AccountId
                                For Each balance As Balance In (From b In balanceAcc.Where(Function(a) a.AccountId = accountId And a.AnalysisId = intAnalysisID) Select b)
                                    Select Case intHPeriod
                                        Case 5
                                            GetH1Amts(balance, decPeriodAmts)
                                        Case 4
                                            GetH2Amts(balance, decPeriodAmts)
                                        Case 3
                                            GetH3Amts(balance, decPeriodAmts)
                                        Case 2
                                            GetH4Amts(balance, decPeriodAmts)
                                        Case 1
                                            GetH5Amts(balance, decPeriodAmts)
                                        Case 0
                                            If blnArchBudget = False Then
                                                GetBudgetAmts(balance, decPeriodAmts)
                                            Else
                                                GetArchivedBudgetAmts(balance, decPeriodAmts)
                                            End If
                                    End Select
                                    For intColumnCounter = 0 To intPeriod
                                        decAccumTotals(intColumnCounter) = decPeriodAmts(intColumnCounter)
                                        decAccumTotals(12) += decPeriodAmts(intColumnCounter)
                                    Next
                                Next
                                Exit For
                            End If
                        Next
                    End If
                    For Each account In selectedAccounts
                        CommonProcedures.AddRow(spdAnalytics, intRowCounter, account.Description, account.AcctDescriptor, account.TypeDesc, strBrowser, 4)
                        Select Case account.AcctDescriptor
                            Case "Heading"
                                CommonProcedures.FormatHeaderRow(spdAnalytics, intRowCounter, intSpanRows, blnTotalFlag, 4)
                            Case "SHeading"
                                FormatSHeaderRow(spdAnalytics, intRowCounter, intSpanRows, blnTotalFlag, 4)
                            Case "STotal"
                                FormatSTotalRow(spdAnalytics, intRowCounter, intFirstNumberColumn, strBrowser)
                            Case "Total"
                                CommonProcedures.FormatTotalRow(spdAnalytics, intRowCounter, intSpanRows, blnTotalFlag, intFirstNumberColumn, intNumberColumn, account.ClassDesc, strBrowser, 4)

                            Case "GPTotal", "IOTotal", "IBTTotal", "NITotal"
                                Array.Clear(intSpanRows, 0, 1)
                                CommonProcedures.FormatGrandTotalRow(spdAnalytics, intRowCounter, blnTotalFlag, intFirstNumberColumn, intNumberColumn, account.ClassDesc, strBrowser, 4)
                        End Select
                        'save accountID 
                        spdAnalytics.Sheets(0).SetValue(intRowCounter, 5, account.AccountId)
                        'save amounts
                        If account.AcctDescriptor <> "Heading" And account.AcctDescriptor <> "SHeading" Then
                            Array.Clear(decPeriodAmts, 0, 12)
                            Array.Clear(decSaveAmts, 0, 13)
                            Dim accountId As Integer = account.AccountId
                            spdAnalytics.Sheets(0).Cells(intRowCounter, 2).CellType = chkbox
                            For Each balance As Balance In (From b In balanceAcc.Where(Function(a) a.AccountId = accountId And a.AnalysisId = intAnalysisID) Select b)
                                'If intPeriod < 11 Then
                                Select Case intHPeriod
                                    Case 5
                                        GetH1Amts(balance, decPeriodAmts)
                                    Case 4
                                        GetH2Amts(balance, decPeriodAmts)
                                    Case 3
                                        GetH3Amts(balance, decPeriodAmts)
                                    Case 2
                                        GetH4Amts(balance, decPeriodAmts)
                                    Case 1
                                        GetH5Amts(balance, decPeriodAmts)
                                    Case 0
                                        If blnArchBudget = False Then
                                            GetBudgetAmts(balance, decPeriodAmts)
                                        Else
                                            GetArchivedBudgetAmts(balance, decPeriodAmts)
                                        End If

                                End Select
                                For intColumnCounter = 0 To 11
                                    If blnShowasPercent = False Then
                                        spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumberColumn + intColumnCounter, decPeriodAmts(intColumnCounter))
                                        decSaveAmts(intColumnCounter) = decPeriodAmts(intColumnCounter)
                                        decSaveAmts(12) += decPeriodAmts(intColumnCounter)
                                    Else
                                        If decAccumTotals(intColumnCounter) <> 0 Then
                                            spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumberColumn + intColumnCounter, decPeriodAmts(intColumnCounter) / decAccumTotals(intColumnCounter))
                                            decSaveAmts(intColumnCounter) = decPeriodAmts(intColumnCounter) / decAccumTotals(intColumnCounter)
                                        Else
                                            spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumberColumn + intColumnCounter, 0)
                                            decSaveAmts(intColumnCounter) = 0
                                        End If
                                        decSaveAmts(12) += decPeriodAmts(intColumnCounter)
                                    End If
                                Next
                                'End If

                            Next
                            If blnShowasPercent = False Then
                                spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumberColumn + 12, decSaveAmts(12))
                            Else
                                If decAccumTotals(12) <> 0 Then
                                    spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumberColumn + 12, decSaveAmts(12) / decAccumTotals(12))
                                    decSaveAmts(12) = decSaveAmts(12) / decAccumTotals(12)
                                Else
                                    spdAnalytics.Sheets(0).SetValue(intRowCounter, intFirstNumberColumn + 12, 0)
                                    decSaveAmts(12) = 0
                                End If

                            End If
                        End If
                    Next

                End Using
                Logger.Log.Info(String.Format("REController:LoadSpread10 method execution Ends"))
            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("LoadSpread10 : Error occured while processing Revenue & Expense Chart - ", ex.Message)
                Logger.Log.Error(String.Format("REController:LoadSpread10 method execution ends with Error message: {0}, StackTrace: {1}", ex.Message, ex.StackTrace))

            End Try
        End Sub
        <HttpPost()>
        <CustomActionFilter()>
        Function AddViews(viewname As String, accountid As String, ByVal formValues As FormValues) As ActionResult
            Try

                Dim selectedAnalysis As Integer
                Logger.Log.Info(String.Format("REController:AddViews method execution Starts"))
                If (Not Session("UserType") Is Nothing) And (Not Session("UserInfo") Is Nothing) Then
                    UserInfo = DirectCast(Session("UserInfo"), User)
                Else
                    Return RedirectToAction("Index", "Home")
                End If

                If (UserInfo.AnalysisId > 0) Then
                    selectedAnalysis = UserInfo.AnalysisId
                Else
                    If (Not Session("SelectedAnalysisFromDropdown") Is Nothing) Then
                        selectedAnalysis = Session("SelectedAnalysisFromDropdown")
                    Else
                        selectedAnalysis = -1
                    End If
                End If
                Dim controller As String = "RE"
                If formValues.intFormat = 7 Then
                    formValues.blnShowBudget = True
                End If
                formValues.intFormat = CommonProcedures.FormatIDtoSave(formValues.intFormat)
                Dim result As Integer = Utility.SaveViews(viewname, controller, accountid, selectedAnalysis, formValues)
                Logger.Log.Info(String.Format("REController:AddViews method execution Ends"))
                Return RedirectToAction("Index")
            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Add View : Error occured while processing Revenue & Expenses - ", ex.Message)
                Logger.Log.Error(String.Format("REController:AddViews method execution ends with Error message: {0}, StackTrace: {1}", ex.Message, ex.StackTrace))
                Return RedirectToAction("Index")
            End Try

        End Function

        'Private Sub AccumNI(ByRef spdAnalytics As FpSpread, ByVal strClassDesc As String, ByVal intFormat As Integer, ByVal intNumberColumn As Integer, ByVal decSaveAmts() As Decimal, ByVal intColumnCount As Integer, ByRef decIncAccumTotals() As Decimal, ByRef intRowCounter As Integer, ByVal intPeriod As Integer)
        '    Try
        '        Logger.Log.Info(String.Format("REController:AccumNI method execution Starts"))
        '        Select Case strClassDesc
        '            Case "Revenue"
        '                For intColCtr = 0 To intNumberColumn - 1
        '                    decIncAccumTotals(intColCtr) = decSaveAmts(intColCtr)
        '                Next
        '            Case "Cost of sales"
        '                For intColCtr = 0 To intNumberColumn - 1
        '                    decIncAccumTotals(intColCtr) -= decSaveAmts(intColCtr)
        '                Next
        '                CommonProcedures.AddTotalRow(spdAnalytics, intFormat, intNumberColumn, intFirstNumberColumn, intColumnCount, decIncAccumTotals, intRowCounter, "GP", "Gross profit", intPeriod, 4)
        '            Case Else
        '                For intColCtr = 0 To intNumberColumn - 1
        '                    decIncAccumTotals(intColCtr) -= decSaveAmts(intColCtr)
        '                Next
        '        End Select
        '        Logger.Log.Info(String.Format("REController:AccumNI method execution Ends"))
        '    Catch ex As Exception
        '        TempData("ErrorMessage") = String.Concat("AccumNI : Error occured while processing Revenue & Expense Chart - ", ex.Message)
        '        Logger.Log.Error(String.Format("REController:AccumNI method execution ends with Error message: {0}, StackTrace: {1}", ex.Message, ex.StackTrace))
        '    End Try

        'End Sub

        <HttpGet()>
        <CustomActionFilter()>
        Function Viewer(Id As Integer, <MvcSpread("spdAnalytics")> ByVal spdAnalytics As FpSpread, ByVal formValues As FormValues) As ActionResult
            isViewer = True
            Dim savedView As SaveView = saveViewRepository.GetSavedViewDetail(Id)
            Dim setupCountObj As SetupCount = Nothing
            Dim selectedAnalysis As Integer = savedView.analysisID
            Try
                Logger.Log.Info(String.Format("REController Viewer (HttpGet) method execution starts"))
                blnArchBudget = DirectCast(Session("BudgetArch"), Boolean)
                If (Not Session("UserType") Is Nothing) And (Not Session("UserInfo") Is Nothing) Then
                    UserInfo = DirectCast(Session("UserInfo"), User)
                    sPeriod = Session("sPeriod")
                    sFormat = Session("sFormat")

                    If (UserInfo.AnalysisId = 0) Then
                        Return RedirectToAction("Index", "Dashboard")
                    Else
                        intAnalysisID = savedView.analysisID 'UserInfo.AnalysisId
                        intFirstFiscal = UserInfo.fiscalMonthOfSelectedCompany
                    End If
                End If

                LoadSetupCount(setupCountObj, UserInfo, selectedAnalysis)

                Dim intCtr As Integer
                Dim periods As List(Of SelectListItem) = New List(Of SelectListItem)
                intNumberofPeriods = NumberofPeriods(sPeriod, intFirstFiscal)
                For intCtr = 0 To 11
                    periods.Add(New SelectListItem With {.Text = CommonProcedures.GetMonth(intCtr, intFirstFiscal), .Value = intCtr + 1, .Selected = False})
                Next
                Select Case savedView.format
                    Case 13
                        savedView.format = 7
                End Select
                If savedView.format = 3 Then
                    Dim filterOn As List(Of SelectListItem) = New List(Of SelectListItem) From {
                        New SelectListItem With {.Text = "Current period amount", .Value = 1, .Selected = False},
                        New SelectListItem With {.Text = "Variance amount", .Value = 2, .Selected = False},
                        New SelectListItem With {.Text = "Percent variance", .Value = 3, .Selected = True}
                    }
                    ViewData("FilterOn") = New SelectList(filterOn, "value", "text", savedView.filteron)

                Else
                    Dim filterOn As List(Of SelectListItem) = New List(Of SelectListItem) From {
                        New SelectListItem With {.Text = "Current period amount", .Value = 1, .Selected = False},
                        New SelectListItem With {.Text = "Variance amount", .Value = 2, .Selected = False},
                        New SelectListItem With {.Text = "Percent variance", .Value = 3, .Selected = True},
                        New SelectListItem With {.Text = "YTD amount", .Value = 4, .Selected = False},
                        New SelectListItem With {.Text = "YTD Variance amount", .Value = 5, .Selected = False},
                        New SelectListItem With {.Text = "YTD Percent variance", .Value = 6, .Selected = True}
                    }
                    ViewData("FilterOn") = New SelectList(filterOn, "value", "text", savedView.filteron)
                End If


                ViewData("Periods") = New SelectList(periods, "value", "text", "May")


                ViewData("blnShowPercent") = savedView.showaspercent
                ViewData("intPeriod") = intNumberofPeriods + 1
                ViewData("intFormat") = savedView.format + 1
                ViewData("intChartType") = savedView.charttype
                ViewData("blnShowTrend") = savedView.trend
                ViewData("blnShowBudget") = savedView.showbudget
                ViewData("blnHighVar") = savedView.highlightva
                ViewData("blnPosVar") = savedView.posvar
                ViewData("intPosVarAmt") = Math.Round(CDec(savedView.posvaramt), 0)
                ViewData("blnNegVar") = savedView.negvar
                ViewData("intNegVarAmt") = Math.Round(CDec(savedView.negvaramt), 0)
                ViewData("blnUseFilter") = savedView.filter
                If savedView.filteron > 99 Then
                    ViewData("intFilterOn") = savedView.filteron - 100
                    ViewData("blnFilterPosVar") = False
                    ViewData("blnFilterNegVar") = True
                Else
                    ViewData("intFilterOn") = savedView.filteron
                    ViewData("blnFilterPosVar") = True
                    ViewData("blnFilterNegVar") = False
                End If
                ViewData("intFilterAmt") = Math.Round(CDec(savedView.filteramt), 0)
                'ViewData("blnFilterOnSelected") = savedView.filter
                ViewData("intDept") = 1
                ViewData("Postback") = False
                Session("intPeriod") = intNumberofPeriods + 1
                Dim accID As String = String.Empty
                Dim array() As Nullable(Of Integer) = {savedView.account1, savedView.account2, savedView.account3, savedView.account4, savedView.account5, savedView.account6, savedView.account7, savedView.account8, savedView.account9, savedView.account10}
                For AcId As Nullable(Of Integer) = 0 To array.Length - 1
                    If (Not IsNothing(array(AcId))) Then
                        accID = accID & array(AcId).ToString() & " "
                    End If
                Next
                accID = accID.TrimEnd(" ")
                ViewData("AccountID") = accID
                Logger.Log.Info(String.Format("REController Viewer (HttpGet) method execution Ends"))
            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Error occured while loading Revenue & Expense Chart Viewer-", ex.Message)
                Logger.Log.Error(String.Format("REController Viewer (HttpGet) method execution ends with Error message: {0}, StackTrace: {1}", ex.Message, ex.StackTrace))

                LoadSetupCount(setupCountObj, UserInfo, selectedAnalysis)

            End Try
            Return View("Viewer")
        End Function

        <HttpPost()> _
        <CustomActionFilter()>
        Function Viewer(Id As Integer, <MvcSpread("spdAnalytics")> ByVal spdAnalytics As FpSpread, <MvcSpread("spdChart")> ByVal spdChart As FpSpread, ByVal formValues As FormValues) As ActionResult
            isViewer = True
            Dim savedView As SaveView = saveViewRepository.GetSavedViewDetail(Id)
            Dim setupCountObj As SetupCount = Nothing
            Dim selectedAnalysis As Integer = savedView.analysisID
            '   Dim strFilteron As String = savedView.filteron
            Dim blnHasTotalCol As Boolean = True

            Logger.Log.Info(String.Format("REController Viewer (HttpPost) method execution Starts"))
            Try
                blnArchBudget = DirectCast(Session("BudgetArch"), Boolean)
                If (Not Session("UserType") Is Nothing) And (Not Session("UserInfo") Is Nothing) Then
                    UserInfo = DirectCast(Session("UserInfo"), User)
                    If (UserInfo.AnalysisId = 0) Then
                        Return RedirectToAction("Index", "Dashboard")
                    Else
                        intAnalysisID = savedView.analysisID 'UserInfo.AnalysisId
                        intFirstFiscal = UserInfo.fiscalMonthOfSelectedCompany
                    End If
                End If

                LoadSetupCount(setupCountObj, UserInfo, selectedAnalysis)

                If (spdChart.Sheets(0).Charts.Count > 0) Then
                    spdChart.Sheets(0).Charts.Remove(spdChart.Sheets(0).Charts(0))
                End If

            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Viewer : Error occured while loading Revenue & Expense Chart -", ex.Message)
                Logger.Log.Error(String.Format("REController Viewer (HttpPost) method execution ends with Error message: {0}, StackTrace: {1}", ex.Message, ex.StackTrace))
                LoadSetupCount(setupCountObj, UserInfo, selectedAnalysis)
            End Try
            Try
                spdAnalytics.Sheets(0).RowCount = 0
                savedView.format = CommonProcedures.ConvertSavedFormatID(savedView.format, savedView.showbudget)
                SetSpreadProperties(spdAnalytics, savedView.format - 1, formValues.intPeriod - 1, savedView.showaspercent, formValues.intHPeriod)

                If (savedView.charttype <> 1 And Not (savedView.account1 Is Nothing)) Then
                    'Select Case savedView.format
                    '    Case 6, 7, 10, 11
                    '        blnHasTotalCol = True
                    'End Select
                    Charts.BuildChart(Me.ControllerContext.Controller, spdChart, spdAnalytics, savedView.charttype, savedView.format - 1, intFirstNumberColumn, savedView.showaspercent, savedView.trend, blnHasTotalCol)
                    ViewData("blnUpdateChart") = True
                End If

                Dim periods As List(Of SelectListItem) = New List(Of SelectListItem)
                intNumberofPeriods = NumberofPeriods(sPeriod, intFirstFiscal)
                'For intCtr = 0 To intNumberofPeriods
                For intCtr = 0 To 11
                    periods.Add(New SelectListItem With {.Text = CommonProcedures.GetMonth(intCtr, intFirstFiscal), .Value = intCtr + 1, .Selected = False})
                Next
                ViewData("Periods") = New SelectList(periods, "value", "text", "May")
                If savedView.format = 3 Then
                    Dim filterOn As List(Of SelectListItem) = New List(Of SelectListItem) From {
                        New SelectListItem With {.Text = "Current period amount", .Value = 1, .Selected = False},
                        New SelectListItem With {.Text = "Variance amount", .Value = 2, .Selected = False},
                        New SelectListItem With {.Text = "Percent variance", .Value = 3, .Selected = True}
                    }
                    ViewData("FilterOn") = New SelectList(filterOn, "value", "text", savedView.filteron)

                Else
                    Dim filterOn As List(Of SelectListItem) = New List(Of SelectListItem) From {
                        New SelectListItem With {.Text = "Current period amount", .Value = 1, .Selected = False},
                        New SelectListItem With {.Text = "Variance amount", .Value = 2, .Selected = False},
                        New SelectListItem With {.Text = "Percent variance", .Value = 3, .Selected = True},
                        New SelectListItem With {.Text = "YTD amount", .Value = 4, .Selected = False},
                        New SelectListItem With {.Text = "YTD Variance amount", .Value = 5, .Selected = False},
                        New SelectListItem With {.Text = "YTD Percent variance", .Value = 6, .Selected = True}
                    }
                    ViewData("FilterOn") = New SelectList(filterOn, "value", "text", savedView.filteron)
                End If
                ViewData("blnFilterOnSelected") = savedView.filter
                ViewData("ChartType") = savedView.charttype
                ViewData("blnHighVar") = savedView.highlightva
                ViewData("blnPosVar") = savedView.posvar
                ViewData("intPosVarAmt") = Math.Round(CDec(savedView.posvaramt), 0)
                ViewData("blnNegVar") = savedView.negvar
                ViewData("intNegVarAmt") = Math.Round(CDec(savedView.negvaramt), 0)
                ViewData("blnFilter") = savedView.filter
                ViewData("intFilterAmt") = Math.Round(CDec(savedView.filteramt), 0)
                If savedView.filteron > 99 Then
                    ViewData("intFilterOn") = savedView.filteron - 100
                    ViewData("blnFilterPosVar") = False
                    ViewData("blnFilterNegVar") = True
                Else
                    ViewData("intFilterOn") = savedView.filteron
                    ViewData("blnFilterPosVar") = True
                    ViewData("blnFilterNegVar") = False
                End If
                ViewData("blnShowPercent") = savedView.showaspercent
                ViewData("intPeriod") = intNumberofPeriods + 1
                ViewData("Period") = intNumberofPeriods + 1
                ViewData("intFormat") = savedView.format
                ViewData("intChartType") = savedView.charttype
                ViewData("blnShowTrend") = savedView.trend
                ViewData("blnShowBudget") = savedView.showbudget
                ViewData("intDept") = 1
                ViewData("postback") = True
                Dim accID As String = String.Empty
                Dim array() As Nullable(Of Integer) = {savedView.account1, savedView.account2, savedView.account3, savedView.account4, savedView.account5, savedView.account6, savedView.account7, savedView.account8, savedView.account9, savedView.account10}
                For AcId As Nullable(Of Integer) = 0 To array.Length - 1
                    If (Not IsNothing(array(AcId))) Then
                        accID = accID & array(AcId).ToString() & " "
                    End If
                Next
                accID = accID.TrimEnd(" ")
                ViewData("AccountID") = accID

                Session("sPeriod") = formValues.intPeriod - 1
                'Session("sFormat") = savedView.format
                Logger.Log.Info(String.Format("REController Viewer (HttpPost) method execution Ends"))
            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Viewer : Error occured while loading Revenue & Expense Chart -", ex.Message)
                Logger.Log.Error(String.Format("REController Viewer (HttpPost) method execution ends with Error message: {0}, StackTrace: {1}", ex.Message, ex.StackTrace))
                LoadSetupCount(setupCountObj, UserInfo, selectedAnalysis)
            End Try
            Return View("Viewer")
        End Function

        Protected Overrides Sub Dispose(disposing As Boolean)
            accountRepository.Dispose()
            accountTypeRepository.Dispose()
            balanceRepository.Dispose()
            chartFormatRepository.Dispose()
            chartTypeRepository.Dispose()

            MyBase.Dispose(disposing)
        End Sub
    End Class
End Namespace