﻿Imports System.Collections.Generic
Imports System.Linq
Imports System.Data

Public Class BalanceRepository
    Inherits GenericRepository(Of Balance)
    Implements IBalanceRepository

    'Implements IDisposable
    Private disposed As Boolean = False


    Public Sub New(context As DataAccess)
        'Me.context = context
        MyBase.New(context)
    End Sub

    Public Function GetBalances() As IEnumerable(Of Balance) Implements IBalanceRepository.GetBalances
        Return context.Balances.ToList()
    End Function

    Public Function GetBalanceRecordsByAnalysisId(analysisId As Integer) As IEnumerable(Of Balance) Implements IBalanceRepository.GetBalanceRecordsByAnalysisId
        Return context.Balances.Where(Function(b) b.AnalysisId = analysisId).ToList()
    End Function

    Public Function GeBalanceById(balanceId As Integer) As Balance Implements IBalanceRepository.GeBalanceById
        Return context.Balances.Find(balanceId)
    End Function

    Public Sub Save() Implements IBalanceRepository.Save
        context.SaveChanges()
    End Sub

    Protected Overridable Sub Dispose(disposing As Boolean)
        If Not Me.disposed Then
            If disposing Then
                context.Dispose()
            End If
        End If
        Me.disposed = True
    End Sub

    Public Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Public Function DeleteBalanceByAnalysis(analysisId As Integer) Implements IBalanceRepository.DeleteBalanceByAnalysis
        Logger.Log.Info(("Start Query : Get the list of Balance Itmes by AnalysisId: " & analysisId))
        Dim balance = (From d In context.Balances.Where(Function(a) a.AnalysisId = analysisId)
                                       Select d)
        If Not (balance Is Nothing) And balance.Count > 0 Then
            Logger.Log.Info(("Balance items count : " & balance.Count))
            Logger.Log.Info("Start deleting : Balance items")
            balance.ToList().ForEach(Sub(item) context.Balances.Remove(item))
            context.SaveChanges()
            Logger.Log.Info(("End deleting : Balance items"))
        End If
        Return balance.Count
    End Function
End Class
