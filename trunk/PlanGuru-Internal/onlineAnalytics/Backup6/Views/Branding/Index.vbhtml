﻿@ModelType onlineAnalytics.Branding
@Imports System.Collections.Generic
@Imports  onlineAnalytics
@Code
    ViewData("Title") = "PlanGuru Analytics | Branding"
    
    If Not Session("action") Is Nothing Then
        
        If (Session("action") = "SetDefaultAnalysis" Or Session("action") = "GetAnalyses") Then
            Layout = Nothing
            Session.Remove("action")
        End If
    Else
        Layout = "~/Views/Shared/AnalyticsMaster.vbhtml"
    End If
   
    Dim BrandingOption As New Branding
    
    If Not (Model Is Nothing) Then
        BrandingOption.PrimaryColor = Model.PrimaryColor
        BrandingOption.SecondaryColor = Model.SecondaryColor
        BrandingOption.Footnote = IIf(Model.Footnote Is Nothing, String.Empty, Model.Footnote)
    End If
    
End Code
<link href="@Url.Content("~/Content/css/jquery.fileupload.css")" rel="stylesheet" type="text/css" />
<link rel="stylesheet" media="screen" type="text/css" href="@Url.Content("~/Content/css/colorpicker/colorpicker.css")" />
<script src="@Url.Content("~/Scripts/jquery.validate.min.js")" type="text/javascript"></script>
<script src="@Url.Content("~/Scripts/jquery.validate.unobtrusive.min.js")" type="text/javascript"></script>
<script src="@Url.Content("~/Scripts/jquery.ui.widget.js")" type="text/javascript"></script>
<script src="@Url.Content("~/Scripts/jquery.fileupload.js")" type="text/javascript"></script>
<script src="@Url.Content("~/Scripts/colorpicker/colorpicker.js")" type="text/javascript"></script>
<div class="headerTable" style="float: right">
    <table>
        <tr>
            <td align="left" width="70%">
                <div class="breadscrum">
                    Branding Options</div>
            </td>
        </tr>
    </table>
</div>
<br />
<br />
<div class="center-panel">
    <fieldset>
        <div style="display: inline-block;">
            @If Not (DirectCast(TempData("Message"), String) Is Nothing) Then
                @<label class="success">@TempData("Message").ToString()
                </label>                         
            End If
            @If Not (DirectCast(TempData("ErrorMessage"), String) Is Nothing) Then
                @<label class="error">@TempData("ErrorMessage").ToString()
                </label>                         
            End If
        </div>
        <div style="margin:0px 0px 0px 20px">
            <h2>
                Site Logo</h2>
            <div style="display: inline-block; margin: 10px 0px 0px 0px">
                Brand PlanGuru Analytics with your own company / firm logo
                <br />
                <div style="display: inline-block; margin: 15px 10px 0px 0px">
                    <b>@Html.Label("Logo Image")</b></div>@Html.TextBox("Imagefilepath", Nothing, New With {.style = "Font-size:1.1em;width:500px;margin-right:10px;vertical-align:centre;"})
                @Html.TextBoxFor(Function(m) m.LogoImageFile, New With {.type = "file", .style = "display:none"})
                <input id="Browse" value="Browse" class="secondbutton_example" type="button" style="margin-right: 20px;" />
                <input id="Upload" value="Upload" class="secondbutton_example" type="button" data="@Url.Action("UploadFile", "Branding")"/>
                <br />
                <label id="fileUpdate">
                </label>
            </div>
        </div>
        @Using Html.BeginForm("Index", "Branding")
            @Html.AntiForgeryToken()   
            @Html.ValidationSummary(True)    
            @<div style="margin: 20px 0px 0px 20px">
                 <h2>Vanity URL</h2>
                  <div style="display: inline-block; margin: 10px 0px 0px 0px; width:70%;">
                    This is where your users under your account will be able to log in (Please note: This can only be set once)
                  </div>
                  <br />
                  <div style="display: inline-block; margin: 15px 10px 0px 0px">
                      <b style="margin:0px 10px 0px 0px">@Html.LabelFor(Function(m) m.VanityFirmName)</b>
                      @Code
            Dim firmName = Model.VanityFirmName
            If (Model.VanityFirmName = String.Empty) Then
                        @Html.TextBoxFor(Function(m) m.VanityFirmName, New With {.Style = "text-align:right;"})
                        @MvcHtmlString.Create(".planguruanalytics.com")
            Else
                        @<div style="float:right;color:blue;">@MvcHtmlString.Create(firmName & ".planguruanalytics.com")</div>
                        @Html.HiddenFor(Function(m) m.VanityFirmName)
            End If
                      End Code                     
                      <br />
                      @Html.ValidationMessageFor(Function(m) m.VanityFirmName, Nothing, New With {.Style = "margin-left:130px;"})
                  </div>
             </div>
            @<div style="margin: 20px 0px 0px 20px">
                <h2>
                    Customize Menu Colors</h2>
                <div style="display: inline-block; margin: 10px 0px 0px 0px; width:60%;">
                    Select your own menu colors to further brand your PlanGuru Analytics Subscription
                    <table style="width: 100%; margin: 10px 0px;" cellpadding="2">
                        <tr>
                            <td style="width: 33%">
                                @Html.LabelFor(Function(m) m.PrimaryColor, New With {.style = "font-weight:bold;"})
                            </td>
                            <td style="width:20%">
                                <div id="primary_color" class="colorSelector" style="float: left;">
                                    <div style="background-color: @BrandingOption.PrimaryColor">
                                    </div>
                                </div>
                                @Html.HiddenFor(Function(m) m.PrimaryColor)
                            </td>
                            <td>
                                <input id="resetcolors" type="button" class="secondbutton_example" value="Reset menu colors"/>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 40%">@Html.LabelFor(Function(m) m.SecondaryColor, New With {.style = "font-weight:bold;"})
                            </td>
                            <td colspan="2">
                                <div id="secondary_color" class="colorSelector" style="float: left;">
                                    <div style="background-color: @BrandingOption.SecondaryColor">
                                    </div>
                                </div>@Html.HiddenFor(Function(m) m.SecondaryColor)
                            </td>                          
                        </tr>
                    </table>
                </div>
            </div>
            @<div style="margin: 10px 0px 0px 20px">
                <h2>
                    Customize Footnote</h2>
                <div style="display: inline-block; margin: 10px 0px 0px 0px">
                    Enter a custom footnote of up to 1000 characters that will appear on pages
                    <br />
                    <div style="display: inline-block; margin: 20px 0px 20px 0px; width: 90%;">
                        <fieldset>
                            <legend>Footnote text</legend>
                            @Html.TextAreaFor(Function(m) m.Footnote, 8, 100, New With {.maxlength = "1000"})
                            <br />@Html.ValidationMessageFor(Function(m) m.Footnote)
                        </fieldset>
                    </div>
                </div>
                <div style="display: inline-block; margin: 0px 0px 20px 0px; width: 90%;">
                    <input value="Update Branding Options" class="secondbutton_example" type="Submit" />
                </div>
            </div>           
        End Using
    </fieldset>
</div>
<script type="text/javascript">
    $(document).ready(function () {

        $('#primary_color').ColorPicker({
            onShow: function (colpkr) {
                $(colpkr).fadeIn(500);
                return false;
            },
            onHide: function (colpkr) {
                $(colpkr).fadeOut(500);
                return false;
            },
            onSubmit: function (hsb, hex, rgb, el) {
                $(el).val(hex);
                $(el).ColorPickerHide();
                $('#primary_color div').css('backgroundColor', '#' + hex);
                $("#PrimaryColor").val('#' + hex);
            }
        }).on('keyup', function () {
            $(this).ColorPickerSetColor(this.value);
        });

        $('#secondary_color').ColorPicker({
            onShow: function (colpkr) {
                $(colpkr).fadeIn(500);
                return false;
            },
            onHide: function (colpkr) {
                $(colpkr).fadeOut(500);
                return false;
            },
            onSubmit: function (hsb, hex, rgb, el) {
                $(el).val(hex);
                $(el).ColorPickerHide();
                $('#secondary_color div').css('backgroundColor', '#' + hex);
                $("#SecondaryColor").val('#' + hex);
            }
        }).on('keyup', function () {
            $(this).ColorPickerSetColor(this.value);
        });

        function getNameFromPath(strFilePath) {

            //var ObjRE = new RegExp(/([^\/\\]+)$/);
            //var filename = strFilePath.split('\\').pop().replace(/\.+$&#/, '');
            var filename = strFilePath.split('\\').pop().replace(/[^a-zA-Z0-9.]+/g, '');

            if (filename == null)
            { return null; } else { return filename; }

        }

        //fire the actual file input click
        $("#Browse").click(function () {
            $("#LogoImageFile").click();
        });

        $("#LogoImageFile").change(function () {

            var file = getNameFromPath($(this).val());
            var flag = false;

            if (file != null) {

                var extension = file.substr((file.lastIndexOf('.') + 1));

                switch (extension) {
                    case 'jpg':
                    case 'png':
                    case 'gif':
                        flag = true;
                        break;
                    default:
                        flag = false;
                }
            }

            if (flag == false) {

                Cleanfile();
                $('#fileUpdate').html("You can upload only jpg, png or gif extension file.");
                $('#fileUpdate').css('color', '#D8000C');
            }
            else {
                if (this.files[0].size / 1048576 > 1) {

                    Cleanfile();
                    $('#fileUpdate').html("You can upload file up to 1 MB.");
                    $('#fileUpdate').css('color', '#D8000C');
                }
            }

        });

        function Cleanfile() {
            $("#LogoImageFile").val('');
            $("#Imagefilepath").val('');
            jQuery("#LogoImageFile").get(0).files[0] = null;
        }

        //display the chosen file name to the user
        $("#LogoImageFile").bind('change', function () {
            //we don't want the           

            $("#Imagefilepath").val(getNameFromPath(this.value));
            $("#LogoImageFile").val($("#Imagefilepath").val());
        });


        $('#Upload').click(function (event) {

            var url = $(this).attr('data');
            uploadFileOnClick("LogoImageFile", $("#LogoImageFile"), $("#LogoImageFile"), true, url);
        });

        function uploadFileOnClick(id, file, post, _autoUpload, url) {
            $('.success').text('').hide();
            $('.error').text('').hide();            
            
            $('#' + id).fileupload({
                type: "POST",
                dataType: 'json',
                multipart: true,
                url: url,
                autoUpload: true,
                cache: false,
                data: function () {
                    var data = new FormData();
                    data.append("fileDescription", jQuery("#LogoImageFile").val());
                    data.append("chosenFile", jQuery("#LogoImageFile").get(0).files[0]);
                    return data;
                    // Or simply return new FormData(jQuery("form")[0]);
                } (),
                success: function (data) {
                    if (data.status = "success") {
                        $('#fileUpdate').html(getNameFromPath(jQuery("#LogoImageFile").val()) + " uploaded successfully.");
                        $('#fileUpdate').css('color', '#4F8A10');
                        $("#branding_logo").attr("src", data.src);
                    }
                },
                error: function (data) {
                    if (data.status = "error") {
                        $('#fileUpdate').html(data.error);
                        $('#fileUpdate').css('color', '#4F8A10');
                    }
                }
            })
            /*
            .on('fileuploadprogressall', function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('.progress .progress-bar').css('width', progress + '%');
            });
            */

            if (_autoUpload) {
                $('#' + id).fileupload('add', { files: file });
            }

        }

        $('#resetcolors').click(function (event) {

            $.ajax({
                type: "POST",
                url: '@Url.Action("ResetMenuColors", "Branding")',
                global: false,
                cache: false,
                data: '',
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {

                    window.location.href = '@Url.Action("Index", "Branding")';

                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    // var msg = JSON.parse(XMLHttpRequest.responseText);
                }
            });

        });

    });
</script>
