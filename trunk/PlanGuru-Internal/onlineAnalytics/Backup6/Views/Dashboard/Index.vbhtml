﻿@Imports Farpoint.Web.Spread
@Imports Farpoint.Mvc.Spread
@Imports  onlineAnalytics
@Code
    ViewData("Title") = "PlanGuru Analytics | Dashboard"
   
    If Not Session("action") Is Nothing Then
        
       If (Session("action") = "SetDefaultAnalysis" or Session("action") = "GetAnalyses") Then
            Layout = Nothing
            Session.Remove("action")
        End If
    Else
        Layout = "~/Views/Shared/AnalyticsMaster.vbhtml"
    End If       
    Layout = "~/Views/Shared/AnalyticsMaster.vbhtml"
End Code
<script type="text/javascript">
    window.ViewerControlId = "#tabbeddashboardView";
    $(function () {
        $("#updatesetup")
            .button()
            .click(function (event) {

                if (isSessionAliveCalled == 'false') { IsSessionAlive(); }

                document.location = '@Url.Action("Index", "DashboardSetup")';
            });
    });
        $(document).ready(function () {

        if (isSessionAliveCalled == 'false') { IsSessionAlive(); }

        $("#footerpanel").removeClass("plangurufooter");
        $("#footerpanel").addClass("plangurufooterfordashboard");
    });
</script>
@Code
    Dim UserInfo As User = Nothing
    Dim ua As New UserAccess
    
    Dim objSetup As New SetupCount
    
    If Not (Session("UserInfo") Is Nothing) Then
        UserInfo = DirectCast(Session("UserInfo"), User)
    End If
    
    If (Not ViewBag.Setup Is Nothing) Then
        objSetup = DirectCast(ViewBag.Setup, SetupCount)
    End If
    
End code
<div class="center-panel" >
    @If Not (DirectCast(TempData("Message"), String) Is Nothing) Then
        @<label class="success">@TempData("Message").ToString()
        </label>                         
    End If
    @If Not (DirectCast(TempData("ErrorMessage"), String) Is Nothing) Then
        @<label class="error">@TempData("ErrorMessage").ToString()
        </label>                         
    End If
    @Code
        
        @Html.Hidden("countOfDataUploaded", objSetup.countDataUploadedOfSelectedAnalysis)        
        @<label id="validationMessage" class="info" style="display: none;">
            @If (objSetup.AanlysisCount > 0 And objSetup.selectedAnalysis = 0) Then
                @MvcHtmlString.Create("Please select company & one of the respective analysis from top-menu selection, to get the dashboard view.")
            ElseIf (objSetup.selectedAnalysis > 0 And objSetup.countDataUploadedOfSelectedAnalysis = 0) Then
                @MvcHtmlString.Create("Data has not been uploaded for selected analysis.")
            End If
        </label> 
        
        If (Not UserInfo Is Nothing) Then
                   
            If (UserInfo.UserRoleId <= UserRoles.SAU) Then
                    
                If (objSetup.CompanyCount = 0 Or objSetup.AanlysisCount = 0) Then
        @<label class="info">You have to create below listed action item, to get the dashboard
            view.</label> 
                End If
                If (objSetup.CompanyCount = 0) Then
        @<div class="secondbutton_example" style='margin: 20px; width: 200px;'>
            <a href='@Url.Action("Create", "Company")' style="text-decoration:none;color: #FFFFFF;">
                Add Company</a>
        </div>  
                End If
                If (objSetup.CompanyCount > 0 And objSetup.AanlysisCount = 0) Then
        @<div class="secondbutton_example" style='margin: 20px; width: 200px;'>
            <a href='@Url.Action("Create", "Analysis")'  style="text-decoration:none;color: #FFFFFF;">
                Add Analysis</a>
        </div>  
                End If
                If (objSetup.CompanyCount = 0 Or objSetup.AanlysisCount = 0) Then
        @<div class="secondbutton_example" style='margin: 20px; width: 200px;'>
            <a href='@Url.Action("CreateUser", "User")'  style="text-decoration:none;color: #FFFFFF;">
                Add User</a>
        </div>  
                End If
            ElseIf (UserInfo.UserRoleId > UserRoles.SAU) Then
                
                If (objSetup.CompanyCount = 0) Then
                    @<label class="info">The Administrator has not give you access to a company / analysis.</label>
                ElseIf (objSetup.AanlysisCount = 0) Then
                    @<label class="info">The Administrator has not given you access to an analysis.</label> 
                End If
                
            End If
        End If
        
        If (objSetup.CompanyCount > 0 And objSetup.AanlysisCount > 0 And objSetup.selectedAnalysis > 0 And objSetup.countDataUploadedOfSelectedAnalysis > 0) Then
        @<div id="tabbeddashboardView" style="display:none;">
            @Using Html.BeginForm("Index", "Dashboard")
                    @<table style="width: 100%; margin-top: 7px;" class="center-top-select">
                        <tr>
                            <td>
                                <span>Period</span>
                                    @Html.DropDownList("intPeriod", DirectCast(ViewData("Periods"), SelectList), New With {.Class = "select"})
                            </td>                            
                           
                            <td id="lnkChangeDashboardSetup" style="display: none;float:right;">
                                 <input id="configure_dashboard" type="button" value="Configure Dashboard" class="cancelbutton" />
                            </td>
                        </tr>
                    </table>
            End Using
            <div id="tabs" style="margin-top:5px;font-size:12.5px;">
                <ul>
                     <li><a href="#dashboardView">Your Dashboard</a></li>
                     <li><a href="#sharedDashboardView">Shared Analysis Dashboard</a></li>
                </ul>
            
                <!-- Personal Dashboard -->
                <div id="dashboardView" style="display:none;">            
                    <div id="spread" style="padding-top: 1px; display: inline-block; margin-left: 50px;">
                        @Html.FpSpread("spdDashboard", Sub(x)
                                                           x.RowHeader.Visible = False
                                                           x.ActiveSheetView.PageSize = 1000
                                                       End Sub)
                    </div>
                </div>
        
                <!-- Shared Analysis Dashboard -->  
                <div id="sharedDashboardView" style="display:none">
                    <div id="analysis_spread" style="padding-top: 1px; display: inline-block; margin-left: 50px;">
                        @Html.FpSpread("spdAnalysisDashboard", Sub(x)
                                                                   x.RowHeader.Visible = False
                                                                   x.ActiveSheetView.PageSize = 1000
                                                               End Sub)
                    </div>    
                </div>
            </div>    
        </div>
        
               
            
        End If
        
    End Code
</div>
<script type="text/javascript">
    $(function () {
        IsSessionAlive();
        $("#tabs").tabs({            
            activate: function (event, ui) {
                if (ui.newTab.index() == 1) {
                    $.getJSON('@Url.Action("SetSession")', { dataToSave: 1 }, function (data) {
                        if (data.message == "Saved") {
                            var loggedUser = '@UserInfo.UserRoleId';
                            
                            var cruuser = '@UserRoles.CRU.GetHashCode()';
                            if (loggedUser == cruuser) {
                                $("#lnkChangeDashboardSetup").hide();
                            }
                            else {
                                $("#lnkChangeDashboardSetup").show();
                            }
                        }
                    });                     
                }
                else if (ui.newTab.index() == 0) {
                    $.getJSON('@Url.Action("SetSession")', { dataToSave: 0 }, function (data) {
                        if (data.message == "Saved") {
                            $("#lnkChangeDashboardSetup").show(); 
                        }
                    });                     
                }
            }
        });
    });

    $("#configure_dashboard").click(function () {
        window.location = '@Url.Action("Index", "DashboardSetup")';
    });

    $(document).ready(function () {
        IsSessionAlive();
        $("#lnkChangeDashboardSetup").show();
        var currentTabIndex = '@UserInfo.currentTabIndex'
        if (currentTabIndex == null || currentTabIndex == "undefined")
        { currentTabIndex = 0; }

        $("#tabs").tabs('option', 'active', currentTabIndex);

        //hide the entire Shared Analysis tab if the Option1 is not set in analysis table 
        //disable just disables the tab, for hiding it we have kept css in style.css ::- li.ui-state-default.ui-state-disabled[role=tab]:not(.ui-tabs-active){display:none;}
        var isSharedAnalysis = '@UserInfo.isSharedAnalysis'
        if (isSharedAnalysis.toLowerCase() == "true") {
            $("#tabs").tabs("enable", 1);
        }
        else {
            if (currentTabIndex == 1) {
                $.getJSON('@Url.Action("SetSession")', { dataToSave: 0 }, function (data) {
                    if (data.message == "Saved") {
                        $("#tabs").tabs('option', 'active', 0);                        
                    }
                });
            }            
            $("#tabs").tabs("disable", 1);            
        }
    });

    $('#intPeriod').change(function () {
        this.form.submit();
    });
</script>
