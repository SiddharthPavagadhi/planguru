﻿Imports FarPoint.Mvc.Spread
Imports System.Data.Entity
Imports System.Web.Routing
Imports System.Security.Cryptography
Imports System.Runtime.CompilerServices

Public Module UrlExtension


    Public Function generateUrlToken(controllerName As String, actionName As String, argumentParams As ArrayList, password As String) As String
        Dim token As String = ""
        'The salt can be defined global
        Dim salt As String = "#PlanGuru#"
        'generating the partial url
        Dim stringToToken As String = controllerName & "/" & actionName & "/"
        For Each param As String In argumentParams
            stringToToken += "/" & param
        Next

        'Converting the salt in to a byte array
        Dim saltValueBytes As Byte() = System.Text.Encoding.ASCII.GetBytes(salt)
        'Encrypt the salt bytes with the password
        Dim key As New Rfc2898DeriveBytes(password, saltValueBytes)
        'get the key bytes from the above process
        Dim secretKey As Byte() = key.GetBytes(16)
        'generate the hash
        Dim tokenHash As New HMACSHA1(secretKey)
        tokenHash.ComputeHash(System.Text.Encoding.ASCII.GetBytes(stringToToken))
        'convert the hash to a base64string
        token = Convert.ToBase64String(tokenHash.Hash)
        Return token
    End Function

    <Extension()>
    Public Function AbsoluteAction(url As UrlHelper, _action As String, controller As String, Optional routeValues As Object = Nothing) As String
        Dim requestUrl As Uri = url.RequestContext.HttpContext.Request.Url
        Dim routeValuesLocal As RouteValueDictionary = Nothing
        Dim arrylist As ArrayList = Nothing
        ' Convert the routeValues to something we can modify.
        If routeValues IsNot Nothing Then
            routeValuesLocal = If(TryCast(routeValues, IDictionary(Of String, Object)), New RouteValueDictionary(routeValues))

            arrylist = New ArrayList()
            For Each routes As Object In routeValuesLocal.Values
                arrylist.Add(Convert.ToString(routes))
            Next

        End If

        Dim newurl As String = String.Empty

        newurl = url.Action(_action, controller, routeValues)

        If routeValues IsNot Nothing Then
            If (newurl.Contains("?")) Then
                newurl += "&urltoken=" & HttpUtility.UrlEncode(generateUrlToken(controller, _action, arrylist, "123456"))
            Else
                newurl += "?urltoken=" & HttpUtility.UrlEncode(generateUrlToken(controller, _action, arrylist, "123456"))
            End If
        End If

        Dim strabsoluteAction As String = String.Format("{0}://{1}{2}", requestUrl.Scheme, requestUrl.Authority, newurl)

        Return strabsoluteAction
    End Function

End Module
