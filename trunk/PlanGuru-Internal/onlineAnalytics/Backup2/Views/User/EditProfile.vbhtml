﻿@ModelType onlineAnalytics.ResetPassword
@Imports System.Collections.Generic
@Imports  onlineAnalytics
@Code
    ViewData("Title") = "PlanGuru Analytics | Edit Profile"
    If Not Session("action") Is Nothing Then
        
        If (Session("action") = "SetDefaultAnalysis" or Session("action") = "GetAnalyses") Then
            Layout = Nothing
            Session.Remove("action")
        End If
    Else
        Layout = "~/Views/Shared/AnalyticsMaster.vbhtml"
    End If
    
    Dim UserInfo As New User()
  
    If Not (Session("UserInfo") Is Nothing) Then
        UserInfo = DirectCast(Session("UserInfo"), User)
    End If
    
    Dim ua As New UserAccess
    If Not (Session("UserAccess") Is Nothing) Then
        ua = DirectCast(Session("UserAccess"), UserAccess)
    End If
End Code
<script src="@Url.Content("~/Scripts/jquery.validate.min.js")" type="text/javascript"></script>
<script src="@Url.Content("~/Scripts/jquery.validate.unobtrusive.min.js")" type="text/javascript"></script>
<div class="headerTable" style="float: right">
    <table>
        <tr>
            <td align="left" width="70%">
                <div class="breadscrum"><a href='@Url.Action("Index", "User")'>User List</a><span> > </span>Edit Profile</div>
            </td>          
        </tr>
    </table>
</div>
<div class="center-panel">
    @Using Html.BeginForm("EditProfile", "User")
        @Html.AntiForgeryToken()   
        @Html.ValidationSummary(True)    
        If (Not IsNothing(Model)) Then                 
            @<fieldset>
            <div style="width: 100;">
                <div style="float: left; width: 33%; display: inline-block;">
                    <ol class="round">
                        <li>
                            @Html.LabelFor(Function(m) m.UserName)<br />
                            @Html.TextBoxFor(Function(m) m.UserName)<br />
                            @Html.ValidationMessageFor(Function(m) m.UserName)
                        </li>
                        <li>
                            @Html.LabelFor(Function(m) m.UserEmail)<br />
                            @Html.TextBoxFor(Function(m) m.UserEmail)<br />
                            @Html.ValidationMessageFor(Function(m) m.UserEmail)
                        </li>
                        @If (UserInfo.UserRoleId = UserRoles.SAU Or UserInfo.UserRoleId = UserRoles.SSU) Then
                            @<li>
                                @Html.LabelFor(Function(m) m.SubscriberName)<br />
                                @Html.TextBoxFor(Function(m) m.SubscriberName)<br />
                                @Html.ValidationMessageFor(Function(m) m.SubscriberName)
                            </li>  
                        Else
                            @Html.HiddenFor(Function(m) m.SubscriberName)
                        End If
                    </ol>
                </div>
                <div style="float: left; width: 33%; display: inline-block;">
                    <ol class="round">
                        <li>
                            @Html.LabelFor(Function(m) m.FirstName)<br />
                            @Html.TextBoxFor(Function(m) m.FirstName)<br />
                            @Html.ValidationMessageFor(Function(m) m.FirstName)
                        </li>
                        <li>
                            @Html.LabelFor(Function(m) m.LastName)<br />
                            @Html.TextBoxFor(Function(m) m.LastName)<br />
                            @Html.ValidationMessageFor(Function(m) m.LastName)
                        </li>
                        <li>
                            <div style="padding: 0 0 20px; float: left;">@Html.Label("Do you want to change your password ?")</div>@Html.CheckBoxFor(Function(m) m.ChangePassword, New With {.class = "styled_checkbox"})
                        </li>
                    </ol>
                </div>
                <div id="ResetPassword" style="width: 33%; float: left; display: none;">
                    <ol class="round">
                        <li>
                            @Html.LabelFor(Function(m) m.CurrentPassword)<br />
                            @Html.PasswordFor(Function(m) m.CurrentPassword)<br />
                            @Html.ValidationMessageFor(Function(m) m.CurrentPassword, "", New With {.id = "ValidateCurrentPassword"})
                        </li>
                        <li>
                            @Html.LabelFor(Function(m) m.NewPassword)<br />
                            @Html.PasswordFor(Function(m) m.NewPassword)<br />
                            @Html.ValidationMessageFor(Function(m) m.NewPassword, "", New With {.id = "ValidateNewPassword"})
                        </li>
                        <li>
                            @Html.LabelFor(Function(m) m.ConfirmNewPassword)<br />
                            @Html.PasswordFor(Function(m) m.ConfirmNewPassword)<br />
                            @Html.ValidationMessageFor(Function(m) m.ConfirmNewPassword, "", New With {.id = "ValidateConfirmPassword"})
                        </li>
                    </ol>
                </div>
                <div class="input-form">
                    <input id="Update" type="submit" value="Update Profile" class="secondbutton_example" />
                    <input id="cancel-button" type="button" value="Cancel" class="secondbutton_example" />
                </div>               
                @Html.HiddenFor(Function(m) m.UserId)
                 @Html.HiddenFor(Function(m) m.UserRoleId)
            </div>
        </fieldset>       
        End If
         @<div style="float: left; margin-left: 20px;">
            @If Not (DirectCast(TempData("Message"), String) Is Nothing) Then
                @<label class="success">@TempData("Message").ToString()
                </label>                         
            End If
            @If Not (DirectCast(TempData("ErrorMessage"), String) Is Nothing) Then
                @<label class="error">@TempData("ErrorMessage").ToString()
                </label>                         
            End If
         </div>
    End Using
</div>
<script type="text/javascript" language="javascript">

    $(document).ready(function () {
        IsSessionAlive();

        $("#ChangePassword").after("<label name='ChangePassword_lbl' class='styled_checkbox' for='ChangePassword'></label>");

        //Input Mask for landline phone number
        $("#ContactTelephone").mask("(999)-999-9999");
        //$("#FiscalMonthStart").mask("9?9");

        ChangePassword();

        $("#ChangePassword").click(function (event) {
            ChangePassword();
        });

        function ChangePassword() {
            IsSessionAlive();
            if ($("#ChangePassword").is(':checked')) {
                $("#ResetPassword").show();
            }
            else {
                $("#ResetPassword").hide();
            }
        }

        $('#Update').click(function (event) {
            IsSessionAlive();
            var validate = true;

            if ($("#ChangePassword").is(':checked')) {

                validate = validateCurrentPassword(validate);
                validate = validateNewPassword(validate);
                validate = validateConfirmPassword(validate);
            }

            return validate;
        });

        $("#CurrentPassword").blur(function (event) {
            validateCurrentPassword(true);
        });

        function validateCurrentPassword(validate) {

            IsSessionAlive();
            if ($("#CurrentPassword").val() == "") {
                $("#ValidateCurrentPassword").removeClass("field-validation-valid").addClass("field-validation-error");
                $("#ValidateCurrentPassword").text("Current Password is required.");
                return false;
            }
            else {
                $("#ValidateCurrentPassword").removeClass("field-validation-error").addClass("field-validation-valid");
                $("#ValidateCurrentPassword").text("");
            }

            if (($("#CurrentPassword").val() != "") && validate == false) {
                return false;
            }

            return true;
        }

        $("#NewPassword").blur(function (event) {
            validateNewPassword(true);
        });
        function validateNewPassword(validate) {
            IsSessionAlive();
            if ($("#NewPassword").val() == "") {
                $("#ValidateNewPassword").removeClass("field-validation-valid").addClass("field-validation-error");
                $("#ValidateNewPassword").text("New Password is required.");
                return false;
            }
            else {
                $("#ValidateNewPassword").removeClass("field-validation-error").addClass("field-validation-valid");
                $("#ValidateNewPassword").text("");
            }

            if (($("#NewPassword").val() != "") && validate == false) {

                return false;
            }

            return true;
        }


        $("#ConfirmNewPassword").blur(function (event) {
            validateConfirmPassword(true);
        });

        function validateConfirmPassword(validate) {

            if ($("#ConfirmNewPassword").val() == "") {
                $("#ValidateConfirmPassword").removeClass("field-validation-valid").addClass("field-validation-error");
                $("#ValidateConfirmPassword").text("Confirm New Password is required.");
                return false;
            }
            else {
                $("#ValidateConfirmPassword").removeClass("field-validation-error").addClass("field-validation-valid");
                // $("#ValidateConfirmPassword").text("");
            }

            if (($("#ConfirmNewPassword").val() != "") && validate == false) {

                return false;
            }

            return true;
        }


    });
    $("#cancel-button").click(function () {
        window.location = '@Url.Action("Index", "Dashboard")';
    });
</script>
