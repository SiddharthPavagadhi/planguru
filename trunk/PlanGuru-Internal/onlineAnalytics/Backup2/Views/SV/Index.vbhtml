﻿@Imports Farpoint.Web.Spread
@Imports Farpoint.Mvc.Spread
@Imports  onlineAnalytics
@Code
    ViewData("Title") = "PlanGuru Analytics | Saved Reports"
    If Not Session("action") Is Nothing Then
        
        If (Session("action") = "SetDefaultAnalysis" Or Session("action") = "GetAnalyses") Then
            Layout = Nothing
            Session.Remove("action")
        End If
    Else
        Layout = "~/Views/Shared/AnalyticsMaster.vbhtml"
    End If
    
    Dim UserInfo As User = Nothing
    Dim ua As New UserAccess
    
    Dim objSetup As New SetupCount
    
    If Not (Session("UserInfo") Is Nothing) Then
        UserInfo = DirectCast(Session("UserInfo"), User)
    End If
    
    If (Not ViewBag.Setup Is Nothing) Then
        objSetup = DirectCast(ViewBag.Setup, SetupCount)
    End If
End Code
<div style="margin-top:20px">
    <div class="savedviewdiv">
        <h2 class="heading">Review Saved Reports</h2>
        <ul id="SavedReportsList">
        </ul>
    </div>
    @Code If (UserInfo.UserRoleId <> UserRoles.CRU) Then End Code
    <div class="savedviewdiv">
        <h2 class="heading">Manage Saved Reports</h2>
        <a class="savedviews" href='@Url.Action("Views", "SV")'>Edit and Delete Saved Reports</a>        
    </div>  
    @Code End If End Code
              
</div>
<script type="text/javascript">
    $(document).ready(function () {
        getSavedReportsList();
    });

    function getSavedReportsList() {

        if (isSessionAliveCalled == 'false') { IsSessionAlive(); }

        $.ajax({
            type: "GET",
            url: '@Url.Action("GetSavedViewsList", "SV")',
            global: false,
            cache: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (msg) {
                var _selectedCompany
                if (msg !== undefined && (msg != "")) {

                    for (var savedreport_index = 0; savedreport_index < msg.length; savedreport_index++) {
                        var idcontrolVal = msg[savedreport_index].Value;
                        var id_controller = idcontrolVal.split("_");
                        var controller = id_controller[0]
                        var url;
                        if (controller.trim() == "RE")
                            url = '@Url.Action("Viewer", "RE", New With {.Id = "-1"})'
                        else if (controller.trim() == "ALC")
                            url = '@Url.Action("Viewer", "ALC", New With {.Id = "-1"})'
                        else if (controller.trim() == "CF")
                            url = '@Url.Action("Viewer", "CF", New With {.Id = "-1"})'
                        else if (controller.trim() == "RA")
                            url = '@Url.Action("Viewer", "RA", New With {.Id = "-1"})'
                        else if (controller.trim() == "OM")
                            url = '@Url.Action("Viewer", "OM", New With {.Id = "-1"})'

                        url = url.replace("-1", id_controller[1]);
                        $("#SavedReportsList").append("<li><a  class='savedviews' href='" + url + "' data='" + msg[savedreport_index].Value + "' >" + msg[savedreport_index].Text + "</a></li>");
                    }
                }
                else {
                    $("#SavedReportsList").html("No Saved Reports found ! ");
                }

            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
            }
        });
    }
</script>