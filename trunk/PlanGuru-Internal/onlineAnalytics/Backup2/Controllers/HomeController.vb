﻿Imports System
Imports System.Collections.Generic
Imports System.Configuration
Imports System.Linq
Imports System.Web.Mvc
Imports FarPoint.Mvc.Spread
Imports System.Data.Entity
Imports Recurly
Imports System.Data.Entity.Validation
Imports onlineAnalytics.Common

Namespace onlineAnalytics
    Public Class HomeController
        Inherits System.Web.Mvc.Controller
        Dim Format As Integer = 1
        Dim Period As Integer = 10
        Dim intFirstNumCol As Integer = 4
        Private utility As New Utility()
        Private common As New Common()
        Private customerRepository As ICustomerRepository
        Private userRepository As IUserRepository
        Private accountRepository As IAccountRepository
        Private chartFormatRepository As IChartFormatRepository
        Private chartTypeRepository As IChartTypeRepository
        Private balanceRepository As IBalanceRepository
        Private analysisRepository As IAnalysisRepository

        Public Sub New()
            Me.userRepository = New UserRepository(New DataAccess())
            Me.customerRepository = New CustomerRepository(New DataAccess())
            Me.chartFormatRepository = New ChartFormatRepository(New DataAccess())
            Me.chartTypeRepository = New ChartTypeRepository(New DataAccess())
            Me.balanceRepository = New BalanceRepository(New DataAccess())
        End Sub

        Public Sub New(chartFormatRepository As IChartFormatRepository)
            Me.chartFormatRepository = chartFormatRepository
        End Sub

        Public Sub New(chartTypeRepository As IChartTypeRepository)
            Me.chartTypeRepository = chartTypeRepository
        End Sub

        Public Sub New(balanceRepository As IBalanceRepository)
            Me.balanceRepository = balanceRepository
        End Sub

        Public Sub New(userRepository As IUserRepository)
            Me.userRepository = userRepository
        End Sub

        Public Sub New(customerRepository As ICustomerRepository)
            Me.customerRepository = customerRepository
        End Sub

        Function Index() As ActionResult

            Logger.Log.Info(String.Format("Index Function Execution Started"))
            Dim result As Customer = Nothing
            Dim subdomain As String = Nothing
            Dim UrlParams() As String
            Try
                TempData.Clear()

                Logger.Log.Info(String.Format("Request.Url: {0}", Request.Url.ToString().ToLower()))

                UrlParams = Request.Url.ToString().Replace("https://", "").Split(".")

                Logger.Log.Info(String.Format("Url Param(0):{0}, Param(1):{1}", UrlParams(0), UrlParams(1)))

                If Not (UrlParams(0).Equals("www") And UrlParams(0).Equals("planguruanalytics")) Then
                    subdomain = GetSubDomain(Request.Url)
                    Logger.Log.Info(String.Format("subdomain:{0}", subdomain))
                End If

                result = (From c In utility.CustomerRepository.GetCustomers
                          Where c.PlanTypeId = PlanType.BP And c.VanityFirmName = subdomain).SingleOrDefault()

                If (Not (result Is Nothing)) Then

                    ViewBag.Logo = result.LogoImageFile

                    Logger.Log.Info(String.Format("CustomerId:{0}, VanityFirmName:{1}, Logo:{2}", result.CustomerId, result.VanityFirmName, result.LogoImageFile))

                Else

                    Logger.Log.Info(String.Format("Doesn't find such vanity firm name requested: {0} ", Request.Url.ToString()))
                    ViewBag.Logo = String.Empty

                End If

                If (Not (result Is Nothing)) Then
                    Return View("Login")
                Else
                    Return View()
                End If

            Catch indexException As Exception

                If (Not IsNothing(result)) Then
                    Logger.Log.Error(String.Format("Error occured while match subdomain name : {0} - " + Environment.NewLine + "{1}, Stack Trace: {2} ", Request.Url.ToString(), indexException.Message, indexException.StackTrace))
                Else
                    Logger.Log.Error(String.Format("Error occured while match subdomain name : {0} Stack Trace: {1} ", indexException.Message, indexException.StackTrace))
                End If
                Return View()
            Finally
                Logger.Log.Info(String.Format("Index Function Execution Ended"))
            End Try

        End Function

        Private Shared Function GetSubDomain(url As Uri) As String

            If url.HostNameType = UriHostNameType.Dns Then
                Dim host As String = url.Host
                If host.Split("."c).Length > 2 Then
                    Dim lastIndex As Integer = host.LastIndexOf(".")
                    Dim index As Integer = host.LastIndexOf(".", lastIndex - 1)
                    Return host.Substring(0, index)
                End If
            End If

            Return Nothing
        End Function

        Function About() As ActionResult
            Return View()
        End Function

        Function Contact() As ActionResult
            ViewData("Message") = "Your contact page."
            Return View()
        End Function

        Protected Overrides Sub Dispose(disposing As Boolean)
            userRepository.Dispose()
            chartFormatRepository.Dispose()
            chartTypeRepository.Dispose()
            balanceRepository.Dispose()
            MyBase.Dispose(disposing)
        End Sub

#Region "ForgotUsername Methods"
        <HttpGet()>
        Function ForgotUsername() As ActionResult
            Try
                TempData.Clear()
            Catch ex As Exception
                TempData("ErrorMessage") = "Error occured on forgot username form -" + ex.Message.ToString()
                Logger.Log.Error(String.Format("Unable to Reset the username for " + Environment.NewLine + "{0}, Stack Trace: {1} ", ex.Message, ex.StackTrace))
            End Try
            Return View()
        End Function

        <HttpPost()>
        Function ForgotUsername(model As ForgotUsername) As ActionResult
            Logger.Log.Info(String.Format("ForgotUsername Function Execution Started"))
            Dim result As User = Nothing
            Try
                TempData.Clear()
                If ModelState.IsValid Then
                    result = utility.UserRepository.GetUsers.Where(Function(u) u.UserEmail = model.emailAddress).SingleOrDefault()
                    If (Not (result Is Nothing)) Then

                        If (SendMail_ForgotUserName(result, EmailType.ForgotUsername)) Then
                            TempData("Message") = "Email has been sent Successfully."
                            Logger.Log.Info(String.Format("ForgotUsername Reset Successfully of UserId : {0}", result.UserId))
                        End If
                    Else
                        TempData("InfoMessage") = "No user with the Email address entered was found."
                    End If
                End If
            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("There were no matching Planguru accounts found with the information you provided.-", ex.Message)
                If (Not IsNothing(result)) Then
                    Logger.Log.Error(String.Format("Error occured while Reset username for User : {0} - " + Environment.NewLine + "{1}, Stack Trace: {2} ", result.UserName, ex.Message, ex.StackTrace))
                Else
                    Logger.Log.Error(String.Format("Error occured while Reset username - {0} Stack Trace: {1} ", ex.Message, ex.StackTrace))
                End If
            Finally
                Logger.Log.Info(String.Format("ForgotUsername Function Execution Ended"))
            End Try

            Return View()
        End Function

        Private Function SendMail_ForgotUserName(User As User, emailId As Integer) As Boolean
            Logger.Log.Info(String.Format("User SendEmail Execution Started"))
            Dim IsMailTrigger As Boolean = True
            Try
                Logger.Log.Info("You have been added as a user - SendMail_ForgotUserName Started")

                Dim objEmailInfo = utility.EmailRepository.GetEmailInfoById(emailId)
                objEmailInfo.EmailBody = common.MergeUserEmailBody(User, objEmailInfo, Nothing)
                common.SendUserEmail(User, objEmailInfo)
                Logger.Log.Info("You have been added as a user - SendEmail Ended")
                Logger.Log.Info(String.Format("Email Sent Successfully to User: {0} for EmailId: {1}", User.UserId, emailId))
            Catch ex As Exception
                IsMailTrigger = False
                TempData("ErrorMessage") = String.Concat("Unable to Send Email -", ex.Message)
                Logger.Log.Error(String.Format("Unable to Send Email, UserId - {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2} ", User.UserId, ex.Message, ex.StackTrace))
            Finally
                Logger.Log.Info(String.Format("SendMail_ForgotUserName Execution Ended"))
            End Try
            Return IsMailTrigger
        End Function
#End Region

#Region "ForgotPassword Methods"

        <HttpGet()>
        Function ForgotPassword() As ActionResult
            Try
                TempData.Clear()
            Catch ex As Exception
                TempData("ErrorMessage") = "Error occured on forgot password form - " + ex.Message.ToString()
                Logger.Log.Error(String.Format("Unable to Reset the password for " + Environment.NewLine + "{0}, Stack Trace: {1} ", ex.Message, ex.StackTrace))
            End Try
            Return View()
        End Function

        Public Function ForgotPassword(userInput As ForgotPassword) As ActionResult
            Logger.Log.Info(String.Format("ForgotPassword Execution Started"))
            Dim userDetails As User = Nothing
            Try
                TempData.Clear()

                If ModelState.IsValid Then
                    userDetails = utility.UserRepository.GetUsers.Where(Function(u) u.UserName = userInput.userName And u.UserEmail = userInput.emailAddress).SingleOrDefault()

                    If (Not (userDetails Is Nothing)) Then
                        'userDetails = New User()
                        Dim password As String = common.Generate(8, 2)
                        userDetails.Password = common.Encrypt(password)
                        userDetails.SecurityKey = Guid.NewGuid().ToString()
                        userDetails.UpdatedBy = userDetails.UserId.ToString()
                        userDetails.UpdatedOn = DateTime.UtcNow()
                        utility.UserRepository.UpdateUser(userDetails)
                        utility.UserRepository.Save()

                        SendEmail(userDetails, EmailType.PasswordReset)
                        If (TempData("ErrorMessage") = Nothing) Then
                            TempData("Message") = "Reset Password link has been sent to your Email Id Successfully."
                        End If
                    Else
                        TempData("InfoMessage") = "No user with the Username and Email address entered was found."
                    End If
                End If
            Catch dbEntityEx As DbEntityValidationException
                TempData("ErrorMessage") = "Error occured, during reset password request - " + dbEntityEx.Message.ToString()
                For Each sError In dbEntityEx.EntityValidationErrors
                    Logger.Log.Error(String.Format("Entity of type ""{0}"" in state ""{1}"" has the following validation errors:", sError.Entry.Entity.[GetType].Name, sError.Entry.State))
                    For Each ve In sError.ValidationErrors
                        Logger.Log.Error(String.Format("-Property: {0}, Error: {1} ", ve.PropertyName, ve.ErrorMessage))
                    Next
                Next
            Catch ex As Exception
                TempData("ErrorMessage") = "Error occured, while reset password - " + ex.Message.ToString()
                If (Not IsNothing(userDetails)) Then
                    Logger.Log.Error(String.Format("Unable to Reset password for User : {0} - " + Environment.NewLine + "{1}, Stack Trace: {2} ", userDetails.UserName, ex.Message, ex.StackTrace))
                Else
                    Logger.Log.Error(String.Format("Unable to Reset password - {0} Stack Trace: {1} ", ex.Message, ex.StackTrace))
                End If

            Finally
                Logger.Log.Info(String.Format("ForgotPassword Execution Ended"))
            End Try
            Return View()
        End Function
#End Region

        Private Sub SendEmail(User As User, emailId As Integer)
            Logger.Log.Info(String.Format("User SendEmail Execution Started"))
            Try
                Logger.Log.Info("You have been added as a user - SendEmail Started")
                Dim planGuruUrl As String = "http://" + HostName(Request) + If(Request.Url.IsDefaultPort, Request.ApplicationPath + "/", ":" + Request.Url.Port.ToString() + "/")
                Dim objEmailInfo = utility.EmailRepository.GetEmailInfoById(emailId)
                objEmailInfo.EmailBody = common.MergeUserEmailBody(User, objEmailInfo, planGuruUrl)
                common.SendUserEmail(User, objEmailInfo)
                Logger.Log.Info("You have been added as a user - SendEmail Ended")
                Logger.Log.Info(String.Format("Email Sent Successfully to User: {0} for EmailId: {1}", User.UserId, emailId))
            Catch ex As Exception
                TempData("ErrorMessage") = String.Concat("Unable to Send Email -", ex.Message)
                Logger.Log.Error(String.Format("Unable to Send Email, UserId - {0} with Message- {1} " + Environment.NewLine + "Stack Trace: {2} ", User.UserId, ex.Message, ex.StackTrace))
            Finally
                Logger.Log.Info(String.Format("User SendEmail Execution Ended"))
            End Try
        End Sub

        Function Login(returnUrl As String) As ActionResult
            ViewBag.ReturnUrl = returnUrl
            Return View()
        End Function
    End Class
End Namespace