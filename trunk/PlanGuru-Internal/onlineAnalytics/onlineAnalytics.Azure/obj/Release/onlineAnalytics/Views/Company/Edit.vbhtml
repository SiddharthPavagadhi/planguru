﻿@ModelType onlineAnalytics.Company
@Imports System.Collections.Generic
@Imports MvcCheckBoxList.Model
@Imports  onlineAnalytics
@Code
    ViewData("Title") = "Edit Company"
    Layout = "~/Views/Shared/AnalyticsMaster.vbhtml"
    Dim ua As New UserAccess
    If Not (Session("UserAccess") Is Nothing) Then
        ua = DirectCast(Session("UserAccess"), UserAccess)
    End If
End Code
<script src="@Url.Content("~/Scripts/jquery.validate.min.js")" type="text/javascript"></script>
<script src="@Url.Content("~/Scripts/jquery.validate.unobtrusive.min.js")" type="text/javascript"></script>
<div style="float: right; padding-top: 5px;">
</div>
@*<div class="bredcurm-panel">
    <div class="bredcrumb-nav">
        <label>
            Company</label></div>
</div>*@
<div class="headerTable" style="float: right">
    <table>
        <tr>
            <td align="left" width="70%">
                Edit Company
            </td>
            <td align="right" width="12%" style="padding-left: 20px;">
                <a href='@Url.Action("Index", "Company")' class="button_example">Company List</a>
            </td>
        </tr>
    </table>
</div>
<br />
<br />
<div class="center-panel">
    @Using Html.BeginForm("Edit", "Company")
        @Html.AntiForgeryToken()   
        @Html.ValidationSummary(True)    
     
        @<fieldset>
            <div style="width: 100;">
                <div style="float: left; width: 33%;">
                    <ol class="round">
                        <li>
                            @Html.LabelFor(Function(m) m.CompanyName)<br />
                            @Html.TextBoxFor(Function(m) m.CompanyName)<br />
                            @Html.ValidationMessageFor(Function(m) m.CompanyName)
                        </li>
                        <li>
                            @Html.LabelFor(Function(m) m.FiscalMonthStart)<br />
                            @Html.DropDownList("FiscalMonthStart", DirectCast(ViewBag.Months, SelectList), "Select Month", New With {.Class = "select", .Style = "width:206px;"})<br />
                            @Html.ValidationMessageFor(Function(m) m.FiscalMonthStart)
                        </li>
                    </ol>
                </div>
                <div style="float: left; width: 33%;">
                    <ol class="round">
                        <li>
                            @Html.LabelFor(Function(m) m.ContactFirstName)<br />
                            @Html.TextBoxFor(Function(m) m.ContactFirstName)<br />
                            @Html.ValidationMessageFor(Function(m) m.ContactFirstName)
                        </li>
                        <li>
                            @Html.LabelFor(Function(m) m.ContactLastName)<br />
                            @Html.TextBoxFor(Function(m) m.ContactLastName)<br />
                            @Html.ValidationMessageFor(Function(m) m.ContactLastName)
                        </li>
                    </ol>
                </div>
                <div style="float: left; width: 33%;">
                    <ol class="round">
                        <li>
                            @Html.LabelFor(Function(m) m.ContactEmail)<br />
                            @Html.TextBoxFor(Function(m) m.ContactEmail)<br />
                            @Html.ValidationMessageFor(Function(m) m.ContactEmail)
                        </li>
                        <li>
                            @Html.LabelFor(Function(m) m.ContactTelephone)<br />
                            @Html.TextBoxFor(Function(m) m.ContactTelephone)<br />
                            @Html.ValidationMessageFor(Function(m) m.ContactTelephone)
                        </li>
                    </ol>
                </div>
                 @If (ua.UserCompanyMapping) Then
                     @<div class="associate_user">
                        @Code
                     Dim htmlListInfo = New HtmlListInfo(HtmlTag.table, 10, New With {.class = "styled_list"}, TextLayout.Default, TemplateIsUsed.No)
                     Dim checkBoxAtt = New With {.class = "styled_checkbox"}
                            End Code
                        <ol class="round">
                            <li>
                                @Html.LabelFor(Function(m) m.PostedUsers.UserIds)
                                @Html.CheckBoxListFor(Function(u) u.PostedUsers.UserIds, Function(u) u.AvailableUsers, Function(u) u.Id, Function(u) u.Name, Function(u) u.SelectedUsers, checkBoxAtt, htmlListInfo, Nothing, Function(u) u.Tag)
                                @Html.ValidationMessageFor(Function(m) m.PostedUsers.UserIds)
                            </li>
                        </ol>
                    </div>
                 End If
                
                <div class="input-form">
                    <input id="Submit" type="submit" value="Update company details" class="secondbutton_example" />
                    <input id="Cancel" type="button" value="Cancel" class="secondbutton_example" data='@Url.Action("Index")' />
                </div>
                <div style="float: left; margin-left: 20px;">
                    @If Not (DirectCast(TempData("Message"), String) Is Nothing) Then
                        @<label class="success">@TempData("Message").ToString()
                        </label>                         
                    End If
                    @If Not (DirectCast(TempData("ErrorMessage"), String) Is Nothing) Then
                        @<label class="error">@TempData("ErrorMessage").ToString()
                        </label>                         
                    End If
                </div>
                @Html.HiddenFor(Function(m) m.CompanyId)
            </div>
        </fieldset>                                  
    End Using
</div>
<script type="text/javascript" language="javascript">

    $(document).ready(function () {

        $('[user="SAU"]').attr("class", "styled_checkbox SAU");
        $('[user="CAU"]').attr("class", "styled_checkbox CAU");
        $('[user="CRU"]').attr("class", "styled_checkbox CRU");

        $('[user="SAU"]').parent().prepend('<img src="@Url.Content("~/Content/Images/SAU.png")" style="height:25px;margin:0 0 0 2px;"/><br/>');
        $('[user="CAU"]').parent().prepend('<img src="@Url.Content("~/Content/Images/CAU.png")" style="height:25px;margin:0 0 0 2px;"/><br/>');
        $('[user="CRU"]').parent().prepend('<img src="@Url.Content("~/Content/Images/CRU.png")" style="height:25px;margin:0 0 0 2px;"/><br/>');

        //Input Mask for landline phone number
        $("#ContactTelephone").mask("(999) 999-9999");
        //$("#FiscalMonthStart").mask("9?9");


        $("#Cancel").click(function (event) {
            //event.preventDefault();          
            var url = $(this).attr('data');
            window.location = url;
        });

    });
</script>
