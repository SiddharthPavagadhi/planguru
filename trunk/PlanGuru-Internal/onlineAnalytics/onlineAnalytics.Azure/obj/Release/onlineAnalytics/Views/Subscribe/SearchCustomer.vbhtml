﻿@ModelType PagedList.IPagedList(Of onlineAnalytics.Customer)
@Code
    ViewData("Title") = "Search Customer"
    Layout = "~/Views/Shared/AnalyticsMaster.vbhtml"
    Dim index As Integer = 1
End Code
@*Search Criteria*@ @*<div class="bredcurm-panel">
    <div class="bredcrumb-nav">
        <label>
            Search</label></div>
</div>*@
<div class="headerTable" style="float: right">
    <table>
        <tr>
            <td align="left" width="70%">
                Search Customer
            </td>
            <td align="right" width="12%" style="padding-left: 20px;">
                <img src='@Url.Content("~/Content/Images/1369484457_users.png")' class="usersIcon"/>
                <a href='@Url.Action("Index", "User")' class="button_example">Users List</a>
            </td>
        </tr>
    </table>
</div>
@Using Html.BeginForm("SearchCustomer", "Subscribe")
    @Html.AntiForgeryToken()   
    @Html.ValidationSummary(True)      
    @<fieldset style="float: left;">
        @Html.Partial("_SearchCustomerPartial", New onlineAnalytics.Customer)
        <div style="float: left; margin-left: 20px;">
            @If Not (DirectCast(TempData("Message"), String) Is Nothing) Then
                @<label class="success">@TempData("Message").ToString()
                </label>                         
    End If
            @If Not (DirectCast(TempData("ErrorMessage"), String) Is Nothing) Then
                @<label class="error">@TempData("ErrorMessage").ToString()
                </label>                         
    End If
        </div>
        <div class="CSSTableGenerator">
            <table>
                <thead>
                    <tr>
                        <th style="width: 15%">
                            Customer No
                        </th>
                        <th style="width: 20%">
                            Customer Full Name
                        </th>
                        <th style="width: 20%">
                            Telephone Number
                        </th>
                        <th style="width: 15%">
                            Company Name
                        </th>
                    </tr>
                </thead>
                @For Each item In Model
        Dim currentItem = item
        Dim cssClass As String = If(index Mod 2 = 0, "even", "")
                  
                    @<tr class='@cssClass'>
                        <td>
                            @Html.DisplayFor(Function(modelItem) currentItem.CustomerId)
                        </td>
                        <td>
                            @Html.DisplayFor(Function(modelItem) currentItem.CustomerFullName)
                        </td>
                        <td>
                            @Html.DisplayFor(Function(modelItem) currentItem.ContactTelephone)
                        </td>
                        <td>
                            @Html.DisplayFor(Function(modelItem) currentItem.CustomerCompanyName)
                        </td>
                    </tr>
        index += 1
    Next
            </table>
        </div>
    </fieldset>
   
   
End Using
@*View Result *@ @*<fieldset>
    <legend>Search Customer Result</legend>
    <br />*@ @*<table class="table-layout">
        <tr>
            <th class="left" style="width: 20%">
                Customer No
            </th>
            <th class="left" style="width: 20%">
                Customer Full Name
            </th>
            <th class="left" style="width: 20%">
                Telephone Number
            </th>
            <th class="center" style="width: 15%">
                SAU Name
            </th>         
                      
        </tr>
        @For Each item In Model
            Dim currentItem = item
            @<tr>
                <td class="left">
                    @Html.DisplayFor(Function(modelItem) currentItem.CustomerId)
                </td>
                <td class="left">
                    @Html.DisplayFor(Function(modelItem) currentItem.CustomerFullName)
                </td>
                <td class="left">
                    @Html.DisplayFor(Function(modelItem) currentItem.ContactTelephone)
                </td>
                <td class="center">
                    @Html.DisplayFor(Function(modelItem) currentItem.SAUName)
                </td>               
                             
            </tr>
        Next
    </table>    
    @If Not (DirectCast(TempData("Message"), String) Is Nothing) Then
        @<label class="success">@TempData("Message").ToString()
        </label>                         
    End If
    @If Not (DirectCast(TempData("ErrorMessage"), String) Is Nothing) Then
        @<label class="error">@TempData("ErrorMessage").ToString()
        </label>                         
    End If*@ @*</fieldset>*@