﻿@ModelType  PagedList.IPagedList(Of onlineAnalytics.Customer)
@Imports  onlineAnalytics
@Code
    ViewData("Title") = "Index"
    Layout = "~/Views/Shared/AnalyticsMaster.vbhtml"
    Dim index As Integer = 1
    
    Dim ua As New UserAccess
    If Not (Session("UserAccess") Is Nothing) Then
        ua = DirectCast(Session("UserAccess"), UserAccess)
    End If
End Code

<div class="headerTable" style="float: right">
    <table>
        <tr>
            <td align="left" width="70%">
                Subscriber List
            </td>
            <td align="right" width="12%" class="add">
                <img src='@Url.Content("~/Content/Images/1369406210_plus.png")' class="plusIcon" />
                <a href='@Url.Action("Subscribe", "Subscribe")' class="button_example">Subscription</a>
            </td>
        </tr>
    </table>
</div>
<div class="center-panel">
    <div class="center-tablesection">
        <div class="CSSTableGenerator">
            <table>
                <thead><tr>
                    <th>
                        Customer Name
                    </th>
                    <th>
                        Email Id
                    </th>
                    <th>
                        Company
                    </th>
                    <th>
                        Plan
                    </th>
                    <th>
                        Quantity
                    </th>
                    <th>
                        Created On
                    </th>
                    @Code
                        If (ua.UpdateSubscription = True) Then
                        @<th>
                            Edit
                        </th>
                        End If
                    End Code
                    @Code
                        If (ua.CancelSubscription = True) Then
                        @<th>
                            Cancel
                        </th>
                        End If
                    End Code
                </tr></thead> 
                @Code 
                    For Each item In Model
                        Dim currentItem = item
                        Dim cssClass As String = If(index Mod 2 = 0, "even", "")
                                          
                    @<tr class='@cssClass'>
                        <td>
                            @Html.DisplayFor(Function(modelItem) currentItem.CustomerFullName)
                        </td>
                        <td>
                            @Html.DisplayFor(Function(modelItem) currentItem.CustomerEmail)
                        </td>
                        <td>
                            @Html.DisplayFor(Function(modelItem) currentItem.CustomerCompanyName)
                        </td>
                        <td>
                            PlanGuru Analytics
                        </td>
                        <td>
                            @Html.DisplayFor(Function(modelItem) currentItem.Quantity)
                        </td>
                        <td>
                            @Code
                                    Dim createdon As String = String.Format("{0:MMM dd, yyyy}", currentItem.CreatedOn)
                            End Code
                            @Html.DisplayFor(Function(modelItem) createdon)
                        </td>
                        @Code
                                If (ua.UpdateSubscription = True) Then
                            @<td>
                                <a href='@Url.Action("EditSubscription", "Subscribe", New With {.AccountCode = currentItem.CustomerId})'>
                                    <img alt="Edit Subscription" src='@Url.Content("~/Content/Images/edit.png")' title="Edit Subscription" />
                                </a>
                            </td>
                                End If
                        End Code
                        @Code
                                If (ua.CancelSubscription = True) Then
                            @<td>
                                <a id='@currentItem.CustomerId' class="Delete" href="#" data='@Url.Action("CancelSubscription", "Subscribe", New With {.AccountCode = currentItem.CustomerId})' >
                                    <img alt="Edit Subscription" src='@Url.Content("~/Content/Images/delete.png")' title="Edit Subscription" />
                                </a>
                            </td>
                                End If
                        End Code
                    </tr>                                  
                        index += 1
                    Next
                End Code
            </table>
        </div>
         <div class="pagination-panel">
                @If Model.HasPreviousPage Then
                    @<a href='@Url.Action("Index", "Subscribe", New With {.page = Model.PageNumber - 1}) ' class="prevous">Previous</a>
                Else
                    @<a class="no-prevous">Previous</a>
                End If
                <div class="pagination-list">
                    <ul>
                        @For index = 1 To Model.PageCount
                            @<li><a href='@Url.Action("Index", "Subscribe", New With {.page = index})' >@index</a></li>
                        Next
                    </ul>
                </div>
                @If Model.HasNextPage Then
                    @<a href='@Url.Action("Index", "Subscribe", New With {.page = Model.PageNumber + 1}) ' class="next">Next</a>
                Else
                    @<a class="no-next">Next</a>
                End If
            </div>
        @If Not (DirectCast(TempData("Message"), String) Is Nothing) Then
            @<label class="success">@TempData("Message").ToString()
            </label>                         
        End If
        @If Not (DirectCast(TempData("ErrorMessage"), String) Is Nothing) Then
            @<label class="error">@TempData("ErrorMessage").ToString()
            </label>                         
        End If
    </div>
</div>
<script type="text/javascript" language="javascript">

    $(".Delete").click(function (event) {
        IsSessionAlive();
        event.preventDefault();
        var url = $(this).attr('data');

        $.msgBox({
            title: "Confirm",
            content: "Are you sure you want to cancel this Subscription?",
            type: "confirm",
            buttons: [{ value: "Yes" }, { value: "No"}],
            success: function (result) {
                if (result == "Yes") {
                    window.location = url;
                }
            }
        });

    }); 
</script>
