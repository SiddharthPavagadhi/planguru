﻿<!DOCTYPE html>
<html>
<head>
    <!-- to get the absolute path for the images into alert used inside the jquery.msgBox.js file-->
    <script type="text/javascript" language="javascript">
        var msgBoxImagePath = '@url.Content("~/Content/MsgBoxImages/")';
    </script>
    <title>@ViewData("Title")</title>
   
    <link href="@Url.Content("~/Content/jquery-ui-1.10.0.custom.css")" rel="stylesheet" type="text/css" />
    <link href="@Url.Content("~/Content/Site.css")" rel="stylesheet" type="text/css" />
    <link href="@Url.Content("~/Content/msgBoxLight.css")" rel="stylesheet" type="text/css" />
    <script src="@Url.Content("~/Scripts/jquery-1.9.1.min.js")" type="text/javascript"></script>
    <script src="@Url.Content("~/Scripts/modernizr-1.7.min.js")" type="text/javascript"></script>
    <script src="@Url.Content("~/Scripts/jquery-ui-1.10.0.custom.js")" type="text/javascript"></script>
    <script src="@Url.Content("~/Scripts/jquery.msgBox.js")" type="text/javascript"></script>
     
</head>
<body>
    <style type="text/css">
        #loginPanel tr td a
        {
            color: White;
            cursor: pointer;
            text-align: center;
            width: auto;
            height: auto;
            text-decoration: none;
            padding: 3px 20px; /*background-color: #107FC7; */
            background-color: #4D90FE;
            background-image: -moz-linear-gradient(center top , #4D90FE, #4787ED);
            background-repeat: repeat-x;
        }
        
        #loginPanel tr td a:hover
        {
            /*background-color: #95B8D3;*/
            background-color: #0072BB;
            background-image: -moz-linear-gradient(center top , #0072BB, #0072BB);
            background-repeat: repeat-x;
            cursor: pointer;
            color: White;
            text-align: center;
            cursor: pointer;
            width: auto;
            padding: 3px 20px;
            height: auto !important;
        }
    </style>
    <table style="width: 100%; border: none;" id="loginPanel">
        <tr>
            <td align="left">
                <img  style="margin-left: -33px;" src="@Url.Content("~/Content/Images/logo-planguru-analytics.png")" alt="Analytics logo" />
            </td>
            <td align="right">
                <div>
                    User Name: @Html.TextBox("userid", "", New With {.Style = "width: 100px;"})
                    Password: @Html.TextBox("password", "", New With {.type = "password", .Style = "width: 100px;"})
                    <input type="button" value="Login" id="login" class="button_example" />
                </div>
            </td>
        </tr>
    </table>
    <div class="nav-panel" id="menucontainer">
        <ul>
            <li><a href='@Url.Action("", "")'>How it works</a></li>
           @* <li><a href='@Url.Action("SampleDashboard", "Home")'>Sample Dashboard</a></li>
            <li><a href='@Url.Action("RE", "Home")'>Sample Revenue and Expenses View</a></li>*@
            <li><a href='@Url.Action("", "")'>Pricing</a></li>
            <li><a href='@Url.Action("Subscribe", "Home")'>Sign Up</a></li>
        </ul>
    </div>   
    <div>
        @RenderBody()
    </div>
    @code
        Dim sessionExpired = Request.QueryString("session")
        If (sessionExpired = "-1") Then
        @<label class="timeout">Your session has expired, Please login again.
        </label>    
        End If
    End Code
    <div id="loading" style="text-align: center;">
    </div>
    <div id="validation" style="text-align: center;">
    </div>
</body>
</html>
<script type="text/javascript" language="javascript">
    var pathArray = window.location.href.split('/');
    var url;
    if (pathArray.length > 2) {
        var protocol = pathArray[0];
        var host = pathArray[2];
        var app = pathArray[3];
        url = protocol + '//' + host + "/" + app;

    } else {
        var protocol = pathArray[0];
        var host = pathArray[2];
        url = protocol + '//' + host;
    }

    var theDialog = $('#loading').dialog({
        open: function (event, ui) { $(".ui-dialog-titlebar-close").hide(); },
        center_modal: true,
        autoOpen: false,
        modal: true,
        close: false,
        closeOnEscape: false,
        width: 200,
        modal: true,
        minHeight: 10,
        show: 'fade',
        hide: 'fade',
        resizable: 'false'        
    });

    var validationDialog = $('#validation').dialog({
        title: "Login",
        open: function (event, ui) { $(".ui-dialog-titlebar-close").hide(); },
        center_modal: true,
        autoOpen: false,
        modal: true,
        close: true,
        closeOnEscape: true,
        width: 500,       
        minHeight: 10,
        show: 'fade',
        hide: 'fade',
        resizable: 'false',       
        buttons: { "OK": function () {
            $(this).dialog("close");
        }
        }
    });


    $(document).ready(function (event) {

        $("#updatesetup").addClass("secondbutton_example").removeClass("ui-button ui-widget ui-state-default ui-corner-all");
        $("#updatesetup").hover(function () {
            $(this).removeClass("ui-state-hover");
            $(this).addClass("secondbutton_example");
        });
        $("#updatesetup").click(function () {
            $(this).removeClass("ui-state-focus");
            $(this).addClass("secondbutton_example");
        });

    });

    $('#password').keypress('click', function (e) {

        if (e.which == 13) {
            $('#login').trigger("click");
        }

    });

    $('#login').click(function (event) {

        event.preventDefault();

        if ($('#userid').val() == "") {

            $('#userid').focus();
            $.msgBox({
                title: "User",
                content: "Enter you Username.",
                type: "Info",
                modal: true,
                buttons: [{ value: "Ok"}]
            });
        }
        else if ($('#password').val() == "") {

            $('#userid').focus();
            $.msgBox({
                title: "User",
                content: "Enter your Password.",
                type: "Info",
                modal: true,
                buttons: [{ value: "Ok"}]
            });
        }
        else if ($('#userid').val() != "" && $('#password').val() != "") {

            $.ajax({
                type: "GET",
                url: '@Url.Action("Authenticate", "User")',
                global: false,
                cache: false,
                data: { userName: $('#userid').val(), password: $('#password').val(), returnUrl: url + "/" },
                dataType: "json",
                beforeSend: function () {
                    var imgSrc = '@Url.Content("~/Content/Images/loading.gif")';

                    $(".ui-dialog-titlebar").hide();
                    $('#loading').html("Please wait.... <img src=" + imgSrc + " />");
                    theDialog.dialog('open');
                },
                success: function (msg) {

                    theDialog.dialog('close');
                    if (msg.Success) {

                        window.location = msg.Url;
                    }

                    if (msg.Error) {

                        $.msgBox({
                            title: "User",
                            content: msg.Error,
                            type: "Error",
                            buttons: [{ value: "Ok"}]
                        });
                    }



                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {

                    theDialog.dialog('close');
                    //alert(XMLHttpRequest.responseText); alert(textStatus); alert(errorThrown);


                    $(".ui-dialog-titlebar").show();
                    $(".ui-dialog-titlebar").text("Login");
                    //$('#validation').html(msg);
                    $('#validation').html(textStatus + ":" + errorThrown);
                    validationDialog.dialog('open');
                }
            });
        }
    });
</script>
