﻿@Imports Farpoint.Web.Spread
@Imports Farpoint.Mvc.Spread
@Code
    ViewData("Title") = "Dashboard Setup"
    Layout = "~/Views/Shared/_Layout.vbhtml"
End Code
<script type="text/javascript">


    $(function () {
        $("#tabs").tabs();

        $('#intChartType').change(function () {
            var selectedindex = $('#intChartType').get(0).selectedIndex;
            if (selectedindex == 2) {
                $('#ShowTrend').show();
                $('#Smoothed').show();
            }
            else {
                $('#ShowTrend').hide();
                $('#Smoothed').hide();
            }
        });

        $(document).ready(function () {

            $("input:checkbox").each(function (index) {
                $("<label>").attr("for", this.id = "checkbox" + index + 1)
                .attr("class", "styled_checkbox dashboard")
                .insertAfter(this);
            });

            //hide detail rows  
            $("#tabs").removeClass("ui-corner-all");
            var spread = document.getElementById("spdRE");
            var rc = spread.GetRowCount();
            var showclass = false
            for (var i = 0; i < rc; i++) {
                var cellval = spread.GetValue(i, 2);
                if (cellval == "Detail") {
                    if (showclass == false) {
                        spread.Rows(i).style.display = "none";
                    }
                }
                if (cellval == "Heading") {
                    var cellvalue = spread.GetValue(i, 0);
                    if (cellvalue == "-") {
                        showclass = true
                        spread.Rows(i).style.display = "table-row";
                    }
                    else {
                        showclass = false
                        spread.Rows(i).style.display = "none";
                    }
                }
            }

        });

        window.onload = function () {
            var spread = document.getElementById("spdRE");
            if (document.all) {
                if (spread.addEventListener) {
                    spread.addEventListener("ActiveCellChanged", cellChanged, false);
                }
                else {
                    spread.onActiveCellChanged = cellChanged;
                }
            }
            else {
                spread.addEventListener("ActiveCellChanged", cellChanged, false);
            }
        }

        function cellChanged(event) {
            if (event.col == 0) {
                var spread = document.getElementById("spdRE");
                var cellval = spread.GetValue(event.row, 0);
                var rc = spread.GetRowCount();
                if (cellval == "-") {
                    for (var i = event.row; i < rc; i++) {
                        var rowtype = spread.GetValue(i, 2);
                        if (rowtype == "Total") {
                            cell = document.getElementById("spdRE").GetCellByRowCol(i, 0);
                            cell.removeAttribute("FpCellType");
                            spread.SetValue(i, 0, "+", false);
                            cell.setAttribute("FpCellType", "readonly");
                            spread.Rows(i).style.display = "table-row";
                            break;
                        }
                        else if (rowtype == "Heading") {
                            cell = document.getElementById("spdRE").GetCellByRowCol(i, 0);
                            cell.removeAttribute("FpCellType");
                            spread.SetValue(i, 0, "+", false);
                            cell.setAttribute("FpCellType", "readonly");
                            spread.Rows(i).style.display = "none";
                        }
                        else {

                            spread.Rows(i).style.display = "none";
                        }
                    }
                }
                else if (cellval == "+") {
                    for (var i = event.row; i > -1; i--) {
                        var rowtype = spread.GetValue(i, 2);
                        if (rowtype == "Total") {
                            cell = document.getElementById("spdRE").GetCellByRowCol(i, 0);
                            cell.removeAttribute("FpCellType");
                            spread.SetValue(i, 0, "", false);
                            cell.setAttribute("FpCellType", "readonly");
                            if ($('#blnUseFilter').is(':checked')) {
                                spread.Rows(i).style.display = "none";
                            }
                        }
                        else if (rowtype == "Heading") {
                            cell = document.getElementById("spdRE").GetCellByRowCol(i, 0);
                            cell.removeAttribute("FpCellType");
                            spread.SetValue(i, 0, "-", false);
                            cell.setAttribute("FpCellType", "readonly");
                            spread.Rows(i).style.display = "table-row";
                            break
                        }

                        else {
                            spread.Rows(i).style.display = "table-row";
                        }
                    }
                }
                spread.SetActiveCell(event.row, 1)
            }
        }

    });

     
</script>
<div style="font-size: 11px;">
    <div class="bredcurm-panel">
        <div class="bredcrumb-nav">
            <label>
                Select Dashboard Item</label></div>
    </div>
</div>
<br />
<div id="tabs" style="font-size: 13px; width: 99.4%;">
    <ul>
        <li><a href="#tabs-1">Revenue and expenses</a></li>
        <li><a href="#tabs-2">Assets, liabilties and capital</a></li>
        <li><a href="#tabs-3">Cash flow</a></li>
        <li><a href="#tabs-3">Financial ratios</a></li>
        <li><a href="#tabs-3">Other metrics</a></li>
    </ul>
    <div id="tabs-1">
        <div id="REspread">
            @Html.FpSpread("spdRE", Sub(x)
                                        x.RowHeader.Visible = False
                                        x.Height = 300
                                        x.Width = 800
                                              
                                              
                                    End Sub)
        </div>
    </div>
    <div id="tabs-2">
        <div id="BSspread">
            @Html.FpSpread("spdBS", Sub(x)
                                        x.RowHeader.Visible = False
                                        x.Height = 300
                                        x.Width = 350
                                              
                                    End Sub)
        </div>
    </div>
</div>
<div>
    <div style="margin-top: 10px;">
        <span>Format</span>
        @Html.DropDownList("intFormat", DirectCast(ViewData("Formats"), SelectList), New With {.Class = "select", .Style = "width:200px;margin-left:18px;"})
    </div>
    <div style="padding-top: 10px;">
        @Html.Label("Chart type")
        @Html.DropDownList("intChartType", DirectCast(ViewData("ChartType"), SelectList), New With {.Class = "select"})
    </div>
    <div id="ShowTrend" style="margin-top: 10px; margin-left: 10px; display: none;">
        @Html.CheckBox("blnShowTrend", New With {.Style = "border: 1px solid Silver; background-color:Silver; margin-right: 5px;"})
        @Html.Label("Show trend")
    </div>
    <div id="Smoothed" style="margin-top: 10px; margin-left: 10px; display: none;">
        @Html.CheckBox("blnSmoothed", New With {.Style = "border: 1px solid Silver; background-color:Silver; margin-right: 5px;"})
        @Html.Label("Smooth line")
    </div>
</div>
<br />
<div id="AddDashboardItem">
    <input id="causepost" type="submit" value="Add Dashboard Item" class="secondbutton_example" />
</div>
