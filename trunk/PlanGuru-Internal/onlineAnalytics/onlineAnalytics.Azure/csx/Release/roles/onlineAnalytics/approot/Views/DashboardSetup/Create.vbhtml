﻿@ModelType onlineAnalytics.Dashboard
@Imports Farpoint.Web.Spread
@Imports Farpoint.Mvc.Spread
@Code
    ViewData("Title") = "Dashboard Setup"
    Layout = "~/Views/Shared/AnalyticsMaster.vbhtml"
End Code
<script src="@Url.Content("~/Scripts/jquery.validate.min.js")" type="text/javascript"></script>
<script src="@Url.Content("~/Scripts/jquery.validate.unobtrusive.min.js")" type="text/javascript"></script>
<div class="headerTable" style="float: right">
    <table>
        <tr>
            <td align="left" width="70%">
                Add Dashboard Item
            </td>
            <td align="right" width="15%" class="add">
                <a href='@Url.Action("Index", "DashboardSetup")' class="button_example">Dashboard Item
                    List</a>
            </td>
        </tr>
    </table>
</div>
<div style="margin-bottom: 20px;">    
    @If Not (DirectCast(TempData("Message"), String) Is Nothing) Then
        @<label class="success">@TempData("Message").ToString()
        </label>                         
    End If
    @If Not (DirectCast(TempData("ErrorMessage"), String) Is Nothing) Then
        @<label class="error">@TempData("ErrorMessage").ToString()
        </label>                         
    End If    
    <div style="font-size: 13px; display: -moz-box; width: 99.9%; border: 1px solid #AAAAAA;">
        @Using Html.BeginForm("Create", "DashboardSetup")
            @Html.AntiForgeryToken()   
            @Html.ValidationSummary(True)       
            @<div>
                @Html.ValidationMessageFor(Function(m) m.AccountId, Nothing, New With {.style = "margin-left:20px;font-family:Segoe UI;font-size: 0.85em;"})
                @Html.HiddenFor(Function(m) m.AccountId)
                 @Html.HiddenFor(Function(m) m.AcctTypeId)
            </div>
            @<div id="tabs" style="float: left; font-size:11px; width: 77%; min-height: 450px; height: auto;">
                <ul>
                    <li><a href="#tabs-1">Revenue and expenses</a></li>
                    <li><a href="#tabs-2">Assets, liabilties and capital</a></li>
                    <li><a href="#tabs-3">Cash flow</a></li>
                    <li><a href="#tabs-4">Financial ratios</a></li>
                    <li><a href="#tabs-5">Other metrics</a></li>
                </ul>
                <div id="tabs-1">
                    <div id="REspread">
                        @Html.FpSpread("spdRE", Sub(x)
                                                    x.RowHeader.Visible = False
                                                    x.Height = 380
                                                    x.Width = 800
                                              
                                              
                                                End Sub)
                    </div>
                </div>
                <div id="tabs-2">
                    <div id="BSspread">
                        @Html.FpSpread("spdBS", Sub(x)
                                                    x.RowHeader.Visible = False
                                                    x.Height = 380
                                                    x.Width = 800
                                              
                                                End Sub)
                    </div>
                </div>
                <div id="tabs-3">
                    <div id="CFspread">
                        @Html.FpSpread("spdCF", Sub(x)
                                                    x.RowHeader.Visible = False
                                                    x.Height = 380
                                                    x.Width = 800                                              
                                                End Sub)
                    </div>
                </div>
                <div id="tabs-4">
                    <div id="FRspread">                    
                        @Html.FpSpread("spdFR", Sub(x)
                                                         x.RowHeader.Visible = False
                                                         x.Height = 380
                                                         x.Width = 800
                                                     End Sub)
                    </div>
                </div>
                <div id="tabs-5">
                    <div id="OMspread">                    
                        @Html.FpSpread("spdOM", Sub(x)
                                                    x.RowHeader.Visible = False
                                                    x.Height = 380
                                                    x.Width = 800                                              
                                                End Sub)
                    </div>
                </div>
            </div>
            @<div style="float: right; width: 22%;">
                <fieldset style="min-height: 441px; height: auto; border: none;">
                    <div style="float: right;">
                        <ol class="round">
                            <li>
                                @Html.LabelFor(Function(m) m.ChartFormatId)<br />
                                @Html.DropDownList("ChartFormatId", DirectCast(ViewBag.Formats, SelectList), "Select Chart Format", New With {.Class = "select", .style = "width:200px;"})<br />
                                @Html.ValidationMessageFor(Function(m) m.ChartFormatId)
                            </li>
                            <li>
                                @Html.LabelFor(Function(m) m.ChartTypeId)<br />
                                @Html.DropDownList("ChartTypeId", DirectCast(ViewBag.ChartType, SelectList), "Select Chart Type", New With {.Class = "select"})<br />
                                @Html.ValidationMessageFor(Function(m) m.ChartTypeId)
                            </li>                           
                            <li>
                                @Html.LabelFor(Function(m) m.ShowAsPercent)
                                @Html.CheckBoxFor(Function(m) m.ShowAsPercent, New With {.class = "styled_checkbox"})<br />
                                @Html.ValidationMessageFor(Function(m) m.ShowAsPercent)
                            </li>
                            <li id="ShowTrend">
                                @Html.LabelFor(Function(m) m.ShowTrendline)
                                @Html.CheckBoxFor(Function(m) m.ShowTrendline, New With {.class = "styled_checkbox"})<br />
                                @Html.ValidationMessageFor(Function(m) m.ShowTrendline)
                            </li>                           
                            <li>
                                <input id="causepost" type="submit" value="Add Dashboard Item" class="secondbutton_example" />
                            </li>
                        </ol>
                    </div>
                </fieldset>
                @* <div id="ShowTrend" style="margin-top: 10px; margin-left: 10px;">
                        @Html.CheckBox("blnShowTrend", New With {.Style = "border: 1px solid Silver; background-color:Silver; margin-right: 5px;"})
                        @Html.Label("Show trend")
                    </div><div id="Smoothed" style="margin-top: 10px; margin-left: 10px;">
                        @Html.CheckBox("blnSmoothed", New With {.Style = "border: 1px solid Silver; background-color:Silver; margin-right: 5px;"})
                        @Html.Label("Smooth line")
                    </div>
                    <div id="AddDashboardItem" style="float: left;">
                        <input id="causepost" type="submit" value="Add Dashboard Item" class="secondbutton_example" />
                    </div>*@
            </div>            
        End Using        
    </div>
</div>
<script type="text/javascript">

    var selectedElement = "";
   
    $(".success").attr("style", "margin:0;padding: 10px 20px 10px 50px;");
    $(".error").attr("style", "margin:0;padding: 10px 20px 10px 50px;");

    $("#ShowTrend").hide();   

    function DSChkBoxClicked(checkedElement, selectedAccount, selectedAcctType) {
        var chkElement = "#" + checkedElement
        
        if ($(chkElement).is(':checked')) {
            Checked(chkElement, selectedAccount, selectedAcctType);
        }
        else {
            selectedElement = ""; $("#AccountId").val(''); $("#AcctTypeId").val(''); 
       }
    }

    function Checked(chkElement, selectedAccount, selectedAcctType) { 
        if ($("#AccountId").val() == "" || $("#AccountId").val() == 0) {
            selectedElement = chkElement;
            $("#AccountId").val(selectedAccount);
            $("#AcctTypeId").val(selectedAcctType);
        }
        else {

            $.msgBox({
                title: "Info",
                content: "Only one item can be included in a dashboard chart.",
                type: "Info",
                buttons: [{ value: "Ok"}]
            });

            $(chkElement).attr('checked', false);
        }
    }

    $(function () {
        $("#tabs").tabs();
        $("#tabs").attr("class", "ui-tabs ui-widget");

        $('#ChartTypeId').change(function () {

            var selectedvalue = $('#ChartTypeId option:selected').text().toLowerCase();
            if (selectedvalue == "line") {
                $('#ShowTrend').show();
            }
            else {
                $('#ShowTrend').hide();
            }
        });

        $(document).ready(function () {


            $("div > input:checkbox").each(function (index) {
                $("<label>").attr("for", this.id = "checkbox" + index)
                .attr("class", "styled_checkbox dashboard")
                .insertAfter(this);
            });

            $("li > input:checkbox").each(function (index) {
                $("<label>").attr("for", this.id)
                .attr("class", "float-left chk-dashboard")
                .insertAfter(this);
            });

            $("#tabs").removeClass("ui-corner-all");
            $("#footerpanel").removeClass("plangurufooter").addClass("plangurufooterfordashboard");
            IsSessionAlive();
            //hide detail rows  
            var spread = document.getElementById("spdRE");
            setRow(spread);

            var spread = document.getElementById("spdBS");
            setRow(spread);

            var spread = document.getElementById("spdCF");
            setRow(spread);

            var spread = document.getElementById("spdFR");
            setRow(spread);

            var spread = document.getElementById("spdOM");
            setRow(spread);

        });

        function setRow(spread) {

            var rc = spread.GetRowCount();
            var showclass = false
            for (var i = 0; i < rc; i++) {
                var cellval = spread.GetValue(i, 2);
                if (cellval == "Detail") {
                    if (showclass == false) {
                        spread.Rows(i).style.display = "none";
                    }
                }
                if (cellval == "Heading") {
                    var cellvalue = spread.GetValue(i, 0);
                    if (cellvalue == "-") {
                        showclass = true
                        spread.Rows(i).style.display = "table-row";
                    }
                    else {
                        showclass = false
                        spread.Rows(i).style.display = "none";
                    }
                }
            }
        }

        window.onload = function () {
            var spreadRE = document.getElementById("spdRE");
            var spreadBS = document.getElementById("spdBS");
            var spreadCF = document.getElementById("spdCF");
            var spreadFR = document.getElementById("spdFR");
            var spreadOM = document.getElementById("spdOM");

            tabEventListener(spreadRE);
            tabEventListener(spreadBS);
            tabEventListener(spreadCF);
            tabEventListener(spreadFR);
            tabEventListener(spreadOM);
        }

        function tabEventListener(spread) {
            if (document.all) {
                if (spread.addEventListener) {
                    spread.addEventListener("ActiveCellChanged", cellChanged, false);
                }
                else {
                    spread.onActiveCellChanged = cellChanged;
                }
            }
            else {
                spread.addEventListener("ActiveCellChanged", cellChanged, false);
            }
        }

        function cellChanged(event) {
            //IsSessionAlive();
            if (event.col == 0) {

                var spread = document.getElementById(event.target.id);  //document.getElementById("spdRE");
                var cellval = spread.GetValue(event.row, 0);
                var rc = spread.GetRowCount();

                if (cellval == "-") {
                    for (var i = event.row; i < rc; i++) {
                        var rowtype = spread.GetValue(i, 2);
                        if (rowtype == "Total") {
                            cell = document.getElementById(event.target.id).GetCellByRowCol(i, 0);
                            cell.removeAttribute("FpCellType");
                            spread.SetValue(i, 0, "+", false);
                            cell.setAttribute("FpCellType", "readonly");
                            spread.Rows(i).style.display = "table-row";
                            break;
                        }
                        else if (rowtype == "Heading") {
                            cell = document.getElementById(event.target.id).GetCellByRowCol(i, 0);
                            cell.removeAttribute("FpCellType");
                            spread.SetValue(i, 0, "+", false);
                            cell.setAttribute("FpCellType", "readonly");
                            spread.Rows(i).style.display = "none";
                        }
                        else {

                            spread.Rows(i).style.display = "none";
                        }
                    }
                }
                else if (cellval == "+") {
                    for (var i = event.row; i > -1; i--) {
                        var rowtype = spread.GetValue(i, 2);
                        if (rowtype == "Total") {
                            cell = document.getElementById(event.target.id).GetCellByRowCol(i, 0);
                            cell.removeAttribute("FpCellType");
                            spread.SetValue(i, 0, "", false);
                            cell.setAttribute("FpCellType", "readonly");
                            if ($('#blnUseFilter').is(':checked')) {
                                spread.Rows(i).style.display = "none";
                            }
                        }
                        else if (rowtype == "Heading") {
                            cell = document.getElementById(event.target.id).GetCellByRowCol(i, 0);
                            cell.removeAttribute("FpCellType");
                            spread.SetValue(i, 0, "-", false);
                            cell.setAttribute("FpCellType", "readonly");
                            spread.Rows(i).style.display = "table-row";
                            break
                        }

                        else {
                            spread.Rows(i).style.display = "table-row";
                        }
                    }
                }
                spread.SetActiveCell(event.row, 1)
            }
        }

    });     
</script>
