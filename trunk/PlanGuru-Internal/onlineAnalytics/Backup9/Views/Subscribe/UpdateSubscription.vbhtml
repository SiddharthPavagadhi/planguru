﻿@ModelType onlineAnalytics.Customer
@Code
    ViewData("Title") = "PlanGuru Analytics | Update Subscription"
    If Not Session("action") Is Nothing Then
        
        If (Session("action") = "SetDefaultAnalysis" or Session("action") = "GetAnalyses") Then
            Layout = Nothing
            Session.Remove("action")
        End If
    Else
        Layout = "~/Views/Shared/AnalyticsMaster.vbhtml"
    End If
End Code
<header>    
<link rel="stylesheet" href="@Url.Content("~/themes/default/recurly.css")" type="text/css" />
    <link rel="stylesheet" href="@Url.Content("~/themes/default/examples.css")" type="text/css" />
    </header>    
<h2>Subscription updated successfully</h2>

<div id="billing-info" class="section">
    <div id="recurly-subscribe">
        <h2 class="heading">
            Billing Information</h2>
        <!-- table: billing info -->
        <table style="width: 100%;">
            <tbody>
                <tr>
                    <td style="text-align:right;width:45%;font-weight:bold;">
                        Subscription Plan:
                    </td>
                    <td style="text-align:left;padding-left:20px;">
                        PlanGuru Analytics
                    </td>
                </tr>
                <tr>
                    <td scope="row" style="text-align:right;width:45%;font-weight:bold;">
                        First Name:
                    </td>
                    <td style="text-align:left;padding-left:20px;">
                        @Model.CustomerFirstName 
                    </td>
                </tr>
                <tr>
                    <th scope="row" style="float:right;margin-right:5px;">
                        Last Name:
                    </th>
                    <td style="text-align:left;padding-left:20px;">
                       @Model.CustomerLastName  
                    </td>
                </tr>
                <tr>
                    <th scope="row" style="float:right;margin-right:5px;">
                        Email Id:
                    </th>
                    <td style="text-align:left;padding-left:20px;">
                        @Model.CustomerEmail 
                    </td>
                </tr>
                <tr>
                    <th scope="row" style="float:right;margin-right:5px;">
                        Credit Card:
                    </th>
                    <td style="text-align:left;padding-left:20px;">
                        XXXX-XXXX-XXXX-@ViewBag.CreditCardLastFourDigit (@ViewBag.CreditCardType)
                    </td>
                </tr>
            </tbody>
        </table>
      <p class="receipt">
      </p>
    </div>
</div>
