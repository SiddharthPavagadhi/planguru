﻿@ModelType onlineAnalytics.SubscriptionPlanType 
@Code
    ViewData("Title") = "PlanGuru Analytics | Choose Subscription"
    Layout = "~/Views/Shared/_Layout.vbhtml"
End Code

    <link rel="stylesheet" href="@Url.Content("~/themes/default/examples.css")" type="text/css" />

<div id="billing-info" class="section">
    <div id="choose_subscription_container" >
        <h2 class="heading">
            PlanGuru Analytics Subscription Plan:</h2>
            
        <div style="width:90%;margin:0px 0px 40px 40px;display:inline-block;"><br />
            <span>Choose the subscription plan that best fits your needs. We offer two types of subscriptions.</span>
            <p>@Html.RadioButton("subscription_plan", 1, New With {.group = "subscription_plan", .style = "width:auto;"})Per User Plan</p>           
            <p><div style="margin:0 0 0 20px;">This plan starts at $19.95 a month and includes one user. Adding additional users will add $19.95 per user to the total monthly subscription cost.</div></p>
            <br />
            <p>@Html.RadioButton("subscription_plan", 2, New With {.group = "subscription_plan", .style = "width:auto;"})Premium Group Plan</p>
            <p><div style="margin:0 0 0 20px;">This plan includes 4 users and start at $59.95 a month. In addition to a lower per user cost this plan also makes our 
                "Branding" feature available. This feature allows you to customize your PlanGuru Analytics subscription with your own custom firm branding. 
                After the 4th user adding additional users will add 14.95 per user to the total monthly subscription cost.</div></p>            
            <br />
            <div class="float-right">
                    <input type="submit" value="Proceed to sign-up page" class="secondbutton_example" />                   
            </div>
        </div>       
    </div>
     
    <div style="float: left; margin-left: 20px;">
    @If Not (DirectCast(TempData("Message"), String) Is Nothing) Then
            @<label class="success">@TempData("Message").ToString()
            </label>                         
    End If
        @If Not (DirectCast(TempData("ErrorMessage"), String) Is Nothing) Then
            @<label class="error">@TempData("ErrorMessage").ToString()
            </label>                         
    End If
    </div>
</div>
<script type="text/javascript" language="javascript">

    $(".secondbutton_example").click(function () {

        var selectedPlan = $('input[name=subscription_plan]:checked').val();

        if (selectedPlan == undefined) {            

            $.msgBox({
                title: "Subscription Plan",
                content: "Please select plan to subscribe.",
                type: "info",
                buttons: [{ value: "Ok"}],
                success: function (result) {                    
                },
                afterShow: function () { $('[name=Ok]').focus(); }
            });

        }
        if (selectedPlan == 1) {
            window.location = '@Url.Action("Subscribe", "SelectSubscription", New With {.selectedPlan = 1})';
        }
        else if (selectedPlan == 2) { window.location = '@Url.Action("Subscribe", "SelectSubscription", New With {.selectedPlan = 2})'; }

    });

</script>