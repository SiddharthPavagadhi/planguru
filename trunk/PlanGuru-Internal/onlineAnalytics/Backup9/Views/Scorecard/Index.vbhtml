﻿@Imports Farpoint.Web.Spread
@Imports Farpoint.Mvc.Spread
@Imports  onlineAnalytics
@Code
    ViewData("Title") = "PlanGuru Analytics | Scorecard"
   
    If Not Session("action") Is Nothing Then
        
        If (Session("action") = "SetDefaultAnalysis" Or Session("action") = "GetAnalyses") Then
            Layout = Nothing
            Session.Remove("action")
        End If
    Else
        Layout = "~/Views/Shared/AnalyticsMaster.vbhtml"
    End If
    Layout = "~/Views/Shared/AnalyticsMaster.vbhtml"
End Code
@Code
    Dim UserInfo As User = Nothing
    Dim ua As New UserAccess
    
    Dim objSetup As New SetupCount
    
    If Not (Session("UserInfo") Is Nothing) Then
        UserInfo = DirectCast(Session("UserInfo"), User)
    End If
    
    If (Not ViewBag.Setup Is Nothing) Then
        objSetup = DirectCast(ViewBag.Setup, SetupCount)
    End If
    
End code
<div class="center-panel">
    @If Not (DirectCast(TempData("Message"), String) Is Nothing) Then
        @<label class="success">@TempData("Message").ToString()
        </label>                         
    End If
    @If Not (DirectCast(TempData("ErrorMessage"), String) Is Nothing) Then
        @<label class="error">@TempData("ErrorMessage").ToString()
        </label>                         
    End If
    @Code
        
        @Html.Hidden("countOfDataUploaded", objSetup.countDataUploadedOfSelectedAnalysis)        
        @<label id="validationMessage" class="info" style="display: none;">
            @If (objSetup.AanlysisCount > 0 And objSetup.selectedAnalysis = 0) Then
                @MvcHtmlString.Create("Please select company & one of the respective analysis from top-menu selection, to get the scorecard view.")
            ElseIf (objSetup.selectedAnalysis > 0 And objSetup.countDataUploadedOfSelectedAnalysis = 0) Then
                @MvcHtmlString.Create("Data has not been uploaded for selected analysis.")
            End If
        </label> 
        
        If (Not UserInfo Is Nothing) Then
                   
            If (UserInfo.UserRoleId <= UserRoles.SAU) Then
                    
                If (objSetup.CompanyCount = 0 Or objSetup.AanlysisCount = 0) Then
        @<label class="info">You have to create below listed action item, to get the scorecard
            view.</label> 
                End If
                If (objSetup.CompanyCount = 0) Then
        @<div class="secondbutton_example" style='margin: 20px; width: 200px;'>
            <a href='@Url.Action("Create", "Company")' style="text-decoration:none;color: #FFFFFF;">
                Add Company</a>
        </div>  
                End If
                If (objSetup.CompanyCount > 0 And objSetup.AanlysisCount = 0) Then
        @<div class="secondbutton_example" style='margin: 20px; width: 200px;'>
            <a href='@Url.Action("Create", "Analysis")'  style="text-decoration:none;color: #FFFFFF;">
                Add Analysis</a>
        </div>  
                End If
                If (objSetup.CompanyCount = 0 Or objSetup.AanlysisCount = 0) Then
        @<div class="secondbutton_example" style='margin: 20px; width: 200px;'>
            <a href='@Url.Action("CreateUser", "User")'  style="text-decoration:none;color: #FFFFFF;">
                Add User</a>
        </div>  
                End If
            ElseIf (UserInfo.UserRoleId > UserRoles.SAU) Then
                
                If (objSetup.CompanyCount = 0) Then
        @<label class="info">The Administrator has not give you access to a company / analysis.</label>
                ElseIf (objSetup.AanlysisCount = 0) Then
        @<label class="info">The Administrator has not given you access to an analysis.</label> 
                End If
                
            End If
        End If
        
        If (objSetup.CompanyCount > 0 And objSetup.AanlysisCount > 0 And objSetup.selectedAnalysis > 0 And objSetup.countDataUploadedOfSelectedAnalysis > 0) Then
           
        @<div id="tabbedscorecardView" style="display: none;">
            @Using Html.BeginForm("Index", "Scorecard")
                @<table style="width: 100%; margin-top: 7px;" class="center-top-select">
                    <tr>
                        <td>
                            <span>Period</span>
                            @Html.DropDownList("intPeriod", DirectCast(ViewData("Periods"), SelectList), New With {.Class = "select"})
                        </td>
                        <td id="lnkChangeScorecardSetup" style="display: none; float: right;">
                            <input id="configure_scorecard" type="button" value="Configure Scorecard" class="cancelbutton" />
                        </td>
                    </tr>
                </table>
                End Using
            <div id="tabs" style="margin-top: 5px; font-size: 12.5px;">
                <ul>
                    <li><a href="#scorecardView">Scorecard</a></li>
                </ul>
                <div id="scorecardView" style="width: 100%;">
                    <div id="spread" style="padding-top: 10px; display: inline-block; margin-left: 2px;
                        width: inherit;">
                        @Html.FpSpread("spdScorecard", Sub(x)
                                                           x.RowHeader.Visible = False
                                                           x.ActiveSheetView.PageSize = 400
                                                       End Sub)
                    </div>
                </div>
            </div>
        </div>           
            
        End If
        
    End Code
</div>
<script type="text/javascript">

    $("#tabs").tabs();

    $("#configure_scorecard").click(function () {
        window.location = '@Url.Action("Index", "ScorecardSetup")';
    });

    $('#intPeriod').change(function () {
        this.form.submit();
    });

    window.ViewerControlId = "#tabbedscorecardView";


    function ShowHideScorecardSetup() {

        var loggedUser = '@UserInfo.UserRoleId';
        var cruuser = '@UserRoles.CRU.GetHashCode()';
        if (loggedUser == cruuser) {
            $("#lnkChangeScorecardSetup").hide();
        }
        else {
            $("#lnkChangeScorecardSetup").show();
        }
    }


    $(function () {

        IsSessionAlive();
        ShowHideScorecardSetup();


        $("#updatesetup")
            .button()
            .click(function (event) {

                if (isSessionAliveCalled == 'false') { IsSessionAlive(); }

                document.location = '@Url.Action("Index", "ScorecardSetup")';
            });
    });

    $(document).ready(function () {

        IsSessionAlive();

        if (isSessionAliveCalled == 'false') { IsSessionAlive(); }

        $("#footerpanel").removeClass("plangurufooter");
        $("#footerpanel").addClass("plangurufooterfordashboard");

        //adjust width
//        var browsewidth = $(window).width();
//        $('#intBrowserWidth').val(browsewidth)
//        var spdWidth = $('#spdScorecard').width();
//        var calcwidth = spdWidth + 250;
//        if (calcwidth > 1100) {
//            if (calcwidth < browsewidth) {
//                //$('#main').css('padding-left', '20px');
//                $(".header").width('1265');
//                $(".nav-panel").width('1238');
//                $(".main-container").width('1250px');
//            }
//            else {
//            }
//        }
        //hide detail rows  
        if (document.getElementById("spdScorecard")) {
            var spread = document.getElementById("spdScorecard");
            var rc = spread.GetRowCount();
            var showclass = false
            var showsubtotal = false
            for (var i = 0; i < rc; i++) {
                var cellval = spread.GetValue(i, 3);
                if (cellval == "SDetail") {
                    spread.Rows(i).style.display = "none";
                }

            }
        }

        window.onload = function () {
            if (document.getElementById("spdScorecard")) {
                var spread = document.getElementById("spdScorecard");
                if (document.all) {
                    if (spread.addEventListener) {
                        spread.addEventListener("ActiveCellChanged", cellChanged, false);
                    }
                    else {
                        spread.onActiveCellChanged = cellChanged;
                    }
                }
                else {
                    spread.addEventListener("ActiveCellChanged", cellChanged, false);
                }
            }
        }

        function cellChanged(event) {

            if (event.col == 1 && document.getElementById("spdScorecard")) {
                var spread = document.getElementById("spdScorecard");
                var cellval = spread.GetValue(event.row, 1);
                var showsubtotal = false
                var rc = spread.GetRowCount();
                if (cellval == "-") { 
                    for (var i = event.row; i > -1 ; i--) { 
                        var rowtype = spread.GetValue(i, 3); 
                        if (rowtype == "Score") {
                            cell = document.getElementById("spdScorecard").GetCellByRowCol(i, 1);
                            cell.removeAttribute("FpCellType");
                            spread.SetValue(i, 1, "+", false);
                            cell.setAttribute("FpCellType", "readonly");
                        }
                        else {
                            spread.Rows(i).style.display = "none";
                        }
                        if (i == event.row - 2) {
                            break
                        }
                    }
                }
                else if (cellval == "+") {
                    for (var i = event.row; i > -1; i--) {
                        var rowtype = spread.GetValue(i, 3);
                        if (rowtype == "Score") {
                            cell = document.getElementById("spdScorecard").GetCellByRowCol(i, 1);
                            cell.removeAttribute("FpCellType");
                            spread.SetValue(i, 1, "-", false);
                            cell.setAttribute("FpCellType", "readonly");
                        }
                        else {
                            spread.Rows(i).style.display = "table-row";
                        }
                        if (i == event.row - 2) {
                            break
                        }
                    }
                }
                spread.SetActiveCell(event.row, 2)
            }

        }
    });
</script>
