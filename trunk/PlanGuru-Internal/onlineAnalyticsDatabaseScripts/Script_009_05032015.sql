IF NOT EXISTS(  SELECT *
				FROM   INFORMATION_SCHEMA.COLUMNS
                WHERE  TABLE_NAME = 'Analysis'
                AND (COLUMN_NAME = 'FirstYear' OR COLUMN_NAME = 'NumberofPeriods')) 
BEGIN
		ALTER TABLE Analysis 
		ADD
		FirstYear INT NULL,
		NumberofPeriods INT NULL
END                 
GO
