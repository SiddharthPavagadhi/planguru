
Alter table SubscriptionPlan add Cost_of_Additional_Analytics_Users decimal(18,2) null

update SubscriptionPlan set Cost_of_Additional_Analytics_Users = 19.00 where PlanTypeId = 3 or PlanTypeId = 5 or PlanTypeId = 7
update SubscriptionPlan set Cost_of_Additional_Analytics_Users = 228.00 where PlanTypeId = 4 or PlanTypeId = 6 or PlanTypeId = 8


Alter table Customer add NumberOfAnalyticsAddOn int not null default 0