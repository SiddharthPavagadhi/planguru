USE [onlineAnalytics.DataAccess]
GO

UPDATE [dbo].[SubscriptionPlan] set
       [PlanType] = 'PlanGuru Monthly Business Upgrade'
      ,[Plan] = 'bnpmupg'
      ,[Cost_of_Subscription] = 35.00
      ,[DefaultAddOnUsers] = 1
      ,[Cost_of_Additional_Users] = 20.00
      ,[ProductOptionId] = 1299
      ,[AllowableSeats] = 1
WHERE [PlanTypeId] = 7
GO

UPDATE [dbo].[SubscriptionPlan] set
       [PlanType] = 'PlanGuru Annual Business Upgrade'
      ,[Plan] = 'bnpyupg'
      ,[Cost_of_Subscription] = 299.00
      ,[DefaultAddOnUsers] = 1
      ,[Cost_of_Additional_Users] = 149.00
      ,[ProductOptionId] = 1300
      ,[AllowableSeats] = 1
WHERE [PlanTypeId] = 8
GO


