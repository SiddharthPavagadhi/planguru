update EmailInfo set EmailBody = '<html>
<body>

<table style="width:1327px">
	<tbody>
		<tr>
			<td style="width:1032px">
			

			<p><img src="http://#HostName#/Content/Images/EmailImage/Picture4.jpg" />&nbsp;</p>

			
			<table style="width:1017px">
				<tbody>
					<tr>
						<td style="width:1017px">
						<h1><strong>1)&nbsp;Activate&nbsp;your&nbsp;Account</strong></h1>

						<h2 style="margin-bottom:5px;"><span style="color:#4f81bd"><strong>##USERFULLNAME##</strong></span></h2>
						</td>
					</tr>
					<tr>
						<td style="width:1017px">
						<p><a href=##PASSWORDRESETLINK##>Click&nbsp;here&nbsp;to&nbsp;activate&nbsp;your&nbsp;account</a>&nbsp;|&nbsp;<a href="https://planguru.zendesk.com/entries/108019723-Setting-up-your-PlanGuru-Account">Instructions</a></p>
						</td>
					</tr>
					<tr>
						<td style="width:1017px">
						<p><strong>Temporary&nbsp;Password</strong>:&nbsp;##TEMPPASSWORD##</p>
						</td>
					</tr>
					<tr>
						<td style="width:1017px">
						<p>Once you have activated your account above, log in at <a href="https://my.planguru.com/" target="_blank">my.planguru.com</a></p>
						</td>
					</tr>
				</tbody>
			</table>

			</td>
			
		</tr>
		
	</tbody>
</table>
<hr/>
<h1><strong>2)&nbsp;Product&nbsp;Downloads&nbsp;&amp;&nbsp;Licenses</strong></h1>

<p><strong>Thank&nbsp;you&nbsp;for&nbsp;subscribing&nbsp;to&nbsp;PlanGuru.&nbsp;Below&nbsp;are&nbsp;your&nbsp;activation&nbsp;codes&nbsp;to&nbsp;unlock&nbsp;all&nbsp;of&nbsp;PlanGuru&rsquo;s&nbsp;features.</strong></p>

[DynamicContent]
<hr/>
<table style="width:638px">
	<tbody>
		<tr>
			<td rowspan="3" style="width:94px">
			<p><img src="http://#HostName#/Content/Images/EmailImage/Picture2.jpg" style="height:80px; width:80px" />&nbsp;</p>
			</td>
			<td colspan="2" style="width:544px">
			<h1 style="margin-bottom:0px;"><span style="color:#4f81bd"><strong>Advanced&nbsp;Reporting&nbsp;Excel&nbsp;Add-in</strong></span></h1>
			</td>
		</tr>
		<tr>
			<td colspan="2" style="width:544px">
			<p><a href="http://www.planguru.com/ftpupdates/pgreportingaddin6.exe">Click&nbsp;here&nbsp;to&nbsp;download&nbsp;the&nbsp;Advanced&nbsp;Reporting&nbsp;Excel&nbsp;Add-in</a></p>
			<p><a href="https://planguru.zendesk.com/hc/en-us/articles/213347806-Downloading-Activating-Installing-the-Advanced-Reporting-Add-In">Installation&nbsp;Instructions</a></p>
			</td>
		</tr>
	</tbody>
</table>
<hr/>


<table style="width:623px">
	<tbody>
		<tr>
			<td rowspan="3" style="width:94px">
			<h1><img src="http://#HostName#/Content/Images/EmailImage/Picture3.jpg" style="height:80px; width:80px" />&nbsp;</h1>
			</td>
			<td colspan="2" style="width:529px">
			<h1 style="margin-bottom:-20px;"><span style="color:#4f81bd"><strong>PlanGuru&nbsp;Analytics</strong></span></h1>
			</td>
		</tr>
		<tr>
			<td colspan="2" style="width:529px">
			<p>Log into <a href="https://www.planguruanalytics.com/">Planguru Analytics</a> using the Username and Password that you created when activating your account above.</p>
			<p><a href="https://planguru.zendesk.com/hc/en-us/articles/213351266">Getting Started in PlanGuru Analytics</a></p>
			</td>
		</tr>
	</tbody>
</table>
<hr/>
<h2>Support&nbsp;Information</h2>

<p><a href="http://www.planguru.com/learning-resources/learning-center/">Visit&nbsp;the&nbsp;PlanGuru&nbsp;Learning&nbsp;Center</a></p>

<p><a href="http://planguru.zendesk.com/">Visit&nbsp;the&nbsp;PlanGuru&nbsp;Knowledge&nbsp;Base</a></p>

<p>For&nbsp;technical&nbsp;support,&nbsp;please&nbsp;email&nbsp;<a href="mailto:support@planguru.com">support@planguru.com</a></p>
</body>
</html>' where Id = 9