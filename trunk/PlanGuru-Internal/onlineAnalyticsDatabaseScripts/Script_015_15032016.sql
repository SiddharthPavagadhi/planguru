Update EmailInfo set EmailSubject = 'Your PlanGuru Downloads & Licenses',EmailBody = '<html>
<body>

<table style="width:1327px">
	<tbody>
		<tr>
			<td style="width:1032px">
			

			<p><img src="http://#HostName#/Content/Images/EmailImage/Picture4.jpg" />&nbsp;</p>

			
			<table style="width:1017px">
				<tbody>
					<tr>
						<td style="width:1017px">
						<h1><strong>Manage&nbsp;your&nbsp;account</strong></h1>

						<h2 style="margin-bottom:5px;"><span style="color:#4f81bd"><strong>##USERFULLNAME##</strong></span></h2>
						</td>
					</tr>
					<tr>
						<td style="width:1017px">
						<p><a href=##PASSWORDRESETLINK##>Click&nbsp;here&nbsp;to&nbsp;activate&nbsp;your&nbsp;account</a>&nbsp;|&nbsp;<a href="https://planguru.zendesk.com/entries/108019723-Setting-up-your-PlanGuru-Account">Instructions</a></p>
						</td>
					</tr>
					<tr>
						<td style="width:1017px">
						<p><strong>Username</strong>:&nbsp;##USERID##&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Temporary&nbsp;Password</strong>:&nbsp;##TEMPPASSWORD##</p>
						</td>
					</tr>
					<tr>
						<td style="width:1017px">
						<p>Once you have activated your account above, log in at <a href="https://planguruanalytics.com/" target="_blank">planguruanalytics.com</a></p>
						</td>
					</tr>
				</tbody>
			</table>

			</td>
			
		</tr>
		
	</tbody>
</table>
<hr/>
<h1><strong>Product&nbsp;downloads&nbsp;&amp;&nbsp;licenses</strong></h1>

<p><strong>Thank&nbsp;you&nbsp;for&nbsp;subscribing&nbsp;to&nbsp;PlanGuru.&nbsp;Below&nbsp;are&nbsp;your&nbsp;activation&nbsp;codes&nbsp;to&nbsp;unlock&nbsp;all&nbsp;of&nbsp;PlanGuru&rsquo;s&nbsp;features.</strong></p>

[DynamicContent]
<hr/>
<table style="width:638px">
	<tbody>
		<tr>
			<td rowspan="3" style="width:94px">
			<p><img src="http://#HostName#/Content/Images/EmailImage/Picture2.jpg" style="height:80px; width:80px" />&nbsp;</p>
			</td>
			<td colspan="2" style="width:544px">
			<h1 style="margin-bottom:0px;"><span style="color:#4f81bd"><strong>Advanced&nbsp;Reporting&nbsp;Excel&nbsp;Add-in</strong></span></h1>
			</td>
		</tr>
		<tr>
			<td colspan="2" style="width:544px">
			<p><a href="http://www.planguru.com/ftpupdates/pgreportingaddin6.exe">Click&nbsp;here&nbsp;to&nbsp;download&nbsp;the&nbsp;Advanced&nbsp;Reporting&nbsp;Excel&nbsp;Add-in</a></p>
			</td>
		</tr>
	</tbody>
</table>
<hr/>


<table style="width:623px">
	<tbody>
		<tr>
			<td rowspan="3" style="width:94px">
			<h1><img src="http://#HostName#/Content/Images/EmailImage/Picture3.jpg" style="height:80px; width:80px" />&nbsp;</h1>
			</td>
			<td colspan="2" style="width:529px">
			<h1 style="margin-bottom:-20px;"><span style="color:#4f81bd"><strong>PlanGuru&nbsp;Analytics</strong></span></h1>
			</td>
		</tr>
		<tr>
			<td colspan="2" style="width:529px">
			<p><strong>Set&nbsp;up&nbsp;PlanGuru&nbsp;Analytics&nbsp;through&nbsp;your&nbsp;Account&nbsp;Log&nbsp;in&nbsp;above&nbsp;&nbsp;</strong></p>
			</td>
		</tr>
	</tbody>
</table>
<hr/>
<h2>Support&nbsp;Information</h2>

<p><a href="http://www.planguru.com/learning-resources/learning-center/">Visit&nbsp;the&nbsp;PlanGuru&nbsp;Learning&nbsp;Center</a></p>

<p><a href="http://planguru.zendesk.com/">Visit&nbsp;the&nbsp;PlanGuru&nbsp;Knowledge&nbsp;Base</a></p>

<p>For&nbsp;technical&nbsp;support,&nbsp;please&nbsp;email&nbsp;<a href="mailto:support@planguru.com">support@planguru.com</a></p>
</body>
</html>



'
where Id = 9


Update EmailInfo set EmailBody = '<html>
	<head>
		<title></title>
	</head>
	<body>
		<p>
			<img height="51" src="http://#HostName#/Content/Images/EmailImage/Picture4.jpg" width="174" /></p>
		<p>
			Thank&nbsp;you&nbsp;for&nbsp;adding&nbsp;an&nbsp;additional&nbsp;user&nbsp;to&nbsp;PlanGuru.&nbsp;Your&nbsp;account&nbsp;as&nbsp;been&nbsp;updated&nbsp;with&nbsp;the&nbsp;additional&nbsp;activation&nbsp;for&nbsp;your&nbsp;license&nbsp;that&nbsp;was&nbsp;emailed&nbsp;to&nbsp;you&nbsp;when&nbsp;you&nbsp;originally&nbsp;purchased.&nbsp;You&nbsp;may&nbsp;access&nbsp;this&nbsp;license&nbsp;by&nbsp;logging&nbsp;in&nbsp;to&nbsp;your&nbsp;account&nbsp;at&nbsp;<a href="##PLANGURUURL##">##PLANGURUURL##</a></p>
		<p>
			If&nbsp;you&nbsp;have&nbsp;any&nbsp;questions,&nbsp;please&nbsp;email&nbsp;us&nbsp;at&nbsp;<a href="mailto:support@planguru.com">support@planguru.com</a></p>
		<p>
			-&nbsp;PlanGuru&nbsp;Team</p>
	</body>
</html>'
where Id = 10



