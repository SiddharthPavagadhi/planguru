ALTER TABLE SubscriptionPlan
ADD ProductOptionId int Null
Go

ALTER TABLE SubscriptionPlan
ADD AllowableSeats int Null
Go

INSERT [dbo].[SubscriptionPlan] ([PlanTypeId], [PlanType], [Plan], [Cost_of_Subscription], [DefaultAddOnUsers], [Cost_of_Additional_Users], [ProductOptionId],[AllowableSeats]) VALUES (3, N'Businesses/Non Profit - Monthly', N'bnpm', CAST(99.00 AS Decimal(18, 2)), 1, CAST(29.00 AS Decimal(18, 2)), 1250,1)
INSERT [dbo].[SubscriptionPlan] ([PlanTypeId], [PlanType], [Plan], [Cost_of_Subscription], [DefaultAddOnUsers], [Cost_of_Additional_Users], [ProductOptionId],[AllowableSeats]) VALUES (4, N'Business/Non Profit - Yearly', N'bnpy', CAST(899.00 AS Decimal(18, 2)), 1, CAST(299.00 AS Decimal(18, 2)), NULL,1)
INSERT [dbo].[SubscriptionPlan] ([PlanTypeId], [PlanType], [Plan], [Cost_of_Subscription], [DefaultAddOnUsers], [Cost_of_Additional_Users], [ProductOptionId],[AllowableSeats]) VALUES (5, N'Accountant/Adviser - Monthly', N'aam', CAST(59.00 AS Decimal(18, 2)), 1, CAST(29.00 AS Decimal(18, 2)), NULL,1)
INSERT [dbo].[SubscriptionPlan] ([PlanTypeId], [PlanType], [Plan], [Cost_of_Subscription], [DefaultAddOnUsers], [Cost_of_Additional_Users], [ProductOptionId],[AllowableSeats]) VALUES (6, N'Accountant/Adviser - Yearly', N'aay', CAST(599.00 AS Decimal(18, 2)), 1, CAST(249.00 AS Decimal(18, 2)), NULL,1)
Go


ALTER TABLE Customer
ADD LicenseCustomerId int Null
Go

ALTER TABLE Customer
ADD ResultCode int Null
Go

ALTER TABLE Customer
ADD LicenseId int Null
Go

ALTER TABLE Customer
ADD Password varchar(50) Null
Go

ALTER TABLE Customer
ADD SerialNumber varchar(50) Null
Go

ALTER TABLE Customer
ADD ActivationPassword varchar(50) Null
Go

ALTER TABLE Customer
ADD AddUserResponse varchar(500) Null
Go