
/****** Object:  Table [dbo].[SubscriptionPlan]    Script Date: 04/23/2014 16:48:11 ******/
DROP TABLE [dbo].[SubscriptionPlan]
GO
/****** Object:  Table [dbo].[SubscriptionPlan]    Script Date: 04/23/2014 16:48:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SubscriptionPlan](
	[PlanTypeId] [int] NOT NULL,
	[PlanType] [varchar](50) NOT NULL,
	[Plan] [nvarchar](100) NOT NULL,
	[Cost_of_Subscription] [decimal](18, 2) NOT NULL,
	[DefaultAddOnUsers] [int] NOT NULL,
	[Cost_of_Additional_Users] [decimal](18, 2) NOT NULL,
 CONSTRAINT [PK_SubscriptionPlan] PRIMARY KEY CLUSTERED 
(
	[PlanTypeId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[SubscriptionPlan] ([PlanTypeId], [PlanType], [Plan], [Cost_of_Subscription], [DefaultAddOnUsers], [Cost_of_Additional_Users]) VALUES (1, N'Pay per user', N'planguru-analytics', CAST(19.95 AS Decimal(18, 2)), 0, CAST(19.95 AS Decimal(18, 2)))
INSERT [dbo].[SubscriptionPlan] ([PlanTypeId], [PlanType], [Plan], [Cost_of_Subscription], [DefaultAddOnUsers], [Cost_of_Additional_Users]) VALUES (2, N'Premium Group Plan', N'planguru-analytics-bundle', CAST(59.95 AS Decimal(18, 2)), 4, CAST(14.95 AS Decimal(18, 2)))



alter table dbo.Customer ADD PlanTypeId int null 
GO
alter table dbo.Customer add constraint defaultPlanTypeId default 1 for PlanTypeId
GO

Update dbo.Customer set PlanTypeId = 1 

alter table dbo.Customer alter column PlanTypeId int not null 
GO



