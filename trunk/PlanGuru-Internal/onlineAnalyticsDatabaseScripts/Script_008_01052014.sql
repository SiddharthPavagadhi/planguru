
SET IDENTITY_INSERT [dbo].[UserRolePermission] ON
INSERT [dbo].[UserRolePermission] ([UserRolePermissionId], [Description]) VALUES (29, N'Branding')
SET IDENTITY_INSERT [dbo].[UserRolePermission] OFF

SET IDENTITY_INSERT [dbo].[UserRolePermissionMapping] ON

INSERT INTO UserRolePermissionMapping(UserRolePermissionMappingId,UserRoleId, UserRolePermissionId,CreatedBy,UpdatedBy,CreatedOn, UpdatedOn)
values(120,3, 29,'110e7aa0-5f05-48a6-adb5-affcc6df8811','110e7aa0-5f05-48a6-adb5-affcc6df8811', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)

INSERT INTO UserRolePermissionMapping(UserRolePermissionMappingId,UserRoleId, UserRolePermissionId,CreatedBy,UpdatedBy,CreatedOn, UpdatedOn)
values(121,6, 29,'110e7aa0-5f05-48a6-adb5-affcc6df8811','110e7aa0-5f05-48a6-adb5-affcc6df8811', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)

SET IDENTITY_INSERT [dbo].[UserRolePermissionMapping] OFF

alter table dbo.Customer ADD 
	LogoImageFile nvarchar(1000) CONSTRAINT CUST_logoImage DEFAULT '' NOT NULL,	 
	PrimaryColor 	nvarchar(10) CONSTRAINT CUST_PrimaryColor DEFAULT ''NOT NULL,
	SecondaryColor 	nvarchar(10) CONSTRAINT CUST_SecondaryColor DEFAULT ''NOT NULL,
	FootNote nvarchar(1000) CONSTRAINT CUST_Footnote DEFAULT '' NOT NULL
GO


