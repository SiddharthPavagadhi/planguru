﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace RecurlyNew.List
{
    public class ExportFileList : RecurlyList<ExportFile>
    {
        public ExportFileList(string baseUrl) : base(Client.HttpRequestMethod.Get, baseUrl)
        { }
        public override RecurlyList<ExportFile> Start { get { return HasStartPage() ? new ExportFileList(StartUrl) : RecurlyList.Empty<ExportFile>(); } }
        public override RecurlyList<ExportFile> Next { get { return HasNextPage() ? new ExportFileList(NextUrl) : RecurlyList.Empty<ExportFile>(); } }
        public override RecurlyList<ExportFile> Prev { get { return HasPrevPage() ? new ExportFileList(PrevUrl) : RecurlyList.Empty<ExportFile>(); } }
        internal override void ReadXml(XmlTextReader reader)
        {
            while (reader.Read())
            {
                if (reader.Name == "export_files" && reader.NodeType == XmlNodeType.EndElement)
                    break;

                if (reader.NodeType == XmlNodeType.Element && reader.Name == "export_file")
                {
                    Add(new ExportFile(reader));
                }
            }
        }
    }
}
