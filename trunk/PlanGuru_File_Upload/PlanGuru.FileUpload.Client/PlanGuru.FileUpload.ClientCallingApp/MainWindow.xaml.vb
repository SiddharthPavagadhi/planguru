﻿Class MainWindow

    Dim planguruClientapppath = "D:\WorkspaceInternal\PGFileUpload\PlanGuru.FileUpload.Client\PlanGuru.FileUpload.Client\bin\Debug\PlanGuru.FileUpload.Client.exe"

    ''' <summary>
    ''' This Click event will call when MenuItem File Button Clicked
    ''' </summary>
    'Private Sub mnuFile_Click(ByVal sender As System.Object, ByVal e As System.Windows.RoutedEventArgs) Handles mnuLaunchClientValid.Click
    '    Dim planguruClientapppath = "E:\Workspace\PlanGuru_Window\Source\PlanGuruFileUploadSVN\PlanGuru.FileUpload.Client\PlanGuru.FileUpload.Client\bin\Debug\PlanGuru.FileUpload.Client.exe"
    '    Process.Start(planguruClientapppath, "D:\Workspace\PlanGuru\Upload\UploadFile.xls")
    '    'MessageBox.Show("Client app will opened from this Menu: " & planguruClientapppath)
    '    Me.Close()
    'End Sub

    ''' <summary>
    ''' This Click event will call when MenuItem Close Button Clicked
    ''' </summary>
    Private Sub mnuClose_Click(ByVal sender As System.Object, ByVal e As System.Windows.RoutedEventArgs) Handles mnuClose.Click
        Me.Close()
    End Sub

    'Private Sub mnuFile_Click(ByVal sender As System.Object, ByVal e As System.Windows.RoutedEventArgs) Handles mnuLaunchClientInvalid.Click
    '    Dim planguruClientapppath = "E:\Workspace\PlanGuru_Window\Source\PlanGuruFileUploadSVN\PlanGuru.FileUpload.Client\PlanGuru.FileUpload.Client\bin\Debug\PlanGuru.FileUpload.Client.exe"
    '    Process.Start(planguruClientapppath, "D:\Workspace\PlanGuru\Upload\UploadFile.xls")
    '    'MessageBox.Show("Client app will opened from this Menu: " & planguruClientapppath)
    '    Me.Close()
    'End Sub

    Private Sub mnuMenu_Click(ByVal sender As System.Object, ByVal e As System.Windows.RoutedEventArgs) Handles mnuMenu.Click
        Dim mnuItem As MenuItem = e.OriginalSource

        Select Case mnuItem.Name
            Case "mnuLaunchClientInvalid"
                CallInvalidFile()
            Case "mnuLaunchClientValid"
                CallValidFile()
        End Select

    End Sub
    Private Sub CallValidFile()
        Dim actualpath As String = "D:\TestFile\OperationsUpload.xls"
        Dim path As String = """" & actualpath & """"
        Process.Start(planguruClientapppath, path)
        Me.Close()
    End Sub

    Private Sub CallInvalidFile()
        Dim actualpath As String = "D:\Workspace\PlanGuru\Upload\Excel Files\UploadFileInvalidData.xls"
        Dim path As String = """" & actualpath & """"
        Process.Start(planguruClientapppath, path)
        Me.Close()
        Me.Close()
    End Sub

End Class
