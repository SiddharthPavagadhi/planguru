﻿<System.Runtime.Serialization.DataContractAttribute()> _
Public Class LoginContract
    Inherits BaseContract

    <DataMember()> _
    Public Property UserID() As Integer

    <DataMember()> _
    Public Property CustomerId() As String

    <DataMember()> _
    Public Property UserRole() As String

    <DataMember()> _
    Public Property ErrorMessage() As String

End Class
