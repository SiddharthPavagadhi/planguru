﻿Public Class BalanceEntity

    Private balanceId_value As Integer
    Private H101_value As Decimal
    Private H102_value As Decimal
    Private H103_value As Decimal
    Private H104_value As Decimal
    Private H105_value As Decimal
    Private H106_value As Decimal
    Private H107_value As Decimal
    Private H108_value As Decimal
    Private H109_value As Decimal
    Private H110_value As Decimal
    Private H111_value As Decimal
    Private H112_value As Decimal
    Private H201_value As Decimal
    Private H202_value As Decimal
    Private H203_value As Decimal
    Private H204_value As Decimal
    Private H205_value As Decimal
    Private H206_value As Decimal
    Private H207_value As Decimal
    Private H208_value As Decimal
    Private H209_value As Decimal
    Private H210_value As Decimal
    Private H211_value As Decimal
    Private H212_value As Decimal
    Private H301_value As Decimal
    Private H302_value As Decimal
    Private H303_value As Decimal
    Private H304_value As Decimal
    Private H305_value As Decimal
    Private H306_value As Decimal
    Private H307_value As Decimal
    Private H308_value As Decimal
    Private H309_value As Decimal
    Private H310_value As Decimal
    Private H311_value As Decimal
    Private H312_value As Decimal
    Private H401_value As Decimal
    Private H402_value As Decimal
    Private H403_value As Decimal
    Private H404_value As Decimal
    Private H405_value As Decimal
    Private H406_value As Decimal
    Private H407_value As Decimal
    Private H408_value As Decimal
    Private H409_value As Decimal
    Private H410_value As Decimal
    Private H411_value As Decimal
    Private H412_value As Decimal
    Private H501_value As Decimal
    Private H502_value As Decimal
    Private H503_value As Decimal
    Private H504_value As Decimal
    Private H505_value As Decimal
    Private H506_value As Decimal
    Private H507_value As Decimal
    Private H508_value As Decimal
    Private H509_value As Decimal
    Private H510_value As Decimal
    Private H511_value As Decimal
    Private H512_value As Decimal
    Private B101_value As Decimal
    Private B102_value As Decimal
    Private B103_value As Decimal
    Private B104_value As Decimal
    Private B105_value As Decimal
    Private B106_value As Decimal
    Private B107_value As Decimal
    Private B108_value As Decimal
    Private B109_value As Decimal
    Private B110_value As Decimal
    Private B111_value As Decimal
    Private B112_value As Decimal
    Private A101_value As Decimal
    Private A102_value As Decimal
    Private A103_value As Decimal
    Private A104_value As Decimal
    Private A105_value As Decimal
    Private A106_value As Decimal
    Private A107_value As Decimal
    Private A108_value As Decimal
    Private A109_value As Decimal
    Private A110_value As Decimal
    Private A111_value As Decimal
    Private A112_value As Decimal
    Private B201_value As Decimal
    Private B202_value As Decimal
    Private B203_value As Decimal
    Private B204_value As Decimal
    Private B205_value As Decimal
    Private B206_value As Decimal
    Private B207_value As Decimal
    Private B208_value As Decimal
    Private B209_value As Decimal
    Private B210_value As Decimal
    Private B211_value As Decimal
    Private B212_value As Decimal
    Private B301_value As Decimal
    Private B302_value As Decimal
    Private B303_value As Decimal
    Private B304_value As Decimal
    Private B305_value As Decimal
    Private B306_value As Decimal
    Private B307_value As Decimal
    Private B308_value As Decimal
    Private B309_value As Decimal
    Private B310_value As Decimal
    Private B311_value As Decimal
    Private B312_value As Decimal
    Private accountId_value As Integer
    Private errorMessage_value As String

    Public Property AccountId As Integer      
    Public Property BalanceId As Integer
    Public Property H101 As Decimal
    Public Property H102 As Decimal
    Public Property H103 As Decimal
    Public Property H104 As Decimal
    Public Property H105 As Decimal
    Public Property H106 As Decimal
    Public Property H107 As Decimal
    Public Property H108 As Decimal
    Public Property H109 As Decimal
    Public Property H110 As Decimal
    Public Property H111 As Decimal
    Public Property H112 As Decimal
    Public Property H201 As Decimal
    Public Property H202 As Decimal
    Public Property H203 As Decimal
    Public Property H204 As Decimal
    Public Property H205 As Decimal
    Public Property H206 As Decimal
    Public Property H207 As Decimal
    Public Property H208 As Decimal
    Public Property H209 As Decimal
    Public Property H210 As Decimal
    Public Property H211 As Decimal
    Public Property H212 As Decimal
    Public Property H301 As Decimal
    Public Property H302 As Decimal
    Public Property H303 As Decimal
    Public Property H304 As Decimal
    Public Property H305 As Decimal
    Public Property H306 As Decimal
    Public Property H307 As Decimal
    Public Property H308 As Decimal
    Public Property H309 As Decimal
    Public Property H310 As Decimal
    Public Property H311 As Decimal
    Public Property H312 As Decimal
    Public Property H401 As Decimal
    Public Property H402 As Decimal
    Public Property H403 As Decimal
    Public Property H404 As Decimal
    Public Property H405 As Decimal
    Public Property H406 As Decimal
    Public Property H407 As Decimal
    Public Property H408 As Decimal
    Public Property H409 As Decimal
    Public Property H410 As Decimal
    Public Property H411 As Decimal
    Public Property H412 As Decimal
    Public Property H501 As Decimal
    Public Property H502 As Decimal
    Public Property H503 As Decimal
    Public Property H504 As Decimal
    Public Property H505 As Decimal
    Public Property H506 As Decimal
    Public Property H507 As Decimal
    Public Property H508 As Decimal
    Public Property H509 As Decimal
    Public Property H510 As Decimal
    Public Property H511 As Decimal
    Public Property H512 As Decimal
    Public Property B101 As Decimal
    Public Property B102 As Decimal
    Public Property B103 As Decimal
    Public Property B104 As Decimal
    Public Property B105 As Decimal
    Public Property B106 As Decimal
    Public Property B107 As Decimal
    Public Property B108 As Decimal
    Public Property B109 As Decimal
    Public Property B110 As Decimal
    Public Property B111 As Decimal
    Public Property B112 As Decimal
    Public Property A101 As Decimal
    Public Property A102 As Decimal
    Public Property A103 As Decimal
    Public Property A104 As Decimal
    Public Property A105 As Decimal
    Public Property A106 As Decimal
    Public Property A107 As Decimal
    Public Property A108 As Decimal
    Public Property A109 As Decimal
    Public Property A110 As Decimal
    Public Property A111 As Decimal
    Public Property A112 As Decimal
    Public Property B201 As Decimal
    Public Property B202 As Decimal
    Public Property B203 As Decimal
    Public Property B204 As Decimal
    Public Property B205 As Decimal
    Public Property B206 As Decimal
    Public Property B207 As Decimal
    Public Property B208 As Decimal
    Public Property B209 As Decimal
    Public Property B210 As Decimal
    Public Property B211 As Decimal
    Public Property B212 As Decimal
    Public Property B301 As Decimal
    Public Property B302 As Decimal
    Public Property B303 As Decimal
    Public Property B304 As Decimal
    Public Property B305 As Decimal
    Public Property B306 As Decimal
    Public Property B307 As Decimal
    Public Property B308 As Decimal
    Public Property B309 As Decimal
    Public Property B310 As Decimal
    Public Property B311 As Decimal
    Public Property B312 As Decimal
    Public Property ErrorMessageBalanceData As String
      
End Class
