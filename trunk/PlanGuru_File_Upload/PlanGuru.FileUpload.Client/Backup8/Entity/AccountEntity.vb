﻿Public Class AccountEntity

    Private accountId_value As Integer
    Private acctDescriptor_value As String
    Private sortsequence_value As Integer
    Private description_value As String
    Private subgrouping_value As String
    Private accttypeId_value As Integer
    Private numberFormat_value As Integer
    Private totalType_value As Integer
    Private errorMessage_value As String

    Public Property AcctTypeId() As Integer

    Public Property SubGrouping() As String

    Public Property Description() As String

    Public Property SortSequence() As Integer

    Public Property AcctDescriptor() As String

    Public Property NumberFormat() As Integer

    Public Property TotalType() As Integer

    Public Property AccountId() As Integer

    Public Property ErrorMessageAccountData() As String

End Class
