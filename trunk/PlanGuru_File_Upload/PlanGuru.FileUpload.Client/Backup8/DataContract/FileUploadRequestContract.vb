﻿<System.Runtime.Serialization.DataContractAttribute()> _
Public Class FileUploadRequestContract
    Inherits BaseContract

    <DataMember()> _
    Public Property CompanyId() As Integer
    
    <DataMember()> _
    Public Property UserId() As Integer
   
    <DataMember()> _
    Public Property FileName() As String
    
    <DataMember()> _
    Public Property AnalysisId() As Integer
   
    <DataMember()> _
    Public Property AcctTypeFailCount() As Integer
    
    <DataMember()> _
    Public Property AccountFailCount() As Integer
    
    <DataMember()> _
    Public Property BalanceFailCount() As Integer

    <DataMember()> _
    Public Property AnalysisFailCount() As Integer

    <DataMember()> _
    Public Property AccountData() As List(Of AccountEntity)
   
    <DataMember()> _
    Public Property BalanceData() As List(Of BalanceEntity)
    
    <DataMember()> _
    Public Property AcctTypeData() As List(Of AcctTypeEntity)

    <DataMember()> _
    Public Property AnalysisData() As List(Of AnalysisEntity)
    
End Class
