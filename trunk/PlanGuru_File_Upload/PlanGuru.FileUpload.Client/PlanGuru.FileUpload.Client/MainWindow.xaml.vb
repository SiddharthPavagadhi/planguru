﻿Imports System.IO
Imports System.Windows
Imports PlanGuru.FileUpload.Client
Imports System.Collections.ObjectModel
Imports System.Collections.Generic
Imports System.Data
Imports System.Reflection
Imports System.Runtime.InteropServices
Imports System.Data.OleDb
Imports PlanGuru.FileUpload.BusinessLayer
Imports System.Collections.Specialized

''' <summary>
''' This is the Main window for PlanGuru Desktop File Upload App.
''' </summary>
''' <remarks></remarks>
Class MainWindow

    Dim objCompanyBL As CompanyBL
    Dim objAnalysisBL As AnalysisBL
    Dim objUploadBL As UploadBL
    Dim objExcelHelper As ExcelHelper

    ''' <summary>
    ''' This Click event will call when Cancel Button Clicked
    ''' </summary>
    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.Windows.RoutedEventArgs) Handles btnCancel.Click

        Common.LogInfoWriter("Button Cancel Clicked by UserId:" & GlobalSettings.UserId)
        Me.Close()
    End Sub

    ''' <summary>
    ''' This LoadCompanies Method will Load the Company data from Service and bind it to Company Dropdown.
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub LoadCompanies()
        Common.LogInfoWriter("LoadCompanies() Execution Start by UserId:" & GlobalSettings.UserId)
        Me.Cursor = Cursors.Wait
        Try
            If cobCompany.Items.Count > 0 Then
                cobCompany.Items.Clear()
            End If
            Dim companies = objCompanyBL.GetCompanies()
            For Each cmp In companies
                cobCompany.Items.Add(cmp)
                cobCompany.DisplayMemberPath = "CompanyName"
                cobCompany.SelectedValuePath = "CompanyId"
            Next
            If cobCompany.Items.Count > 0 Then
                cobCompany.SelectedIndex = 0
            End If
            Common.LogInfoWriter("LoadCompanies() Execution Ended with " & companies.Count.ToString() & " Companies by UserId:" & GlobalSettings.UserId)
        Catch ex As Exception
            Common.LogErrorWriter("LoadCompanies() Executed with Error Message:" & ex.Message & Environment.NewLine & "Stack Strace: " & ex.StackTrace & "by UserId:" & GlobalSettings.UserId)
        Finally
            Me.Cursor = Cursors.Arrow
        End Try

    End Sub

    ''' <summary>
    ''' This LoadAnalysis Method will Load the Analysis data from Service and bind it to Analysis Dropdown.
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub LoadAnalysis()
        Common.LogInfoWriter("LoadAnalysis() Execution Start by UserId:" & GlobalSettings.UserId)
        Me.Cursor = Cursors.Wait
        Try
            If cobAnalysis.Items.Count > 0 Then
                cobAnalysis.Items.Clear()
            End If
            Dim analysis = objAnalysisBL.GetAnalysis(cobCompany.SelectedValue.ToString())

            For Each ans In analysis
                cobAnalysis.Items.Add(ans)
                cobAnalysis.SelectedValuePath = "AnalysisId"
                cobAnalysis.DisplayMemberPath = "AnalysisName"
            Next
            If cobAnalysis.Items.Count > 0 Then
                cobAnalysis.SelectedIndex = 0
            End If
            Common.LogInfoWriter("LoadAnalysis() Execution End with " & analysis.Count.ToString() & " Analysis by UserId:" & GlobalSettings.UserId)
        Catch ex As Exception
            Common.LogErrorWriter("LoadAnalysis() Executed with Error Message:" & ex.Message & Environment.NewLine & "Stack Strace: " & ex.StackTrace & "by UserId:" & GlobalSettings.UserId)
        Finally
            Me.Cursor = Cursors.Arrow
        End Try


    End Sub

    ''' <summary>
    ''' This UploadFIle Method will Upload the selected file and send it to Service in byte array format.
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub UploadFile()
        Try
            Common.LogInfoWriter("UploadFile() Execution Start by UserId:" & GlobalSettings.UserId)
            'Validate Input Fields on the Form.
            If Not ValiedateInput() Then
                Return
            End If
            'Validate Excel File
            Dim strMessage = objExcelHelper.ValidatExcel(txtFilePath.Text.Trim())

            If strMessage.Equals(Common.SUCCESS) Then
                'Move all files from Input Folder to Archive Folder with datetime suffix.
                MoveInputFolderFilesToArchiveFolder()

                'Upload file to with Excel File Data, CompanyId and Analysis Id.
                strMessage = objUploadBL.UploadFile(GlobalSettings.FileData, cobAnalysis.SelectedValue.ToString(), cobCompany.SelectedValue.ToString())

                'Convert Response Contract to DataSet
                Dim dsResponse As DataSet = objUploadBL.ConvertResponseToDataSet(GlobalSettings.UploadResponse)
                ' Move all files from Error Folder to Archive Folder with DataTiem Suffix.
                MoveErrorFolderFilesToArchiveFolder()
                objUploadBL.WriteRequestErrorFile()
                'Export response Dataset to CSV file.
                ExportDataToCSV(dsResponse)

                If (strMessage.Equals(Common.SUCCESS)) Then
                    'MessageBox.Show("File Processing summary:" & CreateSummaryMessage(), Common.MSG_INFORMATION, MessageBoxButton.OK, MessageBoxImage.Information)
                    ' SES
                    MessageBox.Show(CreateSummaryMessage(), Common.MSG_INFORMATION, MessageBoxButton.OK, MessageBoxImage.Information)
                    Common.LogInfoWriter("Successfully Uploaded")
                    Me.Close() 'SES Added to close the main window if the file was uploaded successfully
                Else
                    MessageBox.Show(strMessage, Common.MSG_ERROR, MessageBoxButton.OK, MessageBoxImage.Error)
                    Common.LogErrorWriter("UploadFile Executed with Message: " & strMessage & " by UserId:" & GlobalSettings.UserId)
                End If
            Else
                MessageBox.Show(strMessage, Common.MSG_ERROR, MessageBoxButton.OK, MessageBoxImage.Error)
                Return
            End If
        Catch ex As Exception
            If ex.Message.Contains("open") Then
                MessageBox.Show("The Requested file to Upload is Opened, please close file and Try again.", Common.MSG_ERROR, MessageBoxButton.OK, MessageBoxImage.Error)
            End If
            Common.LogErrorWriter("UploadFile() Executed with Error Message:" & ex.Message & Environment.NewLine & "Stack Strace: " & ex.StackTrace & " by UserId:" & GlobalSettings.UserId)
        Finally
        End Try
    End Sub

    ''' <summary>
    ''' This Click event will call when Company Dropdown Selection Changed
    ''' </summary>
    Private Sub cobCompany_SelectionChanged(ByVal sender As System.Object, ByVal e As System.Windows.Controls.SelectionChangedEventArgs) Handles cobCompany.SelectionChanged
        LoadAnalysis()
    End Sub

    ''' <summary>
    ''' This Window_Initialized method will call when Application start and Hide Main Window to Show Login Form to User
    '''  Authentication.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub Window_Initialized(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Initialized
        Me.Hide()
        Try
            Common.LogInfoWriter("Window_Initialized() Execution Start by UserId:" & GlobalSettings.UserId)
            Dim args = sender

            GlobalSettings.LogPath = Common.LogDirectoryPath ' SES Added for logging to PG data folder
            Dim objLogin As Login = New Login()
            Dim isLogin = objLogin.ShowDialog()
            If isLogin Then
                objCompanyBL = New CompanyBL()
                objAnalysisBL = New AnalysisBL()
                objUploadBL = New UploadBL()
                objExcelHelper = New ExcelHelper()
                LoadCompanies()
                Me.txtFilePath.Text = Common.FilePath
                GlobalSettings.FilePath = Me.txtFilePath.Text
                Me.Show()
                CreateFileProcessFolders()
            Else
                Me.Close()
            End If
            Common.LogInfoWriter("Window_Initialized() Execution End by UserId:" & GlobalSettings.UserId)
        Catch ex As Exception
            Common.LogErrorWriter("Window_Initialized() Executed with Error Message:" & ex.Message & Environment.NewLine & "Stack Strace: " & ex.StackTrace & "by UserId:" & GlobalSettings.UserId)

        End Try

    End Sub

    ''' <summary>
    ''' Creating the Uploaded Files Folder Structure if not exist
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub CreateFileProcessFolders()
        Try
            Common.LogInfoWriter("CreateFileProcessFolders() Execution Start by UserId:" & GlobalSettings.UserId)
            Dim basePath As String = Common.BaseDirectoryPath
            If Not Directory.Exists(basePath) Then
                Directory.CreateDirectory(basePath)
            End If
            If Not Directory.Exists(basePath & Common.INPUTFOLDER) Then
                Directory.CreateDirectory(basePath & Common.INPUTFOLDER)
            End If
            If Not Directory.Exists(basePath & Common.INPUTFOLDER & Common.ARCHIEVEFOLDER) Then
                Directory.CreateDirectory(basePath & Common.INPUTFOLDER & Common.ARCHIEVEFOLDER)
            End If
            If Not Directory.Exists(basePath & Common.ERRORFOLDER) Then
                Directory.CreateDirectory(basePath & Common.ERRORFOLDER)
            End If
            If Not Directory.Exists(basePath & Common.ERRORFOLDER & Common.ARCHIEVEFOLDER) Then
                Directory.CreateDirectory(basePath & Common.ERRORFOLDER & Common.ARCHIEVEFOLDER)
            End If
            Common.LogInfoWriter("CreateFileProcessFolders() Execution End by UserId:" & GlobalSettings.UserId)
        Catch ex As Exception
            Common.LogErrorWriter("CreateFileProcessFolders() Executed with Error Message:" & ex.Message & Environment.NewLine & "Stack Strace: " & ex.StackTrace & "by UserId:" & GlobalSettings.UserId)
        End Try
    End Sub

    ''' <summary>
    ''' Moving Input Folder's Files to Input\Archive Folder with date time suffix.
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub MoveInputFolderFilesToArchiveFolder()
        Dim basePath As String = Common.BaseDirectoryPath & Common.INPUTFOLDER
        Try
            Common.LogInfoWriter("MoveInputFolderFilesToArchiveFolder() Execution Start by UserId:" & GlobalSettings.UserId)
            Dim fileEntries As String() = Directory.GetFiles(basePath)
            Dim suffixDate As String = DateTime.Now.ToString(Common.DATEFORMAT)
            Dim fileName As String
            For Each fileName In fileEntries
                File.Move(fileName, basePath & Common.ARCHIEVEFOLDER & "\" & Path.GetFileNameWithoutExtension(fileName) & suffixDate & Path.GetExtension(fileName))
            Next fileName
            File.Copy(txtFilePath.Text.Trim(), basePath & "\" & Path.GetFileName(txtFilePath.Text.Trim()))
            Common.LogInfoWriter("MoveInputFolderFilesToArchiveFolder() Execution End by UserId:" & GlobalSettings.UserId)
        Catch ex As Exception
            Common.LogErrorWriter("MoveInputFolderFilesToArchiveFolder() Executed with Error Message:" & ex.Message & Environment.NewLine & "Stack Strace: " & ex.StackTrace & "by UserId:" & GlobalSettings.UserId)
        End Try

    End Sub

    ''' <summary>
    ''' Moving Error Folder's Files to Error\Archive Folder with date time suffix.
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub MoveErrorFolderFilesToArchiveFolder()
        Try
            Common.LogInfoWriter("MoveErrorFolderFilesToArchiveFolder() Execution Start by UserId:" & GlobalSettings.UserId)
            Dim basePath As String = Common.BaseDirectoryPath & Common.ERRORFOLDER
            Dim suffixDate As String = DateTime.Now.ToString(Common.DATEFORMAT)
            Dim fileEntries As String() = Directory.GetFiles(basePath)
            Dim fileName As String
            For Each fileName In fileEntries
                File.Move(fileName, basePath & Common.ARCHIEVEFOLDER & "\" & Path.GetFileNameWithoutExtension(fileName) & suffixDate & Path.GetExtension(fileName))
            Next fileName
            Common.LogInfoWriter("MoveErrorFolderFilesToArchiveFolder() Execution End by UserId:" & GlobalSettings.UserId)
        Catch ex As Exception
            Common.LogErrorWriter("MoveErrorFolderFilesToArchiveFolder() Executed with Error Message:" & ex.Message & Environment.NewLine & "Stack Strace: " & ex.StackTrace & "by UserId:" & GlobalSettings.UserId)

        End Try

    End Sub

    ''' <summary>
    ''' Create CSV file from Response Dataset
    ''' </summary>
    ''' <param name="dsExportData"></param>
    ''' <remarks></remarks>
    Private Sub ExportDataToCSV(ByVal dsExportData As DataSet)
        Try
            Common.LogInfoWriter("ExportDataToCSV() Execution Start by UserId:" & GlobalSettings.UserId)
            Dim basePath As String = Common.BaseDirectoryPath & Common.ERRORFOLDER & "\"
            Dim strMessage = objExcelHelper.ExportDatasetToCsv(dsExportData.Tables("AcctType"), basePath & "_AcctTypeResponseError" & Common.EXT_CSV)
            If Not (strMessage.Equals(Common.SUCCESS)) Then
                MessageBox.Show(strMessage, Common.MSG_ERROR, MessageBoxButton.OK, MessageBoxImage.Error)
            End If
            strMessage = objExcelHelper.ExportDatasetToCsv(dsExportData.Tables("Account"), basePath & "_AccountResponseError" & Common.EXT_CSV)
            If Not (strMessage.Equals(Common.SUCCESS)) Then
                MessageBox.Show(strMessage, Common.MSG_ERROR, MessageBoxButton.OK, MessageBoxImage.Error)
            End If
            strMessage = objExcelHelper.ExportDatasetToCsv(dsExportData.Tables("Balance"), basePath & "_BalanceResponseError" & Common.EXT_CSV)
            If Not (strMessage.Equals(Common.SUCCESS)) Then
                MessageBox.Show(strMessage, Common.MSG_ERROR, MessageBoxButton.OK, MessageBoxImage.Error)
            End If

            If (dsExportData.Tables.Contains("Analysis")) Then
                strMessage = objExcelHelper.ExportDatasetToCsv(dsExportData.Tables("Analysis"), basePath & "_AnalysisResponseError" & Common.EXT_CSV)
                If Not (strMessage.Equals(Common.SUCCESS)) Then
                    MessageBox.Show(strMessage, Common.MSG_ERROR, MessageBoxButton.OK, MessageBoxImage.Error)
                End If

            End If
            Common.LogInfoWriter("ExportDataToCSV() Execution End by UserId:" & GlobalSettings.UserId)
        Catch ex As Exception
            Common.LogErrorWriter("ExportDataToCSV() Executed with Error Message:" & ex.Message & Environment.NewLine & "Stack Strace: " & ex.StackTrace & "by UserId:" & GlobalSettings.UserId)

        End Try

    End Sub

    ''' <summary>
    ''' Creating Summary Message to Display User after Upload.
    ''' </summary>
    ''' <returns>Summary Message as String</returns>
    ''' <remarks>SES Summary message uodated</remarks>
    Private Function CreateSummaryMessage() As String
        Dim summary As String
        Dim basePath As String = Common.BaseDirectoryPath & Common.ERRORFOLDER & "\"
        Try
            Common.LogInfoWriter("CreateSummaryMessage() Execution Start by UserId:" & GlobalSettings.UserId)
            If GlobalSettings.UploadResponse.AcctTypeFailCount = 0 And GlobalSettings.UploadResponse.AccountFailCount = 0 And GlobalSettings.UploadResponse.BalanceFailCount = 0 Then
                summary = Environment.NewLine & "Upload Completed Successfully!"
            Else
                summary = Environment.NewLine & "All account information did not upload:" & _
                     Environment.NewLine & "AcctType => Records: Total= " & GlobalSettings.UploadResponse.AcctTypeSuccessCount + GlobalSettings.UploadResponse.AcctTypeFailCount & ", Success=" & GlobalSettings.UploadResponse.AcctTypeSuccessCount.ToString() & ", Fail=" & GlobalSettings.UploadResponse.AcctTypeFailCount.ToString() & _
                     Environment.NewLine & "Account  => Records: Total= " & GlobalSettings.UploadResponse.AccountSuccessCount + GlobalSettings.UploadResponse.AccountFailCount & ", Success=" & GlobalSettings.UploadResponse.AccountSuccessCount.ToString() & ", Fail=" & GlobalSettings.UploadResponse.AccountFailCount.ToString() & _
                     Environment.NewLine & "Balance  => Records: Total= " & GlobalSettings.UploadResponse.BalanceSuccessCount + GlobalSettings.UploadResponse.BalanceFailCount & ", Success=" & GlobalSettings.UploadResponse.BalanceSuccessCount.ToString() & ", Fail=" & GlobalSettings.UploadResponse.BalanceFailCount.ToString() & _
                     Environment.NewLine & "Please review the log files in " & basePath & " to see more detail."
                ' SES Moved additional message here
                If (GlobalSettings.FileData.Tables.Contains("Analysis")) Then
                    summary = summary & Environment.NewLine & "Analysis  => Records: Total= " & GlobalSettings.UploadResponse.AnalysisSuccessCount + GlobalSettings.UploadResponse.AnalysisFailCount & ", Success=" & GlobalSettings.UploadResponse.AnalysisSuccessCount.ToString() & ", Fail=" & GlobalSettings.UploadResponse.AnalysisFailCount.ToString()
                End If
            End If
            ' SES - commented out here so it doesn't appear as part of the Success message
            'If (GlobalSettings.FileData.Tables.Contains("Analysis")) Then
            '    summary = summary & Environment.NewLine & "Analysis  => Records: Total= " & GlobalSettings.UploadResponse.AnalysisSuccessCount + GlobalSettings.UploadResponse.AnalysisFailCount & ", Success=" & GlobalSettings.UploadResponse.AnalysisSuccessCount.ToString() & ", Fail=" & GlobalSettings.UploadResponse.AnalysisFailCount.ToString()
            'End If
            Return summary
            Common.LogInfoWriter("CreateSummaryMessage() Execution End by UserId:" & GlobalSettings.UserId)
        Catch ex As Exception
            Common.LogErrorWriter("CreateSummaryMessage() Executed with Error Message:" & ex.Message & Environment.NewLine & "Stack Strace: " & ex.StackTrace & "by UserId:" & GlobalSettings.UserId)
            Return String.Empty
        End Try

    End Function

    ''' <summary>
    ''' Validate Input Fields for Data Upload Screen.
    ''' </summary>
    ''' <returns>True on Correct False on Failure</returns>
    ''' <remarks></remarks>
    Private Function ValiedateInput() As Boolean
        Dim isValid = True
        Try
            Common.LogInfoWriter("ValiedateInput() Execution Start by UserId:" & GlobalSettings.UserId)
            If Not (cobCompany.Items.Count > 0) Then
                MessageBox.Show("There is no Company available for this User.", Common.MSG_ERROR, MessageBoxButton.OK, MessageBoxImage.Error)
                Return False
            End If
            If Not (cobAnalysis.Items.Count > 0) Then
                MessageBox.Show("There is no Analysis available for this Company.", Common.MSG_ERROR, MessageBoxButton.OK, MessageBoxImage.Error)
                Return False
            End If
            If (cobCompany.SelectedValue = -1) Then
                MessageBox.Show("Please Select the Company from Dropdown.", Common.MSG_ERROR, MessageBoxButton.OK, MessageBoxImage.Error)
                Return False
            End If
            If (cobAnalysis.SelectedValue = -1) Then
                MessageBox.Show("Please Select the Analysis from Dropdown.", Common.MSG_ERROR, MessageBoxButton.OK, MessageBoxImage.Error)
                Return False
            End If
            If txtFilePath.Text.Trim() = String.Empty Then
                MessageBox.Show("Please Browse the Excel file to Upload.", Common.MSG_ERROR, MessageBoxButton.OK, MessageBoxImage.Error)
                Return False
            End If
            If Not System.IO.File.Exists(txtFilePath.Text.Trim()) Then
                MessageBox.Show("Please verify the selected file is exist on the Drive.", Common.MSG_ERROR, MessageBoxButton.OK, MessageBoxImage.Error)
                Return False
            End If
            Common.LogInfoWriter("ValiedateInput() Execution End by UserId:" & GlobalSettings.UserId)
        Catch ex As Exception
            Common.LogErrorWriter("ValiedateInput() Executed with Error Message:" & ex.Message & Environment.NewLine & "Stack Strace: " & ex.StackTrace & "by UserId:" & GlobalSettings.UserId)

        End Try
        Return isValid
    End Function

    Private Sub btnUpload_Click(ByVal sender As System.Object, ByVal e As System.Windows.RoutedEventArgs) Handles btnUpload.Click
        UploadFile()
    End Sub
    ''' <summary>
    ''' This "Open Error Folder" button click event will open the Error Folder where Error files created for invalid data.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnShowErrorFiles_Click(ByVal sender As System.Object, ByVal e As System.Windows.RoutedEventArgs) Handles btnShowErrorFiles.Click
        Try
            Common.LogInfoWriter("btnShowErrorFiles_Click() Execution Start by UserId:" & GlobalSettings.UserId)
            Dim MyProcess As New Process()
            MyProcess.StartInfo.FileName = "explorer.exe"
            MyProcess.StartInfo.Arguments = Common.BaseDirectoryPath & Common.ERRORFOLDER
            MyProcess.Start()
            MyProcess.WaitForExit()
            MyProcess.Close()
            MyProcess.Dispose()
            Common.LogInfoWriter("btnShowErrorFiles_Click() Execution End by UserId:" & GlobalSettings.UserId)
        Catch ex As Exception
            Common.LogErrorWriter("btnShowErrorFiles_Click() Executed with Error Message:" & ex.Message & Environment.NewLine & "Stack Strace: " & ex.StackTrace & "by UserId:" & GlobalSettings.UserId)
        End Try
    End Sub

End Class
