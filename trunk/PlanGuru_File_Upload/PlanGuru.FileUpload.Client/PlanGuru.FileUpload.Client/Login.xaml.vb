﻿Imports System.ServiceModel
Imports System.IO
Imports PlanGuru.FileUpload.BusinessLayer

Public Class Login
    Dim objLoginBL As  LoginBL
    Private Sub btnLogin_Click(sender As System.Object, e As System.Windows.RoutedEventArgs) Handles btnLogin.Click
        OnLoginClick()
    End Sub

    Private Sub btnCancel_Click(sender As System.Object, e As System.Windows.RoutedEventArgs) Handles btnCancel.Click
        Me.DialogResult = False
        Me.Close()
    End Sub

    Private Sub Window_Loaded(sender As System.Object, e As System.Windows.RoutedEventArgs) Handles MyBase.Loaded
        objLoginBL = New LoginBL()
    End Sub
    Private Sub OnLoginClick()
        Try
            Common.LogInfoWriter("OnLoginClick screen Start")
            If (txtUserName.Text.Trim() = String.Empty) And (txtPassword.Password.Trim() = String.Empty) Then
                MessageBox.Show("Please enter credential", Common.MSG_ERROR, MessageBoxButton.OK, MessageBoxImage.Error)
                Return
            End If
            Dim isValid = objLoginBL.Login(txtUserName.Text.Trim(), txtPassword.Password.Trim())
            If (isValid.ErrorMessage.Equals(Common.SUCCESS)) Then
                Me.DialogResult = True
                Me.Close()
            Else
                MessageBox.Show(isValid.ErrorMessage, Common.MSG_ERROR, MessageBoxButton.OK, MessageBoxImage.Error)
                Return
            End If
            Common.LogInfoWriter("OnLoginClick screen End")
        Catch fex As FaultException
            MessageBox.Show(fex.Message, Common.MSG_ERROR, MessageBoxButton.OK, MessageBoxImage.Error)
            Common.LogErrorWriter("OnLoginClick Exception occured: " & fex.Message & fex.StackTrace)
            Return
        Catch ex As Exception
            MessageBox.Show(ex.Message, Common.MSG_ERROR, MessageBoxButton.OK, MessageBoxImage.Error)
            Common.LogErrorWriter("OnLoginClick Exception occured: " & ex.Message & ex.StackTrace)
            Return
        End Try

    End Sub

End Class
