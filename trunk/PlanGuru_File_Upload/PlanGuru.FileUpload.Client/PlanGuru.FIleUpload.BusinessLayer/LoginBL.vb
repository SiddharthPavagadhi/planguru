﻿Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports PlanGuru.FIleUpload.BusinessLayer.PlanGuruService
Imports System.ServiceModel
Imports System.Net.Security
Imports System.Security.Cryptography.X509Certificates
''' <summary>
''' This is Login Business Logic class which will call WCF Service methods to GET or POST data related to Login Functionality
''' </summary>
''' <remarks></remarks>
Public Class LoginBL
    
    Private channel As ChannelFactory(Of IPlanGuruFileUploadService) = Nothing
    Private client As IPlanGuruFileUploadService
    Private _userName As String
    Private _userrole As Integer
    Dim objLoginBL As LoginBL

    Public Sub New()
        System.Net.ServicePointManager.ServerCertificateValidationCallback = Function(se As Object, _
         cert As System.Security.Cryptography.X509Certificates.X509Certificate, _
         chain As System.Security.Cryptography.X509Certificates.X509Chain, _
         sslerror As System.Net.Security.SslPolicyErrors) True

        Me.channel = New ChannelFactory(Of IPlanGuruFileUploadService)("BasicHttpBinding_IPlanGuruFileUploadService")
        Me.client = channel.CreateChannel()       
    End Sub

    ''' <summary>
    ''' Thie method will call WCF login method to check whether User is valid or not and 
    ''' If user valid then store the User information globally.
    ''' </summary>
    ''' <param name="uname">UserName as string</param>
    ''' <param name="pass">Password as string</param>
    ''' <returns>LoginContract Object</returns>
    ''' <remarks></remarks>
    Public Function Login(ByVal uname As String, ByVal pass As String) As LoginContract
        Dim objlogin As LoginContract = New LoginContract()

        Common.LogInfoWriter("Login in LoginBL execution start")
        Try           

            Dim objBaseContract As BaseContract = New BaseContract With {.UserName = uname, .Password = pass}
            objlogin = client.Login(objBaseContract)
            GlobalSettings.UserName = objlogin.UserName
            GlobalSettings.UserRole = objlogin.UserRole
            GlobalSettings.UserId = objlogin.UserID
            GlobalSettings.CustomerId = objlogin.CustomerId
            GlobalSettings.Password = pass
            objlogin.ErrorMessage = Common.SUCCESS
            Common.LogInfoWriter("Login in LoginBL execution end")

        Catch ex As Exception

            If (ex.Message.Contains("The underlying connection was closed: An unexpected error occurred on a receive.")) Then

                objlogin.ErrorMessage = String.Concat("Currently you do not have an  internet connection consequently PlanGuru is unable to upload data to PlanGuru Analytics.",
                                        Environment.NewLine, "Please connect to the internet and try again.")
            Else

                objlogin.ErrorMessage = ex.Message

            End If
            Common.LogErrorWriter("Login in LoginBL execution end with error message:" & ex.Message & ex.StackTrace)

        End Try
        Return objlogin
    End Function

End Class
