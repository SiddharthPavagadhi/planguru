﻿Imports PlanGuru.FIleUpload.BusinessLayer.PlanGuruService
Imports System.Data.OleDb
Imports System.IO
Imports System.Configuration
Imports System.Collections.Generic
Imports System.Runtime.InteropServices
Imports System
Imports System.Text
Imports System.Data
Imports System.Reflection

''' <summary>
''' This is ExcelHelper class to validate the Excel File.
''' </summary>
''' <remarks></remarks>
''' 
Public Class ExcelHelper
    Dim client As PlanGuruFileUploadServiceClient = New PlanGuruFileUploadServiceClient()

    ''' <summary>
    ''' This Method will validate the Excel File which going to be upload on Server.
    ''' </summary>
    ''' <param name="dsExcelPath"> Excel File Path</param>
    ''' <returns>Message As String "Success" or "Error Messages" on Failure</returns>
    ''' <remarks></remarks>
    Public Function ValidatExcel(ByVal dsExcelPath As String) As String
        Dim strMessage = String.Empty
        Try
            Common.LogInfoWriter("ValidatExcel in ExcelHelper start")
            strMessage = FileFormat(dsExcelPath) 'Check File Format
            If Not (strMessage.Equals("Success")) Then
                Return strMessage
            Else
                Dim dsExcelFile As DataSet = New DataSet()
                'Get DataSet from Excel File
                dsExcelFile = GetDataFromExcel(dsExcelPath.Trim())
                GlobalSettings.FileData = dsExcelFile
                'Check Excel File's No. of Sheets and Name
                strMessage = ExcelFileSheetNameAndCount(dsExcelFile)
                If Not (strMessage.Equals("Success")) Then
                    Return strMessage
                Else
                    'Check Excel File's Column Count for each Sheet
                    strMessage = FileColumnCount(dsExcelFile)
                    If Not (strMessage.Equals("Success")) Then
                        Return strMessage
                    Else
                        'Check Excel File's Blank data for all Sheet
                        strMessage = ExcelFileBlankData(dsExcelFile)
                        If Not (strMessage.Equals("Success")) Then
                            Return strMessage
                        End If
                    End If
                End If
            End If
            Common.LogInfoWriter("ValidatExcel in ExcelHelper end")
        Catch ex As Exception
            Common.LogErrorWriter("ValidatExcel in ExcelHelper end with error message: " & ex.Message & ex.StackTrace)
        End Try
        Return strMessage
    End Function

    ''' <summary>
    ''' This method will check the selected file format .xls or .xlsx.
    ''' </summary>
    ''' <param name="dsExcelPath">Excel File Path</param>
    ''' <returns>Message As String "Success" or "Error Messages" on Failure</returns>
    ''' <remarks></remarks>
    Private Function FileFormat(ByVal dsExcelPath As String) As String
        Dim strMessage = "Success"
        Try
            Common.LogInfoWriter("FileFormat in ExcelHelper start")
            If Not (dsExcelPath.Contains(".xls") Or dsExcelPath.Contains(".xlsx")) Then
                strMessage = ".xls or .xlsx files are not valid to upload."
            End If
            Common.LogInfoWriter("FileFormat in ExcelHelper end")
        Catch ex As Exception
            Common.LogErrorWriter("FileFormat in ExcelHelper end with error message: " & ex.Message & ex.StackTrace)
        End Try
        Return strMessage
    End Function

    ''' <summary>
    ''' This Method will check for Blank data for all Sheets.
    ''' </summary>
    ''' <param name="dsExcelData">ExcelFile as DataSet</param>
    ''' <returns>Message As String "Success" or "Error Messages" on Failure</returns>
    ''' <remarks></remarks>
    Private Function ExcelFileBlankData(ByVal dsExcelData As DataSet) As String
        Dim strMessage = "Success"
        Try
            Common.LogInfoWriter("ExcelFileBlankData in ExcelHelper start")
            If (dsExcelData.Tables("Account").Rows.Count = 0) Or (dsExcelData.Tables("AcctType").Rows.Count = 0) Or (dsExcelData.Tables("Balance").Rows.Count = 0) Then
                strMessage = "Please Verify all the sheets having data to upload."
                Return strMessage
            End If

            If (dsExcelData.Tables.Contains("Analysis") And dsExcelData.Tables("Analysis").Rows.Count = 0) Then
                strMessage = "Please Verify all the sheets having data to upload."
                Return strMessage
            End If

            Common.LogInfoWriter("ExcelFileBlankData in ExcelHelper end")
        Catch ex As Exception
            Common.LogErrorWriter("ExcelFileBlankData in ExcelHelper end with error message: " & ex.Message & ex.StackTrace)
        End Try
        Return strMessage
    End Function

    ''' <summary>
    ''' This Method will check for no. of columns in column count and Name.
    ''' </summary>
    ''' <param name="dsExcelData">ExcelFile Data as DataSet</param>
    ''' <returns>Message As String "Success" or "Error Messages" on Failure</returns>
    ''' <remarks></remarks>
    Private Function FileColumnCount(ByVal dsExcelData As DataSet) As String
        Dim strMessage = "Success"
        Try
            Common.LogInfoWriter("FileColumnCount in ExcelHelper start")
            'Column Count Validation for Account Sheet in Excel File.
            If Not (dsExcelData.Tables("Account")) Is Nothing Then
                If Not (dsExcelData.Tables("Account").Columns.Count = 8) Then
                    strMessage = strMessage & Environment.NewLine & " Account Sheet having " & dsExcelData.Tables("Account").Columns.Count.ToString() & " instead of 8 columns"
                End If
                'Column Name Validation Start for Account Sheet in Excel File.
                If Not (dsExcelData.Tables("Account").Columns(0).ColumnName.Equals("AccountId")) Then
                    strMessage = strMessage & Environment.NewLine & " Account Sheet's 1st Column is invalid " & dsExcelData.Tables("Account").Columns(0).ColumnName
                End If

                If Not (dsExcelData.Tables("Account").Columns(1).ColumnName.Equals("AcctDescriptor")) Then
                    strMessage = strMessage & Environment.NewLine & " Account Sheet's 2nd Column is invalid " & dsExcelData.Tables("Account").Columns(1).ColumnName
                End If

                If Not (dsExcelData.Tables("Account").Columns(2).ColumnName.Equals("SortSequence")) Then
                    strMessage = strMessage & Environment.NewLine & " Account Sheet's 3rd Column is invalid " & dsExcelData.Tables("Account").Columns(2).ColumnName
                End If

                If Not (dsExcelData.Tables("Account").Columns(3).ColumnName.Equals("Description")) Then
                    strMessage = strMessage & Environment.NewLine & " Account Sheet's 4th Column is invalid " & dsExcelData.Tables("Account").Columns(3).ColumnName
                End If
                If Not (dsExcelData.Tables("Account").Columns(4).ColumnName.Equals("Subgrouping")) Then
                    strMessage = strMessage & Environment.NewLine & " Account Sheet's 5th Column is invalid " & dsExcelData.Tables("Account").Columns(4).ColumnName
                End If
                If Not (dsExcelData.Tables("Account").Columns(5).ColumnName.Equals("AcctTypeId")) Then
                    strMessage = strMessage & Environment.NewLine & " Account Sheet's 6th Column is invalid " & dsExcelData.Tables("Account").Columns(5).ColumnName
                End If
                If Not (dsExcelData.Tables("Account").Columns(6).ColumnName.Equals("NumberFormat")) Then
                    strMessage = strMessage & Environment.NewLine & " Account Sheet's 7th Column is invalid " & dsExcelData.Tables("Account").Columns(6).ColumnName
                End If
                If Not (dsExcelData.Tables("Account").Columns(7).ColumnName.Equals("TotalType")) Then
                    strMessage = strMessage & Environment.NewLine & " Account Sheet's 8th Column is invalid " & dsExcelData.Tables("Account").Columns(7).ColumnName
                End If
                ''Column Name Validation End for Account Sheet in Excel File.
            End If

            If Not (dsExcelData.Tables("AcctType")) Is Nothing Then
                'Column Count Validation for AcctType Sheet in Excel File.
                If Not (dsExcelData.Tables("AcctType").Columns.Count = 5) Then
                    strMessage = strMessage & Environment.NewLine & " AcctType Sheet having " & dsExcelData.Tables("AcctType").Columns.Count.ToString() & " instead of 5 columns"
                End If

                'Column Name Validation Start for AcctType Sheet in Excel File.
                If Not (dsExcelData.Tables("AcctType").Columns(0).ColumnName.Equals("AcctTypeId")) Then
                    strMessage = strMessage & Environment.NewLine & " AcctType Sheet's 1st Column is invalid " & dsExcelData.Tables("AcctType").Columns(0).ColumnName
                End If

                If Not (dsExcelData.Tables("AcctType").Columns(1).ColumnName.Equals("TypeDesc")) Then
                    strMessage = strMessage & Environment.NewLine & " AcctType Sheet's 2nd Column is invalid " & dsExcelData.Tables("AcctType").Columns(1).ColumnName
                End If

                If Not (dsExcelData.Tables("AcctType").Columns(2).ColumnName.Equals("ClassDesc")) Then
                    strMessage = strMessage & Environment.NewLine & " AcctType Sheet's 3rd Column is invalid " & dsExcelData.Tables("AcctType").Columns(2).ColumnName
                End If

                If Not (dsExcelData.Tables("AcctType").Columns(3).ColumnName.Equals("SubclassDesc")) Then
                    strMessage = strMessage & Environment.NewLine & " AcctType Sheet's 4th Column is invalid " & dsExcelData.Tables("AcctType").Columns(3).ColumnName
                End If
                If Not (dsExcelData.Tables("AcctType").Columns(4).ColumnName.Equals("AnalysisId")) Then
                    strMessage = strMessage & Environment.NewLine & " AcctType Sheet's 5th Column is invalid " & dsExcelData.Tables("AcctType").Columns(4).ColumnName
                End If
                ''Column Name Validation End for AcctType Sheet in Excel File.
            End If

            If Not (dsExcelData.Tables("Balance")) Is Nothing Then
                'Column Count Validation for Balance Sheet in Excel File.
                If Not (dsExcelData.Tables("Balance").Columns.Count = 110) And Not (dsExcelData.Tables("Balance").Columns.Count = 98) Then
                    strMessage = strMessage & Environment.NewLine & " Balance Sheet having " & dsExcelData.Tables("Balance").Columns.Count.ToString() & " instead of 110 or 98 columns"
                End If

                'Column Name Validation Start for Balance Sheet in Excel File.

                Dim balanceColumnList As New ArrayList()
                Common.TableBalanceColumnList(balanceColumnList)


                For columnIndex As Integer = 1 To balanceColumnList.Count - 1
                    Dim exlColumn = dsExcelData.Tables("Balance").Columns(columnIndex).ColumnName.ToLower()
                    Dim listColumn = balanceColumnList.Item(columnIndex).ToString().ToLower()
                    If Not (exlColumn.Equals(listColumn)) Then
                        strMessage = strMessage & Environment.NewLine & " Balance Sheet's " + Common.NumberWithSuffix(columnIndex) + " Column is invalid " & dsExcelData.Tables("Balance").Columns(columnIndex).ColumnName
                    End If
                Next

                ''Column Name Validation End for Balance Sheet in Excel File.
            End If

            If Not (dsExcelData.Tables("Analysis")) Is Nothing Then

                'Column Count Validation for Analysis Sheet in Excel File.
                If Not (dsExcelData.Tables("Analysis").Columns.Count = 2) Then
                    strMessage = strMessage & Environment.NewLine & " Analysis Sheet having " & dsExcelData.Tables("Analysis").Columns.Count.ToString() & " instead of 2 columns"
                End If

                If Not (dsExcelData.Tables("Analysis").Columns(0).ColumnName.Equals("FirstYear")) Then
                    strMessage = strMessage & Environment.NewLine & " Analysis Sheet's 1st Column is invalid " & dsExcelData.Tables("AcctType").Columns(0).ColumnName
                End If

                If Not (dsExcelData.Tables("Analysis").Columns(1).ColumnName.Equals("NumberofPeriods")) Then
                    strMessage = strMessage & Environment.NewLine & " Analysis Sheet's 2nd Column is invalid " & dsExcelData.Tables("AcctType").Columns(1).ColumnName
                End If

            End If
            Common.LogInfoWriter("FileColumnCount in ExcelHelper end")
        Catch ex As Exception
            Common.LogErrorWriter("FileColumnCount in ExcelHelper end with error message: " & ex.Message & ex.StackTrace)
        End Try

        Return strMessage
    End Function

    ''' <summary>
    ''' This Method will check for Excel File contains no. of sheet and its Name.
    ''' </summary>
    ''' <param name="dsExcelData">ExcelFile Data as DataSet</param>
    ''' <returns>Message As String "Success" or "Error Messages" on Failure</returns>
    ''' <remarks></remarks>
    Private Function ExcelFileSheetNameAndCount(ByVal dsExcelData As DataSet) As String
        Dim strMessage = "Success"
        Try
            Common.LogInfoWriter("ExcelFileSheetNameAndCount in ExcelHelper start")
            Dim strTblName = String.Empty
            If Not (dsExcelData.Tables.Count >= 3) Then
                strMessage = " File does not contains all sheet"
            End If
            If Not (dsExcelData.Tables.Contains("Account")) Then
                strMessage = strMessage & Environment.NewLine & " Account Sheet does not Exist"
            End If
            If Not (dsExcelData.Tables.Contains("AcctType")) Then
                strMessage = strMessage & Environment.NewLine & " AcctType Sheet does not Exist"
            End If
            If Not (dsExcelData.Tables.Contains("Balance")) Then
                strMessage = strMessage & Environment.NewLine & " Balance Sheet does not Exist"
            End If
            If (dsExcelData.Tables.Count = 4) And Not (dsExcelData.Tables.Contains("Analysis")) Then
                strMessage = strMessage & Environment.NewLine & " Analysis Sheet does not Exist"
            End If
            Common.LogInfoWriter("ExcelFileSheetNameAndCount in ExcelHelper end")
        Catch ex As Exception
            Common.LogErrorWriter("ExcelFileSheetNameAndCount in ExcelHelper end with error message: " & ex.Message & ex.StackTrace)
        End Try
        Return strMessage
    End Function

    ''' <summary>
    ''' Return data in dataset from excel file.
    ''' </summary>
    ''' <param name="a_sFilepath">Excel file name for extract data.</param>
    ''' <returns>DataSet with Excel File Data</returns>
    Public Shared Function GetDataFromExcel(ByVal a_sFilepath As String) As DataSet
        Dim dsUploadExcel As New DataSet()
        Dim cnOLDB As OleDbConnection = Nothing
        Try
            Common.LogInfoWriter("GetDataFromExcel in ExcelHelper start")

            If a_sFilepath.Contains("xlsx") Then
                cnOLDB = New OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & a_sFilepath & ";Extended Properties=""Excel 12.0;HDR=YES;IMEX=1"";")
            Else
                cnOLDB = New OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & a_sFilepath & ";Extended Properties=""Excel 8.0;HDR=YES;IMEX=1"";")
            End If
            Try
                cnOLDB.Open()
            Catch ex As OleDbException
                If ex.Message.Contains("opened") Then
                    'MessageBox.Show("Excel File is Opened, Please Close and Try Again.", "Error Message", MessageBoxButton.OK, MessageBoxImage.Error)
                End If
                Console.WriteLine(ex.Message)
            Catch ex As Exception
                Console.WriteLine(ex.Message)
            End Try

            ' It Represents Excel data table Schema.
            Dim dtUploadExcel As New System.Data.DataTable()
            dtUploadExcel = cnOLDB.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, Nothing)
            If dtUploadExcel IsNot Nothing OrElse dtUploadExcel.Rows.Count > 0 Then
                For sheet_count As Integer = 0 To dtUploadExcel.Rows.Count - 1
                    Try
                        'Create Query to get Data from sheet.
                        Dim sheetname As String = dtUploadExcel.Rows(sheet_count)("table_name").ToString()
                        Dim da As New OleDbDataAdapter("SELECT * FROM [" & sheetname & "]", cnOLDB)
                        da.Fill(dsUploadExcel, sheetname.Replace("$", ""))
                    Catch ex As DataException
                        Console.WriteLine(ex.Message)
                    Catch ex As Exception
                        Console.WriteLine(ex.Message)
                    End Try
                Next
            End If
            cnOLDB.Close()
            Common.LogInfoWriter("GetDataFromExcel in ExcelHelper end")
        Catch ex As Exception
            Common.LogErrorWriter("GetDataFromExcel in ExcelHelper end with error message: " & ex.Message & ex.StackTrace)
        Finally
            If Not (cnOLDB Is Nothing) Then
                cnOLDB.Close()
            End If
        End Try

        Return dsUploadExcel
    End Function

    ''' <summary>
    ''' This method will create .csv file based on the data table data, on given gile path.
    ''' </summary>
    ''' <param name="excelsheetAsDataTable">DataTable for .csv file data</param>
    ''' <param name="fileName">Full File path with FIle name</param>
    ''' <returns>Message: success or error message</returns>
    ''' <remarks></remarks>
    Public Function ExportDatasetToCsv(ByVal excelsheetAsDataTable As DataTable, ByVal fileName As String) As String
        Common.LogInfoWriter("ExportDatasetToCsv in ExcelHelper execution start")
        Dim strMessage As String = Common.SUCCESS
        Dim drExcelRow As DataRow
        Dim dcExcelColumn As DataColumn
        Dim strData As String = String.Empty
        Dim bFirstRecord As Boolean = True
        Dim objWriter As New System.IO.StreamWriter(fileName)

        Try
            For Each dcExcelColumn In excelsheetAsDataTable.Columns
                strData = strData & dcExcelColumn.ColumnName & ","
            Next
            strData = strData.Substring(0, strData.Length - 1)
            strData = strData & Environment.NewLine
            For Each drExcelRow In excelsheetAsDataTable.Rows
                bFirstRecord = True
                For Each field As Object In drExcelRow.ItemArray
                    If Not bFirstRecord Then
                        strData = strData & ","
                    End If
                    strData = strData & field.ToString
                    bFirstRecord = False
                Next
                strData = strData & Environment.NewLine
            Next
            objWriter.WriteLine(strData)
            Common.LogInfoWriter("ExportDatasetToCsv in ExcelHelper execution end")
        Catch ex As Exception
            strMessage = ex.Message + ex.StackTrace
            Common.LogErrorWriter("ExportDatasetToCsv in ExcelHelper execution endwith Error Message:" & ex.Message & Environment.NewLine & ex.StackTrace)
        Finally
            If Not (objWriter Is Nothing) Then
                objWriter.Close()
            End If
        End Try
        Return strMessage
    End Function

End Class
