﻿Imports PlanGuru.FIleUpload.BusinessLayer.PlanGuruService
Imports System.ServiceModel
Imports System.Net.Security
Imports System.Security.Cryptography.X509Certificates

''' <summary>
''' This is Analysis Business Logic class which will call WCF Service methods to GET or POST data related to Analysis Functionality
''' </summary>
''' <remarks></remarks>
Public Class AnalysisBL

    Private channel As ChannelFactory(Of IPlanGuruFileUploadService) = Nothing
    Private client As IPlanGuruFileUploadService

    Public Sub New()
        System.Net.ServicePointManager.ServerCertificateValidationCallback = Function(se As Object, _
         cert As System.Security.Cryptography.X509Certificates.X509Certificate, _
         chain As System.Security.Cryptography.X509Certificates.X509Chain, _
         sslerror As System.Net.Security.SslPolicyErrors) True

        Me.channel = New ChannelFactory(Of IPlanGuruFileUploadService)("BasicHttpBinding_IPlanGuruFileUploadService")
        Me.client = channel.CreateChannel()
    End Sub

    ''' <summary>
    ''' This Method will call WCF GetAnalysisByCompanyId method to fetch Analysis based on Selected Company by User.
    ''' </summary>
    ''' <param name="companyId">CompanyId as string</param>
    ''' <returns>List of AnalysisContract as Object</returns>
    ''' <remarks></remarks>
    Public Function GetAnalysis(ByVal companyId As String) As List(Of AnalysisContract)
        Common.LogInfoWriter("GetAnalysis execution start")
        Dim objAnalysisRequest As AnalysisRequest = New AnalysisRequest()
        Dim analysis As List(Of AnalysisContract) = New List(Of AnalysisContract)
        Try
            objAnalysisRequest.CompanyId = companyId
            objAnalysisRequest.UserName = GlobalSettings.UserName
            objAnalysisRequest.Password = GlobalSettings.Password
            objAnalysisRequest.UserId = GlobalSettings.UserId
            objAnalysisRequest.UserRoleId = Convert.ToInt16(GlobalSettings.UserRole)
            objAnalysisRequest.CustomerId = GlobalSettings.CustomerId
            analysis = client.GetAnalysisByCompanyIdUserId(objAnalysisRequest).ToList()
            Common.LogInfoWriter("GetAnalysis execution End")
        Catch ex As Exception
            Common.LogErrorWriter("GetAnalysis Execution end with Error message: " & ex.Message & ex.StackTrace)
        End Try
        Return analysis
    End Function
End Class
