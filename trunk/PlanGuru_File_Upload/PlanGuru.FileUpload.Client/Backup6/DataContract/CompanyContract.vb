﻿<System.Runtime.Serialization.DataContractAttribute()> _
Public Class CompanyContract
    Inherits BaseContract

    <DataMember()> _
    Public Property CompanyId() As Integer

    <DataMember()> _
    Public Property CompanyName() As String
 
End Class
