﻿<System.Runtime.Serialization.DataContractAttribute()> _
Public Class FileUploadResponseContract
    Inherits BaseContract

    <DataMember()> _
    Public Property AcctTypeSuccessCount() As Integer
   
    <DataMember()> _
    Public Property AccountSuccessCount() As Integer
    
    <DataMember()> _
    Public Property BalanceSuccessCount() As Integer

    <DataMember()> _
    Public Property AnalysisSuccessCount() As Integer

    <DataMember()> _
    Public Property AcctTypeFailCount() As Integer
    
    <DataMember()> _
    Public Property AccountFailCount() As Integer
    
    <DataMember()> _
    Public Property BalanceFailCount() As Integer

    <DataMember()> _
    Public Property AnalysisFailCount() As Integer

    <DataMember()> _
    Public Property ResponseMessage() As String

    <DataMember()> _
    Public Property ErrorMessageAccount() As List(Of AccountEntity)
    
    <DataMember()> _
    Public Property ErrorMessageBalance() As List(Of BalanceEntity)
    
    <DataMember()> _
    Public Property ErrorMessageAcctType() As List(Of AcctTypeEntity)

    <DataMember()> _
    Public Property ErrorMessageAnalysis() As List(Of AnalysisEntity)
     
End Class
