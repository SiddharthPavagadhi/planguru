﻿<System.Runtime.Serialization.DataContractAttribute()> _
Public Class BaseContract
    Implements IExtensibleDataObject

    Public Property ExtensionData() As ExtensionDataObject _
       Implements IExtensibleDataObject.ExtensionData

    <DataMember()> _
    Public Property UserName() As String

    <DataMember()> _
    Public Property Password() As String

End Class
