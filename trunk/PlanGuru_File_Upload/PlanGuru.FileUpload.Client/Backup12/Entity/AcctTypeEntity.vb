﻿Public Class AcctTypeEntity

    Private accttypeId_value As Integer
    Private typeDesc_value As String
    Private subclassDesc_value As String
    Private classDesc_value As String
    Private analysisId_value As Integer
    Private errorMessage_value As String

    Public Property AcctTypeId() As Integer

    Public Property TypeDesc() As String

    Public Property ClassDesc() As String

    Public Property SubclassDesc() As String

    Public Property AnalysisId() As String

    Public Property ErrorMessageAcctTypeData() As String

End Class
