﻿Imports PlanGuru.FileUpload.Service.Company
Imports PlanGuru.FileUpload.Service.Analysis

''' <summary>
'''  An interface to define the Service Contract for PlanGuru Service.
''' </summary>
''' <remarks></remarks>
<ServiceContract()>
Public Interface IPlanGuruFileUploadService

    <OperationContract()>
    Function GetAnalysisByCompanyIdUserId(ByVal analysisRequest As AnalysisRequest) As IEnumerable(Of AnalysisContract)

    <OperationContract()>
    Function GetCompaniesByUserId(ByVal companyRequest As CompanyRequestContract) As List(Of CompanyContract)

    <OperationContract()>
    Function UploadFile(ByVal requestData As FileUploadRequestContract) As FileUploadResponseContract ', ByVal analysisId As String, ByVal companyid As String, ByVal fileName As String, ByVal userId As Integer) As ResponseContract

    <OperationContract()>
    Function Login(ByVal loginRequest As BaseContract) As LoginContract

    '<OperationContract()>
    'Function GetErrorMessage(ByVal userId As String) As ErrorMessageContract

End Interface


