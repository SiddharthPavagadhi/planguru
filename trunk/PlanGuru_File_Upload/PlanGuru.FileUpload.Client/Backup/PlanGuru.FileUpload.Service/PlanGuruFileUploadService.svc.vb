﻿Imports System.Runtime.Remoting.Contexts
Imports Microsoft.WindowsAzure.Storage.Table.DataServices
Imports PlanGuru.FileUpload.Service.PGFileUploadEntities
Imports System.Data.Objects.ObjectContext

''' <summary>
''' A PlanGuruFIleUPload Service class which Implements the IPlanguruFileUploadService Interface methods.
''' </summary>
''' <remarks></remarks>
Public Class PlanGuruFileUploadService
    Implements IPlanGuruFileUploadService
    Dim dbcontext As PGFileUploadEntities = New PGFileUploadEntities()


    ''' <summary>
    ''' This Service Method will check User Login with Database User table.
    ''' </summary>
    ''' <param name="loginRequest">BaseContract object</param>
    ''' <returns>Login Contract</returns>
    ''' <remarks></remarks>
    Public Function Login(ByVal loginRequest As BaseContract) As LoginContract Implements IPlanGuruFileUploadService.Login
        Dim objLoginContract As LoginContract = Nothing
        Common.LogInfoWriter("Login Start ")
        Try
            objLoginContract = ValidateLogin(loginRequest.UserName, loginRequest.Password)
            Common.LogInfoWriter("Login End")
        Catch fex As FaultException
            Common.LogErrorWriter(fex.Message & fex.StackTrace)
            Dim errorContract = New ErrorContract With {.ErrorCode = 1001, .ErrorMessage = fex.Message()}
            Throw New FaultException(Of ErrorContract)(errorContract, New FaultReason(errorContract.ErrorMessage))
        Catch ex As Exception
            Common.LogErrorWriter(ex.Message)
            Throw New Exception(ex.Message)
        End Try

        Return objLoginContract
    End Function

    ''' <summary>
    ''' 'To Get List of Analysis filter By Company Id in response to AnalysisContract object
    ''' </summary>
    ''' <param name="analysisRequest">AnalysisRequest object</param>
    ''' <returns>AnalysisContract Object</returns>
    ''' <remarks></remarks>
    Public Function GetAnalysisByCompanyIdUserId(ByVal analysisRequest As AnalysisRequest) As IEnumerable(Of AnalysisContract) Implements IPlanGuruFileUploadService.GetAnalysisByCompanyIdUserId
        Common.LogInfoWriter("GetAnalysisByCompanyId Execution Start")
        Dim listEnumAnalysis As List(Of AnalysisContract) = New List(Of AnalysisContract)
        Try
            Dim enumAnalysis = Nothing
            If (analysisRequest.UserRoleId = UserRoles.SSU) Then

                enumAnalysis = (From user In dbcontext.Users Join uam In dbcontext.UserAnalysisMappings On user.UserId Equals uam.UserId Join a In dbcontext.Analyses _
                    On a.AnalysisId Equals uam.AnalysisId _
                    Where user.CustomerId = analysisRequest.CustomerId And user.UserRoleId = UserRoles.SAU And analysisRequest.CompanyId = a.CompanyId
                   Select a.AnalysisId, a.AnalysisName)

            ElseIf (analysisRequest.UserRoleId <> UserRoles.SSU) Then

                enumAnalysis = (From user In dbcontext.Users Join uam In dbcontext.UserAnalysisMappings On user.UserId Equals uam.UserId Join a In dbcontext.Analyses _
                       On a.AnalysisId Equals uam.AnalysisId _
                       Where analysisRequest.UserId = user.UserId And analysisRequest.CompanyId = a.CompanyId
                      Select a.AnalysisId, a.AnalysisName)
            End If

           
            For Each analysis In enumAnalysis
                listEnumAnalysis.Add(New AnalysisContract With {.AnalysisId = analysis.AnalysisId, .AnalysisName = analysis.AnalysisName})
            Next
            Common.LogInfoWriter("GetAnalysisByCompany() Execution End")

        Catch fex As FaultException
            Common.LogErrorWriter(fex.Message & fex.StackTrace)
            Dim errorContract = New ErrorContract With {.ErrorCode = 2001, .ErrorMessage = fex.Message()}
            Throw New FaultException(Of ErrorContract)(errorContract, New FaultReason(errorContract.ErrorMessage))
        Catch ex As Exception
            Common.LogErrorWriter("GetAnalysisByCompany() Executed with Error Message:" & ex.Message & Environment.NewLine & "Stack Strace: " & ex.StackTrace)
            Throw New Exception(ex.Message)
        End Try

        Return listEnumAnalysis
    End Function

    ''' <summary>
    ''' Get List of Company in response to CompanyContract object
    ''' </summary>
    ''' <returns>CompanyContract Object</returns>
    ''' <remarks></remarks>
    Public Function GetCompaniesByUserId(ByVal companyRequest As CompanyRequestContract) As List(Of CompanyContract) Implements IPlanGuruFileUploadService.GetCompaniesByUserId
        Common.LogInfoWriter("GetCompanies Execution Start")
        Dim listEnumCompany As List(Of CompanyContract) = New List(Of CompanyContract)
        Try
            Dim objLoginContract = ValidateLogin(companyRequest.UserName, companyRequest.Password)
            If Not (objLoginContract Is Nothing) Then
                Try
                  
                    Dim enumCompanies = Nothing
                    If (companyRequest.UserRoleId = UserRoles.SSU) Then
                        enumCompanies = (From user In dbcontext.Users
                                        Join usc In dbcontext.UserCompanyMappings On user.UserId Equals usc.UserId
                                        Join c In dbcontext.Companies On c.CompanyId Equals usc.CompanyId _
                              Where user.CustomerId = companyRequest.CustomerId And user.UserRoleId = UserRoles.SAU
                             Select c.CompanyId, c.CompanyName).Distinct().ToList()

                    ElseIf (companyRequest.UserRoleId <> UserRoles.SSU) Then

                        enumCompanies = (From user In dbcontext.Users Join usc In dbcontext.UserCompanyMappings On user.UserId Equals usc.UserId Join c In dbcontext.Companies _
                                     On c.CompanyId Equals usc.CompanyId _
                                     Where companyRequest.UserId = user.UserId _
                                    Select c.CompanyId, c.CompanyName, user.UserId)

                    End If                   

                    For Each company In enumCompanies
                        listEnumCompany.Add(New CompanyContract With {.CompanyId = company.CompanyId, .CompanyName = Common.Decrypt(company.CompanyName)})
                    Next
                    Common.LogInfoWriter("GetCompanies Execution End")
                Catch ex As Exception
                    Common.LogErrorWriter("GetCompanies() Executed with Error Message:" & ex.Message & Environment.NewLine & "Stack Strace: " & ex.StackTrace)
                Finally

                End Try
            End If
        Catch fex As FaultException
            Common.LogErrorWriter(fex.Message & fex.StackTrace)
            Dim errorContract = New ErrorContract With {.ErrorCode = 3001, .ErrorMessage = fex.Message()}
            Throw New FaultException(Of ErrorContract)(errorContract, New FaultReason(errorContract.ErrorMessage))
        Catch ex As Exception
            Common.LogErrorWriter(ex.Message & ex.StackTrace)
            Throw New Exception(ex.Message)
        End Try

        Return listEnumCompany
    End Function

    ''' <summary>
    ''' The UploadFile Service Method will accept file in Byte array format.
    ''' </summary>
    ''' <param name="requestData">FileData as DataSet</param>
    ''' <returns>True or False</returns>
    ''' <remarks></remarks>
    Public Function UploadFile(ByVal fileUploadRequestData As FileUploadRequestContract) As FileUploadResponseContract Implements IPlanGuruFileUploadService.UploadFile
        Common.LogInfoWriter("UploadFile Execution Start by UserId: " & fileUploadRequestData.UserId)

        Dim objResponseContract As FileUploadResponseContract = New FileUploadResponseContract()
        Try
            Dim objLoginContract = ValidateLogin(fileUploadRequestData.UserName, fileUploadRequestData.Password)
            If Not (objLoginContract Is Nothing) Then
                objResponseContract.ResponseMessage = "Success"
                Try
                    'Insert into FileProcess Table.
                    Dim fileProcessId = SaveFileProcess(fileUploadRequestData)
                    'Insert into AcctType Table from Excel's AcctType Sheet Data.
                    objResponseContract = SaveAcctType(fileUploadRequestData, objResponseContract, fileProcessId)
                    'Insert into Account Table from Excel's Account Sheet Data.
                    objResponseContract = SaveAccount(fileUploadRequestData, objResponseContract, fileProcessId)
                    'Insert into Balance Table from Excel's Balance Sheet Data.
                    objResponseContract = SaveBalance(fileUploadRequestData, objResponseContract, fileProcessId)
                    'Insert into Analysis Table from Excel's Analysis Sheet Data.
                    If (fileUploadRequestData.AnalysisData.Count > 0) Then
                        objResponseContract = SaveAnalysis(fileUploadRequestData, objResponseContract, fileProcessId)
                    End If

                    Common.LogInfoWriter("Start Query : Get the list of invalid dashboard items by UserId: " & fileUploadRequestData.UserId)
                    Dim dashboardInvalidItems = (From d In dbcontext.Dashboards
                                                   Where Not (From a In dbcontext.Accounts Select New With {a.AccountId, a.AnalysisId}).Contains(New With {d.AccountId, d.AnalysisId}) _
                                                   And d.UserId = fileUploadRequestData.UserId
                                                   Select d)
                    Common.LogInfoWriter("Invalid dashboard items count : " & dashboardInvalidItems.Count)
                    Common.LogInfoWriter("End Query : Get the list of invalid dashboard items by UserId: " & fileUploadRequestData.UserId)

                    Common.LogInfoWriter("Start deleting : Invalid dashboard items")
                    dashboardInvalidItems.ToList().ForEach(Sub(item) dbcontext.Dashboards.DeleteObject(item))
                    dbcontext.SaveChanges()
                    Common.LogInfoWriter("End deleting : Invalid dashboard items")


                    If (objResponseContract.ErrorMessageAcctType.Count > 0) Or (objResponseContract.ErrorMessageAccount.Count > 0) Or (objResponseContract.ErrorMessageBalance.Count > 0) Or ((Not IsNothing(objResponseContract.ErrorMessageAnalysis) And objResponseContract.ErrorMessageAnalysis.Count > 0)) Then
                        objResponseContract.ResponseMessage = "Data validation failed, Please see the Response for not uploaded columns."
                        Common.LogInfoWriter("File Upload Executed Partially by UserId: " & fileUploadRequestData.UserId)
                    Else
                        Common.LogInfoWriter("File Upload Execution End by UserId: " & fileUploadRequestData.UserId)
                    End If
                Catch fex As FaultException
                    Common.LogErrorWriter(fex.Message & fex.StackTrace)
                    Dim errorContract = New ErrorContract With {.ErrorCode = 4001, .ErrorMessage = fex.Message()}
                    Throw New FaultException(Of ErrorContract)(errorContract, New FaultReason(errorContract.ErrorMessage))
                Catch ex As Exception
                    objResponseContract.ResponseMessage = ex.Message
                    Common.LogErrorWriter("UploadFile() Executed with Error Message:" & ex.Message & Environment.NewLine & "Stack Strace: " & ex.StackTrace & " by UserId:" & fileUploadRequestData.UserId)
                End Try
            End If
        Catch fex As FaultException
            Common.LogErrorWriter(fex.Message & fex.StackTrace)
            Dim errorContract = New ErrorContract With {.ErrorCode = 4002, .ErrorMessage = fex.Message()}
            Throw New FaultException(Of ErrorContract)(errorContract, New FaultReason(errorContract.ErrorMessage))
        Catch ex As Exception
            Common.LogErrorWriter(ex.Message & ex.StackTrace)
            Throw New Exception(ex.Message)
        End Try


        Return objResponseContract
    End Function

    ''' <summary>
    ''' This Method will save data in to FileProcess Table and return the last inserted record Id.
    ''' </summary>
    ''' <param name="fileUploadRequestData">FileUploadRequestContract object</param>
    ''' <returns>Last Inserted Id</returns>
    ''' <remarks></remarks>
    Private Function SaveFileProcess(ByVal fileUploadRequestData As FileUploadRequestContract) As Integer
        Dim fileProcessId As Integer = 0
        Common.LogInfoWriter("SaveFileProcess Start by UserId:" & fileUploadRequestData.UserId)
        Try
            Dim newFileProcess = New FileProcess With {.FileName = fileUploadRequestData.FileName, .AnalysisId = fileUploadRequestData.AnalysisId, .UserId = fileUploadRequestData.UserId, .UploadedDate = DateTime.Now.ToString("G"), .CompanyId = fileUploadRequestData.CompanyId}
            dbcontext.FileProcesses.AddObject(newFileProcess)
            dbcontext.SaveChanges()
            fileProcessId = newFileProcess.FileProcessId
            Common.LogInfoWriter("SaveFileProcess End by UserId:" & fileUploadRequestData.UserId)
        Catch fex As FaultException
            Common.LogErrorWriter(fex.Message & fex.StackTrace)
            Dim errorContract = New ErrorContract With {.ErrorCode = 5001, .ErrorMessage = fex.Message()}
            Throw New FaultException(Of ErrorContract)(errorContract, New FaultReason(errorContract.ErrorMessage))
        Catch ex As Exception
            Common.LogErrorWriter("SaveFileProcess got exception message: " & ex.Message & ex.StackTrace & " by UserId:" & fileUploadRequestData.UserId)
        End Try

        Return fileProcessId
    End Function

    ''' <summary>
    ''' This method will Insert accountType Excel Sheet data into AcctType Table.
    ''' </summary>
    ''' <param name="fileUploadRequestData">FileUPloadRequesetContract Object</param>
    ''' <param name="objResponseContract">FileUploadResponseContract Object</param>
    ''' <param name="fileProcessId">FileProcess Id</param>
    ''' <returns>FileUploadedResponseCOntract Object</returns>
    ''' <remarks></remarks>
    Private Function SaveAcctType(ByVal fileUploadRequestData As FileUploadRequestContract, ByVal objResponseContract As FileUploadResponseContract, ByVal fileProcessId As Integer) As FileUploadResponseContract
        Try
            Common.LogInfoWriter("SaveAcctType Start by UserId:" & fileUploadRequestData.UserId)
            objResponseContract.ErrorMessageAcctType = DataValidationAndInsert.CheckAndInsertIntoAcctType(fileUploadRequestData.AcctTypeData, fileUploadRequestData.AnalysisId)
            objResponseContract.ResponseMessage = SaveFileUploadedResult(fileProcessId, 1, fileUploadRequestData.AcctTypeData.Count - objResponseContract.ErrorMessageAcctType.Count, objResponseContract.ErrorMessageAcctType.Count + fileUploadRequestData.AcctTypeFailCount)
            objResponseContract.AcctTypeFailCount = objResponseContract.ErrorMessageAcctType.Count + fileUploadRequestData.AcctTypeFailCount
            objResponseContract.AcctTypeSuccessCount = fileUploadRequestData.AcctTypeData.Count - objResponseContract.ErrorMessageAcctType.Count
            Common.LogInfoWriter("SaveAcctType End by UserId:" & fileUploadRequestData.UserId)
        Catch fex As FaultException
            Common.LogErrorWriter(fex.Message & fex.StackTrace)
            Dim errorContract = New ErrorContract With {.ErrorCode = 6001, .ErrorMessage = fex.Message()}
            Throw New FaultException(Of ErrorContract)(errorContract, New FaultReason(errorContract.ErrorMessage))
        Catch ex As Exception
            Common.LogErrorWriter(ex.Message & Environment.NewLine & ex.StackTrace)
        End Try
        Return objResponseContract
    End Function

    ''' <summary>
    ''' This method will Insert account Excel Sheet data into Account Table.
    ''' </summary>
    ''' <param name="fileUploadRequestData">FileUPloadRequesetContract Object</param>
    ''' <param name="objResponseContract">FileUploadResponseContract Object</param>
    ''' <param name="fileProcessId">FileProcess Id</param>
    ''' <returns>FileUploadedResponseCOntract Object</returns>
    ''' <remarks></remarks>
    Private Function SaveAccount(ByVal fileUploadRequestData As FileUploadRequestContract, ByVal objResponseContract As FileUploadResponseContract, ByVal fileProcessId As Integer) As FileUploadResponseContract
        Try
            Common.LogInfoWriter("SaveAccount Start by UserId:" & fileUploadRequestData.UserId)
            objResponseContract.ErrorMessageAccount = DataValidationAndInsert.CheckAndInsertIntoAccount(fileUploadRequestData.AccountData, fileUploadRequestData.AnalysisId)
            objResponseContract.ResponseMessage = SaveFileUploadedResult(fileProcessId, 2, fileUploadRequestData.AccountData.Count - objResponseContract.ErrorMessageAccount.Count, objResponseContract.ErrorMessageAccount.Count + fileUploadRequestData.AccountFailCount)
            objResponseContract.AccountFailCount = objResponseContract.ErrorMessageAccount.Count + fileUploadRequestData.AccountFailCount
            objResponseContract.AccountSuccessCount = fileUploadRequestData.AccountData.Count - objResponseContract.ErrorMessageAccount.Count
            Common.LogInfoWriter("SaveAccount End by UserId:" & fileUploadRequestData.UserId)
        Catch fex As FaultException
            Common.LogErrorWriter(fex.Message & fex.StackTrace)
            Dim errorContract = New ErrorContract With {.ErrorCode = 7001, .ErrorMessage = fex.Message()}
            Throw New FaultException(Of ErrorContract)(errorContract, New FaultReason(errorContract.ErrorMessage))
        Catch ex As Exception
            Common.LogErrorWriter(ex.Message & Environment.NewLine & ex.StackTrace)
        End Try
        Return objResponseContract
    End Function

    ''' <summary>
    ''' This method will Insert Balance Excel Sheet data into Balance Table.
    ''' </summary>
    ''' <param name="fileUploadRequestData">FileUPloadRequesetContract Object</param>
    ''' <param name="objResponseContract">FileUploadResponseContract Object</param>
    ''' <param name="fileProcessId">FileProcess Id</param>
    ''' <returns>FileUploadedResponseCOntract Object</returns>
    ''' <remarks></remarks>
    Private Function SaveBalance(ByVal fileUploadRequestData As FileUploadRequestContract, ByVal objResponseContract As FileUploadResponseContract, ByVal fileProcessId As Integer) As FileUploadResponseContract
        Try
            Common.LogInfoWriter("SaveBalance Start by UserId:" & fileUploadRequestData.UserId)
            objResponseContract.ErrorMessageBalance = DataValidationAndInsert.CheckAndInsertIntoBalance(fileUploadRequestData.BalanceData, fileUploadRequestData.AnalysisId)
            objResponseContract.ResponseMessage = SaveFileUploadedResult(fileProcessId, 3, fileUploadRequestData.BalanceData.Count - objResponseContract.ErrorMessageBalance.Count, objResponseContract.ErrorMessageBalance.Count + fileUploadRequestData.BalanceFailCount)
            objResponseContract.BalanceFailCount = objResponseContract.ErrorMessageBalance.Count + fileUploadRequestData.BalanceFailCount
            objResponseContract.BalanceSuccessCount = fileUploadRequestData.BalanceData.Count - objResponseContract.ErrorMessageBalance.Count
            Common.LogInfoWriter("SaveBalance End by UserId:" & fileUploadRequestData.UserId)
        Catch fex As FaultException
            Common.LogErrorWriter(fex.Message & fex.StackTrace)
            Dim errorContract = New ErrorContract With {.ErrorCode = 8001, .ErrorMessage = fex.Message()}
            Throw New FaultException(Of ErrorContract)(errorContract, New FaultReason(errorContract.ErrorMessage))
        Catch ex As Exception
            Common.LogErrorWriter(ex.Message & Environment.NewLine & ex.StackTrace)
        End Try
        Return objResponseContract
    End Function

    ''' <summary>
    ''' This method will Insert Analysis Excel Sheet data into Analysis Table.
    ''' </summary>
    ''' <param name="fileUploadRequestData">FileUPloadRequesetContract Object</param>
    ''' <param name="objResponseContract">FileUploadResponseContract Object</param>
    ''' <param name="fileProcessId">FileProcess Id</param>
    ''' <returns>FileUploadedResponseCOntract Object</returns>
    ''' <remarks></remarks>
    Private Function SaveAnalysis(ByVal fileUploadRequestData As FileUploadRequestContract, ByVal objResponseContract As FileUploadResponseContract, ByVal fileProcessId As Integer) As FileUploadResponseContract
        Try
            Common.LogInfoWriter("SaveBalance Start by UserId:" & fileUploadRequestData.UserId)
            objResponseContract.ErrorMessageAnalysis = DataValidationAndInsert.CheckAndUpdateIntoAnalysis(fileUploadRequestData.AnalysisData, fileUploadRequestData.AnalysisId)
            objResponseContract.ResponseMessage = SaveFileUploadedResult(fileProcessId, 4, fileUploadRequestData.AnalysisData.Count - objResponseContract.ErrorMessageAnalysis.Count, objResponseContract.ErrorMessageAnalysis.Count + fileUploadRequestData.AnalysisFailCount)
            objResponseContract.AnalysisFailCount = objResponseContract.ErrorMessageAnalysis.Count + fileUploadRequestData.AnalysisFailCount
            objResponseContract.AnalysisSuccessCount = fileUploadRequestData.AnalysisData.Count - objResponseContract.ErrorMessageAnalysis.Count
            Common.LogInfoWriter("SaveBalance End by UserId:" & fileUploadRequestData.UserId)
        Catch fex As FaultException
            Common.LogErrorWriter(fex.Message & fex.StackTrace)
            Dim errorContract = New ErrorContract With {.ErrorCode = 8001, .ErrorMessage = fex.Message()}
            Throw New FaultException(Of ErrorContract)(errorContract, New FaultReason(errorContract.ErrorMessage))
        Catch ex As Exception
            Common.LogErrorWriter(ex.Message & Environment.NewLine & ex.StackTrace)
        End Try
        Return objResponseContract
    End Function

    ''' <summary>
    ''' This method will Insert the data into FileUplaodResult table with Success, Fail and Total count of the Records.
    ''' </summary>
    ''' <param name="fileProcessId">FileProcessID</param>
    ''' <param name="userTableId">UserTableId</param>
    ''' <param name="successCount">SuccessCount</param>
    ''' <param name="failCount">FailCount</param>
    ''' <returns>Message: Success or Error</returns>
    ''' <remarks></remarks>
    Private Function SaveFileUploadedResult(ByVal fileProcessId As Integer, ByVal userTableId As Integer, ByVal successCount As Integer, ByVal failCount As Integer) As String
        Dim strMessage As String = Common.SUCCESS
        Common.LogInfoWriter("SaveFileUploadResult Start ")
        Try
            Dim newFileUploadedResult = New FileUploadedResult With {.FileProcessId = fileProcessId, .TableId = userTableId, .SuccessCount = successCount, .FailCount = failCount, .TotalCount = successCount + failCount}
            dbcontext.FileUploadedResults.AddObject(newFileUploadedResult)
            dbcontext.SaveChanges()
            Common.LogInfoWriter("SaveFileUploadResult End")
        Catch fex As FaultException
            Common.LogErrorWriter(fex.Message & fex.StackTrace)
            Dim errorContract = New ErrorContract With {.ErrorCode = 9001, .ErrorMessage = fex.Message()}
            Throw New FaultException(Of ErrorContract)(errorContract, New FaultReason(errorContract.ErrorMessage))
        Catch ex As Exception
            Common.LogErrorWriter(ex.Message)
        End Try
        Return strMessage
    End Function

    ''' <summary>
    ''' This ValidateLogin method will call every Service Method call it will validate against Username and Password
    ''' </summary>
    ''' <param name="userName">User Name</param>
    ''' <param name="password">Password</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function ValidateLogin(ByVal userName As String, ByVal password As String) As LoginContract
        Common.LogInfoWriter("ValidateLogin() Execution Start ")
        Dim objLoginContract As LoginContract = Nothing
        Try
            If (userName.Equals(String.Empty)) Then
                Throw New FaultException("UserName is Empty.")
            End If
            If (password.Equals(String.Empty)) Then
                Throw New FaultException("Password is Empty.")
            End If
            Dim encPass = Common.EncryptPassword(password)
            Dim user = From a In dbcontext.Users.Where(Function(an) an.UserName = userName And an.Password = encPass)
            If (user.Count > 0) Then
                objLoginContract = New LoginContract()
                objLoginContract.UserName = user.First.UserName
                objLoginContract.CustomerId = user.First.CustomerId
                objLoginContract.Password = user.First.Password
                objLoginContract.UserRole = user.First.UserRoleId
                objLoginContract.UserID = user.First.UserId
            Else
                Common.LogErrorWriter("Invalid Credential")
                Dim errorContract = New ErrorContract With {.ErrorCode = 1002, .ErrorMessage = "Invalid Credential"}
                Throw New FaultException(Of ErrorContract)(errorContract, New FaultReason(errorContract.ErrorMessage))
            End If
            Common.LogInfoWriter("ValidateLogin() Execution End")
        Catch fex As FaultException
            Common.LogErrorWriter(fex.Message & fex.StackTrace)
            Dim errorContract = New ErrorContract With {.ErrorCode = 1002, .ErrorMessage = fex.Message()}
            Throw New FaultException(Of ErrorContract)(errorContract, New FaultReason(errorContract.ErrorMessage))
        Catch ex As Exception
            Common.LogErrorWriter("ValidateLogin() Executed with Error Message:" & ex.Message & Environment.NewLine & "Stack Strace: " & ex.StackTrace)
            Throw New Exception(ex.Message)
        End Try

        Return objLoginContract
    End Function

End Class


