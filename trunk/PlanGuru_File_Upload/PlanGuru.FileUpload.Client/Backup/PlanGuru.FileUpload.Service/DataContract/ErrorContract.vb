﻿<System.Runtime.Serialization.DataContractAttribute()> _
Public Class ErrorContract
    Inherits BaseContract

    <DataMember()> _
    Public Property ErrorMessage() As String

    <DataMember()> _
    Public Property ErrorCode() As Integer

End Class
