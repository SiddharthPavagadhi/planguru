﻿Public Class AnalysisEntity
    Private analysisId_value As Integer
    Private analysisName_value As String
    Private companyId_value As Integer
    Private createdOn_value As Date
    Private updatedOn_value As Date
    Private showBS_value As Boolean
    Private showRatios_value As Boolean
    Private option1_value As Boolean
    Private createdBy_value As String
    Private updatedBy_value As String
    Private firstYear_value As Integer
    Private numberofPeriods_value As Integer
    Private errormessageanalysisData_value As String

    Public Property AnalysisId() As Integer
    Public Property AnalysisName() As String
    Public Property CompanyId() As Integer
    Public Property CreatedOn() As Date
    Public Property UpdatedOn() As Date
    Public Property ShowBS() As Boolean
    Public Property ShowRatios() As Boolean
    Public Property Option1() As Boolean
    Public Property CreatedBy() As String
    Public Property UpdatedBy() As String
    Public Property FirstYear() As Integer
    Public Property NumberofPeriods() As Integer
    Public Property ErrorMessageAnalysisData() As String
End Class
