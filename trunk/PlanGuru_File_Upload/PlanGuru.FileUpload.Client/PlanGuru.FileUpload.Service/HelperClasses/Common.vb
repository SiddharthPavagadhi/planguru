﻿Imports System.Security
Imports System.Security.Cryptography
Imports System.ServiceModel
Imports System.IO

'This Common class have Logging Methods to write Information or Error logs in respective files.
Public Class Common

    Public Const LoggerDateFormat As String = "dd-MM-yyyy"
    Public Const SUCCESS As String = "Success"

    ''' <summary>
    ''' This Method will Write Error logs into ErrorLog file.
    ''' </summary>
    ''' <param name="message">Message to write in logfile.</param>
    ''' <remarks></remarks>
    Public Shared Sub LogErrorWriter(ByVal message As String)

        Dim FILE_NAME As String = AppDomain.CurrentDomain.BaseDirectory() & "Logs\Errors\ErrorLog" & Date.Now.Date.ToString(LoggerDateFormat) & ".log"

        If Not System.IO.Directory.Exists(AppDomain.CurrentDomain.BaseDirectory() & "Logs\Errors") Then
            System.IO.Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory() & "Logs\Errors")
        End If

        Dim sw As System.IO.StreamWriter = System.IO.File.AppendText(FILE_NAME)

        Try
            Dim logLine As String = System.String.Format("[{0:MM/dd/yyyy HH:mm:ss.fff }] {1}", DateTime.Now, message)
            sw.WriteLine(logLine)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        Finally
            sw.Close()
        End Try
    End Sub

    ''' <summary>
    ''' 'This Method will Write Info logs into InfoLog file.
    ''' </summary>
    ''' <param name="message">Message to write in Logfile</param>
    ''' <remarks></remarks>
    Public Shared Sub LogInfoWriter(ByVal message As String)

        Dim FILE_NAME As String = AppDomain.CurrentDomain.BaseDirectory() & "Logs\Info\InfoLog" & Date.Now.Date.ToString(LoggerDateFormat) & ".log"

        If Not System.IO.Directory.Exists(AppDomain.CurrentDomain.BaseDirectory() & "Logs\Info") Then
            System.IO.Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory() & "Logs\Info")
        End If

        Dim sw As System.IO.StreamWriter = System.IO.File.AppendText(FILE_NAME)

        Try
            Dim logLine As String = System.String.Format("[{0:MM/dd/yyyy HH:mm:ss.fff }] {1}", DateTime.Now, message)
            sw.WriteLine(logLine)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        Finally
            sw.Close()
        End Try
    End Sub

    ''' <summary>
    ''' This method will create Encryption for Plain Text as string
    ''' </summary>
    ''' <param name="plainTextPassword">plain text as string</param>
    ''' <returns>Encrypted string</returns>
    ''' <remarks></remarks>
    Public Shared Function EncryptPassword(ByVal plainTextPassword As String) As String
        Dim cipherText As String = String.Empty
        LogInfoWriter("EncryptPassword Starts")
        Try
            Dim passPhrase As String = "yourPassPhrase"
            Dim saltValue As String = "mySaltValue"
            Dim hashAlgorithm As String = "SHA1"

            Dim passwordIterations As Integer = 2
            Dim initVector As String = "@1B2c3D4e5F6g7H8"
            Dim keySize As Integer = 256

            Dim initVectorBytes As Byte() = Encoding.ASCII.GetBytes(initVector)
            Dim saltValueBytes As Byte() = Encoding.ASCII.GetBytes(saltValue)

            Dim plainTextBytes As Byte() = Encoding.UTF8.GetBytes(plainTextPassword)
            Dim password As New PasswordDeriveBytes(passPhrase, saltValueBytes, hashAlgorithm, passwordIterations)

            Dim keyBytes As Byte() = password.GetBytes(keySize / 8)
            Dim symmetricKey As New RijndaelManaged()

            symmetricKey.Mode = CipherMode.CBC

            Dim encryptor As ICryptoTransform = symmetricKey.CreateEncryptor(keyBytes, initVectorBytes)

            Dim memoryStream As New MemoryStream()
            Dim cryptoStream As New CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write)

            cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length)
            cryptoStream.FlushFinalBlock()
            Dim cipherTextBytes As Byte() = memoryStream.ToArray()
            memoryStream.Close()
            cryptoStream.Close()
            cipherText = Convert.ToBase64String(cipherTextBytes)
            LogInfoWriter("EncryptPassword Ends")
        Catch ex As Exception
            LogErrorWriter("EncryptPassword Error Message: " & ex.Message & ex.StackTrace)
            Throw New Exception(ex.Message)
        End Try

        Return cipherText
    End Function


    Public Shared Function Decrypt(ByVal cipherText As String) As String
        Dim passPhrase As String = "yourPassPhrase"
        Dim saltValue As String = "mySaltValue"
        Dim hashAlgorithm As String = "SHA1"

        Dim passwordIterations As Integer = 2
        Dim initVector As String = "@1B2c3D4e5F6g7H8"
        Dim keySize As Integer = 256
        ' Convert strings defining encryption key characteristics into byte
        ' arrays. Let us assume that strings only contain ASCII codes.
        ' If strings include Unicode characters, use Unicode, UTF7, or UTF8
        ' encoding.
        Dim initVectorBytes As Byte() = Encoding.ASCII.GetBytes(initVector)
        Dim saltValueBytes As Byte() = Encoding.ASCII.GetBytes(saltValue)

        ' Convert our ciphertext into a byte array.
        Dim cipherTextBytes As Byte() = Convert.FromBase64String(cipherText)

        ' First, we must create a password, from which the key will be 
        ' derived. This password will be generated from the specified 
        ' passphrase and salt value. The password will be created using
        ' the specified hash algorithm. Password creation can be done in
        ' several iterations.
        Dim password As New PasswordDeriveBytes(passPhrase, saltValueBytes, hashAlgorithm, passwordIterations)

        ' Use the password to generate pseudo-random bytes for the encryption
        ' key. Specify the size of the key in bytes (instead of bits).
        Dim keyBytes As Byte() = password.GetBytes(keySize / 8)

        ' Create uninitialized Rijndael encryption object.
        Dim symmetricKey As New RijndaelManaged()

        ' It is reasonable to set encryption mode to Cipher Block Chaining
        ' (CBC). Use default options for other symmetric key parameters.
        symmetricKey.Mode = CipherMode.CBC

        ' Generate decryptor from the existing key bytes and initialization 
        ' vector. Key size will be defined based on the number of the key 
        ' bytes.
        Dim decryptor As ICryptoTransform = symmetricKey.CreateDecryptor(keyBytes, initVectorBytes)

        ' Define memory stream which will be used to hold encrypted data.
        Dim memoryStream As New MemoryStream(cipherTextBytes)

        ' Define cryptographic stream (always use Read mode for encryption).
        Dim cryptoStream As New CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read)

        ' Since at this point we don't know what the size of decrypted data
        ' will be, allocate the buffer long enough to hold ciphertext;
        ' plaintext is never longer than ciphertext.
        Dim plainTextBytes As Byte() = New Byte(cipherTextBytes.Length - 1) {}

        ' Start decrypting.
        Dim decryptedByteCount As Integer = cryptoStream.Read(plainTextBytes, 0, plainTextBytes.Length)

        ' Close both streams.
        memoryStream.Close()
        cryptoStream.Close()

        ' Convert decrypted data into a string. 
        ' Let us assume that the original plaintext string was UTF8-encoded.
        Dim plainText As String = Encoding.UTF8.GetString(plainTextBytes, 0, decryptedByteCount)

        ' Return decrypted string.   
        Return plainText
    End Function

End Class

Public Enum UserRoles As Integer
    PGAAdmin = 1
    PGASupport = 2
    'Customer Administrative User (Super User) 
    SAU = 3
    'Company Administrative User
    CAU = 4
    'Company Regular User
    CRU = 5
    'Subscription Supervisor user
    SSU = 6
End Enum