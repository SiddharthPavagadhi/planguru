﻿<System.Runtime.Serialization.DataContractAttribute()> _
Public Class CompanyRequestContract
    Inherits BaseContract
 
    <DataMember()> _
    Public Property UserId() As Integer

    <DataMember()> _
    Public Property UserRoleId() As Integer

    <DataMember()> _
    Public Property CustomerId() As Integer

End Class
