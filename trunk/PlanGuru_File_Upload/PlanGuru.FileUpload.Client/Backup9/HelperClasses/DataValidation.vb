﻿Imports System.Reflection
Imports PlanGuru.FileUpload.Service.PGFileUploadEntities
Imports System.Data.Objects.ObjectContext

''' <summary>
''' This class perform Data Validation And Insertion functionality. 
''' </summary>
''' <remarks></remarks>
Public Class DataValidationAndInsert
    Shared context As PGFileUploadEntities = New PGFileUploadEntities()

    ''' <summary>
    ''' This Method will check data for Each row in given sheet and Insert into Balance, if valid else it will make entry into 
    ''' BalanceContractList to send back to caller.
    ''' </summary>
    ''' <param name="BalanceContractList">BalanceContractList as Object</param>
    ''' <returns>BalanceContractList object</returns>
    ''' <remarks></remarks>
    Public Shared Function CheckAndInsertIntoBalance(ByVal BalanceContractList As List(Of BalanceEntity), ByVal analysisId As String) As List(Of BalanceEntity)
        Dim strMessage = "Success"
        Common.LogInfoWriter("CheckAndInsertIntoBalance Execution Start")
        Dim objListErrorBalance As List(Of BalanceEntity) = New List(Of BalanceEntity)
        Try
            Dim countRowBalance As Integer = 1
            If (BalanceContractList.Count > 0) Then
                context = New PGFileUploadEntities()
                Dim listBalances = From balance In context.Balances _
                          Where balance.AnalysisId = analysisId _
                          Select balance

                If listBalances.Count > 0 Then
                    For Each objBalance In listBalances
                        context.Balances.DeleteObject(objBalance)
                    Next
                    context.SaveChanges()
                End If
            End If
            For Each balanceRow In BalanceContractList
                Try
                    Dim objBalanceContract As BalanceEntity = New BalanceEntity()

                    If (balanceRow.BalanceId.ToString().Equals(String.Empty)) Then
                        strMessage = strMessage & Environment.NewLine & " BalanceId is Blank at Row:" & countRowBalance
                    Else
                        If Not IsNumeric(balanceRow.BalanceId) Then
                            strMessage = strMessage & Environment.NewLine & " BalanceId is not Numeric at Row:" & countRowBalance
                        End If
                    End If


                    If (balanceRow.H101.ToString().Equals(String.Empty)) Then
                        strMessage = strMessage & Environment.NewLine & " H101 is Blank at Row:" & countRowBalance
                    Else
                        If Not IsNumeric(balanceRow.H101) Then
                            strMessage = strMessage & Environment.NewLine & " H101 is not Numeric at Row: " & countRowBalance
                        End If
                    End If
                    If (balanceRow.H102.ToString().Equals(String.Empty)) Then
                        strMessage = strMessage & Environment.NewLine & " H102 is Blank at Row:" & countRowBalance
                    Else
                        If Not IsNumeric(balanceRow.H102) Then
                            strMessage = strMessage & Environment.NewLine & " H102 is not Numeric at Row: " & countRowBalance
                        End If
                    End If
                    If (balanceRow.H103.ToString().Equals(String.Empty)) Then
                        strMessage = strMessage & Environment.NewLine & " H103 is Blank at Row:" & countRowBalance
                    Else
                        If Not IsNumeric(balanceRow.H103) Then
                            strMessage = strMessage & Environment.NewLine & " H103 is not Numeric at Row: " & countRowBalance
                        End If
                    End If
                    If (balanceRow.H104.ToString().Equals(String.Empty)) Then
                        strMessage = strMessage & Environment.NewLine & " H104 is Blank at Row:" & countRowBalance
                    Else
                        If Not IsNumeric(balanceRow.H104) Then
                            strMessage = strMessage & Environment.NewLine & " H104 is not Numeric at Row: " & countRowBalance
                        End If
                    End If
                    If (balanceRow.H105.ToString().Equals(String.Empty)) Then
                        strMessage = strMessage & Environment.NewLine & " H105 is Blank at Row:" & countRowBalance
                    Else
                        If Not IsNumeric(balanceRow.H105) Then
                            strMessage = strMessage & Environment.NewLine & " H105 is not Numeric at Row: " & countRowBalance
                        End If
                    End If
                    If (balanceRow.H106.ToString().Equals(String.Empty)) Then
                        strMessage = strMessage & Environment.NewLine & " H106 is Blank at Row:" & countRowBalance
                    Else
                        If Not IsNumeric(balanceRow.H106) Then
                            strMessage = strMessage & Environment.NewLine & " H106 is not Numeric at Row: " & countRowBalance
                        End If
                    End If
                    If (balanceRow.H107.ToString().Equals(String.Empty)) Then
                        strMessage = strMessage & Environment.NewLine & " H107 is Blank at Row:" & countRowBalance
                    Else
                        If Not IsNumeric(balanceRow.H107) Then
                            strMessage = strMessage & Environment.NewLine & " H107 is not Numeric at Row: " & countRowBalance
                        End If
                    End If
                    If (balanceRow.H108.ToString().Equals(String.Empty)) Then
                        strMessage = strMessage & Environment.NewLine & " H108 is Blank at Row:" & countRowBalance
                    Else
                        If Not IsNumeric(balanceRow.H108) Then
                            strMessage = strMessage & Environment.NewLine & " H108 is not Numeric at Row: " & countRowBalance
                        End If
                    End If
                    If (balanceRow.H109.ToString().Equals(String.Empty)) Then
                        strMessage = strMessage & Environment.NewLine & " H109 is Blank at Row:" & countRowBalance
                    Else
                        If Not IsNumeric(balanceRow.H109) Then
                            strMessage = strMessage & Environment.NewLine & " H109 is not Numeric at Row: " & countRowBalance
                        End If
                    End If
                    If (balanceRow.H110.ToString().Equals(String.Empty)) Then
                        strMessage = strMessage & Environment.NewLine & " H110 is Blank at Row:" & countRowBalance
                    Else
                        If Not IsNumeric(balanceRow.H110) Then
                            strMessage = strMessage & Environment.NewLine & " H110 is not Numeric at Row: " & countRowBalance
                        End If
                    End If
                    If (balanceRow.H111.ToString().Equals(String.Empty)) Then
                        strMessage = strMessage & Environment.NewLine & " H111 is Blank at Row:" & countRowBalance
                    Else
                        If Not IsNumeric(balanceRow.H111) Then
                            strMessage = strMessage & Environment.NewLine & " H111 is not Numeric at Row: " & countRowBalance
                        End If
                    End If
                    If (balanceRow.H112.ToString().Equals(String.Empty)) Then
                        strMessage = strMessage & Environment.NewLine & " H112 is Blank at Row:" & countRowBalance
                    Else
                        If Not IsNumeric(balanceRow.H112) Then
                            strMessage = strMessage & Environment.NewLine & " H112 is not Numeric at Row: " & countRowBalance
                        End If
                    End If
                    If (balanceRow.H201.ToString().Equals(String.Empty)) Then
                        strMessage = strMessage & Environment.NewLine & " H201 is Blank at Row:" & countRowBalance
                    Else
                        If Not IsNumeric(balanceRow.H201) Then
                            strMessage = strMessage & Environment.NewLine & " H201 is not Numeric at Row: " & countRowBalance
                        End If
                    End If
                    If (balanceRow.H202.ToString().Equals(String.Empty)) Then
                        strMessage = strMessage & Environment.NewLine & " H202 is Blank at Row:" & countRowBalance
                    Else
                        If Not IsNumeric(balanceRow.H202) Then
                            strMessage = strMessage & Environment.NewLine & " H202 is not Numeric at Row: " & countRowBalance
                        End If
                    End If
                    If (balanceRow.H203.ToString().Equals(String.Empty)) Then
                        strMessage = strMessage & Environment.NewLine & " H203 is Blank at Row:" & countRowBalance
                    Else
                        If Not IsNumeric(balanceRow.H203) Then
                            strMessage = strMessage & Environment.NewLine & " H203 is not Numeric at Row: " & countRowBalance
                        End If
                    End If
                    If (balanceRow.H204.ToString().Equals(String.Empty)) Then
                        strMessage = strMessage & Environment.NewLine & " H204 is Blank at Row:" & countRowBalance
                    Else
                        If Not IsNumeric(balanceRow.H204) Then
                            strMessage = strMessage & Environment.NewLine & " H204 is not Numeric at Row: " & countRowBalance
                        End If
                    End If
                    If (balanceRow.H205.ToString().Equals(String.Empty)) Then
                        strMessage = strMessage & Environment.NewLine & " H205 is Blank at Row:" & countRowBalance
                    Else
                        If Not IsNumeric(balanceRow.H205) Then
                            strMessage = strMessage & Environment.NewLine & " H205 is not Numeric at Row: " & countRowBalance
                        End If
                    End If
                    If (balanceRow.H206.ToString().Equals(String.Empty)) Then
                        strMessage = strMessage & Environment.NewLine & " H206 is Blank at Row:" & countRowBalance
                    Else
                        If Not IsNumeric(balanceRow.H206) Then
                            strMessage = strMessage & Environment.NewLine & " H206 is not Numeric at Row: " & countRowBalance
                        End If
                    End If
                    If (balanceRow.H207.ToString().Equals(String.Empty)) Then
                        strMessage = strMessage & Environment.NewLine & " H207 is Blank at Row:" & countRowBalance
                    Else
                        If Not IsNumeric(balanceRow.H207) Then
                            strMessage = strMessage & Environment.NewLine & " H207 is not Numeric at Row: " & countRowBalance
                        End If
                    End If
                    If (balanceRow.H208.ToString().Equals(String.Empty)) Then
                        strMessage = strMessage & Environment.NewLine & " H208 is Blank at Row:" & countRowBalance
                    Else
                        If Not IsNumeric(balanceRow.H208) Then
                            strMessage = strMessage & Environment.NewLine & " H208 is not Numeric at Row: " & countRowBalance
                        End If
                    End If
                    If (balanceRow.H209.ToString().Equals(String.Empty)) Then
                        strMessage = strMessage & Environment.NewLine & " H209 is Blank at Row:" & countRowBalance
                    Else
                        If Not IsNumeric(balanceRow.H209) Then
                            strMessage = strMessage & Environment.NewLine & " H209 is not Numeric at Row: " & countRowBalance
                        End If
                    End If
                    If (balanceRow.H210.ToString().Equals(String.Empty)) Then
                        strMessage = strMessage & Environment.NewLine & " H210 is Blank at Row:" & countRowBalance
                    Else
                        If Not IsNumeric(balanceRow.H210) Then
                            strMessage = strMessage & Environment.NewLine & " H210 is not Numeric at Row: " & countRowBalance
                        End If
                    End If
                    If (balanceRow.H211.ToString().Equals(String.Empty)) Then
                        strMessage = strMessage & Environment.NewLine & " H211 is Blank at Row:" & countRowBalance
                    Else
                        If Not IsNumeric(balanceRow.H211) Then
                            strMessage = strMessage & Environment.NewLine & " H211 is not Numeric at Row: " & countRowBalance
                        End If
                    End If
                    If (balanceRow.H212.ToString().Equals(String.Empty)) Then
                        strMessage = strMessage & Environment.NewLine & " H212 is Blank at Row:" & countRowBalance
                    Else
                        If Not IsNumeric(balanceRow.H212) Then
                            strMessage = strMessage & Environment.NewLine & " H212 is not Numeric at Row: " & countRowBalance
                        End If
                    End If
                    If (balanceRow.H301.ToString().Equals(String.Empty)) Then
                        strMessage = strMessage & Environment.NewLine & " H301 is Blank at Row:" & countRowBalance
                    Else
                        If Not IsNumeric(balanceRow.H301) Then
                            strMessage = strMessage & Environment.NewLine & " H301 is not Numeric at Row: " & countRowBalance
                        End If
                    End If
                    If (balanceRow.H302.ToString().Equals(String.Empty)) Then
                        strMessage = strMessage & Environment.NewLine & " H302 is Blank at Row:" & countRowBalance
                    Else
                        If Not IsNumeric(balanceRow.H302) Then
                            strMessage = strMessage & Environment.NewLine & " H302 is not Numeric at Row: " & countRowBalance
                        End If
                    End If
                    If (balanceRow.H303.ToString().Equals(String.Empty)) Then
                        strMessage = strMessage & Environment.NewLine & " H303 is Blank at Row:" & countRowBalance
                    Else
                        If Not IsNumeric(balanceRow.H303) Then
                            strMessage = strMessage & Environment.NewLine & " H303 is not Numeric at Row: " & countRowBalance
                        End If
                    End If
                    If (balanceRow.H304.ToString().Equals(String.Empty)) Then
                        strMessage = strMessage & Environment.NewLine & " H304 is Blank at Row:" & countRowBalance
                    Else
                        If Not IsNumeric(balanceRow.H304) Then
                            strMessage = strMessage & Environment.NewLine & " H304 is not Numeric at Row: " & countRowBalance
                        End If
                    End If
                    If (balanceRow.H305.ToString().Equals(String.Empty)) Then
                        strMessage = strMessage & Environment.NewLine & " H305 is Blank at Row:" & countRowBalance
                    Else
                        If Not IsNumeric(balanceRow.H305) Then
                            strMessage = strMessage & Environment.NewLine & " H305 is not Numeric at Row: " & countRowBalance
                        End If
                    End If
                    If (balanceRow.H306.ToString().Equals(String.Empty)) Then
                        strMessage = strMessage & Environment.NewLine & " H306 is Blank at Row:" & countRowBalance
                    Else
                        If Not IsNumeric(balanceRow.H306) Then
                            strMessage = strMessage & Environment.NewLine & " H306 is not Numeric at Row: " & countRowBalance
                        End If
                    End If
                    If (balanceRow.H307.ToString().Equals(String.Empty)) Then
                        strMessage = strMessage & Environment.NewLine & " H307 is Blank at Row:" & countRowBalance
                    Else
                        If Not IsNumeric(balanceRow.H307) Then
                            strMessage = strMessage & Environment.NewLine & " H307 is not Numeric at Row: " & countRowBalance
                        End If
                    End If
                    If (balanceRow.H308.ToString().Equals(String.Empty)) Then
                        strMessage = strMessage & Environment.NewLine & " H308 is Blank at Row:" & countRowBalance
                    Else
                        If Not IsNumeric(balanceRow.H308) Then
                            strMessage = strMessage & Environment.NewLine & " H308 is not Numeric at Row: " & countRowBalance
                        End If
                    End If
                    If (balanceRow.H309.ToString().Equals(String.Empty)) Then
                        strMessage = strMessage & Environment.NewLine & " H309 is Blank at Row:" & countRowBalance
                    Else
                        If Not IsNumeric(balanceRow.H309) Then
                            strMessage = strMessage & Environment.NewLine & " H309 is not Numeric at Row: " & countRowBalance
                        End If
                    End If
                    If (balanceRow.H310.ToString().Equals(String.Empty)) Then
                        strMessage = strMessage & Environment.NewLine & " H310 is Blank at Row:" & countRowBalance
                    Else
                        If Not IsNumeric(balanceRow.H310) Then
                            strMessage = strMessage & Environment.NewLine & " H310 is not Numeric at Row: " & countRowBalance
                        End If
                    End If
                    If (balanceRow.H311.ToString().Equals(String.Empty)) Then
                        strMessage = strMessage & Environment.NewLine & " H311 is Blank at Row:" & countRowBalance
                    Else
                        If Not IsNumeric(balanceRow.H311) Then
                            strMessage = strMessage & Environment.NewLine & " H311 is not Numeric at Row: " & countRowBalance
                        End If
                    End If
                    If (balanceRow.H312.ToString().Equals(String.Empty)) Then
                        strMessage = strMessage & Environment.NewLine & " H312 is Blank at Row:" & countRowBalance
                    Else
                        If Not IsNumeric(balanceRow.H312) Then
                            strMessage = strMessage & Environment.NewLine & " H312 is not Numeric at Row: " & countRowBalance
                        End If
                    End If
                    If (balanceRow.H401.ToString().Equals(String.Empty)) Then
                        strMessage = strMessage & Environment.NewLine & " H401 is Blank at Row:" & countRowBalance
                    Else
                        If Not IsNumeric(balanceRow.H401) Then
                            strMessage = strMessage & Environment.NewLine & " H401 is not Numeric at Row: " & countRowBalance
                        End If
                    End If
                    If (balanceRow.H402.ToString().Equals(String.Empty)) Then
                        strMessage = strMessage & Environment.NewLine & " H402 is Blank at Row:" & countRowBalance
                    Else
                        If Not IsNumeric(balanceRow.H402) Then
                            strMessage = strMessage & Environment.NewLine & " H402 is not Numeric at Row: " & countRowBalance
                        End If
                    End If
                    If (balanceRow.H403.ToString().Equals(String.Empty)) Then
                        strMessage = strMessage & Environment.NewLine & " H403 is Blank at Row:" & countRowBalance
                    Else
                        If Not IsNumeric(balanceRow.H403) Then
                            strMessage = strMessage & Environment.NewLine & " H403 is not Numeric at Row: " & countRowBalance
                        End If
                    End If
                    If (balanceRow.H404.ToString().Equals(String.Empty)) Then
                        strMessage = strMessage & Environment.NewLine & " H404 is Blank at Row:" & countRowBalance
                    Else
                        If Not IsNumeric(balanceRow.H404) Then
                            strMessage = strMessage & Environment.NewLine & " H404 is not Numeric at Row: " & countRowBalance
                        End If
                    End If
                    If (balanceRow.H405.ToString().Equals(String.Empty)) Then
                        strMessage = strMessage & Environment.NewLine & " H405 is Blank at Row:" & countRowBalance
                    Else
                        If Not IsNumeric(balanceRow.H405) Then
                            strMessage = strMessage & Environment.NewLine & " H405 is not Numeric at Row: " & countRowBalance
                        End If
                    End If
                    If (balanceRow.H406.ToString().Equals(String.Empty)) Then
                        strMessage = strMessage & Environment.NewLine & " H406 is Blank at Row:" & countRowBalance
                    Else
                        If Not IsNumeric(balanceRow.H406) Then
                            strMessage = strMessage & Environment.NewLine & " H406 is not Numeric at Row: " & countRowBalance
                        End If
                    End If
                    If (balanceRow.H407.ToString().Equals(String.Empty)) Then
                        strMessage = strMessage & Environment.NewLine & " H407 is Blank at Row:" & countRowBalance
                    Else
                        If Not IsNumeric(balanceRow.H407) Then
                            strMessage = strMessage & Environment.NewLine & " H407 is not Numeric at Row: " & countRowBalance
                        End If
                    End If
                    If (balanceRow.H408.ToString().Equals(String.Empty)) Then
                        strMessage = strMessage & Environment.NewLine & " H408 is Blank at Row:" & countRowBalance
                    Else
                        If Not IsNumeric(balanceRow.H408) Then
                            strMessage = strMessage & Environment.NewLine & " H408 is not Numeric at Row: " & countRowBalance
                        End If
                    End If
                    If (balanceRow.H409.ToString().Equals(String.Empty)) Then
                        strMessage = strMessage & Environment.NewLine & " H409 is Blank at Row:" & countRowBalance
                    Else
                        If Not IsNumeric(balanceRow.H409) Then
                            strMessage = strMessage & Environment.NewLine & " H409 is not Numeric at Row: " & countRowBalance
                        End If
                    End If
                    If (balanceRow.H410.ToString().Equals(String.Empty)) Then
                        strMessage = strMessage & Environment.NewLine & " H410 is Blank at Row:" & countRowBalance
                    Else
                        If Not IsNumeric(balanceRow.H410) Then
                            strMessage = strMessage & Environment.NewLine & " H410 is not Numeric at Row: " & countRowBalance
                        End If
                    End If
                    If (balanceRow.H411.ToString().Equals(String.Empty)) Then
                        strMessage = strMessage & Environment.NewLine & " H411 is Blank at Row:" & countRowBalance
                    Else
                        If Not IsNumeric(balanceRow.H411) Then
                            strMessage = strMessage & Environment.NewLine & " H411 is not Numeric at Row: " & countRowBalance
                        End If
                    End If
                    If (balanceRow.H412.ToString().Equals(String.Empty)) Then
                        strMessage = strMessage & Environment.NewLine & " H412 is Blank at Row:" & countRowBalance
                    Else
                        If Not IsNumeric(balanceRow.H412) Then
                            strMessage = strMessage & Environment.NewLine & " H412 is not Numeric at Row: " & countRowBalance
                        End If
                    End If
                    If (balanceRow.H501.ToString().Equals(String.Empty)) Then
                        strMessage = strMessage & Environment.NewLine & " H501 is Blank at Row:" & countRowBalance
                    Else
                        If Not IsNumeric(balanceRow.H501) Then
                            strMessage = strMessage & Environment.NewLine & " H501 is not Numeric at Row: " & countRowBalance
                        End If
                    End If
                    If (balanceRow.H502.ToString().Equals(String.Empty)) Then
                        strMessage = strMessage & Environment.NewLine & " H502 is Blank at Row:" & countRowBalance
                    Else
                        If Not IsNumeric(balanceRow.H502) Then
                            strMessage = strMessage & Environment.NewLine & " H502 is not Numeric at Row: " & countRowBalance
                        End If
                    End If
                    If (balanceRow.H503.ToString().Equals(String.Empty)) Then
                        strMessage = strMessage & Environment.NewLine & " H503 is Blank at Row:" & countRowBalance
                    Else
                        If Not IsNumeric(balanceRow.H503) Then
                            strMessage = strMessage & Environment.NewLine & " H503 is not Numeric at Row: " & countRowBalance
                        End If
                    End If
                    If (balanceRow.H504.ToString().Equals(String.Empty)) Then
                        strMessage = strMessage & Environment.NewLine & " H504 is Blank at Row:" & countRowBalance
                    Else
                        If Not IsNumeric(balanceRow.H504) Then
                            strMessage = strMessage & Environment.NewLine & " H504 is not Numeric at Row: " & countRowBalance
                        End If
                    End If
                    If (balanceRow.H505.ToString().Equals(String.Empty)) Then
                        strMessage = strMessage & Environment.NewLine & " H505 is Blank at Row:" & countRowBalance
                    Else
                        If Not IsNumeric(balanceRow.H505) Then
                            strMessage = strMessage & Environment.NewLine & " H505 is not Numeric at Row: " & countRowBalance
                        End If
                    End If
                    If (balanceRow.H506.ToString().Equals(String.Empty)) Then
                        strMessage = strMessage & Environment.NewLine & " H506 is Blank at Row:" & countRowBalance
                    Else
                        If Not IsNumeric(balanceRow.H506) Then
                            strMessage = strMessage & Environment.NewLine & " H506 is not Numeric at Row: " & countRowBalance
                        End If
                    End If
                    If (balanceRow.H507.ToString().Equals(String.Empty)) Then
                        strMessage = strMessage & Environment.NewLine & " H507 is Blank at Row:" & countRowBalance
                    Else
                        If Not IsNumeric(balanceRow.H507) Then
                            strMessage = strMessage & Environment.NewLine & " H507 is not Numeric at Row: " & countRowBalance
                        End If
                    End If
                    If (balanceRow.H508.ToString().Equals(String.Empty)) Then
                        strMessage = strMessage & Environment.NewLine & " H508 is Blank at Row:" & countRowBalance
                    Else
                        If Not IsNumeric(balanceRow.H508) Then
                            strMessage = strMessage & Environment.NewLine & " H508 is not Numeric at Row: " & countRowBalance
                        End If
                    End If
                    If (balanceRow.H509.ToString().Equals(String.Empty)) Then
                        strMessage = strMessage & Environment.NewLine & " H509 is Blank at Row:" & countRowBalance
                    Else
                        If Not IsNumeric(balanceRow.H509) Then
                            strMessage = strMessage & Environment.NewLine & " H509 is not Numeric at Row: " & countRowBalance
                        End If
                    End If
                    If (balanceRow.H510.ToString().Equals(String.Empty)) Then
                        strMessage = strMessage & Environment.NewLine & " H510 is Blank at Row:" & countRowBalance
                    Else
                        If Not IsNumeric(balanceRow.H510) Then
                            strMessage = strMessage & Environment.NewLine & " H510 is not Numeric at Row: " & countRowBalance
                        End If
                    End If
                    If (balanceRow.H511.ToString().Equals(String.Empty)) Then
                        strMessage = strMessage & Environment.NewLine & " H511 is Blank at Row:" & countRowBalance
                    Else
                        If Not IsNumeric(balanceRow.H511) Then
                            strMessage = strMessage & Environment.NewLine & " H511 is not Numeric at Row: " & countRowBalance
                        End If
                    End If
                    If (balanceRow.H512.ToString().Equals(String.Empty)) Then
                        strMessage = strMessage & Environment.NewLine & " H512 is Blank at Row:" & countRowBalance
                    Else
                        If Not IsNumeric(balanceRow.H512) Then
                            strMessage = strMessage & Environment.NewLine & " H512 is not Numeric at Row: " & countRowBalance
                        End If
                    End If


                    If (balanceRow.B101.ToString().Equals(String.Empty)) Then
                        strMessage = strMessage & Environment.NewLine & " B101 is Blank at Row:" & countRowBalance
                    Else
                        If Not IsNumeric(balanceRow.B101) Then
                            strMessage = strMessage & Environment.NewLine & " B101 is not Numeric at Row: " & countRowBalance
                        End If
                    End If
                    If (balanceRow.B102.ToString().Equals(String.Empty)) Then
                        strMessage = strMessage & Environment.NewLine & " B102 is Blank at Row:" & countRowBalance
                    Else
                        If Not IsNumeric(balanceRow.B102) Then
                            strMessage = strMessage & Environment.NewLine & " B102 is not Numeric at Row: " & countRowBalance
                        End If
                    End If
                    If (balanceRow.B103.ToString().Equals(String.Empty)) Then
                        strMessage = strMessage & Environment.NewLine & " B103 is Blank at Row:" & countRowBalance
                    Else
                        If Not IsNumeric(balanceRow.B103) Then
                            strMessage = strMessage & Environment.NewLine & " B103 is not Numeric at Row: " & countRowBalance
                        End If
                    End If
                    If (balanceRow.B104.ToString().Equals(String.Empty)) Then
                        strMessage = strMessage & Environment.NewLine & " B104 is Blank at Row:" & countRowBalance
                    Else
                        If Not IsNumeric(balanceRow.B104) Then
                            strMessage = strMessage & Environment.NewLine & " B104 is not Numeric at Row: " & countRowBalance
                        End If
                    End If
                    If (balanceRow.B105.ToString().Equals(String.Empty)) Then
                        strMessage = strMessage & Environment.NewLine & " B105 is Blank at Row:" & countRowBalance
                    Else
                        If Not IsNumeric(balanceRow.B105) Then
                            strMessage = strMessage & Environment.NewLine & " B105 is not Numeric at Row: " & countRowBalance
                        End If
                    End If
                    If (balanceRow.B106.ToString().Equals(String.Empty)) Then
                        strMessage = strMessage & Environment.NewLine & " B106 is Blank at Row:" & countRowBalance
                    Else
                        If Not IsNumeric(balanceRow.B106) Then
                            strMessage = strMessage & Environment.NewLine & " B106 is not Numeric at Row: " & countRowBalance
                        End If
                    End If
                    If (balanceRow.B107.ToString().Equals(String.Empty)) Then
                        strMessage = strMessage & Environment.NewLine & " B107 is Blank at Row:" & countRowBalance
                    Else
                        If Not IsNumeric(balanceRow.B107) Then
                            strMessage = strMessage & Environment.NewLine & " B107 is not Numeric at Row: " & countRowBalance
                        End If
                    End If
                    If (balanceRow.B108.ToString().Equals(String.Empty)) Then
                        strMessage = strMessage & Environment.NewLine & " B108 is Blank at Row:" & countRowBalance
                    Else
                        If Not IsNumeric(balanceRow.B108) Then
                            strMessage = strMessage & Environment.NewLine & " B108 is not Numeric at Row: " & countRowBalance
                        End If
                    End If
                    If (balanceRow.B109.ToString().Equals(String.Empty)) Then
                        strMessage = strMessage & Environment.NewLine & " B109 is Blank at Row:" & countRowBalance
                    Else
                        If Not IsNumeric(balanceRow.B109) Then
                            strMessage = strMessage & Environment.NewLine & " B109 is not Numeric at Row: " & countRowBalance
                        End If
                    End If
                    If (balanceRow.B110.ToString().Equals(String.Empty)) Then
                        strMessage = strMessage & Environment.NewLine & " B110 is Blank at Row:" & countRowBalance
                    Else
                        If Not IsNumeric(balanceRow.B110) Then
                            strMessage = strMessage & Environment.NewLine & " B110 is not Numeric at Row: " & countRowBalance
                        End If
                    End If
                    If (balanceRow.B111.ToString().Equals(String.Empty)) Then
                        strMessage = strMessage & Environment.NewLine & " B111 is Blank at Row:" & countRowBalance
                    Else
                        If Not IsNumeric(balanceRow.B111) Then
                            strMessage = strMessage & Environment.NewLine & " B111 is not Numeric at Row: " & countRowBalance
                        End If
                    End If
                    If (balanceRow.B112.ToString().Equals(String.Empty)) Then
                        strMessage = strMessage & Environment.NewLine & " B112 is Blank at Row:" & countRowBalance
                    Else
                        If Not IsNumeric(balanceRow.B112) Then
                            strMessage = strMessage & Environment.NewLine & " B112 is not Numeric at Row: " & countRowBalance
                        End If
                    End If


                    If (balanceRow.A101.ToString().Equals(String.Empty)) Then
                        strMessage = strMessage & Environment.NewLine & " A101 is Blank at Row:" & countRowBalance
                    Else
                        If Not IsNumeric(balanceRow.A101) Then
                            strMessage = strMessage & Environment.NewLine & " A101 is not Numeric at Row: " & countRowBalance
                        End If
                    End If
                    If (balanceRow.A102.ToString().Equals(String.Empty)) Then
                        strMessage = strMessage & Environment.NewLine & " A102 is Blank at Row:" & countRowBalance
                    Else
                        If Not IsNumeric(balanceRow.A102) Then
                            strMessage = strMessage & Environment.NewLine & " A102 is not Numeric at Row: " & countRowBalance
                        End If
                    End If
                    If (balanceRow.A103.ToString().Equals(String.Empty)) Then
                        strMessage = strMessage & Environment.NewLine & " A103 is Blank at Row:" & countRowBalance
                    Else
                        If Not IsNumeric(balanceRow.A103) Then
                            strMessage = strMessage & Environment.NewLine & " A103 is not Numeric at Row: " & countRowBalance
                        End If
                    End If
                    If (balanceRow.A104.ToString().Equals(String.Empty)) Then
                        strMessage = strMessage & Environment.NewLine & " A104 is Blank at Row:" & countRowBalance
                    Else
                        If Not IsNumeric(balanceRow.A104) Then
                            strMessage = strMessage & Environment.NewLine & " A104 is not Numeric at Row: " & countRowBalance
                        End If
                    End If
                    If (balanceRow.A105.ToString().Equals(String.Empty)) Then
                        strMessage = strMessage & Environment.NewLine & " A105 is Blank at Row:" & countRowBalance
                    Else
                        If Not IsNumeric(balanceRow.A105) Then
                            strMessage = strMessage & Environment.NewLine & " A105 is not Numeric at Row: " & countRowBalance
                        End If
                    End If
                    If (balanceRow.A106.ToString().Equals(String.Empty)) Then
                        strMessage = strMessage & Environment.NewLine & " A106 is Blank at Row:" & countRowBalance
                    Else
                        If Not IsNumeric(balanceRow.A106) Then
                            strMessage = strMessage & Environment.NewLine & " A106 is not Numeric at Row: " & countRowBalance
                        End If
                    End If
                    If (balanceRow.A107.ToString().Equals(String.Empty)) Then
                        strMessage = strMessage & Environment.NewLine & " A107 is Blank at Row:" & countRowBalance
                    Else
                        If Not IsNumeric(balanceRow.A107) Then
                            strMessage = strMessage & Environment.NewLine & " A107 is not Numeric at Row: " & countRowBalance
                        End If
                    End If
                    If (balanceRow.A108.ToString().Equals(String.Empty)) Then
                        strMessage = strMessage & Environment.NewLine & " A108 is Blank at Row:" & countRowBalance
                    Else
                        If Not IsNumeric(balanceRow.A108) Then
                            strMessage = strMessage & Environment.NewLine & " A108 is not Numeric at Row: " & countRowBalance
                        End If
                    End If
                    If (balanceRow.A109.ToString().Equals(String.Empty)) Then
                        strMessage = strMessage & Environment.NewLine & " A109 is Blank at Row:" & countRowBalance
                    Else
                        If Not IsNumeric(balanceRow.A109) Then
                            strMessage = strMessage & Environment.NewLine & " A109 is not Numeric at Row: " & countRowBalance
                        End If
                    End If
                    If (balanceRow.A110.ToString().Equals(String.Empty)) Then
                        strMessage = strMessage & Environment.NewLine & " A110 is Blank at Row:" & countRowBalance
                    Else
                        If Not IsNumeric(balanceRow.A110) Then
                            strMessage = strMessage & Environment.NewLine & " A110 is not Numeric at Row: " & countRowBalance
                        End If
                    End If
                    If (balanceRow.A111.ToString().Equals(String.Empty)) Then
                        strMessage = strMessage & Environment.NewLine & " A111 is Blank at Row:" & countRowBalance
                    Else
                        If Not IsNumeric(balanceRow.A111) Then
                            strMessage = strMessage & Environment.NewLine & " A111 is not Numeric at Row: " & countRowBalance
                        End If
                    End If
                    If (balanceRow.A112.ToString().Equals(String.Empty)) Then
                        strMessage = strMessage & Environment.NewLine & " A112 is Blank at Row:" & countRowBalance
                    Else
                        If Not IsNumeric(balanceRow.A112) Then
                            strMessage = strMessage & Environment.NewLine & " A112 is not Numeric at Row: " & countRowBalance
                        End If
                    End If
                    If (balanceRow.B201.ToString().Equals(String.Empty)) Then
                        strMessage = strMessage & Environment.NewLine & " B201 is Blank at Row:" & countRowBalance
                    Else
                        If Not IsNumeric(balanceRow.B201) Then
                            strMessage = strMessage & Environment.NewLine & " B201 is not Numeric at Row: " & countRowBalance
                        End If
                    End If
                    If (balanceRow.B202.ToString().Equals(String.Empty)) Then
                        strMessage = strMessage & Environment.NewLine & " B202 is Blank at Row:" & countRowBalance
                    Else
                        If Not IsNumeric(balanceRow.B202) Then
                            strMessage = strMessage & Environment.NewLine & " B202 is not Numeric at Row: " & countRowBalance
                        End If
                    End If
                    If (balanceRow.B203.ToString().Equals(String.Empty)) Then
                        strMessage = strMessage & Environment.NewLine & " B203 is Blank at Row:" & countRowBalance
                    Else
                        If Not IsNumeric(balanceRow.B203) Then
                            strMessage = strMessage & Environment.NewLine & " B203 is not Numeric at Row: " & countRowBalance
                        End If
                    End If
                    If (balanceRow.B204.ToString().Equals(String.Empty)) Then
                        strMessage = strMessage & Environment.NewLine & " B204 is Blank at Row:" & countRowBalance
                    Else
                        If Not IsNumeric(balanceRow.B204) Then
                            strMessage = strMessage & Environment.NewLine & " B204 is not Numeric at Row: " & countRowBalance
                        End If
                    End If
                    If (balanceRow.B205.ToString().Equals(String.Empty)) Then
                        strMessage = strMessage & Environment.NewLine & " B205 is Blank at Row:" & countRowBalance
                    Else
                        If Not IsNumeric(balanceRow.B205) Then
                            strMessage = strMessage & Environment.NewLine & " B205 is not Numeric at Row: " & countRowBalance
                        End If
                    End If
                    If (balanceRow.B206.ToString().Equals(String.Empty)) Then
                        strMessage = strMessage & Environment.NewLine & " B206 is Blank at Row:" & countRowBalance
                    Else
                        If Not IsNumeric(balanceRow.B206) Then
                            strMessage = strMessage & Environment.NewLine & " B206 is not Numeric at Row: " & countRowBalance
                        End If
                    End If
                    If (balanceRow.B207.ToString().Equals(String.Empty)) Then
                        strMessage = strMessage & Environment.NewLine & " B207 is Blank at Row:" & countRowBalance
                    Else
                        If Not IsNumeric(balanceRow.B207) Then
                            strMessage = strMessage & Environment.NewLine & " B207 is not Numeric at Row: " & countRowBalance
                        End If
                    End If
                    If (balanceRow.B208.ToString().Equals(String.Empty)) Then
                        strMessage = strMessage & Environment.NewLine & " B208 is Blank at Row:" & countRowBalance
                    Else
                        If Not IsNumeric(balanceRow.B208) Then
                            strMessage = strMessage & Environment.NewLine & " B208 is not Numeric at Row: " & countRowBalance
                        End If
                    End If
                    If (balanceRow.B209.ToString().Equals(String.Empty)) Then
                        strMessage = strMessage & Environment.NewLine & " B209 is Blank at Row:" & countRowBalance
                    Else
                        If Not IsNumeric(balanceRow.B209) Then
                            strMessage = strMessage & Environment.NewLine & " B209 is not Numeric at Row: " & countRowBalance
                        End If
                    End If
                    If (balanceRow.B210.ToString().Equals(String.Empty)) Then
                        strMessage = strMessage & Environment.NewLine & " B210 is Blank at Row:" & countRowBalance
                    Else
                        If Not IsNumeric(balanceRow.B210) Then
                            strMessage = strMessage & Environment.NewLine & " B210 is not Numeric at Row: " & countRowBalance
                        End If
                    End If
                    If (balanceRow.B211.ToString().Equals(String.Empty)) Then
                        strMessage = strMessage & Environment.NewLine & " B211 is Blank at Row:" & countRowBalance
                    Else
                        If Not IsNumeric(balanceRow.B211) Then
                            strMessage = strMessage & Environment.NewLine & " B211 is not Numeric at Row: " & countRowBalance
                        End If
                    End If
                    If (balanceRow.B212.ToString().Equals(String.Empty)) Then
                        strMessage = strMessage & Environment.NewLine & " B212 is Blank at Row:" & countRowBalance
                    Else
                        If Not IsNumeric(balanceRow.B212) Then
                            strMessage = strMessage & Environment.NewLine & " B212 is not Numeric at Row: " & countRowBalance
                        End If
                    End If

                    If (balanceRow.AccountId.ToString().Equals(String.Empty)) Then
                        strMessage = strMessage & Environment.NewLine & " AccountId is Blank at Row:" & countRowBalance
                    Else
                        If Not IsNumeric(balanceRow.AccountId) Then
                            strMessage = strMessage & Environment.NewLine & " AccountId is not Numeric at Row:" & countRowBalance
                        End If
                    End If

                    If (balanceRow.B301.ToString().Equals(String.Empty)) Then
                        strMessage = strMessage & Environment.NewLine & " B301 is Blank at Row:" & countRowBalance
                    Else
                        If Not IsNumeric(balanceRow.B301) Then
                            strMessage = strMessage & Environment.NewLine & " B301 is not Numeric at Row: " & countRowBalance
                        End If
                    End If
                    If (balanceRow.B302.ToString().Equals(String.Empty)) Then
                        strMessage = strMessage & Environment.NewLine & " B302 is Blank at Row:" & countRowBalance
                    Else
                        If Not IsNumeric(balanceRow.B302) Then
                            strMessage = strMessage & Environment.NewLine & " B302 is not Numeric at Row: " & countRowBalance
                        End If
                    End If
                    If (balanceRow.B303.ToString().Equals(String.Empty)) Then
                        strMessage = strMessage & Environment.NewLine & " B303 is Blank at Row:" & countRowBalance
                    Else
                        If Not IsNumeric(balanceRow.B303) Then
                            strMessage = strMessage & Environment.NewLine & " B303 is not Numeric at Row: " & countRowBalance
                        End If
                    End If
                    If (balanceRow.B304.ToString().Equals(String.Empty)) Then
                        strMessage = strMessage & Environment.NewLine & " B304 is Blank at Row:" & countRowBalance
                    Else
                        If Not IsNumeric(balanceRow.B304) Then
                            strMessage = strMessage & Environment.NewLine & " B304 is not Numeric at Row: " & countRowBalance
                        End If
                    End If
                    If (balanceRow.B305.ToString().Equals(String.Empty)) Then
                        strMessage = strMessage & Environment.NewLine & " B305 is Blank at Row:" & countRowBalance
                    Else
                        If Not IsNumeric(balanceRow.B305) Then
                            strMessage = strMessage & Environment.NewLine & " B305 is not Numeric at Row: " & countRowBalance
                        End If
                    End If
                    If (balanceRow.B306.ToString().Equals(String.Empty)) Then
                        strMessage = strMessage & Environment.NewLine & " B306 is Blank at Row:" & countRowBalance
                    Else
                        If Not IsNumeric(balanceRow.B306) Then
                            strMessage = strMessage & Environment.NewLine & " B306 is not Numeric at Row: " & countRowBalance
                        End If
                    End If
                    If (balanceRow.B307.ToString().Equals(String.Empty)) Then
                        strMessage = strMessage & Environment.NewLine & " B307 is Blank at Row:" & countRowBalance
                    Else
                        If Not IsNumeric(balanceRow.B307) Then
                            strMessage = strMessage & Environment.NewLine & " B307 is not Numeric at Row: " & countRowBalance
                        End If
                    End If
                    If (balanceRow.B308.ToString().Equals(String.Empty)) Then
                        strMessage = strMessage & Environment.NewLine & " B308 is Blank at Row:" & countRowBalance
                    Else
                        If Not IsNumeric(balanceRow.B308) Then
                            strMessage = strMessage & Environment.NewLine & " B308 is not Numeric at Row: " & countRowBalance
                        End If
                    End If
                    If (balanceRow.B309.ToString().Equals(String.Empty)) Then
                        strMessage = strMessage & Environment.NewLine & " B309 is Blank at Row:" & countRowBalance
                    Else
                        If Not IsNumeric(balanceRow.B309) Then
                            strMessage = strMessage & Environment.NewLine & " B309 is not Numeric at Row: " & countRowBalance
                        End If
                    End If
                    If (balanceRow.B310.ToString().Equals(String.Empty)) Then
                        strMessage = strMessage & Environment.NewLine & " B310 is Blank at Row:" & countRowBalance
                    Else
                        If Not IsNumeric(balanceRow.B310) Then
                            strMessage = strMessage & Environment.NewLine & " B310 is not Numeric at Row: " & countRowBalance
                        End If
                    End If
                    If (balanceRow.B311.ToString().Equals(String.Empty)) Then
                        strMessage = strMessage & Environment.NewLine & " B311 is Blank at Row:" & countRowBalance
                    Else
                        If Not IsNumeric(balanceRow.B311) Then
                            strMessage = strMessage & Environment.NewLine & " B311 is not Numeric at Row: " & countRowBalance
                        End If
                    End If
                    If (balanceRow.B312.ToString().Equals(String.Empty)) Then
                        strMessage = strMessage & Environment.NewLine & " B312 is Blank at Row:" & countRowBalance
                    Else
                        If Not IsNumeric(balanceRow.B312) Then
                            strMessage = strMessage & Environment.NewLine & " B312 is not Numeric at Row: " & countRowBalance
                        End If
                    End If

                    If Not (strMessage.Equals("Success")) Then

                        objBalanceContract = BalanceFailedDataIntoResponseContract(objBalanceContract, balanceRow, strMessage)
                        objListErrorBalance.Add(objBalanceContract)
                    Else
                        Try                           
                            Dim newBalance = New Balance With {.BalanceId = balanceRow.BalanceId, _
                                                       .H101 = balanceRow.H101, .H102 = balanceRow.H102, _
                                                       .H103 = balanceRow.H103, .H104 = balanceRow.H104, _
                                                       .H105 = balanceRow.H105, .H106 = balanceRow.H106, _
                                                       .H107 = balanceRow.H107, .H108 = balanceRow.H108, _
                                                       .H109 = balanceRow.H109, .H110 = balanceRow.H110, _
                                                       .H111 = balanceRow.H111, .H112 = balanceRow.H112, _
                                                       .H201 = balanceRow.H201, .H202 = balanceRow.H202, _
                                                       .H203 = balanceRow.H203, .H204 = balanceRow.H204, _
                                                       .H205 = balanceRow.H205, .H206 = balanceRow.H206, _
                                                       .H207 = balanceRow.H207, .H208 = balanceRow.H208, _
                                                       .H209 = balanceRow.H209, .H210 = balanceRow.H210, _
                                                       .H211 = balanceRow.H211, .H212 = balanceRow.H212, _
                                                       .H301 = balanceRow.H301, .H302 = balanceRow.H302, _
                                                       .H303 = balanceRow.H303, .H304 = balanceRow.H304, _
                                                       .H305 = balanceRow.H305, .H306 = balanceRow.H306, _
                                                       .H307 = balanceRow.H307, .H308 = balanceRow.H308, _
                                                       .H309 = balanceRow.H309, .H310 = balanceRow.H310, _
                                                       .H311 = balanceRow.H311, .H312 = balanceRow.H312, _
                                                       .H401 = balanceRow.H401, .H402 = balanceRow.H402, _
                                                       .H403 = balanceRow.H403, .H404 = balanceRow.H404, _
                                                       .H405 = balanceRow.H405, .H406 = balanceRow.H406, _
                                                       .H407 = balanceRow.H407, .H408 = balanceRow.H408, _
                                                       .H409 = balanceRow.H409, .H410 = balanceRow.H410, _
                                                       .H411 = balanceRow.H411, .H412 = balanceRow.H412, _
                                                       .H501 = balanceRow.H501, .H502 = balanceRow.H502, _
                                                       .H503 = balanceRow.H503, .H504 = balanceRow.H504, _
                                                       .H505 = balanceRow.H505, .H506 = balanceRow.H506, _
                                                       .H507 = balanceRow.H507, .H508 = balanceRow.H508, _
                                                       .H509 = balanceRow.H509, .H510 = balanceRow.H510, _
                                                       .H511 = balanceRow.H511, .H512 = balanceRow.H512, _
                                                       .B101 = balanceRow.B101, .B102 = balanceRow.B102, _
                                                       .B103 = balanceRow.B103, .B104 = balanceRow.B104, _
                                                       .B105 = balanceRow.B105, .B106 = balanceRow.B106, _
                                                       .B107 = balanceRow.B107, .B108 = balanceRow.B108, _
                                                       .B109 = balanceRow.B109, .B110 = balanceRow.B110, _
                                                       .B111 = balanceRow.B111, .B112 = balanceRow.B112, _
                                                       .A101 = balanceRow.A101, .A102 = balanceRow.A102, _
                                                       .A103 = balanceRow.A103, .A104 = balanceRow.A104, _
                                                       .A105 = balanceRow.A105, .A106 = balanceRow.A106, _
                                                       .A107 = balanceRow.A107, .A108 = balanceRow.A108, _
                                                       .A109 = balanceRow.A109, .A110 = balanceRow.A110, _
                                                       .A111 = balanceRow.A111, .A112 = balanceRow.A112, _
                                                       .B201 = balanceRow.B201, .B202 = balanceRow.B202, _
                                                       .B203 = balanceRow.B203, .B204 = balanceRow.B204, _
                                                       .B205 = balanceRow.B205, .B206 = balanceRow.B206, _
                                                       .B207 = balanceRow.B207, .B208 = balanceRow.B208, _
                                                       .B209 = balanceRow.B209, .B210 = balanceRow.B210, _
                                                       .B211 = balanceRow.B211, .B212 = balanceRow.B212, _
                                                        .B301 = balanceRow.B301, .B302 = balanceRow.B302, _
                                                        .B303 = balanceRow.B303, .B304 = balanceRow.B304, _
                                                        .B305 = balanceRow.B305, .B306 = balanceRow.B306, _
                                                        .B307 = balanceRow.B307, .B308 = balanceRow.B308, _
                                                        .B309 = balanceRow.B309, .B310 = balanceRow.B310, _
                                                        .B311 = balanceRow.B311, .B312 = balanceRow.B312, _                                                      
                                                        .AccountId = balanceRow.AccountId, .AnalysisId = analysisId}

                            context.Balances.AddObject(newBalance)
                            context.SaveChanges()
                        Catch ex As Exception
                            objBalanceContract = BalanceFailedDataIntoResponseContract(objBalanceContract, balanceRow, ex.Message)
                            objListErrorBalance.Add(objBalanceContract)
                            Common.LogErrorWriter(ex.Message & Environment.NewLine & ex.InnerException.ToString() & Environment.NewLine & ex.StackTrace)
                        End Try
                    End If
                    countRowBalance = countRowBalance + 1
                Catch ex As Exception
                    countRowBalance = countRowBalance + 1
                    Common.LogErrorWriter(ex.Message & Environment.NewLine & ex.StackTrace)
                End Try
            Next
            Common.LogInfoWriter("CheckAndInsertIntoBalance Execution End")
        Catch ex As Exception
            Common.LogErrorWriter("CheckAndInsertIntoBalance Execution End with Error Message:" & ex.Message & ex.StackTrace)
        End Try
        Return objListErrorBalance
    End Function

    ''' <summary>
    ''' This Method will check data for each row in given sheet and insert into Account table, if valid else it will make an entry into 
    ''' AccountContractList to send back to caller.
    ''' </summary>
    ''' <param name="AccountContractList">AccountContractList as Object</param>
    ''' <param name="analysisId">Analysis Id</param>
    ''' <returns>List of AccountEntity</returns>
    ''' <remarks></remarks>
    Public Shared Function CheckAndInsertIntoAccount(ByVal AccountContractList As List(Of AccountEntity), ByVal analysisId As String) As List(Of AccountEntity)
        Dim strMessage = "Success"
        Common.LogInfoWriter("CheckAndInsertIntoAccount Execution Start")
        Dim objListErrorAccount As List(Of AccountEntity) = New List(Of AccountEntity)
        Try
            Dim countRowAccount As Integer = 1
            If (AccountContractList.Count > 0) Then
                context = New PGFileUploadEntities()
                Dim listAccounts = From account In context.Accounts _
                          Where account.AnalysisId = analysisId _
                          Select account
                If listAccounts.Count > 0 Then
                    For Each objAccount In listAccounts
                        context.Accounts.DeleteObject(objAccount)
                    Next
                    context.SaveChanges()
                End If
            End If
            For Each accountRow In AccountContractList
                Try
                    Dim objAccountEntity As AccountEntity = New AccountEntity()
                    If Not IsNumeric(accountRow.AccountId) Then
                        strMessage = strMessage & Environment.NewLine & " AccountId is not Numeric at Row:" & countRowAccount
                    End If
                    If (accountRow.AcctDescriptor.Equals(String.Empty)) Then
                        strMessage = strMessage & Environment.NewLine & " AcctDescriptor is Null at Row:" & countRowAccount
                    End If
                    If (accountRow.Description.Equals(String.Empty)) Then
                        strMessage = strMessage & Environment.NewLine & " Description is Null at Row:" & countRowAccount
                    End If

                    If (accountRow.SortSequence.Equals(String.Empty)) Then
                        strMessage = strMessage & Environment.NewLine & " SortSequence is Null at Row:" & countRowAccount
                    ElseIf Not IsNumeric(accountRow.SortSequence) Then
                        strMessage = strMessage & Environment.NewLine & " SortSequence is not Numeric at Row:" & countRowAccount
                    End If

                    If (accountRow.AcctTypeId.Equals(String.Empty)) Then
                        strMessage = strMessage & Environment.NewLine & " AcctTypeId is Null at Row:" & countRowAccount
                    ElseIf Not IsNumeric(accountRow.AcctTypeId) Then
                        strMessage = strMessage & Environment.NewLine & " AcctTypeId is not Numeric at Row:" & countRowAccount
                    End If

                    If Not (strMessage.Equals("Success")) Then
                        objAccountEntity = AccountFailedDataIntoResponseContract(objAccountEntity, accountRow, strMessage)
                        objListErrorAccount.Add(objAccountEntity)
                    Else
                        Try
                            Dim newAccount = New Account With {.AccountId = accountRow.AccountId, .AcctDescriptor = accountRow.AcctDescriptor, _
                                                           .SortSequence = accountRow.SortSequence, .Description = accountRow.Description, _
                                                           .Subgrouping = accountRow.SubGrouping, .AcctTypeId = accountRow.AcctTypeId, .AnalysisId = analysisId, .NumberFormat = accountRow.NumberFormat, .TotalType = accountRow.TotalType}
                            context.Accounts.AddObject(newAccount)
                            context.SaveChanges()
                        Catch ex As Exception
                            objAccountEntity = AccountFailedDataIntoResponseContract(objAccountEntity, accountRow, ex.Message)
                            objListErrorAccount.Add(objAccountEntity)
                            Common.LogErrorWriter(ex.Message & Environment.NewLine & ex.StackTrace)
                        End Try

                    End If
                    countRowAccount = countRowAccount + 1
                Catch ex As Exception
                    countRowAccount = countRowAccount + 1
                    Common.LogErrorWriter(ex.Message & Environment.NewLine & ex.StackTrace)
                End Try
            Next
            Common.LogInfoWriter("CheckAndInsertIntoAccount Execution End")
        Catch ex As Exception
            Common.LogErrorWriter("CheckAndInsertIntoAccount Execution End with Error Message:" & ex.Message & ex.StackTrace)
        End Try
        Return objListErrorAccount
    End Function

    ''' <summary>
    ''' This Medhod will check AcctType data for each row in given sheet and insert into AcctType Table, if valie else it will add an entry into
    ''' AcctTypeContractList to send back to caller
    ''' </summary>
    ''' <param name="AcctTypeContractList">AcctTypeContractList object</param>
    ''' <param name="analysisId">analysis Id</param>
    ''' <returns>List of AcctTypeEntity</returns>
    ''' <remarks></remarks>
    Public Shared Function CheckAndInsertIntoAcctType(ByVal AcctTypeContractList As List(Of AcctTypeEntity), ByVal analysisId As String) As List(Of AcctTypeEntity)
        Common.LogInfoWriter("CheckAndInsertIntoAcctType Start ")
        Dim objListErrorAcctType As List(Of AcctTypeEntity) = New List(Of AcctTypeEntity)
        Try
            Dim strMessage = "Success"
            Dim countRowAcctType As Integer = 1
            If (AcctTypeContractList.Count > 0) Then
                context = New PGFileUploadEntities()
                Dim listAcctTypes = From acctType In context.AcctTypes _
                          Where acctType.AnalysisId = analysisId _
                          Select acctType

                If listAcctTypes.Count > 0 Then
                    For Each obhAcctType In listAcctTypes
                        context.AcctTypes.DeleteObject(obhAcctType)
                    Next
                    context.SaveChanges()
                End If

            End If
            For Each acctTypeRow In AcctTypeContractList
                Try

                    Dim objAcctTypeContract As AcctTypeEntity = New AcctTypeEntity()
                    If (acctTypeRow.AcctTypeId.ToString().Equals(String.Empty)) Then
                        strMessage = strMessage & Environment.NewLine & " AcctTypeId is Blank at Row:" & countRowAcctType
                    Else
                        If Not IsNumeric(acctTypeRow.AcctTypeId) Then
                            strMessage = strMessage & Environment.NewLine & " AcctTypeId is not Numeric at Row:" & countRowAcctType
                        End If
                    End If

                    If (acctTypeRow.TypeDesc.Equals(String.Empty)) Then
                        strMessage = strMessage & Environment.NewLine & " TypeDesc is Null at Row:" & countRowAcctType
                    End If
                    If (acctTypeRow.ClassDesc.Equals(String.Empty)) Then
                        strMessage = strMessage & Environment.NewLine & " ClassDesc is Null at Row:" & countRowAcctType
                    End If

                    If (acctTypeRow.SubclassDesc.Equals(String.Empty)) Then
                        strMessage = strMessage & Environment.NewLine & " SubclassDesc is Null at Row:" & countRowAcctType
                    End If

                    If Not (strMessage.Equals("Success")) Then
                        objAcctTypeContract = AcctTypeFailedDataIntoResponseContract(objAcctTypeContract, acctTypeRow, strMessage)
                        objListErrorAcctType.Add(objAcctTypeContract)
                    Else
                        Try
                            Dim newAccType = New AcctType With {.AcctTypeId = acctTypeRow.AcctTypeId, .TypeDesc = acctTypeRow.TypeDesc, .ClassDesc = acctTypeRow.ClassDesc, .SubclassDesc = acctTypeRow.SubclassDesc, .AnalysisId = analysisId}
                            context.AcctTypes.AddObject(newAccType)
                            context.SaveChanges()

                        Catch ex As Exception
                            objAcctTypeContract = AcctTypeFailedDataIntoResponseContract(objAcctTypeContract, acctTypeRow, ex.Message)
                            objListErrorAcctType.Add(objAcctTypeContract)
                            Common.LogErrorWriter(ex.Message & Environment.NewLine & ex.StackTrace)
                        End Try

                    End If
                    countRowAcctType = countRowAcctType + 1
                Catch ex As Exception
                    countRowAcctType = countRowAcctType + 1
                    Common.LogErrorWriter(ex.Message & Environment.NewLine & ex.StackTrace)
                End Try
            Next
            Common.LogInfoWriter("CheckAndInsertIntoAcctType Execution End")
        Catch ex As Exception
            Common.LogErrorWriter("CheckAndInsertIntoAcctType Execution End with Error Message:" & ex.Message & ex.StackTrace)
        End Try

        Return objListErrorAcctType
    End Function


    Public Shared Function CheckAndUpdateIntoAnalysis(ByVal AnalysisContractList As List(Of AnalysisEntity), ByVal analysisId As String) As List(Of AnalysisEntity)
        Common.LogInfoWriter("CheckAndInsertIntoAnalysis Start ")
        Dim objListErrorAnalysis As List(Of AnalysisEntity) = New List(Of AnalysisEntity)
        Try
            Dim strMessage = "Success"
            Dim countRowAnalysis As Integer = 1
            Dim objAnalysis As Analysis = Nothing

            For Each analysisRow In AnalysisContractList
                Try

                    Dim objAnalysisContract As AnalysisEntity = New AnalysisEntity()

                    If (analysisRow.FirstYear.ToString().Equals(String.Empty)) Then
                        strMessage = strMessage & Environment.NewLine & " FirstYear is Blank at Row:" & countRowAnalysis
                    Else
                        If Not IsNumeric(analysisRow.FirstYear) Then
                            strMessage = strMessage & Environment.NewLine & " FirstYear is not Numeric at Row:" & countRowAnalysis
                        End If
                    End If

                    If (analysisRow.NumberofPeriods.ToString().Equals(String.Empty)) Then
                        strMessage = strMessage & Environment.NewLine & " NumberofPeriods is Blank at Row:" & countRowAnalysis
                    Else
                        If Not IsNumeric(analysisRow.NumberofPeriods) Then
                            strMessage = strMessage & Environment.NewLine & " NumberofPeriods is not Numeric at Row:" & countRowAnalysis
                        End If
                    End If

                    If Not (strMessage.Equals("Success")) Then
                        objAnalysisContract = AnalysisFailedDataIntoResponseContract(objAnalysisContract, analysisRow, strMessage)
                        objListErrorAnalysis.Add(objAnalysisContract)
                    Else
                        Try
                            context = New PGFileUploadEntities()
                            objAnalysis = (From analysis In context.Analyses _
                                      Where analysis.AnalysisId = analysisId _
                                      Select analysis).FirstOrDefault()

                            objAnalysis.FirstYear = analysisRow.FirstYear
                            objAnalysis.NumberofPeriods = analysisRow.NumberofPeriods
                            context.SaveChanges()

                        Catch ex As Exception
                            objAnalysisContract = AnalysisFailedDataIntoResponseContract(objAnalysisContract, analysisRow, ex.Message)
                            objListErrorAnalysis.Add(objAnalysisContract)
                            Common.LogErrorWriter(ex.Message & Environment.NewLine & ex.StackTrace)
                        End Try

                    End If
                    countRowAnalysis = countRowAnalysis + 1
                Catch ex As Exception
                    countRowAnalysis = countRowAnalysis + 1
                    Common.LogErrorWriter(ex.Message & Environment.NewLine & ex.StackTrace)
                End Try
            Next
            Common.LogInfoWriter("CheckAndUpdateIntoAnalysis Execution End")
        Catch ex As Exception
            Common.LogErrorWriter("CheckAndUpdateIntoAnalysis Execution End with Error Message:" & ex.Message & ex.StackTrace)
        End Try

        Return objListErrorAnalysis

    End Function

    ''' <summary>
    ''' This Method will create object of AcctTypeEntity object with data.
    ''' </summary>
    ''' <param name="objAcctTypeContract">AcctTypeEntity Object</param>
    ''' <param name="acctTypeRow">AcctTypeEntity data object</param>
    ''' <param name="strMessage">Error message if any</param>
    ''' <returns>AcctTypeEntity Object with data</returns>
    ''' <remarks></remarks>
    Private Shared Function AcctTypeFailedDataIntoResponseContract(ByVal objAcctTypeContract As AcctTypeEntity, ByVal acctTypeRow As AcctTypeEntity, ByVal strMessage As String) As AcctTypeEntity
        objAcctTypeContract.AcctTypeId = acctTypeRow.AcctTypeId
        objAcctTypeContract.TypeDesc = acctTypeRow.TypeDesc
        objAcctTypeContract.ClassDesc = acctTypeRow.ClassDesc
        objAcctTypeContract.SubclassDesc = acctTypeRow.SubclassDesc
        objAcctTypeContract.AnalysisId = acctTypeRow.AnalysisId
        objAcctTypeContract.ErrorMessageAcctTypeData = strMessage
        Return objAcctTypeContract
    End Function

    Private Shared Function AnalysisFailedDataIntoResponseContract(ByVal objAnalysisContract As AnalysisEntity, ByVal analysisRow As AnalysisEntity, ByVal strMessage As String) As AnalysisEntity
        objAnalysisContract.FirstYear = analysisRow.FirstYear
        objAnalysisContract.NumberofPeriods = analysisRow.NumberofPeriods
        objAnalysisContract.ErrorMessageAnalysisData = strMessage
        Return objAnalysisContract
    End Function
    ''' <summary>
    ''' This Method will create object of AccountEntity object with data.
    ''' </summary>
    ''' <param name="objAccountContract">AccountEntity Object</param>
    ''' <param name="accountRow">AccountEntity data object</param>
    ''' <param name="strMessage">Error message if any</param>
    ''' <returns>AccountEntity Object with data</returns>
    ''' <remarks></remarks>
    Private Shared Function AccountFailedDataIntoResponseContract(ByVal objAccountContract As AccountEntity, ByVal accountRow As AccountEntity, ByVal strMessage As String) As AccountEntity
        objAccountContract.AccountId = accountRow.AccountId
        objAccountContract.AcctTypeId = accountRow.AcctTypeId
        objAccountContract.AcctDescriptor = accountRow.AcctDescriptor
        objAccountContract.Description = accountRow.Description
        objAccountContract.SortSequence = accountRow.SortSequence
        objAccountContract.SubGrouping = accountRow.SubGrouping
        objAccountContract.NumberFormat = accountRow.NumberFormat
        objAccountContract.TotalType = accountRow.TotalType
        objAccountContract.ErrorMessageAccountData = strMessage
        Return objAccountContract
    End Function

    ''' <summary>
    ''' This Method will create object of BalanceEntity object with data.
    ''' </summary>
    ''' <param name="objBalanceContract">BalanceEntity Object</param>
    ''' <param name="balanceRow">BalanceEntity data object</param>
    ''' <param name="strMessage">Error message if any</param>
    ''' <returns>BalanceEntity Object with data</returns>
    ''' <remarks></remarks>
    Private Shared Function BalanceFailedDataIntoResponseContract(ByVal objBalanceContract As BalanceEntity, ByVal balanceRow As BalanceEntity, ByVal strMessage As String) As BalanceEntity
        objBalanceContract.BalanceId = balanceRow.BalanceId
        objBalanceContract.AccountId = balanceRow.AccountId
        objBalanceContract.H101 = balanceRow.H101 & objBalanceContract.H102 = balanceRow.H102
        objBalanceContract.H103 = balanceRow.H103 & objBalanceContract.H104 = balanceRow.H104
        objBalanceContract.H105 = balanceRow.H105 & objBalanceContract.H106 = balanceRow.H106
        objBalanceContract.H107 = balanceRow.H107 & objBalanceContract.H108 = balanceRow.H108
        objBalanceContract.H109 = balanceRow.H109 & objBalanceContract.H110 = balanceRow.H110
        objBalanceContract.H111 = balanceRow.H111 & objBalanceContract.H112 = balanceRow.H112
        objBalanceContract.H201 = balanceRow.H201 & objBalanceContract.H202 = balanceRow.H202
        objBalanceContract.H203 = balanceRow.H203 & objBalanceContract.H204 = balanceRow.H204
        objBalanceContract.H205 = balanceRow.H205 & objBalanceContract.H206 = balanceRow.H206
        objBalanceContract.H207 = balanceRow.H207 & objBalanceContract.H208 = balanceRow.H208
        objBalanceContract.H209 = balanceRow.H209 & objBalanceContract.H210 = balanceRow.H210
        objBalanceContract.H211 = balanceRow.H211 & objBalanceContract.H212 = balanceRow.H212
        objBalanceContract.H301 = balanceRow.H301 & objBalanceContract.H302 = balanceRow.H302
        objBalanceContract.H303 = balanceRow.H303 & objBalanceContract.H304 = balanceRow.H304
        objBalanceContract.H305 = balanceRow.H305 & objBalanceContract.H306 = balanceRow.H306
        objBalanceContract.H307 = balanceRow.H307 & objBalanceContract.H308 = balanceRow.H308
        objBalanceContract.H309 = balanceRow.H309 & objBalanceContract.H310 = balanceRow.H310
        objBalanceContract.H311 = balanceRow.H311 & objBalanceContract.H312 = balanceRow.H312
        objBalanceContract.H401 = balanceRow.H401 & objBalanceContract.H402 = balanceRow.H402
        objBalanceContract.H403 = balanceRow.H403 & objBalanceContract.H404 = balanceRow.H404
        objBalanceContract.H405 = balanceRow.H405 & objBalanceContract.H406 = balanceRow.H406
        objBalanceContract.H407 = balanceRow.H407 & objBalanceContract.H408 = balanceRow.H408
        objBalanceContract.H409 = balanceRow.H409 & objBalanceContract.H410 = balanceRow.H410
        objBalanceContract.H411 = balanceRow.H411 & objBalanceContract.H412 = balanceRow.H412
        objBalanceContract.H501 = balanceRow.H501 & objBalanceContract.H502 = balanceRow.H502
        objBalanceContract.H503 = balanceRow.H503 & objBalanceContract.H504 = balanceRow.H504
        objBalanceContract.H505 = balanceRow.H505 & objBalanceContract.H506 = balanceRow.H506
        objBalanceContract.H507 = balanceRow.H507 & objBalanceContract.H508 = balanceRow.H508
        objBalanceContract.H509 = balanceRow.H509 & objBalanceContract.H510 = balanceRow.H510
        objBalanceContract.H511 = balanceRow.H511 & objBalanceContract.H512 = balanceRow.H512
        objBalanceContract.B101 = balanceRow.B101 & objBalanceContract.B102 = balanceRow.B102
        objBalanceContract.B103 = balanceRow.B103 & objBalanceContract.B104 = balanceRow.B104
        objBalanceContract.B105 = balanceRow.B105 & objBalanceContract.B106 = balanceRow.B106
        objBalanceContract.B107 = balanceRow.B107 & objBalanceContract.B108 = balanceRow.B108
        objBalanceContract.B109 = balanceRow.B109 & objBalanceContract.B110 = balanceRow.B110
        objBalanceContract.B111 = balanceRow.B111 & objBalanceContract.B112 = balanceRow.B112
        objBalanceContract.A101 = balanceRow.A101 & objBalanceContract.A102 = balanceRow.A102
        objBalanceContract.A103 = balanceRow.A103 & objBalanceContract.A104 = balanceRow.A104
        objBalanceContract.A105 = balanceRow.A105 & objBalanceContract.A106 = balanceRow.A106
        objBalanceContract.A107 = balanceRow.A107 & objBalanceContract.A108 = balanceRow.A108
        objBalanceContract.A109 = balanceRow.A109 & objBalanceContract.A110 = balanceRow.A110
        objBalanceContract.A111 = balanceRow.A111 & objBalanceContract.A112 = balanceRow.A112
        objBalanceContract.B201 = balanceRow.B201 & objBalanceContract.B202 = balanceRow.B202
        objBalanceContract.B203 = balanceRow.B203 & objBalanceContract.B204 = balanceRow.B204
        objBalanceContract.B205 = balanceRow.B205 & objBalanceContract.B206 = balanceRow.B206
        objBalanceContract.B207 = balanceRow.B207 & objBalanceContract.B208 = balanceRow.B208
        objBalanceContract.B209 = balanceRow.B209 & objBalanceContract.B210 = balanceRow.B210
        objBalanceContract.B211 = balanceRow.B211 & objBalanceContract.B212 = balanceRow.B212
        objBalanceContract.B301 = balanceRow.B301 & objBalanceContract.B302 = balanceRow.B302
        objBalanceContract.B303 = balanceRow.B303 & objBalanceContract.B304 = balanceRow.B304
        objBalanceContract.B305 = balanceRow.B305 & objBalanceContract.B306 = balanceRow.B306
        objBalanceContract.B307 = balanceRow.B307 & objBalanceContract.B308 = balanceRow.B308
        objBalanceContract.B309 = balanceRow.B309 & objBalanceContract.B310 = balanceRow.B310
        objBalanceContract.B311 = balanceRow.B311 & objBalanceContract.B312 = balanceRow.B312
        objBalanceContract.ErrorMessageBalanceData = strMessage
        Return objBalanceContract
    End Function

End Class
